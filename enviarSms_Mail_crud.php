<?php
session_start();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
include(__DIR__."/../conexion.php");
if(isset($_GET['sendMail'])){
    $cuenta=isset($_POST['cuenta']) ? $_POST['cuenta'] :false;
    $para=isset($_POST['para']) ? $_POST['para'] :false;
    $cc=isset($_POST['cc']) ? $_POST['cc'] :null;
    $cco=isset($_POST['cco']) ? $_POST['cco'] :null;
    $asunto=isset($_POST['asunto']) ? $_POST['asunto'] :false;
    $cuerpo=isset($_POST['cuerpo']) ? $_POST['cuerpo'] :false;
    $adjuntos=isset($_POST['stradjuntos']) ? $_POST['stradjuntos'] :null;
    
    if($cuenta && $para && $asunto && $cuerpo){
        $sql=$mysqli->query("SELECT id_huesped FROM dyalogo_canales_electronicos.dy_ce_configuracion WHERE id={$cuenta}");
        if($sql && $sql->num_rows==1){
            $sql=$sql->fetch_object();
            $huesped=$sql->id_huesped;
            $data = array(
                "strUsuario_t" => "crm",
                "strToken_t" => "D43dasd321",
                "strIdCfg_t" => $cuenta,
                "strTo_t" => $para,
                "strCC_t" => $cc,
                "strCCO_t" => $cco,                
                "strSubject_t" => $asunto,
                "strMessage_t" => $cuerpo,
                "strListaAdjuntos_t" => $adjuntos,
                "intIdEstPas_t" => null,
                "intConsinteMiembro_t" => null,
                "intIdAgente_t" => null,
                "intIdHuesped_t" => $huesped
            );

            $data_string = json_encode($data);
            //echo $data_string;
            $ch = curl_init('http://localhost:8080/dyalogocore/api/ce/correo/sendmailservice');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $respuesta = curl_exec($ch);
            echo $respuesta;
            $error = curl_error($ch);
            echo $error;
            curl_close($ch);
        }else{
            echo json_encode(array('strEstado_t' => 'error', 'strMensaje_t' => 'No se pudo identificar la cuenta'));
        }
    }else{
        echo json_encode(array('strEstado_t' => 'error', 'strMensaje_t' => 'Datos no validos'));
    }

}

if(isset($_GET['sendSms'])){
    $cuenta=isset($_POST['cuenta']) ? $_POST['cuenta'] :false;
    $para=isset($_POST['destinatario']) ? $_POST['destinatario'] :false;
    $cuerpo=isset($_POST['mensaje']) ? $_POST['mensaje'] :false;
    $agente=isset($_POST['agente']) ? $_POST['agente'] :  false;
    if(!$agente  || $agente ==0){
        $agente=isset($_SESSION['IDENTIFICACION']) ? $_SESSION['IDENTIFICACION'] :  false;
        if(!$agente){
            $agente=isset($_SESSION['HUESPED']) ? $_SESSION['HUESPED'] : '-10';
        }
    }
    if($cuenta && $para && $cuerpo){
        $sql=$mysqli->query("SELECT id_huesped FROM dy_sms.configuracion WHERE id={$cuenta}");
        if($sql && $sql->num_rows==1){
            $sql=$sql->fetch_object();
            $huesped=$sql->id_huesped;
            $data = array(
                "strUsuario_t" => "crm",
                "strToken_t" => "D43dasd321",
                "strTelefono_t"  => '57'.$para,
                "strMensaje_t"  =>  $cuerpo, 
                "intIdConfiguracion_t"  =>  $cuenta, 
                "intIdEstPas_t"  =>  null,
                "intConsinteMiembro_t"  =>  null,
                "intIdAgente_t"  =>  $agente,
                "intIdHuesped_t"  =>  $huesped
            );

            $data_string = json_encode($data);
            //echo $data_string;
            $ch = curl_init('http://localhost:8080/dyalogocore/api/bi/enviarSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );
            $respuesta = curl_exec($ch);
            echo $respuesta;
            $error = curl_error($ch);
            echo $error;
            curl_close($ch);
        }else{
            echo json_encode(array('strEstado_t' => 'error', 'strMensaje_t' => 'No se pudo identificar la cuenta'));
        }
    }else{
        echo json_encode(array('strEstado_t' => 'error', 'strMensaje_t' => 'Datos no validos'));
    }

}

if(isset($_GET['searchMail'])){
    $fecha_inicial=isset($_POST['fecha_inicial']) ? $_POST['fecha_inicial'] :false;
    $fecha_final=isset($_POST['fecha_final']) ? $_POST['fecha_final'] :false;
    $asunto=isset($_POST['asunto']) ? $_POST['asunto'] :false;
    $de=isset($_POST['de']) ? $_POST['de'] :false;
    $adjuntos=isset($_POST['adjuntos']) ? $_POST['adjuntos'] :false;
    $cuerpo=isset($_POST['cuerpo']) ? $_POST['cuerpo'] :false;
    $cuenta=isset($_POST['cuenta']) ? $_POST['cuenta'] :false;
    
    if($cuenta){
        
        $sql="select * from dyalogo_canales_electronicos.dy_ce_entrantes where id_ce_configuracion={$cuenta}";
        
        if($fecha_inicial && $fecha_final){
            $sql.=" and fecha_hora_recibido_servidor between '{$fecha_inicial}' and '{$fecha_final} 23:59:59'";
        }elseif($fecha_inicial){
            $sql.=" and fecha_hora_recibido_servidor >= '{$fecha_inicial}'";
        }elseif($fecha_final){
            $sql.=" and fecha_hora_recibido_servidor <= '{$fecha_final} 23:59:59'";
        }
        
        if($asunto){
            $sql.=" and asunto like '%{$asunto}%'";
        }
        
        if($de){
            $sql.=" and de='{$de}'";
        }
        
        if($cuerpo){
            $sql.=" and cuerpo like '%{$cuerpo}%'";
        }
        $sql.=" limit 20";
        $sql=$mysqli->query($sql);
        if($sql && $sql->num_rows>0){
            $html='';
            $fila=0;
            $adjunto=false;
            $strAdjuntos='';
            while($mail=$sql->fetch_object()){
                if($adjuntos){
                    $sqlAdjuntos=$mysqli->query("SELECT nombre,ruta_fisica FROM dyalogo_canales_electronicos.dy_ce_entrante_adjuntos WHERE id_ce_entrante={$mail->id}");
                    if($sqlAdjuntos && $sqlAdjuntos->num_rows>0){
                        $html.="<div class=\"panel box box-primary box-solid\" style=\"margin-bottom:10px;\"><div class=\"box-header with-border\" style=\"height:30px\"><h3 class=\"box-title\" style=\"width:100%\"><a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#fila_{$fila}\" style=\"display:flex;justify-content:space-between;font-size:14px\"><span>Fecha: {$mail->fecha_hora}</span><span>De: {$mail->de}</span><span>Asunto: ".urldecode($mail->asunto)."</span></a></h3></div><div id=\"fila_{$fila}\" class=\"panel-collapse collapse\"><div class=\"box-body\"><div class=\"row\"><div class=\"col-md-12 col-xs-12 form-group\"><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Fecha: </strong><span>{$mail->fecha_hora}</span></div><div><div class=\"btn btn-block btn-primary reenviar\" fila='{$fila}'>Reenviar</div></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Asunto: </strong><span>".urldecode($mail->asunto)."</span></div><div></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>De: </strong><span>{$mail->de}</span></div><div></div></div></div><div class=\"form-group\"><table class=\"table table-bordered\"><thead><tr><th>Cuerpo del mail recibido</th></tr></thead><tbody><tr><td>".urldecode($mail->cuerpo)."</td></tr></tbody></table></div>";                        
                        $html.="<div class=\"form-group\"><table class=\"table table-bordered\"><thead><tr><th>Adjuntos</th></tr></thead><tbody>";
                        while($adjunto = $sqlAdjuntos->fetch_object()){
                            $html.="<tr><td><a href='#' target='_blank' onclick=\"location.href='formularios/enviarSms_Mail_crud.php?descargarAdjunto=si&ruta={$adjunto->ruta_fisica}'\">{$adjunto->nombre}</a></td></tr>";
                            $strAdjuntos.="{$adjunto->ruta_fisica}";
                        }
                        $html.="</tbody></table></div></div></div>";
                        $html.="</div></div><input type='hidden' id='de_{$fila}' de={$mail->de}><input type='hidden' id='cc_{$fila}' cc={$mail->cc}><input type='hidden' id='cco_{$fila}' cco={$mail->cco}><input type='hidden' id='asunto_{$fila}' asunto='".urldecode($mail->asunto)."'><input type='hidden' id='adjuntos_{$fila}' adjuntos='".$strAdjuntos."'></div><section hidden><input type='hidden' id='cuerpo_{$fila}' value='".urldecode($mail->cuerpo)."'></section>";
                        $adjunto=true;
                    }
                }else{
                    $html.="<div class=\"panel box box-primary box-solid\" style=\"margin-bottom:10px;\"><div class=\"box-header with-border\" style=\"height:30px\"><h3 class=\"box-title\" style=\"width:100%\"><a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#fila_{$fila}\" style=\"display:flex;justify-content:space-between;font-size:14px\"><span>Fecha: {$mail->fecha_hora}</span><span>De: {$mail->de}</span><span>Asunto: ".urldecode($mail->asunto)."</span></a></h3></div><div id=\"fila_{$fila}\" class=\"panel-collapse collapse\"><div class=\"box-body\"><div class=\"row\"><div class=\"col-md-12 col-xs-12 form-group\"><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Fecha: </strong><span>{$mail->fecha_hora}</span></div><div><div class=\"btn btn-block btn-primary reenviar\" fila='{$fila}'>Reenviar</div></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Asunto: </strong><span>".urldecode($mail->asunto)."</span></div><div></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>De: </strong><span>{$mail->de}</span></div><div></div></div></div><div class=\"form-group\"><table class=\"table table-bordered\"><thead><tr><th>Cuerpo del mail recibido</th></tr></thead><tbody><tr><td>".urldecode($mail->cuerpo)."</td></tr></tbody></table></div></div></div></div></div><input type='hidden' id='de_{$fila}' de={$mail->de}><input type='hidden' id='cc_{$fila}' cc={$mail->cc}><input type='hidden' id='cco_{$fila}' cco={$mail->cco}><input type='hidden' id='asunto_{$fila}' asunto=".urldecode($mail->asunto)."><input type='hidden' id='cuerpo_{$fila}' value='".urldecode($mail->cuerpo)."'></div><section hidden><input type='hidden' id='cuerpo_{$fila}' value='".urldecode($mail->cuerpo)."'></section>";
                }
                $fila++;
            }
            if($adjuntos && !$adjunto){
                echo json_encode(array('estado' => 'error', 'mensaje' => 'No se encontraron correos'));
            }else{
                $html.="<script>$('.reenviar').on('click', function(e){
                    e.preventDefault();
                    var fila=$(this).attr('fila');
                    $('#modal_sendMail').modal('show');
                    $('#para').val($('#de_'+fila).attr('de'));
                    $('#asunto').val($('#asunto_'+fila).attr('asunto'));
                    $('#cuerpo').val($('#cuerpo_'+fila).val());
                    $('#cuerpo').html($('#cuerpo_'+fila).val());
                    $('#stradjuntos').val($('#adjuntos_'+fila).attr('adjuntos'));
                    $('#cuentaMail').val($('#cuentaSearchMail').val());            
                });</script>";                
                echo json_encode(array('estado' => 'ok', 'mensaje' => $html));
            }
        }else{
            echo json_encode(array('estado' => 'error', 'mensaje' => 'No se encontraron correos'));
        }
    }else{
        echo json_encode(array('estado' => 'error', 'mensaje' => 'No hay cuenta configurada'));
    }
}

if(isset($_GET['descargarAdjunto'])){
    $ruta=isset($_GET['ruta']) ? $_GET['ruta'] :false;
    
    if($ruta){
        if (is_file($ruta)){
            $size = strlen($ruta);

            if ($size>0) {
                $nombre=basename($ruta);
                $tamano = filesize($ruta);
                header("Content-Description: File Transfer");
                header("Content-type: application/force-download");
                header("Content-disposition: attachment; filename=".$nombre);
                header("Content-Transfer-Encoding: binary");
                header("Expires: 0");
                header("Cache-Control: must-revalidate");
                header("Pragma: public");
                header("Content-Length: " . $tamano);
                ob_clean();
                flush();
                readfile($ruta);
            }else{
                echo json_encode(array('estado' => 'error', 'mensaje' => 'Archivo dañado'));
            }
        }else{
            echo json_encode(array('estado' => 'error', 'mensaje' => 'Archivo no encontrado'));
        }
    }else{
        echo json_encode(array('estado' => 'error', 'mensaje' => 'Archivo sin ruta'));    
    }
}

if(isset($_GET['textDefaultSms'])){
    $campo= isset($_POST['campo']) ? $_POST['campo'] :false;
    $valor= isset($_POST['valor']) ? $_POST['valor'] :false;
    if($campo){
        $sql=$mysqli->query("SELECT PREGUN_Tipo______b FROM DYALOGOCRM_SISTEMA.PREGUN WHERE PREGUN_ConsInte__b={$campo}");
        if($sql){
            $sql=$sql->fetch_object();
            $tipo=$sql->PREGUN_Tipo______b;
            
            if($tipo == 6 && $valor!=0){
                $sqlOption=$mysqli->query("SELECT LISOPC_Nombre____b FROM DYALOGOCRM_SISTEMA.LISOPC WHERE LISOPC_ConsInte__b={$valor}");
                if($sqlOption){
                    $sqlOption=$sqlOption->fetch_object();
                    $valor=$sqlOption->LISOPC_Nombre____b;
                }else{
                    echo json_encode(array('estado'=>'error', 'mensaje' => 'No se pudo obtener el valor del mensaje predefinido'));
                    exit();
                }
            }
            
            echo json_encode(array('estado'=>'ok', 'mensaje'=> $valor));
        }else{
            echo json_encode(array('estado'=>'error', 'mensaje' => 'No se pudo obtener el tipo de campo del mensaje'));
        }
    }else{
        echo json_encode(array('estado'=>'error', 'mensaje' => 'No se pudo obtener el campo del mensaje'));
    }
}
?>