<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1411;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1411
                  SET G1411_C = -201
                  WHERE G1411_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1411 
                    WHERE G1411_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1411_C"]) || $gestion["G1411_C"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1411_C"];
        }

        if (is_null($gestion["G1411_C"]) || $gestion["G1411_C"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1411_C"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1411_FechaInsercion"]."',".$gestion["G1411_Usuario"].",'".$gestion["G1411_C".$P["P"]]."','".$gestion["G1411_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "customers.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>Añadir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1411 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1411_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1411_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1411_LinkContenido as url FROM ".$BaseDatos.".G1411 WHERE G1411_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1411_ConsInte__b, G1411_FechaInsercion , G1411_Usuario ,  G1411_CodigoMiembro  , G1411_PoblacionOrigen , G1411_EstadoDiligenciamiento ,  G1411_IdLlamada , G1411_C24749 as principal ,G1411_C24945,G1411_C24749,G1411_C24748,G1411_C24750,G1411_C24946,G1411_C24947,G1411_C24751,G1411_C25504,G1411_C24754,G1411_C24755,G1411_C24756,G1411_C24757,G1411_C24758,G1411_C24759,G1411_C24760,G1411_C24761,G1411_C24762,G1411_C24763,G1411_C24764 FROM '.$BaseDatos.'.G1411 WHERE G1411_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1411_C24945'] = $key->G1411_C24945;

                $datos[$i]['G1411_C24749'] = $key->G1411_C24749;

                $datos[$i]['G1411_C24748'] = $key->G1411_C24748;

                $datos[$i]['G1411_C24750'] = $key->G1411_C24750;

                $datos[$i]['G1411_C24946'] = $key->G1411_C24946;

                $datos[$i]['G1411_C24947'] = $key->G1411_C24947;

                $datos[$i]['G1411_C24751'] = $key->G1411_C24751;

                $datos[$i]['G1411_C25504'] = $key->G1411_C25504;

                $datos[$i]['G1411_C24754'] = $key->G1411_C24754;

                $datos[$i]['G1411_C24755'] = $key->G1411_C24755;

                $datos[$i]['G1411_C24756'] = explode(' ', $key->G1411_C24756)[0];
  
                $hora = '';
                if(!is_null($key->G1411_C24757)){
                    $hora = explode(' ', $key->G1411_C24757)[1];
                }

                $datos[$i]['G1411_C24757'] = $hora;

                $datos[$i]['G1411_C24758'] = $key->G1411_C24758;

                $datos[$i]['G1411_C24759'] = $key->G1411_C24759;

                $datos[$i]['G1411_C24760'] = $key->G1411_C24760;

                $datos[$i]['G1411_C24761'] = $key->G1411_C24761;

                $datos[$i]['G1411_C24762'] = $key->G1411_C24762;

                $datos[$i]['G1411_C24763'] = $key->G1411_C24763;

                $datos[$i]['G1411_C24764'] = $key->G1411_C24764;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1411";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = " AND G1411_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $regProp = "";
            }

            //JDBD valores filtros.
            $B = $_POST["B"];
            $A = $_POST["A"];
            $T = $_POST["T"];
            $F = $_POST["F"];
            $E = $_POST["E"];


            $Lsql = "SELECT G1411_ConsInte__b as id,  G1411_C24749 as camp1 , G1411_C24748 as camp2 
                     FROM ".$BaseDatos.".G1411  WHERE TRUE ".$regProp;

            if ($B != "" && $B != NULL) {
                $Lsql .= " AND (G1411_C24749 LIKE '%".$B."%' OR G1411_C24748 LIKE '%".$B."%') ";
            }
            if ($A != 0 && $A != -1 && $A != NULL) {
                $Lsql .= " AND G1411_Usuario = ".$A." ";
            }
            if ($T != 0 && $T != -1 && $T != NULL) {
                $Lsql .= " AND G1411_C24754 = ".$T." ";
            }
            if ($F != "" && $F != NULL) {
                $Lsql .= " AND G1411_FechaInsercion = '".$F."' ";
            }
            if ($E != 0 && $E != -1 && $E != NULL) {
                if ($E == -203) {
                    $Lsql .= " AND G1411_C = -203 OR G1411_C = '' OR G1411_C IS NULL ";
                }else{
                    $Lsql .= " AND G1411_C = ".$E." "; 
                }
            }


            $Lsql .= " ORDER BY G1411_ConsInte__b DESC LIMIT 0, 50 "; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1411");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1411_ConsInte__b, G1411_FechaInsercion , G1411_Usuario ,  G1411_CodigoMiembro  , G1411_PoblacionOrigen , G1411_EstadoDiligenciamiento ,  G1411_IdLlamada , G1411_C24749 as principal ,G1411_C24945,G1411_C24749,G1411_C24748,G1411_C24750,G1411_C24946,G1411_C24947, a.LISOPC_Nombre____b as G1411_C24751, b.LISOPC_Nombre____b as G1411_C25504, c.LISOPC_Nombre____b as G1411_C24754, d.LISOPC_Nombre____b as G1411_C24755,G1411_C24756,G1411_C24757,G1411_C24758,G1411_C24759,G1411_C24760,G1411_C24761,G1411_C24762,G1411_C24763,G1411_C24764 FROM '.$BaseDatos.'.G1411 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1411_C24751 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1411_C25504 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1411_C24754 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1411_C24755';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1411_C24757)){
                    $hora_a = explode(' ', $fila->G1411_C24757)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1411_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1411_ConsInte__b , ($fila->G1411_C24945) , ($fila->G1411_C24749) , ($fila->G1411_C24748) , ($fila->G1411_C24750) , ($fila->G1411_C24946) , ($fila->G1411_C24947) , ($fila->G1411_C24751) , ($fila->G1411_C25504) , ($fila->G1411_C24754) , ($fila->G1411_C24755) , explode(' ', $fila->G1411_C24756)[0] , $hora_a , ($fila->G1411_C24758) , ($fila->G1411_C24759) , ($fila->G1411_C24760) , ($fila->G1411_C24761) , ($fila->G1411_C24762) , ($fila->G1411_C24763) , ($fila->G1411_C24764) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1411 WHERE G1411_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    //echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1411";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = ' AND G1411_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $regProp = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $A = 0;
            $T = 0;
            $F = "";
            $B = "";
            $E = 0;

            if (isset($_POST["A"])) {
                $A = $_POST["A"];
            }
            if (isset($_POST["T"])) {
                $T = $_POST["T"];
            }
            if (isset($_POST["F"])) {
                $F = $_POST["F"];
            }
            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            if (isset($_POST["E"])) {
                $E = $_POST["E"];
            }

            $Zsql = 'SELECT  G1411_ConsInte__b as id,  G1411_C24749 as camp1 , G1411_C24748 as camp2  FROM '.$BaseDatos.'.G1411 WHERE TRUE'.$regProp; 

            if ($A != 0) {
                $Zsql .= ' AND G1411_Usuario = '.$A.' ';
            }

            if ($T != 0) {
                $Zsql .= ' AND G1411_C24754 = '.$T.' ';
            }

            if ($F != "") {
                $Zsql .= ' AND G1411_FechaInsercion = "'.$F.'" ';
            }

            if ($B != "") {
                $Zsql .= ' AND (G1411_C24749 LIKE "%'.$B.'%" OR G1411_C24748 LIKE "%'.$B.'%") ';
            }

            if ($E != 0) {
                if ($E == -203) {
                    $Zsql .= ' AND G1411_C = -203 OR G1411_C = "" OR G1411_C IS NULL ';
                }else{
                    $Zsql .= ' AND G1411_C = '.$E.'  ';
                }
            }

            $Zsql .= ' ORDER BY G1411_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1411 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1411(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1411_C24945"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24945 = '".$_POST["G1411_C24945"]."'";
                $LsqlI .= $separador."G1411_C24945";
                $LsqlV .= $separador."'".$_POST["G1411_C24945"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24749"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24749 = '".$_POST["G1411_C24749"]."'";
                $LsqlI .= $separador."G1411_C24749";
                $LsqlV .= $separador."'".$_POST["G1411_C24749"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24748"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24748 = '".$_POST["G1411_C24748"]."'";
                $LsqlI .= $separador."G1411_C24748";
                $LsqlV .= $separador."'".$_POST["G1411_C24748"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24750"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24750 = '".$_POST["G1411_C24750"]."'";
                $LsqlI .= $separador."G1411_C24750";
                $LsqlV .= $separador."'".$_POST["G1411_C24750"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24946"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24946 = '".$_POST["G1411_C24946"]."'";
                $LsqlI .= $separador."G1411_C24946";
                $LsqlV .= $separador."'".$_POST["G1411_C24946"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24947"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24947 = '".$_POST["G1411_C24947"]."'";
                $LsqlI .= $separador."G1411_C24947";
                $LsqlV .= $separador."'".$_POST["G1411_C24947"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24751"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24751 = '".$_POST["G1411_C24751"]."'";
                $LsqlI .= $separador."G1411_C24751";
                $LsqlV .= $separador."'".$_POST["G1411_C24751"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C25504"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C25504 = '".$_POST["G1411_C25504"]."'";
                $LsqlI .= $separador."G1411_C25504";
                $LsqlV .= $separador."'".$_POST["G1411_C25504"]."'";
                $validar = 1;
            }
             
 
            $G1411_C24754 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1411_C24754 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1411_C24754 = ".$G1411_C24754;
                    $LsqlI .= $separador." G1411_C24754";
                    $LsqlV .= $separador.$G1411_C24754;
                    $validar = 1;

                    
                }
            }
 
            $G1411_C24755 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1411_C24755 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1411_C24755 = ".$G1411_C24755;
                    $LsqlI .= $separador." G1411_C24755";
                    $LsqlV .= $separador.$G1411_C24755;
                    $validar = 1;
                }
            }
 
            $G1411_C24756 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1411_C24756 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1411_C24756 = ".$G1411_C24756;
                    $LsqlI .= $separador." G1411_C24756";
                    $LsqlV .= $separador.$G1411_C24756;
                    $validar = 1;
                }
            }
 
            $G1411_C24757 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1411_C24757 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1411_C24757 = ".$G1411_C24757;
                    $LsqlI .= $separador." G1411_C24757";
                    $LsqlV .= $separador.$G1411_C24757;
                    $validar = 1;
                }
            }
 
            $G1411_C24758 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1411_C24758 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1411_C24758 = ".$G1411_C24758;
                    $LsqlI .= $separador." G1411_C24758";
                    $LsqlV .= $separador.$G1411_C24758;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1411_C24759"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24759 = '".$_POST["G1411_C24759"]."'";
                $LsqlI .= $separador."G1411_C24759";
                $LsqlV .= $separador."'".$_POST["G1411_C24759"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24760"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24760 = '".$_POST["G1411_C24760"]."'";
                $LsqlI .= $separador."G1411_C24760";
                $LsqlV .= $separador."'".$_POST["G1411_C24760"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24761"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24761 = '".$_POST["G1411_C24761"]."'";
                $LsqlI .= $separador."G1411_C24761";
                $LsqlV .= $separador."'".$_POST["G1411_C24761"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24762"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24762 = '".$_POST["G1411_C24762"]."'";
                $LsqlI .= $separador."G1411_C24762";
                $LsqlV .= $separador."'".$_POST["G1411_C24762"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24763"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24763 = '".$_POST["G1411_C24763"]."'";
                $LsqlI .= $separador."G1411_C24763";
                $LsqlV .= $separador."'".$_POST["G1411_C24763"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1411_C24764"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_C24764 = '".$_POST["G1411_C24764"]."'";
                $LsqlI .= $separador."G1411_C24764";
                $LsqlV .= $separador."'".$_POST["G1411_C24764"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1411_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1411_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1411_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1411_Usuario , G1411_FechaInsercion, G1411_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1411_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1411 WHERE G1411_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        //echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1446_ConsInte__b, G1446_C25931, G1446_C25389, G1446_C25390, G1446_C25982, G1446_C25383, f.LISOPC_Nombre____b as  G1446_C25384, G1446_C25932, h.LISOPC_Nombre____b as  G1446_C25385, i.LISOPC_Nombre____b as  G1446_C25386, j.LISOPC_Nombre____b as  G1446_C25387, G1446_C25388, l.LISOPC_Nombre____b as  G1446_C25933, G1446_C25934, G1446_C25380, G1446_C25381, p.LISOPC_Nombre____b as  G1446_C25382 FROM ".$BaseDatos.".G1446  LEFT JOIN ".$BaseDatos_systema.".LISOPC as f ON f.LISOPC_ConsInte__b =  G1446_C25384 LEFT JOIN ".$BaseDatos_systema.".LISOPC as h ON h.LISOPC_ConsInte__b =  G1446_C25385 LEFT JOIN ".$BaseDatos_systema.".LISOPC as i ON i.LISOPC_ConsInte__b =  G1446_C25386 LEFT JOIN ".$BaseDatos_systema.".LISOPC as j ON j.LISOPC_ConsInte__b =  G1446_C25387 LEFT JOIN ".$BaseDatos_systema.".LISOPC as l ON l.LISOPC_ConsInte__b =  G1446_C25933 LEFT JOIN ".$BaseDatos_systema.".LISOPC as p ON p.LISOPC_ConsInte__b =  G1446_C25382 ";

        $SQL .= " WHERE G1446_C25389 = '".$numero."'"; 

        $SQL .= " ORDER BY G1446_C25931";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1446_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1446_ConsInte__b)."</cell>"; 
            

            echo "<cell>". $fila->G1446_C25931."</cell>"; 

            echo "<cell>". ($fila->G1446_C25389)."</cell>";

            echo "<cell>". ($fila->G1446_C25390)."</cell>";

            if($fila->G1446_C25982 != ''){
                echo "<cell>". explode(' ', $fila->G1446_C25982)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1446_C25383 != ''){
                echo "<cell>". explode(' ', $fila->G1446_C25383)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1446_C25384)."</cell>";

            echo "<cell>". ($fila->G1446_C25932)."</cell>";

            echo "<cell>". ($fila->G1446_C25385)."</cell>";

            echo "<cell>". ($fila->G1446_C25386)."</cell>";

            echo "<cell>". ($fila->G1446_C25387)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1446_C25388)."]]></cell>";

            echo "<cell>". ($fila->G1446_C25933)."</cell>";

            echo "<cell>". ($fila->G1446_C25934)."</cell>";

            echo "<cell>". ($fila->G1446_C25380)."</cell>";

            echo "<cell>". ($fila->G1446_C25381)."</cell>";

            echo "<cell>". ($fila->G1446_C25382)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1446 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1446(";
            $LsqlV = " VALUES ("; 
 
            $G1446_C25931= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1446_C25931"])){    
                if($_POST["G1446_C25931"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1446_C25931 = $_POST["G1446_C25931"];
                    $LsqlU .= $separador." G1446_C25931 = '".$G1446_C25931."'";
                    $LsqlI .= $separador." G1446_C25931";
                    $LsqlV .= $separador."'".$G1446_C25931."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1446_C25390"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25390 = '".$_POST["G1446_C25390"]."'";
                $LsqlI .= $separador."G1446_C25390";
                $LsqlV .= $separador."'".$_POST["G1446_C25390"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1446_C25982 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1446_C25982"])){    
                if($_POST["G1446_C25982"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1446_C25982 = "'".str_replace(' ', '',$_POST["G1446_C25982"])." 00:00:00'";
                    $LsqlU .= $separador." G1446_C25982 = ".$G1446_C25982;
                    $LsqlI .= $separador." G1446_C25982";
                    $LsqlV .= $separador.$G1446_C25982;
                    $validar = 1;
                }
            }

            $G1446_C25383 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1446_C25383"])){    
                if($_POST["G1446_C25383"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1446_C25383 = "'".str_replace(' ', '',$_POST["G1446_C25383"])." 00:00:00'";
                    $LsqlU .= $separador." G1446_C25383 = ".$G1446_C25383;
                    $LsqlI .= $separador." G1446_C25383";
                    $LsqlV .= $separador.$G1446_C25383;
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1446_C25384"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25384 = '".$_POST["G1446_C25384"]."'";
                $LsqlI .= $separador."G1446_C25384";
                $LsqlV .= $separador."'".$_POST["G1446_C25384"]."'";
                $validar = 1;
            }
 
                                                                         
            if(isset($_POST["G1446_C25932"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25932 = '".$_POST["G1446_C25932"]."'";
                $LsqlI .= $separador."G1446_C25932";
                $LsqlV .= $separador."'".$_POST["G1446_C25932"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1446_C25385"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25385 = '".$_POST["G1446_C25385"]."'";
                $LsqlI .= $separador."G1446_C25385";
                $LsqlV .= $separador."'".$_POST["G1446_C25385"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1446_C25386"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25386 = '".$_POST["G1446_C25386"]."'";
                $LsqlI .= $separador."G1446_C25386";
                $LsqlV .= $separador."'".$_POST["G1446_C25386"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1446_C25387"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25387 = '".$_POST["G1446_C25387"]."'";
                $LsqlI .= $separador."G1446_C25387";
                $LsqlV .= $separador."'".$_POST["G1446_C25387"]."'";
                $validar = 1;
            }
  

            if(isset($_POST["G1446_C25388"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25388 = '".$_POST["G1446_C25388"]."'";
                $LsqlI .= $separador."G1446_C25388";
                $LsqlV .= $separador."'".$_POST["G1446_C25388"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1446_C25933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25933 = '".$_POST["G1446_C25933"]."'";
                $LsqlI .= $separador."G1446_C25933";
                $LsqlV .= $separador."'".$_POST["G1446_C25933"]."'";
                $validar = 1;
            }
 
                                                                         
            if(isset($_POST["G1446_C25934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25934 = '".$_POST["G1446_C25934"]."'";
                $LsqlI .= $separador."G1446_C25934";
                $LsqlV .= $separador."'".$_POST["G1446_C25934"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1446_C25380"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25380 = '".$_POST["G1446_C25380"]."'";
                $LsqlI .= $separador."G1446_C25380";
                $LsqlV .= $separador."'".$_POST["G1446_C25380"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1446_C25381"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25381 = '".$_POST["G1446_C25381"]."'";
                $LsqlI .= $separador."G1446_C25381";
                $LsqlV .= $separador."'".$_POST["G1446_C25381"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1446_C25382"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25382 = '".$_POST["G1446_C25382"]."'";
                $LsqlI .= $separador."G1446_C25382";
                $LsqlV .= $separador."'".$_POST["G1446_C25382"]."'";
                $validar = 1;
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1446_C25389 = $numero;
                    $LsqlU .= ", G1446_C25389 = ".$G1446_C25389."";
                    $LsqlI .= ", G1446_C25389";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1446_Usuario ,  G1446_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1446_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1446 WHERE  G1446_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
