<?php 
    /*
        Document   : index
        Created on : 2021-02-20 09:13:30
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = Mjc0NQ==  
    */
    $url_crud =  "formularios/G2745/G2745_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G2745/G2745_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2745_C53217" id="LblG2745_C53217">NOMBRE</label>
								<input type="text" class="form-control input-sm" id="G2745_C53217" value=""  name="G2745_C53217"  placeholder="NOMBRE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2745_C53218" id="LblG2745_C53218">CEDULA</label>
								<input type="text" class="form-control input-sm" id="G2745_C53218" value=""  name="G2745_C53218"  placeholder="CEDULA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2745_C53219" id="LblG2745_C53219">TELEFONO PERSONAL</label>
								<input type="text" class="form-control input-sm" id="G2745_C53219" value=""  name="G2745_C53219"  placeholder="TELEFONO PERSONAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2745_C53220" id="LblG2745_C53220">CIUDAD</label>
								<input type="text" class="form-control input-sm" id="G2745_C53220" value=""  name="G2745_C53220"  placeholder="CIUDAD">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Buenos días|tardes|noches, podría comunicarme con el señor(a) |NombreCliente|</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Mi nombre es |Agente|, le estoy llamando de |Empresa| con el fin de ...</h3>
                            <!-- FIN LIBRETO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61248" id="LblG2745_C61248">Nombre de la empresa que realizo la venta</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61248" id="G2745_C61248">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3699 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61249" id="LblG2745_C61249">Nombre de la empresa para la que se realiza la venta</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61249" id="G2745_C61249">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3699 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2745_C61250" id="LblG2745_C61250">Fecha de realización de llamada</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2745_C61250" id="G2745_C61250" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61252" id="LblG2745_C61252">Tipo de campaña</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61252" id="G2745_C61252">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3697 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62562" id="LblG2745_C62562">1. Sondeo venta telefónica</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62562" id="G2745_C62562" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62563" id="LblG2745_C62563">2. Cobro chec no compromete la propiedad</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62563" id="G2745_C62563" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62564" id="LblG2745_C62564">3. Llamada grabada inicio de venta</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62564" id="G2745_C62564" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62565" id="LblG2745_C62565">4. Solicitud de datos</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62565" id="G2745_C62565" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62566" id="LblG2745_C62566">5. Servicios básicos</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62566" id="G2745_C62566" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62567" id="LblG2745_C62567">6. Servicios complementarios</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62567" id="G2745_C62567" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62568" id="LblG2745_C62568">7. Información plan de Colombia (PEI)</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62568" id="G2745_C62568" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62569" id="LblG2745_C62569">8. Cláusulas de la 1 a la 12</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62569" id="G2745_C62569" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62570" id="LblG2745_C62570">9. Cláusulas 13 y 14 (75 años)</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62570" id="G2745_C62570" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62571" id="LblG2745_C62571">10. Cláusulas de la 15 a la 20</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62571" id="G2745_C62571" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62572" id="LblG2745_C62572">11. Numerales anexo post venta</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62572" id="G2745_C62572" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62573" id="LblG2745_C62573">12. Llamada grabada resumen de venta</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62573" id="G2745_C62573" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C61282" id="LblG2745_C61282">13. Habeas data</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C61282" id="G2745_C61282" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62574" id="LblG2745_C62574">14. Autorización chec (CHEC)</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62574" id="G2745_C62574" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62575" id="LblG2745_C62575">15. Libranza telefónica (EMPRESARIAL)</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62575" id="G2745_C62575" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62576" id="LblG2745_C62576">16. Incremento anual</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62576" id="G2745_C62576" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62577" id="LblG2745_C62577">17. Resumen de los inscritos</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62577" id="G2745_C62577" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62578" id="LblG2745_C62578">18. Fechas de protección</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62578" id="G2745_C62578" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62579" id="LblG2745_C62579">19. Resumen características del programa</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62579" id="G2745_C62579" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62580" id="LblG2745_C62580">20. Líneas de atención al cliente y referidos</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62580" id="G2745_C62580" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62581" id="LblG2745_C62581">21. Promociones actuales</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62581" id="G2745_C62581" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2745_C62582" id="LblG2745_C62582">22. Solicitud de servicio con el certificado de defunción</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2745_C62582" id="G2745_C62582" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2745/G2745_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2745_C53208").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2745_C61250").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G2745_C53209").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para Nombre de la empresa que realizo la venta 

    $("#G2745_C61248").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Nombre de la empresa para la que se realiza la venta 

    $("#G2745_C61249").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo de campaña 

    $("#G2745_C61252").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
