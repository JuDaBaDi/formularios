<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2745;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2745
                  SET G2745_C61647 = -201
                  WHERE G2745_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2745 
                    WHERE G2745_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2745_C61646"]) || $gestion["G2745_C61646"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2745_C61646"];
        }

        if (is_null($gestion["G2745_C61648"]) || $gestion["G2745_C61648"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2745_C61648"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2745_FechaInsercion"]."',".$gestion["G2745_Usuario"].",'".$gestion["G2745_C".$P["P"]]."','".$gestion["G2745_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "customers.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2745 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2745_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2745_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2745_LinkContenido as url FROM ".$BaseDatos.".G2745 WHERE G2745_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2745_LinkContenido as url FROM ".$BaseDatos.".G2745 WHERE G2745_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2745_ConsInte__b, G2745_FechaInsercion , G2745_Usuario ,  G2745_CodigoMiembro  , G2745_PoblacionOrigen , G2745_EstadoDiligenciamiento ,  G2745_IdLlamada , G2745_C53218 as principal ,G2745_C53217,G2745_C53218,G2745_C53219,G2745_C53220,G2745_C53206,G2745_C53207,G2745_C53208,G2745_C53209,G2745_C53210,G2745_C53211,G2745_C53212,G2745_C53213,G2745_C53214,G2745_C61248,G2745_C61249,G2745_C61250,G2745_C61252,G2745_C62562,G2745_C62563,G2745_C62564,G2745_C62565,G2745_C62566,G2745_C62567,G2745_C62568,G2745_C62569,G2745_C62570,G2745_C62571,G2745_C62572,G2745_C62573,G2745_C61282,G2745_C62574,G2745_C62575,G2745_C62576,G2745_C62577,G2745_C62578,G2745_C62579,G2745_C62580,G2745_C62581,G2745_C62582,G2745_C62222,G2745_C62212,G2745_C62223,G2745_C61647,G2745_C61646,G2745_C61648,G2745_C61649,G2745_C62210,G2745_C62211,G2745_C63243,G2745_C63244,G2745_C63246,G2745_C63247 FROM '.$BaseDatos.'.G2745 WHERE G2745_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2745_C53217'] = $key->G2745_C53217;

                $datos[$i]['G2745_C53218'] = $key->G2745_C53218;

                $datos[$i]['G2745_C53219'] = $key->G2745_C53219;

                $datos[$i]['G2745_C53220'] = $key->G2745_C53220;

                $datos[$i]['G2745_C53206'] = $key->G2745_C53206;

                $datos[$i]['G2745_C53207'] = $key->G2745_C53207;

                $datos[$i]['G2745_C53208'] = explode(' ', $key->G2745_C53208)[0];
  
                $hora = '';
                if(!is_null($key->G2745_C53209)){
                    $hora = explode(' ', $key->G2745_C53209)[1];
                }

                $datos[$i]['G2745_C53209'] = $hora;

                $datos[$i]['G2745_C53210'] = $key->G2745_C53210;

                $datos[$i]['G2745_C53211'] = $key->G2745_C53211;

                $datos[$i]['G2745_C53212'] = $key->G2745_C53212;

                $datos[$i]['G2745_C53213'] = $key->G2745_C53213;

                $datos[$i]['G2745_C53214'] = $key->G2745_C53214;

                $datos[$i]['G2745_C61248'] = $key->G2745_C61248;

                $datos[$i]['G2745_C61249'] = $key->G2745_C61249;

                $datos[$i]['G2745_C61250'] = explode(' ', $key->G2745_C61250)[0];

                $datos[$i]['G2745_C61252'] = $key->G2745_C61252;

                $datos[$i]['G2745_C62562'] = $key->G2745_C62562;

                $datos[$i]['G2745_C62563'] = $key->G2745_C62563;

                $datos[$i]['G2745_C62564'] = $key->G2745_C62564;

                $datos[$i]['G2745_C62565'] = $key->G2745_C62565;

                $datos[$i]['G2745_C62566'] = $key->G2745_C62566;

                $datos[$i]['G2745_C62567'] = $key->G2745_C62567;

                $datos[$i]['G2745_C62568'] = $key->G2745_C62568;

                $datos[$i]['G2745_C62569'] = $key->G2745_C62569;

                $datos[$i]['G2745_C62570'] = $key->G2745_C62570;

                $datos[$i]['G2745_C62571'] = $key->G2745_C62571;

                $datos[$i]['G2745_C62572'] = $key->G2745_C62572;

                $datos[$i]['G2745_C62573'] = $key->G2745_C62573;

                $datos[$i]['G2745_C61282'] = $key->G2745_C61282;

                $datos[$i]['G2745_C62574'] = $key->G2745_C62574;

                $datos[$i]['G2745_C62575'] = $key->G2745_C62575;

                $datos[$i]['G2745_C62576'] = $key->G2745_C62576;

                $datos[$i]['G2745_C62577'] = $key->G2745_C62577;

                $datos[$i]['G2745_C62578'] = $key->G2745_C62578;

                $datos[$i]['G2745_C62579'] = $key->G2745_C62579;

                $datos[$i]['G2745_C62580'] = $key->G2745_C62580;

                $datos[$i]['G2745_C62581'] = $key->G2745_C62581;

                $datos[$i]['G2745_C62582'] = $key->G2745_C62582;

                $datos[$i]['G2745_C62222'] = $key->G2745_C62222;

                $datos[$i]['G2745_C62212'] = $key->G2745_C62212;

                $datos[$i]['G2745_C62223'] = $key->G2745_C62223;

                $datos[$i]['G2745_C61647'] = $key->G2745_C61647;

                $datos[$i]['G2745_C61646'] = $key->G2745_C61646;

                $datos[$i]['G2745_C61648'] = $key->G2745_C61648;

                $datos[$i]['G2745_C61649'] = $key->G2745_C61649;

                $datos[$i]['G2745_C62210'] = explode(' ', $key->G2745_C62210)[0];

                $datos[$i]['G2745_C62211'] = $key->G2745_C62211;

                $datos[$i]['G2745_C63243'] = explode(' ', $key->G2745_C63243)[0];

                $datos[$i]['G2745_C63244'] = $key->G2745_C63244;

                $datos[$i]['G2745_C63246'] = $key->G2745_C63246;

                $datos[$i]['G2745_C63247'] = $key->G2745_C63247;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2745";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2745_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2745_ConsInte__b as id,  G2745_C53218 as camp1 , G2745_C53218 as camp2 
                     FROM ".$BaseDatos.".G2745  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2745_ConsInte__b as id,  G2745_C53218 as camp1 , G2745_C53218 as camp2  
                    FROM ".$BaseDatos.".G2745  JOIN ".$BaseDatos.".G2745_M".$_POST['muestra']." ON G2745_ConsInte__b = G2745_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2745_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2745_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2745_C53218 LIKE '%".$B."%' OR G2745_C53218 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2745_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2745");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2745_ConsInte__b, G2745_FechaInsercion , G2745_Usuario ,  G2745_CodigoMiembro  , G2745_PoblacionOrigen , G2745_EstadoDiligenciamiento ,  G2745_IdLlamada , G2745_C53218 as principal ,G2745_C53217,G2745_C53218,G2745_C53219,G2745_C53220, a.LISOPC_Nombre____b as G2745_C53206, b.LISOPC_Nombre____b as G2745_C53207,G2745_C53208,G2745_C53209,G2745_C53210,G2745_C53211,G2745_C53212,G2745_C53213,G2745_C53214, c.LISOPC_Nombre____b as G2745_C61248, d.LISOPC_Nombre____b as G2745_C61249,G2745_C61250, e.LISOPC_Nombre____b as G2745_C61252,G2745_C62562,G2745_C62563,G2745_C62564,G2745_C62565,G2745_C62566,G2745_C62567,G2745_C62568,G2745_C62569,G2745_C62570,G2745_C62571,G2745_C62572,G2745_C62573,G2745_C61282,G2745_C62574,G2745_C62575,G2745_C62576,G2745_C62577,G2745_C62578,G2745_C62579,G2745_C62580,G2745_C62581,G2745_C62582, f.LISOPC_Nombre____b as G2745_C62222,G2745_C62212,G2745_C62223, g.LISOPC_Nombre____b as G2745_C61647,G2745_C61646,G2745_C61648,G2745_C61649,G2745_C62210,G2745_C62211,G2745_C63243,G2745_C63244,G2745_C63246, h.LISOPC_Nombre____b as G2745_C63247 FROM '.$BaseDatos.'.G2745 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2745_C53206 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2745_C53207 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2745_C61248 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2745_C61249 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2745_C61252 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2745_C62222 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2745_C61647 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2745_C63247';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2745_C53209)){
                    $hora_a = explode(' ', $fila->G2745_C53209)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2745_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2745_ConsInte__b , ($fila->G2745_C53217) , ($fila->G2745_C53218) , ($fila->G2745_C53219) , ($fila->G2745_C53220) , ($fila->G2745_C53206) , ($fila->G2745_C53207) , explode(' ', $fila->G2745_C53208)[0] , $hora_a , ($fila->G2745_C53210) , ($fila->G2745_C53211) , ($fila->G2745_C53212) , ($fila->G2745_C53213) , ($fila->G2745_C53214) , ($fila->G2745_C61248) , ($fila->G2745_C61249) , explode(' ', $fila->G2745_C61250)[0] , ($fila->G2745_C61252) , ($fila->G2745_C62562) , ($fila->G2745_C62563) , ($fila->G2745_C62564) , ($fila->G2745_C62565) , ($fila->G2745_C62566) , ($fila->G2745_C62567) , ($fila->G2745_C62568) , ($fila->G2745_C62569) , ($fila->G2745_C62570) , ($fila->G2745_C62571) , ($fila->G2745_C62572) , ($fila->G2745_C62573) , ($fila->G2745_C61282) , ($fila->G2745_C62574) , ($fila->G2745_C62575) , ($fila->G2745_C62576) , ($fila->G2745_C62577) , ($fila->G2745_C62578) , ($fila->G2745_C62579) , ($fila->G2745_C62580) , ($fila->G2745_C62581) , ($fila->G2745_C62582) , ($fila->G2745_C62222) , ($fila->G2745_C62212) , ($fila->G2745_C62223) , ($fila->G2745_C61647) , ($fila->G2745_C61646) , ($fila->G2745_C61648) , ($fila->G2745_C61649) , explode(' ', $fila->G2745_C62210)[0] , ($fila->G2745_C62211) , explode(' ', $fila->G2745_C63243)[0] , ($fila->G2745_C63244) , ($fila->G2745_C63246) , ($fila->G2745_C63247) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2745 WHERE G2745_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2745";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2745_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2745_ConsInte__b as id,  G2745_C53218 as camp1 , G2745_C53218 as camp2  FROM '.$BaseDatos.'.G2745 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2745_ConsInte__b as id,  G2745_C53218 as camp1 , G2745_C53218 as camp2  
                    FROM ".$BaseDatos.".G2745  JOIN ".$BaseDatos.".G2745_M".$_POST['muestra']." ON G2745_ConsInte__b = G2745_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2745_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2745_C53218 LIKE "%'.$B.'%" OR G2745_C53218 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2745_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2745 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2745(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2745_C53217"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53217 = '".$_POST["G2745_C53217"]."'";
                $LsqlI .= $separador."G2745_C53217";
                $LsqlV .= $separador."'".$_POST["G2745_C53217"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53218"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53218 = '".$_POST["G2745_C53218"]."'";
                $LsqlI .= $separador."G2745_C53218";
                $LsqlV .= $separador."'".$_POST["G2745_C53218"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53219"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53219 = '".$_POST["G2745_C53219"]."'";
                $LsqlI .= $separador."G2745_C53219";
                $LsqlV .= $separador."'".$_POST["G2745_C53219"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53220"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53220 = '".$_POST["G2745_C53220"]."'";
                $LsqlI .= $separador."G2745_C53220";
                $LsqlV .= $separador."'".$_POST["G2745_C53220"]."'";
                $validar = 1;
            }
             
 
            $G2745_C53206 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2745_C53206 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2745_C53206 = ".$G2745_C53206;
                    $LsqlI .= $separador." G2745_C53206";
                    $LsqlV .= $separador.$G2745_C53206;
                    $validar = 1;

                    
                }
            }
 
            $G2745_C53207 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2745_C53207 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2745_C53207 = ".$G2745_C53207;
                    $LsqlI .= $separador." G2745_C53207";
                    $LsqlV .= $separador.$G2745_C53207;
                    $validar = 1;
                }
            }
 
            $G2745_C53208 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2745_C53208 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2745_C53208 = ".$G2745_C53208;
                    $LsqlI .= $separador." G2745_C53208";
                    $LsqlV .= $separador.$G2745_C53208;
                    $validar = 1;
                }
            }
 
            $G2745_C53209 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2745_C53209 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2745_C53209 = ".$G2745_C53209;
                    $LsqlI .= $separador." G2745_C53209";
                    $LsqlV .= $separador.$G2745_C53209;
                    $validar = 1;
                }
            }
 
            $G2745_C53210 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2745_C53210 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2745_C53210 = ".$G2745_C53210;
                    $LsqlI .= $separador." G2745_C53210";
                    $LsqlV .= $separador.$G2745_C53210;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2745_C53211"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53211 = '".$_POST["G2745_C53211"]."'";
                $LsqlI .= $separador."G2745_C53211";
                $LsqlV .= $separador."'".$_POST["G2745_C53211"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53212"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53212 = '".$_POST["G2745_C53212"]."'";
                $LsqlI .= $separador."G2745_C53212";
                $LsqlV .= $separador."'".$_POST["G2745_C53212"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53213"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53213 = '".$_POST["G2745_C53213"]."'";
                $LsqlI .= $separador."G2745_C53213";
                $LsqlV .= $separador."'".$_POST["G2745_C53213"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53214"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53214 = '".$_POST["G2745_C53214"]."'";
                $LsqlI .= $separador."G2745_C53214";
                $LsqlV .= $separador."'".$_POST["G2745_C53214"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53215"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53215 = '".$_POST["G2745_C53215"]."'";
                $LsqlI .= $separador."G2745_C53215";
                $LsqlV .= $separador."'".$_POST["G2745_C53215"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C53216"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C53216 = '".$_POST["G2745_C53216"]."'";
                $LsqlI .= $separador."G2745_C53216";
                $LsqlV .= $separador."'".$_POST["G2745_C53216"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C61248"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C61248 = '".$_POST["G2745_C61248"]."'";
                $LsqlI .= $separador."G2745_C61248";
                $LsqlV .= $separador."'".$_POST["G2745_C61248"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C61249"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C61249 = '".$_POST["G2745_C61249"]."'";
                $LsqlI .= $separador."G2745_C61249";
                $LsqlV .= $separador."'".$_POST["G2745_C61249"]."'";
                $validar = 1;
            }
             
 
            $G2745_C61250 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2745_C61250"])){    
                if($_POST["G2745_C61250"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2745_C61250"]);
                    if(count($tieneHora) > 1){
                        $G2745_C61250 = "'".$_POST["G2745_C61250"]."'";
                    }else{
                        $G2745_C61250 = "'".str_replace(' ', '',$_POST["G2745_C61250"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2745_C61250 = ".$G2745_C61250;
                    $LsqlI .= $separador." G2745_C61250";
                    $LsqlV .= $separador.$G2745_C61250;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2745_C61252"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C61252 = '".$_POST["G2745_C61252"]."'";
                $LsqlI .= $separador."G2745_C61252";
                $LsqlV .= $separador."'".$_POST["G2745_C61252"]."'";
                $validar = 1;
            }
             
  
            $G2745_C62562 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62562"])){
                if($_POST["G2745_C62562"] == 'Yes'){
                    $G2745_C62562 = 1;
                }else if($_POST["G2745_C62562"] == 'off'){
                    $G2745_C62562 = 0;
                }else if($_POST["G2745_C62562"] == 'on'){
                    $G2745_C62562 = 1;
                }else if($_POST["G2745_C62562"] == 'No'){
                    $G2745_C62562 = 1;
                }else{
                    $G2745_C62562 = $_POST["G2745_C62562"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62562 = ".$G2745_C62562."";
                $LsqlI .= $separador." G2745_C62562";
                $LsqlV .= $separador.$G2745_C62562;

                $validar = 1;
            }
  
            $G2745_C62563 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62563"])){
                if($_POST["G2745_C62563"] == 'Yes'){
                    $G2745_C62563 = 1;
                }else if($_POST["G2745_C62563"] == 'off'){
                    $G2745_C62563 = 0;
                }else if($_POST["G2745_C62563"] == 'on'){
                    $G2745_C62563 = 1;
                }else if($_POST["G2745_C62563"] == 'No'){
                    $G2745_C62563 = 1;
                }else{
                    $G2745_C62563 = $_POST["G2745_C62563"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62563 = ".$G2745_C62563."";
                $LsqlI .= $separador." G2745_C62563";
                $LsqlV .= $separador.$G2745_C62563;

                $validar = 1;
            }
  
            $G2745_C62564 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62564"])){
                if($_POST["G2745_C62564"] == 'Yes'){
                    $G2745_C62564 = 1;
                }else if($_POST["G2745_C62564"] == 'off'){
                    $G2745_C62564 = 0;
                }else if($_POST["G2745_C62564"] == 'on'){
                    $G2745_C62564 = 1;
                }else if($_POST["G2745_C62564"] == 'No'){
                    $G2745_C62564 = 1;
                }else{
                    $G2745_C62564 = $_POST["G2745_C62564"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62564 = ".$G2745_C62564."";
                $LsqlI .= $separador." G2745_C62564";
                $LsqlV .= $separador.$G2745_C62564;

                $validar = 1;
            }
  
            $G2745_C62565 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62565"])){
                if($_POST["G2745_C62565"] == 'Yes'){
                    $G2745_C62565 = 1;
                }else if($_POST["G2745_C62565"] == 'off'){
                    $G2745_C62565 = 0;
                }else if($_POST["G2745_C62565"] == 'on'){
                    $G2745_C62565 = 1;
                }else if($_POST["G2745_C62565"] == 'No'){
                    $G2745_C62565 = 1;
                }else{
                    $G2745_C62565 = $_POST["G2745_C62565"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62565 = ".$G2745_C62565."";
                $LsqlI .= $separador." G2745_C62565";
                $LsqlV .= $separador.$G2745_C62565;

                $validar = 1;
            }
  
            $G2745_C62566 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62566"])){
                if($_POST["G2745_C62566"] == 'Yes'){
                    $G2745_C62566 = 1;
                }else if($_POST["G2745_C62566"] == 'off'){
                    $G2745_C62566 = 0;
                }else if($_POST["G2745_C62566"] == 'on'){
                    $G2745_C62566 = 1;
                }else if($_POST["G2745_C62566"] == 'No'){
                    $G2745_C62566 = 1;
                }else{
                    $G2745_C62566 = $_POST["G2745_C62566"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62566 = ".$G2745_C62566."";
                $LsqlI .= $separador." G2745_C62566";
                $LsqlV .= $separador.$G2745_C62566;

                $validar = 1;
            }
  
            $G2745_C62567 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62567"])){
                if($_POST["G2745_C62567"] == 'Yes'){
                    $G2745_C62567 = 1;
                }else if($_POST["G2745_C62567"] == 'off'){
                    $G2745_C62567 = 0;
                }else if($_POST["G2745_C62567"] == 'on'){
                    $G2745_C62567 = 1;
                }else if($_POST["G2745_C62567"] == 'No'){
                    $G2745_C62567 = 1;
                }else{
                    $G2745_C62567 = $_POST["G2745_C62567"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62567 = ".$G2745_C62567."";
                $LsqlI .= $separador." G2745_C62567";
                $LsqlV .= $separador.$G2745_C62567;

                $validar = 1;
            }
  
            $G2745_C62568 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62568"])){
                if($_POST["G2745_C62568"] == 'Yes'){
                    $G2745_C62568 = 1;
                }else if($_POST["G2745_C62568"] == 'off'){
                    $G2745_C62568 = 0;
                }else if($_POST["G2745_C62568"] == 'on'){
                    $G2745_C62568 = 1;
                }else if($_POST["G2745_C62568"] == 'No'){
                    $G2745_C62568 = 1;
                }else{
                    $G2745_C62568 = $_POST["G2745_C62568"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62568 = ".$G2745_C62568."";
                $LsqlI .= $separador." G2745_C62568";
                $LsqlV .= $separador.$G2745_C62568;

                $validar = 1;
            }
  
            $G2745_C62569 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62569"])){
                if($_POST["G2745_C62569"] == 'Yes'){
                    $G2745_C62569 = 1;
                }else if($_POST["G2745_C62569"] == 'off'){
                    $G2745_C62569 = 0;
                }else if($_POST["G2745_C62569"] == 'on'){
                    $G2745_C62569 = 1;
                }else if($_POST["G2745_C62569"] == 'No'){
                    $G2745_C62569 = 1;
                }else{
                    $G2745_C62569 = $_POST["G2745_C62569"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62569 = ".$G2745_C62569."";
                $LsqlI .= $separador." G2745_C62569";
                $LsqlV .= $separador.$G2745_C62569;

                $validar = 1;
            }
  
            $G2745_C62570 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62570"])){
                if($_POST["G2745_C62570"] == 'Yes'){
                    $G2745_C62570 = 1;
                }else if($_POST["G2745_C62570"] == 'off'){
                    $G2745_C62570 = 0;
                }else if($_POST["G2745_C62570"] == 'on'){
                    $G2745_C62570 = 1;
                }else if($_POST["G2745_C62570"] == 'No'){
                    $G2745_C62570 = 1;
                }else{
                    $G2745_C62570 = $_POST["G2745_C62570"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62570 = ".$G2745_C62570."";
                $LsqlI .= $separador." G2745_C62570";
                $LsqlV .= $separador.$G2745_C62570;

                $validar = 1;
            }
  
            $G2745_C62571 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62571"])){
                if($_POST["G2745_C62571"] == 'Yes'){
                    $G2745_C62571 = 1;
                }else if($_POST["G2745_C62571"] == 'off'){
                    $G2745_C62571 = 0;
                }else if($_POST["G2745_C62571"] == 'on'){
                    $G2745_C62571 = 1;
                }else if($_POST["G2745_C62571"] == 'No'){
                    $G2745_C62571 = 1;
                }else{
                    $G2745_C62571 = $_POST["G2745_C62571"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62571 = ".$G2745_C62571."";
                $LsqlI .= $separador." G2745_C62571";
                $LsqlV .= $separador.$G2745_C62571;

                $validar = 1;
            }
  
            $G2745_C62572 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62572"])){
                if($_POST["G2745_C62572"] == 'Yes'){
                    $G2745_C62572 = 1;
                }else if($_POST["G2745_C62572"] == 'off'){
                    $G2745_C62572 = 0;
                }else if($_POST["G2745_C62572"] == 'on'){
                    $G2745_C62572 = 1;
                }else if($_POST["G2745_C62572"] == 'No'){
                    $G2745_C62572 = 1;
                }else{
                    $G2745_C62572 = $_POST["G2745_C62572"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62572 = ".$G2745_C62572."";
                $LsqlI .= $separador." G2745_C62572";
                $LsqlV .= $separador.$G2745_C62572;

                $validar = 1;
            }
  
            $G2745_C62573 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62573"])){
                if($_POST["G2745_C62573"] == 'Yes'){
                    $G2745_C62573 = 1;
                }else if($_POST["G2745_C62573"] == 'off'){
                    $G2745_C62573 = 0;
                }else if($_POST["G2745_C62573"] == 'on'){
                    $G2745_C62573 = 1;
                }else if($_POST["G2745_C62573"] == 'No'){
                    $G2745_C62573 = 1;
                }else{
                    $G2745_C62573 = $_POST["G2745_C62573"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62573 = ".$G2745_C62573."";
                $LsqlI .= $separador." G2745_C62573";
                $LsqlV .= $separador.$G2745_C62573;

                $validar = 1;
            }
  
            $G2745_C61282 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C61282"])){
                if($_POST["G2745_C61282"] == 'Yes'){
                    $G2745_C61282 = 1;
                }else if($_POST["G2745_C61282"] == 'off'){
                    $G2745_C61282 = 0;
                }else if($_POST["G2745_C61282"] == 'on'){
                    $G2745_C61282 = 1;
                }else if($_POST["G2745_C61282"] == 'No'){
                    $G2745_C61282 = 1;
                }else{
                    $G2745_C61282 = $_POST["G2745_C61282"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C61282 = ".$G2745_C61282."";
                $LsqlI .= $separador." G2745_C61282";
                $LsqlV .= $separador.$G2745_C61282;

                $validar = 1;
            }
  
            $G2745_C62574 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62574"])){
                if($_POST["G2745_C62574"] == 'Yes'){
                    $G2745_C62574 = 1;
                }else if($_POST["G2745_C62574"] == 'off'){
                    $G2745_C62574 = 0;
                }else if($_POST["G2745_C62574"] == 'on'){
                    $G2745_C62574 = 1;
                }else if($_POST["G2745_C62574"] == 'No'){
                    $G2745_C62574 = 1;
                }else{
                    $G2745_C62574 = $_POST["G2745_C62574"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62574 = ".$G2745_C62574."";
                $LsqlI .= $separador." G2745_C62574";
                $LsqlV .= $separador.$G2745_C62574;

                $validar = 1;
            }
  
            $G2745_C62575 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62575"])){
                if($_POST["G2745_C62575"] == 'Yes'){
                    $G2745_C62575 = 1;
                }else if($_POST["G2745_C62575"] == 'off'){
                    $G2745_C62575 = 0;
                }else if($_POST["G2745_C62575"] == 'on'){
                    $G2745_C62575 = 1;
                }else if($_POST["G2745_C62575"] == 'No'){
                    $G2745_C62575 = 1;
                }else{
                    $G2745_C62575 = $_POST["G2745_C62575"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62575 = ".$G2745_C62575."";
                $LsqlI .= $separador." G2745_C62575";
                $LsqlV .= $separador.$G2745_C62575;

                $validar = 1;
            }
  
            $G2745_C62576 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62576"])){
                if($_POST["G2745_C62576"] == 'Yes'){
                    $G2745_C62576 = 1;
                }else if($_POST["G2745_C62576"] == 'off'){
                    $G2745_C62576 = 0;
                }else if($_POST["G2745_C62576"] == 'on'){
                    $G2745_C62576 = 1;
                }else if($_POST["G2745_C62576"] == 'No'){
                    $G2745_C62576 = 1;
                }else{
                    $G2745_C62576 = $_POST["G2745_C62576"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62576 = ".$G2745_C62576."";
                $LsqlI .= $separador." G2745_C62576";
                $LsqlV .= $separador.$G2745_C62576;

                $validar = 1;
            }
  
            $G2745_C62577 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62577"])){
                if($_POST["G2745_C62577"] == 'Yes'){
                    $G2745_C62577 = 1;
                }else if($_POST["G2745_C62577"] == 'off'){
                    $G2745_C62577 = 0;
                }else if($_POST["G2745_C62577"] == 'on'){
                    $G2745_C62577 = 1;
                }else if($_POST["G2745_C62577"] == 'No'){
                    $G2745_C62577 = 1;
                }else{
                    $G2745_C62577 = $_POST["G2745_C62577"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62577 = ".$G2745_C62577."";
                $LsqlI .= $separador." G2745_C62577";
                $LsqlV .= $separador.$G2745_C62577;

                $validar = 1;
            }
  
            $G2745_C62578 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62578"])){
                if($_POST["G2745_C62578"] == 'Yes'){
                    $G2745_C62578 = 1;
                }else if($_POST["G2745_C62578"] == 'off'){
                    $G2745_C62578 = 0;
                }else if($_POST["G2745_C62578"] == 'on'){
                    $G2745_C62578 = 1;
                }else if($_POST["G2745_C62578"] == 'No'){
                    $G2745_C62578 = 1;
                }else{
                    $G2745_C62578 = $_POST["G2745_C62578"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62578 = ".$G2745_C62578."";
                $LsqlI .= $separador." G2745_C62578";
                $LsqlV .= $separador.$G2745_C62578;

                $validar = 1;
            }
  
            $G2745_C62579 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62579"])){
                if($_POST["G2745_C62579"] == 'Yes'){
                    $G2745_C62579 = 1;
                }else if($_POST["G2745_C62579"] == 'off'){
                    $G2745_C62579 = 0;
                }else if($_POST["G2745_C62579"] == 'on'){
                    $G2745_C62579 = 1;
                }else if($_POST["G2745_C62579"] == 'No'){
                    $G2745_C62579 = 1;
                }else{
                    $G2745_C62579 = $_POST["G2745_C62579"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62579 = ".$G2745_C62579."";
                $LsqlI .= $separador." G2745_C62579";
                $LsqlV .= $separador.$G2745_C62579;

                $validar = 1;
            }
  
            $G2745_C62580 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62580"])){
                if($_POST["G2745_C62580"] == 'Yes'){
                    $G2745_C62580 = 1;
                }else if($_POST["G2745_C62580"] == 'off'){
                    $G2745_C62580 = 0;
                }else if($_POST["G2745_C62580"] == 'on'){
                    $G2745_C62580 = 1;
                }else if($_POST["G2745_C62580"] == 'No'){
                    $G2745_C62580 = 1;
                }else{
                    $G2745_C62580 = $_POST["G2745_C62580"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62580 = ".$G2745_C62580."";
                $LsqlI .= $separador." G2745_C62580";
                $LsqlV .= $separador.$G2745_C62580;

                $validar = 1;
            }
  
            $G2745_C62581 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62581"])){
                if($_POST["G2745_C62581"] == 'Yes'){
                    $G2745_C62581 = 1;
                }else if($_POST["G2745_C62581"] == 'off'){
                    $G2745_C62581 = 0;
                }else if($_POST["G2745_C62581"] == 'on'){
                    $G2745_C62581 = 1;
                }else if($_POST["G2745_C62581"] == 'No'){
                    $G2745_C62581 = 1;
                }else{
                    $G2745_C62581 = $_POST["G2745_C62581"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62581 = ".$G2745_C62581."";
                $LsqlI .= $separador." G2745_C62581";
                $LsqlV .= $separador.$G2745_C62581;

                $validar = 1;
            }
  
            $G2745_C62582 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2745_C62582"])){
                if($_POST["G2745_C62582"] == 'Yes'){
                    $G2745_C62582 = 1;
                }else if($_POST["G2745_C62582"] == 'off'){
                    $G2745_C62582 = 0;
                }else if($_POST["G2745_C62582"] == 'on'){
                    $G2745_C62582 = 1;
                }else if($_POST["G2745_C62582"] == 'No'){
                    $G2745_C62582 = 1;
                }else{
                    $G2745_C62582 = $_POST["G2745_C62582"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2745_C62582 = ".$G2745_C62582."";
                $LsqlI .= $separador." G2745_C62582";
                $LsqlV .= $separador.$G2745_C62582;

                $validar = 1;
            }
  

            if(isset($_POST["G2745_C62222"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C62222 = '".$_POST["G2745_C62222"]."'";
                $LsqlI .= $separador."G2745_C62222";
                $LsqlV .= $separador."'".$_POST["G2745_C62222"]."'";
                $validar = 1;
            }
             
  
            $G2745_C62212 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2745_C62212"])){
                if($_POST["G2745_C62212"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2745_C62212 = $_POST["G2745_C62212"];
                    $LsqlU .= $separador." G2745_C62212 = ".$G2745_C62212."";
                    $LsqlI .= $separador." G2745_C62212";
                    $LsqlV .= $separador.$G2745_C62212;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2745_C62223"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C62223 = '".$_POST["G2745_C62223"]."'";
                $LsqlI .= $separador."G2745_C62223";
                $LsqlV .= $separador."'".$_POST["G2745_C62223"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C61647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C61647 = '".$_POST["G2745_C61647"]."'";
                $LsqlI .= $separador."G2745_C61647";
                $LsqlV .= $separador."'".$_POST["G2745_C61647"]."'";
                $validar = 1;
            }
             
  
            $G2745_C61646 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2745_C61646"])){
                if($_POST["G2745_C61646"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2745_C61646 = $_POST["G2745_C61646"];
                    $LsqlU .= $separador." G2745_C61646 = ".$G2745_C61646."";
                    $LsqlI .= $separador." G2745_C61646";
                    $LsqlV .= $separador.$G2745_C61646;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2745_C61648"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C61648 = '".$_POST["G2745_C61648"]."'";
                $LsqlI .= $separador."G2745_C61648";
                $LsqlV .= $separador."'".$_POST["G2745_C61648"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C61649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C61649 = '".$_POST["G2745_C61649"]."'";
                $LsqlI .= $separador."G2745_C61649";
                $LsqlV .= $separador."'".$_POST["G2745_C61649"]."'";
                $validar = 1;
            }
             
 
            $G2745_C62210 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2745_C62210"])){    
                if($_POST["G2745_C62210"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2745_C62210"]);
                    if(count($tieneHora) > 1){
                        $G2745_C62210 = "'".$_POST["G2745_C62210"]."'";
                    }else{
                        $G2745_C62210 = "'".str_replace(' ', '',$_POST["G2745_C62210"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2745_C62210 = ".$G2745_C62210;
                    $LsqlI .= $separador." G2745_C62210";
                    $LsqlV .= $separador.$G2745_C62210;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2745_C62211"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C62211 = '".$_POST["G2745_C62211"]."'";
                $LsqlI .= $separador."G2745_C62211";
                $LsqlV .= $separador."'".$_POST["G2745_C62211"]."'";
                $validar = 1;
            }
             
 
            $G2745_C63243 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2745_C63243"])){    
                if($_POST["G2745_C63243"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2745_C63243"]);
                    if(count($tieneHora) > 1){
                        $G2745_C63243 = "'".$_POST["G2745_C63243"]."'";
                    }else{
                        $G2745_C63243 = "'".str_replace(' ', '',$_POST["G2745_C63243"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2745_C63243 = ".$G2745_C63243;
                    $LsqlI .= $separador." G2745_C63243";
                    $LsqlV .= $separador.$G2745_C63243;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2745_C63244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C63244 = '".$_POST["G2745_C63244"]."'";
                $LsqlI .= $separador."G2745_C63244";
                $LsqlV .= $separador."'".$_POST["G2745_C63244"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C63246"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C63246 = '".$_POST["G2745_C63246"]."'";
                $LsqlI .= $separador."G2745_C63246";
                $LsqlV .= $separador."'".$_POST["G2745_C63246"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2745_C63247"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_C63247 = '".$_POST["G2745_C63247"]."'";
                $LsqlI .= $separador."G2745_C63247";
                $LsqlV .= $separador."'".$_POST["G2745_C63247"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2745_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2745_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2745_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2745_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2745_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2745_Usuario , G2745_FechaInsercion, G2745_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2745_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2745 WHERE G2745_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
