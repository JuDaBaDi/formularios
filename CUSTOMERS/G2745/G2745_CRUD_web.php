<?php

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
        
    //Inserciones o actualizaciones

    if(isset($_POST['getListaHija'])){
        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }
        
    if(isset($_POST["oper"])){
            $str_Lsql  = '';

            $validar = 0;
            $str_LsqlU = "UPDATE ".$BaseDatos.".G2745 SET "; 
            $str_LsqlI = "INSERT INTO ".$BaseDatos.".G2745( G2745_FechaInsercion ,";
            $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G2745_C53217"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C53217 = '".$_POST["G2745_C53217"]."'";
            $str_LsqlI .= $separador."G2745_C53217";
            $str_LsqlV .= $separador."'".$_POST["G2745_C53217"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2745_C53218"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C53218 = '".$_POST["G2745_C53218"]."'";
            $str_LsqlI .= $separador."G2745_C53218";
            $str_LsqlV .= $separador."'".$_POST["G2745_C53218"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2745_C53219"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C53219 = '".$_POST["G2745_C53219"]."'";
            $str_LsqlI .= $separador."G2745_C53219";
            $str_LsqlV .= $separador."'".$_POST["G2745_C53219"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2745_C53220"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C53220 = '".$_POST["G2745_C53220"]."'";
            $str_LsqlI .= $separador."G2745_C53220";
            $str_LsqlV .= $separador."'".$_POST["G2745_C53220"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2745_C61248"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C61248 = '".$_POST["G2745_C61248"]."'";
            $str_LsqlI .= $separador."G2745_C61248";
            $str_LsqlV .= $separador."'".$_POST["G2745_C61248"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2745_C61249"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C61249 = '".$_POST["G2745_C61249"]."'";
            $str_LsqlI .= $separador."G2745_C61249";
            $str_LsqlV .= $separador."'".$_POST["G2745_C61249"]."'";
            $validar = 1;
        }
         
 
        $G2745_C61250 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G2745_C61250"])){    
            if($_POST["G2745_C61250"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G2745_C61250 = "'".str_replace(' ', '',$_POST["G2745_C61250"])." 00:00:00'";
                $str_LsqlU .= $separador." G2745_C61250 = ".$G2745_C61250;
                $str_LsqlI .= $separador." G2745_C61250";
                $str_LsqlV .= $separador.$G2745_C61250;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G2745_C61252"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_C61252 = '".$_POST["G2745_C61252"]."'";
            $str_LsqlI .= $separador."G2745_C61252";
            $str_LsqlV .= $separador."'".$_POST["G2745_C61252"]."'";
            $validar = 1;
        }
         
  
        $G2745_C62562 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62562"])){
            if($_POST["G2745_C62562"] == 'Yes'){
                $G2745_C62562 = 1;
            }else if($_POST["G2745_C62562"] == 'off'){
                $G2745_C62562 = 0;
            }else if($_POST["G2745_C62562"] == 'on'){
                $G2745_C62562 = 1;
            }else if($_POST["G2745_C62562"] == 'No'){
                $G2745_C62562 = 0;
            }else{
                $G2745_C62562 = $_POST["G2745_C62562"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62562 = ".$G2745_C62562."";
            $str_LsqlI .= $separador." G2745_C62562";
            $str_LsqlV .= $separador.$G2745_C62562;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62562 = ".$G2745_C62562."";
            $str_LsqlI .= $separador." G2745_C62562";
            $str_LsqlV .= $separador.$G2745_C62562;

            $validar = 1;
        }
  
        $G2745_C62563 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62563"])){
            if($_POST["G2745_C62563"] == 'Yes'){
                $G2745_C62563 = 1;
            }else if($_POST["G2745_C62563"] == 'off'){
                $G2745_C62563 = 0;
            }else if($_POST["G2745_C62563"] == 'on'){
                $G2745_C62563 = 1;
            }else if($_POST["G2745_C62563"] == 'No'){
                $G2745_C62563 = 0;
            }else{
                $G2745_C62563 = $_POST["G2745_C62563"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62563 = ".$G2745_C62563."";
            $str_LsqlI .= $separador." G2745_C62563";
            $str_LsqlV .= $separador.$G2745_C62563;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62563 = ".$G2745_C62563."";
            $str_LsqlI .= $separador." G2745_C62563";
            $str_LsqlV .= $separador.$G2745_C62563;

            $validar = 1;
        }
  
        $G2745_C62564 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62564"])){
            if($_POST["G2745_C62564"] == 'Yes'){
                $G2745_C62564 = 1;
            }else if($_POST["G2745_C62564"] == 'off'){
                $G2745_C62564 = 0;
            }else if($_POST["G2745_C62564"] == 'on'){
                $G2745_C62564 = 1;
            }else if($_POST["G2745_C62564"] == 'No'){
                $G2745_C62564 = 0;
            }else{
                $G2745_C62564 = $_POST["G2745_C62564"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62564 = ".$G2745_C62564."";
            $str_LsqlI .= $separador." G2745_C62564";
            $str_LsqlV .= $separador.$G2745_C62564;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62564 = ".$G2745_C62564."";
            $str_LsqlI .= $separador." G2745_C62564";
            $str_LsqlV .= $separador.$G2745_C62564;

            $validar = 1;
        }
  
        $G2745_C62565 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62565"])){
            if($_POST["G2745_C62565"] == 'Yes'){
                $G2745_C62565 = 1;
            }else if($_POST["G2745_C62565"] == 'off'){
                $G2745_C62565 = 0;
            }else if($_POST["G2745_C62565"] == 'on'){
                $G2745_C62565 = 1;
            }else if($_POST["G2745_C62565"] == 'No'){
                $G2745_C62565 = 0;
            }else{
                $G2745_C62565 = $_POST["G2745_C62565"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62565 = ".$G2745_C62565."";
            $str_LsqlI .= $separador." G2745_C62565";
            $str_LsqlV .= $separador.$G2745_C62565;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62565 = ".$G2745_C62565."";
            $str_LsqlI .= $separador." G2745_C62565";
            $str_LsqlV .= $separador.$G2745_C62565;

            $validar = 1;
        }
  
        $G2745_C62566 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62566"])){
            if($_POST["G2745_C62566"] == 'Yes'){
                $G2745_C62566 = 1;
            }else if($_POST["G2745_C62566"] == 'off'){
                $G2745_C62566 = 0;
            }else if($_POST["G2745_C62566"] == 'on'){
                $G2745_C62566 = 1;
            }else if($_POST["G2745_C62566"] == 'No'){
                $G2745_C62566 = 0;
            }else{
                $G2745_C62566 = $_POST["G2745_C62566"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62566 = ".$G2745_C62566."";
            $str_LsqlI .= $separador." G2745_C62566";
            $str_LsqlV .= $separador.$G2745_C62566;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62566 = ".$G2745_C62566."";
            $str_LsqlI .= $separador." G2745_C62566";
            $str_LsqlV .= $separador.$G2745_C62566;

            $validar = 1;
        }
  
        $G2745_C62567 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62567"])){
            if($_POST["G2745_C62567"] == 'Yes'){
                $G2745_C62567 = 1;
            }else if($_POST["G2745_C62567"] == 'off'){
                $G2745_C62567 = 0;
            }else if($_POST["G2745_C62567"] == 'on'){
                $G2745_C62567 = 1;
            }else if($_POST["G2745_C62567"] == 'No'){
                $G2745_C62567 = 0;
            }else{
                $G2745_C62567 = $_POST["G2745_C62567"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62567 = ".$G2745_C62567."";
            $str_LsqlI .= $separador." G2745_C62567";
            $str_LsqlV .= $separador.$G2745_C62567;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62567 = ".$G2745_C62567."";
            $str_LsqlI .= $separador." G2745_C62567";
            $str_LsqlV .= $separador.$G2745_C62567;

            $validar = 1;
        }
  
        $G2745_C62568 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62568"])){
            if($_POST["G2745_C62568"] == 'Yes'){
                $G2745_C62568 = 1;
            }else if($_POST["G2745_C62568"] == 'off'){
                $G2745_C62568 = 0;
            }else if($_POST["G2745_C62568"] == 'on'){
                $G2745_C62568 = 1;
            }else if($_POST["G2745_C62568"] == 'No'){
                $G2745_C62568 = 0;
            }else{
                $G2745_C62568 = $_POST["G2745_C62568"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62568 = ".$G2745_C62568."";
            $str_LsqlI .= $separador." G2745_C62568";
            $str_LsqlV .= $separador.$G2745_C62568;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62568 = ".$G2745_C62568."";
            $str_LsqlI .= $separador." G2745_C62568";
            $str_LsqlV .= $separador.$G2745_C62568;

            $validar = 1;
        }
  
        $G2745_C62569 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62569"])){
            if($_POST["G2745_C62569"] == 'Yes'){
                $G2745_C62569 = 1;
            }else if($_POST["G2745_C62569"] == 'off'){
                $G2745_C62569 = 0;
            }else if($_POST["G2745_C62569"] == 'on'){
                $G2745_C62569 = 1;
            }else if($_POST["G2745_C62569"] == 'No'){
                $G2745_C62569 = 0;
            }else{
                $G2745_C62569 = $_POST["G2745_C62569"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62569 = ".$G2745_C62569."";
            $str_LsqlI .= $separador." G2745_C62569";
            $str_LsqlV .= $separador.$G2745_C62569;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62569 = ".$G2745_C62569."";
            $str_LsqlI .= $separador." G2745_C62569";
            $str_LsqlV .= $separador.$G2745_C62569;

            $validar = 1;
        }
  
        $G2745_C62570 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62570"])){
            if($_POST["G2745_C62570"] == 'Yes'){
                $G2745_C62570 = 1;
            }else if($_POST["G2745_C62570"] == 'off'){
                $G2745_C62570 = 0;
            }else if($_POST["G2745_C62570"] == 'on'){
                $G2745_C62570 = 1;
            }else if($_POST["G2745_C62570"] == 'No'){
                $G2745_C62570 = 0;
            }else{
                $G2745_C62570 = $_POST["G2745_C62570"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62570 = ".$G2745_C62570."";
            $str_LsqlI .= $separador." G2745_C62570";
            $str_LsqlV .= $separador.$G2745_C62570;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62570 = ".$G2745_C62570."";
            $str_LsqlI .= $separador." G2745_C62570";
            $str_LsqlV .= $separador.$G2745_C62570;

            $validar = 1;
        }
  
        $G2745_C62571 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62571"])){
            if($_POST["G2745_C62571"] == 'Yes'){
                $G2745_C62571 = 1;
            }else if($_POST["G2745_C62571"] == 'off'){
                $G2745_C62571 = 0;
            }else if($_POST["G2745_C62571"] == 'on'){
                $G2745_C62571 = 1;
            }else if($_POST["G2745_C62571"] == 'No'){
                $G2745_C62571 = 0;
            }else{
                $G2745_C62571 = $_POST["G2745_C62571"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62571 = ".$G2745_C62571."";
            $str_LsqlI .= $separador." G2745_C62571";
            $str_LsqlV .= $separador.$G2745_C62571;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62571 = ".$G2745_C62571."";
            $str_LsqlI .= $separador." G2745_C62571";
            $str_LsqlV .= $separador.$G2745_C62571;

            $validar = 1;
        }
  
        $G2745_C62572 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62572"])){
            if($_POST["G2745_C62572"] == 'Yes'){
                $G2745_C62572 = 1;
            }else if($_POST["G2745_C62572"] == 'off'){
                $G2745_C62572 = 0;
            }else if($_POST["G2745_C62572"] == 'on'){
                $G2745_C62572 = 1;
            }else if($_POST["G2745_C62572"] == 'No'){
                $G2745_C62572 = 0;
            }else{
                $G2745_C62572 = $_POST["G2745_C62572"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62572 = ".$G2745_C62572."";
            $str_LsqlI .= $separador." G2745_C62572";
            $str_LsqlV .= $separador.$G2745_C62572;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62572 = ".$G2745_C62572."";
            $str_LsqlI .= $separador." G2745_C62572";
            $str_LsqlV .= $separador.$G2745_C62572;

            $validar = 1;
        }
  
        $G2745_C62573 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62573"])){
            if($_POST["G2745_C62573"] == 'Yes'){
                $G2745_C62573 = 1;
            }else if($_POST["G2745_C62573"] == 'off'){
                $G2745_C62573 = 0;
            }else if($_POST["G2745_C62573"] == 'on'){
                $G2745_C62573 = 1;
            }else if($_POST["G2745_C62573"] == 'No'){
                $G2745_C62573 = 0;
            }else{
                $G2745_C62573 = $_POST["G2745_C62573"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62573 = ".$G2745_C62573."";
            $str_LsqlI .= $separador." G2745_C62573";
            $str_LsqlV .= $separador.$G2745_C62573;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62573 = ".$G2745_C62573."";
            $str_LsqlI .= $separador." G2745_C62573";
            $str_LsqlV .= $separador.$G2745_C62573;

            $validar = 1;
        }
  
        $G2745_C61282 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C61282"])){
            if($_POST["G2745_C61282"] == 'Yes'){
                $G2745_C61282 = 1;
            }else if($_POST["G2745_C61282"] == 'off'){
                $G2745_C61282 = 0;
            }else if($_POST["G2745_C61282"] == 'on'){
                $G2745_C61282 = 1;
            }else if($_POST["G2745_C61282"] == 'No'){
                $G2745_C61282 = 0;
            }else{
                $G2745_C61282 = $_POST["G2745_C61282"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C61282 = ".$G2745_C61282."";
            $str_LsqlI .= $separador." G2745_C61282";
            $str_LsqlV .= $separador.$G2745_C61282;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C61282 = ".$G2745_C61282."";
            $str_LsqlI .= $separador." G2745_C61282";
            $str_LsqlV .= $separador.$G2745_C61282;

            $validar = 1;
        }
  
        $G2745_C62574 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62574"])){
            if($_POST["G2745_C62574"] == 'Yes'){
                $G2745_C62574 = 1;
            }else if($_POST["G2745_C62574"] == 'off'){
                $G2745_C62574 = 0;
            }else if($_POST["G2745_C62574"] == 'on'){
                $G2745_C62574 = 1;
            }else if($_POST["G2745_C62574"] == 'No'){
                $G2745_C62574 = 0;
            }else{
                $G2745_C62574 = $_POST["G2745_C62574"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62574 = ".$G2745_C62574."";
            $str_LsqlI .= $separador." G2745_C62574";
            $str_LsqlV .= $separador.$G2745_C62574;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62574 = ".$G2745_C62574."";
            $str_LsqlI .= $separador." G2745_C62574";
            $str_LsqlV .= $separador.$G2745_C62574;

            $validar = 1;
        }
  
        $G2745_C62575 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62575"])){
            if($_POST["G2745_C62575"] == 'Yes'){
                $G2745_C62575 = 1;
            }else if($_POST["G2745_C62575"] == 'off'){
                $G2745_C62575 = 0;
            }else if($_POST["G2745_C62575"] == 'on'){
                $G2745_C62575 = 1;
            }else if($_POST["G2745_C62575"] == 'No'){
                $G2745_C62575 = 0;
            }else{
                $G2745_C62575 = $_POST["G2745_C62575"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62575 = ".$G2745_C62575."";
            $str_LsqlI .= $separador." G2745_C62575";
            $str_LsqlV .= $separador.$G2745_C62575;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62575 = ".$G2745_C62575."";
            $str_LsqlI .= $separador." G2745_C62575";
            $str_LsqlV .= $separador.$G2745_C62575;

            $validar = 1;
        }
  
        $G2745_C62576 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62576"])){
            if($_POST["G2745_C62576"] == 'Yes'){
                $G2745_C62576 = 1;
            }else if($_POST["G2745_C62576"] == 'off'){
                $G2745_C62576 = 0;
            }else if($_POST["G2745_C62576"] == 'on'){
                $G2745_C62576 = 1;
            }else if($_POST["G2745_C62576"] == 'No'){
                $G2745_C62576 = 0;
            }else{
                $G2745_C62576 = $_POST["G2745_C62576"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62576 = ".$G2745_C62576."";
            $str_LsqlI .= $separador." G2745_C62576";
            $str_LsqlV .= $separador.$G2745_C62576;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62576 = ".$G2745_C62576."";
            $str_LsqlI .= $separador." G2745_C62576";
            $str_LsqlV .= $separador.$G2745_C62576;

            $validar = 1;
        }
  
        $G2745_C62577 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62577"])){
            if($_POST["G2745_C62577"] == 'Yes'){
                $G2745_C62577 = 1;
            }else if($_POST["G2745_C62577"] == 'off'){
                $G2745_C62577 = 0;
            }else if($_POST["G2745_C62577"] == 'on'){
                $G2745_C62577 = 1;
            }else if($_POST["G2745_C62577"] == 'No'){
                $G2745_C62577 = 0;
            }else{
                $G2745_C62577 = $_POST["G2745_C62577"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62577 = ".$G2745_C62577."";
            $str_LsqlI .= $separador." G2745_C62577";
            $str_LsqlV .= $separador.$G2745_C62577;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62577 = ".$G2745_C62577."";
            $str_LsqlI .= $separador." G2745_C62577";
            $str_LsqlV .= $separador.$G2745_C62577;

            $validar = 1;
        }
  
        $G2745_C62578 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62578"])){
            if($_POST["G2745_C62578"] == 'Yes'){
                $G2745_C62578 = 1;
            }else if($_POST["G2745_C62578"] == 'off'){
                $G2745_C62578 = 0;
            }else if($_POST["G2745_C62578"] == 'on'){
                $G2745_C62578 = 1;
            }else if($_POST["G2745_C62578"] == 'No'){
                $G2745_C62578 = 0;
            }else{
                $G2745_C62578 = $_POST["G2745_C62578"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62578 = ".$G2745_C62578."";
            $str_LsqlI .= $separador." G2745_C62578";
            $str_LsqlV .= $separador.$G2745_C62578;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62578 = ".$G2745_C62578."";
            $str_LsqlI .= $separador." G2745_C62578";
            $str_LsqlV .= $separador.$G2745_C62578;

            $validar = 1;
        }
  
        $G2745_C62579 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62579"])){
            if($_POST["G2745_C62579"] == 'Yes'){
                $G2745_C62579 = 1;
            }else if($_POST["G2745_C62579"] == 'off'){
                $G2745_C62579 = 0;
            }else if($_POST["G2745_C62579"] == 'on'){
                $G2745_C62579 = 1;
            }else if($_POST["G2745_C62579"] == 'No'){
                $G2745_C62579 = 0;
            }else{
                $G2745_C62579 = $_POST["G2745_C62579"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62579 = ".$G2745_C62579."";
            $str_LsqlI .= $separador." G2745_C62579";
            $str_LsqlV .= $separador.$G2745_C62579;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62579 = ".$G2745_C62579."";
            $str_LsqlI .= $separador." G2745_C62579";
            $str_LsqlV .= $separador.$G2745_C62579;

            $validar = 1;
        }
  
        $G2745_C62580 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62580"])){
            if($_POST["G2745_C62580"] == 'Yes'){
                $G2745_C62580 = 1;
            }else if($_POST["G2745_C62580"] == 'off'){
                $G2745_C62580 = 0;
            }else if($_POST["G2745_C62580"] == 'on'){
                $G2745_C62580 = 1;
            }else if($_POST["G2745_C62580"] == 'No'){
                $G2745_C62580 = 0;
            }else{
                $G2745_C62580 = $_POST["G2745_C62580"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62580 = ".$G2745_C62580."";
            $str_LsqlI .= $separador." G2745_C62580";
            $str_LsqlV .= $separador.$G2745_C62580;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62580 = ".$G2745_C62580."";
            $str_LsqlI .= $separador." G2745_C62580";
            $str_LsqlV .= $separador.$G2745_C62580;

            $validar = 1;
        }
  
        $G2745_C62581 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62581"])){
            if($_POST["G2745_C62581"] == 'Yes'){
                $G2745_C62581 = 1;
            }else if($_POST["G2745_C62581"] == 'off'){
                $G2745_C62581 = 0;
            }else if($_POST["G2745_C62581"] == 'on'){
                $G2745_C62581 = 1;
            }else if($_POST["G2745_C62581"] == 'No'){
                $G2745_C62581 = 0;
            }else{
                $G2745_C62581 = $_POST["G2745_C62581"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62581 = ".$G2745_C62581."";
            $str_LsqlI .= $separador." G2745_C62581";
            $str_LsqlV .= $separador.$G2745_C62581;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62581 = ".$G2745_C62581."";
            $str_LsqlI .= $separador." G2745_C62581";
            $str_LsqlV .= $separador.$G2745_C62581;

            $validar = 1;
        }
  
        $G2745_C62582 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G2745_C62582"])){
            if($_POST["G2745_C62582"] == 'Yes'){
                $G2745_C62582 = 1;
            }else if($_POST["G2745_C62582"] == 'off'){
                $G2745_C62582 = 0;
            }else if($_POST["G2745_C62582"] == 'on'){
                $G2745_C62582 = 1;
            }else if($_POST["G2745_C62582"] == 'No'){
                $G2745_C62582 = 0;
            }else{
                $G2745_C62582 = $_POST["G2745_C62582"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62582 = ".$G2745_C62582."";
            $str_LsqlI .= $separador." G2745_C62582";
            $str_LsqlV .= $separador.$G2745_C62582;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G2745_C62582 = ".$G2745_C62582."";
            $str_LsqlI .= $separador." G2745_C62582";
            $str_LsqlV .= $separador.$G2745_C62582;

            $validar = 1;
        }
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G2745_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2745_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G2745_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //Si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;
                //ahora toca ver lo de la muestra asi que toca ver que pasa 
                /* primero buscamos la campaña que nos esta llegando */
                $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana"];

                //echo $Lsql_Campan;

                $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
                $datoCampan = $res_Lsql_Campan->fetch_array();
                $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
                $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
                $int_CAMPAN_ActPo = $datoCampan['CAMPAN_ActPobGui_b'];


                if($int_CAMPAN_ActPo == '-1'){
                    /* toca hacer actualizacion desde Script */
                    
                    $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana"];
                    $resultcampSql = $mysqli->query($campSql);
                    $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
                    $i=0;
                    while($key = $resultcampSql->fetch_object()){

                        if($i == 0){
                            $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }else{
                            $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }
                        $i++;
                    } 
                    $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$ultimoResgistroInsertado.' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
                    //echo "Esta ".$Lsql;
                    if($mysqli->query($Lsql) === TRUE ){

                    }else{
                        echo "NO sE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
                    }
                }


                //Ahora toca actualizar la muestra
                $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 3, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = '-9' , 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".date('Y-m-d H:i:s')."', 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = 3,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = 'No desea participar',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = NULL,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = '-9',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b ='".date('Y-m-d H:i:s')."'";
                $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_POST['id'];
                // echo $MuestraSql;
                if($mysqli->query($MuestraSql) === true){

                }else{
                    echo "Error insertando la muesta => ".$mysqli->error;
                }
                
                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=Mjc0NQ==&result=1');

            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
