
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2745/G2745_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2745_ConsInte__b as id, G2745_C53218 as camp1 , G2745_C53218 as camp2 FROM ".$BaseDatos.".G2745  WHERE G2745_Usuario = ".$idUsuario." ORDER BY G2745_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2745_ConsInte__b as id, G2745_C53218 as camp1 , G2745_C53218 as camp2 FROM ".$BaseDatos.".G2745  ORDER BY G2745_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2745_ConsInte__b as id, G2745_C53218 as camp1 , G2745_C53218 as camp2 FROM ".$BaseDatos.".G2745 JOIN ".$BaseDatos.".G2745_M".$resultEstpas->muestr." ON G2745_ConsInte__b = G2745_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2745_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2745_ConsInte__b as id, G2745_C53218 as camp1 , G2745_C53218 as camp2 FROM ".$BaseDatos.".G2745 JOIN ".$BaseDatos.".G2745_M".$resultEstpas->muestr." ON G2745_ConsInte__b = G2745_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2745_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2745_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G2745_ConsInte__b as id, G2745_C53218 as camp1 , G2745_C53218 as camp2 FROM ".$BaseDatos.".G2745  ORDER BY G2745_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    
    //JDBD - Validamos si se pudo obtener el dato principal.
    if ($nombre = $mysqli->query($XLsql)) {

        $nombreUsuario = NULL;
        //echo $XLsql;
        while ($key = $nombre->fetch_object()) {
            echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
            $nombreUsuario = $key->nombre;
            break;
        } 


        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                        
            $data = array(  "strToken_t" => $_GET['token'], 
                            "strIdGestion_t" => $_GET['id_gestion_cbx'],
                            "strDatoPrincipal_t" => $nombreUsuario,
                            "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
            $data_string = json_encode($data);    

            $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                      
            ); 
            //recogemos la respuesta
            $respuesta = curl_exec ($ch);
            //o el error, por si falla
            $error = curl_error($ch);
            //y finalmente cerramos curl
            //echo "Respuesta =>  ". $respuesta;
            //echo "<br/>Error => ".$error;
            //include "Log.class.php";
            //$log = new Log("log", "./Log/");
            //$log->insert($error, $respuesta, false, true, false);
            //echo "nada";
            curl_close ($ch);
        }

    }else{
        echo "<script>alert('NO SE PUDO OBTENER EL DATO PRINCIPAL DEL REGISTRO.');</script>";
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  id="8180" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53217" id="LblG2745_C53217">NOMBRE</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53217" value="<?php if (isset($_GET['G2745_C53217'])) {
                            echo $_GET['G2745_C53217'];
                        } ?>"  name="G2745_C53217"  placeholder="NOMBRE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53218" id="LblG2745_C53218">CEDULA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53218" value="<?php if (isset($_GET['G2745_C53218'])) {
                            echo $_GET['G2745_C53218'];
                        } ?>"  name="G2745_C53218"  placeholder="CEDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53219" id="LblG2745_C53219">TELEFONO PERSONAL</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53219" value="<?php if (isset($_GET['G2745_C53219'])) {
                            echo $_GET['G2745_C53219'];
                        } ?>"  name="G2745_C53219"  placeholder="TELEFONO PERSONAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53220" id="LblG2745_C53220">CIUDAD</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53220" value="<?php if (isset($_GET['G2745_C53220'])) {
                            echo $_GET['G2745_C53220'];
                        } ?>"  name="G2745_C53220"  placeholder="CIUDAD"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8182" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53211" id="LblG2745_C53211">Agente</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53211" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G2745_C53211"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53212" id="LblG2745_C53212">Fecha</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53212" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G2745_C53212"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53213" id="LblG2745_C53213">Hora</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53213" value="<?php echo date('H:i:s');?>" readonly name="G2745_C53213"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C53214" id="LblG2745_C53214">Campaña</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C53214" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G2745_C53214"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8181" >
<h3 class="box box-title"></h3>

</div>

<div  id="8183" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días|tardes|noches, podría comunicarme con el señor(a) |NombreCliente|</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Mi nombre es |Agente|, le estoy llamando de |Empresa| con el fin de ...</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="9468" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9468c">
                CAMPOS A DILIGENCIAR
            </a>
        </h4>
        
    </div>
    <div id="s_9468c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61248" id="LblG2745_C61248">Nombre de la empresa que realizo la venta</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61248" id="G2745_C61248">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3699 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61249" id="LblG2745_C61249">Nombre de la empresa para la que se realiza la venta</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61249" id="G2745_C61249">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3699 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2745_C61250" id="LblG2745_C61250">Fecha de realización de llamada</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2745_C61250'])) {
                            echo $_GET['G2745_C61250'];
                        } ?>"  name="G2745_C61250" id="G2745_C61250" placeholder="YYYY-MM-DD" nombre="Fecha de realización de llamada">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61252" id="LblG2745_C61252">Tipo de campaña</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61252" id="G2745_C61252">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3697 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9475" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9475c">
                CAMPOS A RECORDAR
            </a>
        </h4>
        
    </div>
    <div id="s_9475c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62562" id="LblG2745_C62562">1. Sondeo venta telefónica</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62562" id="G2745_C62562" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62563" id="LblG2745_C62563">2. Cobro chec no compromete la propiedad</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62563" id="G2745_C62563" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62564" id="LblG2745_C62564">3. Llamada grabada inicio de venta</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62564" id="G2745_C62564" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62565" id="LblG2745_C62565">4. Solicitud de datos</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62565" id="G2745_C62565" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62566" id="LblG2745_C62566">5. Servicios básicos</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62566" id="G2745_C62566" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62567" id="LblG2745_C62567">6. Servicios complementarios</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62567" id="G2745_C62567" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62568" id="LblG2745_C62568">7. Información plan de Colombia (PEI)</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62568" id="G2745_C62568" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62569" id="LblG2745_C62569">8. Cláusulas de la 1 a la 12</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62569" id="G2745_C62569" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62570" id="LblG2745_C62570">9. Cláusulas 13 y 14 (75 años)</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62570" id="G2745_C62570" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62571" id="LblG2745_C62571">10. Cláusulas de la 15 a la 20</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62571" id="G2745_C62571" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62572" id="LblG2745_C62572">11. Numerales anexo post venta</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62572" id="G2745_C62572" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62573" id="LblG2745_C62573">12. Llamada grabada resumen de venta</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62573" id="G2745_C62573" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C61282" id="LblG2745_C61282">13. Habeas data</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C61282" id="G2745_C61282" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62574" id="LblG2745_C62574">14. Autorización chec (CHEC)</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62574" id="G2745_C62574" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62575" id="LblG2745_C62575">15. Libranza telefónica (EMPRESARIAL)</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62575" id="G2745_C62575" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62576" id="LblG2745_C62576">16. Incremento anual</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62576" id="G2745_C62576" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62577" id="LblG2745_C62577">17. Resumen de los inscritos</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62577" id="G2745_C62577" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62578" id="LblG2745_C62578">18. Fechas de protección</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62578" id="G2745_C62578" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62579" id="LblG2745_C62579">19. Resumen características del programa</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62579" id="G2745_C62579" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62580" id="LblG2745_C62580">20. Líneas de atención al cliente y referidos</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62580" id="G2745_C62580" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62581" id="LblG2745_C62581">21. Promociones actuales</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62581" id="G2745_C62581" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2745_C62582" id="LblG2745_C62582">22. Solicitud de servicio con el certificado de defunción</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2745_C62582" id="G2745_C62582" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div hidden class="panel box box-primary" id="s_9513" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9513c">
                AUDITORIA
            </a>
        </h4>
        
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a>
    </div>
    <div id="s_9513c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C62222" id="LblG2745_C62222">TIPIFICACION AUDITORIA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2745_C62222" id="G2745_C62222">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3739 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2745_C62212" id="LblG2745_C62212">Total errores</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2745_C62212'])) {
                            echo $_GET['G2745_C62212'];
                        } ?>"  name="G2745_C62212" id="G2745_C62212" placeholder="Total errores">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C62223" id="LblG2745_C62223">Codigo programa</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C62223" value="<?php if (isset($_GET['G2745_C62223'])) {
                            echo $_GET['G2745_C62223'];
                        } ?>"  name="G2745_C62223"  placeholder="Codigo programa"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C61647" id="LblG2745_C61647">ESTADO_CALIDAD_Q_DY</label>
                        <select disabled class="form-control input-sm select2"  style="width: 100%;" name="G2745_C61647" id="G2745_C61647">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 0 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO DECIMAL -->
                    <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2745_C61646" id="LblG2745_C61646">CALIFICACION_Q_DY</label>
                        <input type="text" class="form-control input-sm Decimal" value="<?php if (isset($_GET['G2745_C61646'])) {
                            echo $_GET['G2745_C61646'];
                        } ?>"  name="G2745_C61646" id="G2745_C61646" placeholder="CALIFICACION_Q_DY">
                    </div>
                    <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2745_C61648" id="LblG2745_C61648">COMENTARIO_CALIDAD_Q_DY</label>
                        <textarea class="form-control input-sm" name="G2745_C61648" id="G2745_C61648"  value="<?php if (isset($_GET['G2745_C61648'])) {
                            echo $_GET['G2745_C61648'];
                        } ?>" placeholder="COMENTARIO_CALIDAD_Q_DY"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2745_C61649" id="LblG2745_C61649">COMENTARIO_AGENTE_Q_DY</label>
                        <textarea class="form-control input-sm" name="G2745_C61649" id="G2745_C61649" readonly value="<?php if (isset($_GET['G2745_C61649'])) {
                            echo $_GET['G2745_C61649'];
                        } ?>" placeholder="COMENTARIO_AGENTE_Q_DY"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2745_C62210" id="LblG2745_C62210">FECHA_AUDITADO_Q_DY</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G2745_C62210" id="G2745_C62210" placeholder="YYYY-MM-DD" nombre="FECHA_AUDITADO_Q_DY">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C62211" id="LblG2745_C62211">NOMBRE_AUDITOR_Q_DY</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C62211" value="<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>" readonly name="G2745_C62211"  placeholder="NOMBRE_AUDITOR_Q_DY"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_9513" style="width: 100%" controls>
                    <source id="btns_9513" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div hidden class="panel box box-primary" id="s_9799" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9799c">
                AUDITORIA INCONSISTENCIAS
            </a>
        </h4>
        
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a>
    </div>
    <div id="s_9799c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2745_C63243" id="LblG2745_C63243">FECHA AUDITORIA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G2745_C63243" id="G2745_C63243" placeholder="YYYY-MM-DD" nombre="FECHA AUDITORIA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C63244" id="LblG2745_C63244">NOMBRE AUDITOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C63244" value="<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>" readonly name="G2745_C63244"  placeholder="NOMBRE AUDITOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2745_C63246" id="LblG2745_C63246">LISTA DE INCONSISTENCIAS</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2745_C63246" value="<?php if (isset($_GET['G2745_C63246'])) {
                            echo $_GET['G2745_C63246'];
                        } ?>"  name="G2745_C63246"  placeholder="LISTA DE INCONSISTENCIAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2745_C63247" id="LblG2745_C63247">TIPIFICACION SECRETARIA COMERCIAL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2745_C63247" id="G2745_C63247">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3807 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_9799" style="width: 100%" controls>
                    <source id="btns_9799" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2745_C53206">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3189;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2745_C53206">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3189;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2745_C53207">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2745_C53208" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2745_C53209" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2745_C53210" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2745/G2745_eventos.js"></script>
<script type="text/javascript" src="formularios/G2745/G2745_extender_funcionalidad.php"></script><?php require_once "G2745_extender_funcionalidad.php";?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
                //JDBD abrimos el modal de correos.
                $(".FinalizarCalificacion").click(function(){
                    $("#calidad").val("1");
                    $("#enviarCalificacion").modal("show");
                });

                $("#sendEmails").click(function(){
                    $("#Save").click();
                    $("#loading").attr("hidden",false);
                    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
                    var cajaCorreos = $("#cajaCorreos").val();

                    if (cajaCorreos == null || cajaCorreos == "") {
                        cajaCorreos = "";
                    }else{
                        cajaCorreos = cajaCorreos.replace(/ /g, "");
                        cajaCorreos = cajaCorreos.replace(/,,,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,/g, ",");

                        if (cajaCorreos[0] == ",") {
                            cajaCorreos = cajaCorreos.substring(1);
                        }

                        if (cajaCorreos[cajaCorreos.length-1] == ",") {
                            cajaCorreos = cajaCorreos.substring(0,cajaCorreos.length-1);
                        }

                        var porciones = cajaCorreos.split(",");

                        for (var i = 0; i < porciones.length; i++) {
                            if (!emailRegex.test(porciones[i])) {
                                porciones.splice(i, 1);
                            }
                        }

                        cajaCorreos = porciones.join(",");
                    }

                    var formData = new FormData($("#FormularioDatos")[0]);
                    formData.append("IdGestion",$("#IdGestion").val());
                    formData.append("IdGuion",<?=$_GET["formulario"];?>);
                    formData.append("IdCal",<?=$_SESSION["IDENTIFICACION"];?>);
                    formData.append("Correos",cajaCorreos);

                    $.ajax({
                        url: "<?=$url_crud;?>?EnviarCalificacion=si",  
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data){
                            alertify.success("Calificacion enviada.");
                        },
                        error : function(){
                            alertify.error("No se pudo enviar la calificacion.");   
                        },
                        complete : function(){
                            $("#loading").attr("hidden",true);
                            $("#CerrarCalificacion").click();
                        }

                    }); 
                    
                });
                
                $("#s_9513").attr("hidden", false);
                $("#s_9799").attr("hidden", false);
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G2745_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G2745_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G2745_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G2745_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G2745_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G2745_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G2745_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G2745_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2745_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G2745_C53213").val("<?php echo date('H:i:s');?>");
            $("#G2745_C62222").val("0").trigger("change");
            $("#G2745_C61647").val("0").trigger("change");
            //JDBD - Damos el valor fecha actual.
            $("#G2745_C62210").val("<?=date("Y-m-d");?>");
            //JDBD - Damos el valor nombre de usuario.
            $("#G2745_C62211").val("<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>");
            //JDBD - Damos el valor fecha actual.
            $("#G2745_C63243").val("<?=date("Y-m-d");?>");
            //JDBD - Damos el valor nombre de usuario.
            $("#G2745_C63244").val("<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>");
            $("#G2745_C63247").val("0").trigger("change");
            
            
        });
    <?php } ?>;
    let arr;
    <?php if(isset($_GET['id_campana_cbx']) && $_GET['id_campana_cbx'] !="" && $_GET['id_campana_cbx'] !=0) :?>
        <?php
        $intHuesped=$mysqli->query("SELECT CAMPAN_ConsInte__PROYEC_b FROM DYALOGOCRM_SISTEMA.CAMPAN WHERE CAMPAN_IdCamCbx__b={$_GET['id_campana_cbx']}");
        if($intHuesped && $intHuesped->num_rows==1){
            $intHuesped=$intHuesped->fetch_object();
            $intHuesped=$intHuesped->CAMPAN_ConsInte__PROYEC_b;
            if($_GET['sentido'] ==1){
                $patron=ObtenerPatron($intHuesped,$_GET['campana_crm']);
            }else{
                $patron=ObtenerPatron($intHuesped);
            }
        } 
        ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php endif; ?>
    <?php if(isset($_SESSION['HUESPED_CRM']) && $_SESSION['HUESPED_CRM']!=0 && $_SESSION['HUESPED_CRM'] !="") {
        $patron=ObtenerPatron($_SESSION['HUESPED_CRM']); ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php } ?>
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2745_C53217").val(item.G2745_C53217); 
                $("#G2745_C53218").val(item.G2745_C53218); 
                $("#G2745_C53219").val(item.G2745_C53219); 
                $("#G2745_C53220").val(item.G2745_C53220); 
                $("#G2745_C53206").val(item.G2745_C53206).trigger("change");  
                $("#G2745_C53207").val(item.G2745_C53207).trigger("change");  
                $("#G2745_C53208").val(item.G2745_C53208); 
                $("#G2745_C53209").val(item.G2745_C53209); 
                $("#G2745_C53210").val(item.G2745_C53210); 
                $("#G2745_C53211").val(item.G2745_C53211); 
                $("#G2745_C53212").val(item.G2745_C53212); 
                $("#G2745_C53213").val(item.G2745_C53213); 
                $("#G2745_C53214").val(item.G2745_C53214);   
                if(item.G2745_C53215 == 1){
                    $("#G2745_C53215").attr('checked', true);
                }    
                if(item.G2745_C53216 == 1){
                    $("#G2745_C53216").attr('checked', true);
                }  
                $("#G2745_C61248").val(item.G2745_C61248).trigger("change");  
                $("#G2745_C61249").val(item.G2745_C61249).trigger("change");  
                $("#G2745_C61250").val(item.G2745_C61250); 
                $("#G2745_C61252").val(item.G2745_C61252).trigger("change");    
                if(item.G2745_C62562 == 1){
                    $("#G2745_C62562").attr('checked', true);
                }    
                if(item.G2745_C62563 == 1){
                    $("#G2745_C62563").attr('checked', true);
                }    
                if(item.G2745_C62564 == 1){
                    $("#G2745_C62564").attr('checked', true);
                }    
                if(item.G2745_C62565 == 1){
                    $("#G2745_C62565").attr('checked', true);
                }    
                if(item.G2745_C62566 == 1){
                    $("#G2745_C62566").attr('checked', true);
                }    
                if(item.G2745_C62567 == 1){
                    $("#G2745_C62567").attr('checked', true);
                }    
                if(item.G2745_C62568 == 1){
                    $("#G2745_C62568").attr('checked', true);
                }    
                if(item.G2745_C62569 == 1){
                    $("#G2745_C62569").attr('checked', true);
                }    
                if(item.G2745_C62570 == 1){
                    $("#G2745_C62570").attr('checked', true);
                }    
                if(item.G2745_C62571 == 1){
                    $("#G2745_C62571").attr('checked', true);
                }    
                if(item.G2745_C62572 == 1){
                    $("#G2745_C62572").attr('checked', true);
                }    
                if(item.G2745_C62573 == 1){
                    $("#G2745_C62573").attr('checked', true);
                }    
                if(item.G2745_C61282 == 1){
                    $("#G2745_C61282").attr('checked', true);
                }    
                if(item.G2745_C62574 == 1){
                    $("#G2745_C62574").attr('checked', true);
                }    
                if(item.G2745_C62575 == 1){
                    $("#G2745_C62575").attr('checked', true);
                }    
                if(item.G2745_C62576 == 1){
                    $("#G2745_C62576").attr('checked', true);
                }    
                if(item.G2745_C62577 == 1){
                    $("#G2745_C62577").attr('checked', true);
                }    
                if(item.G2745_C62578 == 1){
                    $("#G2745_C62578").attr('checked', true);
                }    
                if(item.G2745_C62579 == 1){
                    $("#G2745_C62579").attr('checked', true);
                }    
                if(item.G2745_C62580 == 1){
                    $("#G2745_C62580").attr('checked', true);
                }    
                if(item.G2745_C62581 == 1){
                    $("#G2745_C62581").attr('checked', true);
                }    
                if(item.G2745_C62582 == 1){
                    $("#G2745_C62582").attr('checked', true);
                }  
                $("#G2745_C62222").val(item.G2745_C62222).trigger("change");  
                $("#G2745_C62212").val(item.G2745_C62212); 
                $("#G2745_C62223").val(item.G2745_C62223); 
                $("#G2745_C61647").val(item.G2745_C61647).trigger("change");  
                $("#G2745_C61646").val(item.G2745_C61646); 
                $("#G2745_C61648").val(item.G2745_C61648); 
                $("#G2745_C61649").val(item.G2745_C61649); 
                $("#G2745_C62210").val(item.G2745_C62210); 
                $("#G2745_C62211").val(item.G2745_C62211); 
                $("#G2745_C63243").val(item.G2745_C63243); 
                $("#G2745_C63244").val(item.G2745_C63244); 
                $("#G2745_C63246").val(item.G2745_C63246); 
                $("#G2745_C63247").val(item.G2745_C63247).trigger("change"); 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2745_C61248").select2();

    $("#G2745_C61249").select2();

    $("#G2745_C61252").select2();

    $("#G2745_C62222").select2();

    $("#G2745_C61647").select2();

    $("#G2745_C63247").select2();
        //datepickers
        

        $("#G2745_C53208").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2745_C61250").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2745_C53209").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G2745_C62212").numeric();
                

        //Validaciones numeros Decimales
        

        $("#G2745_C61646").numeric({ decimal : ".",  negative : false, scale: 4 });
                

        /* Si son d formulas */
        


        //function para Total errores 

    $("#G2745_C62212").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = 100-Number($("#G2745_C62212").val());

        $("#G2745_C61646").val(intCalculo_t.toFixed(2));

    });

        //Si tienen dependencias

        


    //function para Nombre de la empresa que realizo la venta 

    $("#G2745_C61248").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Nombre de la empresa para la que se realiza la venta 

    $("#G2745_C61249").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo de campaña 

    $("#G2745_C61252").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPIFICACION AUDITORIA 

    $("#G2745_C62222").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_CALIDAD_Q_DY 

    $("#G2745_C61647").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPIFICACION SECRETARIA COMERCIAL 

    $("#G2745_C63247").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2745_C53217").val(item.G2745_C53217);
 
                                                $("#G2745_C53218").val(item.G2745_C53218);
 
                                                $("#G2745_C53219").val(item.G2745_C53219);
 
                                                $("#G2745_C53220").val(item.G2745_C53220);
 
                    $("#G2745_C53206").val(item.G2745_C53206).trigger("change"); 
 
                    $("#G2745_C53207").val(item.G2745_C53207).trigger("change"); 
 
                                                $("#G2745_C53208").val(item.G2745_C53208);
 
                                                $("#G2745_C53209").val(item.G2745_C53209);
 
                                                $("#G2745_C53210").val(item.G2745_C53210);
 
                                                $("#G2745_C53211").val(item.G2745_C53211);
 
                                                $("#G2745_C53212").val(item.G2745_C53212);
 
                                                $("#G2745_C53213").val(item.G2745_C53213);
 
                                                $("#G2745_C53214").val(item.G2745_C53214);
      
                                                if(item.G2745_C53215 == 1){
                                                   $("#G2745_C53215").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C53216 == 1){
                                                   $("#G2745_C53216").attr('checked', true);
                                                } 
 
                    $("#G2745_C61248").val(item.G2745_C61248).trigger("change"); 
 
                    $("#G2745_C61249").val(item.G2745_C61249).trigger("change"); 
 
                                                $("#G2745_C61250").val(item.G2745_C61250);
 
                    $("#G2745_C61252").val(item.G2745_C61252).trigger("change"); 
      
                                                if(item.G2745_C62562 == 1){
                                                   $("#G2745_C62562").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62563 == 1){
                                                   $("#G2745_C62563").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62564 == 1){
                                                   $("#G2745_C62564").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62565 == 1){
                                                   $("#G2745_C62565").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62566 == 1){
                                                   $("#G2745_C62566").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62567 == 1){
                                                   $("#G2745_C62567").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62568 == 1){
                                                   $("#G2745_C62568").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62569 == 1){
                                                   $("#G2745_C62569").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62570 == 1){
                                                   $("#G2745_C62570").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62571 == 1){
                                                   $("#G2745_C62571").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62572 == 1){
                                                   $("#G2745_C62572").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62573 == 1){
                                                   $("#G2745_C62573").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C61282 == 1){
                                                   $("#G2745_C61282").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62574 == 1){
                                                   $("#G2745_C62574").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62575 == 1){
                                                   $("#G2745_C62575").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62576 == 1){
                                                   $("#G2745_C62576").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62577 == 1){
                                                   $("#G2745_C62577").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62578 == 1){
                                                   $("#G2745_C62578").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62579 == 1){
                                                   $("#G2745_C62579").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62580 == 1){
                                                   $("#G2745_C62580").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62581 == 1){
                                                   $("#G2745_C62581").attr('checked', true);
                                                } 
      
                                                if(item.G2745_C62582 == 1){
                                                   $("#G2745_C62582").attr('checked', true);
                                                } 
 
                    $("#G2745_C62222").val(item.G2745_C62222).trigger("change"); 
 
                                                $("#G2745_C62212").val(item.G2745_C62212);
 
                                                $("#G2745_C62223").val(item.G2745_C62223);
 
                    $("#G2745_C61647").val(item.G2745_C61647).trigger("change"); 
 
                                                $("#G2745_C61646").val(item.G2745_C61646);
 
                                                $("#G2745_C61648").val(item.G2745_C61648);
 
                                                $("#G2745_C61649").val(item.G2745_C61649);
 
                                                $("#G2745_C62210").val(item.G2745_C62210);
 
                                                $("#G2745_C62211").val(item.G2745_C62211);
 
                                                $("#G2745_C63243").val(item.G2745_C63243);
 
                                                $("#G2745_C63244").val(item.G2745_C63244);
 
                                                $("#G2745_C63246").val(item.G2745_C63246);
 
                    $("#G2745_C63247").val(item.G2745_C63247).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G2745_C61248").val()==0 || $("#G2745_C61248").val() == null || $("#G2745_C61248").val() == -1) && $("#G2745_C61248").prop("disabled") == false){
                alertify.error('Nombre de la empresa que realizo la venta debe ser diligenciado');
                $("#G2745_C61248").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','NOMBRE','CEDULA','TELEFONO PERSONAL','CIUDAD','Agente','Fecha','Hora','Campaña','Nombre de la empresa que realizo la venta','Nombre de la empresa para la que se realiza la venta','Fecha de realización de llamada','Tipo de campaña','1. Sondeo venta telefónica','2. Cobro chec no compromete la propiedad','3. Llamada grabada inicio de venta','4. Solicitud de datos','5. Servicios básicos','6. Servicios complementarios','7. Información plan de Colombia (PEI)','8. Cláusulas de la 1 a la 12','9. Cláusulas 13 y 14 (75 años)','10. Cláusulas de la 15 a la 20','11. Numerales anexo post venta','12. Llamada grabada resumen de venta','13. Habeas data','14. Autorización chec (CHEC)','15. Libranza telefónica (EMPRESARIAL)','16. Incremento anual','17. Resumen de los inscritos','18. Fechas de protección','19. Resumen características del programa','20. Líneas de atención al cliente y referidos','21. Promociones actuales','22. Solicitud de servicio con el certificado de defunción','TIPIFICACION AUDITORIA','Total errores','Codigo programa','ESTADO_CALIDAD_Q_DY','CALIFICACION_Q_DY','COMENTARIO_CALIDAD_Q_DY','COMENTARIO_AGENTE_Q_DY','FECHA_AUDITADO_Q_DY','NOMBRE_AUDITOR_Q_DY','FECHA AUDITORIA','NOMBRE AUDITOR','LISTA DE INCONSISTENCIAS','TIPIFICACION SECRETARIA COMERCIAL'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2745_C53217', 
                        index: 'G2745_C53217', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53218', 
                        index: 'G2745_C53218', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53219', 
                        index: 'G2745_C53219', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53220', 
                        index: 'G2745_C53220', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53211', 
                        index: 'G2745_C53211', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53212', 
                        index: 'G2745_C53212', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53213', 
                        index: 'G2745_C53213', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C53214', 
                        index: 'G2745_C53214', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C61248', 
                        index:'G2745_C61248', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3699&campo=G2745_C61248'
                        }
                    }

                    ,
                    { 
                        name:'G2745_C61249', 
                        index:'G2745_C61249', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3699&campo=G2745_C61249'
                        }
                    }

                    ,
                    {  
                        name:'G2745_C61250', 
                        index:'G2745_C61250', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2745_C61252', 
                        index:'G2745_C61252', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3697&campo=G2745_C61252'
                        }
                    }

                    ,
                    { 
                        name:'G2745_C62562', 
                        index:'G2745_C62562', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62563', 
                        index:'G2745_C62563', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62564', 
                        index:'G2745_C62564', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62565', 
                        index:'G2745_C62565', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62566', 
                        index:'G2745_C62566', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62567', 
                        index:'G2745_C62567', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62568', 
                        index:'G2745_C62568', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62569', 
                        index:'G2745_C62569', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62570', 
                        index:'G2745_C62570', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62571', 
                        index:'G2745_C62571', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62572', 
                        index:'G2745_C62572', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62573', 
                        index:'G2745_C62573', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C61282', 
                        index:'G2745_C61282', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62574', 
                        index:'G2745_C62574', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62575', 
                        index:'G2745_C62575', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62576', 
                        index:'G2745_C62576', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62577', 
                        index:'G2745_C62577', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62578', 
                        index:'G2745_C62578', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62579', 
                        index:'G2745_C62579', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62580', 
                        index:'G2745_C62580', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62581', 
                        index:'G2745_C62581', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62582', 
                        index:'G2745_C62582', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62222', 
                        index:'G2745_C62222', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3739&campo=G2745_C62222'
                        }
                    }
 
                    ,
                    {  
                        name:'G2745_C62212', 
                        index:'G2745_C62212', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C62223', 
                        index: 'G2745_C62223', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C61647', 
                        index:'G2745_C61647', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=0&campo=G2745_C61647'
                        }
                    }

                    ,
                    {  
                        name:'G2745_C61646', 
                        index:'G2745_C61646', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2745_C61648', 
                        index:'G2745_C61648', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C61649', 
                        index:'G2745_C61649', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2745_C62210', 
                        index:'G2745_C62210', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2745_C62211', 
                        index: 'G2745_C62211', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2745_C63243', 
                        index:'G2745_C63243', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2745_C63244', 
                        index: 'G2745_C63244', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C63246', 
                        index: 'G2745_C63246', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2745_C63247', 
                        index:'G2745_C63247', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3807&campo=G2745_C63247'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2745_C53218',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2745_C53217").val(item.G2745_C53217);

                        $("#G2745_C53218").val(item.G2745_C53218);

                        $("#G2745_C53219").val(item.G2745_C53219);

                        $("#G2745_C53220").val(item.G2745_C53220);
 
                    $("#G2745_C53206").val(item.G2745_C53206).trigger("change"); 
 
                    $("#G2745_C53207").val(item.G2745_C53207).trigger("change"); 

                        $("#G2745_C53208").val(item.G2745_C53208);

                        $("#G2745_C53209").val(item.G2745_C53209);

                        $("#G2745_C53210").val(item.G2745_C53210);

                        $("#G2745_C53211").val(item.G2745_C53211);

                        $("#G2745_C53212").val(item.G2745_C53212);

                        $("#G2745_C53213").val(item.G2745_C53213);

                        $("#G2745_C53214").val(item.G2745_C53214);
    
                        if(item.G2745_C53215 == 1){
                           $("#G2745_C53215").attr('checked', true);
                        } 
    
                        if(item.G2745_C53216 == 1){
                           $("#G2745_C53216").attr('checked', true);
                        } 
 
                    $("#G2745_C61248").val(item.G2745_C61248).trigger("change"); 
 
                    $("#G2745_C61249").val(item.G2745_C61249).trigger("change"); 

                        $("#G2745_C61250").val(item.G2745_C61250);
 
                    $("#G2745_C61252").val(item.G2745_C61252).trigger("change"); 
    
                        if(item.G2745_C62562 == 1){
                           $("#G2745_C62562").attr('checked', true);
                        } 
    
                        if(item.G2745_C62563 == 1){
                           $("#G2745_C62563").attr('checked', true);
                        } 
    
                        if(item.G2745_C62564 == 1){
                           $("#G2745_C62564").attr('checked', true);
                        } 
    
                        if(item.G2745_C62565 == 1){
                           $("#G2745_C62565").attr('checked', true);
                        } 
    
                        if(item.G2745_C62566 == 1){
                           $("#G2745_C62566").attr('checked', true);
                        } 
    
                        if(item.G2745_C62567 == 1){
                           $("#G2745_C62567").attr('checked', true);
                        } 
    
                        if(item.G2745_C62568 == 1){
                           $("#G2745_C62568").attr('checked', true);
                        } 
    
                        if(item.G2745_C62569 == 1){
                           $("#G2745_C62569").attr('checked', true);
                        } 
    
                        if(item.G2745_C62570 == 1){
                           $("#G2745_C62570").attr('checked', true);
                        } 
    
                        if(item.G2745_C62571 == 1){
                           $("#G2745_C62571").attr('checked', true);
                        } 
    
                        if(item.G2745_C62572 == 1){
                           $("#G2745_C62572").attr('checked', true);
                        } 
    
                        if(item.G2745_C62573 == 1){
                           $("#G2745_C62573").attr('checked', true);
                        } 
    
                        if(item.G2745_C61282 == 1){
                           $("#G2745_C61282").attr('checked', true);
                        } 
    
                        if(item.G2745_C62574 == 1){
                           $("#G2745_C62574").attr('checked', true);
                        } 
    
                        if(item.G2745_C62575 == 1){
                           $("#G2745_C62575").attr('checked', true);
                        } 
    
                        if(item.G2745_C62576 == 1){
                           $("#G2745_C62576").attr('checked', true);
                        } 
    
                        if(item.G2745_C62577 == 1){
                           $("#G2745_C62577").attr('checked', true);
                        } 
    
                        if(item.G2745_C62578 == 1){
                           $("#G2745_C62578").attr('checked', true);
                        } 
    
                        if(item.G2745_C62579 == 1){
                           $("#G2745_C62579").attr('checked', true);
                        } 
    
                        if(item.G2745_C62580 == 1){
                           $("#G2745_C62580").attr('checked', true);
                        } 
    
                        if(item.G2745_C62581 == 1){
                           $("#G2745_C62581").attr('checked', true);
                        } 
    
                        if(item.G2745_C62582 == 1){
                           $("#G2745_C62582").attr('checked', true);
                        } 
 
                    $("#G2745_C62222").val(item.G2745_C62222).trigger("change"); 

                        $("#G2745_C62212").val(item.G2745_C62212);

                        $("#G2745_C62223").val(item.G2745_C62223);
 
                    $("#G2745_C61647").val(item.G2745_C61647).trigger("change"); 

                        $("#G2745_C61646").val(item.G2745_C61646);

                        $("#G2745_C61648").val(item.G2745_C61648);

                        $("#G2745_C61649").val(item.G2745_C61649);

                        $("#G2745_C62210").val(item.G2745_C62210);

                        $("#G2745_C62211").val(item.G2745_C62211);

                        $("#G2745_C63243").val(item.G2745_C63243);

                        $("#G2745_C63244").val(item.G2745_C63244);

                        $("#G2745_C63246").val(item.G2745_C63246);
 
                    $("#G2745_C63247").val(item.G2745_C63247).trigger("change"); 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_9513");
                        $("#btns_9513").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_9799");
                        $("#btns_9799").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
