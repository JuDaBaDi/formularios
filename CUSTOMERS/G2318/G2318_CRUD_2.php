<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2318_ConsInte__b, G2318_FechaInsercion , G2318_Usuario ,  G2318_CodigoMiembro  , G2318_PoblacionOrigen , G2318_EstadoDiligenciamiento ,  G2318_IdLlamada , G2318_C45375 as principal ,G2318_C45375,G2318_C45376,G2318_C47960,G2318_C45364,G2318_C45365,G2318_C45366,G2318_C45367,G2318_C45368,G2318_C45369,G2318_C45370,G2318_C45371,G2318_C45372 FROM '.$BaseDatos.'.G2318 WHERE G2318_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2318_C45375'] = $key->G2318_C45375;

                $datos[$i]['G2318_C45376'] = $key->G2318_C45376;

                $datos[$i]['G2318_C47960'] = $key->G2318_C47960;

                $datos[$i]['G2318_C45364'] = $key->G2318_C45364;

                $datos[$i]['G2318_C45365'] = $key->G2318_C45365;

                $datos[$i]['G2318_C45366'] = explode(' ', $key->G2318_C45366)[0];
  
                $hora = '';
                if(!is_null($key->G2318_C45367)){
                    $hora = explode(' ', $key->G2318_C45367)[1];
                }

                $datos[$i]['G2318_C45367'] = $hora;

                $datos[$i]['G2318_C45368'] = $key->G2318_C45368;

                $datos[$i]['G2318_C45369'] = $key->G2318_C45369;

                $datos[$i]['G2318_C45370'] = $key->G2318_C45370;

                $datos[$i]['G2318_C45371'] = $key->G2318_C45371;

                $datos[$i]['G2318_C45372'] = $key->G2318_C45372;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2318";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2318_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2318_ConsInte__b as id,  G2318_C45375 as camp1 , G2318_C45376 as camp2 
                     FROM ".$BaseDatos.".G2318  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2318_ConsInte__b as id,  G2318_C45375 as camp1 , G2318_C45376 as camp2  
                    FROM ".$BaseDatos.".G2318  JOIN ".$BaseDatos.".G2318_M".$_POST['muestra']." ON G2318_ConsInte__b = G2318_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2318_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2318_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2318_C45375 LIKE '%".$B."%' OR G2318_C45376 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2318_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2318");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2318_ConsInte__b, G2318_FechaInsercion , G2318_Usuario ,  G2318_CodigoMiembro  , G2318_PoblacionOrigen , G2318_EstadoDiligenciamiento ,  G2318_IdLlamada , G2318_C45375 as principal ,G2318_C45375,G2318_C45376,G2318_C47960, a.LISOPC_Nombre____b as G2318_C45364, b.LISOPC_Nombre____b as G2318_C45365,G2318_C45366,G2318_C45367,G2318_C45368,G2318_C45369,G2318_C45370,G2318_C45371,G2318_C45372 FROM '.$BaseDatos.'.G2318 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2318_C45364 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2318_C45365';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2318_C45367)){
                    $hora_a = explode(' ', $fila->G2318_C45367)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2318_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2318_ConsInte__b , ($fila->G2318_C45375) , ($fila->G2318_C45376) , ($fila->G2318_C47960) , ($fila->G2318_C45364) , ($fila->G2318_C45365) , explode(' ', $fila->G2318_C45366)[0] , $hora_a , ($fila->G2318_C45368) , ($fila->G2318_C45369) , ($fila->G2318_C45370) , ($fila->G2318_C45371) , ($fila->G2318_C45372) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2318 WHERE G2318_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2318";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2318_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2318_ConsInte__b as id,  G2318_C45375 as camp1 , G2318_C45376 as camp2  FROM '.$BaseDatos.'.G2318 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2318_ConsInte__b as id,  G2318_C45375 as camp1 , G2318_C45376 as camp2  
                    FROM ".$BaseDatos.".G2318  JOIN ".$BaseDatos.".G2318_M".$_POST['muestra']." ON G2318_ConsInte__b = G2318_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2318_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2318_C45375 LIKE "%'.$B.'%" OR G2318_C45376 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2318_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2318 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2318(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2318_C45375"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45375 = '".$_POST["G2318_C45375"]."'";
                $LsqlI .= $separador."G2318_C45375";
                $LsqlV .= $separador."'".$_POST["G2318_C45375"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45376 = '".$_POST["G2318_C45376"]."'";
                $LsqlI .= $separador."G2318_C45376";
                $LsqlV .= $separador."'".$_POST["G2318_C45376"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C47960"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C47960 = '".$_POST["G2318_C47960"]."'";
                $LsqlI .= $separador."G2318_C47960";
                $LsqlV .= $separador."'".$_POST["G2318_C47960"]."'";
                $validar = 1;
            }
             
 
            $G2318_C45364 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2318_C45364 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2318_C45364 = ".$G2318_C45364;
                    $LsqlI .= $separador." G2318_C45364";
                    $LsqlV .= $separador.$G2318_C45364;
                    $validar = 1;

                    
                }
            }
 
            $G2318_C45365 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2318_C45365 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2318_C45365 = ".$G2318_C45365;
                    $LsqlI .= $separador." G2318_C45365";
                    $LsqlV .= $separador.$G2318_C45365;
                    $validar = 1;
                }
            }
 
            $G2318_C45366 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2318_C45366 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2318_C45366 = ".$G2318_C45366;
                    $LsqlI .= $separador." G2318_C45366";
                    $LsqlV .= $separador.$G2318_C45366;
                    $validar = 1;
                }
            }
 
            $G2318_C45367 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2318_C45367 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2318_C45367 = ".$G2318_C45367;
                    $LsqlI .= $separador." G2318_C45367";
                    $LsqlV .= $separador.$G2318_C45367;
                    $validar = 1;
                }
            }
 
            $G2318_C45368 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2318_C45368 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2318_C45368 = ".$G2318_C45368;
                    $LsqlI .= $separador." G2318_C45368";
                    $LsqlV .= $separador.$G2318_C45368;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2318_C45369"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45369 = '".$_POST["G2318_C45369"]."'";
                $LsqlI .= $separador."G2318_C45369";
                $LsqlV .= $separador."'".$_POST["G2318_C45369"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45370"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45370 = '".$_POST["G2318_C45370"]."'";
                $LsqlI .= $separador."G2318_C45370";
                $LsqlV .= $separador."'".$_POST["G2318_C45370"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45371"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45371 = '".$_POST["G2318_C45371"]."'";
                $LsqlI .= $separador."G2318_C45371";
                $LsqlV .= $separador."'".$_POST["G2318_C45371"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45372"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45372 = '".$_POST["G2318_C45372"]."'";
                $LsqlI .= $separador."G2318_C45372";
                $LsqlV .= $separador."'".$_POST["G2318_C45372"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45373"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45373 = '".$_POST["G2318_C45373"]."'";
                $LsqlI .= $separador."G2318_C45373";
                $LsqlV .= $separador."'".$_POST["G2318_C45373"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45374 = '".$_POST["G2318_C45374"]."'";
                $LsqlI .= $separador."G2318_C45374";
                $LsqlV .= $separador."'".$_POST["G2318_C45374"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45377 = '".$_POST["G2318_C45377"]."'";
                $LsqlI .= $separador."G2318_C45377";
                $LsqlV .= $separador."'".$_POST["G2318_C45377"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45378"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45378 = '".$_POST["G2318_C45378"]."'";
                $LsqlI .= $separador."G2318_C45378";
                $LsqlV .= $separador."'".$_POST["G2318_C45378"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45379"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45379 = '".$_POST["G2318_C45379"]."'";
                $LsqlI .= $separador."G2318_C45379";
                $LsqlV .= $separador."'".$_POST["G2318_C45379"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45380"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45380 = '".$_POST["G2318_C45380"]."'";
                $LsqlI .= $separador."G2318_C45380";
                $LsqlV .= $separador."'".$_POST["G2318_C45380"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45381"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45381 = '".$_POST["G2318_C45381"]."'";
                $LsqlI .= $separador."G2318_C45381";
                $LsqlV .= $separador."'".$_POST["G2318_C45381"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45382"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45382 = '".$_POST["G2318_C45382"]."'";
                $LsqlI .= $separador."G2318_C45382";
                $LsqlV .= $separador."'".$_POST["G2318_C45382"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45383"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45383 = '".$_POST["G2318_C45383"]."'";
                $LsqlI .= $separador."G2318_C45383";
                $LsqlV .= $separador."'".$_POST["G2318_C45383"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45384"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45384 = '".$_POST["G2318_C45384"]."'";
                $LsqlI .= $separador."G2318_C45384";
                $LsqlV .= $separador."'".$_POST["G2318_C45384"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45385"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45385 = '".$_POST["G2318_C45385"]."'";
                $LsqlI .= $separador."G2318_C45385";
                $LsqlV .= $separador."'".$_POST["G2318_C45385"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45386"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45386 = '".$_POST["G2318_C45386"]."'";
                $LsqlI .= $separador."G2318_C45386";
                $LsqlV .= $separador."'".$_POST["G2318_C45386"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45387"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45387 = '".$_POST["G2318_C45387"]."'";
                $LsqlI .= $separador."G2318_C45387";
                $LsqlV .= $separador."'".$_POST["G2318_C45387"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45388"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45388 = '".$_POST["G2318_C45388"]."'";
                $LsqlI .= $separador."G2318_C45388";
                $LsqlV .= $separador."'".$_POST["G2318_C45388"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45389"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45389 = '".$_POST["G2318_C45389"]."'";
                $LsqlI .= $separador."G2318_C45389";
                $LsqlV .= $separador."'".$_POST["G2318_C45389"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45390"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45390 = '".$_POST["G2318_C45390"]."'";
                $LsqlI .= $separador."G2318_C45390";
                $LsqlV .= $separador."'".$_POST["G2318_C45390"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45391"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45391 = '".$_POST["G2318_C45391"]."'";
                $LsqlI .= $separador."G2318_C45391";
                $LsqlV .= $separador."'".$_POST["G2318_C45391"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45392"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45392 = '".$_POST["G2318_C45392"]."'";
                $LsqlI .= $separador."G2318_C45392";
                $LsqlV .= $separador."'".$_POST["G2318_C45392"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45393"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45393 = '".$_POST["G2318_C45393"]."'";
                $LsqlI .= $separador."G2318_C45393";
                $LsqlV .= $separador."'".$_POST["G2318_C45393"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45394"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45394 = '".$_POST["G2318_C45394"]."'";
                $LsqlI .= $separador."G2318_C45394";
                $LsqlV .= $separador."'".$_POST["G2318_C45394"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45395"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45395 = '".$_POST["G2318_C45395"]."'";
                $LsqlI .= $separador."G2318_C45395";
                $LsqlV .= $separador."'".$_POST["G2318_C45395"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45396"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45396 = '".$_POST["G2318_C45396"]."'";
                $LsqlI .= $separador."G2318_C45396";
                $LsqlV .= $separador."'".$_POST["G2318_C45396"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45397"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45397 = '".$_POST["G2318_C45397"]."'";
                $LsqlI .= $separador."G2318_C45397";
                $LsqlV .= $separador."'".$_POST["G2318_C45397"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45398"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45398 = '".$_POST["G2318_C45398"]."'";
                $LsqlI .= $separador."G2318_C45398";
                $LsqlV .= $separador."'".$_POST["G2318_C45398"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45399"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45399 = '".$_POST["G2318_C45399"]."'";
                $LsqlI .= $separador."G2318_C45399";
                $LsqlV .= $separador."'".$_POST["G2318_C45399"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45400"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45400 = '".$_POST["G2318_C45400"]."'";
                $LsqlI .= $separador."G2318_C45400";
                $LsqlV .= $separador."'".$_POST["G2318_C45400"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45401"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45401 = '".$_POST["G2318_C45401"]."'";
                $LsqlI .= $separador."G2318_C45401";
                $LsqlV .= $separador."'".$_POST["G2318_C45401"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45402"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45402 = '".$_POST["G2318_C45402"]."'";
                $LsqlI .= $separador."G2318_C45402";
                $LsqlV .= $separador."'".$_POST["G2318_C45402"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45403"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45403 = '".$_POST["G2318_C45403"]."'";
                $LsqlI .= $separador."G2318_C45403";
                $LsqlV .= $separador."'".$_POST["G2318_C45403"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45404"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45404 = '".$_POST["G2318_C45404"]."'";
                $LsqlI .= $separador."G2318_C45404";
                $LsqlV .= $separador."'".$_POST["G2318_C45404"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45405"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45405 = '".$_POST["G2318_C45405"]."'";
                $LsqlI .= $separador."G2318_C45405";
                $LsqlV .= $separador."'".$_POST["G2318_C45405"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45406"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45406 = '".$_POST["G2318_C45406"]."'";
                $LsqlI .= $separador."G2318_C45406";
                $LsqlV .= $separador."'".$_POST["G2318_C45406"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45407"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45407 = '".$_POST["G2318_C45407"]."'";
                $LsqlI .= $separador."G2318_C45407";
                $LsqlV .= $separador."'".$_POST["G2318_C45407"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45408"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45408 = '".$_POST["G2318_C45408"]."'";
                $LsqlI .= $separador."G2318_C45408";
                $LsqlV .= $separador."'".$_POST["G2318_C45408"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45409"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45409 = '".$_POST["G2318_C45409"]."'";
                $LsqlI .= $separador."G2318_C45409";
                $LsqlV .= $separador."'".$_POST["G2318_C45409"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45410"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45410 = '".$_POST["G2318_C45410"]."'";
                $LsqlI .= $separador."G2318_C45410";
                $LsqlV .= $separador."'".$_POST["G2318_C45410"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45411"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45411 = '".$_POST["G2318_C45411"]."'";
                $LsqlI .= $separador."G2318_C45411";
                $LsqlV .= $separador."'".$_POST["G2318_C45411"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45412"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45412 = '".$_POST["G2318_C45412"]."'";
                $LsqlI .= $separador."G2318_C45412";
                $LsqlV .= $separador."'".$_POST["G2318_C45412"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45413"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45413 = '".$_POST["G2318_C45413"]."'";
                $LsqlI .= $separador."G2318_C45413";
                $LsqlV .= $separador."'".$_POST["G2318_C45413"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45414"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45414 = '".$_POST["G2318_C45414"]."'";
                $LsqlI .= $separador."G2318_C45414";
                $LsqlV .= $separador."'".$_POST["G2318_C45414"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45415"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45415 = '".$_POST["G2318_C45415"]."'";
                $LsqlI .= $separador."G2318_C45415";
                $LsqlV .= $separador."'".$_POST["G2318_C45415"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45416"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45416 = '".$_POST["G2318_C45416"]."'";
                $LsqlI .= $separador."G2318_C45416";
                $LsqlV .= $separador."'".$_POST["G2318_C45416"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45417"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45417 = '".$_POST["G2318_C45417"]."'";
                $LsqlI .= $separador."G2318_C45417";
                $LsqlV .= $separador."'".$_POST["G2318_C45417"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45418"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45418 = '".$_POST["G2318_C45418"]."'";
                $LsqlI .= $separador."G2318_C45418";
                $LsqlV .= $separador."'".$_POST["G2318_C45418"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45419"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45419 = '".$_POST["G2318_C45419"]."'";
                $LsqlI .= $separador."G2318_C45419";
                $LsqlV .= $separador."'".$_POST["G2318_C45419"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45420"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45420 = '".$_POST["G2318_C45420"]."'";
                $LsqlI .= $separador."G2318_C45420";
                $LsqlV .= $separador."'".$_POST["G2318_C45420"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45421"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45421 = '".$_POST["G2318_C45421"]."'";
                $LsqlI .= $separador."G2318_C45421";
                $LsqlV .= $separador."'".$_POST["G2318_C45421"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45422"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45422 = '".$_POST["G2318_C45422"]."'";
                $LsqlI .= $separador."G2318_C45422";
                $LsqlV .= $separador."'".$_POST["G2318_C45422"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45423"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45423 = '".$_POST["G2318_C45423"]."'";
                $LsqlI .= $separador."G2318_C45423";
                $LsqlV .= $separador."'".$_POST["G2318_C45423"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45424"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45424 = '".$_POST["G2318_C45424"]."'";
                $LsqlI .= $separador."G2318_C45424";
                $LsqlV .= $separador."'".$_POST["G2318_C45424"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45425"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45425 = '".$_POST["G2318_C45425"]."'";
                $LsqlI .= $separador."G2318_C45425";
                $LsqlV .= $separador."'".$_POST["G2318_C45425"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45426"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45426 = '".$_POST["G2318_C45426"]."'";
                $LsqlI .= $separador."G2318_C45426";
                $LsqlV .= $separador."'".$_POST["G2318_C45426"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45427"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45427 = '".$_POST["G2318_C45427"]."'";
                $LsqlI .= $separador."G2318_C45427";
                $LsqlV .= $separador."'".$_POST["G2318_C45427"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45428"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45428 = '".$_POST["G2318_C45428"]."'";
                $LsqlI .= $separador."G2318_C45428";
                $LsqlV .= $separador."'".$_POST["G2318_C45428"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45429"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45429 = '".$_POST["G2318_C45429"]."'";
                $LsqlI .= $separador."G2318_C45429";
                $LsqlV .= $separador."'".$_POST["G2318_C45429"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45430"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45430 = '".$_POST["G2318_C45430"]."'";
                $LsqlI .= $separador."G2318_C45430";
                $LsqlV .= $separador."'".$_POST["G2318_C45430"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45431"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45431 = '".$_POST["G2318_C45431"]."'";
                $LsqlI .= $separador."G2318_C45431";
                $LsqlV .= $separador."'".$_POST["G2318_C45431"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45432"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45432 = '".$_POST["G2318_C45432"]."'";
                $LsqlI .= $separador."G2318_C45432";
                $LsqlV .= $separador."'".$_POST["G2318_C45432"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45433"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45433 = '".$_POST["G2318_C45433"]."'";
                $LsqlI .= $separador."G2318_C45433";
                $LsqlV .= $separador."'".$_POST["G2318_C45433"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45434"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45434 = '".$_POST["G2318_C45434"]."'";
                $LsqlI .= $separador."G2318_C45434";
                $LsqlV .= $separador."'".$_POST["G2318_C45434"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45435"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45435 = '".$_POST["G2318_C45435"]."'";
                $LsqlI .= $separador."G2318_C45435";
                $LsqlV .= $separador."'".$_POST["G2318_C45435"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2318_C45436"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_C45436 = '".$_POST["G2318_C45436"]."'";
                $LsqlI .= $separador."G2318_C45436";
                $LsqlV .= $separador."'".$_POST["G2318_C45436"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2318_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2318_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2318_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2318_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2318_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2318_Usuario , G2318_FechaInsercion, G2318_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2318_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2318 WHERE G2318_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;

                        $strSQLUpdateM_t = "";

                        if (isset($_POST["TipNoEF"]) && ($_POST["TipNoEF"] == 2 || $_POST["TipNoEF"] == "2")) {

                            $strSQLUpdateM_t = "UPDATE ".$BaseDatos.".G2321_M1620 SET G2321_M1620_ConIntUsu_b = ".$_GET['usuario']." WHERE G2321_M1620_CoInMiPo__b = ".$_GET['CodigoMiembro'];

                            $mysqli->query($strSQLUpdateM_t);

                        }

                        echo $UltimoID;
                        
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
