<?php 
    /*
        Document   : index
        Created on : 2020-06-08 15:43:41
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjMxOA==  
    */
    $url_crud =  "formularios/G2318/G2318_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G2318/G2318_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2318_C45375" id="LblG2318_C45375">NOMBRE TITULAR</label>
								<input type="text" class="form-control input-sm" id="G2318_C45375" value=""  name="G2318_C45375"  placeholder="NOMBRE TITULAR">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2318_C45376" id="LblG2318_C45376">NÚMERO DE CONTACTO</label>
								<input type="text" class="form-control input-sm" id="G2318_C45376" value=""  name="G2318_C45376"  placeholder="NÚMERO DE CONTACTO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2318_C47960" id="LblG2318_C47960">CEDULA</label>
								<input type="text" class="form-control input-sm" id="G2318_C47960" value=""  name="G2318_C47960"  placeholder="CEDULA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Buenos días, tardes, noches, mi nombre es…. le llamo desde la Aurora Funerales y Capillas, con quien tengo el gusto de hablar?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr Encantado de saludarlo El motivo de nuestra llamada el día de hoy primero es brindarle un cordial saludo de parte de nuestra organización</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Nos gustaría saber quiénes de su familia NO cuentan con protección funeraria, o están desprotegidos, ya que desde este momento pueden contar con la protección</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr..... sabe que es muy importante contar con este servicio. Ya que en caso de presentarse el fallecimiento de alguna persona de su grupo familiar y al no tener protección funeraria tendría que pagar de 3 a 4 millones o endeudarse para cubrir inmediatamente el servicio. Sr De cuantas personas se compone su núcleo familiar? Esperar respuesta…</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr. le recuerdo que nuestra llamada es grabada y monitoreada para efectos de calidad y legalidad entre las partes.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>El titular es quien debe reportar los datos del programa.(no el agente con la posibilidad de revisar planes anteriores).“POR FAVOR NO OMITIR UN SOLO PUNTO DE LA ACTUALIZACION DE DATOS BRINDAD PR EL CLIENTE” Sr…. su programa es un plan (aurora plus) que tiene un valor mensual de</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Tiene los siguientes servicios BASICOS:</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>1. El traslado Nacional del fallecido hasta ubicarlo en la sala de velación (según el plan)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>2. Preparación normal del cuerpo para una velación de hasta 24 horas (tanatopraxia) y el suministro de los implementos requeridos.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>3. Suministro del cofre según el plan.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>4. Diligencias civiles y eclésiasticas, que permitan la inhumación o la cremación.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>5. Habito si se requiere, (este debe solicitarse).</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>6. Servicio urbano de carroza para las exequias (siempre que este servicio se preste y sea de uso común en la localidad).</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>7. Una serie de avisos murales</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>8. Pago del servicio religioso</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>9. Servicio de sala de velación (tiempo continuo uso de la sala de acuerdo con la normatividad establecida por la alcaldía de cada localidad, hasta por 24 horas o implementos para la velación si esta es domiciliaria (Un cristo, dos velones, dos bases y dos candeleros) y servicios de cafetería (tinto y aromáticas).</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>1. Sr…. le recuerdo entonces que: La cláusula 1. Está en el resumen de venta.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>2. Si usted no solicita el servicio funerario a nuestra compañía, no tendrá derecho a ninguna compensación económica. (Ley 795, artículo 111 de 14 de Enero de 2003).</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>3. Los pagos efectuados que se generen por medio de este contrato telefónico en ningún momento se convierte en ahorro, por lo tanto no son reembolsables solo dan derecho a tomar o no los servicios acordados, a su elección.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>4. La no utilización de uno o varios servicios no obliga a nuestra compañía al reconocimiento económico por los no usados pudiendo el usuario utilizar, a su libre elección, uno, varios o todos los servicios y dejando en claro que Promotora La Aurora S.A. tiene a su disposición los servicios cada que el cliente lo requiera.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>La cláusula 5. Está en el resumen de venta</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>6. Si una persona se encuentra inscrita en dos o más contratos de previsión exequial será responsabilidad solo de titular o titulares de cada contrato la inscripción y permanencia en los mismos y solo tendrá derecho a la reclamación de un servicio, sin que esto genere pago de alguna compensación económica.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>La cláusula 7. Está en el resumen de venta.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Esta cláusula se lee únicamente cuando en el programa haya un bebe por nacer: 8*. Los naciturus deben ser registrados en esta llamada, como inscritos, de los cual queda el soporte en la grabación, para tener derecho al servicio de cofre y destino final de acuerdo al plan, especificando de quien es hijo y parentesco con el titular. Y que en caso de fallecimiento de la mamá y el bebe se prestara un sólo servicio.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>9. En caso de ocurrencia de catástrofes naturales, antrópicas o tecnológicas, Guerra interna o externa o por muerte colectiva La Aurora Funerales y Capillas, está exenta de cumplir con el presente acuerdo, para lo cual, en tales eventos prestará hasta un máximo de veinte (20) servicios exequiales, los que corresponderán a los primeros que sean solicitados; si la catástrofe afecta la infraestructura de la Aurora Funerales y Capillas, esta quedará exenta de cumplir con las obligaciones contenidas en el presente acuerdo sin que por ese hecho pueda exigírsele devolución alguna de las cuotas canceladas o por cualquier otro concepto derivado de este acuerdo.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>La cláusula 10. Esta. No se menciona.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>La cláusula 11. Está en el resumen de venta</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>(esto para cuando ingresan personas mayores de 66 años en adelante). ANTES NO APLICA Sr... le informo Que habra cambios en el valor del programa, cuando la persona mayor de 66 (3500) años cumpla sus 70 años se le incrementara tan solo 2500 pesos... y a sus 75 años habra otro incremento el cual sera opcional. (10000)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>INFORMAR PARA PROGRAMAS COMERCIALIZADOS POR MEDIO DE PAGO DIFERENTE AL MASIVO (OFICINA, RECAUDADOR ENTRE OTROS)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>13 y 14. El titular y los inscritos tendrán plena cobertura mientras sus edades sean inferiores a 74 años y 364 días. A partir de los 75 años la cobertura del presente acuerdo será del 50% del valor del servicio pactado, y para el destino final 1,0 smlv en caso de su fallecimiento; cuando el titular o cualquiera de sus inscritos supere los 75 años, tiene la opción de seguir Con la cobertura al 100% adquiriendo y cancelando un comodín por edad. En caso de utilizar el servicio deberá cancelar el resto de las cuotas del período contratado. La adquisición del adicional por edad es opcional para el titular del programa.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>INFORMAR PARA PROGRAMAS POR RECAUDO MASIVO (CHEC)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>15, 16. El inscrito al programa de previsión exequial a través de recaudo masivo que no pague en la fecha límite estipulada en la factura, deberá pagar lo correspondiente a dos períodos en las oficinas de la Aurora o en la siguiente factura, para seguir inscritos al plan de pago por recaudo masivo, recuerde sr. XXXX, que el NO pago oportuno ocasiona la perdida del servicio.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>17. Aplica para programas cancelados por oficina o con recaudador Para los programas cancelados en la oficina o mediante recuador, que no paguen en la fecha estipulada PROMOTORA LA AURORA S.A. le da vigencia al cubrimiento de las personas inscritas en el contrato de prevision exequial hasta las 24 horas del quinceavo día de la fecha de pago, para que cancele lo adeudado. A partir de allí los inscritos quedarán sin vigencia y en caso de presentarse un servicio quienes hayan cancelado sus cuotas atrasadas tendrán el soporte en la factura donde consta que nuestra compañía les presta el 50% de los servicios funerarios, sin incluir los gastos de inhumación o cremación, estos correrán por cuenta del cliente por un período de 15 días calendario, momento a partir del cual podrá acceder a los servicios completos conforme al programa adquirido.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>18. En los programas familiares y en los comercializados a través de recaudo masivo, en caso de fallecimiento de alguno de los aquí inscritos incluyendo el titular, durante el año contratado deberá cancelar el saldo de las cuotas restantes para tener derecho al servicio y no podrá reemplazarse la persona fallecida hasta el nuevo contrato.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Una Vez actualice o suministre sus datos, LA PROMOTORA LA AURORA S.A, realizara como un tratamiento relativo a su compilación, actualización y procesamiento</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>1. El recaudo es a través del pago acordado (oficina, susuerte, efecty, recaudo, factura energía chec)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>2. Todas las personas aquí inscritas se encuentran en buen estado de salud?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>3. Acepta y está de acuerdo en su integridad todos los puntos y numerales que le he mencionado?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Informar para programas comercializados por recaudo masivo (chec):</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>1. Es consciente que además del concepto de energía en la próxima factura le llegara el valor adicional correspondiente a la previsión exequial? Además, que en caso de tener un corte de energía por no pago, queda inmediatamente suspendida la protección y el cobro del mes posterior, por lo cual debe dirigirse a nuestras oficinas a cancelar".</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Cualquier inquietud sobre el plan de previsión exequial, será atendido por un funcionario de la aurora funerales y capillas en nuestras oficinas o el representante en la localidad y no en las oficinas en la chec</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>NOTA: el anterior punto se debe mencionar para los programas comercializados para recaudo por un medio diferente a la factura CHEC.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Cualquier inquietud sobre el plan de previsión exequial, será atendido por un funcionario de la aurora funerales y capillas en nuestras oficinas o el representante en la localidad.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr. XXX le pregunto ha tomado usted está afiliación voluntariamente y ha escuchado y aceptado las condiciones y cláusulas del contrato?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>El pago de la cuota oportunamente es en todo caso, responsabilidad del titular. Las alternativas de recaudo ofrecidas por la Aurora no eximen al titular de su responsabilidad con el pago oportuno.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Con la lectura y aceptación del presente acuerdo, se sustituye cualquier otro suscrito entre las mismas partes y para los mismos fines.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr. le recuerdo que nuestra llamada es grabada y monitoreada para efectos de calidad y legalidad entre las partes y lo aquí pactado</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Es tan amable de indicarme que fecha es hoy???</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>NOTA: SIRVE PARA HABEAS DATA Y CIFIN</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Para suscribir la afiliación inicial del plan adquirido de previsión exequial es necesario que usted auto- rice a LA AURORA con el fin de que se recolecte, almacene, use, trate, reporte sus datos personales y los comparta con sus empresas aliadas. De la misma forma autoriza a LA AURORA y sus empresas aliadas a enviarle información relacionada con actividades, programas, productos y servicios que sean promocionados por LA AURORA o empre- sas aliadas (Entonces usted si autoriza?, esto en caso de que el titular se quede callado cuando termi- nemos de pedir la autorización y no dar la opción del no.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>INFORMAR CUANDO ES POR RECUDO MASIVO (CHEC)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr(a)... autoriza usted a la Central Hidroeléctrica de Caldas empresa de servicios públicos, Chec, para cargar en la factura el valor de su previsión exequial tomado con promotora la aurora s.a por valor de…..pesos mensuales?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr(a). Me recuerda por favor su número de cédula, recuerde son... mensuales y cuando se cumpla el año se hará un incremento anual aproximado al IPC.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr. XXXX le recuerdo entonces que las personas inscritas en este programa son Únicamente las siguientes:</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Datos completos de: Nombre completo del titular, dirección teléfono y correo del titular, Nombre completo, edad y parentesco de cada uno de los inscritos</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Esta es la cláusula N. 1 Le informo las fechas de protección para usted como titular y sus inscritos son: Para usted como titular tiene protección inmediata por cualquier concepto de fallecimiento, excepto por suicidio que se cubrirá después de seis meses a partir de la fecha de inicio del programa. (Indicar fecha) Sus inscritos tendrán protección inmediata por muerte accidental, trágica o violenta; por muerte natural o enfermedad preexistente después de dos meses y un día (indicar fecha) y por suicidio se protegerán también en los seis meses siguientes a la fecha de inicio de protección.(indicar fecha).</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Las caracteristicas de su programa son: Cobertura… indicar cual Traslado en su plan (indicar cuál) hasta por 3 SMLV El destino final del cuerpo es: (indicar cuál) La elección de la funeraria es: (indicar cuál). La elección del cofre es: (indicar cuál)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Nota: en todos los casos indicar fechas.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Le informo sr XXXX que la modificación o cancelación de este contrato podrá realizarse hasta dentro de un año (se indica la fecha en el período de Nuevo contrato) y que en caso de necesitar de nuestros servicios comuníquese inmediatamente con nuestra área de servicios a la línea gratuita nacional 01 8000 916 966 o en Manizales al 8 99 77 00 o el cel de nuestra área de servicios 321 799 7030 o el 321 799 7012 a cualquier hora del día o de la noche durante los 365 días del año, indicando el nombre completo, el número de la cédula; Sr (a), tenga presente que en caso de presentarse un fallecimiento debe solicitar ante el médico tratante o a la institución responsable la expedición del certificado médico de defunción, esto como una condición de ley y de obligatorio cumplimiento previo a la custodia del cuerpo, para tener derecho a la prestación del servicio.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Solicitar referidos nacionales e internacionales.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Bienvenido! ... Le felicito es usted una persona muy responsable, porque esto es quererse usted y querer a su familia, nuestra línea gratuita de atención al cliente es 018000 916966, desde un celular no se puede marcar, pero usted también puede comunicarse a nuestro servicio por nuestro call center, al 8997700 en Manizales o en su municipio a tal teléfono!.. (Número de los teléfonos locales), las 24 horas del día.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Le recuerdo que mi nombre es...... de La Aurora Funerales y Capillas espero tenga buena tarde. Dia, noche muchas gracias por atenderme.</h3>
                            <!-- FIN LIBRETO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2318/G2318_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2318_C45366").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G2318_C45367").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               

                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
