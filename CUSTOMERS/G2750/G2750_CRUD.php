<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2750;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2750
                  SET G2750_C62643 = -201
                  WHERE G2750_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2750 
                    WHERE G2750_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2750_C62642"]) || $gestion["G2750_C62642"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2750_C62642"];
        }

        if (is_null($gestion["G2750_C62644"]) || $gestion["G2750_C62644"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2750_C62644"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2750_FechaInsercion"]."',".$gestion["G2750_Usuario"].",'".$gestion["G2750_C".$P["P"]]."','".$gestion["G2750_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "customers.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2750 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2750_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2750_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2750_LinkContenido as url FROM ".$BaseDatos.".G2750 WHERE G2750_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2750_ConsInte__b, G2750_FechaInsercion , G2750_Usuario ,  G2750_CodigoMiembro  , G2750_PoblacionOrigen , G2750_EstadoDiligenciamiento ,  G2750_IdLlamada , G2750_C53282 as principal ,G2750_C53282,G2750_C53283,G2750_C56934,G2750_C56935,G2750_C56936,G2750_C62650,G2750_C60442,G2750_C62651,G2750_C62652,G2750_C53286,G2750_C53287,G2750_C53288,G2750_C53289,G2750_C53290,G2750_C53291,G2750_C53292,G2750_C53293,G2750_C53294,G2750_C61587,G2750_C61588,G2750_C61589,G2750_C61590,G2750_C62462,G2750_C62463,G2750_C62464,G2750_C62466,G2750_C62467,G2750_C62643,G2750_C62642,G2750_C62644,G2750_C62645 FROM '.$BaseDatos.'.G2750 WHERE G2750_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2750_C53282'] = $key->G2750_C53282;

                $datos[$i]['G2750_C53283'] = $key->G2750_C53283;

                $datos[$i]['G2750_C56934'] = $key->G2750_C56934;

                $datos[$i]['G2750_C56935'] = $key->G2750_C56935;

                $datos[$i]['G2750_C56936'] = $key->G2750_C56936;

                $datos[$i]['G2750_C62650'] = $key->G2750_C62650;

                $datos[$i]['G2750_C60442'] = explode(' ', $key->G2750_C60442)[0];

                $datos[$i]['G2750_C62651'] = $key->G2750_C62651;

                $datos[$i]['G2750_C62652'] = $key->G2750_C62652;

                $datos[$i]['G2750_C53286'] = $key->G2750_C53286;

                $datos[$i]['G2750_C53287'] = $key->G2750_C53287;

                $datos[$i]['G2750_C53288'] = explode(' ', $key->G2750_C53288)[0];
  
                $hora = '';
                if(!is_null($key->G2750_C53289)){
                    $hora = explode(' ', $key->G2750_C53289)[1];
                }

                $datos[$i]['G2750_C53289'] = $hora;

                $datos[$i]['G2750_C53290'] = $key->G2750_C53290;

                $datos[$i]['G2750_C53291'] = $key->G2750_C53291;

                $datos[$i]['G2750_C53292'] = $key->G2750_C53292;

                $datos[$i]['G2750_C53293'] = $key->G2750_C53293;

                $datos[$i]['G2750_C53294'] = $key->G2750_C53294;

                $datos[$i]['G2750_C61587'] = $key->G2750_C61587;

                $datos[$i]['G2750_C61588'] = $key->G2750_C61588;

                $datos[$i]['G2750_C61589'] = explode(' ', $key->G2750_C61589)[0];

                $datos[$i]['G2750_C61590'] = $key->G2750_C61590;

                $datos[$i]['G2750_C62462'] = $key->G2750_C62462;

                $datos[$i]['G2750_C62463'] = $key->G2750_C62463;

                $datos[$i]['G2750_C62464'] = $key->G2750_C62464;

                $datos[$i]['G2750_C62466'] = explode(' ', $key->G2750_C62466)[0];

                $datos[$i]['G2750_C62467'] = $key->G2750_C62467;

                $datos[$i]['G2750_C62643'] = $key->G2750_C62643;

                $datos[$i]['G2750_C62642'] = $key->G2750_C62642;

                $datos[$i]['G2750_C62644'] = $key->G2750_C62644;

                $datos[$i]['G2750_C62645'] = $key->G2750_C62645;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2750";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2750_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2750_ConsInte__b as id,  G2750_C53282 as camp1 , G2750_C53283 as camp2 
                     FROM ".$BaseDatos.".G2750  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2750_ConsInte__b as id,  G2750_C53282 as camp1 , G2750_C53283 as camp2  
                    FROM ".$BaseDatos.".G2750  JOIN ".$BaseDatos.".G2750_M".$_POST['muestra']." ON G2750_ConsInte__b = G2750_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2750_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2750_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2750_C53282 LIKE '%".$B."%' OR G2750_C53283 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2750_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2750");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2750_ConsInte__b, G2750_FechaInsercion , G2750_Usuario ,  G2750_CodigoMiembro  , G2750_PoblacionOrigen , G2750_EstadoDiligenciamiento ,  G2750_IdLlamada , G2750_C53282 as principal ,G2750_C53282,G2750_C53283,G2750_C56934,G2750_C56935,G2750_C56936,G2750_C62650,G2750_C60442,G2750_C62651,G2750_C62652, a.LISOPC_Nombre____b as G2750_C53286, b.LISOPC_Nombre____b as G2750_C53287,G2750_C53288,G2750_C53289,G2750_C53290,G2750_C53291,G2750_C53292,G2750_C53293,G2750_C53294, c.LISOPC_Nombre____b as G2750_C61587, d.LISOPC_Nombre____b as G2750_C61588,G2750_C61589, e.LISOPC_Nombre____b as G2750_C61590, f.LISOPC_Nombre____b as G2750_C62462,G2750_C62463,G2750_C62464,G2750_C62466,G2750_C62467, g.LISOPC_Nombre____b as G2750_C62643,G2750_C62642,G2750_C62644,G2750_C62645 FROM '.$BaseDatos.'.G2750 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2750_C53286 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2750_C53287 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2750_C61587 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2750_C61588 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2750_C61590 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2750_C62462 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2750_C62643';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2750_C53289)){
                    $hora_a = explode(' ', $fila->G2750_C53289)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2750_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2750_ConsInte__b , ($fila->G2750_C53282) , ($fila->G2750_C53283) , ($fila->G2750_C56934) , ($fila->G2750_C56935) , ($fila->G2750_C56936) , ($fila->G2750_C62650) , explode(' ', $fila->G2750_C60442)[0] , ($fila->G2750_C62651) , ($fila->G2750_C62652) , ($fila->G2750_C53286) , ($fila->G2750_C53287) , explode(' ', $fila->G2750_C53288)[0] , $hora_a , ($fila->G2750_C53290) , ($fila->G2750_C53291) , ($fila->G2750_C53292) , ($fila->G2750_C53293) , ($fila->G2750_C53294) , ($fila->G2750_C61587) , ($fila->G2750_C61588) , explode(' ', $fila->G2750_C61589)[0] , ($fila->G2750_C61590) , ($fila->G2750_C62462) , ($fila->G2750_C62463) , ($fila->G2750_C62464) , explode(' ', $fila->G2750_C62466)[0] , ($fila->G2750_C62467) , ($fila->G2750_C62643) , ($fila->G2750_C62642) , ($fila->G2750_C62644) , ($fila->G2750_C62645) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2750 WHERE G2750_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2750";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2750_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2750_ConsInte__b as id,  G2750_C53282 as camp1 , G2750_C53283 as camp2  FROM '.$BaseDatos.'.G2750 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2750_ConsInte__b as id,  G2750_C53282 as camp1 , G2750_C53283 as camp2  
                    FROM ".$BaseDatos.".G2750  JOIN ".$BaseDatos.".G2750_M".$_POST['muestra']." ON G2750_ConsInte__b = G2750_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2750_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2750_C53282 LIKE "%'.$B.'%" OR G2750_C53283 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2750_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2750 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2750(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2750_C53282"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C53282 = '".$_POST["G2750_C53282"]."'";
                $LsqlI .= $separador."G2750_C53282";
                $LsqlV .= $separador."'".$_POST["G2750_C53282"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C53283"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C53283 = '".$_POST["G2750_C53283"]."'";
                $LsqlI .= $separador."G2750_C53283";
                $LsqlV .= $separador."'".$_POST["G2750_C53283"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C56934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C56934 = '".$_POST["G2750_C56934"]."'";
                $LsqlI .= $separador."G2750_C56934";
                $LsqlV .= $separador."'".$_POST["G2750_C56934"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C56935"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C56935 = '".$_POST["G2750_C56935"]."'";
                $LsqlI .= $separador."G2750_C56935";
                $LsqlV .= $separador."'".$_POST["G2750_C56935"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C56936"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C56936 = '".$_POST["G2750_C56936"]."'";
                $LsqlI .= $separador."G2750_C56936";
                $LsqlV .= $separador."'".$_POST["G2750_C56936"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C62650"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62650 = '".$_POST["G2750_C62650"]."'";
                $LsqlI .= $separador."G2750_C62650";
                $LsqlV .= $separador."'".$_POST["G2750_C62650"]."'";
                $validar = 1;
            }
             
 
            $G2750_C60442 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2750_C60442"])){    
                if($_POST["G2750_C60442"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2750_C60442"]);
                    if(count($tieneHora) > 1){
                        $G2750_C60442 = "'".$_POST["G2750_C60442"]."'";
                    }else{
                        $G2750_C60442 = "'".str_replace(' ', '',$_POST["G2750_C60442"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2750_C60442 = ".$G2750_C60442;
                    $LsqlI .= $separador." G2750_C60442";
                    $LsqlV .= $separador.$G2750_C60442;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2750_C62651"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62651 = '".$_POST["G2750_C62651"]."'";
                $LsqlI .= $separador."G2750_C62651";
                $LsqlV .= $separador."'".$_POST["G2750_C62651"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C62652"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62652 = '".$_POST["G2750_C62652"]."'";
                $LsqlI .= $separador."G2750_C62652";
                $LsqlV .= $separador."'".$_POST["G2750_C62652"]."'";
                $validar = 1;
            }
             
 
            $G2750_C53286 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2750_C53286 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2750_C53286 = ".$G2750_C53286;
                    $LsqlI .= $separador." G2750_C53286";
                    $LsqlV .= $separador.$G2750_C53286;
                    $validar = 1;

                    
                }
            }
 
            $G2750_C53287 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2750_C53287 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2750_C53287 = ".$G2750_C53287;
                    $LsqlI .= $separador." G2750_C53287";
                    $LsqlV .= $separador.$G2750_C53287;
                    $validar = 1;
                }
            }
 
            $G2750_C53288 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2750_C53288 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2750_C53288 = ".$G2750_C53288;
                    $LsqlI .= $separador." G2750_C53288";
                    $LsqlV .= $separador.$G2750_C53288;
                    $validar = 1;
                }
            }
 
            $G2750_C53289 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2750_C53289 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2750_C53289 = ".$G2750_C53289;
                    $LsqlI .= $separador." G2750_C53289";
                    $LsqlV .= $separador.$G2750_C53289;
                    $validar = 1;
                }
            }
 
            $G2750_C53290 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2750_C53290 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2750_C53290 = ".$G2750_C53290;
                    $LsqlI .= $separador." G2750_C53290";
                    $LsqlV .= $separador.$G2750_C53290;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2750_C53291"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C53291 = '".$_POST["G2750_C53291"]."'";
                $LsqlI .= $separador."G2750_C53291";
                $LsqlV .= $separador."'".$_POST["G2750_C53291"]."'";
                $validar = 1;
            }
             
  
            //JDBD - Fecha oculta Script
            if(isset($_POST["G2750_C53292"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date("Y-m-d H:i:s");

                $LsqlU .= $separador."G2750_C53292 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2750_C53292";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            //JDBD - Hora oculta Script
            if(isset($_POST["G2750_C53293"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date("H:i:s");

                $LsqlU .= $separador."G2750_C53293 = '".$strHora_t."'";
                $LsqlI .= $separador."G2750_C53293";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C53294"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C53294 = '".$_POST["G2750_C53294"]."'";
                $LsqlI .= $separador."G2750_C53294";
                $LsqlV .= $separador."'".$_POST["G2750_C53294"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C53295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C53295 = '".$_POST["G2750_C53295"]."'";
                $LsqlI .= $separador."G2750_C53295";
                $LsqlV .= $separador."'".$_POST["G2750_C53295"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C53296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C53296 = '".$_POST["G2750_C53296"]."'";
                $LsqlI .= $separador."G2750_C53296";
                $LsqlV .= $separador."'".$_POST["G2750_C53296"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C61587"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C61587 = '".$_POST["G2750_C61587"]."'";
                $LsqlI .= $separador."G2750_C61587";
                $LsqlV .= $separador."'".$_POST["G2750_C61587"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C61588"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C61588 = '".$_POST["G2750_C61588"]."'";
                $LsqlI .= $separador."G2750_C61588";
                $LsqlV .= $separador."'".$_POST["G2750_C61588"]."'";
                $validar = 1;
            }
             
 
            $G2750_C61589 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2750_C61589"])){    
                if($_POST["G2750_C61589"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2750_C61589"]);
                    if(count($tieneHora) > 1){
                        $G2750_C61589 = "'".$_POST["G2750_C61589"]."'";
                    }else{
                        $G2750_C61589 = "'".str_replace(' ', '',$_POST["G2750_C61589"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2750_C61589 = ".$G2750_C61589;
                    $LsqlI .= $separador." G2750_C61589";
                    $LsqlV .= $separador.$G2750_C61589;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2750_C61590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C61590 = '".$_POST["G2750_C61590"]."'";
                $LsqlI .= $separador."G2750_C61590";
                $LsqlV .= $separador."'".$_POST["G2750_C61590"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C62462"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62462 = '".$_POST["G2750_C62462"]."'";
                $LsqlI .= $separador."G2750_C62462";
                $LsqlV .= $separador."'".$_POST["G2750_C62462"]."'";
                $validar = 1;
            }
             
  
            $G2750_C62463 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2750_C62463"])){
                if($_POST["G2750_C62463"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2750_C62463 = $_POST["G2750_C62463"];
                    $LsqlU .= $separador." G2750_C62463 = ".$G2750_C62463."";
                    $LsqlI .= $separador." G2750_C62463";
                    $LsqlV .= $separador.$G2750_C62463;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2750_C62464"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62464 = '".$_POST["G2750_C62464"]."'";
                $LsqlI .= $separador."G2750_C62464";
                $LsqlV .= $separador."'".$_POST["G2750_C62464"]."'";
                $validar = 1;
            }
             
 
            $G2750_C62466 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2750_C62466"])){    
                if($_POST["G2750_C62466"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2750_C62466"]);
                    if(count($tieneHora) > 1){
                        $G2750_C62466 = "'".$_POST["G2750_C62466"]."'";
                    }else{
                        $G2750_C62466 = "'".str_replace(' ', '',$_POST["G2750_C62466"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2750_C62466 = ".$G2750_C62466;
                    $LsqlI .= $separador." G2750_C62466";
                    $LsqlV .= $separador.$G2750_C62466;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2750_C62467"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62467 = '".$_POST["G2750_C62467"]."'";
                $LsqlI .= $separador."G2750_C62467";
                $LsqlV .= $separador."'".$_POST["G2750_C62467"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C62643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62643 = '".$_POST["G2750_C62643"]."'";
                $LsqlI .= $separador."G2750_C62643";
                $LsqlV .= $separador."'".$_POST["G2750_C62643"]."'";
                $validar = 1;
            }
             
  
            $G2750_C62642 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2750_C62642"])){
                if($_POST["G2750_C62642"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2750_C62642 = $_POST["G2750_C62642"];
                    $LsqlU .= $separador." G2750_C62642 = ".$G2750_C62642."";
                    $LsqlI .= $separador." G2750_C62642";
                    $LsqlV .= $separador.$G2750_C62642;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2750_C62644"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62644 = '".$_POST["G2750_C62644"]."'";
                $LsqlI .= $separador."G2750_C62644";
                $LsqlV .= $separador."'".$_POST["G2750_C62644"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2750_C62645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_C62645 = '".$_POST["G2750_C62645"]."'";
                $LsqlI .= $separador."G2750_C62645";
                $LsqlV .= $separador."'".$_POST["G2750_C62645"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2750_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2750_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2750_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2750_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2750_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2750_Usuario , G2750_FechaInsercion, G2750_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2750_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2750 WHERE G2750_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2750_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2750 WHERE G2750_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3061_ConsInte__b, G3061_C62217, G3061_C62219, G3061_C62220 FROM ".$BaseDatos.".G3061  ";

        $SQL .= " WHERE G3061_C62217 = '".$numero."'"; 

        $SQL .= " ORDER BY G3061_C62217";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3061_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3061_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G3061_C62217)."</cell>";

                echo "<cell>". ($fila->G3061_C62219)."</cell>";

                echo "<cell>". $fila->G3061_C62220."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3061 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3061(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G3061_C62219"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3061_C62219 = '".$_POST["G3061_C62219"]."'";
                    $LsqlI .= $separador."G3061_C62219";
                    $LsqlV .= $separador."'".$_POST["G3061_C62219"]."'";
                    $validar = 1;
                }

                                                                               
 
                $G3061_C62220= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G3061_C62220"])){    
                    if($_POST["G3061_C62220"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3061_C62220 = $_POST["G3061_C62220"];
                        $LsqlU .= $separador." G3061_C62220 = '".$G3061_C62220."'";
                        $LsqlI .= $separador." G3061_C62220";
                        $LsqlV .= $separador."'".$G3061_C62220."'";
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3061_C62217 = $numero;
                    $LsqlU .= ", G3061_C62217 = ".$G3061_C62217."";
                    $LsqlI .= ", G3061_C62217";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3061_Usuario ,  G3061_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3061_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3061 WHERE  G3061_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
