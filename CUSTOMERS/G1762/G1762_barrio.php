<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dyalogo CRM</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../../assets/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../../assets/ionicons-master/css/ionicons.min.css">


        <!-- Theme style -->
        <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../assets/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../../assets/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../../assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="../../assets/plugins/daterangepicker/daterangepicker.css">
         <!-- Bootstrap time Picker -->
               <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="../../assets/css/alertify.core.css">

        <link rel="stylesheet" href="../../assets/css/alertify.default.css">
        <link rel="stylesheet" type="text/css" media="screen" href="../../assets/Guriddo_jqGrid_/css/ui.jqgrid-bootstrap.css" />
        <link rel="stylesheet" href="../../assets/plugins/select2/select2.min.css" />
        <link rel="stylesheet" href="../../assets/plugins/sweetalert/sweetalert.css" />

        <script src="../../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        
        <link rel="stylesheet" href="../../assets/plugins/datatables/dataTables.bootstrap.css">
        <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

        <style type="text/css">
            [class^='select2'] {
                border-radius: 0px !important;
            }

            .modal-lg {
                width: 1200px;
            }

            .embed-container {
                position: relative;
                padding-bottom: 56.25%;
                height: 0;
                overflow: hidden;
            }

            .embed-container iframe {
                position: absolute;
                top:0;
                left: 0;
                width: 100%;
                height: 100%;
            }
        </style>
    </head>
<?php 
    include("../../conexion.php");
    
    $Lsql = "SELECT G1763_ConsInte__b as id ,G1763_C31908 as barrio, G1763_C31825 as caso, date_format(G1763_C31826,'%Y-%m-%d') as fecha, G1763_C31830 as direccion, c.LISOPC_Nombre____b as tipo, b.LISOPC_Nombre____b as subtipo, G1763_C31864 as observacion FROM DYALOGOCRM_WEB.G1763 left join DYALOGOCRM_SISTEMA.LISOPC as c on G1763_C31835 = c.LISOPC_ConsInte__b left join DYALOGOCRM_SISTEMA.LISOPC as b on G1763_C31858 = b.LISOPC_ConsInte__b where G1763_C31908='".$_GET['barrio']."'";
    $result = $mysqli->query($Lsql);    
?>    
<link rel="stylesheet" href="../../assets/plugins/datatables/dataTables.bootstrap.css">
<script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body">
				<table class="table table-bordered table-striped dataTable" id="grid">
				    <thead>
				        <tr>
				            <th>Barrio</th>
				            <th>No.caso</th>
				            <th>Fecha</th>
				            <th>Dirección</th>
				            <th>Tipo de caso</th>
				            <th>Sub-tipo</th>
				            <th>Observaciones</th>
				        </tr>
				    </thead>
				    <tbody>
				            <?php while($key = $result->fetch_object()){ ?>
                                    <tr>
                                        <td><?php echo $key->barrio?></td>
                                        <td><?php echo $key->caso?></td>
                                        <td><?php echo $key->fecha?></td>
                                        <td><?php echo $key->direccion?></td>
                                        <td><?php echo $key->tipo?></td>
                                        <td><?php echo $key->subtipo?></td>
                                        <td><?php echo $key->observacion?></td>
                                    </tr>
                            <?php } ?>
				    </tbody>
				</table>				 
			</div>
		</div>
	</div>
</div>   
<!-- page script -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#grid').DataTable({
        });    
    }); 
</script>   
