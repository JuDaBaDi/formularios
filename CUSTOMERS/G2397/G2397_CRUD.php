<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2397_ConsInte__b, G2397_FechaInsercion , G2397_Usuario ,  G2397_CodigoMiembro  , G2397_PoblacionOrigen , G2397_EstadoDiligenciamiento ,  G2397_IdLlamada , G2397_C47131 as principal ,G2397_C47129,G2397_C47130,G2397_C47131,G2397_C47132,G2397_C47133,G2397_C47134,G2397_C47135,G2397_C47136,G2397_C47137,G2397_C47138,G2397_C47139,G2397_C47140,G2397_C47141,G2397_C47142,G2397_C47143,G2397_C47144,G2397_C47145,G2397_C47146,G2397_C47147 FROM '.$BaseDatos.'.G2397 WHERE G2397_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2397_C47129'] = $key->G2397_C47129;

                $datos[$i]['G2397_C47130'] = $key->G2397_C47130;

                $datos[$i]['G2397_C47131'] = $key->G2397_C47131;

                $datos[$i]['G2397_C47132'] = $key->G2397_C47132;

                $datos[$i]['G2397_C47133'] = $key->G2397_C47133;

                $datos[$i]['G2397_C47134'] = $key->G2397_C47134;

                $datos[$i]['G2397_C47135'] = $key->G2397_C47135;

                $datos[$i]['G2397_C47136'] = $key->G2397_C47136;

                $datos[$i]['G2397_C47137'] = $key->G2397_C47137;

                $datos[$i]['G2397_C47138'] = $key->G2397_C47138;

                $datos[$i]['G2397_C47139'] = $key->G2397_C47139;

                $datos[$i]['G2397_C47140'] = $key->G2397_C47140;

                $datos[$i]['G2397_C47141'] = $key->G2397_C47141;

                $datos[$i]['G2397_C47142'] = $key->G2397_C47142;

                $datos[$i]['G2397_C47143'] = $key->G2397_C47143;

                $datos[$i]['G2397_C47144'] = $key->G2397_C47144;

                $datos[$i]['G2397_C47145'] = $key->G2397_C47145;

                $datos[$i]['G2397_C47146'] = $key->G2397_C47146;

                $datos[$i]['G2397_C47147'] = $key->G2397_C47147;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2397";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            $objRegPro_t = $resRegPro_t->fetch_object();

            if ($objRegPro_t->reg != 0) {
                $strRegProp_t = " AND G2397_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2397_ConsInte__b as id,  G2397_C47131 as camp1 , G2397_C47138 as camp2 
                     FROM ".$BaseDatos.".G2397  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2397_ConsInte__b as id,  G2397_C47131 as camp1 , G2397_C47138 as camp2  
                    FROM ".$BaseDatos.".G2397  JOIN ".$BaseDatos.".G2397_M".$_POST['muestra']." ON G2397_ConsInte__b = G2397_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2397_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2397_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2397_C47131 LIKE '%".$B."%' OR G2397_C47138 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2397_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2397");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2397_ConsInte__b, G2397_FechaInsercion , G2397_Usuario ,  G2397_CodigoMiembro  , G2397_PoblacionOrigen , G2397_EstadoDiligenciamiento ,  G2397_IdLlamada , G2397_C47131 as principal ,G2397_C47129,G2397_C47130,G2397_C47131,G2397_C47132,G2397_C47133,G2397_C47134,G2397_C47135,G2397_C47136,G2397_C47137,G2397_C47138,G2397_C47139,G2397_C47140,G2397_C47141,G2397_C47142,G2397_C47143, a.LISOPC_Nombre____b as G2397_C47144,G2397_C47145,G2397_C47146, b.LISOPC_Nombre____b as G2397_C47147 FROM '.$BaseDatos.'.G2397 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2397_C47144 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2397_C47147';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2397_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2397_ConsInte__b , ($fila->G2397_C47129) , ($fila->G2397_C47130) , ($fila->G2397_C47131) , ($fila->G2397_C47132) , ($fila->G2397_C47133) , ($fila->G2397_C47134) , ($fila->G2397_C47135) , ($fila->G2397_C47136) , ($fila->G2397_C47137) , ($fila->G2397_C47138) , ($fila->G2397_C47139) , ($fila->G2397_C47140) , ($fila->G2397_C47141) , ($fila->G2397_C47142) , ($fila->G2397_C47143) , ($fila->G2397_C47144) , ($fila->G2397_C47145) , ($fila->G2397_C47146) , ($fila->G2397_C47147) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2397 WHERE G2397_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2397";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            $objRegPro_t = $resRegPro_t->fetch_object();

            if ($objRegPro_t->reg != 0) {
                $strRegProp_t = ' AND G2397_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $strRegProp_t = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2397_ConsInte__b as id,  G2397_C47131 as camp1 , G2397_C47138 as camp2  FROM '.$BaseDatos.'.G2397 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2397_ConsInte__b as id,  G2397_C47131 as camp1 , G2397_C47138 as camp2  
                    FROM ".$BaseDatos.".G2397  JOIN ".$BaseDatos.".G2397_M".$_POST['muestra']." ON G2397_ConsInte__b = G2397_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2397_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2397_C47131 LIKE "%'.$B.'%" OR G2397_C47138 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2397_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2397 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2397(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2397_C47129"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47129 = '".$_POST["G2397_C47129"]."'";
                $LsqlI .= $separador."G2397_C47129";
                $LsqlV .= $separador."'".$_POST["G2397_C47129"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47130"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47130 = '".$_POST["G2397_C47130"]."'";
                $LsqlI .= $separador."G2397_C47130";
                $LsqlV .= $separador."'".$_POST["G2397_C47130"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47131"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47131 = '".$_POST["G2397_C47131"]."'";
                $LsqlI .= $separador."G2397_C47131";
                $LsqlV .= $separador."'".$_POST["G2397_C47131"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47132"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47132 = '".$_POST["G2397_C47132"]."'";
                $LsqlI .= $separador."G2397_C47132";
                $LsqlV .= $separador."'".$_POST["G2397_C47132"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47133"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47133 = '".$_POST["G2397_C47133"]."'";
                $LsqlI .= $separador."G2397_C47133";
                $LsqlV .= $separador."'".$_POST["G2397_C47133"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47134"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47134 = '".$_POST["G2397_C47134"]."'";
                $LsqlI .= $separador."G2397_C47134";
                $LsqlV .= $separador."'".$_POST["G2397_C47134"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47135"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47135 = '".$_POST["G2397_C47135"]."'";
                $LsqlI .= $separador."G2397_C47135";
                $LsqlV .= $separador."'".$_POST["G2397_C47135"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47136"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47136 = '".$_POST["G2397_C47136"]."'";
                $LsqlI .= $separador."G2397_C47136";
                $LsqlV .= $separador."'".$_POST["G2397_C47136"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47137"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47137 = '".$_POST["G2397_C47137"]."'";
                $LsqlI .= $separador."G2397_C47137";
                $LsqlV .= $separador."'".$_POST["G2397_C47137"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47138"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47138 = '".$_POST["G2397_C47138"]."'";
                $LsqlI .= $separador."G2397_C47138";
                $LsqlV .= $separador."'".$_POST["G2397_C47138"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47139 = '".$_POST["G2397_C47139"]."'";
                $LsqlI .= $separador."G2397_C47139";
                $LsqlV .= $separador."'".$_POST["G2397_C47139"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47140"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47140 = '".$_POST["G2397_C47140"]."'";
                $LsqlI .= $separador."G2397_C47140";
                $LsqlV .= $separador."'".$_POST["G2397_C47140"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47141"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47141 = '".$_POST["G2397_C47141"]."'";
                $LsqlI .= $separador."G2397_C47141";
                $LsqlV .= $separador."'".$_POST["G2397_C47141"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47142"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47142 = '".$_POST["G2397_C47142"]."'";
                $LsqlI .= $separador."G2397_C47142";
                $LsqlV .= $separador."'".$_POST["G2397_C47142"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47143"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47143 = '".$_POST["G2397_C47143"]."'";
                $LsqlI .= $separador."G2397_C47143";
                $LsqlV .= $separador."'".$_POST["G2397_C47143"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47144 = '".$_POST["G2397_C47144"]."'";
                $LsqlI .= $separador."G2397_C47144";
                $LsqlV .= $separador."'".$_POST["G2397_C47144"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47145"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47145 = '".$_POST["G2397_C47145"]."'";
                $LsqlI .= $separador."G2397_C47145";
                $LsqlV .= $separador."'".$_POST["G2397_C47145"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47146 = '".$_POST["G2397_C47146"]."'";
                $LsqlI .= $separador."G2397_C47146";
                $LsqlV .= $separador."'".$_POST["G2397_C47146"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2397_C47147"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_C47147 = '".$_POST["G2397_C47147"]."'";
                $LsqlI .= $separador."G2397_C47147";
                $LsqlV .= $separador."'".$_POST["G2397_C47147"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2397_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2397_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2397_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2397_Usuario , G2397_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2397_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2397 WHERE G2397_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2397 SET G2397_UltiGest__b =-14, G2397_GesMasImp_b =-14, G2397_TipoReintentoUG_b =0, G2397_TipoReintentoGMI_b =0, G2397_EstadoUG_b =-14, G2397_EstadoGMI_b =-14, G2397_CantidadIntentos =0, G2397_CantidadIntentosGMI_b =0 WHERE G2397_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
