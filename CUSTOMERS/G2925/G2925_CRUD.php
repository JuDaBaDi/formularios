<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2925_ConsInte__b, G2925_FechaInsercion , G2925_Usuario ,  G2925_CodigoMiembro  , G2925_PoblacionOrigen , G2925_EstadoDiligenciamiento ,  G2925_IdLlamada , G2925_C56784 as principal ,G2925_C56784,G2925_C56785,G2925_C56786,G2925_C56787,G2925_C56788,G2925_C56789,G2925_C56790,G2925_C56791,G2925_C56792,G2925_C57154,G2925_C56773,G2925_C56774,G2925_C56775,G2925_C56776,G2925_C56777,G2925_C56778,G2925_C56779,G2925_C56780,G2925_C56781 FROM '.$BaseDatos.'.G2925 WHERE G2925_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2925_C56784'] = $key->G2925_C56784;

                $datos[$i]['G2925_C56785'] = $key->G2925_C56785;

                $datos[$i]['G2925_C56786'] = $key->G2925_C56786;

                $datos[$i]['G2925_C56787'] = $key->G2925_C56787;

                $datos[$i]['G2925_C56788'] = $key->G2925_C56788;

                $datos[$i]['G2925_C56789'] = $key->G2925_C56789;

                $datos[$i]['G2925_C56790'] = $key->G2925_C56790;

                $datos[$i]['G2925_C56791'] = $key->G2925_C56791;

                $datos[$i]['G2925_C56792'] = $key->G2925_C56792;

                $datos[$i]['G2925_C57154'] = $key->G2925_C57154;

                $datos[$i]['G2925_C56773'] = $key->G2925_C56773;

                $datos[$i]['G2925_C56774'] = $key->G2925_C56774;

                $datos[$i]['G2925_C56775'] = explode(' ', $key->G2925_C56775)[0];
  
                $hora = '';
                if(!is_null($key->G2925_C56776)){
                    $hora = explode(' ', $key->G2925_C56776)[1];
                }

                $datos[$i]['G2925_C56776'] = $hora;

                $datos[$i]['G2925_C56777'] = $key->G2925_C56777;

                $datos[$i]['G2925_C56778'] = $key->G2925_C56778;

                $datos[$i]['G2925_C56779'] = $key->G2925_C56779;

                $datos[$i]['G2925_C56780'] = $key->G2925_C56780;

                $datos[$i]['G2925_C56781'] = $key->G2925_C56781;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2925";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2925_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2925_ConsInte__b as id,  G2925_C56784 as camp1 , G2925_C56785 as camp2 
                     FROM ".$BaseDatos.".G2925  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2925_ConsInte__b as id,  G2925_C56784 as camp1 , G2925_C56785 as camp2  
                    FROM ".$BaseDatos.".G2925  JOIN ".$BaseDatos.".G2925_M".$_POST['muestra']." ON G2925_ConsInte__b = G2925_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2925_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2925_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2925_C56784 LIKE '%".$B."%' OR G2925_C56785 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2925_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2925");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2925_ConsInte__b, G2925_FechaInsercion , G2925_Usuario ,  G2925_CodigoMiembro  , G2925_PoblacionOrigen , G2925_EstadoDiligenciamiento ,  G2925_IdLlamada , G2925_C56784 as principal ,G2925_C56784,G2925_C56785,G2925_C56786,G2925_C56787,G2925_C56788,G2925_C56789,G2925_C56790,G2925_C56791,G2925_C56792,G2925_C57154, a.LISOPC_Nombre____b as G2925_C56773, b.LISOPC_Nombre____b as G2925_C56774,G2925_C56775,G2925_C56776,G2925_C56777,G2925_C56778,G2925_C56779,G2925_C56780,G2925_C56781 FROM '.$BaseDatos.'.G2925 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2925_C56773 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2925_C56774';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2925_C56776)){
                    $hora_a = explode(' ', $fila->G2925_C56776)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2925_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2925_ConsInte__b , ($fila->G2925_C56784) , ($fila->G2925_C56785) , ($fila->G2925_C56786) , ($fila->G2925_C56787) , ($fila->G2925_C56788) , ($fila->G2925_C56789) , ($fila->G2925_C56790) , ($fila->G2925_C56791) , ($fila->G2925_C56792) , ($fila->G2925_C57154) , ($fila->G2925_C56773) , ($fila->G2925_C56774) , explode(' ', $fila->G2925_C56775)[0] , $hora_a , ($fila->G2925_C56777) , ($fila->G2925_C56778) , ($fila->G2925_C56779) , ($fila->G2925_C56780) , ($fila->G2925_C56781) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2925 WHERE G2925_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2925";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2925_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2925_ConsInte__b as id,  G2925_C56784 as camp1 , G2925_C56785 as camp2  FROM '.$BaseDatos.'.G2925 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2925_ConsInte__b as id,  G2925_C56784 as camp1 , G2925_C56785 as camp2  
                    FROM ".$BaseDatos.".G2925  JOIN ".$BaseDatos.".G2925_M".$_POST['muestra']." ON G2925_ConsInte__b = G2925_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2925_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2925_C56784 LIKE "%'.$B.'%" OR G2925_C56785 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2925_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2925 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2925(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2925_C56784"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56784 = '".$_POST["G2925_C56784"]."'";
                $LsqlI .= $separador."G2925_C56784";
                $LsqlV .= $separador."'".$_POST["G2925_C56784"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56785"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56785 = '".$_POST["G2925_C56785"]."'";
                $LsqlI .= $separador."G2925_C56785";
                $LsqlV .= $separador."'".$_POST["G2925_C56785"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56786"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56786 = '".$_POST["G2925_C56786"]."'";
                $LsqlI .= $separador."G2925_C56786";
                $LsqlV .= $separador."'".$_POST["G2925_C56786"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56787"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56787 = '".$_POST["G2925_C56787"]."'";
                $LsqlI .= $separador."G2925_C56787";
                $LsqlV .= $separador."'".$_POST["G2925_C56787"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56788"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56788 = '".$_POST["G2925_C56788"]."'";
                $LsqlI .= $separador."G2925_C56788";
                $LsqlV .= $separador."'".$_POST["G2925_C56788"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56789"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56789 = '".$_POST["G2925_C56789"]."'";
                $LsqlI .= $separador."G2925_C56789";
                $LsqlV .= $separador."'".$_POST["G2925_C56789"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56790"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56790 = '".$_POST["G2925_C56790"]."'";
                $LsqlI .= $separador."G2925_C56790";
                $LsqlV .= $separador."'".$_POST["G2925_C56790"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56791"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56791 = '".$_POST["G2925_C56791"]."'";
                $LsqlI .= $separador."G2925_C56791";
                $LsqlV .= $separador."'".$_POST["G2925_C56791"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56792"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56792 = '".$_POST["G2925_C56792"]."'";
                $LsqlI .= $separador."G2925_C56792";
                $LsqlV .= $separador."'".$_POST["G2925_C56792"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C57154"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C57154 = '".$_POST["G2925_C57154"]."'";
                $LsqlI .= $separador."G2925_C57154";
                $LsqlV .= $separador."'".$_POST["G2925_C57154"]."'";
                $validar = 1;
            }
             
 
            $G2925_C56773 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2925_C56773 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2925_C56773 = ".$G2925_C56773;
                    $LsqlI .= $separador." G2925_C56773";
                    $LsqlV .= $separador.$G2925_C56773;
                    $validar = 1;

                    
                }
            }
 
            $G2925_C56774 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2925_C56774 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2925_C56774 = ".$G2925_C56774;
                    $LsqlI .= $separador." G2925_C56774";
                    $LsqlV .= $separador.$G2925_C56774;
                    $validar = 1;
                }
            }
 
            $G2925_C56775 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2925_C56775 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2925_C56775 = ".$G2925_C56775;
                    $LsqlI .= $separador." G2925_C56775";
                    $LsqlV .= $separador.$G2925_C56775;
                    $validar = 1;
                }
            }
 
            $G2925_C56776 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2925_C56776 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2925_C56776 = ".$G2925_C56776;
                    $LsqlI .= $separador." G2925_C56776";
                    $LsqlV .= $separador.$G2925_C56776;
                    $validar = 1;
                }
            }
 
            $G2925_C56777 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2925_C56777 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2925_C56777 = ".$G2925_C56777;
                    $LsqlI .= $separador." G2925_C56777";
                    $LsqlV .= $separador.$G2925_C56777;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2925_C56778"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56778 = '".$_POST["G2925_C56778"]."'";
                $LsqlI .= $separador."G2925_C56778";
                $LsqlV .= $separador."'".$_POST["G2925_C56778"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56779"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56779 = '".$_POST["G2925_C56779"]."'";
                $LsqlI .= $separador."G2925_C56779";
                $LsqlV .= $separador."'".$_POST["G2925_C56779"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56780"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56780 = '".$_POST["G2925_C56780"]."'";
                $LsqlI .= $separador."G2925_C56780";
                $LsqlV .= $separador."'".$_POST["G2925_C56780"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56781"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56781 = '".$_POST["G2925_C56781"]."'";
                $LsqlI .= $separador."G2925_C56781";
                $LsqlV .= $separador."'".$_POST["G2925_C56781"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56782"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56782 = '".$_POST["G2925_C56782"]."'";
                $LsqlI .= $separador."G2925_C56782";
                $LsqlV .= $separador."'".$_POST["G2925_C56782"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2925_C56783"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_C56783 = '".$_POST["G2925_C56783"]."'";
                $LsqlI .= $separador."G2925_C56783";
                $LsqlV .= $separador."'".$_POST["G2925_C56783"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2925_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2925_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2925_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2925_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2925_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2925_Usuario , G2925_FechaInsercion, G2925_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2925_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2925 WHERE G2925_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
