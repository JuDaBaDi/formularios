<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2317_ConsInte__b, G2317_FechaInsercion , G2317_Usuario ,  G2317_CodigoMiembro  , G2317_PoblacionOrigen , G2317_EstadoDiligenciamiento ,  G2317_IdLlamada , G2317_C45300 as principal ,G2317_C45300,G2317_C45301,G2317_C52557,G2317_C45289,G2317_C45290,G2317_C45291,G2317_C45292,G2317_C45293,G2317_C45294,G2317_C45295,G2317_C45296,G2317_C45297 FROM '.$BaseDatos.'.G2317 WHERE G2317_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2317_C45300'] = $key->G2317_C45300;

                $datos[$i]['G2317_C45301'] = $key->G2317_C45301;

                $datos[$i]['G2317_C52557'] = $key->G2317_C52557;

                $datos[$i]['G2317_C45289'] = $key->G2317_C45289;

                $datos[$i]['G2317_C45290'] = $key->G2317_C45290;

                $datos[$i]['G2317_C45291'] = explode(' ', $key->G2317_C45291)[0];
  
                $hora = '';
                if(!is_null($key->G2317_C45292)){
                    $hora = explode(' ', $key->G2317_C45292)[1];
                }

                $datos[$i]['G2317_C45292'] = $hora;

                $datos[$i]['G2317_C45293'] = $key->G2317_C45293;

                $datos[$i]['G2317_C45294'] = $key->G2317_C45294;

                $datos[$i]['G2317_C45295'] = $key->G2317_C45295;

                $datos[$i]['G2317_C45296'] = $key->G2317_C45296;

                $datos[$i]['G2317_C45297'] = $key->G2317_C45297;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2317";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2317_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2317_ConsInte__b as id,  G2317_C45300 as camp1 , G2317_C45301 as camp2 
                     FROM ".$BaseDatos.".G2317  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2317_ConsInte__b as id,  G2317_C45300 as camp1 , G2317_C45301 as camp2  
                    FROM ".$BaseDatos.".G2317  JOIN ".$BaseDatos.".G2317_M".$_POST['muestra']." ON G2317_ConsInte__b = G2317_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2317_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2317_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2317_C45300 LIKE '%".$B."%' OR G2317_C45301 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2317_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2317");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2317_ConsInte__b, G2317_FechaInsercion , G2317_Usuario ,  G2317_CodigoMiembro  , G2317_PoblacionOrigen , G2317_EstadoDiligenciamiento ,  G2317_IdLlamada , G2317_C45300 as principal ,G2317_C45300,G2317_C45301,G2317_C52557, a.LISOPC_Nombre____b as G2317_C45289, b.LISOPC_Nombre____b as G2317_C45290,G2317_C45291,G2317_C45292,G2317_C45293,G2317_C45294,G2317_C45295,G2317_C45296,G2317_C45297 FROM '.$BaseDatos.'.G2317 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2317_C45289 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2317_C45290';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2317_C45292)){
                    $hora_a = explode(' ', $fila->G2317_C45292)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2317_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2317_ConsInte__b , ($fila->G2317_C45300) , ($fila->G2317_C45301) , ($fila->G2317_C52557) , ($fila->G2317_C45289) , ($fila->G2317_C45290) , explode(' ', $fila->G2317_C45291)[0] , $hora_a , ($fila->G2317_C45293) , ($fila->G2317_C45294) , ($fila->G2317_C45295) , ($fila->G2317_C45296) , ($fila->G2317_C45297) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2317 WHERE G2317_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2317";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2317_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2317_ConsInte__b as id,  G2317_C45300 as camp1 , G2317_C45301 as camp2  FROM '.$BaseDatos.'.G2317 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2317_ConsInte__b as id,  G2317_C45300 as camp1 , G2317_C45301 as camp2  
                    FROM ".$BaseDatos.".G2317  JOIN ".$BaseDatos.".G2317_M".$_POST['muestra']." ON G2317_ConsInte__b = G2317_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2317_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2317_C45300 LIKE "%'.$B.'%" OR G2317_C45301 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2317_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2317 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2317(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2317_C45300"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45300 = '".$_POST["G2317_C45300"]."'";
                $LsqlI .= $separador."G2317_C45300";
                $LsqlV .= $separador."'".$_POST["G2317_C45300"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45301"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45301 = '".$_POST["G2317_C45301"]."'";
                $LsqlI .= $separador."G2317_C45301";
                $LsqlV .= $separador."'".$_POST["G2317_C45301"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C52557"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C52557 = '".$_POST["G2317_C52557"]."'";
                $LsqlI .= $separador."G2317_C52557";
                $LsqlV .= $separador."'".$_POST["G2317_C52557"]."'";
                $validar = 1;
            }
             
 
            $G2317_C45289 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2317_C45289 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2317_C45289 = ".$G2317_C45289;
                    $LsqlI .= $separador." G2317_C45289";
                    $LsqlV .= $separador.$G2317_C45289;
                    $validar = 1;

                    
                }
            }
 
            $G2317_C45290 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2317_C45290 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2317_C45290 = ".$G2317_C45290;
                    $LsqlI .= $separador." G2317_C45290";
                    $LsqlV .= $separador.$G2317_C45290;
                    $validar = 1;
                }
            }
 
            $G2317_C45291 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2317_C45291 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2317_C45291 = ".$G2317_C45291;
                    $LsqlI .= $separador." G2317_C45291";
                    $LsqlV .= $separador.$G2317_C45291;
                    $validar = 1;
                }
            }
 
            $G2317_C45292 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2317_C45292 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2317_C45292 = ".$G2317_C45292;
                    $LsqlI .= $separador." G2317_C45292";
                    $LsqlV .= $separador.$G2317_C45292;
                    $validar = 1;
                }
            }
 
            $G2317_C45293 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2317_C45293 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2317_C45293 = ".$G2317_C45293;
                    $LsqlI .= $separador." G2317_C45293";
                    $LsqlV .= $separador.$G2317_C45293;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2317_C45294"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45294 = '".$_POST["G2317_C45294"]."'";
                $LsqlI .= $separador."G2317_C45294";
                $LsqlV .= $separador."'".$_POST["G2317_C45294"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45295 = '".$_POST["G2317_C45295"]."'";
                $LsqlI .= $separador."G2317_C45295";
                $LsqlV .= $separador."'".$_POST["G2317_C45295"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45296 = '".$_POST["G2317_C45296"]."'";
                $LsqlI .= $separador."G2317_C45296";
                $LsqlV .= $separador."'".$_POST["G2317_C45296"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45297"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45297 = '".$_POST["G2317_C45297"]."'";
                $LsqlI .= $separador."G2317_C45297";
                $LsqlV .= $separador."'".$_POST["G2317_C45297"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45302"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45302 = '".$_POST["G2317_C45302"]."'";
                $LsqlI .= $separador."G2317_C45302";
                $LsqlV .= $separador."'".$_POST["G2317_C45302"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45303"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45303 = '".$_POST["G2317_C45303"]."'";
                $LsqlI .= $separador."G2317_C45303";
                $LsqlV .= $separador."'".$_POST["G2317_C45303"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45304"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45304 = '".$_POST["G2317_C45304"]."'";
                $LsqlI .= $separador."G2317_C45304";
                $LsqlV .= $separador."'".$_POST["G2317_C45304"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45305"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45305 = '".$_POST["G2317_C45305"]."'";
                $LsqlI .= $separador."G2317_C45305";
                $LsqlV .= $separador."'".$_POST["G2317_C45305"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45306"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45306 = '".$_POST["G2317_C45306"]."'";
                $LsqlI .= $separador."G2317_C45306";
                $LsqlV .= $separador."'".$_POST["G2317_C45306"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45307"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45307 = '".$_POST["G2317_C45307"]."'";
                $LsqlI .= $separador."G2317_C45307";
                $LsqlV .= $separador."'".$_POST["G2317_C45307"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45308"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45308 = '".$_POST["G2317_C45308"]."'";
                $LsqlI .= $separador."G2317_C45308";
                $LsqlV .= $separador."'".$_POST["G2317_C45308"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45309"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45309 = '".$_POST["G2317_C45309"]."'";
                $LsqlI .= $separador."G2317_C45309";
                $LsqlV .= $separador."'".$_POST["G2317_C45309"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45310"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45310 = '".$_POST["G2317_C45310"]."'";
                $LsqlI .= $separador."G2317_C45310";
                $LsqlV .= $separador."'".$_POST["G2317_C45310"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45311"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45311 = '".$_POST["G2317_C45311"]."'";
                $LsqlI .= $separador."G2317_C45311";
                $LsqlV .= $separador."'".$_POST["G2317_C45311"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45312"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45312 = '".$_POST["G2317_C45312"]."'";
                $LsqlI .= $separador."G2317_C45312";
                $LsqlV .= $separador."'".$_POST["G2317_C45312"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45313 = '".$_POST["G2317_C45313"]."'";
                $LsqlI .= $separador."G2317_C45313";
                $LsqlV .= $separador."'".$_POST["G2317_C45313"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45314 = '".$_POST["G2317_C45314"]."'";
                $LsqlI .= $separador."G2317_C45314";
                $LsqlV .= $separador."'".$_POST["G2317_C45314"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45315 = '".$_POST["G2317_C45315"]."'";
                $LsqlI .= $separador."G2317_C45315";
                $LsqlV .= $separador."'".$_POST["G2317_C45315"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45316 = '".$_POST["G2317_C45316"]."'";
                $LsqlI .= $separador."G2317_C45316";
                $LsqlV .= $separador."'".$_POST["G2317_C45316"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45317 = '".$_POST["G2317_C45317"]."'";
                $LsqlI .= $separador."G2317_C45317";
                $LsqlV .= $separador."'".$_POST["G2317_C45317"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45318"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45318 = '".$_POST["G2317_C45318"]."'";
                $LsqlI .= $separador."G2317_C45318";
                $LsqlV .= $separador."'".$_POST["G2317_C45318"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45319"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45319 = '".$_POST["G2317_C45319"]."'";
                $LsqlI .= $separador."G2317_C45319";
                $LsqlV .= $separador."'".$_POST["G2317_C45319"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45320"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45320 = '".$_POST["G2317_C45320"]."'";
                $LsqlI .= $separador."G2317_C45320";
                $LsqlV .= $separador."'".$_POST["G2317_C45320"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45321"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45321 = '".$_POST["G2317_C45321"]."'";
                $LsqlI .= $separador."G2317_C45321";
                $LsqlV .= $separador."'".$_POST["G2317_C45321"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45322 = '".$_POST["G2317_C45322"]."'";
                $LsqlI .= $separador."G2317_C45322";
                $LsqlV .= $separador."'".$_POST["G2317_C45322"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45323"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45323 = '".$_POST["G2317_C45323"]."'";
                $LsqlI .= $separador."G2317_C45323";
                $LsqlV .= $separador."'".$_POST["G2317_C45323"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45324"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45324 = '".$_POST["G2317_C45324"]."'";
                $LsqlI .= $separador."G2317_C45324";
                $LsqlV .= $separador."'".$_POST["G2317_C45324"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45325"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45325 = '".$_POST["G2317_C45325"]."'";
                $LsqlI .= $separador."G2317_C45325";
                $LsqlV .= $separador."'".$_POST["G2317_C45325"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45326"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45326 = '".$_POST["G2317_C45326"]."'";
                $LsqlI .= $separador."G2317_C45326";
                $LsqlV .= $separador."'".$_POST["G2317_C45326"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45327"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45327 = '".$_POST["G2317_C45327"]."'";
                $LsqlI .= $separador."G2317_C45327";
                $LsqlV .= $separador."'".$_POST["G2317_C45327"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45328"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45328 = '".$_POST["G2317_C45328"]."'";
                $LsqlI .= $separador."G2317_C45328";
                $LsqlV .= $separador."'".$_POST["G2317_C45328"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45329"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45329 = '".$_POST["G2317_C45329"]."'";
                $LsqlI .= $separador."G2317_C45329";
                $LsqlV .= $separador."'".$_POST["G2317_C45329"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45330"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45330 = '".$_POST["G2317_C45330"]."'";
                $LsqlI .= $separador."G2317_C45330";
                $LsqlV .= $separador."'".$_POST["G2317_C45330"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45331"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45331 = '".$_POST["G2317_C45331"]."'";
                $LsqlI .= $separador."G2317_C45331";
                $LsqlV .= $separador."'".$_POST["G2317_C45331"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45332"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45332 = '".$_POST["G2317_C45332"]."'";
                $LsqlI .= $separador."G2317_C45332";
                $LsqlV .= $separador."'".$_POST["G2317_C45332"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45333"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45333 = '".$_POST["G2317_C45333"]."'";
                $LsqlI .= $separador."G2317_C45333";
                $LsqlV .= $separador."'".$_POST["G2317_C45333"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45334"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45334 = '".$_POST["G2317_C45334"]."'";
                $LsqlI .= $separador."G2317_C45334";
                $LsqlV .= $separador."'".$_POST["G2317_C45334"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45335"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45335 = '".$_POST["G2317_C45335"]."'";
                $LsqlI .= $separador."G2317_C45335";
                $LsqlV .= $separador."'".$_POST["G2317_C45335"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45336"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45336 = '".$_POST["G2317_C45336"]."'";
                $LsqlI .= $separador."G2317_C45336";
                $LsqlV .= $separador."'".$_POST["G2317_C45336"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45337"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45337 = '".$_POST["G2317_C45337"]."'";
                $LsqlI .= $separador."G2317_C45337";
                $LsqlV .= $separador."'".$_POST["G2317_C45337"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45338"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45338 = '".$_POST["G2317_C45338"]."'";
                $LsqlI .= $separador."G2317_C45338";
                $LsqlV .= $separador."'".$_POST["G2317_C45338"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45339"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45339 = '".$_POST["G2317_C45339"]."'";
                $LsqlI .= $separador."G2317_C45339";
                $LsqlV .= $separador."'".$_POST["G2317_C45339"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45340"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45340 = '".$_POST["G2317_C45340"]."'";
                $LsqlI .= $separador."G2317_C45340";
                $LsqlV .= $separador."'".$_POST["G2317_C45340"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45341"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45341 = '".$_POST["G2317_C45341"]."'";
                $LsqlI .= $separador."G2317_C45341";
                $LsqlV .= $separador."'".$_POST["G2317_C45341"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45342"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45342 = '".$_POST["G2317_C45342"]."'";
                $LsqlI .= $separador."G2317_C45342";
                $LsqlV .= $separador."'".$_POST["G2317_C45342"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45343"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45343 = '".$_POST["G2317_C45343"]."'";
                $LsqlI .= $separador."G2317_C45343";
                $LsqlV .= $separador."'".$_POST["G2317_C45343"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45344"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45344 = '".$_POST["G2317_C45344"]."'";
                $LsqlI .= $separador."G2317_C45344";
                $LsqlV .= $separador."'".$_POST["G2317_C45344"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45345"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45345 = '".$_POST["G2317_C45345"]."'";
                $LsqlI .= $separador."G2317_C45345";
                $LsqlV .= $separador."'".$_POST["G2317_C45345"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45346"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45346 = '".$_POST["G2317_C45346"]."'";
                $LsqlI .= $separador."G2317_C45346";
                $LsqlV .= $separador."'".$_POST["G2317_C45346"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45347"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45347 = '".$_POST["G2317_C45347"]."'";
                $LsqlI .= $separador."G2317_C45347";
                $LsqlV .= $separador."'".$_POST["G2317_C45347"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45348"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45348 = '".$_POST["G2317_C45348"]."'";
                $LsqlI .= $separador."G2317_C45348";
                $LsqlV .= $separador."'".$_POST["G2317_C45348"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45349"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45349 = '".$_POST["G2317_C45349"]."'";
                $LsqlI .= $separador."G2317_C45349";
                $LsqlV .= $separador."'".$_POST["G2317_C45349"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45350"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45350 = '".$_POST["G2317_C45350"]."'";
                $LsqlI .= $separador."G2317_C45350";
                $LsqlV .= $separador."'".$_POST["G2317_C45350"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45351"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45351 = '".$_POST["G2317_C45351"]."'";
                $LsqlI .= $separador."G2317_C45351";
                $LsqlV .= $separador."'".$_POST["G2317_C45351"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45352"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45352 = '".$_POST["G2317_C45352"]."'";
                $LsqlI .= $separador."G2317_C45352";
                $LsqlV .= $separador."'".$_POST["G2317_C45352"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45353"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45353 = '".$_POST["G2317_C45353"]."'";
                $LsqlI .= $separador."G2317_C45353";
                $LsqlV .= $separador."'".$_POST["G2317_C45353"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45354"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45354 = '".$_POST["G2317_C45354"]."'";
                $LsqlI .= $separador."G2317_C45354";
                $LsqlV .= $separador."'".$_POST["G2317_C45354"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45355"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45355 = '".$_POST["G2317_C45355"]."'";
                $LsqlI .= $separador."G2317_C45355";
                $LsqlV .= $separador."'".$_POST["G2317_C45355"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45356"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45356 = '".$_POST["G2317_C45356"]."'";
                $LsqlI .= $separador."G2317_C45356";
                $LsqlV .= $separador."'".$_POST["G2317_C45356"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45357"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45357 = '".$_POST["G2317_C45357"]."'";
                $LsqlI .= $separador."G2317_C45357";
                $LsqlV .= $separador."'".$_POST["G2317_C45357"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45358"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45358 = '".$_POST["G2317_C45358"]."'";
                $LsqlI .= $separador."G2317_C45358";
                $LsqlV .= $separador."'".$_POST["G2317_C45358"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45359"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45359 = '".$_POST["G2317_C45359"]."'";
                $LsqlI .= $separador."G2317_C45359";
                $LsqlV .= $separador."'".$_POST["G2317_C45359"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45360"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45360 = '".$_POST["G2317_C45360"]."'";
                $LsqlI .= $separador."G2317_C45360";
                $LsqlV .= $separador."'".$_POST["G2317_C45360"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45361"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45361 = '".$_POST["G2317_C45361"]."'";
                $LsqlI .= $separador."G2317_C45361";
                $LsqlV .= $separador."'".$_POST["G2317_C45361"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45362"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45362 = '".$_POST["G2317_C45362"]."'";
                $LsqlI .= $separador."G2317_C45362";
                $LsqlV .= $separador."'".$_POST["G2317_C45362"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2317_C45363"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_C45363 = '".$_POST["G2317_C45363"]."'";
                $LsqlI .= $separador."G2317_C45363";
                $LsqlV .= $separador."'".$_POST["G2317_C45363"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2317_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2317_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2317_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2317_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2317_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2317_Usuario , G2317_FechaInsercion, G2317_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2317_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2317 WHERE G2317_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;

                        $strSQLUpdateM_t = "Vacio : ";

                        if (isset($_POST["TipNoEF"]) && ($_POST["TipNoEF"] == 2 || $_POST["TipNoEF"] == "2")) {

                            $strSQLUpdateM_t = "UPDATE ".$BaseDatos.".G2320_M1619 SET G2320_M1619_ConIntUsu_b = ".$_GET['usuario']." WHERE G2320_M1619_CoInMiPo__b = ".$_GET['CodigoMiembro'];

                            $mysqli->query($strSQLUpdateM_t);

                        }
                        
                        echo $UltimoID;
                        
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
