
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2317/G2317_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2317_ConsInte__b as id, G2317_C45300 as camp1 , G2317_C45301 as camp2 FROM ".$BaseDatos.".G2317  WHERE G2317_Usuario = ".$idUsuario." ORDER BY G2317_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2317_ConsInte__b as id, G2317_C45300 as camp1 , G2317_C45301 as camp2 FROM ".$BaseDatos.".G2317  ORDER BY G2317_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2317_ConsInte__b as id, G2317_C45300 as camp1 , G2317_C45301 as camp2 FROM ".$BaseDatos.".G2317 JOIN ".$BaseDatos.".G2317_M".$resultEstpas->muestr." ON G2317_ConsInte__b = G2317_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2317_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2317_ConsInte__b as id, G2317_C45300 as camp1 , G2317_C45301 as camp2 FROM ".$BaseDatos.".G2317 JOIN ".$BaseDatos.".G2317_M".$resultEstpas->muestr." ON G2317_ConsInte__b = G2317_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2317_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2317_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2317_ConsInte__b as id, G2317_C45300 as camp1 , G2317_C45301 as camp2 FROM ".$BaseDatos.".G2317  ORDER BY G2317_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){


    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="6622" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6622c">
                DATOS BASICOS
            </a>
        </h4>
        
    </div>
    <div id="s_6622c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C45300" id="LblG2317_C45300">NOMBRE TITULAR</label>
                        <input type="text" class="form-control input-sm" id="G2317_C45300" value="<?php if (isset($_GET['G2317_C45300'])) {
                            echo $_GET['G2317_C45300'];
                        } ?>"  name="G2317_C45300"  placeholder="NOMBRE TITULAR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C45301" id="LblG2317_C45301">NÚMERO DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2317_C45301" value="<?php if (isset($_GET['G2317_C45301'])) {
                            echo $_GET['G2317_C45301'];
                        } ?>"  name="G2317_C45301"  placeholder="NÚMERO DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C52557" id="LblG2317_C52557">CEDULA</label>
                        <input type="text" class="form-control input-sm" id="G2317_C52557" value="<?php if (isset($_GET['G2317_C52557'])) {
                            echo $_GET['G2317_C52557'];
                        } ?>"  name="G2317_C52557"  placeholder="CEDULA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="6624" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C45294" id="LblG2317_C45294">Agente</label>
                        <input type="text" class="form-control input-sm" id="G2317_C45294" value="<?php echo getNombreUser($token);?>" readonly name="G2317_C45294"  placeholder="Agente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C45295" id="LblG2317_C45295">Fecha</label>
                        <input type="text" class="form-control input-sm" id="G2317_C45295" value="<?php echo date('Y-m-d');?>" readonly name="G2317_C45295"  placeholder="Fecha">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C45296" id="LblG2317_C45296">Hora</label>
                        <input type="text" class="form-control input-sm" id="G2317_C45296" value="<?php echo date('H:i:s');?>" readonly name="G2317_C45296"  placeholder="Hora">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2317_C45297" id="LblG2317_C45297">Campaña</label>
                        <input type="text" class="form-control input-sm" id="G2317_C45297" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G2317_C45297"  placeholder="Campaña">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="6623" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="6626" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6626c">
                SALUDO
            </a>
        </h4>
        
    </div>
    <div id="s_6626c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días, tardes, noches, mi nombre es…. le llamo desde la Aurora Funerales y Capillas, con quien tengo el gusto de hablar?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr Encantado de saludarlo El motivo de nuestra llamada el día de hoy primero es brindarle un cordial saludo de parte de nuestra organización</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Nos gustaría saber quiénes de su familia NO cuentan con protección funeraria, o están desprotegidos, ya que desde este momento pueden contar con la protección</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6627" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6627c">
                SONDEO SI NO ESTA AFILIADO
            </a>
        </h4>
        
    </div>
    <div id="s_6627c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr..... sabe que es muy importante contar con este servicio. Ya que en caso de presentarse el fallecimiento de alguna persona de su grupo familiar y al no tener protección funeraria tendría que pagar de 3 a 4 millones o endeudarse para cubrir inmediatamente el servicio. Sr De cuantas personas se compone su núcleo familiar? Esperar respuesta…</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6628" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6628c">
                PROCESO OBLIGATORIO EN EL CIERRE DE VENTAS
            </a>
        </h4>
        
    </div>
    <div id="s_6628c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr. le recuerdo que nuestra llamada es grabada y monitoreada para efectos de calidad y legalidad entre las partes.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">El titular es quien debe reportar los datos del programa.(no el agente con la posibilidad de revisar planes anteriores).“POR FAVOR NO OMITIR UN SOLO PUNTO DE LA ACTUALIZACION DE DATOS BRINDAD PR EL CLIENTE” Sr…. su programa es un plan (aurora plus) que tiene un valor mensual de</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Tiene los siguientes servicios BASICOS:</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">1. El traslado Nacional del fallecido hasta ubicarlo en la sala de velación (según el plan)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">2. Preparación normal del cuerpo para una velación de hasta 24 horas (tanatopraxia) y el suministro de los implementos requeridos.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">3. Suministro del cofre según el plan.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">4. Diligencias civiles y eclésiasticas, que permitan la inhumación o la cremación.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">5. Habito si se requiere, (este debe solicitarse).</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">6. Servicio urbano de carroza para las exequias (siempre que este servicio se preste y sea de uso común en la localidad).</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">7. Una serie de avisos murales</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">8. Pago del servicio religioso</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">9. Servicio de sala de velación (tiempo continuo uso de la sala de acuerdo con la normatividad establecida por la alcaldía de cada localidad, hasta por 24 horas o implementos para la velación si esta es domiciliaria (Un cristo, dos velones, dos bases y dos candeleros) y servicios de cafetería (tinto y aromáticas).</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6629" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6629c">
                CLAUSULAS
            </a>
        </h4>
        
    </div>
    <div id="s_6629c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">1. Sr…. le recuerdo entonces que: La cláusula 1. Está en el resumen de venta.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">2. Si usted no solicita el servicio funerario a nuestra compañía, no tendrá derecho a ninguna compensación económica. (Ley 795, artículo 111 de 14 de Enero de 2003).</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">3. Los pagos efectuados que se generen por medio de este contrato telefónico en ningún momento se convierte en ahorro, por lo tanto no son reembolsables solo dan derecho a tomar o no los servicios acordados, a su elección.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">4. La no utilización de uno o varios servicios no obliga a nuestra compañía al reconocimiento económico por los no usados pudiendo el usuario utilizar, a su libre elección, uno, varios o todos los servicios y dejando en claro que Promotora La Aurora S.A. tiene a su disposición los servicios cada que el cliente lo requiera.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">La cláusula 5. Está en el resumen de venta</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">6. Si una persona se encuentra inscrita en dos o más contratos de previsión exequial será responsabilidad solo de titular o titulares de cada contrato la inscripción y permanencia en los mismos y solo tendrá derecho a la reclamación de un servicio, sin que esto genere pago de alguna compensación económica.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">La cláusula 7. Está en el resumen de venta.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Esta cláusula se lee únicamente cuando en el programa haya un bebe por nacer: 8*. Los naciturus deben ser registrados en esta llamada, como inscritos, de los cual queda el soporte en la grabación, para tener derecho al servicio de cofre y destino final de acuerdo al plan, especificando de quien es hijo y parentesco con el titular. Y que en caso de fallecimiento de la mamá y el bebe se prestara un sólo servicio.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">9. En caso de ocurrencia de catástrofes naturales, antrópicas o tecnológicas, Guerra interna o externa o por muerte colectiva La Aurora Funerales y Capillas, está exenta de cumplir con el presente acuerdo, para lo cual, en tales eventos prestará hasta un máximo de veinte (20) servicios exequiales, los que corresponderán a los primeros que sean solicitados; si la catástrofe afecta la infraestructura de la Aurora Funerales y Capillas, esta quedará exenta de cumplir con las obligaciones contenidas en el presente acuerdo sin que por ese hecho pueda exigírsele devolución alguna de las cuotas canceladas o por cualquier otro concepto derivado de este acuerdo.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">La cláusula 10. Esta. No se menciona.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">La cláusula 11. Está en el resumen de venta</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">(esto para cuando ingresan personas mayores de 66 años en adelante). ANTES NO APLICA Sr... le informo Que habra cambios en el valor del programa, cuando la persona mayor de 66 (3500) años cumpla sus 70 años se le incrementara tan solo 2500 pesos... y a sus 75 años habra otro incremento el cual sera opcional. (10000)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">INFORMAR PARA PROGRAMAS COMERCIALIZADOS POR MEDIO DE PAGO DIFERENTE AL MASIVO (OFICINA, RECAUDADOR ENTRE OTROS)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">13 y 14. El titular y los inscritos tendrán plena cobertura mientras sus edades sean inferiores a 74 años y 364 días. A partir de los 75 años la cobertura del presente acuerdo será del 50% del valor del servicio pactado, y para el destino final 1,0 smlv en caso de su fallecimiento; cuando el titular o cualquiera de sus inscritos supere los 75 años, tiene la opción de seguir Con la cobertura al 100% adquiriendo y cancelando un comodín por edad. En caso de utilizar el servicio deberá cancelar el resto de las cuotas del período contratado. La adquisición del adicional por edad es opcional para el titular del programa.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">INFORMAR PARA PROGRAMAS POR RECAUDO MASIVO (CHEC)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">15, 16. El inscrito al programa de previsión exequial a través de recaudo masivo que no pague en la fecha límite estipulada en la factura, deberá pagar lo correspondiente a dos períodos en las oficinas de la Aurora o en la siguiente factura, para seguir inscritos al plan de pago por recaudo masivo, recuerde sr. XXXX, que el NO pago oportuno ocasiona la perdida del servicio.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">17. Aplica para programas cancelados por oficina o con recaudador Para los programas cancelados en la oficina o mediante recuador, que no paguen en la fecha estipulada PROMOTORA LA AURORA S.A. le da vigencia al cubrimiento de las personas inscritas en el contrato de prevision exequial hasta las 24 horas del quinceavo día de la fecha de pago, para que cancele lo adeudado. A partir de allí los inscritos quedarán sin vigencia y en caso de presentarse un servicio quienes hayan cancelado sus cuotas atrasadas tendrán el soporte en la factura donde consta que nuestra compañía les presta el 50% de los servicios funerarios, sin incluir los gastos de inhumación o cremación, estos correrán por cuenta del cliente por un período de 15 días calendario, momento a partir del cual podrá acceder a los servicios completos conforme al programa adquirido.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">18. En los programas familiares y en los comercializados a través de recaudo masivo, en caso de fallecimiento de alguno de los aquí inscritos incluyendo el titular, durante el año contratado deberá cancelar el saldo de las cuotas restantes para tener derecho al servicio y no podrá reemplazarse la persona fallecida hasta el nuevo contrato.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Una Vez actualice o suministre sus datos, LA PROMOTORA LA AURORA S.A, realizara como un tratamiento relativo a su compilación, actualización y procesamiento</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6630" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6630c">
                ANEXO POST- VENTAS
            </a>
        </h4>
        
    </div>
    <div id="s_6630c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">1. El recaudo es a través del pago acordado (oficina, susuerte, efecty, recaudo, factura energía chec)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">2. Todas las personas aquí inscritas se encuentran en buen estado de salud?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">3. Acepta y está de acuerdo en su integridad todos los puntos y numerales que le he mencionado?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Informar para programas comercializados por recaudo masivo (chec):</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">1. Es consciente que además del concepto de energía en la próxima factura le llegara el valor adicional correspondiente a la previsión exequial? Además, que en caso de tener un corte de energía por no pago, queda inmediatamente suspendida la protección y el cobro del mes posterior, por lo cual debe dirigirse a nuestras oficinas a cancelar".</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Cualquier inquietud sobre el plan de previsión exequial, será atendido por un funcionario de la aurora funerales y capillas en nuestras oficinas o el representante en la localidad y no en las oficinas en la chec</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">NOTA: el anterior punto se debe mencionar para los programas comercializados para recaudo por un medio diferente a la factura CHEC.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Cualquier inquietud sobre el plan de previsión exequial, será atendido por un funcionario de la aurora funerales y capillas en nuestras oficinas o el representante en la localidad.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr. XXX le pregunto ha tomado usted está afiliación voluntariamente y ha escuchado y aceptado las condiciones y cláusulas del contrato?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">El pago de la cuota oportunamente es en todo caso, responsabilidad del titular. Las alternativas de recaudo ofrecidas por la Aurora no eximen al titular de su responsabilidad con el pago oportuno.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Con la lectura y aceptación del presente acuerdo, se sustituye cualquier otro suscrito entre las mismas partes y para los mismos fines.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6631" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6631c">
                HACER RESUMEN DE VENTA. OBLIGATORIO
            </a>
        </h4>
        
    </div>
    <div id="s_6631c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr. le recuerdo que nuestra llamada es grabada y monitoreada para efectos de calidad y legalidad entre las partes y lo aquí pactado</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Es tan amable de indicarme que fecha es hoy???</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">NOTA: SIRVE PARA HABEAS DATA Y CIFIN</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Para suscribir la afiliación inicial del plan adquirido de previsión exequial es necesario que usted auto- rice a LA AURORA con el fin de que se recolecte, almacene, use, trate, reporte sus datos personales y los comparta con sus empresas aliadas. De la misma forma autoriza a LA AURORA y sus empresas aliadas a enviarle información relacionada con actividades, programas, productos y servicios que sean promocionados por LA AURORA o empre- sas aliadas (Entonces usted si autoriza?, esto en caso de que el titular se quede callado cuando termi- nemos de pedir la autorización y no dar la opción del no.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">INFORMAR CUANDO ES POR RECUDO MASIVO (CHEC)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr(a)... autoriza usted a la Central Hidroeléctrica de Caldas empresa de servicios públicos, Chec, para cargar en la factura el valor de su previsión exequial tomado con promotora la aurora s.a por valor de…..pesos mensuales?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr(a). Me recuerda por favor su número de cédula, recuerde son... mensuales y cuando se cumpla el año se hará un incremento anual aproximado al IPC.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr. XXXX le recuerdo entonces que las personas inscritas en este programa son Únicamente las siguientes:</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Datos completos de: Nombre completo del titular, dirección teléfono y correo del titular, Nombre completo, edad y parentesco de cada uno de los inscritos</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Esta es la cláusula N. 1 Le informo las fechas de protección para usted como titular y sus inscritos son: Para usted como titular tiene protección inmediata por cualquier concepto de fallecimiento, excepto por suicidio que se cubrirá después de seis meses a partir de la fecha de inicio del programa. (Indicar fecha) Sus inscritos tendrán protección inmediata por muerte accidental, trágica o violenta; por muerte natural o enfermedad preexistente después de dos meses y un día (indicar fecha) y por suicidio se protegerán también en los seis meses siguientes a la fecha de inicio de protección.(indicar fecha).</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Las caracteristicas de su programa son: Cobertura… indicar cual Traslado en su plan (indicar cuál) hasta por 3 SMLV El destino final del cuerpo es: (indicar cuál) La elección de la funeraria es: (indicar cuál). La elección del cofre es: (indicar cuál)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Nota: en todos los casos indicar fechas.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Le informo sr XXXX que la modificación o cancelación de este contrato podrá realizarse hasta dentro de un año (se indica la fecha en el período de Nuevo contrato) y que en caso de necesitar de nuestros servicios comuníquese inmediatamente con nuestra área de servicios a la línea gratuita nacional 01 8000 916 966 o en Manizales al 8 99 77 00 o el cel de nuestra área de servicios 321 799 7030 o el 321 799 7012 a cualquier hora del día o de la noche durante los 365 días del año, indicando el nombre completo, el número de la cédula; Sr (a), tenga presente que en caso de presentarse un fallecimiento debe solicitar ante el médico tratante o a la institución responsable la expedición del certificado médico de defunción, esto como una condición de ley y de obligatorio cumplimiento previo a la custodia del cuerpo, para tener derecho a la prestación del servicio.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Solicitar referidos nacionales e internacionales.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6632" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6632c">
                DESPEDIDA
            </a>
        </h4>
        
    </div>
    <div id="s_6632c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Bienvenido! ... Le felicito es usted una persona muy responsable, porque esto es quererse usted y querer a su familia, nuestra línea gratuita de atención al cliente es 018000 916966, desde un celular no se puede marcar, pero usted también puede comunicarse a nuestro servicio por nuestro call center, al 8997700 en Manizales o en su municipio a tal teléfono!.. (Número de los teléfonos locales), las 24 horas del día.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Le recuerdo que mi nombre es...... de La Aurora Funerales y Capillas espero tenga buena tarde. Dia, noche muchas gracias por atenderme.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2317_C45289">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2562;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2317_C45289">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2562;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2317_C45290">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2317_C45291" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2317_C45292" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2317_C45293" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2317/G2317_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2317_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2317_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2317_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2317_C45300").val(item.G2317_C45300); 
                $("#G2317_C45301").val(item.G2317_C45301); 
                $("#G2317_C52557").val(item.G2317_C52557); 
                $("#G2317_C45289").val(item.G2317_C45289).trigger("change");  
                $("#G2317_C45290").val(item.G2317_C45290).trigger("change");  
                $("#G2317_C45291").val(item.G2317_C45291); 
                $("#G2317_C45292").val(item.G2317_C45292); 
                $("#G2317_C45293").val(item.G2317_C45293); 
                $("#G2317_C45294").val(item.G2317_C45294); 
                $("#G2317_C45295").val(item.G2317_C45295); 
                $("#G2317_C45296").val(item.G2317_C45296); 
                $("#G2317_C45297").val(item.G2317_C45297);   
                if(item.G2317_C45302 == 1){
                    $("#G2317_C45302").attr('checked', true);
                }    
                if(item.G2317_C45303 == 1){
                    $("#G2317_C45303").attr('checked', true);
                }    
                if(item.G2317_C45304 == 1){
                    $("#G2317_C45304").attr('checked', true);
                }    
                if(item.G2317_C45305 == 1){
                    $("#G2317_C45305").attr('checked', true);
                }    
                if(item.G2317_C45306 == 1){
                    $("#G2317_C45306").attr('checked', true);
                }    
                if(item.G2317_C45307 == 1){
                    $("#G2317_C45307").attr('checked', true);
                }    
                if(item.G2317_C45308 == 1){
                    $("#G2317_C45308").attr('checked', true);
                }    
                if(item.G2317_C45309 == 1){
                    $("#G2317_C45309").attr('checked', true);
                }    
                if(item.G2317_C45310 == 1){
                    $("#G2317_C45310").attr('checked', true);
                }    
                if(item.G2317_C45311 == 1){
                    $("#G2317_C45311").attr('checked', true);
                }    
                if(item.G2317_C45312 == 1){
                    $("#G2317_C45312").attr('checked', true);
                }    
                if(item.G2317_C45313 == 1){
                    $("#G2317_C45313").attr('checked', true);
                }    
                if(item.G2317_C45314 == 1){
                    $("#G2317_C45314").attr('checked', true);
                }    
                if(item.G2317_C45315 == 1){
                    $("#G2317_C45315").attr('checked', true);
                }    
                if(item.G2317_C45316 == 1){
                    $("#G2317_C45316").attr('checked', true);
                }    
                if(item.G2317_C45317 == 1){
                    $("#G2317_C45317").attr('checked', true);
                }    
                if(item.G2317_C45318 == 1){
                    $("#G2317_C45318").attr('checked', true);
                }    
                if(item.G2317_C45319 == 1){
                    $("#G2317_C45319").attr('checked', true);
                }    
                if(item.G2317_C45320 == 1){
                    $("#G2317_C45320").attr('checked', true);
                }    
                if(item.G2317_C45321 == 1){
                    $("#G2317_C45321").attr('checked', true);
                }    
                if(item.G2317_C45322 == 1){
                    $("#G2317_C45322").attr('checked', true);
                }    
                if(item.G2317_C45323 == 1){
                    $("#G2317_C45323").attr('checked', true);
                }    
                if(item.G2317_C45324 == 1){
                    $("#G2317_C45324").attr('checked', true);
                }    
                if(item.G2317_C45325 == 1){
                    $("#G2317_C45325").attr('checked', true);
                }    
                if(item.G2317_C45326 == 1){
                    $("#G2317_C45326").attr('checked', true);
                }    
                if(item.G2317_C45327 == 1){
                    $("#G2317_C45327").attr('checked', true);
                }    
                if(item.G2317_C45328 == 1){
                    $("#G2317_C45328").attr('checked', true);
                }    
                if(item.G2317_C45329 == 1){
                    $("#G2317_C45329").attr('checked', true);
                }    
                if(item.G2317_C45330 == 1){
                    $("#G2317_C45330").attr('checked', true);
                }    
                if(item.G2317_C45331 == 1){
                    $("#G2317_C45331").attr('checked', true);
                }    
                if(item.G2317_C45332 == 1){
                    $("#G2317_C45332").attr('checked', true);
                }    
                if(item.G2317_C45333 == 1){
                    $("#G2317_C45333").attr('checked', true);
                }    
                if(item.G2317_C45334 == 1){
                    $("#G2317_C45334").attr('checked', true);
                }    
                if(item.G2317_C45335 == 1){
                    $("#G2317_C45335").attr('checked', true);
                }    
                if(item.G2317_C45336 == 1){
                    $("#G2317_C45336").attr('checked', true);
                }    
                if(item.G2317_C45337 == 1){
                    $("#G2317_C45337").attr('checked', true);
                }    
                if(item.G2317_C45338 == 1){
                    $("#G2317_C45338").attr('checked', true);
                }    
                if(item.G2317_C45339 == 1){
                    $("#G2317_C45339").attr('checked', true);
                }    
                if(item.G2317_C45340 == 1){
                    $("#G2317_C45340").attr('checked', true);
                }    
                if(item.G2317_C45341 == 1){
                    $("#G2317_C45341").attr('checked', true);
                }    
                if(item.G2317_C45342 == 1){
                    $("#G2317_C45342").attr('checked', true);
                }    
                if(item.G2317_C45343 == 1){
                    $("#G2317_C45343").attr('checked', true);
                }    
                if(item.G2317_C45344 == 1){
                    $("#G2317_C45344").attr('checked', true);
                }    
                if(item.G2317_C45345 == 1){
                    $("#G2317_C45345").attr('checked', true);
                }    
                if(item.G2317_C45346 == 1){
                    $("#G2317_C45346").attr('checked', true);
                }    
                if(item.G2317_C45347 == 1){
                    $("#G2317_C45347").attr('checked', true);
                }    
                if(item.G2317_C45348 == 1){
                    $("#G2317_C45348").attr('checked', true);
                }    
                if(item.G2317_C45349 == 1){
                    $("#G2317_C45349").attr('checked', true);
                }    
                if(item.G2317_C45350 == 1){
                    $("#G2317_C45350").attr('checked', true);
                }    
                if(item.G2317_C45351 == 1){
                    $("#G2317_C45351").attr('checked', true);
                }    
                if(item.G2317_C45352 == 1){
                    $("#G2317_C45352").attr('checked', true);
                }    
                if(item.G2317_C45353 == 1){
                    $("#G2317_C45353").attr('checked', true);
                }    
                if(item.G2317_C45354 == 1){
                    $("#G2317_C45354").attr('checked', true);
                }    
                if(item.G2317_C45355 == 1){
                    $("#G2317_C45355").attr('checked', true);
                }    
                if(item.G2317_C45356 == 1){
                    $("#G2317_C45356").attr('checked', true);
                }    
                if(item.G2317_C45357 == 1){
                    $("#G2317_C45357").attr('checked', true);
                }    
                if(item.G2317_C45358 == 1){
                    $("#G2317_C45358").attr('checked', true);
                }    
                if(item.G2317_C45359 == 1){
                    $("#G2317_C45359").attr('checked', true);
                }    
                if(item.G2317_C45360 == 1){
                    $("#G2317_C45360").attr('checked', true);
                }    
                if(item.G2317_C45361 == 1){
                    $("#G2317_C45361").attr('checked', true);
                }    
                if(item.G2317_C45362 == 1){
                    $("#G2317_C45362").attr('checked', true);
                }    
                if(item.G2317_C45363 == 1){
                    $("#G2317_C45363").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        

        //datepickers
        

        $("#G2317_C45291").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2317_C45292").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        

        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2317_C45300").val(item.G2317_C45300);
 
                                                $("#G2317_C45301").val(item.G2317_C45301);
 
                                                $("#G2317_C52557").val(item.G2317_C52557);
 
                    $("#G2317_C45289").val(item.G2317_C45289).trigger("change"); 
 
                    $("#G2317_C45290").val(item.G2317_C45290).trigger("change"); 
 
                                                $("#G2317_C45291").val(item.G2317_C45291);
 
                                                $("#G2317_C45292").val(item.G2317_C45292);
 
                                                $("#G2317_C45293").val(item.G2317_C45293);
 
                                                $("#G2317_C45294").val(item.G2317_C45294);
 
                                                $("#G2317_C45295").val(item.G2317_C45295);
 
                                                $("#G2317_C45296").val(item.G2317_C45296);
 
                                                $("#G2317_C45297").val(item.G2317_C45297);
      
                                                if(item.G2317_C45302 == 1){
                                                   $("#G2317_C45302").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45303 == 1){
                                                   $("#G2317_C45303").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45304 == 1){
                                                   $("#G2317_C45304").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45305 == 1){
                                                   $("#G2317_C45305").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45306 == 1){
                                                   $("#G2317_C45306").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45307 == 1){
                                                   $("#G2317_C45307").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45308 == 1){
                                                   $("#G2317_C45308").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45309 == 1){
                                                   $("#G2317_C45309").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45310 == 1){
                                                   $("#G2317_C45310").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45311 == 1){
                                                   $("#G2317_C45311").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45312 == 1){
                                                   $("#G2317_C45312").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45313 == 1){
                                                   $("#G2317_C45313").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45314 == 1){
                                                   $("#G2317_C45314").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45315 == 1){
                                                   $("#G2317_C45315").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45316 == 1){
                                                   $("#G2317_C45316").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45317 == 1){
                                                   $("#G2317_C45317").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45318 == 1){
                                                   $("#G2317_C45318").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45319 == 1){
                                                   $("#G2317_C45319").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45320 == 1){
                                                   $("#G2317_C45320").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45321 == 1){
                                                   $("#G2317_C45321").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45322 == 1){
                                                   $("#G2317_C45322").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45323 == 1){
                                                   $("#G2317_C45323").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45324 == 1){
                                                   $("#G2317_C45324").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45325 == 1){
                                                   $("#G2317_C45325").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45326 == 1){
                                                   $("#G2317_C45326").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45327 == 1){
                                                   $("#G2317_C45327").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45328 == 1){
                                                   $("#G2317_C45328").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45329 == 1){
                                                   $("#G2317_C45329").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45330 == 1){
                                                   $("#G2317_C45330").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45331 == 1){
                                                   $("#G2317_C45331").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45332 == 1){
                                                   $("#G2317_C45332").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45333 == 1){
                                                   $("#G2317_C45333").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45334 == 1){
                                                   $("#G2317_C45334").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45335 == 1){
                                                   $("#G2317_C45335").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45336 == 1){
                                                   $("#G2317_C45336").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45337 == 1){
                                                   $("#G2317_C45337").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45338 == 1){
                                                   $("#G2317_C45338").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45339 == 1){
                                                   $("#G2317_C45339").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45340 == 1){
                                                   $("#G2317_C45340").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45341 == 1){
                                                   $("#G2317_C45341").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45342 == 1){
                                                   $("#G2317_C45342").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45343 == 1){
                                                   $("#G2317_C45343").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45344 == 1){
                                                   $("#G2317_C45344").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45345 == 1){
                                                   $("#G2317_C45345").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45346 == 1){
                                                   $("#G2317_C45346").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45347 == 1){
                                                   $("#G2317_C45347").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45348 == 1){
                                                   $("#G2317_C45348").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45349 == 1){
                                                   $("#G2317_C45349").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45350 == 1){
                                                   $("#G2317_C45350").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45351 == 1){
                                                   $("#G2317_C45351").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45352 == 1){
                                                   $("#G2317_C45352").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45353 == 1){
                                                   $("#G2317_C45353").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45354 == 1){
                                                   $("#G2317_C45354").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45355 == 1){
                                                   $("#G2317_C45355").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45356 == 1){
                                                   $("#G2317_C45356").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45357 == 1){
                                                   $("#G2317_C45357").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45358 == 1){
                                                   $("#G2317_C45358").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45359 == 1){
                                                   $("#G2317_C45359").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45360 == 1){
                                                   $("#G2317_C45360").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45361 == 1){
                                                   $("#G2317_C45361").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45362 == 1){
                                                   $("#G2317_C45362").attr('checked', true);
                                                } 
      
                                                if(item.G2317_C45363 == 1){
                                                   $("#G2317_C45363").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','NOMBRE TITULAR','NÚMERO DE CONTACTO','CEDULA','Agente','Fecha','Hora','Campaña'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2317_C45300', 
                        index: 'G2317_C45300', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2317_C45301', 
                        index: 'G2317_C45301', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2317_C52557', 
                        index: 'G2317_C52557', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2317_C45294', 
                        index: 'G2317_C45294', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2317_C45295', 
                        index: 'G2317_C45295', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2317_C45296', 
                        index: 'G2317_C45296', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2317_C45297', 
                        index: 'G2317_C45297', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2317_C45300',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2317_C45300").val(item.G2317_C45300);

                        $("#G2317_C45301").val(item.G2317_C45301);

                        $("#G2317_C52557").val(item.G2317_C52557);
 
                    $("#G2317_C45289").val(item.G2317_C45289).trigger("change"); 
 
                    $("#G2317_C45290").val(item.G2317_C45290).trigger("change"); 

                        $("#G2317_C45291").val(item.G2317_C45291);

                        $("#G2317_C45292").val(item.G2317_C45292);

                        $("#G2317_C45293").val(item.G2317_C45293);

                        $("#G2317_C45294").val(item.G2317_C45294);

                        $("#G2317_C45295").val(item.G2317_C45295);

                        $("#G2317_C45296").val(item.G2317_C45296);

                        $("#G2317_C45297").val(item.G2317_C45297);
    
                        if(item.G2317_C45302 == 1){
                           $("#G2317_C45302").attr('checked', true);
                        } 
    
                        if(item.G2317_C45303 == 1){
                           $("#G2317_C45303").attr('checked', true);
                        } 
    
                        if(item.G2317_C45304 == 1){
                           $("#G2317_C45304").attr('checked', true);
                        } 
    
                        if(item.G2317_C45305 == 1){
                           $("#G2317_C45305").attr('checked', true);
                        } 
    
                        if(item.G2317_C45306 == 1){
                           $("#G2317_C45306").attr('checked', true);
                        } 
    
                        if(item.G2317_C45307 == 1){
                           $("#G2317_C45307").attr('checked', true);
                        } 
    
                        if(item.G2317_C45308 == 1){
                           $("#G2317_C45308").attr('checked', true);
                        } 
    
                        if(item.G2317_C45309 == 1){
                           $("#G2317_C45309").attr('checked', true);
                        } 
    
                        if(item.G2317_C45310 == 1){
                           $("#G2317_C45310").attr('checked', true);
                        } 
    
                        if(item.G2317_C45311 == 1){
                           $("#G2317_C45311").attr('checked', true);
                        } 
    
                        if(item.G2317_C45312 == 1){
                           $("#G2317_C45312").attr('checked', true);
                        } 
    
                        if(item.G2317_C45313 == 1){
                           $("#G2317_C45313").attr('checked', true);
                        } 
    
                        if(item.G2317_C45314 == 1){
                           $("#G2317_C45314").attr('checked', true);
                        } 
    
                        if(item.G2317_C45315 == 1){
                           $("#G2317_C45315").attr('checked', true);
                        } 
    
                        if(item.G2317_C45316 == 1){
                           $("#G2317_C45316").attr('checked', true);
                        } 
    
                        if(item.G2317_C45317 == 1){
                           $("#G2317_C45317").attr('checked', true);
                        } 
    
                        if(item.G2317_C45318 == 1){
                           $("#G2317_C45318").attr('checked', true);
                        } 
    
                        if(item.G2317_C45319 == 1){
                           $("#G2317_C45319").attr('checked', true);
                        } 
    
                        if(item.G2317_C45320 == 1){
                           $("#G2317_C45320").attr('checked', true);
                        } 
    
                        if(item.G2317_C45321 == 1){
                           $("#G2317_C45321").attr('checked', true);
                        } 
    
                        if(item.G2317_C45322 == 1){
                           $("#G2317_C45322").attr('checked', true);
                        } 
    
                        if(item.G2317_C45323 == 1){
                           $("#G2317_C45323").attr('checked', true);
                        } 
    
                        if(item.G2317_C45324 == 1){
                           $("#G2317_C45324").attr('checked', true);
                        } 
    
                        if(item.G2317_C45325 == 1){
                           $("#G2317_C45325").attr('checked', true);
                        } 
    
                        if(item.G2317_C45326 == 1){
                           $("#G2317_C45326").attr('checked', true);
                        } 
    
                        if(item.G2317_C45327 == 1){
                           $("#G2317_C45327").attr('checked', true);
                        } 
    
                        if(item.G2317_C45328 == 1){
                           $("#G2317_C45328").attr('checked', true);
                        } 
    
                        if(item.G2317_C45329 == 1){
                           $("#G2317_C45329").attr('checked', true);
                        } 
    
                        if(item.G2317_C45330 == 1){
                           $("#G2317_C45330").attr('checked', true);
                        } 
    
                        if(item.G2317_C45331 == 1){
                           $("#G2317_C45331").attr('checked', true);
                        } 
    
                        if(item.G2317_C45332 == 1){
                           $("#G2317_C45332").attr('checked', true);
                        } 
    
                        if(item.G2317_C45333 == 1){
                           $("#G2317_C45333").attr('checked', true);
                        } 
    
                        if(item.G2317_C45334 == 1){
                           $("#G2317_C45334").attr('checked', true);
                        } 
    
                        if(item.G2317_C45335 == 1){
                           $("#G2317_C45335").attr('checked', true);
                        } 
    
                        if(item.G2317_C45336 == 1){
                           $("#G2317_C45336").attr('checked', true);
                        } 
    
                        if(item.G2317_C45337 == 1){
                           $("#G2317_C45337").attr('checked', true);
                        } 
    
                        if(item.G2317_C45338 == 1){
                           $("#G2317_C45338").attr('checked', true);
                        } 
    
                        if(item.G2317_C45339 == 1){
                           $("#G2317_C45339").attr('checked', true);
                        } 
    
                        if(item.G2317_C45340 == 1){
                           $("#G2317_C45340").attr('checked', true);
                        } 
    
                        if(item.G2317_C45341 == 1){
                           $("#G2317_C45341").attr('checked', true);
                        } 
    
                        if(item.G2317_C45342 == 1){
                           $("#G2317_C45342").attr('checked', true);
                        } 
    
                        if(item.G2317_C45343 == 1){
                           $("#G2317_C45343").attr('checked', true);
                        } 
    
                        if(item.G2317_C45344 == 1){
                           $("#G2317_C45344").attr('checked', true);
                        } 
    
                        if(item.G2317_C45345 == 1){
                           $("#G2317_C45345").attr('checked', true);
                        } 
    
                        if(item.G2317_C45346 == 1){
                           $("#G2317_C45346").attr('checked', true);
                        } 
    
                        if(item.G2317_C45347 == 1){
                           $("#G2317_C45347").attr('checked', true);
                        } 
    
                        if(item.G2317_C45348 == 1){
                           $("#G2317_C45348").attr('checked', true);
                        } 
    
                        if(item.G2317_C45349 == 1){
                           $("#G2317_C45349").attr('checked', true);
                        } 
    
                        if(item.G2317_C45350 == 1){
                           $("#G2317_C45350").attr('checked', true);
                        } 
    
                        if(item.G2317_C45351 == 1){
                           $("#G2317_C45351").attr('checked', true);
                        } 
    
                        if(item.G2317_C45352 == 1){
                           $("#G2317_C45352").attr('checked', true);
                        } 
    
                        if(item.G2317_C45353 == 1){
                           $("#G2317_C45353").attr('checked', true);
                        } 
    
                        if(item.G2317_C45354 == 1){
                           $("#G2317_C45354").attr('checked', true);
                        } 
    
                        if(item.G2317_C45355 == 1){
                           $("#G2317_C45355").attr('checked', true);
                        } 
    
                        if(item.G2317_C45356 == 1){
                           $("#G2317_C45356").attr('checked', true);
                        } 
    
                        if(item.G2317_C45357 == 1){
                           $("#G2317_C45357").attr('checked', true);
                        } 
    
                        if(item.G2317_C45358 == 1){
                           $("#G2317_C45358").attr('checked', true);
                        } 
    
                        if(item.G2317_C45359 == 1){
                           $("#G2317_C45359").attr('checked', true);
                        } 
    
                        if(item.G2317_C45360 == 1){
                           $("#G2317_C45360").attr('checked', true);
                        } 
    
                        if(item.G2317_C45361 == 1){
                           $("#G2317_C45361").attr('checked', true);
                        } 
    
                        if(item.G2317_C45362 == 1){
                           $("#G2317_C45362").attr('checked', true);
                        } 
    
                        if(item.G2317_C45363 == 1){
                           $("#G2317_C45363").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
