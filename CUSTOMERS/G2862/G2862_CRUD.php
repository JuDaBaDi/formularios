<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2862;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2862
                  SET G2862_C = -201
                  WHERE G2862_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2862 
                    WHERE G2862_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2862_C"]) || $gestion["G2862_C"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2862_C"];
        }

        if (is_null($gestion["G2862_C"]) || $gestion["G2862_C"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2862_C"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2862_FechaInsercion"]."',".$gestion["G2862_Usuario"].",'".$gestion["G2862_C".$P["P"]]."','".$gestion["G2862_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "customers.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>Añadir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2862 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2862_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2862_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2862_LinkContenido as url FROM ".$BaseDatos.".G2862 WHERE G2862_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2862_ConsInte__b, G2862_FechaInsercion , G2862_Usuario ,  G2862_CodigoMiembro  , G2862_PoblacionOrigen , G2862_EstadoDiligenciamiento ,  G2862_IdLlamada , G2862_C55379 as principal ,G2862_C55379,G2862_C55380,G2862_C55381,G2862_C55382,G2862_C55383,G2862_C55384,G2862_C55385,G2862_C55386,G2862_C55387,G2862_C55388,G2862_C55389,G2862_C55403,G2862_C55404,G2862_C55405,G2862_C55390,G2862_C55391,G2862_C55392,G2862_C55393,G2862_C55394,G2862_C55395,G2862_C55396,G2862_C55397,G2862_C55398 FROM '.$BaseDatos.'.G2862 WHERE G2862_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2862_C55379'] = $key->G2862_C55379;

                $datos[$i]['G2862_C55380'] = $key->G2862_C55380;

                $datos[$i]['G2862_C55381'] = $key->G2862_C55381;

                $datos[$i]['G2862_C55382'] = $key->G2862_C55382;

                $datos[$i]['G2862_C55383'] = $key->G2862_C55383;

                $datos[$i]['G2862_C55384'] = $key->G2862_C55384;

                $datos[$i]['G2862_C55385'] = $key->G2862_C55385;

                $datos[$i]['G2862_C55386'] = $key->G2862_C55386;

                $datos[$i]['G2862_C55387'] = $key->G2862_C55387;

                $datos[$i]['G2862_C55388'] = $key->G2862_C55388;

                $datos[$i]['G2862_C55389'] = $key->G2862_C55389;

                $datos[$i]['G2862_C55403'] = $key->G2862_C55403;

                $datos[$i]['G2862_C55404'] = $key->G2862_C55404;

                $datos[$i]['G2862_C55405'] = explode(' ', $key->G2862_C55405)[0];

                $datos[$i]['G2862_C55390'] = $key->G2862_C55390;

                $datos[$i]['G2862_C55391'] = $key->G2862_C55391;

                $datos[$i]['G2862_C55392'] = explode(' ', $key->G2862_C55392)[0];
  
                $hora = '';
                if(!is_null($key->G2862_C55393)){
                    $hora = explode(' ', $key->G2862_C55393)[1];
                }

                $datos[$i]['G2862_C55393'] = $hora;

                $datos[$i]['G2862_C55394'] = $key->G2862_C55394;

                $datos[$i]['G2862_C55395'] = $key->G2862_C55395;

                $datos[$i]['G2862_C55396'] = $key->G2862_C55396;

                $datos[$i]['G2862_C55397'] = $key->G2862_C55397;

                $datos[$i]['G2862_C55398'] = $key->G2862_C55398;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2862";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2862_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2862_ConsInte__b as id,  G2862_C55379 as camp1 , G2862_C55380 as camp2 
                     FROM ".$BaseDatos.".G2862  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2862_ConsInte__b as id,  G2862_C55379 as camp1 , G2862_C55380 as camp2  
                    FROM ".$BaseDatos.".G2862  JOIN ".$BaseDatos.".G2862_M".$_POST['muestra']." ON G2862_ConsInte__b = G2862_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2862_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2862_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2862_C55379 LIKE '%".$B."%' OR G2862_C55380 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2862_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2862");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2862_ConsInte__b, G2862_FechaInsercion , G2862_Usuario ,  G2862_CodigoMiembro  , G2862_PoblacionOrigen , G2862_EstadoDiligenciamiento ,  G2862_IdLlamada , G2862_C55379 as principal ,G2862_C55379,G2862_C55380,G2862_C55381,G2862_C55382,G2862_C55383,G2862_C55384,G2862_C55385,G2862_C55386,G2862_C55387,G2862_C55388,G2862_C55389,G2862_C55403, a.LISOPC_Nombre____b as G2862_C55404,G2862_C55405, b.LISOPC_Nombre____b as G2862_C55390, c.LISOPC_Nombre____b as G2862_C55391,G2862_C55392,G2862_C55393,G2862_C55394,G2862_C55395,G2862_C55396,G2862_C55397,G2862_C55398 FROM '.$BaseDatos.'.G2862 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2862_C55404 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2862_C55390 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2862_C55391';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2862_C55393)){
                    $hora_a = explode(' ', $fila->G2862_C55393)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2862_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2862_ConsInte__b , ($fila->G2862_C55379) , ($fila->G2862_C55380) , ($fila->G2862_C55381) , ($fila->G2862_C55382) , ($fila->G2862_C55383) , ($fila->G2862_C55384) , ($fila->G2862_C55385) , ($fila->G2862_C55386) , ($fila->G2862_C55387) , ($fila->G2862_C55388) , ($fila->G2862_C55389) , ($fila->G2862_C55403) , ($fila->G2862_C55404) , explode(' ', $fila->G2862_C55405)[0] , ($fila->G2862_C55390) , ($fila->G2862_C55391) , explode(' ', $fila->G2862_C55392)[0] , $hora_a , ($fila->G2862_C55394) , ($fila->G2862_C55395) , ($fila->G2862_C55396) , ($fila->G2862_C55397) , ($fila->G2862_C55398) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2862 WHERE G2862_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2862";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2862_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2862_ConsInte__b as id,  G2862_C55379 as camp1 , G2862_C55380 as camp2  FROM '.$BaseDatos.'.G2862 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2862_ConsInte__b as id,  G2862_C55379 as camp1 , G2862_C55380 as camp2  
                    FROM ".$BaseDatos.".G2862  JOIN ".$BaseDatos.".G2862_M".$_POST['muestra']." ON G2862_ConsInte__b = G2862_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2862_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2862_C55379 LIKE "%'.$B.'%" OR G2862_C55380 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2862_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2862 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2862(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2862_C55379"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55379 = '".$_POST["G2862_C55379"]."'";
                $LsqlI .= $separador."G2862_C55379";
                $LsqlV .= $separador."'".$_POST["G2862_C55379"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55380"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55380 = '".$_POST["G2862_C55380"]."'";
                $LsqlI .= $separador."G2862_C55380";
                $LsqlV .= $separador."'".$_POST["G2862_C55380"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55381"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55381 = '".$_POST["G2862_C55381"]."'";
                $LsqlI .= $separador."G2862_C55381";
                $LsqlV .= $separador."'".$_POST["G2862_C55381"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55382"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55382 = '".$_POST["G2862_C55382"]."'";
                $LsqlI .= $separador."G2862_C55382";
                $LsqlV .= $separador."'".$_POST["G2862_C55382"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55383"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55383 = '".$_POST["G2862_C55383"]."'";
                $LsqlI .= $separador."G2862_C55383";
                $LsqlV .= $separador."'".$_POST["G2862_C55383"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55384"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55384 = '".$_POST["G2862_C55384"]."'";
                $LsqlI .= $separador."G2862_C55384";
                $LsqlV .= $separador."'".$_POST["G2862_C55384"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55385"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55385 = '".$_POST["G2862_C55385"]."'";
                $LsqlI .= $separador."G2862_C55385";
                $LsqlV .= $separador."'".$_POST["G2862_C55385"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55386"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55386 = '".$_POST["G2862_C55386"]."'";
                $LsqlI .= $separador."G2862_C55386";
                $LsqlV .= $separador."'".$_POST["G2862_C55386"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55387"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55387 = '".$_POST["G2862_C55387"]."'";
                $LsqlI .= $separador."G2862_C55387";
                $LsqlV .= $separador."'".$_POST["G2862_C55387"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55388"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55388 = '".$_POST["G2862_C55388"]."'";
                $LsqlI .= $separador."G2862_C55388";
                $LsqlV .= $separador."'".$_POST["G2862_C55388"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55389"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55389 = '".$_POST["G2862_C55389"]."'";
                $LsqlI .= $separador."G2862_C55389";
                $LsqlV .= $separador."'".$_POST["G2862_C55389"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55403"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55403 = '".$_POST["G2862_C55403"]."'";
                $LsqlI .= $separador."G2862_C55403";
                $LsqlV .= $separador."'".$_POST["G2862_C55403"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55404"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55404 = '".$_POST["G2862_C55404"]."'";
                $LsqlI .= $separador."G2862_C55404";
                $LsqlV .= $separador."'".$_POST["G2862_C55404"]."'";
                $validar = 1;
            }
             
 
            $G2862_C55405 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2862_C55405"])){    
                if($_POST["G2862_C55405"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2862_C55405"]);
                    if(count($tieneHora) > 1){
                        $G2862_C55405 = "'".$_POST["G2862_C55405"]."'";
                    }else{
                        $G2862_C55405 = "'".str_replace(' ', '',$_POST["G2862_C55405"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2862_C55405 = ".$G2862_C55405;
                    $LsqlI .= $separador." G2862_C55405";
                    $LsqlV .= $separador.$G2862_C55405;
                    $validar = 1;
                }
            }
 
            $G2862_C55390 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2862_C55390 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2862_C55390 = ".$G2862_C55390;
                    $LsqlI .= $separador." G2862_C55390";
                    $LsqlV .= $separador.$G2862_C55390;
                    $validar = 1;

                    
                }
            }
 
            $G2862_C55391 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2862_C55391 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2862_C55391 = ".$G2862_C55391;
                    $LsqlI .= $separador." G2862_C55391";
                    $LsqlV .= $separador.$G2862_C55391;
                    $validar = 1;
                }
            }
 
            $G2862_C55392 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2862_C55392 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2862_C55392 = ".$G2862_C55392;
                    $LsqlI .= $separador." G2862_C55392";
                    $LsqlV .= $separador.$G2862_C55392;
                    $validar = 1;
                }
            }
 
            $G2862_C55393 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2862_C55393 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2862_C55393 = ".$G2862_C55393;
                    $LsqlI .= $separador." G2862_C55393";
                    $LsqlV .= $separador.$G2862_C55393;
                    $validar = 1;
                }
            }
 
            $G2862_C55394 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2862_C55394 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2862_C55394 = ".$G2862_C55394;
                    $LsqlI .= $separador." G2862_C55394";
                    $LsqlV .= $separador.$G2862_C55394;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2862_C55395"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55395 = '".$_POST["G2862_C55395"]."'";
                $LsqlI .= $separador."G2862_C55395";
                $LsqlV .= $separador."'".$_POST["G2862_C55395"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55396"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55396 = '".$_POST["G2862_C55396"]."'";
                $LsqlI .= $separador."G2862_C55396";
                $LsqlV .= $separador."'".$_POST["G2862_C55396"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55397"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55397 = '".$_POST["G2862_C55397"]."'";
                $LsqlI .= $separador."G2862_C55397";
                $LsqlV .= $separador."'".$_POST["G2862_C55397"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55398"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55398 = '".$_POST["G2862_C55398"]."'";
                $LsqlI .= $separador."G2862_C55398";
                $LsqlV .= $separador."'".$_POST["G2862_C55398"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55399"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55399 = '".$_POST["G2862_C55399"]."'";
                $LsqlI .= $separador."G2862_C55399";
                $LsqlV .= $separador."'".$_POST["G2862_C55399"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55401"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55401 = '".$_POST["G2862_C55401"]."'";
                $LsqlI .= $separador."G2862_C55401";
                $LsqlV .= $separador."'".$_POST["G2862_C55401"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2862_C55402"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_C55402 = '".$_POST["G2862_C55402"]."'";
                $LsqlI .= $separador."G2862_C55402";
                $LsqlV .= $separador."'".$_POST["G2862_C55402"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2862_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2862_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2862_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2862_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2862_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2862_Usuario , G2862_FechaInsercion, G2862_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2862_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2862 WHERE G2862_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
