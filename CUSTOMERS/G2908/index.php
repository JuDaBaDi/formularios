<?php 
    /*
        Document   : index
        Created on : 2020-08-31 15:25:02
        Author     : Jose David y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjkwOA==  
    */
    $url_crud =  "formularios/G2908/G2908_CRUD_web.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }

            form#formLogin h3{
                font-size: 14px;
            }
        </style>
    </head>
    <?php  
        if(isset($_GET['v'])){
            
        }else{
            echo '<body class="hold-transition" >';
        }
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <?php if(!isset($_GET['aceptaTerminos'])){ ?>
                    <div class="login-logo hed">
                        <?php if(!isset($_GET['v'])){ ?>
                        <img src="/img_usr/66.jpg"  alt="Dyalogo">
                        <?php }else{ 
                                
                            }
                        ?>

                    </div><!-- /.login-logo -->
                    <?php if(isset($_GET['v'])){ 
                        
                    }else { ?>
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
					<?php } ?>
                        <form action="formularios/G2908/G2908_CRUD_web.php" method="post" id="formLogin" enctype="multipart/form-data">

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56458" id="LblG2908_C56458">TIPO DOCUMENTO*</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56458" id="G2908_C56458" required>
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3422 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56459" id="LblG2908_C56459">DOCUMENTO IDENTIDAD*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56459" value=""  name="G2908_C56459"  placeholder="DOCUMENTO IDENTIDAD" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2908_C56460" id="LblG2908_C56460">FECHA NACIMIENTO*</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2908_C56460" id="G2908_C56460" placeholder="YYYY-MM-DD" required>
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56461" id="LblG2908_C56461">EDAD*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56461" value=""  name="G2908_C56461"  placeholder="EDAD" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56462" id="LblG2908_C56462">PRIMER NOMBRE*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56462" value=""  name="G2908_C56462"  placeholder="PRIMER NOMBRE" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56463" id="LblG2908_C56463">SEGUNDO NOMBRE*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56463" value=""  name="G2908_C56463"  placeholder="SEGUNDO NOMBRE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56464" id="LblG2908_C56464">PRIMER APELLIDO*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56464" value=""  name="G2908_C56464"  placeholder="PRIMER APELLIDO" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56465" id="LblG2908_C56465">SEGUNDO APELLIDO*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56465" value=""  name="G2908_C56465"  placeholder="SEGUNDO APELLIDO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56466" id="LblG2908_C56466">DIRECCION*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56466" value=""  name="G2908_C56466"  placeholder="DIRECCION" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56467" id="LblG2908_C56467">TELEFONO DE CONTACTO*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56467" value=""  name="G2908_C56467"  placeholder="TELEFONO DE CONTACTO" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56468" id="LblG2908_C56468">OTRO TELEFONO DE CONTACTO*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56468" value=""  name="G2908_C56468"  placeholder="OTRO TELEFONO DE CONTACTO" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO EMAIL -->
                            <div class="form-group">
                                <label for="G2908_C56469" id="LblG2908_C56469">CORREO ELECTRONICO*</label>
                                <input type="email" class="form-control input-sm" id="G2908_C56469" value=""  name="G2908_C56469"  placeholder="CORREO ELECTRONICO" required>
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2908_C56470" id="LblG2908_C56470">DIAGNOSTICO*</label>
								<input type="text" class="form-control input-sm" id="G2908_C56470" value=""  name="G2908_C56470"  placeholder="DIAGNOSTICO" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56471" id="LblG2908_C56471">SUFRE ALGUNA  ENFERMEDAD COMO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56471" id="G2908_C56471" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3423 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2908_C56472" id="LblG2908_C56472">NUMERO DE AUTORIZACION - En caso de no contar con numero de autorizacion, favor tramitarla con la EPS o dirijase al primer piso de la clinica. Muchas Gracias*</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G2908_C56472" id="G2908_C56472" placeholder="NUMERO DE AUTORIZACION - En caso de no contar con numero de autorizacion, favor tramitarla con la EPS o dirijase al primer piso de la clinica. Muchas Gracias" required>
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56473" id="LblG2908_C56473">EPS*</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56473" id="G2908_C56473" required>
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3424 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56474" id="LblG2908_C56474">CITA QUE REQUIERE*</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56474" id="G2908_C56474" required>
                            <option value="0">Seleccione</option>
                            <option value="40114">Cirugia Maxilofacial</option>
                            <option value="40117">Consulta Anestesia</option>
                            <option value="40118">Consulta de cirugia general</option>
                            <option value="40119">Consulta de Ginecologia</option>
                            <option value="40120">Consulta Urologia</option>
                            <option value="43465">Fisiatria</option>
                            <option value="40122">Neurocirugia</option>
                            <option value="40123">Neurologia</option>
                            <option value="40130">Ortopedia Rodilla</option>
                            <option value="40127">Ortopedia Hombro</option>
                            <option value="40128">Ortopedia Otros</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="G2908_C62209" id="LblG2908_C62209">ESPECIALISTA*</label>
                        <input type="text" class="form-control input-sm" id="G2908_C62209" value=""  name="G2908_C62209"  placeholder="ESPECIALISTA" readonly>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2908_C56475" id="LblG2908_C56475">OBSERVACIONES*</label>
                                <textarea class="form-control input-sm" name="G2908_C56475" id="G2908_C56475"  value="" placeholder="OBSERVACIONES" required></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="pasoId" id="pasoId" value="<?php if (isset($_GET["paso"])) {
                                echo $_GET["paso"];
                            }else{ echo "0"; } ?>">
                            <input type="hidden" name="id" id="hidId" value='0'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="ORIGEN_DY_WF" id="ORIGEN_DY_WF" value='<?php if(isset($_GET['origen'])){ echo $_GET['origen']; }else{ echo "0"; }?>' >
                            
                            <?php if (isset($_GET['v'])){ ?>
                                    
                            <?php }else{ ?>
                                    <input type= "hidden" name="OPTIN_DY_WF" id="OPTIN_DY_WF" value="SIMPLE">
                            <?php }?>

                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                    <?php 
                        }else{ ?>
                            <?php if(isset($_GET['v'])){ 
                                
                            }else { ?>
                            <div class="login-box-body">
                                <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                            <?php } ?>
                    <?php 
                            if($_GET['aceptaTerminos'] == 'acepto'){
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);

                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2908 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){


                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2908 WHERE G2908_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                        $Lsql = "UPDATE ".$BaseDatos.".G2908 SET G2908_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'CONFIRMADO' WHERE G2908_ConsInte__b =".$ultimoResgistroInsertado;
                                        if ($mysqli->query($Lsql) === TRUE) {
                                            /* Acepto toca meterlo en la muestra  G626_M285*/

                                            

                                            echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";

                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";   
                                    }


                                }

                            }else{
                                /* NO CONFIRMAN */
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);
                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2908 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){
                                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2908 WHERE G2908_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $Lsql = "UPDATE ".$BaseDatos.".G2908 SET G2908_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'NO CONFIRMADO' WHERE G2908_ConsInte__b =".$ultimoResgistroInsertado;

                                        if ($mysqli->query($Lsql) === TRUE) {
                                            echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";
                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";            
                                    }

                                }
                            }

                                
                        } ?>


                </div><!-- /.login-box -->

            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2908/G2908_eventos.js"></script>
        <script type="text/javascript">

            $("#formLogin").submit(function(event) {
                
            });
            $(function(){

                $("#G2908_C56474").change(function(){

                    if($(this).val() == "40130" || $(this).val() == "40127"){
                        $("#G2908_C62209").val("EIBER SANABRIA");
                    }else{
                        $("#G2908_C62209").val("OTRO");
                    }   

                });

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2908_C56460").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


                //Validaciones numeros Enteros
                

            $("#G2908_C56472").numeric();
            

                //Validaciones numeros Decimales
               


               /* Si tiene dependiendias */
               


    //function para TIPO DOCUMENTO 

    $("#G2908_C56458").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SUFRE ALGUNA  ENFERMEDAD COMO 

    $("#G2908_C56471").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para EPS 

    $("#G2908_C56473").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para CITA QUE REQUIERE 

    $("#G2908_C56474").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });


               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){
                            <?php 
                            if(isset($_GET['v'])) {
                                
                            } 
                            ?>
                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>

        <?php 
        if(isset($_GET['v'])) {
            
        } 
        ?>
        
