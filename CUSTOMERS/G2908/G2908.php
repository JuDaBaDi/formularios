
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2908/G2908_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2908_ConsInte__b as id, G2908_C56459 as camp1 , G2908_C56462 as camp2 FROM ".$BaseDatos.".G2908  WHERE G2908_Usuario = ".$idUsuario." ORDER BY G2908_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2908_ConsInte__b as id, G2908_C56459 as camp1 , G2908_C56462 as camp2 FROM ".$BaseDatos.".G2908  ORDER BY G2908_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2908_ConsInte__b as id, G2908_C56459 as camp1 , G2908_C56462 as camp2 FROM ".$BaseDatos.".G2908 JOIN ".$BaseDatos.".G2908_M".$resultEstpas->muestr." ON G2908_ConsInte__b = G2908_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2908_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2908_ConsInte__b as id, G2908_C56459 as camp1 , G2908_C56462 as camp2 FROM ".$BaseDatos.".G2908 JOIN ".$BaseDatos.".G2908_M".$resultEstpas->muestr." ON G2908_ConsInte__b = G2908_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2908_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2908_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2908_ConsInte__b as id, G2908_C56459 as camp1 , G2908_C56462 as camp2 FROM ".$BaseDatos.".G2908  ORDER BY G2908_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="8807" >
    <h3 class="box box-title">CONVERSACION</h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Solicitud de Citas Usuarios EPS</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Con este formulario se realiza la solicitud de asignación de cita, posteriormente será contactado por la clinica para la respectiva asignación</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8801" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56458" id="LblG2908_C56458">TIPO DOCUMENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56458" id="G2908_C56458">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3422 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56459" id="LblG2908_C56459">DOCUMENTO IDENTIDAD</label><input type="text" class="form-control input-sm" id="G2908_C56459" value="<?php if (isset($_GET['G2908_C56459'])) {
                            echo $_GET['G2908_C56459'];
                        } ?>"  name="G2908_C56459"  placeholder="DOCUMENTO IDENTIDAD"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2908_C56460" id="LblG2908_C56460">FECHA NACIMIENTO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2908_C56460'])) {
                            echo $_GET['G2908_C56460'];
                        } ?>"  name="G2908_C56460" id="G2908_C56460" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56461" id="LblG2908_C56461">EDAD</label><input type="text" class="form-control input-sm" id="G2908_C56461" value="<?php if (isset($_GET['G2908_C56461'])) {
                            echo $_GET['G2908_C56461'];
                        } ?>"  name="G2908_C56461"  placeholder="EDAD"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56462" id="LblG2908_C56462">PRIMER NOMBRE</label><input type="text" class="form-control input-sm" id="G2908_C56462" value="<?php if (isset($_GET['G2908_C56462'])) {
                            echo $_GET['G2908_C56462'];
                        } ?>"  name="G2908_C56462"  placeholder="PRIMER NOMBRE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56463" id="LblG2908_C56463">SEGUNDO NOMBRE</label><input type="text" class="form-control input-sm" id="G2908_C56463" value="<?php if (isset($_GET['G2908_C56463'])) {
                            echo $_GET['G2908_C56463'];
                        } ?>"  name="G2908_C56463"  placeholder="SEGUNDO NOMBRE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56464" id="LblG2908_C56464">PRIMER APELLIDO</label><input type="text" class="form-control input-sm" id="G2908_C56464" value="<?php if (isset($_GET['G2908_C56464'])) {
                            echo $_GET['G2908_C56464'];
                        } ?>"  name="G2908_C56464"  placeholder="PRIMER APELLIDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56465" id="LblG2908_C56465">SEGUNDO APELLIDO</label><input type="text" class="form-control input-sm" id="G2908_C56465" value="<?php if (isset($_GET['G2908_C56465'])) {
                            echo $_GET['G2908_C56465'];
                        } ?>"  name="G2908_C56465"  placeholder="SEGUNDO APELLIDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56466" id="LblG2908_C56466">DIRECCION</label><input type="text" class="form-control input-sm" id="G2908_C56466" value="<?php if (isset($_GET['G2908_C56466'])) {
                            echo $_GET['G2908_C56466'];
                        } ?>"  name="G2908_C56466"  placeholder="DIRECCION"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56467" id="LblG2908_C56467">TELEFONO DE CONTACTO</label><input type="text" class="form-control input-sm" id="G2908_C56467" value="<?php if (isset($_GET['G2908_C56467'])) {
                            echo $_GET['G2908_C56467'];
                        } ?>"  name="G2908_C56467"  placeholder="TELEFONO DE CONTACTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56468" id="LblG2908_C56468">OTRO TELEFONO DE CONTACTO</label><input type="text" class="form-control input-sm" id="G2908_C56468" value="<?php if (isset($_GET['G2908_C56468'])) {
                            echo $_GET['G2908_C56468'];
                        } ?>"  name="G2908_C56468"  placeholder="OTRO TELEFONO DE CONTACTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56469" id="LblG2908_C56469">CORREO ELECTRONICO</label>
                        <input type="email" class="form-control input-sm" id="G2908_C56469" value="<?php if (isset($_GET['G2908_C56469'])) {
                            echo $_GET['G2908_C56469'];
                        } ?>"  name="G2908_C56469"  placeholder="CORREO ELECTRONICO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56470" id="LblG2908_C56470">DIAGNOSTICO</label><input type="text" class="form-control input-sm" id="G2908_C56470" value="<?php if (isset($_GET['G2908_C56470'])) {
                            echo $_GET['G2908_C56470'];
                        } ?>"  name="G2908_C56470"  placeholder="DIAGNOSTICO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56471" id="LblG2908_C56471">SUFRE ALGUNA  ENFERMEDAD COMO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56471" id="G2908_C56471">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3423 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2908_C56472" id="LblG2908_C56472">NUMERO DE AUTORIZACION - En caso de no contar con numero de autorizacion, favor tramitarla con la EPS o dirijase al primero piso de la clinica. Muchas Gracias</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2908_C56472'])) {
                            echo $_GET['G2908_C56472'];
                        } ?>"  name="G2908_C56472" id="G2908_C56472" placeholder="NUMERO DE AUTORIZACION - En caso de no contar con numero de autorizacion, favor tramitarla con la EPS o dirijase al primero piso de la clinica. Muchas Gracias">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56473" id="LblG2908_C56473">EPS</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56473" id="G2908_C56473">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3424 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56474" id="LblG2908_C56474">CITA QUE REQUIERE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56474" id="G2908_C56474">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3425 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2908_C56475" id="LblG2908_C56475">OBSERVACIONES</label>
                        <textarea class="form-control input-sm" name="G2908_C56475" id="G2908_C56475"  value="<?php if (isset($_GET['G2908_C56475'])) {
                            echo $_GET['G2908_C56475'];
                        } ?>" placeholder="OBSERVACIONES"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8802" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56455" id="LblG2908_C56455">ORIGEN_DY_WF</label><input type="text" class="form-control input-sm" id="G2908_C56455" value="<?php if (isset($_GET['G2908_C56455'])) {
                            echo $_GET['G2908_C56455'];
                        } ?>" readonly name="G2908_C56455"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2908_C56456" id="LblG2908_C56456">OPTIN_DY_WF</label><input type="text" class="form-control input-sm" id="G2908_C56456" value="<?php if (isset($_GET['G2908_C56456'])) {
                            echo $_GET['G2908_C56456'];
                        } ?>" readonly name="G2908_C56456"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2908_C56457" id="LblG2908_C56457">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2908_C56457" id="G2908_C56457">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3421 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2908/G2908_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2908_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2908_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2908_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2908_C56458").val(item.G2908_C56458).trigger("change");  
                $("#G2908_C56459").val(item.G2908_C56459); 
                $("#G2908_C56460").val(item.G2908_C56460); 
                $("#G2908_C56461").val(item.G2908_C56461); 
                $("#G2908_C56462").val(item.G2908_C56462); 
                $("#G2908_C56463").val(item.G2908_C56463); 
                $("#G2908_C56464").val(item.G2908_C56464); 
                $("#G2908_C56465").val(item.G2908_C56465); 
                $("#G2908_C56466").val(item.G2908_C56466); 
                $("#G2908_C56467").val(item.G2908_C56467); 
                $("#G2908_C56468").val(item.G2908_C56468); 
                $("#G2908_C56469").val(item.G2908_C56469); 
                $("#G2908_C56470").val(item.G2908_C56470); 
                $("#G2908_C56471").val(item.G2908_C56471).trigger("change");  
                $("#G2908_C56472").val(item.G2908_C56472); 
                $("#G2908_C56473").val(item.G2908_C56473).trigger("change");  
                $("#G2908_C56474").val(item.G2908_C56474).trigger("change");  
                $("#G2908_C56475").val(item.G2908_C56475); 
                $("#G2908_C56455").val(item.G2908_C56455); 
                $("#G2908_C56456").val(item.G2908_C56456); 
                $("#G2908_C56457").val(item.G2908_C56457).trigger("change");    
                if(item.G2908_C56505 == 1){
                    $("#G2908_C56505").attr('checked', true);
                }    
                if(item.G2908_C56506 == 1){
                    $("#G2908_C56506").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2908_C56458").select2();

    $("#G2908_C56471").select2();

    $("#G2908_C56473").select2();

    $("#G2908_C56474").select2();

    $("#G2908_C56457").select2();
        //datepickers
        

        $("#G2908_C56460").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G2908_C56472").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para TIPO DOCUMENTO 

    $("#G2908_C56458").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SUFRE ALGUNA  ENFERMEDAD COMO 

    $("#G2908_C56471").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para EPS 

    $("#G2908_C56473").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para CITA QUE REQUIERE 

    $("#G2908_C56474").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G2908_C56457").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G2908_C56458").val()==0 || $("#G2908_C56458").val() == null || $("#G2908_C56458").val() == -1) && $("#G2908_C56458").prop("disabled") == false){
                alertify.error('TIPO DOCUMENTO debe ser diligenciado');
                $("#G2908_C56458").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56459").val() == "") && $("#G2908_C56459").prop("disabled") == false){
                alertify.error('DOCUMENTO IDENTIDAD debe ser diligenciado');
                $("#G2908_C56459").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56460").val() == "") && $("#G2908_C56460").prop("disabled") == false){
                alertify.error('FECHA NACIMIENTO debe ser diligenciado');
                $("#G2908_C56460").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56461").val() == "") && $("#G2908_C56461").prop("disabled") == false){
                alertify.error('EDAD debe ser diligenciado');
                $("#G2908_C56461").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56462").val() == "") && $("#G2908_C56462").prop("disabled") == false){
                alertify.error('PRIMER NOMBRE debe ser diligenciado');
                $("#G2908_C56462").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56463").val() == "") && $("#G2908_C56463").prop("disabled") == false){
                alertify.error('SEGUNDO NOMBRE debe ser diligenciado');
                $("#G2908_C56463").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56464").val() == "") && $("#G2908_C56464").prop("disabled") == false){
                alertify.error('PRIMER APELLIDO debe ser diligenciado');
                $("#G2908_C56464").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56465").val() == "") && $("#G2908_C56465").prop("disabled") == false){
                alertify.error('SEGUNDO APELLIDO debe ser diligenciado');
                $("#G2908_C56465").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56466").val() == "") && $("#G2908_C56466").prop("disabled") == false){
                alertify.error('DIRECCION debe ser diligenciado');
                $("#G2908_C56466").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56467").val() == "") && $("#G2908_C56467").prop("disabled") == false){
                alertify.error('TELEFONO DE CONTACTO debe ser diligenciado');
                $("#G2908_C56467").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56468").val() == "") && $("#G2908_C56468").prop("disabled") == false){
                alertify.error('OTRO TELEFONO DE CONTACTO debe ser diligenciado');
                $("#G2908_C56468").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56470").val() == "") && $("#G2908_C56470").prop("disabled") == false){
                alertify.error('DIAGNOSTICO debe ser diligenciado');
                $("#G2908_C56470").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56472").val() == "") && $("#G2908_C56472").prop("disabled") == false){
                alertify.error('NUMERO DE AUTORIZACION - En caso de no contar con numero de autorizacion, favor tramitarla con la EPS o dirijase al primero piso de la clinica. Muchas Gracias debe ser diligenciado');
                $("#G2908_C56472").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56473").val()==0 || $("#G2908_C56473").val() == null || $("#G2908_C56473").val() == -1) && $("#G2908_C56473").prop("disabled") == false){
                alertify.error('EPS debe ser diligenciado');
                $("#G2908_C56473").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56474").val()==0 || $("#G2908_C56474").val() == null || $("#G2908_C56474").val() == -1) && $("#G2908_C56474").prop("disabled") == false){
                alertify.error('CITA QUE REQUIERE debe ser diligenciado');
                $("#G2908_C56474").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2908_C56475").val() == "") && $("#G2908_C56475").prop("disabled") == false){
                alertify.error('OBSERVACIONES debe ser diligenciado');
                $("#G2908_C56475").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                    $("#G2908_C56458").val(item.G2908_C56458).trigger("change"); 
 
                                                $("#G2908_C56459").val(item.G2908_C56459);
 
                                                $("#G2908_C56460").val(item.G2908_C56460);
 
                                                $("#G2908_C56461").val(item.G2908_C56461);
 
                                                $("#G2908_C56462").val(item.G2908_C56462);
 
                                                $("#G2908_C56463").val(item.G2908_C56463);
 
                                                $("#G2908_C56464").val(item.G2908_C56464);
 
                                                $("#G2908_C56465").val(item.G2908_C56465);
 
                                                $("#G2908_C56466").val(item.G2908_C56466);
 
                                                $("#G2908_C56467").val(item.G2908_C56467);
 
                                                $("#G2908_C56468").val(item.G2908_C56468);
 
                                                $("#G2908_C56469").val(item.G2908_C56469);
 
                                                $("#G2908_C56470").val(item.G2908_C56470);
 
                    $("#G2908_C56471").val(item.G2908_C56471).trigger("change"); 
 
                                                $("#G2908_C56472").val(item.G2908_C56472);
 
                    $("#G2908_C56473").val(item.G2908_C56473).trigger("change"); 
 
                    $("#G2908_C56474").val(item.G2908_C56474).trigger("change"); 
 
                                                $("#G2908_C56475").val(item.G2908_C56475);
 
                                                $("#G2908_C56455").val(item.G2908_C56455);
 
                                                $("#G2908_C56456").val(item.G2908_C56456);
 
                    $("#G2908_C56457").val(item.G2908_C56457).trigger("change"); 
      
                                                if(item.G2908_C56505 == 1){
                                                   $("#G2908_C56505").attr('checked', true);
                                                } 
      
                                                if(item.G2908_C56506 == 1){
                                                   $("#G2908_C56506").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','TIPO DOCUMENTO','DOCUMENTO IDENTIDAD','FECHA NACIMIENTO','EDAD','PRIMER NOMBRE','SEGUNDO NOMBRE','PRIMER APELLIDO','SEGUNDO APELLIDO','DIRECCION','TELEFONO DE CONTACTO','OTRO TELEFONO DE CONTACTO','CORREO ELECTRONICO','DIAGNOSTICO','SUFRE ALGUNA  ENFERMEDAD COMO','NUMERO DE AUTORIZACION - En caso de no contar con numero de autorizacion, favor tramitarla con la EPS o dirijase al primero piso de la clinica. Muchas Gracias','EPS','CITA QUE REQUIERE','OBSERVACIONES','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2908_C56458', 
                        index:'G2908_C56458', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3422&campo=G2908_C56458'
                        }
                    }

                    ,
                    { 
                        name:'G2908_C56459', 
                        index: 'G2908_C56459', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2908_C56460', 
                        index:'G2908_C56460', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2908_C56461', 
                        index: 'G2908_C56461', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56462', 
                        index: 'G2908_C56462', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56463', 
                        index: 'G2908_C56463', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56464', 
                        index: 'G2908_C56464', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56465', 
                        index: 'G2908_C56465', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56466', 
                        index: 'G2908_C56466', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56467', 
                        index: 'G2908_C56467', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56468', 
                        index: 'G2908_C56468', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56470', 
                        index: 'G2908_C56470', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56471', 
                        index:'G2908_C56471', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3423&campo=G2908_C56471'
                        }
                    }
 
                    ,
                    {  
                        name:'G2908_C56472', 
                        index:'G2908_C56472', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2908_C56473', 
                        index:'G2908_C56473', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3424&campo=G2908_C56473'
                        }
                    }

                    ,
                    { 
                        name:'G2908_C56474', 
                        index:'G2908_C56474', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3425&campo=G2908_C56474'
                        }
                    }

                    ,
                    { 
                        name:'G2908_C56475', 
                        index:'G2908_C56475', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56455', 
                        index: 'G2908_C56455', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56456', 
                        index: 'G2908_C56456', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2908_C56457', 
                        index:'G2908_C56457', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3421&campo=G2908_C56457'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2908_C56459',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        
 
                    $("#G2908_C56458").val(item.G2908_C56458).trigger("change"); 

                        $("#G2908_C56459").val(item.G2908_C56459);

                        $("#G2908_C56460").val(item.G2908_C56460);

                        $("#G2908_C56461").val(item.G2908_C56461);

                        $("#G2908_C56462").val(item.G2908_C56462);

                        $("#G2908_C56463").val(item.G2908_C56463);

                        $("#G2908_C56464").val(item.G2908_C56464);

                        $("#G2908_C56465").val(item.G2908_C56465);

                        $("#G2908_C56466").val(item.G2908_C56466);

                        $("#G2908_C56467").val(item.G2908_C56467);

                        $("#G2908_C56468").val(item.G2908_C56468);

                        $("#G2908_C56469").val(item.G2908_C56469);

                        $("#G2908_C56470").val(item.G2908_C56470);
 
                    $("#G2908_C56471").val(item.G2908_C56471).trigger("change"); 

                        $("#G2908_C56472").val(item.G2908_C56472);
 
                    $("#G2908_C56473").val(item.G2908_C56473).trigger("change"); 
 
                    $("#G2908_C56474").val(item.G2908_C56474).trigger("change"); 

                        $("#G2908_C56475").val(item.G2908_C56475);

                        $("#G2908_C56455").val(item.G2908_C56455);

                        $("#G2908_C56456").val(item.G2908_C56456);
 
                    $("#G2908_C56457").val(item.G2908_C56457).trigger("change"); 
    
                        if(item.G2908_C56505 == 1){
                           $("#G2908_C56505").attr('checked', true);
                        } 
    
                        if(item.G2908_C56506 == 1){
                           $("#G2908_C56506").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
