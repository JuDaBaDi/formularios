<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2908_ConsInte__b, G2908_FechaInsercion , G2908_Usuario ,  G2908_CodigoMiembro  , G2908_PoblacionOrigen , G2908_EstadoDiligenciamiento ,  G2908_IdLlamada , G2908_C56459 as principal ,G2908_C56458,G2908_C56459,G2908_C56460,G2908_C56461,G2908_C56462,G2908_C56463,G2908_C56464,G2908_C56465,G2908_C56466,G2908_C56467,G2908_C56468,G2908_C56469,G2908_C56470,G2908_C56471,G2908_C56472,G2908_C56473,G2908_C56474,G2908_C56475,G2908_C56455,G2908_C56456,G2908_C56457 FROM '.$BaseDatos.'.G2908 WHERE G2908_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2908_C56458'] = $key->G2908_C56458;

                $datos[$i]['G2908_C56459'] = $key->G2908_C56459;

                $datos[$i]['G2908_C56460'] = explode(' ', $key->G2908_C56460)[0];

                $datos[$i]['G2908_C56461'] = $key->G2908_C56461;

                $datos[$i]['G2908_C56462'] = $key->G2908_C56462;

                $datos[$i]['G2908_C56463'] = $key->G2908_C56463;

                $datos[$i]['G2908_C56464'] = $key->G2908_C56464;

                $datos[$i]['G2908_C56465'] = $key->G2908_C56465;

                $datos[$i]['G2908_C56466'] = $key->G2908_C56466;

                $datos[$i]['G2908_C56467'] = $key->G2908_C56467;

                $datos[$i]['G2908_C56468'] = $key->G2908_C56468;

                $datos[$i]['G2908_C56469'] = $key->G2908_C56469;

                $datos[$i]['G2908_C56470'] = $key->G2908_C56470;

                $datos[$i]['G2908_C56471'] = $key->G2908_C56471;

                $datos[$i]['G2908_C56472'] = $key->G2908_C56472;

                $datos[$i]['G2908_C56473'] = $key->G2908_C56473;

                $datos[$i]['G2908_C56474'] = $key->G2908_C56474;

                $datos[$i]['G2908_C56475'] = $key->G2908_C56475;

                $datos[$i]['G2908_C56455'] = $key->G2908_C56455;

                $datos[$i]['G2908_C56456'] = $key->G2908_C56456;

                $datos[$i]['G2908_C56457'] = $key->G2908_C56457;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2908";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2908_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2908_ConsInte__b as id,  G2908_C56459 as camp1 , G2908_C56462 as camp2 
                     FROM ".$BaseDatos.".G2908  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2908_ConsInte__b as id,  G2908_C56459 as camp1 , G2908_C56462 as camp2  
                    FROM ".$BaseDatos.".G2908  JOIN ".$BaseDatos.".G2908_M".$_POST['muestra']." ON G2908_ConsInte__b = G2908_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2908_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2908_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2908_C56459 LIKE '%".$B."%' OR G2908_C56462 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2908_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2908");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2908_ConsInte__b, G2908_FechaInsercion , G2908_Usuario ,  G2908_CodigoMiembro  , G2908_PoblacionOrigen , G2908_EstadoDiligenciamiento ,  G2908_IdLlamada , G2908_C56459 as principal , a.LISOPC_Nombre____b as G2908_C56458,G2908_C56459,G2908_C56460,G2908_C56461,G2908_C56462,G2908_C56463,G2908_C56464,G2908_C56465,G2908_C56466,G2908_C56467,G2908_C56468,G2908_C56469,G2908_C56470, b.LISOPC_Nombre____b as G2908_C56471,G2908_C56472, c.LISOPC_Nombre____b as G2908_C56473, d.LISOPC_Nombre____b as G2908_C56474,G2908_C56475,G2908_C56455,G2908_C56456, e.LISOPC_Nombre____b as G2908_C56457 FROM '.$BaseDatos.'.G2908 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2908_C56458 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2908_C56471 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2908_C56473 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2908_C56474 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2908_C56457';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2908_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2908_ConsInte__b , ($fila->G2908_C56458) , ($fila->G2908_C56459) , explode(' ', $fila->G2908_C56460)[0] , ($fila->G2908_C56461) , ($fila->G2908_C56462) , ($fila->G2908_C56463) , ($fila->G2908_C56464) , ($fila->G2908_C56465) , ($fila->G2908_C56466) , ($fila->G2908_C56467) , ($fila->G2908_C56468) , ($fila->G2908_C56469) , ($fila->G2908_C56470) , ($fila->G2908_C56471) , ($fila->G2908_C56472) , ($fila->G2908_C56473) , ($fila->G2908_C56474) , ($fila->G2908_C56475) , ($fila->G2908_C56455) , ($fila->G2908_C56456) , ($fila->G2908_C56457) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2908 WHERE G2908_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2908";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2908_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2908_ConsInte__b as id,  G2908_C56459 as camp1 , G2908_C56462 as camp2  FROM '.$BaseDatos.'.G2908 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2908_ConsInte__b as id,  G2908_C56459 as camp1 , G2908_C56462 as camp2  
                    FROM ".$BaseDatos.".G2908  JOIN ".$BaseDatos.".G2908_M".$_POST['muestra']." ON G2908_ConsInte__b = G2908_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2908_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2908_C56459 LIKE "%'.$B.'%" OR G2908_C56462 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2908_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2908 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2908(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2908_C56458"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56458 = '".$_POST["G2908_C56458"]."'";
                $LsqlI .= $separador."G2908_C56458";
                $LsqlV .= $separador."'".$_POST["G2908_C56458"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56459"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56459 = '".$_POST["G2908_C56459"]."'";
                $LsqlI .= $separador."G2908_C56459";
                $LsqlV .= $separador."'".$_POST["G2908_C56459"]."'";
                $validar = 1;
            }
             
 
            $G2908_C56460 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2908_C56460"])){    
                if($_POST["G2908_C56460"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2908_C56460"]);
                    if(count($tieneHora) > 1){
                        $G2908_C56460 = "'".$_POST["G2908_C56460"]."'";
                    }else{
                        $G2908_C56460 = "'".str_replace(' ', '',$_POST["G2908_C56460"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2908_C56460 = ".$G2908_C56460;
                    $LsqlI .= $separador." G2908_C56460";
                    $LsqlV .= $separador.$G2908_C56460;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2908_C56461"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56461 = '".$_POST["G2908_C56461"]."'";
                $LsqlI .= $separador."G2908_C56461";
                $LsqlV .= $separador."'".$_POST["G2908_C56461"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56462"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56462 = '".$_POST["G2908_C56462"]."'";
                $LsqlI .= $separador."G2908_C56462";
                $LsqlV .= $separador."'".$_POST["G2908_C56462"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56463"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56463 = '".$_POST["G2908_C56463"]."'";
                $LsqlI .= $separador."G2908_C56463";
                $LsqlV .= $separador."'".$_POST["G2908_C56463"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56464"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56464 = '".$_POST["G2908_C56464"]."'";
                $LsqlI .= $separador."G2908_C56464";
                $LsqlV .= $separador."'".$_POST["G2908_C56464"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56465"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56465 = '".$_POST["G2908_C56465"]."'";
                $LsqlI .= $separador."G2908_C56465";
                $LsqlV .= $separador."'".$_POST["G2908_C56465"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56466"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56466 = '".$_POST["G2908_C56466"]."'";
                $LsqlI .= $separador."G2908_C56466";
                $LsqlV .= $separador."'".$_POST["G2908_C56466"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56467"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56467 = '".$_POST["G2908_C56467"]."'";
                $LsqlI .= $separador."G2908_C56467";
                $LsqlV .= $separador."'".$_POST["G2908_C56467"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56468"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56468 = '".$_POST["G2908_C56468"]."'";
                $LsqlI .= $separador."G2908_C56468";
                $LsqlV .= $separador."'".$_POST["G2908_C56468"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56469"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56469 = '".$_POST["G2908_C56469"]."'";
                $LsqlI .= $separador."G2908_C56469";
                $LsqlV .= $separador."'".$_POST["G2908_C56469"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56470"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56470 = '".$_POST["G2908_C56470"]."'";
                $LsqlI .= $separador."G2908_C56470";
                $LsqlV .= $separador."'".$_POST["G2908_C56470"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56471"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56471 = '".$_POST["G2908_C56471"]."'";
                $LsqlI .= $separador."G2908_C56471";
                $LsqlV .= $separador."'".$_POST["G2908_C56471"]."'";
                $validar = 1;
            }
             
  
            $G2908_C56472 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2908_C56472"])){
                if($_POST["G2908_C56472"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2908_C56472 = $_POST["G2908_C56472"];
                    $LsqlU .= $separador." G2908_C56472 = ".$G2908_C56472."";
                    $LsqlI .= $separador." G2908_C56472";
                    $LsqlV .= $separador.$G2908_C56472;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2908_C56473"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56473 = '".$_POST["G2908_C56473"]."'";
                $LsqlI .= $separador."G2908_C56473";
                $LsqlV .= $separador."'".$_POST["G2908_C56473"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56474"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56474 = '".$_POST["G2908_C56474"]."'";
                $LsqlI .= $separador."G2908_C56474";
                $LsqlV .= $separador."'".$_POST["G2908_C56474"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56475"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56475 = '".$_POST["G2908_C56475"]."'";
                $LsqlI .= $separador."G2908_C56475";
                $LsqlV .= $separador."'".$_POST["G2908_C56475"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56455"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56455 = '".$_POST["G2908_C56455"]."'";
                $LsqlI .= $separador."G2908_C56455";
                $LsqlV .= $separador."'".$_POST["G2908_C56455"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56456"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56456 = '".$_POST["G2908_C56456"]."'";
                $LsqlI .= $separador."G2908_C56456";
                $LsqlV .= $separador."'".$_POST["G2908_C56456"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56457"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56457 = '".$_POST["G2908_C56457"]."'";
                $LsqlI .= $separador."G2908_C56457";
                $LsqlV .= $separador."'".$_POST["G2908_C56457"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56505"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56505 = '".$_POST["G2908_C56505"]."'";
                $LsqlI .= $separador."G2908_C56505";
                $LsqlV .= $separador."'".$_POST["G2908_C56505"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2908_C56506"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_C56506 = '".$_POST["G2908_C56506"]."'";
                $LsqlI .= $separador."G2908_C56506";
                $LsqlV .= $separador."'".$_POST["G2908_C56506"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2908_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2908_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2908_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2908_Usuario , G2908_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2908_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2908 WHERE G2908_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2908 SET G2908_UltiGest__b =-14, G2908_GesMasImp_b =-14, G2908_TipoReintentoUG_b =0, G2908_TipoReintentoGMI_b =0, G2908_EstadoUG_b =-14, G2908_EstadoGMI_b =-14, G2908_CantidadIntentos =0, G2908_CantidadIntentosGMI_b =0 WHERE G2908_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
