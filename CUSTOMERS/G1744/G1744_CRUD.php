<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1744;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1744
                  SET G1744_C = -201
                  WHERE G1744_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1744 
                    WHERE G1744_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1744_C"]) || $gestion["G1744_C"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1744_C"];
        }

        if (is_null($gestion["G1744_C"]) || $gestion["G1744_C"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1744_C"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1744_FechaInsercion"]."',".$gestion["G1744_Usuario"].",'".$gestion["G1744_C".$P["P"]]."','".$gestion["G1744_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "customers.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1744 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1744_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1744_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1744_LinkContenido as url FROM ".$BaseDatos.".G1744 WHERE G1744_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1744_ConsInte__b, G1744_FechaInsercion , G1744_Usuario ,  G1744_CodigoMiembro  , G1744_PoblacionOrigen , G1744_EstadoDiligenciamiento ,  G1744_IdLlamada , G1744_C31375 as principal ,G1744_C31375,G1744_C31376,G1744_C31377,G1744_C31397,G1744_C31398,G1744_C31399,G1744_C31400,G1744_C50486,G1744_C50487,G1744_C50488,G1744_C50489,G1744_C31364,G1744_C31365,G1744_C31366,G1744_C31367,G1744_C31368,G1744_C31369,G1744_C31370,G1744_C31371,G1744_C31372,G1744_C31374 FROM '.$BaseDatos.'.G1744 WHERE G1744_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1744_C31375'] = $key->G1744_C31375;

                $datos[$i]['G1744_C31376'] = $key->G1744_C31376;

                $datos[$i]['G1744_C31377'] = $key->G1744_C31377;

                $datos[$i]['G1744_C31397'] = $key->G1744_C31397;

                $datos[$i]['G1744_C31398'] = $key->G1744_C31398;

                $datos[$i]['G1744_C31399'] = $key->G1744_C31399;

                $datos[$i]['G1744_C31400'] = $key->G1744_C31400;

                $datos[$i]['G1744_C50486'] = $key->G1744_C50486;

                $datos[$i]['G1744_C50487'] = $key->G1744_C50487;

                $datos[$i]['G1744_C50488'] = $key->G1744_C50488;

                $datos[$i]['G1744_C50489'] = $key->G1744_C50489;

                $datos[$i]['G1744_C31364'] = $key->G1744_C31364;

                $datos[$i]['G1744_C31365'] = $key->G1744_C31365;

                $datos[$i]['G1744_C31366'] = explode(' ', $key->G1744_C31366)[0];
  
                $hora = '';
                if(!is_null($key->G1744_C31367)){
                    $hora = explode(' ', $key->G1744_C31367)[1];
                }

                $datos[$i]['G1744_C31367'] = $hora;

                $datos[$i]['G1744_C31368'] = $key->G1744_C31368;

                $datos[$i]['G1744_C31369'] = $key->G1744_C31369;

                $datos[$i]['G1744_C31370'] = $key->G1744_C31370;

                $datos[$i]['G1744_C31371'] = $key->G1744_C31371;

                $datos[$i]['G1744_C31372'] = $key->G1744_C31372;

                $datos[$i]['G1744_C31374'] = $key->G1744_C31374;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1744";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1744_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1744_ConsInte__b as id,  G1744_C31375 as camp1 , G1744_C31376 as camp2 
                     FROM ".$BaseDatos.".G1744  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1744_ConsInte__b as id,  G1744_C31375 as camp1 , G1744_C31376 as camp2  
                    FROM ".$BaseDatos.".G1744  JOIN ".$BaseDatos.".G1744_M".$_POST['muestra']." ON G1744_ConsInte__b = G1744_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1744_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1744_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1744_C31375 LIKE '%".$B."%' OR G1744_C31376 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1744_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

            if(isset($_GET['MostrarCombo_Guion_1741_C31245'])){
                echo '<select class="form-control input-sm"  name="1741_C31245" id="1741_C31245">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_1741_C31245'])){
                $Ysql = "SELECT G1740_ConsInte__b as id,  G1740_C31227 as text FROM ".$BaseDatos.".G1740 WHERE G1740_C31227 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_1741_C31245'])){
                $Lsql = "SELECT  G1740_ConsInte__b as id , G1740_C31228 FROM ".$BaseDatos.".G1740 WHERE G1740_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1741_C31245'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G1741_C31239'] = $key->G1740_C31228;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1744");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1744_ConsInte__b, G1744_FechaInsercion , G1744_Usuario ,  G1744_CodigoMiembro  , G1744_PoblacionOrigen , G1744_EstadoDiligenciamiento ,  G1744_IdLlamada , G1744_C31375 as principal ,G1744_C31375,G1744_C31376,G1744_C31377,G1744_C31397,G1744_C31398,G1744_C31399,G1744_C31400, a.LISOPC_Nombre____b as G1744_C50486, b.LISOPC_Nombre____b as G1744_C50487, c.LISOPC_Nombre____b as G1744_C50488, d.LISOPC_Nombre____b as G1744_C50489, e.LISOPC_Nombre____b as G1744_C31364, f.LISOPC_Nombre____b as G1744_C31365,G1744_C31366,G1744_C31367,G1744_C31368,G1744_C31369,G1744_C31370,G1744_C31371,G1744_C31372, g.LISOPC_Nombre____b as G1744_C31374 FROM '.$BaseDatos.'.G1744 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1744_C50486 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1744_C50487 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1744_C50488 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1744_C50489 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1744_C31364 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1744_C31365 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1744_C31374';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1744_C31367)){
                    $hora_a = explode(' ', $fila->G1744_C31367)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1744_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1744_ConsInte__b , ($fila->G1744_C31375) , ($fila->G1744_C31376) , ($fila->G1744_C31377) , ($fila->G1744_C31397) , ($fila->G1744_C31398) , ($fila->G1744_C31399) , ($fila->G1744_C31400) , ($fila->G1744_C50486) , ($fila->G1744_C50487) , ($fila->G1744_C50488) , ($fila->G1744_C50489) , ($fila->G1744_C31364) , ($fila->G1744_C31365) , explode(' ', $fila->G1744_C31366)[0] , $hora_a , ($fila->G1744_C31368) , ($fila->G1744_C31369) , ($fila->G1744_C31370) , ($fila->G1744_C31371) , ($fila->G1744_C31372) , ($fila->G1744_C31374) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1744 WHERE G1744_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1744";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1744_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1744_ConsInte__b as id,  G1744_C31375 as camp1 , G1744_C31376 as camp2  FROM '.$BaseDatos.'.G1744 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1744_ConsInte__b as id,  G1744_C31375 as camp1 , G1744_C31376 as camp2  
                    FROM ".$BaseDatos.".G1744  JOIN ".$BaseDatos.".G1744_M".$_POST['muestra']." ON G1744_ConsInte__b = G1744_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1744_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1744_C31375 LIKE "%'.$B.'%" OR G1744_C31376 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1744_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1744 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1744(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1744_C31375"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31375 = '".$_POST["G1744_C31375"]."'";
                $LsqlI .= $separador."G1744_C31375";
                $LsqlV .= $separador."'".$_POST["G1744_C31375"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31376 = '".$_POST["G1744_C31376"]."'";
                $LsqlI .= $separador."G1744_C31376";
                $LsqlV .= $separador."'".$_POST["G1744_C31376"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31377 = '".$_POST["G1744_C31377"]."'";
                $LsqlI .= $separador."G1744_C31377";
                $LsqlV .= $separador."'".$_POST["G1744_C31377"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31397"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31397 = '".$_POST["G1744_C31397"]."'";
                $LsqlI .= $separador."G1744_C31397";
                $LsqlV .= $separador."'".$_POST["G1744_C31397"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31398"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31398 = '".$_POST["G1744_C31398"]."'";
                $LsqlI .= $separador."G1744_C31398";
                $LsqlV .= $separador."'".$_POST["G1744_C31398"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31399"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31399 = '".$_POST["G1744_C31399"]."'";
                $LsqlI .= $separador."G1744_C31399";
                $LsqlV .= $separador."'".$_POST["G1744_C31399"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31400"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31400 = '".$_POST["G1744_C31400"]."'";
                $LsqlI .= $separador."G1744_C31400";
                $LsqlV .= $separador."'".$_POST["G1744_C31400"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C50486"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C50486 = '".$_POST["G1744_C50486"]."'";
                $LsqlI .= $separador."G1744_C50486";
                $LsqlV .= $separador."'".$_POST["G1744_C50486"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C50487"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C50487 = '".$_POST["G1744_C50487"]."'";
                $LsqlI .= $separador."G1744_C50487";
                $LsqlV .= $separador."'".$_POST["G1744_C50487"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C50488"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C50488 = '".$_POST["G1744_C50488"]."'";
                $LsqlI .= $separador."G1744_C50488";
                $LsqlV .= $separador."'".$_POST["G1744_C50488"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C50489"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C50489 = '".$_POST["G1744_C50489"]."'";
                $LsqlI .= $separador."G1744_C50489";
                $LsqlV .= $separador."'".$_POST["G1744_C50489"]."'";
                $validar = 1;
            }
             
 
            $G1744_C31364 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1744_C31364 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1744_C31364 = ".$G1744_C31364;
                    $LsqlI .= $separador." G1744_C31364";
                    $LsqlV .= $separador.$G1744_C31364;
                    $validar = 1;

                    
                }
            }
 
            $G1744_C31365 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1744_C31365 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1744_C31365 = ".$G1744_C31365;
                    $LsqlI .= $separador." G1744_C31365";
                    $LsqlV .= $separador.$G1744_C31365;
                    $validar = 1;
                }
            }
 
            $G1744_C31366 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1744_C31366 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1744_C31366 = ".$G1744_C31366;
                    $LsqlI .= $separador." G1744_C31366";
                    $LsqlV .= $separador.$G1744_C31366;
                    $validar = 1;
                }
            }
 
            $G1744_C31367 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1744_C31367 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1744_C31367 = ".$G1744_C31367;
                    $LsqlI .= $separador." G1744_C31367";
                    $LsqlV .= $separador.$G1744_C31367;
                    $validar = 1;
                }
            }
 
            $G1744_C31368 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1744_C31368 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1744_C31368 = ".$G1744_C31368;
                    $LsqlI .= $separador." G1744_C31368";
                    $LsqlV .= $separador.$G1744_C31368;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1744_C31369"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31369 = '".$_POST["G1744_C31369"]."'";
                $LsqlI .= $separador."G1744_C31369";
                $LsqlV .= $separador."'".$_POST["G1744_C31369"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31370"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31370 = '".$_POST["G1744_C31370"]."'";
                $LsqlI .= $separador."G1744_C31370";
                $LsqlV .= $separador."'".$_POST["G1744_C31370"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31371"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31371 = '".$_POST["G1744_C31371"]."'";
                $LsqlI .= $separador."G1744_C31371";
                $LsqlV .= $separador."'".$_POST["G1744_C31371"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31372"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31372 = '".$_POST["G1744_C31372"]."'";
                $LsqlI .= $separador."G1744_C31372";
                $LsqlV .= $separador."'".$_POST["G1744_C31372"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31373"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31373 = '".$_POST["G1744_C31373"]."'";
                $LsqlI .= $separador."G1744_C31373";
                $LsqlV .= $separador."'".$_POST["G1744_C31373"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1744_C31374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_C31374 = '".$_POST["G1744_C31374"]."'";
                $LsqlI .= $separador."G1744_C31374";
                $LsqlV .= $separador."'".$_POST["G1744_C31374"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1744_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1744_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1744_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1744_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1744_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1744_Usuario , G1744_FechaInsercion, G1744_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1744_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1744 WHERE G1744_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  
    if(isset($_GET["cargarCasos"])){

        $intNumeroCaso_t = $_GET['intNumeroCaso_t']; 

        $SQL = "SELECT G1741_ConsInte__b, G1741_C31240, G1741_C31244, c.LISOPC_Nombre____b as  G1741_C31241, d.LISOPC_Nombre____b as  G1741_C31242, e.LISOPC_Nombre____b as  G1741_C31243, f.LISOPC_Nombre____b as  G1741_C31363, g.LISOPC_Nombre____b as  G1741_C31388, G1741_C31239, G1740_C31228 as G1741_C31245, j.LISOPC_Nombre____b as  G1741_C31391, G1741_C31389, G1741_C31390, G1741_C31958, G1741_C31959, G1741_C31960, G1741_C31961, G1741_C31962, G1741_C31963, G1741_C31964 FROM ".$BaseDatos.".G1741  LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G1741_C31241 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G1741_C31242 LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b =  G1741_C31243 LEFT JOIN ".$BaseDatos_systema.".LISOPC as f ON f.LISOPC_ConsInte__b =  G1741_C31363 LEFT JOIN ".$BaseDatos_systema.".LISOPC as g ON g.LISOPC_ConsInte__b =  G1741_C31388 LEFT JOIN ".$BaseDatos.".G1740 ON G1740_ConsInte__b  =  G1741_C31245 LEFT JOIN ".$BaseDatos_systema.".LISOPC as j ON j.LISOPC_ConsInte__b =  G1741_C31391 ";

        $SQL .= " WHERE G1741_C31240 = '".$intNumeroCaso_t."'"; 

        $SQL .= " ORDER BY G1741_C31240";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1741_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1741_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G1741_C31240."</cell>"; 

                echo "<cell><![CDATA[". ($fila->G1741_C31244)."]]></cell>";

                echo "<cell>". ($fila->G1741_C31241)."</cell>";

                echo "<cell>". ($fila->G1741_C31242)."</cell>";

                echo "<cell>". ($fila->G1741_C31243)."</cell>";

                echo "<cell>". ($fila->G1741_C31363)."</cell>";

                echo "<cell>". ($fila->G1741_C31388)."</cell>";

                echo "<cell>". ($fila->G1741_C31239)."</cell>";

                echo "<cell>". ($fila->G1741_C31245)."</cell>";

                echo "<cell>". ($fila->G1741_C31391)."</cell>";

                if($fila->G1741_C31389 != ''){
                    echo "<cell>". explode(' ', $fila->G1741_C31389)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G1741_C31390 != ''){
                    echo "<cell>". explode(' ', $fila->G1741_C31390)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G1741_C31958)."</cell>";

                echo "<cell>". ($fila->G1741_C31959)."</cell>";

                echo "<cell>". ($fila->G1741_C31960)."</cell>";

                echo "<cell>". ($fila->G1741_C31961)."</cell>";

                echo "<cell>". ($fila->G1741_C31962)."</cell>";

                echo "<cell>". ($fila->G1741_C31963)."</cell>";

                echo "<cell>". ($fila->G1741_C31964)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1741_ConsInte__b, G1741_C31240, G1741_C31244, c.LISOPC_Nombre____b as  G1741_C31241, d.LISOPC_Nombre____b as  G1741_C31242, e.LISOPC_Nombre____b as  G1741_C31243, f.LISOPC_Nombre____b as  G1741_C31363, g.LISOPC_Nombre____b as  G1741_C31388, G1741_C31239, G1740_C31228 as G1741_C31245, j.LISOPC_Nombre____b as  G1741_C31391, G1741_C31389, G1741_C31390, G1741_C31958, G1741_C31959, G1741_C31960, G1741_C31961, G1741_C31962, G1741_C31963, G1741_C31964 FROM ".$BaseDatos.".G1741  LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G1741_C31241 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G1741_C31242 LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b =  G1741_C31243 LEFT JOIN ".$BaseDatos_systema.".LISOPC as f ON f.LISOPC_ConsInte__b =  G1741_C31363 LEFT JOIN ".$BaseDatos_systema.".LISOPC as g ON g.LISOPC_ConsInte__b =  G1741_C31388 LEFT JOIN ".$BaseDatos.".G1740 ON G1740_ConsInte__b  =  G1741_C31245 LEFT JOIN ".$BaseDatos_systema.".LISOPC as j ON j.LISOPC_ConsInte__b =  G1741_C31391 ";

        $SQL .= " WHERE G1741_C31238 = '".$numero."'"; 

        $SQL .= " ORDER BY G1741_C31240";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1741_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1741_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G1741_C31240."</cell>"; 

                echo "<cell><![CDATA[". ($fila->G1741_C31244)."]]></cell>";

                echo "<cell>". ($fila->G1741_C31241)."</cell>";

                echo "<cell>". ($fila->G1741_C31242)."</cell>";

                echo "<cell>". ($fila->G1741_C31243)."</cell>";

                echo "<cell>". ($fila->G1741_C31363)."</cell>";

                echo "<cell>". ($fila->G1741_C31388)."</cell>";

                echo "<cell>". ($fila->G1741_C31239)."</cell>";

                echo "<cell>". ($fila->G1741_C31245)."</cell>";

                echo "<cell>". ($fila->G1741_C31391)."</cell>";

                if($fila->G1741_C31389 != ''){
                    echo "<cell>". explode(' ', $fila->G1741_C31389)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G1741_C31390 != ''){
                    echo "<cell>". explode(' ', $fila->G1741_C31390)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G1741_C31958)."</cell>";

                echo "<cell>". ($fila->G1741_C31959)."</cell>";

                echo "<cell>". ($fila->G1741_C31960)."</cell>";

                echo "<cell>". ($fila->G1741_C31961)."</cell>";

                echo "<cell>". ($fila->G1741_C31962)."</cell>";

                echo "<cell>". ($fila->G1741_C31963)."</cell>";

                echo "<cell>". ($fila->G1741_C31964)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1741 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1741(";
            $LsqlV = " VALUES ("; 
 
                $G1741_C31240= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G1741_C31240"])){    
                    if($_POST["G1741_C31240"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1741_C31240 = $_POST["G1741_C31240"];
                        $LsqlU .= $separador." G1741_C31240 = '".$G1741_C31240."'";
                        $LsqlI .= $separador." G1741_C31240";
                        $LsqlV .= $separador."'".$G1741_C31240."'";
                        $validar = 1;
                    }
                }
  

                if(isset($_POST["G1741_C31244"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31244 = '".$_POST["G1741_C31244"]."'";
                    $LsqlI .= $separador."G1741_C31244";
                    $LsqlV .= $separador."'".$_POST["G1741_C31244"]."'";
                    $validar = 1;
                }
                                                                               
 
                if(isset($_POST["G1741_C31241"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31241 = '".$_POST["G1741_C31241"]."'";
                    $LsqlI .= $separador."G1741_C31241";
                    $LsqlV .= $separador."'".$_POST["G1741_C31241"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G1741_C31242"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31242 = '".$_POST["G1741_C31242"]."'";
                    $LsqlI .= $separador."G1741_C31242";
                    $LsqlV .= $separador."'".$_POST["G1741_C31242"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G1741_C31243"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31243 = '".$_POST["G1741_C31243"]."'";
                    $LsqlI .= $separador."G1741_C31243";
                    $LsqlV .= $separador."'".$_POST["G1741_C31243"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G1741_C31363"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31363 = '".$_POST["G1741_C31363"]."'";
                    $LsqlI .= $separador."G1741_C31363";
                    $LsqlV .= $separador."'".$_POST["G1741_C31363"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G1741_C31388"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31388 = '".$_POST["G1741_C31388"]."'";
                    $LsqlI .= $separador."G1741_C31388";
                    $LsqlV .= $separador."'".$_POST["G1741_C31388"]."'";
                    $validar = 1;
                }
 

                if(isset($_POST["G1741_C31239"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31239 = '".$_POST["G1741_C31239"]."'";
                    $LsqlI .= $separador."G1741_C31239";
                    $LsqlV .= $separador."'".$_POST["G1741_C31239"]."'";
                    $validar = 1;
                }

                                                                               
  
                if(isset($_POST["G1741_C31245"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31245 = '".$_POST["G1741_C31245"]."'";
                    $LsqlI .= $separador."G1741_C31245";
                    $LsqlV .= $separador."'".$_POST["G1741_C31245"]."'";
                    $validar = 1;
                }
                
 
                if(isset($_POST["G1741_C31391"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31391 = '".$_POST["G1741_C31391"]."'";
                    $LsqlI .= $separador."G1741_C31391";
                    $LsqlV .= $separador."'".$_POST["G1741_C31391"]."'";
                    $validar = 1;
                }

                $G1741_C31389 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G1741_C31389"])){    
                    if($_POST["G1741_C31389"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1741_C31389 = "'".str_replace(' ', '',$_POST["G1741_C31389"])." 00:00:00'";
                        $LsqlU .= $separador." G1741_C31389 = ".$G1741_C31389;
                        $LsqlI .= $separador." G1741_C31389";
                        $LsqlV .= $separador.$G1741_C31389;
                        $validar = 1;
                    }
                }

                $G1741_C31390 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G1741_C31390"])){    
                    if($_POST["G1741_C31390"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1741_C31390 = "'".str_replace(' ', '',$_POST["G1741_C31390"])." 00:00:00'";
                        $LsqlU .= $separador." G1741_C31390 = ".$G1741_C31390;
                        $LsqlI .= $separador." G1741_C31390";
                        $LsqlV .= $separador.$G1741_C31390;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G1741_C31958"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31958 = '".$_POST["G1741_C31958"]."'";
                    $LsqlI .= $separador."G1741_C31958";
                    $LsqlV .= $separador."'".$_POST["G1741_C31958"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1741_C31959"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31959 = '".$_POST["G1741_C31959"]."'";
                    $LsqlI .= $separador."G1741_C31959";
                    $LsqlV .= $separador."'".$_POST["G1741_C31959"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1741_C31960"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31960 = '".$_POST["G1741_C31960"]."'";
                    $LsqlI .= $separador."G1741_C31960";
                    $LsqlV .= $separador."'".$_POST["G1741_C31960"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1741_C31961"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31961 = '".$_POST["G1741_C31961"]."'";
                    $LsqlI .= $separador."G1741_C31961";
                    $LsqlV .= $separador."'".$_POST["G1741_C31961"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1741_C31962"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31962 = '".$_POST["G1741_C31962"]."'";
                    $LsqlI .= $separador."G1741_C31962";
                    $LsqlV .= $separador."'".$_POST["G1741_C31962"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1741_C31963"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31963 = '".$_POST["G1741_C31963"]."'";
                    $LsqlI .= $separador."G1741_C31963";
                    $LsqlV .= $separador."'".$_POST["G1741_C31963"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1741_C31964"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1741_C31964 = '".$_POST["G1741_C31964"]."'";
                    $LsqlI .= $separador."G1741_C31964";
                    $LsqlV .= $separador."'".$_POST["G1741_C31964"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1741_C31238 = $numero;
                    $LsqlU .= ", G1741_C31238 = ".$G1741_C31238."";
                    $LsqlI .= ", G1741_C31238";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1741_Usuario ,  G1741_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1741_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1741 WHERE  G1741_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
