<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2929_ConsInte__b, G2929_FechaInsercion , G2929_Usuario ,  G2929_CodigoMiembro  , G2929_PoblacionOrigen , G2929_EstadoDiligenciamiento ,  G2929_IdLlamada , G2929_C56851 as principal ,G2929_C56850,G2929_C56851,G2929_C56852,G2929_C56853,G2929_C56854,G2929_C56855,G2929_C56856,G2929_C56857,G2929_C56858,G2929_C56859,G2929_C56860,G2929_C56872,G2929_C56861,G2929_C56862,G2929_C56863,G2929_C56864,G2929_C56865,G2929_C56866,G2929_C56867,G2929_C56868,G2929_C56869 FROM '.$BaseDatos.'.G2929 WHERE G2929_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2929_C56850'] = $key->G2929_C56850;

                $datos[$i]['G2929_C56851'] = $key->G2929_C56851;

                $datos[$i]['G2929_C56852'] = $key->G2929_C56852;

                $datos[$i]['G2929_C56853'] = $key->G2929_C56853;

                $datos[$i]['G2929_C56854'] = $key->G2929_C56854;

                $datos[$i]['G2929_C56855'] = $key->G2929_C56855;

                $datos[$i]['G2929_C56856'] = $key->G2929_C56856;

                $datos[$i]['G2929_C56857'] = $key->G2929_C56857;

                $datos[$i]['G2929_C56858'] = $key->G2929_C56858;

                $datos[$i]['G2929_C56859'] = $key->G2929_C56859;

                $datos[$i]['G2929_C56860'] = $key->G2929_C56860;

                $datos[$i]['G2929_C56872'] = $key->G2929_C56872;

                $datos[$i]['G2929_C56861'] = $key->G2929_C56861;

                $datos[$i]['G2929_C56862'] = $key->G2929_C56862;

                $datos[$i]['G2929_C56863'] = explode(' ', $key->G2929_C56863)[0];
  
                $hora = '';
                if(!is_null($key->G2929_C56864)){
                    $hora = explode(' ', $key->G2929_C56864)[1];
                }

                $datos[$i]['G2929_C56864'] = $hora;

                $datos[$i]['G2929_C56865'] = $key->G2929_C56865;

                $datos[$i]['G2929_C56866'] = $key->G2929_C56866;

                $datos[$i]['G2929_C56867'] = $key->G2929_C56867;

                $datos[$i]['G2929_C56868'] = $key->G2929_C56868;

                $datos[$i]['G2929_C56869'] = $key->G2929_C56869;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2929";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2929_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2929_ConsInte__b as id,  G2929_C56851 as camp1 , G2929_C56854 as camp2 
                     FROM ".$BaseDatos.".G2929  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2929_ConsInte__b as id,  G2929_C56851 as camp1 , G2929_C56854 as camp2  
                    FROM ".$BaseDatos.".G2929  JOIN ".$BaseDatos.".G2929_M".$_POST['muestra']." ON G2929_ConsInte__b = G2929_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2929_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2929_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2929_C56851 LIKE '%".$B."%' OR G2929_C56854 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2929_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2929");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2929_ConsInte__b, G2929_FechaInsercion , G2929_Usuario ,  G2929_CodigoMiembro  , G2929_PoblacionOrigen , G2929_EstadoDiligenciamiento ,  G2929_IdLlamada , G2929_C56851 as principal , a.LISOPC_Nombre____b as G2929_C56850,G2929_C56851,G2929_C56852,G2929_C56853,G2929_C56854,G2929_C56855,G2929_C56856,G2929_C56857,G2929_C56858,G2929_C56859,G2929_C56860,G2929_C56872, b.LISOPC_Nombre____b as G2929_C56861, c.LISOPC_Nombre____b as G2929_C56862,G2929_C56863,G2929_C56864,G2929_C56865,G2929_C56866,G2929_C56867,G2929_C56868,G2929_C56869 FROM '.$BaseDatos.'.G2929 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2929_C56850 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2929_C56861 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2929_C56862';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2929_C56864)){
                    $hora_a = explode(' ', $fila->G2929_C56864)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2929_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2929_ConsInte__b , ($fila->G2929_C56850) , ($fila->G2929_C56851) , ($fila->G2929_C56852) , ($fila->G2929_C56853) , ($fila->G2929_C56854) , ($fila->G2929_C56855) , ($fila->G2929_C56856) , ($fila->G2929_C56857) , ($fila->G2929_C56858) , ($fila->G2929_C56859) , ($fila->G2929_C56860) , ($fila->G2929_C56872) , ($fila->G2929_C56861) , ($fila->G2929_C56862) , explode(' ', $fila->G2929_C56863)[0] , $hora_a , ($fila->G2929_C56865) , ($fila->G2929_C56866) , ($fila->G2929_C56867) , ($fila->G2929_C56868) , ($fila->G2929_C56869) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2929 WHERE G2929_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2929";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2929_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2929_ConsInte__b as id,  G2929_C56851 as camp1 , G2929_C56854 as camp2  FROM '.$BaseDatos.'.G2929 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2929_ConsInte__b as id,  G2929_C56851 as camp1 , G2929_C56854 as camp2  
                    FROM ".$BaseDatos.".G2929  JOIN ".$BaseDatos.".G2929_M".$_POST['muestra']." ON G2929_ConsInte__b = G2929_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2929_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2929_C56851 LIKE "%'.$B.'%" OR G2929_C56854 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2929_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2929 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2929(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2929_C56850"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56850 = '".$_POST["G2929_C56850"]."'";
                $LsqlI .= $separador."G2929_C56850";
                $LsqlV .= $separador."'".$_POST["G2929_C56850"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56851"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56851 = '".$_POST["G2929_C56851"]."'";
                $LsqlI .= $separador."G2929_C56851";
                $LsqlV .= $separador."'".$_POST["G2929_C56851"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56852"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56852 = '".$_POST["G2929_C56852"]."'";
                $LsqlI .= $separador."G2929_C56852";
                $LsqlV .= $separador."'".$_POST["G2929_C56852"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56853"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56853 = '".$_POST["G2929_C56853"]."'";
                $LsqlI .= $separador."G2929_C56853";
                $LsqlV .= $separador."'".$_POST["G2929_C56853"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56854"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56854 = '".$_POST["G2929_C56854"]."'";
                $LsqlI .= $separador."G2929_C56854";
                $LsqlV .= $separador."'".$_POST["G2929_C56854"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56855"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56855 = '".$_POST["G2929_C56855"]."'";
                $LsqlI .= $separador."G2929_C56855";
                $LsqlV .= $separador."'".$_POST["G2929_C56855"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56856"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56856 = '".$_POST["G2929_C56856"]."'";
                $LsqlI .= $separador."G2929_C56856";
                $LsqlV .= $separador."'".$_POST["G2929_C56856"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56857"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56857 = '".$_POST["G2929_C56857"]."'";
                $LsqlI .= $separador."G2929_C56857";
                $LsqlV .= $separador."'".$_POST["G2929_C56857"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56858"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56858 = '".$_POST["G2929_C56858"]."'";
                $LsqlI .= $separador."G2929_C56858";
                $LsqlV .= $separador."'".$_POST["G2929_C56858"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56859"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56859 = '".$_POST["G2929_C56859"]."'";
                $LsqlI .= $separador."G2929_C56859";
                $LsqlV .= $separador."'".$_POST["G2929_C56859"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56860"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56860 = '".$_POST["G2929_C56860"]."'";
                $LsqlI .= $separador."G2929_C56860";
                $LsqlV .= $separador."'".$_POST["G2929_C56860"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56872"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56872 = '".$_POST["G2929_C56872"]."'";
                $LsqlI .= $separador."G2929_C56872";
                $LsqlV .= $separador."'".$_POST["G2929_C56872"]."'";
                $validar = 1;
            }
             
 
            $G2929_C56861 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2929_C56861 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2929_C56861 = ".$G2929_C56861;
                    $LsqlI .= $separador." G2929_C56861";
                    $LsqlV .= $separador.$G2929_C56861;
                    $validar = 1;

                    
                }
            }
 
            $G2929_C56862 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2929_C56862 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2929_C56862 = ".$G2929_C56862;
                    $LsqlI .= $separador." G2929_C56862";
                    $LsqlV .= $separador.$G2929_C56862;
                    $validar = 1;
                }
            }
 
            $G2929_C56863 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2929_C56863 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2929_C56863 = ".$G2929_C56863;
                    $LsqlI .= $separador." G2929_C56863";
                    $LsqlV .= $separador.$G2929_C56863;
                    $validar = 1;
                }
            }
 
            $G2929_C56864 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2929_C56864 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2929_C56864 = ".$G2929_C56864;
                    $LsqlI .= $separador." G2929_C56864";
                    $LsqlV .= $separador.$G2929_C56864;
                    $validar = 1;
                }
            }
 
            $G2929_C56865 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2929_C56865 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2929_C56865 = ".$G2929_C56865;
                    $LsqlI .= $separador." G2929_C56865";
                    $LsqlV .= $separador.$G2929_C56865;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2929_C56866"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56866 = '".$_POST["G2929_C56866"]."'";
                $LsqlI .= $separador."G2929_C56866";
                $LsqlV .= $separador."'".$_POST["G2929_C56866"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56867"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56867 = '".$_POST["G2929_C56867"]."'";
                $LsqlI .= $separador."G2929_C56867";
                $LsqlV .= $separador."'".$_POST["G2929_C56867"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56868"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56868 = '".$_POST["G2929_C56868"]."'";
                $LsqlI .= $separador."G2929_C56868";
                $LsqlV .= $separador."'".$_POST["G2929_C56868"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2929_C56869"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_C56869 = '".$_POST["G2929_C56869"]."'";
                $LsqlI .= $separador."G2929_C56869";
                $LsqlV .= $separador."'".$_POST["G2929_C56869"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2929_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2929_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2929_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2929_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2929_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2929_Usuario , G2929_FechaInsercion, G2929_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2929_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2929 WHERE G2929_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                            $UltimoID = $_POST["id"];
                            echo $UltimoID;
                        }
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
