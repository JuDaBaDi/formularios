
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1446/G1446_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1446_ConsInte__b as id, a.LISOPC_Nombre____b as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1446  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1446_C25385 LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1446_C25386 WHERE G1446_Usuario = ".$idUsuario." ORDER BY G1446_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1446_ConsInte__b as id, a.LISOPC_Nombre____b as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1446  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1446_C25385 LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1446_C25386 ORDER BY G1446_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1446_ConsInte__b as id, a.LISOPC_Nombre____b as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1446  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1446_C25385 LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1446_C25386 ORDER BY G1446_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div id="3328" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1446_C25931" id="LblG1446_C25931">Numero Radicado</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"]) && isset($_GET["view"])){  if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 25931")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 25931");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }
                             }?>" readonly name="G1446_C25931" id="G1446_C25931" placeholder="Numero Radicado">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1446_C25389" id="LblG1446_C25389">Nit del Cliente</label>
                        <input type="text" class="form-control input-sm" id="G1446_C25389" value="<?php if (isset($_GET['G1446_C25389'])) {
                            echo $_GET['G1446_C25389'];
                        } ?>" readonly name="G1446_C25389"  placeholder="Nit del Cliente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1446_C25390" id="LblG1446_C25390">Razon social</label>
                        <input readonly type="text" class="form-control input-sm" id="G1446_C25390" value="<?php if (isset($_GET['RS'])) {
                            echo $_GET['RS'];
                        } ?>"  name="G1446_C25390"  placeholder="Razon social">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1446_C25982" id="LblG1446_C25982">Fecha de Apertura</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G1446_C25982" id="G1446_C25982" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1446_C25383" id="LblG1446_C25383">Fecha del correo</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1446_C25383'])) {
                            echo $_GET['G1446_C25383'];
                        } ?>"  name="G1446_C25383" id="G1446_C25383" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1446_C25934" id="LblG1446_C25934">Email para notificar</label>
                        <input type="text" class="form-control input-sm" id="G1446_C25934" value="<?php if (isset($_GET['G1446_C25934'])) {
                            echo $_GET['G1446_C25934'];
                        } ?>"  name="G1446_C25934"  placeholder="Email para notificar">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->
            <div class="col-md-6 col-xs-6" hidden>


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1446_C25384" id="LblG1446_C25384">Requerimiento</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1446_C25384" id="G1446_C25384">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1235 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1446_C25932" id="LblG1446_C25932">Agente</label>
                        <input type="text" class="form-control input-sm" id="G1446_C25932" value="<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>" readonly name="G1446_C25932"  placeholder="Agente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1446_C25385" id="LblG1446_C25385">Area de Escalamiento</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1446_C25385" id="G1446_C25385">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1236 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1446_C25386" id="LblG1446_C25386">Clasificación del Escalamiento</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1446_C25386" id="G1446_C25386">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1237 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA 
                    <div class="form-group">
                        <label for="G1446_C25387" id="LblG1446_C25387">ANS (tiempo de respuesta)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1446_C25387" id="G1446_C25387">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1238 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            */?>
                        </select>
                    </div>  -->
                    <div class="form-group">
                        <label for="G1446_C25387" id="LblG1446_C25387">ANS (tiempo de respuesta)</label>
                        <input type="text" class="form-control input-sm Numerico" value="0" readonly name="G1446_C25387" id="G1446_C25387" placeholder="ANS (tiempo de respuesta)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1446_C25388" id="LblG1446_C25388">DETALLE DE LA SOLICITUD</label>
                        <textarea class="form-control input-sm" name="G1446_C25388" id="G1446_C25388"  value="<?php if (isset($_GET['G1446_C25388'])) {
                            echo $_GET['G1446_C25388'];
                        } ?>" placeholder="DETALLE DE LA SOLICITUD"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1446_C25933" id="LblG1446_C25933">Estado</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1446_C25933" id="G1446_C25933">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1322 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

</div>

<div id="3329" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1446_C25380" id="LblG1446_C25380">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1446_C25380" value="<?php if (isset($_GET['G1446_C25380'])) {
                            echo $_GET['G1446_C25380'];
                        } ?>" readonly name="G1446_C25380"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1446_C25381" id="LblG1446_C25381">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1446_C25381" value="<?php if (isset($_GET['G1446_C25381'])) {
                            echo $_GET['G1446_C25381'];
                        } ?>" readonly name="G1446_C25381"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1446_C25382" id="LblG1446_C25382">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1446_C25382" id="G1446_C25382">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1287 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1446/G1446_eventos.js"></script> 
<script type="text/javascript">
function bindEvent(element, eventName, eventHandler) {
    if (element.addEventListener) {
        element.addEventListener(eventName, eventHandler, false);
    } else if (element.attachEvent) {
        element.attachEvent('on' + eventName, eventHandler);
    }
}
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1446_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1446_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1446_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            //JDBD - Creamos un nuevo id incrementable y lo asignamos a este campo
            $.ajax({
                url:'<?=$url_crud;?>',
                type:'POST',
                data:{INCTB:"si"},
                success:function(data){
                    $("#G1446_C25931").val(data);
                }
            });
            //JDBD - Damos el valor fecha actual.
            $("#G1446_C25982").val("<?=date("Y-m-d");?>");
            //JDBD - Damos el valor nombre de usuario.
            $("#G1446_C25932").val("<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>");             
        });
    <?php } ?>; 
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G1446_C25931").val(item.G1446_C25931); 
                $("#G1446_C25389").val(item.G1446_C25389); 
                $("#G1446_C25390").val(item.G1446_C25390); 
                $("#G1446_C25982").val(item.G1446_C25982); 
                $("#G1446_C25383").val(item.G1446_C25383); 
                $("#G1446_C25384").val(item.G1446_C25384).trigger("change");  
                $("#G1446_C25932").val(item.G1446_C25932); 
                $("#G1446_C25385").val(item.G1446_C25385).trigger("change");  
                $("#G1446_C25386").attr("opt",item.G1446_C25386);  
                // $("#G1446_C25387").attr("opt",item.G1446_C25387);  
                $("#G1446_C25387").val(item.G1446_C25387);  
                $("#G1446_C25388").val(item.G1446_C25388); 
                $("#G1446_C25933").val(item.G1446_C25933).trigger("change");  
                $("#G1446_C25934").val(item.G1446_C25934); 
                $("#G1446_C25380").val(item.G1446_C25380); 
                $("#G1446_C25381").val(item.G1446_C25381); 
                $("#G1446_C25382").val(item.G1446_C25382).trigger("change"); 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1446_C25384").select2();

    $("#G1446_C25385").select2();

    $("#G1446_C25386").select2();

    // $("#G1446_C25387").select2();

    $("#G1446_C25933").select2();

    $("#G1446_C25382").select2();
        //datepickers
        

        $("#G1446_C25383").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G1446_C25931").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Requerimiento 

    $("#G1446_C25384").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Area de Escalamiento 

    $("#G1446_C25385").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $("#G1446_C25387").val(null);
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '1237' , idPadre : $(this).val() },
            success : function(data){
                var optG1446_C25386 = $("#G1446_C25386").attr("opt");
                $("#G1446_C25386").html(data);
                if (optG1446_C25386 != null) {
                    $("#G1446_C25386").val(optG1446_C25386).trigger("change");
                }
            }
        });
        
    });

    //function para Clasificación del Escalamiento 

    $("#G1446_C25386").change(function(){  
        //Esto es la parte de las listas dependientes
        switch ($(this).val()) {
          case '17110':
            $("#G1446_C25387").val(2);
            break;
          case '17111':
            $("#G1446_C25387").val(2);
            break;
          case '17112':
            $("#G1446_C25387").val(3);
            break;
          case '17113':
            $("#G1446_C25387").val(2);
            break;
          case '17114':
            $("#G1446_C25387").val(4);
            break;
          case '17115':
            $("#G1446_C25387").val(2);
            break;
          case '17116':
            $("#G1446_C25387").val(2);
            break;
          case '17117':
            $("#G1446_C25387").val(2);
            break;
          case '17118':
            $("#G1446_C25387").val(2);
            break;
          case '17119':
            $("#G1446_C25387").val(10);
            break;
          case '17120':
            $("#G1446_C25387").val(1);
            break;
          case '17121':
            $("#G1446_C25387").val(1);
            break;
          case '17122':
            $("#G1446_C25387").val(1);
            break;
          case '17123':
            $("#G1446_C25387").val(1);
            break;
          case '17124':
            $("#G1446_C25387").val(1);
            break;
          case '17125':
            $("#G1446_C25387").val(1);
            break;
          case '17126':
            $("#G1446_C25387").val(1);
            break;
          case '17127':
            $("#G1446_C25387").val(1);
            break;
          case '17128':
            $("#G1446_C25387").val(1);
            break;
          case '17129':
            $("#G1446_C25387").val(1);
            break;
          case '17130':
            $("#G1446_C25387").val(1);
            break;
          case '17131':
            $("#G1446_C25387").val(1);
            break;
          case '17128':
            $("#G1446_C25387").val(1);
            break;
          case '17133':
            $("#G1446_C25387").val(1);
            break;
          case '17134':
            $("#G1446_C25387").val(2);
            break;
          case '17135':
            $("#G1446_C25387").val(1);
            break;
          case '17136':
            $("#G1446_C25387").val(1);
            break;
          case '17138':
            $("#G1446_C25387").val(3);
            break;
          case '17139':
            $("#G1446_C25387").val(2);
            break;
          case '17140':
            $("#G1446_C25387").val(2);
            break;
          case '17142':
            $("#G1446_C25387").val(1);
            break;
          case '17143':
            $("#G1446_C25387").val(1);
            break;
          case '17144':
            $("#G1446_C25387").val(1);
            break;
          case '17145':
            $("#G1446_C25387").val(1);
            break;
          case '17148':
            $("#G1446_C25387").val(1);
            break;
          case '17149':
            $("#G1446_C25387").val(1);
            break;
          case '17150':
            $("#G1446_C25387").val(1);
            break;
          case '17151':
            $("#G1446_C25387").val(1);
            break;
          case '17152':
            $("#G1446_C25387").val(1);
            break;
          case '17153':
            $("#G1446_C25387").val(1);
            break;
          case '17154':
            $("#G1446_C25387").val(1);
            break;
          case '17155':
            $("#G1446_C25387").val(1);
            break;
          case '17157':
            $("#G1446_C25387").val(2);
            break;
          default:
            $("#G1446_C25387").val(0);
        }
        
    });

    //function para ANS (tiempo de respuesta) 

    $("#G1446_C25387").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Estado 

    $("#G1446_C25933").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G1446_C25382").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar

        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){

                // bindEvent(window, 'message', function (e) {
                //     results.innerHTML = e.data;
                // });
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                var sendMessage = function (msg) {
                                    // Make sure you are sending a string, and to stringify JSON
                                    window.parent.postMessage(msg, '*');
                                };
                                sendMessage('Cierrame');
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1446_C25931").val(item.G1446_C25931);
 
                                                $("#G1446_C25389").val(item.G1446_C25389);
 
                                                $("#G1446_C25390").val(item.G1446_C25390);
 
                                                $("#G1446_C25982").val(item.G1446_C25982);
 
                                                $("#G1446_C25383").val(item.G1446_C25383);
 
                    $("#G1446_C25384").val(item.G1446_C25384).trigger("change"); 
 
                                                $("#G1446_C25932").val(item.G1446_C25932);
 
                    $("#G1446_C25385").val(item.G1446_C25385).trigger("change"); 
 
                    $("#G1446_C25386").attr("opt",item.G1446_C25386); 
 
                    // $("#G1446_C25387").attr("opt",item.G1446_C25387); 
                    $("#G1446_C25387").val(item.G1446_C25387); 
 
                                                $("#G1446_C25388").val(item.G1446_C25388);
 
                    $("#G1446_C25933").val(item.G1446_C25933).trigger("change"); 
 
                                                $("#G1446_C25934").val(item.G1446_C25934);
 
                                                $("#G1446_C25380").val(item.G1446_C25380);
 
                                                $("#G1446_C25381").val(item.G1446_C25381);
 
                    $("#G1446_C25382").val(item.G1446_C25382).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Numero Radicado','Nit del Cliente','Razon social','Fecha de Apertura','Fecha del correo','Requerimiento','Agente','Area de Escalamiento','Clasificación del Escalamiento','ANS (tiempo de respuesta)','DETALLE DE LA SOLICITUD','Estado','Email para notificar','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G1446_C25931', 
                        index:'G1446_C25931', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G1446_C25389', 
                        index: 'G1446_C25389', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1446_C25390', 
                        index: 'G1446_C25390', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G1446_C25982', 
                        index:'G1446_C25982', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G1446_C25383', 
                        index:'G1446_C25383', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1446_C25384', 
                        index:'G1446_C25384', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1235&campo=G1446_C25384'
                        }
                    }

                    ,
                    { 
                        name:'G1446_C25932', 
                        index: 'G1446_C25932', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1446_C25385', 
                        index:'G1446_C25385', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1236&campo=G1446_C25385'
                        }
                    }

                    ,
                    { 
                        name:'G1446_C25386', 
                        index:'G1446_C25386', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1237&campo=G1446_C25386'
                        }
                    }

                    ,
                    { 
                        name:'G1446_C25387', 
                        index:'G1446_C25387', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1238&campo=G1446_C25387'
                        }
                    }

                    ,
                    { 
                        name:'G1446_C25388', 
                        index:'G1446_C25388', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1446_C25933', 
                        index:'G1446_C25933', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1322&campo=G1446_C25933'
                        }
                    }

                    ,
                    { 
                        name:'G1446_C25934', 
                        index: 'G1446_C25934', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1446_C25380', 
                        index: 'G1446_C25380', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1446_C25381', 
                        index: 'G1446_C25381', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1446_C25382', 
                        index:'G1446_C25382', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1287&campo=G1446_C25382'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1446_C25385',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1446_C25931").val(item.G1446_C25931);

                        $("#G1446_C25389").val(item.G1446_C25389);

                        $("#G1446_C25390").val(item.G1446_C25390);

                        $("#G1446_C25982").val(item.G1446_C25982);

                        $("#G1446_C25383").val(item.G1446_C25383);
 
                    $("#G1446_C25384").val(item.G1446_C25384).trigger("change"); 

                        $("#G1446_C25932").val(item.G1446_C25932);
 
                    $("#G1446_C25385").val(item.G1446_C25385).trigger("change"); 
 
                    $("#G1446_C25386").attr("opt",item.G1446_C25386); 
 
                    $("#G1446_C25387").val(item.G1446_C25387); 
                    // $("#G1446_C25387").attr("opt",item.G1446_C25387); 

                        $("#G1446_C25388").val(item.G1446_C25388);
 
                    $("#G1446_C25933").val(item.G1446_C25933).trigger("change"); 

                        $("#G1446_C25934").val(item.G1446_C25934);

                        $("#G1446_C25380").val(item.G1446_C25380);

                        $("#G1446_C25381").val(item.G1446_C25381);
 
                    $("#G1446_C25382").val(item.G1446_C25382).trigger("change"); 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
