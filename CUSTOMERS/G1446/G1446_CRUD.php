<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 25931")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 25931");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1446_ConsInte__b, G1446_FechaInsercion , G1446_Usuario ,  G1446_CodigoMiembro  , G1446_PoblacionOrigen , G1446_EstadoDiligenciamiento ,  G1446_IdLlamada , G1446_C25385 as principal ,G1446_C25931,G1446_C25389,G1446_C25390,G1446_C25982,G1446_C25383,G1446_C25384,G1446_C25932,G1446_C25385,G1446_C25386,G1446_C25387,G1446_C25388,G1446_C25933,G1446_C25934,G1446_C25380,G1446_C25381,G1446_C25382 FROM '.$BaseDatos.'.G1446 WHERE G1446_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1446_C25931'] = $key->G1446_C25931;

                $datos[$i]['G1446_C25389'] = $key->G1446_C25389;

                $datos[$i]['G1446_C25390'] = $key->G1446_C25390;

                $datos[$i]['G1446_C25982'] = explode(' ', $key->G1446_C25982)[0];

                $datos[$i]['G1446_C25383'] = explode(' ', $key->G1446_C25383)[0];

                $datos[$i]['G1446_C25384'] = $key->G1446_C25384;

                $datos[$i]['G1446_C25932'] = $key->G1446_C25932;

                $datos[$i]['G1446_C25385'] = $key->G1446_C25385;

                $datos[$i]['G1446_C25386'] = $key->G1446_C25386;

                $datos[$i]['G1446_C25387'] = $key->G1446_C25387;

                $datos[$i]['G1446_C25388'] = $key->G1446_C25388;

                $datos[$i]['G1446_C25933'] = $key->G1446_C25933;

                $datos[$i]['G1446_C25934'] = $key->G1446_C25934;

                $datos[$i]['G1446_C25380'] = $key->G1446_C25380;

                $datos[$i]['G1446_C25381'] = $key->G1446_C25381;

                $datos[$i]['G1446_C25382'] = $key->G1446_C25382;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1446";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = " AND G1446_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $regProp = "";
            }

            //JDBD valores filtros.
            $B = $_POST["B"];
            $A = $_POST["A"];
            $T = $_POST["T"];
            $F = $_POST["F"];
            $E = $_POST["E"];


            $Lsql = "SELECT G1446_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , b.LISOPC_Nombre____b as camp2 
                     FROM ".$BaseDatos.".G1446  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1446_C25385 LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1446_C25386 WHERE TRUE ".$regProp;

            if ($B != "" && $B != NULL) {
                $Lsql .= " AND (G1446_C25385 LIKE '%".$B."%' OR G1446_C25386 LIKE '%".$B."%') ";
            }
            if ($A != 0 && $A != -1 && $A != NULL) {
                $Lsql .= " AND G1446_Usuario = ".$A." ";
            }
            if ($T != 0 && $T != -1 && $T != NULL) {
                $Lsql .= " AND G1446_C = ".$T." ";
            }
            if ($F != "" && $F != NULL) {
                $Lsql .= " AND G1446_FechaInsercion = '".$F."' ";
            }
            if ($E != 0 && $E != -1 && $E != NULL) {
                if ($E == -203) {
                    $Lsql .= " AND G1446_C = -203 OR G1446_C = '' OR G1446_C IS NULL ";
                }else{
                    $Lsql .= " AND G1446_C = ".$E." "; 
                }
            }


            $Lsql .= " ORDER BY G1446_ConsInte__b DESC LIMIT 0, 50 "; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1446");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1446_ConsInte__b, G1446_FechaInsercion , G1446_Usuario ,  G1446_CodigoMiembro  , G1446_PoblacionOrigen , G1446_EstadoDiligenciamiento ,  G1446_IdLlamada , G1446_C25385 as principal ,G1446_C25931,G1446_C25389,G1446_C25390,G1446_C25982,G1446_C25383, a.LISOPC_Nombre____b as G1446_C25384,G1446_C25932, b.LISOPC_Nombre____b as G1446_C25385, c.LISOPC_Nombre____b as G1446_C25386, d.LISOPC_Nombre____b as G1446_C25387,G1446_C25388, e.LISOPC_Nombre____b as G1446_C25933,G1446_C25934,G1446_C25380,G1446_C25381, f.LISOPC_Nombre____b as G1446_C25382 FROM '.$BaseDatos.'.G1446 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1446_C25384 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1446_C25385 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1446_C25386 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1446_C25387 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1446_C25933 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1446_C25382';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G1446_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1446_ConsInte__b , ($fila->G1446_C25931) , ($fila->G1446_C25389) , ($fila->G1446_C25390) , explode(' ', $fila->G1446_C25982)[0] , explode(' ', $fila->G1446_C25383)[0] , ($fila->G1446_C25384) , ($fila->G1446_C25932) , ($fila->G1446_C25385) , ($fila->G1446_C25386) , ($fila->G1446_C25387) , ($fila->G1446_C25388) , ($fila->G1446_C25933) , ($fila->G1446_C25934) , ($fila->G1446_C25380) , ($fila->G1446_C25381) , ($fila->G1446_C25382) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1446 WHERE G1446_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1446";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = ' AND G1446_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $regProp = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $A = 0;
            $T = 0;
            $F = "";
            $B = "";
            $E = 0;

            if (isset($_POST["A"])) {
                $A = $_POST["A"];
            }
            if (isset($_POST["T"])) {
                $T = $_POST["T"];
            }
            if (isset($_POST["F"])) {
                $F = $_POST["F"];
            }
            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            if (isset($_POST["E"])) {
                $E = $_POST["E"];
            }

            $Zsql = 'SELECT  G1446_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , b.LISOPC_Nombre____b as camp2  FROM '.$BaseDatos.'.G1446 WHERE TRUE'.$regProp; 

            if ($A != 0) {
                $Zsql .= ' AND G1446_Usuario = '.$A.' ';
            }

            if ($T != 0) {
                $Zsql .= ' AND G1446_C = '.$T.' ';
            }

            if ($F != "") {
                $Zsql .= ' AND G1446_FechaInsercion = "'.$F.'" ';
            }

            if ($B != "") {
                $Zsql .= ' AND (G1446_C25385 LIKE "%'.$B.'%" OR G1446_C25386 LIKE "%'.$B.'%") ';
            }

            if ($E != 0) {
                if ($E == -203) {
                    $Zsql .= ' AND G1446_C = -203 OR G1446_C = "" OR G1446_C IS NULL ';
                }else{
                    $Zsql .= ' AND G1446_C = '.$E.'  ';
                }
            }

            $Zsql .= ' ORDER BY G1446_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1446 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1446(";
            $LsqlV = " VALUES ("; 
  
            $G1446_C25931 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1446_C25931"])){
                if($_POST["G1446_C25931"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1446_C25931 = $_POST["G1446_C25931"];
                    $LsqlU .= $separador." G1446_C25931 = ".$G1446_C25931."";
                    $LsqlI .= $separador." G1446_C25931";
                    $LsqlV .= $separador.$G1446_C25931;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1446_C25389"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25389 = '".$_POST["G1446_C25389"]."'";
                $LsqlI .= $separador."G1446_C25389";
                $LsqlV .= $separador."'".$_POST["G1446_C25389"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25390"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25390 = '".$_POST["G1446_C25390"]."'";
                $LsqlI .= $separador."G1446_C25390";
                $LsqlV .= $separador."'".$_POST["G1446_C25390"]."'";
                $validar = 1;
            }
             
 
            $G1446_C25982 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1446_C25982"])){    
                if($_POST["G1446_C25982"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1446_C25982"]);
                    if(count($tieneHora) > 1){
                        $G1446_C25982 = "'".$_POST["G1446_C25982"]."'";
                    }else{
                        $G1446_C25982 = "'".str_replace(' ', '',$_POST["G1446_C25982"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1446_C25982 = ".$G1446_C25982;
                    $LsqlI .= $separador." G1446_C25982";
                    $LsqlV .= $separador.$G1446_C25982;
                    $validar = 1;
                }
            }
 
            $G1446_C25383 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1446_C25383"])){    
                if($_POST["G1446_C25383"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1446_C25383"]);
                    if(count($tieneHora) > 1){
                        $G1446_C25383 = "'".$_POST["G1446_C25383"]."'";
                    }else{
                        $G1446_C25383 = "'".str_replace(' ', '',$_POST["G1446_C25383"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1446_C25383 = ".$G1446_C25383;
                    $LsqlI .= $separador." G1446_C25383";
                    $LsqlV .= $separador.$G1446_C25383;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1446_C25384"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25384 = '".$_POST["G1446_C25384"]."'";
                $LsqlI .= $separador."G1446_C25384";
                $LsqlV .= $separador."'".$_POST["G1446_C25384"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25932"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25932 = '".$_POST["G1446_C25932"]."'";
                $LsqlI .= $separador."G1446_C25932";
                $LsqlV .= $separador."'".$_POST["G1446_C25932"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25385"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25385 = '".$_POST["G1446_C25385"]."'";
                $LsqlI .= $separador."G1446_C25385";
                $LsqlV .= $separador."'".$_POST["G1446_C25385"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25386"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25386 = '".$_POST["G1446_C25386"]."'";
                $LsqlI .= $separador."G1446_C25386";
                $LsqlV .= $separador."'".$_POST["G1446_C25386"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25387"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25387 = ".$_POST["G1446_C25387"];
                $LsqlI .= $separador."G1446_C25387";
                $LsqlV .= $separador.$_POST["G1446_C25387"];
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25388"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25388 = '".$_POST["G1446_C25388"]."'";
                $LsqlI .= $separador."G1446_C25388";
                $LsqlV .= $separador."'".$_POST["G1446_C25388"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25933 = '".$_POST["G1446_C25933"]."'";
                $LsqlI .= $separador."G1446_C25933";
                $LsqlV .= $separador."'".$_POST["G1446_C25933"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25934 = '".$_POST["G1446_C25934"]."'";
                $LsqlI .= $separador."G1446_C25934";
                $LsqlV .= $separador."'".$_POST["G1446_C25934"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25380"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25380 = '".$_POST["G1446_C25380"]."'";
                $LsqlI .= $separador."G1446_C25380";
                $LsqlV .= $separador."'".$_POST["G1446_C25380"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25381"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25381 = '".$_POST["G1446_C25381"]."'";
                $LsqlI .= $separador."G1446_C25381";
                $LsqlV .= $separador."'".$_POST["G1446_C25381"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1446_C25382"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_C25382 = '".$_POST["G1446_C25382"]."'";
                $LsqlI .= $separador."G1446_C25382";
                $LsqlV .= $separador."'".$_POST["G1446_C25382"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1446_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1446_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1446_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1446_Usuario , G1446_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1446_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1446 WHERE G1446_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        echo $mysqli->insert_id;
                    }else{
                        
                        echo "1";           
                    }

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
