
<?php
	include(__DIR__."/../../conexion.php");
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button">Cancelar</button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i> Busqueda</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#btnBuscar").click(function(){
			var datos = $("#formId").serialize();
			$.ajax({
				url     	: 'formularios/G1741/G1741_Funciones_Busqueda_Manual.php?action=GET_DATOS',
				type		: 'post',
				dataType	: 'json',
				data		: datos,
				success 	: function(datosq){
					if(datosq[0].cantidad_registros > 1){
						var valores = null;
						var tabla_a_mostrar = '<div class="box box-default">'+
			            '<div class="box-header">'+
			                '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>'+
			            '</div>'+
			            '<div class="box-body">'+
			        		'<table class="table table-hover table-bordered" style="width:100%;">';
						tabla_a_mostrar += '<thead>';
						tabla_a_mostrar += '<tr>';
						tabla_a_mostrar += '  ';
						tabla_a_mostrar += '</tr>';
						tabla_a_mostrar += '</thead>';
						tabla_a_mostrar += '<tbody>';
						$.each(datosq[0].registros, function(i, item) {
							tabla_a_mostrar += '<tr ConsInte="'+ item.G1741_ConsInte__b +'" class="EditRegistro">';
							tabla_a_mostrar += '';
							tabla_a_mostrar += '</tr>';
						});
						tabla_a_mostrar += '</tbody>';
						tabla_a_mostrar += '</table></div></div>';
						
						$("#resultadosBusqueda").html(tabla_a_mostrar);
						
						$(".EditRegistro").dblclick(function(){
							var id = $(this).attr("ConsInte");
							swal({
	                            html : true,
	                            title: "Información - Dyalogo CRM",
	                            text: 'Esta seguro de editar este registro?',
	                            type: "warning",
	                            confirmButtonText: "Editar registro",
	                            cancelButtonText : "No Editar registro",
	                            showCancelButton : true,
	                            closeOnConfirm : true
	                        },
		                        function(isconfirm){
		                        	if(isconfirm){
		                        		$("#buscador").hide();
		                        		$("#botones").hide();
		                        		$("#resulados").hide();
		                        		<?php if(isset($_GET['token'])){ ?>
	                        			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?consinte='+id+'&campan=true&user='+ id +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
		                        		<?php } ?>
		                        	}else{
		                        		$("#buscador").show();
		                    			$("#botones").show();
		                    			$("#resulados").show();
		                        	}
		                        });
							});
					}else if(datosq[0].cantidad_registros == 1){
						$("#buscador").hide();
                		$("#botones").hide();
                		$("#resulados").hide();
						var id = datosq[0].registros[0].G1741_ConsInte__b;
						<?php if(isset($_GET['token'])){ ?>
            			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
	            		<?php } ?>
					}else{
					 	swal({
                            html : true,
                            title: "Información - Dyalogo CRM",
                            text: 'No se encontraron datos, desea adicionar un registro?',
                            type: "warning",
                            confirmButtonText: "Adicionar registro",
                            cancelButtonText : "No adicionar registro",
                            showCancelButton : true,
                            closeOnConfirm : true
                        },
                        function(isconfirm){
                        	$("#buscador").hide();
                    		$("#botones").hide();
                    		$("#resulados").hide();
                        	if(isconfirm){
								$.ajax({
								url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
								type		: 'post',
								dataType	: 'json',
								success 	: function(numeroIdnuevo){
									<?php if(isset($_GET['token'])){ ?>
			            			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&nuevoregistro=true');
				            		<?php } ?>
								}
							});
                        	}else{
                        		$("#buscador").show();
                        		$("#buscador :input").each(function(){
                        			$(this).val('');
                        		});
                    			$("#botones").show();
                    			$("#resulados").show();
                        	}
                        });
					}
				}
			});
		});

		$("#btnCancelar").click(function(){
			window.location = 'formularios/generados/PHP_Cerrar_Cancelar.php?token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>';
		});
	});
</script>
