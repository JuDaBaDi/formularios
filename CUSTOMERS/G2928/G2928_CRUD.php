<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2928_ConsInte__b, G2928_FechaInsercion , G2928_Usuario ,  G2928_CodigoMiembro  , G2928_PoblacionOrigen , G2928_EstadoDiligenciamiento ,  G2928_IdLlamada , G2928_C56827 as principal ,G2928_C56826,G2928_C56827,G2928_C56828,G2928_C56829,G2928_C56830,G2928_C56831,G2928_C56832,G2928_C56833,G2928_C56834,G2928_C56835,G2928_C56836,G2928_C56848,G2928_C56849,G2928_C56837,G2928_C56838,G2928_C56839,G2928_C56840,G2928_C56841,G2928_C56842,G2928_C56843,G2928_C56844,G2928_C56845 FROM '.$BaseDatos.'.G2928 WHERE G2928_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2928_C56826'] = $key->G2928_C56826;

                $datos[$i]['G2928_C56827'] = $key->G2928_C56827;

                $datos[$i]['G2928_C56828'] = $key->G2928_C56828;

                $datos[$i]['G2928_C56829'] = $key->G2928_C56829;

                $datos[$i]['G2928_C56830'] = $key->G2928_C56830;

                $datos[$i]['G2928_C56831'] = $key->G2928_C56831;

                $datos[$i]['G2928_C56832'] = $key->G2928_C56832;

                $datos[$i]['G2928_C56833'] = $key->G2928_C56833;

                $datos[$i]['G2928_C56834'] = $key->G2928_C56834;

                $datos[$i]['G2928_C56835'] = $key->G2928_C56835;

                $datos[$i]['G2928_C56836'] = $key->G2928_C56836;

                $datos[$i]['G2928_C56848'] = $key->G2928_C56848;

                $datos[$i]['G2928_C56849'] = $key->G2928_C56849;

                $datos[$i]['G2928_C56837'] = $key->G2928_C56837;

                $datos[$i]['G2928_C56838'] = $key->G2928_C56838;

                $datos[$i]['G2928_C56839'] = explode(' ', $key->G2928_C56839)[0];
  
                $hora = '';
                if(!is_null($key->G2928_C56840)){
                    $hora = explode(' ', $key->G2928_C56840)[1];
                }

                $datos[$i]['G2928_C56840'] = $hora;

                $datos[$i]['G2928_C56841'] = $key->G2928_C56841;

                $datos[$i]['G2928_C56842'] = $key->G2928_C56842;

                $datos[$i]['G2928_C56843'] = $key->G2928_C56843;

                $datos[$i]['G2928_C56844'] = $key->G2928_C56844;

                $datos[$i]['G2928_C56845'] = $key->G2928_C56845;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2928";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2928_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2928_ConsInte__b as id,  G2928_C56827 as camp1 , G2928_C56830 as camp2 
                     FROM ".$BaseDatos.".G2928  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2928_ConsInte__b as id,  G2928_C56827 as camp1 , G2928_C56830 as camp2  
                    FROM ".$BaseDatos.".G2928  JOIN ".$BaseDatos.".G2928_M".$_POST['muestra']." ON G2928_ConsInte__b = G2928_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2928_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2928_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2928_C56827 LIKE '%".$B."%' OR G2928_C56830 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2928_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2928");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2928_ConsInte__b, G2928_FechaInsercion , G2928_Usuario ,  G2928_CodigoMiembro  , G2928_PoblacionOrigen , G2928_EstadoDiligenciamiento ,  G2928_IdLlamada , G2928_C56827 as principal , a.LISOPC_Nombre____b as G2928_C56826,G2928_C56827,G2928_C56828,G2928_C56829,G2928_C56830,G2928_C56831,G2928_C56832,G2928_C56833,G2928_C56834,G2928_C56835,G2928_C56836,G2928_C56848,G2928_C56849, b.LISOPC_Nombre____b as G2928_C56837, c.LISOPC_Nombre____b as G2928_C56838,G2928_C56839,G2928_C56840,G2928_C56841,G2928_C56842,G2928_C56843,G2928_C56844,G2928_C56845 FROM '.$BaseDatos.'.G2928 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2928_C56826 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2928_C56837 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2928_C56838';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2928_C56840)){
                    $hora_a = explode(' ', $fila->G2928_C56840)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2928_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2928_ConsInte__b , ($fila->G2928_C56826) , ($fila->G2928_C56827) , ($fila->G2928_C56828) , ($fila->G2928_C56829) , ($fila->G2928_C56830) , ($fila->G2928_C56831) , ($fila->G2928_C56832) , ($fila->G2928_C56833) , ($fila->G2928_C56834) , ($fila->G2928_C56835) , ($fila->G2928_C56836) , ($fila->G2928_C56848) , ($fila->G2928_C56849) , ($fila->G2928_C56837) , ($fila->G2928_C56838) , explode(' ', $fila->G2928_C56839)[0] , $hora_a , ($fila->G2928_C56841) , ($fila->G2928_C56842) , ($fila->G2928_C56843) , ($fila->G2928_C56844) , ($fila->G2928_C56845) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2928 WHERE G2928_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2928";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2928_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2928_ConsInte__b as id,  G2928_C56827 as camp1 , G2928_C56830 as camp2  FROM '.$BaseDatos.'.G2928 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2928_ConsInte__b as id,  G2928_C56827 as camp1 , G2928_C56830 as camp2  
                    FROM ".$BaseDatos.".G2928  JOIN ".$BaseDatos.".G2928_M".$_POST['muestra']." ON G2928_ConsInte__b = G2928_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2928_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2928_C56827 LIKE "%'.$B.'%" OR G2928_C56830 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2928_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2928 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2928(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2928_C56826"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56826 = '".$_POST["G2928_C56826"]."'";
                $LsqlI .= $separador."G2928_C56826";
                $LsqlV .= $separador."'".$_POST["G2928_C56826"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56827"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56827 = '".$_POST["G2928_C56827"]."'";
                $LsqlI .= $separador."G2928_C56827";
                $LsqlV .= $separador."'".$_POST["G2928_C56827"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56828"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56828 = '".$_POST["G2928_C56828"]."'";
                $LsqlI .= $separador."G2928_C56828";
                $LsqlV .= $separador."'".$_POST["G2928_C56828"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56829"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56829 = '".$_POST["G2928_C56829"]."'";
                $LsqlI .= $separador."G2928_C56829";
                $LsqlV .= $separador."'".$_POST["G2928_C56829"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56830"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56830 = '".$_POST["G2928_C56830"]."'";
                $LsqlI .= $separador."G2928_C56830";
                $LsqlV .= $separador."'".$_POST["G2928_C56830"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56831"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56831 = '".$_POST["G2928_C56831"]."'";
                $LsqlI .= $separador."G2928_C56831";
                $LsqlV .= $separador."'".$_POST["G2928_C56831"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56832"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56832 = '".$_POST["G2928_C56832"]."'";
                $LsqlI .= $separador."G2928_C56832";
                $LsqlV .= $separador."'".$_POST["G2928_C56832"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56833"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56833 = '".$_POST["G2928_C56833"]."'";
                $LsqlI .= $separador."G2928_C56833";
                $LsqlV .= $separador."'".$_POST["G2928_C56833"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56834"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56834 = '".$_POST["G2928_C56834"]."'";
                $LsqlI .= $separador."G2928_C56834";
                $LsqlV .= $separador."'".$_POST["G2928_C56834"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56835"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56835 = '".$_POST["G2928_C56835"]."'";
                $LsqlI .= $separador."G2928_C56835";
                $LsqlV .= $separador."'".$_POST["G2928_C56835"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56836"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56836 = '".$_POST["G2928_C56836"]."'";
                $LsqlI .= $separador."G2928_C56836";
                $LsqlV .= $separador."'".$_POST["G2928_C56836"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56848"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56848 = '".$_POST["G2928_C56848"]."'";
                $LsqlI .= $separador."G2928_C56848";
                $LsqlV .= $separador."'".$_POST["G2928_C56848"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56849"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56849 = '".$_POST["G2928_C56849"]."'";
                $LsqlI .= $separador."G2928_C56849";
                $LsqlV .= $separador."'".$_POST["G2928_C56849"]."'";
                $validar = 1;
            }
             
 
            $G2928_C56837 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2928_C56837 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2928_C56837 = ".$G2928_C56837;
                    $LsqlI .= $separador." G2928_C56837";
                    $LsqlV .= $separador.$G2928_C56837;
                    $validar = 1;

                    
                }
            }
 
            $G2928_C56838 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2928_C56838 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2928_C56838 = ".$G2928_C56838;
                    $LsqlI .= $separador." G2928_C56838";
                    $LsqlV .= $separador.$G2928_C56838;
                    $validar = 1;
                }
            }
 
            $G2928_C56839 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2928_C56839 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2928_C56839 = ".$G2928_C56839;
                    $LsqlI .= $separador." G2928_C56839";
                    $LsqlV .= $separador.$G2928_C56839;
                    $validar = 1;
                }
            }
 
            $G2928_C56840 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2928_C56840 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2928_C56840 = ".$G2928_C56840;
                    $LsqlI .= $separador." G2928_C56840";
                    $LsqlV .= $separador.$G2928_C56840;
                    $validar = 1;
                }
            }
 
            $G2928_C56841 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2928_C56841 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2928_C56841 = ".$G2928_C56841;
                    $LsqlI .= $separador." G2928_C56841";
                    $LsqlV .= $separador.$G2928_C56841;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2928_C56842"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56842 = '".$_POST["G2928_C56842"]."'";
                $LsqlI .= $separador."G2928_C56842";
                $LsqlV .= $separador."'".$_POST["G2928_C56842"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56843"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56843 = '".$_POST["G2928_C56843"]."'";
                $LsqlI .= $separador."G2928_C56843";
                $LsqlV .= $separador."'".$_POST["G2928_C56843"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56844"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56844 = '".$_POST["G2928_C56844"]."'";
                $LsqlI .= $separador."G2928_C56844";
                $LsqlV .= $separador."'".$_POST["G2928_C56844"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2928_C56845"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_C56845 = '".$_POST["G2928_C56845"]."'";
                $LsqlI .= $separador."G2928_C56845";
                $LsqlV .= $separador."'".$_POST["G2928_C56845"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2928_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2928_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2928_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2928_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2928_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2928_Usuario , G2928_FechaInsercion, G2928_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2928_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2928 WHERE G2928_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;

                        $strSQLDeleteM_t = "DELETE FROM ".$BaseDatos.".G2926_M2090";
                        $mysqli->query($strSQLDeleteM_t);

                        echo $UltimoID;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
