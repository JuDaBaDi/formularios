<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2866_ConsInte__b, G2866_FechaInsercion , G2866_Usuario ,  G2866_CodigoMiembro  , G2866_PoblacionOrigen , G2866_EstadoDiligenciamiento ,  G2866_IdLlamada , G2866_C55440 as principal ,G2866_C55449,G2866_C55450,G2866_C55451,G2866_C55452,G2866_C55440,G2866_C55441,G2866_C55442,G2866_C55444,G2866_C55459,G2866_C55460,G2866_C55461,G2866_C55462,G2866_C60386,G2866_C55429,G2866_C55430,G2866_C55431,G2866_C55432,G2866_C55433,G2866_C55434,G2866_C55435,G2866_C55436,G2866_C55437,G2866_C55476,G2866_C55475,G2866_C55463,G2866_C55464,G2866_C55465,G2866_C55466,G2866_C55467,G2866_C55482,G2866_C55468,G2866_C55483,G2866_C55469,G2866_C55473,G2866_C55474,G2866_C55537,G2866_C55484,G2866_C55493,G2866_C55486,G2866_C55488,G2866_C55487,G2866_C56049,G2866_C55497,G2866_C61253,G2866_C61254,G2866_C61255,G2866_C61257,G2866_C63125,G2866_C63126,G2866_C63127,G2866_C63128 FROM '.$BaseDatos.'.G2866 WHERE G2866_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2866_C55449'] = $key->G2866_C55449;

                $datos[$i]['G2866_C55450'] = $key->G2866_C55450;

                $datos[$i]['G2866_C55451'] = $key->G2866_C55451;

                $datos[$i]['G2866_C55452'] = $key->G2866_C55452;

                $datos[$i]['G2866_C55440'] = $key->G2866_C55440;

                $datos[$i]['G2866_C55441'] = $key->G2866_C55441;

                $datos[$i]['G2866_C55442'] = $key->G2866_C55442;

                $datos[$i]['G2866_C55444'] = $key->G2866_C55444;

                $datos[$i]['G2866_C55459'] = $key->G2866_C55459;

                $datos[$i]['G2866_C55460'] = $key->G2866_C55460;

                $datos[$i]['G2866_C55461'] = $key->G2866_C55461;

                $datos[$i]['G2866_C55462'] = $key->G2866_C55462;

                $datos[$i]['G2866_C60386'] = explode(' ', $key->G2866_C60386)[0];

                $datos[$i]['G2866_C55429'] = $key->G2866_C55429;

                $datos[$i]['G2866_C55430'] = $key->G2866_C55430;

                $datos[$i]['G2866_C55431'] = explode(' ', $key->G2866_C55431)[0];
  
                $hora = '';
                if(!is_null($key->G2866_C55432)){
                    $hora = explode(' ', $key->G2866_C55432)[1];
                }

                $datos[$i]['G2866_C55432'] = $hora;

                $datos[$i]['G2866_C55433'] = $key->G2866_C55433;

                $datos[$i]['G2866_C55434'] = $key->G2866_C55434;

                $datos[$i]['G2866_C55435'] = $key->G2866_C55435;

                $datos[$i]['G2866_C55436'] = $key->G2866_C55436;

                $datos[$i]['G2866_C55437'] = $key->G2866_C55437;

                $datos[$i]['G2866_C55476'] = $key->G2866_C55476;

                $datos[$i]['G2866_C55475'] = $key->G2866_C55475;

                $datos[$i]['G2866_C55463'] = $key->G2866_C55463;

                $datos[$i]['G2866_C55464'] = $key->G2866_C55464;

                $datos[$i]['G2866_C55465'] = $key->G2866_C55465;

                $datos[$i]['G2866_C55466'] = $key->G2866_C55466;

                $datos[$i]['G2866_C55467'] = $key->G2866_C55467;

                $datos[$i]['G2866_C55482'] = $key->G2866_C55482;

                $datos[$i]['G2866_C55468'] = $key->G2866_C55468;

                $datos[$i]['G2866_C55483'] = $key->G2866_C55483;

                $datos[$i]['G2866_C55469'] = $key->G2866_C55469;

                $datos[$i]['G2866_C55473'] = $key->G2866_C55473;

                $datos[$i]['G2866_C55474'] = $key->G2866_C55474;

                $datos[$i]['G2866_C55537'] = $key->G2866_C55537;

                $datos[$i]['G2866_C55484'] = $key->G2866_C55484;

                $datos[$i]['G2866_C55493'] = $key->G2866_C55493;

                $datos[$i]['G2866_C55486'] = $key->G2866_C55486;

                $datos[$i]['G2866_C55488'] = $key->G2866_C55488;

                $datos[$i]['G2866_C55487'] = $key->G2866_C55487;

                $datos[$i]['G2866_C56049'] = $key->G2866_C56049;

                $datos[$i]['G2866_C55497'] = $key->G2866_C55497;

                $datos[$i]['G2866_C61253'] = $key->G2866_C61253;

                $datos[$i]['G2866_C61254'] = $key->G2866_C61254;

                $datos[$i]['G2866_C61255'] = explode(' ', $key->G2866_C61255)[0];

                $datos[$i]['G2866_C61257'] = $key->G2866_C61257;

                $datos[$i]['G2866_C63125'] = $key->G2866_C63125;

                $datos[$i]['G2866_C63126'] = $key->G2866_C63126;

                $datos[$i]['G2866_C63127'] = $key->G2866_C63127;

                $datos[$i]['G2866_C63128'] = $key->G2866_C63128;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2866";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2866_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2866_ConsInte__b as id,  G2866_C55440 as camp1 , G2866_C55441 as camp2 
                     FROM ".$BaseDatos.".G2866  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2866_ConsInte__b as id,  G2866_C55440 as camp1 , G2866_C55441 as camp2  
                    FROM ".$BaseDatos.".G2866  JOIN ".$BaseDatos.".G2866_M".$_POST['muestra']." ON G2866_ConsInte__b = G2866_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2866_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2866_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2866_C55440 LIKE '%".$B."%' OR G2866_C55441 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2866_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2866");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2866_ConsInte__b, G2866_FechaInsercion , G2866_Usuario ,  G2866_CodigoMiembro  , G2866_PoblacionOrigen , G2866_EstadoDiligenciamiento ,  G2866_IdLlamada , G2866_C55440 as principal ,G2866_C55449,G2866_C55450,G2866_C55451,G2866_C55452,G2866_C55440,G2866_C55441,G2866_C55442,G2866_C55444,G2866_C55459,G2866_C55460,G2866_C55461,G2866_C55462,G2866_C60386, a.LISOPC_Nombre____b as G2866_C55429, b.LISOPC_Nombre____b as G2866_C55430,G2866_C55431,G2866_C55432,G2866_C55433,G2866_C55434,G2866_C55435,G2866_C55436,G2866_C55437, c.LISOPC_Nombre____b as G2866_C55476, d.LISOPC_Nombre____b as G2866_C55475, e.LISOPC_Nombre____b as G2866_C55463, f.LISOPC_Nombre____b as G2866_C55464,G2866_C55465, g.LISOPC_Nombre____b as G2866_C55466, h.LISOPC_Nombre____b as G2866_C55467,G2866_C55482, i.LISOPC_Nombre____b as G2866_C55468,G2866_C55483, j.LISOPC_Nombre____b as G2866_C55469, k.LISOPC_Nombre____b as G2866_C55473, l.LISOPC_Nombre____b as G2866_C55474, m.LISOPC_Nombre____b as G2866_C55537, n.LISOPC_Nombre____b as G2866_C55484, o.LISOPC_Nombre____b as G2866_C55493, p.LISOPC_Nombre____b as G2866_C55486,G2866_C55488,G2866_C55487,G2866_C56049, q.LISOPC_Nombre____b as G2866_C55497, r.LISOPC_Nombre____b as G2866_C61253, s.LISOPC_Nombre____b as G2866_C61254,G2866_C61255, t.LISOPC_Nombre____b as G2866_C61257, u.LISOPC_Nombre____b as G2866_C63125, v.LISOPC_Nombre____b as G2866_C63126,G2866_C63127,G2866_C63128 FROM '.$BaseDatos.'.G2866 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2866_C55429 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2866_C55430 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2866_C55476 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2866_C55475 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2866_C55463 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2866_C55464 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2866_C55466 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2866_C55467 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2866_C55468 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2866_C55469 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G2866_C55473 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G2866_C55474 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G2866_C55537 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G2866_C55484 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G2866_C55493 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G2866_C55486 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as q ON q.LISOPC_ConsInte__b =  G2866_C55497 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as r ON r.LISOPC_ConsInte__b =  G2866_C61253 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as s ON s.LISOPC_ConsInte__b =  G2866_C61254 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as t ON t.LISOPC_ConsInte__b =  G2866_C61257 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as u ON u.LISOPC_ConsInte__b =  G2866_C63125 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as v ON v.LISOPC_ConsInte__b =  G2866_C63126';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2866_C55432)){
                    $hora_a = explode(' ', $fila->G2866_C55432)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2866_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2866_ConsInte__b , ($fila->G2866_C55449) , ($fila->G2866_C55450) , ($fila->G2866_C55451) , ($fila->G2866_C55452) , ($fila->G2866_C55440) , ($fila->G2866_C55441) , ($fila->G2866_C55442) , ($fila->G2866_C55444) , ($fila->G2866_C55459) , ($fila->G2866_C55460) , ($fila->G2866_C55461) , ($fila->G2866_C55462) , explode(' ', $fila->G2866_C60386)[0] , ($fila->G2866_C55429) , ($fila->G2866_C55430) , explode(' ', $fila->G2866_C55431)[0] , $hora_a , ($fila->G2866_C55433) , ($fila->G2866_C55434) , ($fila->G2866_C55435) , ($fila->G2866_C55436) , ($fila->G2866_C55437) , ($fila->G2866_C55476) , ($fila->G2866_C55475) , ($fila->G2866_C55463) , ($fila->G2866_C55464) , ($fila->G2866_C55465) , ($fila->G2866_C55466) , ($fila->G2866_C55467) , ($fila->G2866_C55482) , ($fila->G2866_C55468) , ($fila->G2866_C55483) , ($fila->G2866_C55469) , ($fila->G2866_C55473) , ($fila->G2866_C55474) , ($fila->G2866_C55537) , ($fila->G2866_C55484) , ($fila->G2866_C55493) , ($fila->G2866_C55486) , ($fila->G2866_C55488) , ($fila->G2866_C55487) , ($fila->G2866_C56049) , ($fila->G2866_C55497) , ($fila->G2866_C61253) , ($fila->G2866_C61254) , explode(' ', $fila->G2866_C61255)[0] , ($fila->G2866_C61257) , ($fila->G2866_C63125) , ($fila->G2866_C63126) , ($fila->G2866_C63127) , ($fila->G2866_C63128) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2866 WHERE G2866_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2866";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2866_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2866_ConsInte__b as id,  G2866_C55440 as camp1 , G2866_C55441 as camp2  FROM '.$BaseDatos.'.G2866 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2866_ConsInte__b as id,  G2866_C55440 as camp1 , G2866_C55441 as camp2  
                    FROM ".$BaseDatos.".G2866  JOIN ".$BaseDatos.".G2866_M".$_POST['muestra']." ON G2866_ConsInte__b = G2866_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2866_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2866_C55440 LIKE "%'.$B.'%" OR G2866_C55441 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2866_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2866 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2866(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2866_C55449"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55449 = '".$_POST["G2866_C55449"]."'";
                $LsqlI .= $separador."G2866_C55449";
                $LsqlV .= $separador."'".$_POST["G2866_C55449"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55450"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55450 = '".$_POST["G2866_C55450"]."'";
                $LsqlI .= $separador."G2866_C55450";
                $LsqlV .= $separador."'".$_POST["G2866_C55450"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55451"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55451 = '".$_POST["G2866_C55451"]."'";
                $LsqlI .= $separador."G2866_C55451";
                $LsqlV .= $separador."'".$_POST["G2866_C55451"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55452"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55452 = '".$_POST["G2866_C55452"]."'";
                $LsqlI .= $separador."G2866_C55452";
                $LsqlV .= $separador."'".$_POST["G2866_C55452"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55440"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55440 = '".$_POST["G2866_C55440"]."'";
                $LsqlI .= $separador."G2866_C55440";
                $LsqlV .= $separador."'".$_POST["G2866_C55440"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55441"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55441 = '".$_POST["G2866_C55441"]."'";
                $LsqlI .= $separador."G2866_C55441";
                $LsqlV .= $separador."'".$_POST["G2866_C55441"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55442"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55442 = '".$_POST["G2866_C55442"]."'";
                $LsqlI .= $separador."G2866_C55442";
                $LsqlV .= $separador."'".$_POST["G2866_C55442"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55444"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55444 = '".$_POST["G2866_C55444"]."'";
                $LsqlI .= $separador."G2866_C55444";
                $LsqlV .= $separador."'".$_POST["G2866_C55444"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55459"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55459 = '".$_POST["G2866_C55459"]."'";
                $LsqlI .= $separador."G2866_C55459";
                $LsqlV .= $separador."'".$_POST["G2866_C55459"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55460"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55460 = '".$_POST["G2866_C55460"]."'";
                $LsqlI .= $separador."G2866_C55460";
                $LsqlV .= $separador."'".$_POST["G2866_C55460"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55461"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55461 = '".$_POST["G2866_C55461"]."'";
                $LsqlI .= $separador."G2866_C55461";
                $LsqlV .= $separador."'".$_POST["G2866_C55461"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55462"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55462 = '".$_POST["G2866_C55462"]."'";
                $LsqlI .= $separador."G2866_C55462";
                $LsqlV .= $separador."'".$_POST["G2866_C55462"]."'";
                $validar = 1;
            }
             
 
            $G2866_C60386 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2866_C60386"])){    
                if($_POST["G2866_C60386"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2866_C60386"]);
                    if(count($tieneHora) > 1){
                        $G2866_C60386 = "'".$_POST["G2866_C60386"]."'";
                    }else{
                        $G2866_C60386 = "'".str_replace(' ', '',$_POST["G2866_C60386"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2866_C60386 = ".$G2866_C60386;
                    $LsqlI .= $separador." G2866_C60386";
                    $LsqlV .= $separador.$G2866_C60386;
                    $validar = 1;
                }
            }
 
            $G2866_C55429 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2866_C55429 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2866_C55429 = ".$G2866_C55429;
                    $LsqlI .= $separador." G2866_C55429";
                    $LsqlV .= $separador.$G2866_C55429;
                    $validar = 1;

                    
                }
            }
 
            $G2866_C55430 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2866_C55430 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2866_C55430 = ".$G2866_C55430;
                    $LsqlI .= $separador." G2866_C55430";
                    $LsqlV .= $separador.$G2866_C55430;
                    $validar = 1;
                }
            }
 
            $G2866_C55431 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2866_C55431 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2866_C55431 = ".$G2866_C55431;
                    $LsqlI .= $separador." G2866_C55431";
                    $LsqlV .= $separador.$G2866_C55431;
                    $validar = 1;
                }
            }
 
            $G2866_C55432 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2866_C55432 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2866_C55432 = ".$G2866_C55432;
                    $LsqlI .= $separador." G2866_C55432";
                    $LsqlV .= $separador.$G2866_C55432;
                    $validar = 1;
                }
            }
 
            $G2866_C55433 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2866_C55433 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2866_C55433 = ".$G2866_C55433;
                    $LsqlI .= $separador." G2866_C55433";
                    $LsqlV .= $separador.$G2866_C55433;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2866_C55434"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55434 = '".$_POST["G2866_C55434"]."'";
                $LsqlI .= $separador."G2866_C55434";
                $LsqlV .= $separador."'".$_POST["G2866_C55434"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55435"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55435 = '".$_POST["G2866_C55435"]."'";
                $LsqlI .= $separador."G2866_C55435";
                $LsqlV .= $separador."'".$_POST["G2866_C55435"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55436"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55436 = '".$_POST["G2866_C55436"]."'";
                $LsqlI .= $separador."G2866_C55436";
                $LsqlV .= $separador."'".$_POST["G2866_C55436"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55437"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55437 = '".$_POST["G2866_C55437"]."'";
                $LsqlI .= $separador."G2866_C55437";
                $LsqlV .= $separador."'".$_POST["G2866_C55437"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55438"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55438 = '".$_POST["G2866_C55438"]."'";
                $LsqlI .= $separador."G2866_C55438";
                $LsqlV .= $separador."'".$_POST["G2866_C55438"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55439"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55439 = '".$_POST["G2866_C55439"]."'";
                $LsqlI .= $separador."G2866_C55439";
                $LsqlV .= $separador."'".$_POST["G2866_C55439"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56042"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56042 = '".$_POST["G2866_C56042"]."'";
                $LsqlI .= $separador."G2866_C56042";
                $LsqlV .= $separador."'".$_POST["G2866_C56042"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55476"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55476 = '".$_POST["G2866_C55476"]."'";
                $LsqlI .= $separador."G2866_C55476";
                $LsqlV .= $separador."'".$_POST["G2866_C55476"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55475"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55475 = '".$_POST["G2866_C55475"]."'";
                $LsqlI .= $separador."G2866_C55475";
                $LsqlV .= $separador."'".$_POST["G2866_C55475"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55477"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55477 = '".$_POST["G2866_C55477"]."'";
                $LsqlI .= $separador."G2866_C55477";
                $LsqlV .= $separador."'".$_POST["G2866_C55477"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55478"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55478 = '".$_POST["G2866_C55478"]."'";
                $LsqlI .= $separador."G2866_C55478";
                $LsqlV .= $separador."'".$_POST["G2866_C55478"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55479"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55479 = '".$_POST["G2866_C55479"]."'";
                $LsqlI .= $separador."G2866_C55479";
                $LsqlV .= $separador."'".$_POST["G2866_C55479"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55480"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55480 = '".$_POST["G2866_C55480"]."'";
                $LsqlI .= $separador."G2866_C55480";
                $LsqlV .= $separador."'".$_POST["G2866_C55480"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55463"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55463 = '".$_POST["G2866_C55463"]."'";
                $LsqlI .= $separador."G2866_C55463";
                $LsqlV .= $separador."'".$_POST["G2866_C55463"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55464"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55464 = '".$_POST["G2866_C55464"]."'";
                $LsqlI .= $separador."G2866_C55464";
                $LsqlV .= $separador."'".$_POST["G2866_C55464"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55465"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55465 = '".$_POST["G2866_C55465"]."'";
                $LsqlI .= $separador."G2866_C55465";
                $LsqlV .= $separador."'".$_POST["G2866_C55465"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55466"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55466 = '".$_POST["G2866_C55466"]."'";
                $LsqlI .= $separador."G2866_C55466";
                $LsqlV .= $separador."'".$_POST["G2866_C55466"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55467"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55467 = '".$_POST["G2866_C55467"]."'";
                $LsqlI .= $separador."G2866_C55467";
                $LsqlV .= $separador."'".$_POST["G2866_C55467"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55482"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55482 = '".$_POST["G2866_C55482"]."'";
                $LsqlI .= $separador."G2866_C55482";
                $LsqlV .= $separador."'".$_POST["G2866_C55482"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55468"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55468 = '".$_POST["G2866_C55468"]."'";
                $LsqlI .= $separador."G2866_C55468";
                $LsqlV .= $separador."'".$_POST["G2866_C55468"]."'";
                $validar = 1;
            }
             
  
            $G2866_C55483 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2866_C55483"])){
                if($_POST["G2866_C55483"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2866_C55483 = $_POST["G2866_C55483"];
                    $LsqlU .= $separador." G2866_C55483 = ".$G2866_C55483."";
                    $LsqlI .= $separador." G2866_C55483";
                    $LsqlV .= $separador.$G2866_C55483;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2866_C55495"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55495 = '".$_POST["G2866_C55495"]."'";
                $LsqlI .= $separador."G2866_C55495";
                $LsqlV .= $separador."'".$_POST["G2866_C55495"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55469"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55469 = '".$_POST["G2866_C55469"]."'";
                $LsqlI .= $separador."G2866_C55469";
                $LsqlV .= $separador."'".$_POST["G2866_C55469"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55473"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55473 = '".$_POST["G2866_C55473"]."'";
                $LsqlI .= $separador."G2866_C55473";
                $LsqlV .= $separador."'".$_POST["G2866_C55473"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55474"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55474 = '".$_POST["G2866_C55474"]."'";
                $LsqlI .= $separador."G2866_C55474";
                $LsqlV .= $separador."'".$_POST["G2866_C55474"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56043"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56043 = '".$_POST["G2866_C56043"]."'";
                $LsqlI .= $separador."G2866_C56043";
                $LsqlV .= $separador."'".$_POST["G2866_C56043"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56044"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56044 = '".$_POST["G2866_C56044"]."'";
                $LsqlI .= $separador."G2866_C56044";
                $LsqlV .= $separador."'".$_POST["G2866_C56044"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56045"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56045 = '".$_POST["G2866_C56045"]."'";
                $LsqlI .= $separador."G2866_C56045";
                $LsqlV .= $separador."'".$_POST["G2866_C56045"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56046"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56046 = '".$_POST["G2866_C56046"]."'";
                $LsqlI .= $separador."G2866_C56046";
                $LsqlV .= $separador."'".$_POST["G2866_C56046"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56047"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56047 = '".$_POST["G2866_C56047"]."'";
                $LsqlI .= $separador."G2866_C56047";
                $LsqlV .= $separador."'".$_POST["G2866_C56047"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56048"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56048 = '".$_POST["G2866_C56048"]."'";
                $LsqlI .= $separador."G2866_C56048";
                $LsqlV .= $separador."'".$_POST["G2866_C56048"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55485"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55485 = '".$_POST["G2866_C55485"]."'";
                $LsqlI .= $separador."G2866_C55485";
                $LsqlV .= $separador."'".$_POST["G2866_C55485"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55489"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55489 = '".$_POST["G2866_C55489"]."'";
                $LsqlI .= $separador."G2866_C55489";
                $LsqlV .= $separador."'".$_POST["G2866_C55489"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55490"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55490 = '".$_POST["G2866_C55490"]."'";
                $LsqlI .= $separador."G2866_C55490";
                $LsqlV .= $separador."'".$_POST["G2866_C55490"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55491"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55491 = '".$_POST["G2866_C55491"]."'";
                $LsqlI .= $separador."G2866_C55491";
                $LsqlV .= $separador."'".$_POST["G2866_C55491"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55492"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55492 = '".$_POST["G2866_C55492"]."'";
                $LsqlI .= $separador."G2866_C55492";
                $LsqlV .= $separador."'".$_POST["G2866_C55492"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55538"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55538 = '".$_POST["G2866_C55538"]."'";
                $LsqlI .= $separador."G2866_C55538";
                $LsqlV .= $separador."'".$_POST["G2866_C55538"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55537"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55537 = '".$_POST["G2866_C55537"]."'";
                $LsqlI .= $separador."G2866_C55537";
                $LsqlV .= $separador."'".$_POST["G2866_C55537"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55484"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55484 = '".$_POST["G2866_C55484"]."'";
                $LsqlI .= $separador."G2866_C55484";
                $LsqlV .= $separador."'".$_POST["G2866_C55484"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55494"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55494 = '".$_POST["G2866_C55494"]."'";
                $LsqlI .= $separador."G2866_C55494";
                $LsqlV .= $separador."'".$_POST["G2866_C55494"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55493"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55493 = '".$_POST["G2866_C55493"]."'";
                $LsqlI .= $separador."G2866_C55493";
                $LsqlV .= $separador."'".$_POST["G2866_C55493"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55486"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55486 = '".$_POST["G2866_C55486"]."'";
                $LsqlI .= $separador."G2866_C55486";
                $LsqlV .= $separador."'".$_POST["G2866_C55486"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55488"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55488 = '".$_POST["G2866_C55488"]."'";
                $LsqlI .= $separador."G2866_C55488";
                $LsqlV .= $separador."'".$_POST["G2866_C55488"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55487"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55487 = '".$_POST["G2866_C55487"]."'";
                $LsqlI .= $separador."G2866_C55487";
                $LsqlV .= $separador."'".$_POST["G2866_C55487"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55496"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55496 = '".$_POST["G2866_C55496"]."'";
                $LsqlI .= $separador."G2866_C55496";
                $LsqlV .= $separador."'".$_POST["G2866_C55496"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C56049"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C56049 = '".$_POST["G2866_C56049"]."'";
                $LsqlI .= $separador."G2866_C56049";
                $LsqlV .= $separador."'".$_POST["G2866_C56049"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55497"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55497 = '".$_POST["G2866_C55497"]."'";
                $LsqlI .= $separador."G2866_C55497";
                $LsqlV .= $separador."'".$_POST["G2866_C55497"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55498"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55498 = '".$_POST["G2866_C55498"]."'";
                $LsqlI .= $separador."G2866_C55498";
                $LsqlV .= $separador."'".$_POST["G2866_C55498"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C55499"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C55499 = '".$_POST["G2866_C55499"]."'";
                $LsqlI .= $separador."G2866_C55499";
                $LsqlV .= $separador."'".$_POST["G2866_C55499"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C61253"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C61253 = '".$_POST["G2866_C61253"]."'";
                $LsqlI .= $separador."G2866_C61253";
                $LsqlV .= $separador."'".$_POST["G2866_C61253"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C61254"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C61254 = '".$_POST["G2866_C61254"]."'";
                $LsqlI .= $separador."G2866_C61254";
                $LsqlV .= $separador."'".$_POST["G2866_C61254"]."'";
                $validar = 1;
            }
             
 
            $G2866_C61255 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2866_C61255"])){    
                if($_POST["G2866_C61255"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2866_C61255"]);
                    if(count($tieneHora) > 1){
                        $G2866_C61255 = "'".$_POST["G2866_C61255"]."'";
                    }else{
                        $G2866_C61255 = "'".str_replace(' ', '',$_POST["G2866_C61255"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2866_C61255 = ".$G2866_C61255;
                    $LsqlI .= $separador." G2866_C61255";
                    $LsqlV .= $separador.$G2866_C61255;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2866_C61257"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C61257 = '".$_POST["G2866_C61257"]."'";
                $LsqlI .= $separador."G2866_C61257";
                $LsqlV .= $separador."'".$_POST["G2866_C61257"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C63125"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C63125 = '".$_POST["G2866_C63125"]."'";
                $LsqlI .= $separador."G2866_C63125";
                $LsqlV .= $separador."'".$_POST["G2866_C63125"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C63126"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C63126 = '".$_POST["G2866_C63126"]."'";
                $LsqlI .= $separador."G2866_C63126";
                $LsqlV .= $separador."'".$_POST["G2866_C63126"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C63127"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C63127 = '".$_POST["G2866_C63127"]."'";
                $LsqlI .= $separador."G2866_C63127";
                $LsqlV .= $separador."'".$_POST["G2866_C63127"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C63128"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C63128 = '".$_POST["G2866_C63128"]."'";
                $LsqlI .= $separador."G2866_C63128";
                $LsqlV .= $separador."'".$_POST["G2866_C63128"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2866_C63129"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_C63129 = '".$_POST["G2866_C63129"]."'";
                $LsqlI .= $separador."G2866_C63129";
                $LsqlV .= $separador."'".$_POST["G2866_C63129"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2866_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2866_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2866_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2866_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2866_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2866_Usuario , G2866_FechaInsercion, G2866_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2866_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2866 WHERE G2866_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
