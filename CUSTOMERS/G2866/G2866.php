
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2866/G2866_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2866_ConsInte__b as id, G2866_C55440 as camp1 , G2866_C55441 as camp2 FROM ".$BaseDatos.".G2866  WHERE G2866_Usuario = ".$idUsuario." ORDER BY G2866_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2866_ConsInte__b as id, G2866_C55440 as camp1 , G2866_C55441 as camp2 FROM ".$BaseDatos.".G2866  ORDER BY G2866_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2866_ConsInte__b as id, G2866_C55440 as camp1 , G2866_C55441 as camp2 FROM ".$BaseDatos.".G2866 JOIN ".$BaseDatos.".G2866_M".$resultEstpas->muestr." ON G2866_ConsInte__b = G2866_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2866_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2866_ConsInte__b as id, G2866_C55440 as camp1 , G2866_C55441 as camp2 FROM ".$BaseDatos.".G2866 JOIN ".$BaseDatos.".G2866_M".$resultEstpas->muestr." ON G2866_ConsInte__b = G2866_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2866_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2866_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G2866_ConsInte__b as id, G2866_C55440 as camp1 , G2866_C55441 as camp2 FROM ".$BaseDatos.".G2866  ORDER BY G2866_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    
    //JDBD - Validamos si se pudo obtener el dato principal.
    if ($nombre = $mysqli->query($XLsql)) {

        $nombreUsuario = NULL;
        //echo $XLsql;
        while ($key = $nombre->fetch_object()) {
            echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
            $nombreUsuario = $key->nombre;
            break;
        } 


        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                        
            $data = array(  "strToken_t" => $_GET['token'], 
                            "strIdGestion_t" => $_GET['id_gestion_cbx'],
                            "strDatoPrincipal_t" => $nombreUsuario,
                            "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
            $data_string = json_encode($data);    

            $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                      
            ); 
            //recogemos la respuesta
            $respuesta = curl_exec ($ch);
            //o el error, por si falla
            $error = curl_error($ch);
            //y finalmente cerramos curl
            //echo "Respuesta =>  ". $respuesta;
            //echo "<br/>Error => ".$error;
            //include "Log.class.php";
            //$log = new Log("log", "./Log/");
            //$log->insert($error, $respuesta, false, true, false);
            //echo "nada";
            curl_close ($ch);
        }

    }else{
        echo "<script>alert('NO SE PUDO OBTENER EL DATO PRINCIPAL DEL REGISTRO.');</script>";
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="8638" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8638c">
                CONVERSACION
            </a>
        </h4>
        
    </div>
    <div id="s_8638c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días, tardes, noches, hablo con la Sr(a) |nombre cliente| Mi nombre es |Agente| Trabajo en la funeraria LA AURORA</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">somos expertos a nivel nacional e internacional con 27 años de experiencia como se encuentra el día de hoy Sr(a)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr (a)...Le recuerdo que nuestra llamada es grabada y monitoreada para efectos de calidad y legalidad entre las partes y lo aquí pactado.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55476" id="LblG2866_C55476">Vamos a iniciar una actualización de datos y encuesta, prometo no quitarle mucho tiempo, de acuerdo?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55476" id="G2866_C55476">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3373 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55475" id="LblG2866_C55475">Aprovecho para recordarle lo importante que es usted para nuestra empresa, es un buen momento para que podamos hablar?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55475" id="G2866_C55475">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3373 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Gracias por atender nuestra llamada, consideramos que no es apropiado colocar en riesgo su familia en estos momentos con personal que lo visite, por eso hemos fortalecido nuestro modelo de negocio siguiendo los protocolos de Bioseguridad</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">las ventas y el contacto con nuestros afiliados lo estamos haciendo a través de nuestro call center</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">aprovechando que tenemos la plataforma y la tecnología adecuada para hacerlo, por su seguridad, la de su familia y las nuestras.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">SOLO PARA ASESOR: actualizar datos basicos en los sistemas funerarios SAF y BOW (nombre titular, cedula, telefonos, correo eletronico, fecha nacimiento, direccion)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="8635" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8635c">
                ACTUALIZACION DE DATOS
            </a>
        </h4>
        
    </div>
    <div id="s_8635c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55440" id="LblG2866_C55440">NOMBRE TOMADOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55440" value="<?php if (isset($_GET['G2866_C55440'])) {
                            echo $_GET['G2866_C55440'];
                        } ?>"  name="G2866_C55440"  placeholder="NOMBRE TOMADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55441" id="LblG2866_C55441">CEDULA TOMADOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55441" value="<?php if (isset($_GET['G2866_C55441'])) {
                            echo $_GET['G2866_C55441'];
                        } ?>"  name="G2866_C55441"  placeholder="CEDULA TOMADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55442" id="LblG2866_C55442">LOCALIDAD TOMADOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55442" value="<?php if (isset($_GET['G2866_C55442'])) {
                            echo $_GET['G2866_C55442'];
                        } ?>"  name="G2866_C55442"  placeholder="LOCALIDAD TOMADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55444" id="LblG2866_C55444">DIRECCION TOMADOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55444" value="<?php if (isset($_GET['G2866_C55444'])) {
                            echo $_GET['G2866_C55444'];
                        } ?>"  name="G2866_C55444"  placeholder="DIRECCION TOMADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55459" id="LblG2866_C55459">TELEFONO PERSONAL</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55459" value="<?php if (isset($_GET['G2866_C55459'])) {
                            echo $_GET['G2866_C55459'];
                        } ?>"  name="G2866_C55459"  placeholder="TELEFONO PERSONAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55460" id="LblG2866_C55460">TELEFONO RESIDENCIA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55460" value="<?php if (isset($_GET['G2866_C55460'])) {
                            echo $_GET['G2866_C55460'];
                        } ?>"  name="G2866_C55460"  placeholder="TELEFONO RESIDENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55461" id="LblG2866_C55461">TELEFONO LABORAL</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55461" value="<?php if (isset($_GET['G2866_C55461'])) {
                            echo $_GET['G2866_C55461'];
                        } ?>"  name="G2866_C55461"  placeholder="TELEFONO LABORAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55462" id="LblG2866_C55462">TELEFONO ALTERNO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55462" value="<?php if (isset($_GET['G2866_C55462'])) {
                            echo $_GET['G2866_C55462'];
                        } ?>"  name="G2866_C55462"  placeholder="TELEFONO ALTERNO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2866_C60386" id="LblG2866_C60386">FECHA ACTUALIZADA DE CARGUE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2866_C60386'])) {
                            echo $_GET['G2866_C60386'];
                        } ?>" readonly name="G2866_C60386" id="G2866_C60386" placeholder="YYYY-MM-DD" nombre="FECHA ACTUALIZADA DE CARGUE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  id="8637" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55434" id="LblG2866_C55434">Agente</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55434" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G2866_C55434"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55435" id="LblG2866_C55435">Fecha</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55435" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G2866_C55435"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55436" id="LblG2866_C55436">Hora</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55436" value="<?php echo date('H:i:s');?>" readonly name="G2866_C55436"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55437" id="LblG2866_C55437">Campaña</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55437" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G2866_C55437"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="8641" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8641c">
                ENCUESTA
            </a>
        </h4>
        
    </div>
    <div id="s_8641c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55463" id="LblG2866_C55463">Califique de 1 a 5 el servicio de La Aurora</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55463" id="G2866_C55463">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3370 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55464" id="LblG2866_C55464">Recomendaria La Aurora</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55464" id="G2866_C55464">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3371 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55465" id="LblG2866_C55465">¿Porque la recomendaria?</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55465" value="<?php if (isset($_GET['G2866_C55465'])) {
                            echo $_GET['G2866_C55465'];
                        } ?>"  name="G2866_C55465"  placeholder="¿Porque la recomendaria?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55466" id="LblG2866_C55466">Malas referencias</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55466" id="G2866_C55466">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1347 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55467" id="LblG2866_C55467">¿Quien dio las malas referencias?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55467" id="G2866_C55467">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3372 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55482" id="LblG2866_C55482">¿Que malas referencias le dieron?</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55482" value="<?php if (isset($_GET['G2866_C55482'])) {
                            echo $_GET['G2866_C55482'];
                        } ?>"  name="G2866_C55482"  placeholder="¿Que malas referencias le dieron?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55468" id="LblG2866_C55468">¿Tiene whatsapp?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55468" id="G2866_C55468">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3373 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2866_C55483" id="LblG2866_C55483">¿Que número es?</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2866_C55483'])) {
                            echo $_GET['G2866_C55483'];
                        } ?>"  name="G2866_C55483" id="G2866_C55483" placeholder="¿Que número es?">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  id="8636" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="8643" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8643c">
                VALIDACION CARPETA DE PAGO
            </a>
        </h4>
        
    </div>
    <div id="s_8643c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">El recaudo es atreves del pago acordado</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Usted paga con…. También tenemos las siguientes</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">EFECTY, SUSUERTE - Manizales</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">FACILISIMO - Quindío</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">APOSTAR - Risaralda</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">PSE - nuestra página web</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">INFORMACION SOLO PARA EL ASESOR</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Acuerdo de pago (si el cliente no esta al dia realizar acuerdo de pago)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Acuerdo de pago 1 (3 meses en mora, paga 2 se le condona1)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Acuerdo de pago 2 (4 meses en mora, paga 2 se le condona 2)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Acuerdo de pago 3 (5 meses en mora, paga 2 se le condona 3)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Acuerdo de pago 4 (2 meses en mora, paga 1 se le condona 1) SI DA REFERIDO EXITOSO</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55537" id="LblG2866_C55537">Referido</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55537" id="G2866_C55537">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55484" id="LblG2866_C55484">¿Cliente al día?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55484" id="G2866_C55484">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3373 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el cliente se encuentra en mora realizar acuerdo de pago</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55493" id="LblG2866_C55493">¿Realiza acuerdo de pago?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55493" id="G2866_C55493">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3378 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55486" id="LblG2866_C55486">Solicitud de referidos</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55486" id="G2866_C55486">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3377 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55488" id="LblG2866_C55488">Telefono referido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55488" value="<?php if (isset($_GET['G2866_C55488'])) {
                            echo $_GET['G2866_C55488'];
                        } ?>"  name="G2866_C55488"  placeholder="Telefono referido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C55487" id="LblG2866_C55487">Nombre referido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C55487" value="<?php if (isset($_GET['G2866_C55487'])) {
                            echo $_GET['G2866_C55487'];
                        } ?>"  name="G2866_C55487"  placeholder="Nombre referido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="8642" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8642c">
                MASCOTA
            </a>
        </h4>
        
    </div>
    <div id="s_8642c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr(a)… Le recuerdo que en la Aurora también tenemos la protección para su mascota.  Usted tiene alguna mascota que desee proteger?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55469" id="LblG2866_C55469">¿Tiene mascota?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55469" id="G2866_C55469">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3382 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55473" id="LblG2866_C55473">¿le interesa la previsión exequial para su mascota?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55473" id="G2866_C55473">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3375 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55474" id="LblG2866_C55474">venta efectiva?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55474" id="G2866_C55474">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3373 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="8644" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8644c">
                CDA
            </a>
        </h4>
        
    </div>
    <div id="s_8644c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Quiero contarle por ser un cliente preferencial de la Aurora usted tiene derecho a los servicios de nuestro Centro de duelo que tiene como objetivo apoyo psicosocial en el proceso del duelo duelo y también apoyo en estos tiempos difíciles</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C56049" id="LblG2866_C56049">PARA EL ASESOR: Comentarle al cliente sobre las charlas mensuales por Facebook</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C56049" value="<?php if (isset($_GET['G2866_C56049'])) {
                            echo $_GET['G2866_C56049'];
                        } ?>"  name="G2866_C56049"  placeholder="PARA EL ASESOR: Comentarle al cliente sobre las charlas mensuales por Facebook"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C55497" id="LblG2866_C55497">¿esta interesado en recibi información del centro de duelo?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C55497" id="G2866_C55497">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3379 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">INFORMACION PARA EL ASEOR: Si el cliente esta interesado, indicarle que va a enviar sus datos a la psicologa del centro de duelo para que se coloque en contacto con el.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="8645" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8645c">
                Recomendaciones de Bioseguridad
            </a>
        </h4>
        
    </div>
    <div id="s_8645c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Para terminar esta conversación quiero hacerle algunas recomendaciones según la organización mundial de la salud en normas de bioseguridad, como lavarse las manos frecuentemente, conservar la distancia y distanciamiento social.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9469" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9469c">
                CAMPOS A DILIGENCIAR
            </a>
        </h4>
        
    </div>
    <div id="s_9469c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C61253" id="LblG2866_C61253">Nombre de la empresa que realizo la venta</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C61253" id="G2866_C61253">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3699 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C61254" id="LblG2866_C61254">Nombre de la empresa para la que se realiza la venta</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C61254" id="G2866_C61254">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3699 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2866_C61255" id="LblG2866_C61255">Fecha de realización de llamada</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2866_C61255'])) {
                            echo $_GET['G2866_C61255'];
                        } ?>"  name="G2866_C61255" id="G2866_C61255" placeholder="YYYY-MM-DD" nombre="Fecha de realización de llamada">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C61257" id="LblG2866_C61257">Tipo de campaña</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C61257" id="G2866_C61257">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3698 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9770" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9770c">
                ENCUESTA BORBUR Y PAUNA
            </a>
        </h4>
        
    </div>
    <div id="s_9770c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C63125" id="LblG2866_C63125">Ha recibido malas referencias?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C63125" id="G2866_C63125">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3373 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2866_C63126" id="LblG2866_C63126">De quien ha sido las malas referencias?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2866_C63126" id="G2866_C63126">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3372 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C63127" id="LblG2866_C63127">Nombre empresaria funeraria?</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C63127" value="<?php if (isset($_GET['G2866_C63127'])) {
                            echo $_GET['G2866_C63127'];
                        } ?>"  name="G2866_C63127"  placeholder="Nombre empresaria funeraria?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2866_C63128" id="LblG2866_C63128">Cual fue la mala referencia?</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2866_C63128" value="<?php if (isset($_GET['G2866_C63128'])) {
                            echo $_GET['G2866_C63128'];
                        } ?>"  name="G2866_C63128"  placeholder="Cual fue la mala referencia?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">RECORDAR BENEFICIOS DE LA AURORA FUNERALES Y CAPILLAS</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2866_C55429">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3368;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2866_C55429">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3368;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2866_C55430">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2866_C55431" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2866_C55432" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2866_C55433" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2866/G2866_eventos.js"></script>
<script type="text/javascript" src="formularios/G2866/G2866_extender_funcionalidad.php"></script><?php require_once "G2866_extender_funcionalidad.php";?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G2866_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G2866_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G2866_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G2866_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G2866_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G2866_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G2866_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G2866_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2866_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G2866_C55436").val("<?php echo date('H:i:s');?>");
            $("#G2866_C63125").val("0").trigger("change");
            $("#G2866_C63126").val("0").trigger("change");
            
            
        });
    <?php } ?>;
    let arr;
    <?php if(isset($_GET['id_campana_cbx']) && $_GET['id_campana_cbx'] !="" && $_GET['id_campana_cbx'] !=0) :?>
        <?php
        $intHuesped=$mysqli->query("SELECT CAMPAN_ConsInte__PROYEC_b FROM DYALOGOCRM_SISTEMA.CAMPAN WHERE CAMPAN_IdCamCbx__b={$_GET['id_campana_cbx']}");
        if($intHuesped && $intHuesped->num_rows==1){
            $intHuesped=$intHuesped->fetch_object();
            $intHuesped=$intHuesped->CAMPAN_ConsInte__PROYEC_b;
            if($_GET['sentido'] ==1){
                $patron=ObtenerPatron($intHuesped,$_GET['campana_crm']);
            }else{
                $patron=ObtenerPatron($intHuesped);
            }
        } 
        ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php endif; ?>
    <?php if(isset($_SESSION['HUESPED_CRM']) && $_SESSION['HUESPED_CRM']!=0 && $_SESSION['HUESPED_CRM'] !="") {
        $patron=ObtenerPatron($_SESSION['HUESPED_CRM']); ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php } ?>
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2866_C55449").val(item.G2866_C55449); 
                $("#G2866_C55450").val(item.G2866_C55450); 
                $("#G2866_C55451").val(item.G2866_C55451); 
                $("#G2866_C55452").val(item.G2866_C55452); 
                $("#G2866_C55440").val(item.G2866_C55440); 
                $("#G2866_C55441").val(item.G2866_C55441); 
                $("#G2866_C55442").val(item.G2866_C55442); 
                $("#G2866_C55444").val(item.G2866_C55444); 
                $("#G2866_C55459").val(item.G2866_C55459); 
                $("#G2866_C55460").val(item.G2866_C55460); 
                $("#G2866_C55461").val(item.G2866_C55461); 
                $("#G2866_C55462").val(item.G2866_C55462); 
                $("#G2866_C60386").val(item.G2866_C60386); 
                $("#G2866_C55429").val(item.G2866_C55429).trigger("change");  
                $("#G2866_C55430").val(item.G2866_C55430).trigger("change");  
                $("#G2866_C55431").val(item.G2866_C55431); 
                $("#G2866_C55432").val(item.G2866_C55432); 
                $("#G2866_C55433").val(item.G2866_C55433); 
                $("#G2866_C55434").val(item.G2866_C55434); 
                $("#G2866_C55435").val(item.G2866_C55435); 
                $("#G2866_C55436").val(item.G2866_C55436); 
                $("#G2866_C55437").val(item.G2866_C55437);   
                if(item.G2866_C55438 == 1){
                    $("#G2866_C55438").attr('checked', true);
                }    
                if(item.G2866_C55439 == 1){
                    $("#G2866_C55439").attr('checked', true);
                }    
                if(item.G2866_C56042 == 1){
                    $("#G2866_C56042").attr('checked', true);
                }  
                $("#G2866_C55476").val(item.G2866_C55476).trigger("change");  
                $("#G2866_C55475").val(item.G2866_C55475).trigger("change");    
                if(item.G2866_C55477 == 1){
                    $("#G2866_C55477").attr('checked', true);
                }    
                if(item.G2866_C55478 == 1){
                    $("#G2866_C55478").attr('checked', true);
                }    
                if(item.G2866_C55479 == 1){
                    $("#G2866_C55479").attr('checked', true);
                }    
                if(item.G2866_C55480 == 1){
                    $("#G2866_C55480").attr('checked', true);
                }  
                $("#G2866_C55463").val(item.G2866_C55463).trigger("change");  
                $("#G2866_C55464").val(item.G2866_C55464).trigger("change");  
                $("#G2866_C55465").val(item.G2866_C55465); 
                $("#G2866_C55466").val(item.G2866_C55466).trigger("change");  
                $("#G2866_C55467").attr("opt",item.G2866_C55467);  
                $("#G2866_C55482").val(item.G2866_C55482); 
                $("#G2866_C55468").val(item.G2866_C55468).trigger("change");  
                $("#G2866_C55483").val(item.G2866_C55483);   
                if(item.G2866_C55495 == 1){
                    $("#G2866_C55495").attr('checked', true);
                }  
                $("#G2866_C55469").val(item.G2866_C55469).trigger("change");  
                $("#G2866_C55473").attr("opt",item.G2866_C55473);  
                $("#G2866_C55474").attr("opt",item.G2866_C55474);    
                if(item.G2866_C56043 == 1){
                    $("#G2866_C56043").attr('checked', true);
                }    
                if(item.G2866_C56044 == 1){
                    $("#G2866_C56044").attr('checked', true);
                }    
                if(item.G2866_C56045 == 1){
                    $("#G2866_C56045").attr('checked', true);
                }    
                if(item.G2866_C56046 == 1){
                    $("#G2866_C56046").attr('checked', true);
                }    
                if(item.G2866_C56047 == 1){
                    $("#G2866_C56047").attr('checked', true);
                }    
                if(item.G2866_C56048 == 1){
                    $("#G2866_C56048").attr('checked', true);
                }    
                if(item.G2866_C55485 == 1){
                    $("#G2866_C55485").attr('checked', true);
                }    
                if(item.G2866_C55489 == 1){
                    $("#G2866_C55489").attr('checked', true);
                }    
                if(item.G2866_C55490 == 1){
                    $("#G2866_C55490").attr('checked', true);
                }    
                if(item.G2866_C55491 == 1){
                    $("#G2866_C55491").attr('checked', true);
                }    
                if(item.G2866_C55492 == 1){
                    $("#G2866_C55492").attr('checked', true);
                }    
                if(item.G2866_C55538 == 1){
                    $("#G2866_C55538").attr('checked', true);
                }  
                $("#G2866_C55537").val(item.G2866_C55537).trigger("change");  
                $("#G2866_C55484").val(item.G2866_C55484).trigger("change");    
                if(item.G2866_C55494 == 1){
                    $("#G2866_C55494").attr('checked', true);
                }  
                $("#G2866_C55493").attr("opt",item.G2866_C55493);  
                $("#G2866_C55486").attr("opt",item.G2866_C55486);  
                $("#G2866_C55488").val(item.G2866_C55488); 
                $("#G2866_C55487").val(item.G2866_C55487);   
                if(item.G2866_C55496 == 1){
                    $("#G2866_C55496").attr('checked', true);
                }  
                $("#G2866_C56049").val(item.G2866_C56049); 
                $("#G2866_C55497").val(item.G2866_C55497).trigger("change");    
                if(item.G2866_C55498 == 1){
                    $("#G2866_C55498").attr('checked', true);
                }    
                if(item.G2866_C55499 == 1){
                    $("#G2866_C55499").attr('checked', true);
                }  
                $("#G2866_C61253").val(item.G2866_C61253).trigger("change");  
                $("#G2866_C61254").val(item.G2866_C61254).trigger("change");  
                $("#G2866_C61255").val(item.G2866_C61255); 
                $("#G2866_C61257").val(item.G2866_C61257).trigger("change");  
                $("#G2866_C63125").val(item.G2866_C63125).trigger("change");  
                $("#G2866_C63126").val(item.G2866_C63126).trigger("change");  
                $("#G2866_C63127").val(item.G2866_C63127); 
                $("#G2866_C63128").val(item.G2866_C63128);   
                if(item.G2866_C63129 == 1){
                    $("#G2866_C63129").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2866_C55476").select2();

    $("#G2866_C55475").select2();

    $("#G2866_C55463").select2();

    $("#G2866_C55464").select2();

    $("#G2866_C55466").select2();

    $("#G2866_C55467").select2();

    $("#G2866_C55468").select2();

    $("#G2866_C55537").select2();

    $("#G2866_C55484").select2();

    $("#G2866_C55493").select2();

    $("#G2866_C55486").select2();

    $("#G2866_C55469").select2();

    $("#G2866_C55473").select2();

    $("#G2866_C55474").select2();

    $("#G2866_C55497").select2();

    $("#G2866_C61253").select2();

    $("#G2866_C61254").select2();

    $("#G2866_C61257").select2();

    $("#G2866_C63125").select2();

    $("#G2866_C63126").select2();
        //datepickers
        

        $("#G2866_C55431").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2866_C61255").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2866_C55432").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G2866_C55483").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Vamos a iniciar una actualización de datos y encuesta, prometo no quitarle mucho tiempo, de acuerdo? 

    $("#G2866_C55476").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Aprovecho para recordarle lo importante que es usted para nuestra empresa, es un buen momento para que podamos hablar? 

    $("#G2866_C55475").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Califique de 1 a 5 el servicio de La Aurora 

    $("#G2866_C55463").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Recomendaria La Aurora 

    $("#G2866_C55464").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Malas referencias 

    $("#G2866_C55466").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3372' , idPadre : $(this).val() },
            success : function(data){
                var optG2866_C55467 = $("#G2866_C55467").attr("opt");
                $("#G2866_C55467").html(data);
                if (optG2866_C55467 != null) {
                    $("#G2866_C55467").val(optG2866_C55467).trigger("change");
                }
            }
        });
        
    });

    //function para ¿Quien dio las malas referencias? 

    $("#G2866_C55467").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Tiene whatsapp? 

    $("#G2866_C55468").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Referido 

    $("#G2866_C55537").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Cliente al día? 

    $("#G2866_C55484").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3377' , idPadre : $(this).val() },
            success : function(data){
                var optG2866_C55486 = $("#G2866_C55486").attr("opt");
                $("#G2866_C55486").html(data);
                if (optG2866_C55486 != null) {
                    $("#G2866_C55486").val(optG2866_C55486).trigger("change");
                }
            }
        });
        
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3378' , idPadre : $(this).val() },
            success : function(data){
                var optG2866_C55493 = $("#G2866_C55493").attr("opt");
                $("#G2866_C55493").html(data);
                if (optG2866_C55493 != null) {
                    $("#G2866_C55493").val(optG2866_C55493).trigger("change");
                }
            }
        });
        
    });

    //function para ¿Realiza acuerdo de pago? 

    $("#G2866_C55493").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Solicitud de referidos 

    $("#G2866_C55486").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Tiene mascota? 

    $("#G2866_C55469").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3375' , idPadre : $(this).val() },
            success : function(data){
                var optG2866_C55473 = $("#G2866_C55473").attr("opt");
                $("#G2866_C55473").html(data);
                if (optG2866_C55473 != null) {
                    $("#G2866_C55473").val(optG2866_C55473).trigger("change");
                }
            }
        });
        
    });

    //function para ¿le interesa la previsión exequial para su mascota? 

    $("#G2866_C55473").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3373' , idPadre : $(this).val() },
            success : function(data){
                var optG2866_C55474 = $("#G2866_C55474").attr("opt");
                $("#G2866_C55474").html(data);
                if (optG2866_C55474 != null) {
                    $("#G2866_C55474").val(optG2866_C55474).trigger("change");
                }
            }
        });
        
    });

    //function para venta efectiva? 

    $("#G2866_C55474").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿esta interesado en recibi información del centro de duelo? 

    $("#G2866_C55497").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Nombre de la empresa que realizo la venta 

    $("#G2866_C61253").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Nombre de la empresa para la que se realiza la venta 

    $("#G2866_C61254").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo de campaña 

    $("#G2866_C61257").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Ha recibido malas referencias? 

    $("#G2866_C63125").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para De quien ha sido las malas referencias? 

    $("#G2866_C63126").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2866_C55449").val(item.G2866_C55449);
 
                                                $("#G2866_C55450").val(item.G2866_C55450);
 
                                                $("#G2866_C55451").val(item.G2866_C55451);
 
                                                $("#G2866_C55452").val(item.G2866_C55452);
 
                                                $("#G2866_C55440").val(item.G2866_C55440);
 
                                                $("#G2866_C55441").val(item.G2866_C55441);
 
                                                $("#G2866_C55442").val(item.G2866_C55442);
 
                                                $("#G2866_C55444").val(item.G2866_C55444);
 
                                                $("#G2866_C55459").val(item.G2866_C55459);
 
                                                $("#G2866_C55460").val(item.G2866_C55460);
 
                                                $("#G2866_C55461").val(item.G2866_C55461);
 
                                                $("#G2866_C55462").val(item.G2866_C55462);
 
                                                $("#G2866_C60386").val(item.G2866_C60386);
 
                    $("#G2866_C55429").val(item.G2866_C55429).trigger("change"); 
 
                    $("#G2866_C55430").val(item.G2866_C55430).trigger("change"); 
 
                                                $("#G2866_C55431").val(item.G2866_C55431);
 
                                                $("#G2866_C55432").val(item.G2866_C55432);
 
                                                $("#G2866_C55433").val(item.G2866_C55433);
 
                                                $("#G2866_C55434").val(item.G2866_C55434);
 
                                                $("#G2866_C55435").val(item.G2866_C55435);
 
                                                $("#G2866_C55436").val(item.G2866_C55436);
 
                                                $("#G2866_C55437").val(item.G2866_C55437);
      
                                                if(item.G2866_C55438 == 1){
                                                   $("#G2866_C55438").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55439 == 1){
                                                   $("#G2866_C55439").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C56042 == 1){
                                                   $("#G2866_C56042").attr('checked', true);
                                                } 
 
                    $("#G2866_C55476").val(item.G2866_C55476).trigger("change"); 
 
                    $("#G2866_C55475").val(item.G2866_C55475).trigger("change"); 
      
                                                if(item.G2866_C55477 == 1){
                                                   $("#G2866_C55477").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55478 == 1){
                                                   $("#G2866_C55478").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55479 == 1){
                                                   $("#G2866_C55479").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55480 == 1){
                                                   $("#G2866_C55480").attr('checked', true);
                                                } 
 
                    $("#G2866_C55463").val(item.G2866_C55463).trigger("change"); 
 
                    $("#G2866_C55464").val(item.G2866_C55464).trigger("change"); 
 
                                                $("#G2866_C55465").val(item.G2866_C55465);
 
                    $("#G2866_C55466").val(item.G2866_C55466).trigger("change"); 
 
                    $("#G2866_C55467").attr("opt",item.G2866_C55467); 
 
                                                $("#G2866_C55482").val(item.G2866_C55482);
 
                    $("#G2866_C55468").val(item.G2866_C55468).trigger("change"); 
 
                                                $("#G2866_C55483").val(item.G2866_C55483);
      
                                                if(item.G2866_C55495 == 1){
                                                   $("#G2866_C55495").attr('checked', true);
                                                } 
 
                    $("#G2866_C55469").val(item.G2866_C55469).trigger("change"); 
 
                    $("#G2866_C55473").attr("opt",item.G2866_C55473); 
 
                    $("#G2866_C55474").attr("opt",item.G2866_C55474); 
      
                                                if(item.G2866_C56043 == 1){
                                                   $("#G2866_C56043").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C56044 == 1){
                                                   $("#G2866_C56044").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C56045 == 1){
                                                   $("#G2866_C56045").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C56046 == 1){
                                                   $("#G2866_C56046").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C56047 == 1){
                                                   $("#G2866_C56047").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C56048 == 1){
                                                   $("#G2866_C56048").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55485 == 1){
                                                   $("#G2866_C55485").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55489 == 1){
                                                   $("#G2866_C55489").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55490 == 1){
                                                   $("#G2866_C55490").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55491 == 1){
                                                   $("#G2866_C55491").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55492 == 1){
                                                   $("#G2866_C55492").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55538 == 1){
                                                   $("#G2866_C55538").attr('checked', true);
                                                } 
 
                    $("#G2866_C55537").val(item.G2866_C55537).trigger("change"); 
 
                    $("#G2866_C55484").val(item.G2866_C55484).trigger("change"); 
      
                                                if(item.G2866_C55494 == 1){
                                                   $("#G2866_C55494").attr('checked', true);
                                                } 
 
                    $("#G2866_C55493").attr("opt",item.G2866_C55493); 
 
                    $("#G2866_C55486").attr("opt",item.G2866_C55486); 
 
                                                $("#G2866_C55488").val(item.G2866_C55488);
 
                                                $("#G2866_C55487").val(item.G2866_C55487);
      
                                                if(item.G2866_C55496 == 1){
                                                   $("#G2866_C55496").attr('checked', true);
                                                } 
 
                                                $("#G2866_C56049").val(item.G2866_C56049);
 
                    $("#G2866_C55497").val(item.G2866_C55497).trigger("change"); 
      
                                                if(item.G2866_C55498 == 1){
                                                   $("#G2866_C55498").attr('checked', true);
                                                } 
      
                                                if(item.G2866_C55499 == 1){
                                                   $("#G2866_C55499").attr('checked', true);
                                                } 
 
                    $("#G2866_C61253").val(item.G2866_C61253).trigger("change"); 
 
                    $("#G2866_C61254").val(item.G2866_C61254).trigger("change"); 
 
                                                $("#G2866_C61255").val(item.G2866_C61255);
 
                    $("#G2866_C61257").val(item.G2866_C61257).trigger("change"); 
 
                    $("#G2866_C63125").val(item.G2866_C63125).trigger("change"); 
 
                    $("#G2866_C63126").val(item.G2866_C63126).trigger("change"); 
 
                                                $("#G2866_C63127").val(item.G2866_C63127);
 
                                                $("#G2866_C63128").val(item.G2866_C63128);
      
                                                if(item.G2866_C63129 == 1){
                                                   $("#G2866_C63129").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','telefono personal','telefono residencial','telefono laboral','telefono alterno','NOMBRE TOMADOR','CEDULA TOMADOR','LOCALIDAD TOMADOR','DIRECCION TOMADOR','TELEFONO PERSONAL','TELEFONO RESIDENCIA','TELEFONO LABORAL','TELEFONO ALTERNO','FECHA ACTUALIZADA DE CARGUE','Agente','Fecha','Hora','Campaña','Vamos a iniciar una actualización de datos y encuesta, prometo no quitarle mucho tiempo, de acuerdo?','Aprovecho para recordarle lo importante que es usted para nuestra empresa, es un buen momento para que podamos hablar?','Califique de 1 a 5 el servicio de La Aurora','Recomendaria La Aurora','¿Porque la recomendaria?','Malas referencias','¿Quien dio las malas referencias?','¿Que malas referencias le dieron?','¿Tiene whatsapp?','¿Que número es?','¿Tiene mascota?','¿le interesa la previsión exequial para su mascota?','venta efectiva?','Referido','¿Cliente al día?','¿Realiza acuerdo de pago?','Solicitud de referidos','Telefono referido','Nombre referido','PARA EL ASESOR: Comentarle al cliente sobre las charlas mensuales por Facebook','¿esta interesado en recibi información del centro de duelo?','Nombre de la empresa que realizo la venta','Nombre de la empresa para la que se realiza la venta','Fecha de realización de llamada','Tipo de campaña','Ha recibido malas referencias?','De quien ha sido las malas referencias?','Nombre empresaria funeraria?','Cual fue la mala referencia?'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2866_C55449', 
                        index: 'G2866_C55449', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55450', 
                        index: 'G2866_C55450', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55451', 
                        index: 'G2866_C55451', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55452', 
                        index: 'G2866_C55452', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55440', 
                        index: 'G2866_C55440', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55441', 
                        index: 'G2866_C55441', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55442', 
                        index: 'G2866_C55442', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55444', 
                        index: 'G2866_C55444', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55459', 
                        index: 'G2866_C55459', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55460', 
                        index: 'G2866_C55460', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55461', 
                        index: 'G2866_C55461', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55462', 
                        index: 'G2866_C55462', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2866_C60386', 
                        index:'G2866_C60386', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55434', 
                        index: 'G2866_C55434', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55435', 
                        index: 'G2866_C55435', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55436', 
                        index: 'G2866_C55436', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55437', 
                        index: 'G2866_C55437', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55476', 
                        index:'G2866_C55476', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3373&campo=G2866_C55476'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55475', 
                        index:'G2866_C55475', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3373&campo=G2866_C55475'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55463', 
                        index:'G2866_C55463', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3370&campo=G2866_C55463'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55464', 
                        index:'G2866_C55464', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3371&campo=G2866_C55464'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55465', 
                        index: 'G2866_C55465', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55466', 
                        index:'G2866_C55466', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1347&campo=G2866_C55466'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55467', 
                        index:'G2866_C55467', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3372&campo=G2866_C55467'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55482', 
                        index: 'G2866_C55482', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55468', 
                        index:'G2866_C55468', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3373&campo=G2866_C55468'
                        }
                    }
 
                    ,
                    {  
                        name:'G2866_C55483', 
                        index:'G2866_C55483', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2866_C55469', 
                        index:'G2866_C55469', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3382&campo=G2866_C55469'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55473', 
                        index:'G2866_C55473', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3375&campo=G2866_C55473'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55474', 
                        index:'G2866_C55474', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3373&campo=G2866_C55474'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55537', 
                        index:'G2866_C55537', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3383&campo=G2866_C55537'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55484', 
                        index:'G2866_C55484', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3373&campo=G2866_C55484'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55493', 
                        index:'G2866_C55493', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3378&campo=G2866_C55493'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55486', 
                        index:'G2866_C55486', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3377&campo=G2866_C55486'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C55488', 
                        index: 'G2866_C55488', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55487', 
                        index: 'G2866_C55487', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C56049', 
                        index: 'G2866_C56049', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C55497', 
                        index:'G2866_C55497', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3379&campo=G2866_C55497'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C61253', 
                        index:'G2866_C61253', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3699&campo=G2866_C61253'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C61254', 
                        index:'G2866_C61254', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3699&campo=G2866_C61254'
                        }
                    }

                    ,
                    {  
                        name:'G2866_C61255', 
                        index:'G2866_C61255', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2866_C61257', 
                        index:'G2866_C61257', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3698&campo=G2866_C61257'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C63125', 
                        index:'G2866_C63125', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3373&campo=G2866_C63125'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C63126', 
                        index:'G2866_C63126', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3372&campo=G2866_C63126'
                        }
                    }

                    ,
                    { 
                        name:'G2866_C63127', 
                        index: 'G2866_C63127', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2866_C63128', 
                        index: 'G2866_C63128', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2866_C55440',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2866_C55449").val(item.G2866_C55449);

                        $("#G2866_C55450").val(item.G2866_C55450);

                        $("#G2866_C55451").val(item.G2866_C55451);

                        $("#G2866_C55452").val(item.G2866_C55452);

                        $("#G2866_C55440").val(item.G2866_C55440);

                        $("#G2866_C55441").val(item.G2866_C55441);

                        $("#G2866_C55442").val(item.G2866_C55442);

                        $("#G2866_C55444").val(item.G2866_C55444);

                        $("#G2866_C55459").val(item.G2866_C55459);

                        $("#G2866_C55460").val(item.G2866_C55460);

                        $("#G2866_C55461").val(item.G2866_C55461);

                        $("#G2866_C55462").val(item.G2866_C55462);

                        $("#G2866_C60386").val(item.G2866_C60386);
 
                    $("#G2866_C55429").val(item.G2866_C55429).trigger("change"); 
 
                    $("#G2866_C55430").val(item.G2866_C55430).trigger("change"); 

                        $("#G2866_C55431").val(item.G2866_C55431);

                        $("#G2866_C55432").val(item.G2866_C55432);

                        $("#G2866_C55433").val(item.G2866_C55433);

                        $("#G2866_C55434").val(item.G2866_C55434);

                        $("#G2866_C55435").val(item.G2866_C55435);

                        $("#G2866_C55436").val(item.G2866_C55436);

                        $("#G2866_C55437").val(item.G2866_C55437);
    
                        if(item.G2866_C55438 == 1){
                           $("#G2866_C55438").attr('checked', true);
                        } 
    
                        if(item.G2866_C55439 == 1){
                           $("#G2866_C55439").attr('checked', true);
                        } 
    
                        if(item.G2866_C56042 == 1){
                           $("#G2866_C56042").attr('checked', true);
                        } 
 
                    $("#G2866_C55476").val(item.G2866_C55476).trigger("change"); 
 
                    $("#G2866_C55475").val(item.G2866_C55475).trigger("change"); 
    
                        if(item.G2866_C55477 == 1){
                           $("#G2866_C55477").attr('checked', true);
                        } 
    
                        if(item.G2866_C55478 == 1){
                           $("#G2866_C55478").attr('checked', true);
                        } 
    
                        if(item.G2866_C55479 == 1){
                           $("#G2866_C55479").attr('checked', true);
                        } 
    
                        if(item.G2866_C55480 == 1){
                           $("#G2866_C55480").attr('checked', true);
                        } 
 
                    $("#G2866_C55463").val(item.G2866_C55463).trigger("change"); 
 
                    $("#G2866_C55464").val(item.G2866_C55464).trigger("change"); 

                        $("#G2866_C55465").val(item.G2866_C55465);
 
                    $("#G2866_C55466").val(item.G2866_C55466).trigger("change"); 
 
                    $("#G2866_C55467").attr("opt",item.G2866_C55467); 

                        $("#G2866_C55482").val(item.G2866_C55482);
 
                    $("#G2866_C55468").val(item.G2866_C55468).trigger("change"); 

                        $("#G2866_C55483").val(item.G2866_C55483);
    
                        if(item.G2866_C55495 == 1){
                           $("#G2866_C55495").attr('checked', true);
                        } 
 
                    $("#G2866_C55469").val(item.G2866_C55469).trigger("change"); 
 
                    $("#G2866_C55473").attr("opt",item.G2866_C55473); 
 
                    $("#G2866_C55474").attr("opt",item.G2866_C55474); 
    
                        if(item.G2866_C56043 == 1){
                           $("#G2866_C56043").attr('checked', true);
                        } 
    
                        if(item.G2866_C56044 == 1){
                           $("#G2866_C56044").attr('checked', true);
                        } 
    
                        if(item.G2866_C56045 == 1){
                           $("#G2866_C56045").attr('checked', true);
                        } 
    
                        if(item.G2866_C56046 == 1){
                           $("#G2866_C56046").attr('checked', true);
                        } 
    
                        if(item.G2866_C56047 == 1){
                           $("#G2866_C56047").attr('checked', true);
                        } 
    
                        if(item.G2866_C56048 == 1){
                           $("#G2866_C56048").attr('checked', true);
                        } 
    
                        if(item.G2866_C55485 == 1){
                           $("#G2866_C55485").attr('checked', true);
                        } 
    
                        if(item.G2866_C55489 == 1){
                           $("#G2866_C55489").attr('checked', true);
                        } 
    
                        if(item.G2866_C55490 == 1){
                           $("#G2866_C55490").attr('checked', true);
                        } 
    
                        if(item.G2866_C55491 == 1){
                           $("#G2866_C55491").attr('checked', true);
                        } 
    
                        if(item.G2866_C55492 == 1){
                           $("#G2866_C55492").attr('checked', true);
                        } 
    
                        if(item.G2866_C55538 == 1){
                           $("#G2866_C55538").attr('checked', true);
                        } 
 
                    $("#G2866_C55537").val(item.G2866_C55537).trigger("change"); 
 
                    $("#G2866_C55484").val(item.G2866_C55484).trigger("change"); 
    
                        if(item.G2866_C55494 == 1){
                           $("#G2866_C55494").attr('checked', true);
                        } 
 
                    $("#G2866_C55493").attr("opt",item.G2866_C55493); 
 
                    $("#G2866_C55486").attr("opt",item.G2866_C55486); 

                        $("#G2866_C55488").val(item.G2866_C55488);

                        $("#G2866_C55487").val(item.G2866_C55487);
    
                        if(item.G2866_C55496 == 1){
                           $("#G2866_C55496").attr('checked', true);
                        } 

                        $("#G2866_C56049").val(item.G2866_C56049);
 
                    $("#G2866_C55497").val(item.G2866_C55497).trigger("change"); 
    
                        if(item.G2866_C55498 == 1){
                           $("#G2866_C55498").attr('checked', true);
                        } 
    
                        if(item.G2866_C55499 == 1){
                           $("#G2866_C55499").attr('checked', true);
                        } 
 
                    $("#G2866_C61253").val(item.G2866_C61253).trigger("change"); 
 
                    $("#G2866_C61254").val(item.G2866_C61254).trigger("change"); 

                        $("#G2866_C61255").val(item.G2866_C61255);
 
                    $("#G2866_C61257").val(item.G2866_C61257).trigger("change"); 
 
                    $("#G2866_C63125").val(item.G2866_C63125).trigger("change"); 
 
                    $("#G2866_C63126").val(item.G2866_C63126).trigger("change"); 

                        $("#G2866_C63127").val(item.G2866_C63127);

                        $("#G2866_C63128").val(item.G2866_C63128);
    
                        if(item.G2866_C63129 == 1){
                           $("#G2866_C63129").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
