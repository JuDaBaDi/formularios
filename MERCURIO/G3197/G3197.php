
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3197/G3197_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : $_SESSION["IDENTIFICACION"];
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3197_ConsInte__b as id, G3197_C64757 as camp1 , G3197_C64757 as camp2 FROM ".$BaseDatos.".G3197  WHERE G3197_Usuario = ".$idUsuario." ORDER BY G3197_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3197_ConsInte__b as id, G3197_C64757 as camp1 , G3197_C64757 as camp2 FROM ".$BaseDatos.".G3197  ORDER BY G3197_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3197_ConsInte__b as id, G3197_C64757 as camp1 , G3197_C64757 as camp2 FROM ".$BaseDatos.".G3197 JOIN ".$BaseDatos.".G3197_M".$resultEstpas->muestr." ON G3197_ConsInte__b = G3197_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3197_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3197_ConsInte__b as id, G3197_C64757 as camp1 , G3197_C64757 as camp2 FROM ".$BaseDatos.".G3197 JOIN ".$BaseDatos.".G3197_M".$resultEstpas->muestr." ON G3197_ConsInte__b = G3197_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3197_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3197_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3197_ConsInte__b as id, G3197_C64757 as camp1 , G3197_C64757 as camp2 FROM ".$BaseDatos.".G3197  ORDER BY G3197_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="9866" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9866c">
                DATOS AUTOMATICOS DEL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9866c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3197_C64757" id="LblG3197_C64757">Número caso</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"]) && isset($_GET["view"])){  if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 64757")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 64757");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }
                             }?>" readonly name="G3197_C64757" id="G3197_C64757" placeholder="Número caso">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3197_C64758" id="LblG3197_C64758">Fecha creación</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G3197_C64758" id="G3197_C64758" placeholder="YYYY-MM-DD" nombre="Fecha creación">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3197_C64759" id="LblG3197_C64759">Hora creación</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php echo date('H:i:s');?>" readonly name="G3197_C64759" id="G3197_C64759" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3197_C64759">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3197_C64760" id="LblG3197_C64760">Fecha cierre</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3197_C64760'])) {
                            echo $_GET['G3197_C64760'];
                        } ?>" readonly name="G3197_C64760" id="G3197_C64760" placeholder="YYYY-MM-DD" nombre="Fecha cierre">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3197_C64761" id="LblG3197_C64761">Hora cierre</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3197_C64761'])) {
                            echo $_GET['G3197_C64761'];
                        } ?>" readonly name="G3197_C64761" id="G3197_C64761" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3197_C64761">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3197_C64762" id="LblG3197_C64762">Fecha última gestión</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G3197_C64762" id="G3197_C64762" placeholder="YYYY-MM-DD" nombre="Fecha última gestión">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3197_C64763" id="LblG3197_C64763">Hora última gestión</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php echo date('H:i:s');?>" readonly name="G3197_C64763" id="G3197_C64763" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3197_C64763">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3197_C64764" id="LblG3197_C64764">Campaña</label>
                        <input type="text" class="form-control input-sm Numerico" value="" readonly name="G3197_C64764" id="G3197_C64764" placeholder="Campaña">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4" style="display:none;">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65476" id="LblG3197_C65476">Agente</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65476" value="<?php if (isset($_GET['G3197_C65476'])) {
                            echo $_GET['G3197_C65476'];
                        } ?>"  name="G3197_C65476"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9867" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9867c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9867c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64754" id="LblG3197_C64754">ORIGEN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64754" value="<?php if (isset($_GET['G3197_C64754'])) {
                            echo $_GET['G3197_C64754'];
                        } ?>" readonly name="G3197_C64754"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64755" id="LblG3197_C64755">OPTIN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64755" value="<?php if (isset($_GET['G3197_C64755'])) {
                            echo $_GET['G3197_C64755'];
                        } ?>" readonly name="G3197_C64755"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3197_C64756" id="LblG3197_C64756">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3197_C64756" id="G3197_C64756">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3968 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9868" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9868c">
                DATOS A DILIGENCIAR PARA CREAR EL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9868c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3197_C64788" id="LblG3197_C64788">Estructura comercial</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3197_C64788" id="G3197_C64788">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64789" id="LblG3197_C64789">Código del responsable que reportó el caso</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64789" value="<?php if (isset($_GET['G3197_C64789'])) {
                            echo $_GET['G3197_C64789'];
                        } ?>" readonly name="G3197_C64789"  placeholder="Código del responsable que reportó el caso"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64768" id="LblG3197_C64768">Código gerencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64768" value="<?php if (isset($_GET['G3197_C64768'])) {
                            echo $_GET['G3197_C64768'];
                        } ?>" readonly name="G3197_C64768"  placeholder="Código gerencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65434" id="LblG3197_C65434">Gerencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65434" value="<?php if (isset($_GET['G3197_C65434'])) {
                            echo $_GET['G3197_C65434'];
                        } ?>"  name="G3197_C65434"  placeholder="Gerencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64766" id="LblG3197_C64766">Código de sector</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64766" value="<?php if (isset($_GET['G3197_C64766'])) {
                            echo $_GET['G3197_C64766'];
                        } ?>" readonly name="G3197_C64766"  placeholder="Código de sector"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64767" id="LblG3197_C64767">Sector</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64767" value="<?php if (isset($_GET['G3197_C64767'])) {
                            echo $_GET['G3197_C64767'];
                        } ?>" readonly name="G3197_C64767"  placeholder="Sector"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64770" id="LblG3197_C64770">Nombre Responsable</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64770" value="<?php if (isset($_GET['G3197_C64770'])) {
                            echo $_GET['G3197_C64770'];
                        } ?>" readonly name="G3197_C64770"  placeholder="Nombre Responsable"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64771" id="LblG3197_C64771">Correo corporativo</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64771" value="<?php if (isset($_GET['G3197_C64771'])) {
                            echo $_GET['G3197_C64771'];
                        } ?>" readonly name="G3197_C64771"  placeholder="Correo corporativo"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3197_C65449" id="LblG3197_C65449">Ciudad</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3197_C65449" id="G3197_C65449">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65435" id="LblG3197_C65435">Departamento</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65435" value="<?php if (isset($_GET['G3197_C65435'])) {
                            echo $_GET['G3197_C65435'];
                        } ?>"  name="G3197_C65435"  placeholder="Departamento"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64772" id="LblG3197_C64772">Teléfono</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64772" value="<?php if (isset($_GET['G3197_C64772'])) {
                            echo $_GET['G3197_C64772'];
                        } ?>" readonly name="G3197_C64772"  placeholder="Teléfono"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3197_C65450" id="LblG3197_C65450">Código consultor</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3197_C65450" id="G3197_C65450">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65442" id="LblG3197_C65442">Cédula consultor</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65442" value="<?php if (isset($_GET['G3197_C65442'])) {
                            echo $_GET['G3197_C65442'];
                        } ?>"  name="G3197_C65442"  placeholder="Cédula consultor"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65443" id="LblG3197_C65443">Nombre consultor</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65443" value="<?php if (isset($_GET['G3197_C65443'])) {
                            echo $_GET['G3197_C65443'];
                        } ?>"  name="G3197_C65443"  placeholder="Nombre consultor"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3197_C64775" id="LblG3197_C64775">Tipificación</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3197_C64775" id="G3197_C64775">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3197_C64776" id="LblG3197_C64776">Subtipificación</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3197_C64776" id="G3197_C64776">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3971 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64774" id="LblG3197_C64774">Código</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64774" value="<?php if (isset($_GET['G3197_C64774'])) {
                            echo $_GET['G3197_C64774'];
                        } ?>"  name="G3197_C64774"  placeholder="Código"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3197_C64773" id="LblG3197_C64773">Clasificación</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3197_C64773" id="G3197_C64773">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3969 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3197_C64777" id="LblG3197_C64777">Tipo escalamiento</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3197_C64777" id="G3197_C64777">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3972 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65444" id="LblG3197_C65444">SLA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65444" value="<?php if (isset($_GET['G3197_C65444'])) {
                            echo $_GET['G3197_C65444'];
                        } ?>"  name="G3197_C65444"  placeholder="SLA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65445" id="LblG3197_C65445">Canal</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65445" value="<?php if (isset($_GET['G3197_C65445'])) {
                            echo $_GET['G3197_C65445'];
                        } ?>"  name="G3197_C65445"  placeholder="Canal"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65446" id="LblG3197_C65446">Numero de pedido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65446" value="<?php if (isset($_GET['G3197_C65446'])) {
                            echo $_GET['G3197_C65446'];
                        } ?>"  name="G3197_C65446"  placeholder="Numero de pedido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64769" id="LblG3197_C64769">Gerencia de ventas</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64769" value="<?php if (isset($_GET['G3197_C64769'])) {
                            echo $_GET['G3197_C64769'];
                        } ?>" readonly name="G3197_C64769"  placeholder="Gerencia de ventas"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3197_C64779" id="LblG3197_C64779">Observaciones</label>
                        <textarea class="form-control input-sm" name="G3197_C64779" id="G3197_C64779"  value="<?php if (isset($_GET['G3197_C64779'])) {
                            echo $_GET['G3197_C64779'];
                        } ?>" placeholder="Observaciones"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C64778" id="LblG3197_C64778">Escalado a</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C64778" value="<?php if (isset($_GET['G3197_C64778'])) {
                            echo $_GET['G3197_C64778'];
                        } ?>"  name="G3197_C64778"  placeholder="Escalado a"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65447" id="LblG3197_C65447">Tiempo transcurrido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65447" value="<?php if (isset($_GET['G3197_C65447'])) {
                            echo $_GET['G3197_C65447'];
                        } ?>"  name="G3197_C65447"  placeholder="Tiempo transcurrido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3197_C65448" id="LblG3197_C65448">Perfil de la Segmentacion Actual</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3197_C65448" value="<?php if (isset($_GET['G3197_C65448'])) {
                            echo $_GET['G3197_C65448'];
                        } ?>"  name="G3197_C65448"  placeholder="Perfil de la Segmentacion Actual"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9870" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9870c">
                DATOS GESTION DEL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9870c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3197_C64785" id="LblG3197_C64785">Estado del caso</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3197_C64785" id="G3197_C64785">
                            <option value="44952" selected >En progreso</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3599 AND LISOPC_ConsInte__b NOT IN (44526,44952) ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

<div class=row>
                        <div class="col-md-12 col-xs-12">
                            
<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">Gestiones</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear Gestiones" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
                        </div>
                    </div>
                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G3197/G3197_eventos.js"></script>
<script type="text/javascript" src="formularios/G3197/G3197_extender_funcionalidad.php"></script><?php require_once "G3197_extender_funcionalidad.php";?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3197_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3197_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3197_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3197_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3197_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3197_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3197_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3197_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3197_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            //JDBD - Creamos un nuevo id incrementable y lo asignamos a este campo
            $.ajax({
                url:'<?=$url_crud;?>',
                type:'POST',
                data:{INCTB:"si"},
                success:function(data){
                    $("#G3197_C64757").val(data);
                }
            });
            //JDBD - Damos el valor fecha actual.
            $("#G3197_C64758").val("<?=date("Y-m-d");?>");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3197_C64759").val("<?php echo date('H:i:s');?>");
            //JDBD - Damos el valor fecha actual.
            $("#G3197_C64762").val("<?=date("Y-m-d");?>");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3197_C64763").val("<?php echo date('H:i:s');?>");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3197_C64764").val("");
            $("#G3197_C64775").val("0").trigger("change");
            $("#G3197_C64776").val("0").trigger("change");
            $("#G3197_C64773").val("0").trigger("change");
            $("#G3197_C64777").val("0").trigger("change");
            $("#G3197_C64785").val("0").trigger("change");
            
            
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G3197_C64757").val(item.G3197_C64757); 
                $("#G3197_C64758").val(item.G3197_C64758); 
                $("#G3197_C64759").val(item.G3197_C64759); 
                $("#G3197_C64760").val(item.G3197_C64760); 
                $("#G3197_C64761").val(item.G3197_C64761); 
                $("#G3197_C64762").val(item.G3197_C64762); 
                $("#G3197_C64763").val(item.G3197_C64763); 
                $("#G3197_C64764").val(item.G3197_C64764); 
                $("#G3197_C65476").val(item.G3197_C65476); 
                $("#G3197_C64754").val(item.G3197_C64754); 
                $("#G3197_C64755").val(item.G3197_C64755); 
                $("#G3197_C64756").val(item.G3197_C64756).trigger("change");  
                $("#G3197_C64788").attr("opt",item.G3197_C64788);
                $("#G3197_C64788").val(item.G3197_C64788).trigger("change"); 
                $("#G3197_C64789").val(item.G3197_C64789); 
                $("#G3197_C64768").val(item.G3197_C64768); 
                $("#G3197_C65434").val(item.G3197_C65434); 
                $("#G3197_C64766").val(item.G3197_C64766); 
                $("#G3197_C64767").val(item.G3197_C64767); 
                $("#G3197_C64770").val(item.G3197_C64770); 
                $("#G3197_C64771").val(item.G3197_C64771); 
                $("#G3197_C65449").attr("opt",item.G3197_C65449);
                $("#G3197_C65449").val(item.G3197_C65449).trigger("change"); 
                $("#G3197_C65435").val(item.G3197_C65435); 
                $("#G3197_C64772").val(item.G3197_C64772); 
                $("#G3197_C65450").attr("opt",item.G3197_C65450);
                $("#G3197_C65450").val(item.G3197_C65450).trigger("change"); 
                $("#G3197_C65442").val(item.G3197_C65442); 
                $("#G3197_C65443").val(item.G3197_C65443); 
                $("#G3197_C64775").attr("opt",item.G3197_C64775);  
                $("#G3197_C64776").attr("opt",item.G3197_C64776);  
                $("#G3197_C64774").val(item.G3197_C64774); 
                $("#G3197_C64773").val(item.G3197_C64773).trigger("change");  
                $("#G3197_C64777").attr("opt",item.G3197_C64777);  
                $("#G3197_C65444").val(item.G3197_C65444); 
                $("#G3197_C65445").val(item.G3197_C65445); 
                $("#G3197_C65446").val(item.G3197_C65446); 
                $("#G3197_C64769").val(item.G3197_C64769); 
                $("#G3197_C64779").val(item.G3197_C64779); 
                $("#G3197_C64778").val(item.G3197_C64778); 
                $("#G3197_C65447").val(item.G3197_C65447); 
                $("#G3197_C65448").val(item.G3197_C65448); 
                $("#G3197_C64785").val(item.G3197_C64785).trigger("change"); 
                
                cargarHijos_0(
        $("#G3197_C64757").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G3197_C64757").val());
            var id_0 = $("#G3197_C64757").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G3197_C64757").val());
            var id_0 = $("#G3197_C64757").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G3197_C64757").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&formaDetalle=si&formularioPadre=3197&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64784<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&formaDetalle=si&formularioPadre=3197&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64784&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=3197&pincheCampo=64784&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G3197_C64756").select2();
                            $("#G3197_C64788").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3197_C64788=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3197_C64788(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3197_C64788").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3197_C64788 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3197_C64788").html('<option value="'+data.G2968_ConsInte__b+'" >'+data.G2968_C59254+'</option>');
                                        
                                $("#G3197_C64769").val(data.G2968_C59249);
                                $("#G3197_C64767").val(data.G2968_C59251);
                                $("#G3197_C64770").val(data.G2968_C59254);
                                $("#G3197_C64771").val(data.G2968_C59255);
                                $("#G3197_C64772").val(data.G2968_C59257);
                                $("#G3197_C64768").val(data.G2968_C60138);
                                $("#G3197_C64766").val(data.G2968_C59250);
                                $("#G3197_C64789").val(data.G2968_C59253);
                                    }
                                });
                            });
                            $("#G3197_C65449").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3197_C65449=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3197_C65449(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3197_C65449").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3197_C65449 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3197_C65449").html('<option value="'+data.G3038_ConsInte__b+'" >'+data.G3038_C60938+'</option>');
                                        
                                $("#G3197_C65435").val(data.G3038_C60939);
                                    }
                                });
                            });
                            $("#G3197_C65450").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3197_C65450=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3197_C65450(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3197_C65450").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3197_C65450 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3197_C65450").html('<option value="'+data.G2969_ConsInte__b+'" >'+data.G2969_C59266+'</option>');
                                        
                                $("#G3197_C65442").val(data.G2969_C59268);
                                $("#G3197_C65443").val(data.G2969_C59267);
                                $("#G3197_C64767").val(data.G2969_C60143);
                                $("#G3197_C65434").val(data.G2969_C60144);
                                $("#G3197_C64770").val(data.G2969_C59340);
                                $("#G3197_C64771").val(data.G2969_C59342);
                                $("#G3197_C64772").val(data.G2969_C59270);
                                $("#G3197_C65449").val(data.G2969_C60940);
                                $("#G3197_C64767").val(data.G2969_C59339);
                                $("#G3197_C65434").val(data.G2969_C59341);
                                    }
                                });
                            });

    $("#G3197_C64775").select2();

    $("#G3197_C64776").select2();

    $("#G3197_C64773").select2();

    // $("#G3197_C64777").select2();

    // $("#G3197_C64785").select2();
        //datepickers
        

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G3197_C64757").numeric();
                
        $("#G3197_C64764").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G3197_C64756").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipificación 

    $("#G3197_C64775").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3971' , idPadre : $(this).val() },
            success : function(data){
                var optG3197_C64776 = $("#G3197_C64776").attr("opt");
                $("#G3197_C64776").html(data);
                if (optG3197_C64776 != null) {
                    $("#G3197_C64776").val(optG3197_C64776).trigger("change");
                }
            }
        });
        
    });

    //function para Subtipificación 

$("#G3197_C64776").change(function(){  

            var arrN2_t = ["47830","47843","47849","47850","47851","47853","47854","47857","47858","47859","47861","47870"];

            if (arrN2_t.includes($(this).val())) {

                $("#G3197_C64777").html('<option value="48443">N2</option>');
                $("#G3197_C64777").val("48443").trigger("change");

            }else {

                $("#G3197_C64777").html('<option value="48442">N1</option>');
                $("#G3197_C64777").val("48442").trigger("change");

            }

            switch ($(this).val()) {

                case '47826':
                    $("#G3197_C64774").val("250");
                    break;
                case '47827':
                    $("#G3197_C64774").val("251");
                    break;
                case '47828':
                    $("#G3197_C64774").val("252");
                    break;
                case '47829':
                    $("#G3197_C64774").val("253");
                    break;
                case '47830':
                    $("#G3197_C64774").val("254");
                    break;
                case '47831':
                    $("#G3197_C64774").val("255");
                    break;
                case '47832':
                    $("#G3197_C64774").val("56");
                    break;
                case '47833':
                    $("#G3197_C64774").val("57");
                    break;
                case '47834':
                    $("#G3197_C64774").val("58");
                    break;
                case '47835':
                    $("#G3197_C64774").val("59");
                    break;
                case '47836':
                    $("#G3197_C64774").val("60");
                    break;
                case '47837':
                    $("#G3197_C64774").val("61");
                    break;
                case '47838':
                    $("#G3197_C64774").val("62");
                    break;
                case '47839':
                    $("#G3197_C64774").val("63");
                    break;
                case '47840':
                    $("#G3197_C64774").val("264");
                    break;
                case '47841':
                    $("#G3197_C64774").val("265");
                    break;
                case '47842':
                    $("#G3197_C64774").val("266");
                    break;
                case '47843':
                    $("#G3197_C64774").val("267");
                    break;
                case '47844':
                    $("#G3197_C64774").val("268");
                    break;
                case '47845':
                    $("#G3197_C64774").val("269");
                    break;
                case '47846':
                    $("#G3197_C64774").val("270");
                    break;
                case '47847':
                    $("#G3197_C64774").val("271");
                    break;
                case '47848':
                    $("#G3197_C64774").val("272");
                    break;
                case '47849':
                    $("#G3197_C64774").val("273");
                    break;
                case '47850':
                    $("#G3197_C64774").val("274");
                    break;
                case '47851':
                    $("#G3197_C64774").val("275");
                    break;
                case '47852':
                    $("#G3197_C64774").val("276");
                    break;
                case '47853':
                    $("#G3197_C64774").val("277");
                    break;
                case '47854':
                    $("#G3197_C64774").val("278");
                    break;
                case '47855':
                    $("#G3197_C64774").val("279");
                    break;
                case '47856':
                    $("#G3197_C64774").val("280");
                    break;
                case '47857':
                    $("#G3197_C64774").val("281");
                    break;
                case '47858':
                    $("#G3197_C64774").val("282");
                    break;
                case '47859':
                    $("#G3197_C64774").val("283");
                    break;
                case '47860':
                    $("#G3197_C64774").val("284");
                    break;
                case '47861':
                    $("#G3197_C64774").val("285");
                    break;
                case '47862':
                    $("#G3197_C64774").val("286");
                    break;
                case '47863':
                    $("#G3197_C64774").val("287");
                    break;
                case '47864':
                    $("#G3197_C64774").val("288");
                    break;
                case '47865':
                    $("#G3197_C64774").val("289");
                    break;
                case '47866':
                    $("#G3197_C64774").val("290");
                    break;
                case '47867':
                    $("#G3197_C64774").val("291");
                    break;
                case '47868':
                    $("#G3197_C64774").val("292");
                    break;
                case '47869':
                    $("#G3197_C64774").val("293");
                    break;
                case '47870':
                    $("#G3197_C64774").val("294");
                    break;
                case '47871':
                    $("#G3197_C64774").val("295");
                    break;
                case '47872':
                    $("#G3197_C64774").val("296");
                    break;
                case '47873':
                    $("#G3197_C64774").val("297");
                    break;
                case '47874':
                    $("#G3197_C64774").val("298");
                    break;
                case '47875':
                    $("#G3197_C64774").val("299");
                    break;
                case '47876':
                    $("#G3197_C64774").val("300");
                    break;
                case '47877':
                    $("#G3197_C64774").val("301");
                    break;
                case '47878':
                    $("#G3197_C64774").val("302");
                    break;
                case '47879':
                    $("#G3197_C64774").val("303");
                    break;
                case '47880':
                    $("#G3197_C64774").val("304");
                    break;
                case '47881':
                    $("#G3197_C64774").val("305");
                    break;
                case '47882':
                    $("#G3197_C64774").val("306");
                    break;
                case '47883':
                    $("#G3197_C64774").val("307");
                    break;
                case '47884':
                    $("#G3197_C64774").val("308");
                    break;
                case '47885':
                    $("#G3197_C64774").val("309");
                    break;
                case '47886':
                    $("#G3197_C64774").val("310");
                    break;
                case '47887':
                    $("#G3197_C64774").val("311");
                    break;
                case '47888':
                    $("#G3197_C64774").val("312");
                    break;
                case '47889':
                    $("#G3197_C64774").val("313");
                    break;


            }

        
    });

    //function para Clasificación 

    $("#G3197_C64773").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3970' , idPadre : $(this).val() },
            success : function(data){
                var optG3197_C64775 = $("#G3197_C64775").attr("opt");
                $("#G3197_C64775").html(data);
                if (optG3197_C64775 != null) {
                    $("#G3197_C64775").val(optG3197_C64775).trigger("change");
                }
            }
        });
        
    });

    //function para Tipo escalamiento 

    $("#G3197_C64777").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Estado del caso 

    $("#G3197_C64785").change(function(){  

        if ($(this).val() == "44527") {

            var today = moment().format('YYYY-MM-DD');
            var todayH = moment().format('HH:mm:ss');

            if ($("#G3197_C64760").val().length == 0) {

                $("#G3197_C64760").val(today);
                
            }

            if ($("#G3197_C64761").val().length == 0) {

                $("#G3197_C64761").val(todayH);
                
            }

            
        }
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3197_C64757").val(item.G3197_C64757);
 
                                                $("#G3197_C64758").val(item.G3197_C64758);
 
                                                $("#G3197_C64759").val(item.G3197_C64759);
 
                                                $("#G3197_C64760").val(item.G3197_C64760);
 
                                                $("#G3197_C64761").val(item.G3197_C64761);
 
                                                $("#G3197_C64762").val(item.G3197_C64762);
 
                                                $("#G3197_C64763").val(item.G3197_C64763);
 
                                                $("#G3197_C64764").val(item.G3197_C64764);
 
                                                $("#G3197_C65476").val(item.G3197_C65476);
 
                                                $("#G3197_C64754").val(item.G3197_C64754);
 
                                                $("#G3197_C64755").val(item.G3197_C64755);
 
                    $("#G3197_C64756").val(item.G3197_C64756).trigger("change"); 
 
                    $("#G3197_C64788").attr("opt",item.G3197_C64788);
                    $("#G3197_C64788").val(item.G3197_C64788).trigger("change");
 
                                                $("#G3197_C64789").val(item.G3197_C64789);
 
                                                $("#G3197_C64768").val(item.G3197_C64768);
 
                                                $("#G3197_C65434").val(item.G3197_C65434);
 
                                                $("#G3197_C64766").val(item.G3197_C64766);
 
                                                $("#G3197_C64767").val(item.G3197_C64767);
 
                                                $("#G3197_C64770").val(item.G3197_C64770);
 
                                                $("#G3197_C64771").val(item.G3197_C64771);
 
                    $("#G3197_C65449").attr("opt",item.G3197_C65449);
                    $("#G3197_C65449").val(item.G3197_C65449).trigger("change");
 
                                                $("#G3197_C65435").val(item.G3197_C65435);
 
                                                $("#G3197_C64772").val(item.G3197_C64772);
 
                    $("#G3197_C65450").attr("opt",item.G3197_C65450);
                    $("#G3197_C65450").val(item.G3197_C65450).trigger("change");
 
                                                $("#G3197_C65442").val(item.G3197_C65442);
 
                                                $("#G3197_C65443").val(item.G3197_C65443);
 
                    $("#G3197_C64775").attr("opt",item.G3197_C64775); 
 
                    $("#G3197_C64776").attr("opt",item.G3197_C64776); 
 
                                                $("#G3197_C64774").val(item.G3197_C64774);
 
                    $("#G3197_C64773").val(item.G3197_C64773).trigger("change"); 
 
                    $("#G3197_C64777").attr("opt",item.G3197_C64777); 
 
                                                $("#G3197_C65444").val(item.G3197_C65444);
 
                                                $("#G3197_C65445").val(item.G3197_C65445);
 
                                                $("#G3197_C65446").val(item.G3197_C65446);
 
                                                $("#G3197_C64769").val(item.G3197_C64769);
 
                                                $("#G3197_C64779").val(item.G3197_C64779);
 
                                                $("#G3197_C64778").val(item.G3197_C64778);
 
                                                $("#G3197_C65447").val(item.G3197_C65447);
 
                                                $("#G3197_C65448").val(item.G3197_C65448);
 
                    $("#G3197_C64785").val(item.G3197_C64785).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            /*let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });

            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }*/

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Número caso','Fecha creación','Hora creación','Fecha cierre','Hora cierre','Fecha última gestión','Hora última gestión','Campaña','Agente','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','Estructura comercial','Código del responsable que reportó el caso','Código gerencia','Gerencia','Código de sector','Sector','Nombre Responsable','Correo corporativo','Ciudad','Departamento','Teléfono','Código consultor','Cédula consultor','Nombre consultor','Tipificación','Subtipificación','Código','Clasificación','Tipo escalamiento','SLA','Canal','Numero de pedido','Gerencia de ventas','Observaciones','Escalado a','Tiempo transcurrido','Perfil de la Segmentacion Actual','Estado del caso'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G3197_C64757', 
                        index:'G3197_C64757', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    {  
                        name:'G3197_C64758', 
                        index:'G3197_C64758', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3197_C64759', 
                        index:'G3197_C64759', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'Hora creación', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3197_C64760', 
                        index:'G3197_C64760', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3197_C64761', 
                        index:'G3197_C64761', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'Hora cierre', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3197_C64762', 
                        index:'G3197_C64762', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3197_C64763', 
                        index:'G3197_C64763', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'Hora última gestión', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G3197_C64764', 
                        index:'G3197_C64764', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3197_C65476', 
                        index: 'G3197_C65476', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64754', 
                        index: 'G3197_C64754', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64755', 
                        index: 'G3197_C64755', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64756', 
                        index:'G3197_C64756', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3968&campo=G3197_C64756'
                        }
                    }

                    ,
                    { 
                        name:'G3197_C64788', 
                        index:'G3197_C64788', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3197_C64788=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3197_C64789', 
                        index: 'G3197_C64789', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64768', 
                        index: 'G3197_C64768', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65434', 
                        index: 'G3197_C65434', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64766', 
                        index: 'G3197_C64766', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64767', 
                        index: 'G3197_C64767', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64770', 
                        index: 'G3197_C64770', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64771', 
                        index: 'G3197_C64771', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65449', 
                        index:'G3197_C65449', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3197_C65449=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3197_C65435', 
                        index: 'G3197_C65435', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64772', 
                        index: 'G3197_C64772', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65450', 
                        index:'G3197_C65450', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3197_C65450=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3197_C65442', 
                        index: 'G3197_C65442', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65443', 
                        index: 'G3197_C65443', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64775', 
                        index:'G3197_C64775', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3970&campo=G3197_C64775'
                        }
                    }

                    ,
                    { 
                        name:'G3197_C64776', 
                        index:'G3197_C64776', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3971&campo=G3197_C64776'
                        }
                    }

                    ,
                    { 
                        name:'G3197_C64774', 
                        index: 'G3197_C64774', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64773', 
                        index:'G3197_C64773', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3969&campo=G3197_C64773'
                        }
                    }

                    ,
                    { 
                        name:'G3197_C64777', 
                        index:'G3197_C64777', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3972&campo=G3197_C64777'
                        }
                    }

                    ,
                    { 
                        name:'G3197_C65444', 
                        index: 'G3197_C65444', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65445', 
                        index: 'G3197_C65445', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65446', 
                        index: 'G3197_C65446', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64769', 
                        index: 'G3197_C64769', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64779', 
                        index:'G3197_C64779', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64778', 
                        index: 'G3197_C64778', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65447', 
                        index: 'G3197_C65447', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C65448', 
                        index: 'G3197_C65448', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3197_C64785', 
                        index:'G3197_C64785', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3599&campo=G3197_C64785'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3197_C64757',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
            ,subGrid: true,
            subGridRowExpanded: function(subgrid_id, row_id) { 
                // we pass two parameters 
                // subgrid_id is a id of the div tag created whitin a table data 
                // the id of this elemenet is a combination of the "sg_" + id of the row 
                // the row_id is the id of the row 
                // If we wan to pass additinal parameters to the url we can use 
                // a method getRowData(row_id) - which returns associative array in type name-value 
                // here we can easy construct the flowing 
                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Comentario de la gestión','Responsable gestión','Fecha gestión','Hora gestión','Número de caso al que pertenece', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G3198_C64780', 
                                index:'G3198_C64780', 
                                width:150, 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G3198_C64781', 
                                index: 'G3198_C64781', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            {  
                                name:'G3198_C64782', 
                                index:'G3198_C64782', 
                                width:120 ,
                                editable: true ,
                                formatter: 'text', 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).datepicker({
                                            language: "es",
                                            autoclose: true,
                                            todayHighlight: true
                                        });
                                    },
                                    defaultValue: function(){
                                        var currentTime = new Date();
                                        var month = parseInt(currentTime.getMonth() + 1);
                                        month = month <= 9 ? "0"+month : month;
                                        var day = currentTime.getDate();
                                        day = day <= 9 ? "0"+day : day;
                                        var year = currentTime.getFullYear();
                                        return year+"-"+month + "-"+day;
                                    }
                                }
                            }

                            ,
                            {  
                                name:'G3198_C64783', 
                                index:'G3198_C64783', 
                                width:70 ,
                                editable: true ,
                                formatter: 'text', 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        //Timepicker
                                         var options = {  //hh:mm 24 hour format only, defaults to current time
                                            timeFormat: 'HH:mm:ss',
                                            interval: 5,
                                            minTime: '10',
                                            dynamic: false,
                                            dropdown: true,
                                            scrollbar: true
                                        }; 
                                        $(el).timepicker(options);


                                    }
                                }
                            }
 
                            ,
                            {  
                                name:'G3198_C64784', 
                                index:'G3198_C64784', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                }, 
                subGridRowColapsed: function(subgrid_id, row_id) { 
                    // this function is called before removing the data 
                    //var subgrid_table_id; 
                    //subgrid_table_id = subgrid_id+"_t"; 
                    //jQuery("#"+subgrid_table_id).remove(); 
                }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G3197_C64757").val(item.G3197_C64757);

                        $("#G3197_C64758").val(item.G3197_C64758);

                        $("#G3197_C64759").val(item.G3197_C64759);

                        $("#G3197_C64760").val(item.G3197_C64760);

                        $("#G3197_C64761").val(item.G3197_C64761);

                        $("#G3197_C64762").val(item.G3197_C64762);

                        $("#G3197_C64763").val(item.G3197_C64763);

                        $("#G3197_C64764").val(item.G3197_C64764);

                        $("#G3197_C65476").val(item.G3197_C65476);

                        $("#G3197_C64754").val(item.G3197_C64754);

                        $("#G3197_C64755").val(item.G3197_C64755);
 
                    $("#G3197_C64756").val(item.G3197_C64756).trigger("change"); 
 
                    $("#G3197_C64788").attr("opt",item.G3197_C64788);
                    $("#G3197_C64788").val(item.G3197_C64788).trigger("change");

                        $("#G3197_C64789").val(item.G3197_C64789);

                        $("#G3197_C64768").val(item.G3197_C64768);

                        $("#G3197_C65434").val(item.G3197_C65434);

                        $("#G3197_C64766").val(item.G3197_C64766);

                        $("#G3197_C64767").val(item.G3197_C64767);

                        $("#G3197_C64770").val(item.G3197_C64770);

                        $("#G3197_C64771").val(item.G3197_C64771);
 
                    $("#G3197_C65449").attr("opt",item.G3197_C65449);
                    $("#G3197_C65449").val(item.G3197_C65449).trigger("change");

                        $("#G3197_C65435").val(item.G3197_C65435);

                        $("#G3197_C64772").val(item.G3197_C64772);
 
                    $("#G3197_C65450").attr("opt",item.G3197_C65450);
                    $("#G3197_C65450").val(item.G3197_C65450).trigger("change");

                        $("#G3197_C65442").val(item.G3197_C65442);

                        $("#G3197_C65443").val(item.G3197_C65443);
 
                    $("#G3197_C64775").attr("opt",item.G3197_C64775); 
 
                    $("#G3197_C64776").attr("opt",item.G3197_C64776); 

                        $("#G3197_C64774").val(item.G3197_C64774);
 
                    $("#G3197_C64773").val(item.G3197_C64773).trigger("change"); 
 
                    $("#G3197_C64777").attr("opt",item.G3197_C64777); 

                        $("#G3197_C65444").val(item.G3197_C65444);

                        $("#G3197_C65445").val(item.G3197_C65445);

                        $("#G3197_C65446").val(item.G3197_C65446);

                        $("#G3197_C64769").val(item.G3197_C64769);

                        $("#G3197_C64779").val(item.G3197_C64779);

                        $("#G3197_C64778").val(item.G3197_C64778);

                        $("#G3197_C65447").val(item.G3197_C65447);

                        $("#G3197_C65448").val(item.G3197_C65448);
 
                    $("#G3197_C64785").val(item.G3197_C64785).trigger("change"); 
                        
            cargarHijos_0(
        $("#G3197_C64757").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Comentario de la gestión','Responsable gestión','Fecha gestión','Hora gestión','Número de caso al que pertenece', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {
                        name:'G3198_C64780', 
                        index:'G3198_C64780', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3198_C64781', 
                        index: 'G3198_C64781', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G3198_C64782', 
                        index:'G3198_C64782', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3198_C64783', 
                        index:'G3198_C64783', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = {  //hh:mm 24 hour format only, defaults to current time
                                    timeFormat: 'HH:mm:ss',
                                    interval: 5,
                                    minTime: '10',
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                }; 
                                $(el).timepicker(options);
                                $(".timepicker").css("z-index", 99999 );
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G3198_C64784', 
                        index:'G3198_C64784', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G3198_C64780',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Gestiones',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=64784&formularioPadre=3197<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G3197_C64757").val());
            var id_0 = $("#G3197_C64757").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G3197_C64757").val());
            var id_0 = $("#G3197_C64757").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
