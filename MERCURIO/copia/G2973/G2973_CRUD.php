<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2973_ConsInte__b, G2973_FechaInsercion , G2973_Usuario ,  G2973_CodigoMiembro  , G2973_PoblacionOrigen , G2973_EstadoDiligenciamiento ,  G2973_IdLlamada , G2973_C59322 as principal ,G2973_C59320,G2973_C59322,G2973_C59321,G2973_C59544,G2973_C59545,G2973_C59546,G2973_C59307,G2973_C59308,G2973_C59309,G2973_C59310,G2973_C59311,G2973_C59312,G2973_C59313,G2973_C59314,G2973_C59315,G2973_C59547,G2973_C59548,G2973_C59549,G2973_C59550,G2973_C59551,G2973_C59552 FROM '.$BaseDatos.'.G2973 WHERE G2973_ConsInte__b ='.$_POST['id'];
            

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2973_C59320'] = $key->G2973_C59320;

                $datos[$i]['G2973_C59322'] = $key->G2973_C59322;

                $datos[$i]['G2973_C59321'] = $key->G2973_C59321;

                $datos[$i]['G2973_C59544'] = $key->G2973_C59544;

                $datos[$i]['G2973_C59545'] = $key->G2973_C59545;

                $datos[$i]['G2973_C59546'] = $key->G2973_C59546;

                $datos[$i]['G2973_C59307'] = $key->G2973_C59307;

                $datos[$i]['G2973_C59308'] = $key->G2973_C59308;

                $datos[$i]['G2973_C59309'] = explode(' ', $key->G2973_C59309)[0];
  
                $hora = '';
                if(!is_null($key->G2973_C59310)){
                    $hora = explode(' ', $key->G2973_C59310)[1];
                }

                $datos[$i]['G2973_C59310'] = $hora;

                $datos[$i]['G2973_C59311'] = $key->G2973_C59311;

                $datos[$i]['G2973_C59312'] = $key->G2973_C59312;

                $datos[$i]['G2973_C59313'] = $key->G2973_C59313;

                $datos[$i]['G2973_C59314'] = $key->G2973_C59314;

                $datos[$i]['G2973_C59315'] = $key->G2973_C59315;

                $datos[$i]['G2973_C59547'] = $key->G2973_C59547;

                $datos[$i]['G2973_C59548'] = $key->G2973_C59548;

                $datos[$i]['G2973_C59549'] = $key->G2973_C59549;

                $datos[$i]['G2973_C59550'] = $key->G2973_C59550;

                $datos[$i]['G2973_C59551'] = $key->G2973_C59551;

                $datos[$i]['G2973_C59552'] = $key->G2973_C59552;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2973";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2973_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1 
                     FROM ".$BaseDatos.".G2973  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1  
                    FROM ".$BaseDatos.".G2973  JOIN ".$BaseDatos.".G2973_M".$_POST['muestra']." ON G2973_ConsInte__b = G2973_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2973_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2973_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2973_C59320 LIKE '%".$B."%' OR G2973_C59322 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2973_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

            if(isset($_GET['MostrarCombo_Guion_2974_C59553'])){
                echo '<select class="form-control input-sm"  name="2974_C59553" id="2974_C59553">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2974_C59553'])){
                $Ysql = "SELECT G2969_ConsInte__b as id,  G2969_C59267 as text FROM ".$BaseDatos.".G2969 WHERE G2969_C59267 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2974_C59553'])){
                $Lsql = "SELECT  G2969_ConsInte__b as id , G2969_C59268, G2969_C59267 FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2974_C59553'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2974_C59554'] = $key->G2969_C59268;

                    $data[$i]['G2974_C59555'] = $key->G2969_C59267;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2973");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2973_ConsInte__b, G2973_FechaInsercion , G2973_Usuario ,  G2973_CodigoMiembro  , G2973_PoblacionOrigen , G2973_EstadoDiligenciamiento ,  G2973_IdLlamada , G2973_C59322 as principal ,G2973_C59320,G2973_C59322,G2973_C59321,G2973_C59544,G2973_C59545,G2973_C59546, a.LISOPC_Nombre____b as G2973_C59307, b.LISOPC_Nombre____b as G2973_C59308,G2973_C59309,G2973_C59310,G2973_C59311,G2973_C59312,G2973_C59313,G2973_C59314,G2973_C59315,G2973_C59547,G2973_C59548,G2973_C59549,G2973_C59550,G2973_C59551,G2973_C59552 FROM '.$BaseDatos.'.G2973 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2973_C59307 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2973_C59308';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2973_C59310)){
                    $hora_a = explode(' ', $fila->G2973_C59310)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2973_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2973_ConsInte__b , ($fila->G2973_C59320) , ($fila->G2973_C59322) , ($fila->G2973_C59321) , ($fila->G2973_C59544) , ($fila->G2973_C59545) , ($fila->G2973_C59546) , ($fila->G2973_C59307) , ($fila->G2973_C59308) , explode(' ', $fila->G2973_C59309)[0] , $hora_a , ($fila->G2973_C59311) , ($fila->G2973_C59312) , ($fila->G2973_C59313) , ($fila->G2973_C59314) , ($fila->G2973_C59315) , ($fila->G2973_C59547) , ($fila->G2973_C59548) , ($fila->G2973_C59549) , ($fila->G2973_C59550) , ($fila->G2973_C59551) , ($fila->G2973_C59552) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2973 WHERE G2973_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2973";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2973_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1  FROM '.$BaseDatos.'.G2973 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1  
                    FROM ".$BaseDatos.".G2973  JOIN ".$BaseDatos.".G2973_M".$_POST['muestra']." ON G2973_ConsInte__b = G2973_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2973_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2973_C59320 LIKE "%'.$B.'%" OR G2973_C59322 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2973_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2973 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2973(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2973_C59320"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59320 = '".$_POST["G2973_C59320"]."'";
                $LsqlI .= $separador."G2973_C59320";
                $LsqlV .= $separador."'".$_POST["G2973_C59320"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59322 = '".$_POST["G2973_C59322"]."'";
                $LsqlI .= $separador."G2973_C59322";
                $LsqlV .= $separador."'".$_POST["G2973_C59322"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59321"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59321 = '".$_POST["G2973_C59321"]."'";
                $LsqlI .= $separador."G2973_C59321";
                $LsqlV .= $separador."'".$_POST["G2973_C59321"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59544"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59544 = '".$_POST["G2973_C59544"]."'";
                $LsqlI .= $separador."G2973_C59544";
                $LsqlV .= $separador."'".$_POST["G2973_C59544"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59545 = '".$_POST["G2973_C59545"]."'";
                $LsqlI .= $separador."G2973_C59545";
                $LsqlV .= $separador."'".$_POST["G2973_C59545"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59546"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59546 = '".$_POST["G2973_C59546"]."'";
                $LsqlI .= $separador."G2973_C59546";
                $LsqlV .= $separador."'".$_POST["G2973_C59546"]."'";
                $validar = 1;
            }
             
 
            $G2973_C59307 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59307 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2973_C59307 = ".$G2973_C59307;
                    $LsqlI .= $separador." G2973_C59307";
                    $LsqlV .= $separador.$G2973_C59307;
                    $validar = 1;

                    
                }
            }
 
            $G2973_C59308 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59308 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2973_C59308 = ".$G2973_C59308;
                    $LsqlI .= $separador." G2973_C59308";
                    $LsqlV .= $separador.$G2973_C59308;
                    $validar = 1;
                }
            }
 
            $G2973_C59309 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59309 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2973_C59309 = ".$G2973_C59309;
                    $LsqlI .= $separador." G2973_C59309";
                    $LsqlV .= $separador.$G2973_C59309;
                    $validar = 1;
                }
            }
 
            $G2973_C59310 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59310 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2973_C59310 = ".$G2973_C59310;
                    $LsqlI .= $separador." G2973_C59310";
                    $LsqlV .= $separador.$G2973_C59310;
                    $validar = 1;
                }
            }
 
            $G2973_C59311 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59311 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2973_C59311 = ".$G2973_C59311;
                    $LsqlI .= $separador." G2973_C59311";
                    $LsqlV .= $separador.$G2973_C59311;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2973_C59312"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59312 = '".$_POST["G2973_C59312"]."'";
                $LsqlI .= $separador."G2973_C59312";
                $LsqlV .= $separador."'".$_POST["G2973_C59312"]."'";
                $validar = 1;
            }
             
  
            //JDBD - Fecha oculta Script
            if(isset($_POST["G2973_C59313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date("Y-m-d H:i:s");

                $LsqlU .= $separador."G2973_C59313 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2973_C59313";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            //JDBD - Hora oculta Script
            if(isset($_POST["G2973_C59314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date("H:i:s");

                $LsqlU .= $separador."G2973_C59314 = '".$strHora_t."'";
                $LsqlI .= $separador."G2973_C59314";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59315 = '".$_POST["G2973_C59315"]."'";
                $LsqlI .= $separador."G2973_C59315";
                $LsqlV .= $separador."'".$_POST["G2973_C59315"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59316 = '".$_POST["G2973_C59316"]."'";
                $LsqlI .= $separador."G2973_C59316";
                $LsqlV .= $separador."'".$_POST["G2973_C59316"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59317 = '".$_POST["G2973_C59317"]."'";
                $LsqlI .= $separador."G2973_C59317";
                $LsqlV .= $separador."'".$_POST["G2973_C59317"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59547"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59547 = '".$_POST["G2973_C59547"]."'";
                $LsqlI .= $separador."G2973_C59547";
                $LsqlV .= $separador."'".$_POST["G2973_C59547"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59548 = '".$_POST["G2973_C59548"]."'";
                $LsqlI .= $separador."G2973_C59548";
                $LsqlV .= $separador."'".$_POST["G2973_C59548"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59549"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59549 = '".$_POST["G2973_C59549"]."'";
                $LsqlI .= $separador."G2973_C59549";
                $LsqlV .= $separador."'".$_POST["G2973_C59549"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59550"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59550 = '".$_POST["G2973_C59550"]."'";
                $LsqlI .= $separador."G2973_C59550";
                $LsqlV .= $separador."'".$_POST["G2973_C59550"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59551 = '".$_POST["G2973_C59551"]."'";
                $LsqlI .= $separador."G2973_C59551";
                $LsqlV .= $separador."'".$_POST["G2973_C59551"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59552"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59552 = '".$_POST["G2973_C59552"]."'";
                $LsqlI .= $separador."G2973_C59552";
                $LsqlV .= $separador."'".$_POST["G2973_C59552"]."'";
                $validar = 1;
            }
                    
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2973_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2973_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2973_Usuario , G2973_FechaInsercion, G2973_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2973_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2973 WHERE G2973_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2974_ConsInte__b, G2974_C59333, G2974_C59334, G2974_C59335, G2974_C59336, G2974_C59337, G2969_C59266 as G2974_C59553, G2974_C59554, G2974_C59555, G2974_C59556, G2974_C59557, G2974_C59558, G2974_C59559, G2974_C59560 FROM ".$BaseDatos.".G2974  LEFT JOIN ".$BaseDatos.".G2969 ON G2969_ConsInte__b  =  G2974_C59553 ";

        $SQL .= " WHERE G2974_C59553 = '".$numero."'"; 

        $SQL .= " ORDER BY G2974_C59333";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2974_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2974_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2974_C59333."</cell>"; 

                if($fila->G2974_C59334 != ''){
                    echo "<cell>". explode(' ', $fila->G2974_C59334)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2974_C59335 != ''){
                    echo "<cell>". explode(' ', $fila->G2974_C59335)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2974_C59336 != ''){
                    echo "<cell>". explode(' ', $fila->G2974_C59336)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2974_C59337 != ''){
                    echo "<cell>". explode(' ', $fila->G2974_C59337)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2974_C59553)."</cell>";

                echo "<cell>". ($fila->G2974_C59554)."</cell>";

                echo "<cell>". ($fila->G2974_C59555)."</cell>";

                echo "<cell>". ($fila->G2974_C59556)."</cell>";

                echo "<cell>". ($fila->G2974_C59557)."</cell>";

                echo "<cell>". ($fila->G2974_C59558)."</cell>";

                echo "<cell>". ($fila->G2974_C59559)."</cell>";

                echo "<cell>". ($fila->G2974_C59560)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2974 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2974(";
            $LsqlV = " VALUES ("; 
 
                $G2974_C59333= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2974_C59333"])){    
                    if($_POST["G2974_C59333"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2974_C59333 = $_POST["G2974_C59333"];
                        $LsqlU .= $separador." G2974_C59333 = '".$G2974_C59333."'";
                        $LsqlI .= $separador." G2974_C59333";
                        $LsqlV .= $separador."'".$G2974_C59333."'";
                        $validar = 1;
                    }
                }

                $G2974_C59334 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2974_C59334"])){    
                    if($_POST["G2974_C59334"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2974_C59334 = "'".str_replace(' ', '',$_POST["G2974_C59334"])." 00:00:00'";
                        $LsqlU .= $separador." G2974_C59334 = ".$G2974_C59334;
                        $LsqlI .= $separador." G2974_C59334";
                        $LsqlV .= $separador.$G2974_C59334;
                        $validar = 1;
                    }
                }
 
                $G2974_C59335 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2974_C59335"])){    
                    if($_POST["G2974_C59335"] != '' && $_POST["G2974_C59335"] != 'undefined' && $_POST["G2974_C59335"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2974_C59335 = "'".$fecha." ".str_replace(' ', '',$_POST["G2974_C59335"])."'";
                        $LsqlU .= $separador."  G2974_C59335 = ".$G2974_C59335."";
                        $LsqlI .= $separador."  G2974_C59335";
                        $LsqlV .= $separador.$G2974_C59335;
                        $validar = 1;
                    }
                }

                $G2974_C59336 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2974_C59336"])){    
                    if($_POST["G2974_C59336"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2974_C59336 = "'".str_replace(' ', '',$_POST["G2974_C59336"])." 00:00:00'";
                        $LsqlU .= $separador." G2974_C59336 = ".$G2974_C59336;
                        $LsqlI .= $separador." G2974_C59336";
                        $LsqlV .= $separador.$G2974_C59336;
                        $validar = 1;
                    }
                }
 
                $G2974_C59337 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2974_C59337"])){    
                    if($_POST["G2974_C59337"] != '' && $_POST["G2974_C59337"] != 'undefined' && $_POST["G2974_C59337"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2974_C59337 = "'".$fecha." ".str_replace(' ', '',$_POST["G2974_C59337"])."'";
                        $LsqlU .= $separador."  G2974_C59337 = ".$G2974_C59337."";
                        $LsqlI .= $separador."  G2974_C59337";
                        $LsqlV .= $separador.$G2974_C59337;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2974_C59554"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59554 = '".$_POST["G2974_C59554"]."'";
                    $LsqlI .= $separador."G2974_C59554";
                    $LsqlV .= $separador."'".$_POST["G2974_C59554"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2974_C59555"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59555 = '".$_POST["G2974_C59555"]."'";
                    $LsqlI .= $separador."G2974_C59555";
                    $LsqlV .= $separador."'".$_POST["G2974_C59555"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2974_C59556"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59556 = '".$_POST["G2974_C59556"]."'";
                    $LsqlI .= $separador."G2974_C59556";
                    $LsqlV .= $separador."'".$_POST["G2974_C59556"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2974_C59557"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59557 = '".$_POST["G2974_C59557"]."'";
                    $LsqlI .= $separador."G2974_C59557";
                    $LsqlV .= $separador."'".$_POST["G2974_C59557"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2974_C59558"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59558 = '".$_POST["G2974_C59558"]."'";
                    $LsqlI .= $separador."G2974_C59558";
                    $LsqlV .= $separador."'".$_POST["G2974_C59558"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2974_C59559"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59559 = '".$_POST["G2974_C59559"]."'";
                    $LsqlI .= $separador."G2974_C59559";
                    $LsqlV .= $separador."'".$_POST["G2974_C59559"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2974_C59560"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2974_C59560 = '".$_POST["G2974_C59560"]."'";
                    $LsqlI .= $separador."G2974_C59560";
                    $LsqlV .= $separador."'".$_POST["G2974_C59560"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2974_C59553 = $numero;
                    $LsqlU .= ", G2974_C59553 = ".$G2974_C59553."";
                    $LsqlI .= ", G2974_C59553";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2974_Usuario ,  G2974_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2974_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2974 WHERE  G2974_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
