<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 59737")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 59737");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2993_ConsInte__b, G2993_FechaInsercion , G2993_Usuario ,  G2993_CodigoMiembro  , G2993_PoblacionOrigen , G2993_EstadoDiligenciamiento ,  G2993_IdLlamada , G2993_C59737 as principal ,G2993_C59737,G2993_C59738,G2993_C59739,G2993_C59740,G2993_C59741,G2993_C59734,G2993_C59735,G2993_C59736,G2993_C59770,G2993_C59771,G2993_C59772,G2993_C59773,G2993_C59774,G2993_C59775,G2993_C59776,G2993_C59777,G2993_C59778,G2993_C59803,G2993_C59804,G2993_C59805,G2993_C59806,G2993_C59897 FROM '.$BaseDatos.'.G2993 WHERE G2993_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2993_C59737'] = $key->G2993_C59737;

                $datos[$i]['G2993_C59738'] = explode(' ', $key->G2993_C59738)[0];
  
                $hora = '';
                if(!is_null($key->G2993_C59739)){
                    $hora = explode(' ', $key->G2993_C59739)[1];
                }

                $datos[$i]['G2993_C59739'] = $hora;

                $datos[$i]['G2993_C59740'] = explode(' ', $key->G2993_C59740)[0];
  
                $hora = '';
                if(!is_null($key->G2993_C59741)){
                    $hora = explode(' ', $key->G2993_C59741)[1];
                }

                $datos[$i]['G2993_C59741'] = $hora;

                $datos[$i]['G2993_C59734'] = $key->G2993_C59734;

                $datos[$i]['G2993_C59735'] = $key->G2993_C59735;

                $datos[$i]['G2993_C59736'] = $key->G2993_C59736;

                $datos[$i]['G2993_C59770'] = $key->G2993_C59770;

                $datos[$i]['G2993_C59771'] = $key->G2993_C59771;

                $datos[$i]['G2993_C59772'] = $key->G2993_C59772;

                $datos[$i]['G2993_C59773'] = $key->G2993_C59773;

                $datos[$i]['G2993_C59774'] = $key->G2993_C59774;

                $datos[$i]['G2993_C59775'] = $key->G2993_C59775;

                $datos[$i]['G2993_C59776'] = $key->G2993_C59776;

                $datos[$i]['G2993_C59777'] = $key->G2993_C59777;

                $datos[$i]['G2993_C59778'] = $key->G2993_C59778;

                $datos[$i]['G2993_C59803'] = $key->G2993_C59803;

                $datos[$i]['G2993_C59804'] = $key->G2993_C59804;

                $datos[$i]['G2993_C59805'] = $key->G2993_C59805;

                $datos[$i]['G2993_C59806'] = $key->G2993_C59806;

                $datos[$i]['G2993_C59897'] = $key->G2993_C59897;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2993";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2993_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2993_ConsInte__b as id,  G2993_C59737 as camp1 , G2993_C59738 as camp2 
                     FROM ".$BaseDatos.".G2993  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2993_ConsInte__b as id,  G2993_C59737 as camp1 , G2993_C59738 as camp2  
                    FROM ".$BaseDatos.".G2993  JOIN ".$BaseDatos.".G2993_M".$_POST['muestra']." ON G2993_ConsInte__b = G2993_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2993_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2993_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2993_C59737 LIKE '%".$B."%' OR G2993_C59738 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2993_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2993_C59770'])){
                                $Ysql = "SELECT G2969_ConsInte__b as id, G2969_C59266 as text FROM ".$BaseDatos.".G2969 WHERE G2969_C59266 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2993_C59770"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2993_C59770"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2993");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2993_ConsInte__b, G2993_FechaInsercion , G2993_Usuario ,  G2993_CodigoMiembro  , G2993_PoblacionOrigen , G2993_EstadoDiligenciamiento ,  G2993_IdLlamada , G2993_C59737 as principal ,G2993_C59737,G2993_C59738,G2993_C59739,G2993_C59740,G2993_C59741,G2993_C59734,G2993_C59735, a.LISOPC_Nombre____b as G2993_C59736, G2969_C59266,G2993_C59771,G2993_C59772,G2993_C59773,G2993_C59774,G2993_C59775,G2993_C59776,G2993_C59777,G2993_C59778, b.LISOPC_Nombre____b as G2993_C59803, c.LISOPC_Nombre____b as G2993_C59804, d.LISOPC_Nombre____b as G2993_C59805,G2993_C59806, e.LISOPC_Nombre____b as G2993_C59897 FROM '.$BaseDatos.'.G2993 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2993_C59736 LEFT JOIN '.$BaseDatos.'.G2969 ON G2969_ConsInte__b  =  G2993_C59770 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2993_C59803 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2993_C59804 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2993_C59805 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2993_C59897';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2993_C59739)){
                    $hora_a = explode(' ', $fila->G2993_C59739)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2993_C59741)){
                    $hora_b = explode(' ', $fila->G2993_C59741)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2993_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2993_ConsInte__b , ($fila->G2993_C59737) , explode(' ', $fila->G2993_C59738)[0] , $hora_a , explode(' ', $fila->G2993_C59740)[0] , $hora_b , ($fila->G2993_C59734) , ($fila->G2993_C59735) , ($fila->G2993_C59736) , ($fila->G2969_C59266) , ($fila->G2993_C59771) , ($fila->G2993_C59772) , ($fila->G2993_C59773) , ($fila->G2993_C59774) , ($fila->G2993_C59775) , ($fila->G2993_C59776) , ($fila->G2993_C59777) , ($fila->G2993_C59778) , ($fila->G2993_C59803) , ($fila->G2993_C59804) , ($fila->G2993_C59805) , ($fila->G2993_C59806) , ($fila->G2993_C59897) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2993 WHERE G2993_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2993";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2993_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2993_ConsInte__b as id,  G2993_C59737 as camp1 , G2993_C59738 as camp2  FROM '.$BaseDatos.'.G2993 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2993_ConsInte__b as id,  G2993_C59737 as camp1 , G2993_C59738 as camp2  
                    FROM ".$BaseDatos.".G2993  JOIN ".$BaseDatos.".G2993_M".$_POST['muestra']." ON G2993_ConsInte__b = G2993_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2993_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2993_C59737 LIKE "%'.$B.'%" OR G2993_C59738 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2993_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2993 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2993(";
            $LsqlV = " VALUES ("; 
  
            $G2993_C59737 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2993_C59737"])){
                if($_POST["G2993_C59737"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2993_C59737 = $_POST["G2993_C59737"];
                    $LsqlU .= $separador." G2993_C59737 = ".$G2993_C59737."";
                    $LsqlI .= $separador." G2993_C59737";
                    $LsqlV .= $separador.$G2993_C59737;
                    $validar = 1;
                }
            }
 
            $G2993_C59738 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2993_C59738"])){    
                if($_POST["G2993_C59738"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2993_C59738"]);
                    if(count($tieneHora) > 1){
                        $G2993_C59738 = "'".$_POST["G2993_C59738"]."'";
                    }else{
                        $G2993_C59738 = "'".str_replace(' ', '',$_POST["G2993_C59738"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2993_C59738 = ".$G2993_C59738;
                    $LsqlI .= $separador." G2993_C59738";
                    $LsqlV .= $separador.$G2993_C59738;
                    $validar = 1;
                }
            }
  
            $G2993_C59739 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G2993_C59739"])){   
                if($_POST["G2993_C59739"] != '' && $_POST["G2993_C59739"] != 'undefined' && $_POST["G2993_C59739"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2993_C59739 = "'".$fecha." ".str_replace(' ', '',$_POST["G2993_C59739"])."'";
                    $LsqlU .= $separador." G2993_C59739 = ".$G2993_C59739."";
                    $LsqlI .= $separador." G2993_C59739";
                    $LsqlV .= $separador.$G2993_C59739;
                    $validar = 1;
                }
            }
 
            $G2993_C59740 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2993_C59740"])){    
                if($_POST["G2993_C59740"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2993_C59740"]);
                    if(count($tieneHora) > 1){
                        $G2993_C59740 = "'".$_POST["G2993_C59740"]."'";
                    }else{
                        $G2993_C59740 = "'".str_replace(' ', '',$_POST["G2993_C59740"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2993_C59740 = ".$G2993_C59740;
                    $LsqlI .= $separador." G2993_C59740";
                    $LsqlV .= $separador.$G2993_C59740;
                    $validar = 1;
                }
            }
  
            $G2993_C59741 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G2993_C59741"])){   
                if($_POST["G2993_C59741"] != '' && $_POST["G2993_C59741"] != 'undefined' && $_POST["G2993_C59741"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2993_C59741 = "'".$fecha." ".str_replace(' ', '',$_POST["G2993_C59741"])."'";
                    $LsqlU .= $separador." G2993_C59741 = ".$G2993_C59741."";
                    $LsqlI .= $separador." G2993_C59741";
                    $LsqlV .= $separador.$G2993_C59741;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2993_C59734"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59734 = '".$_POST["G2993_C59734"]."'";
                $LsqlI .= $separador."G2993_C59734";
                $LsqlV .= $separador."'".$_POST["G2993_C59734"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59735"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59735 = '".$_POST["G2993_C59735"]."'";
                $LsqlI .= $separador."G2993_C59735";
                $LsqlV .= $separador."'".$_POST["G2993_C59735"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59736"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59736 = '".$_POST["G2993_C59736"]."'";
                $LsqlI .= $separador."G2993_C59736";
                $LsqlV .= $separador."'".$_POST["G2993_C59736"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59770"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59770 = '".$_POST["G2993_C59770"]."'";
                $LsqlI .= $separador."G2993_C59770";
                $LsqlV .= $separador."'".$_POST["G2993_C59770"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59771"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59771 = '".$_POST["G2993_C59771"]."'";
                $LsqlI .= $separador."G2993_C59771";
                $LsqlV .= $separador."'".$_POST["G2993_C59771"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59772"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59772 = '".$_POST["G2993_C59772"]."'";
                $LsqlI .= $separador."G2993_C59772";
                $LsqlV .= $separador."'".$_POST["G2993_C59772"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59773"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59773 = '".$_POST["G2993_C59773"]."'";
                $LsqlI .= $separador."G2993_C59773";
                $LsqlV .= $separador."'".$_POST["G2993_C59773"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59774"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59774 = '".$_POST["G2993_C59774"]."'";
                $LsqlI .= $separador."G2993_C59774";
                $LsqlV .= $separador."'".$_POST["G2993_C59774"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59775"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59775 = '".$_POST["G2993_C59775"]."'";
                $LsqlI .= $separador."G2993_C59775";
                $LsqlV .= $separador."'".$_POST["G2993_C59775"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59776"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59776 = '".$_POST["G2993_C59776"]."'";
                $LsqlI .= $separador."G2993_C59776";
                $LsqlV .= $separador."'".$_POST["G2993_C59776"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59777"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59777 = '".$_POST["G2993_C59777"]."'";
                $LsqlI .= $separador."G2993_C59777";
                $LsqlV .= $separador."'".$_POST["G2993_C59777"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59778"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59778 = '".$_POST["G2993_C59778"]."'";
                $LsqlI .= $separador."G2993_C59778";
                $LsqlV .= $separador."'".$_POST["G2993_C59778"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59803"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59803 = '".$_POST["G2993_C59803"]."'";
                $LsqlI .= $separador."G2993_C59803";
                $LsqlV .= $separador."'".$_POST["G2993_C59803"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59804"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59804 = '".$_POST["G2993_C59804"]."'";
                $LsqlI .= $separador."G2993_C59804";
                $LsqlV .= $separador."'".$_POST["G2993_C59804"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59805"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59805 = '".$_POST["G2993_C59805"]."'";
                $LsqlI .= $separador."G2993_C59805";
                $LsqlV .= $separador."'".$_POST["G2993_C59805"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59806"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59806 = '".$_POST["G2993_C59806"]."'";
                $LsqlI .= $separador."G2993_C59806";
                $LsqlV .= $separador."'".$_POST["G2993_C59806"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2993_C59897"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_C59897 = '".$_POST["G2993_C59897"]."'";
                $LsqlI .= $separador."G2993_C59897";
                $LsqlV .= $separador."'".$_POST["G2993_C59897"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2993_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2993_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2993_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2993_Usuario , G2993_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2993_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2993 WHERE G2993_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2993 SET G2993_UltiGest__b =-14, G2993_GesMasImp_b =-14, G2993_TipoReintentoUG_b =0, G2993_TipoReintentoGMI_b =0, G2993_EstadoUG_b =-14, G2993_EstadoGMI_b =-14, G2993_CantidadIntentos =0, G2993_CantidadIntentosGMI_b =0 WHERE G2993_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2993_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2993 WHERE G2993_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2992_ConsInte__b, G2992_C59729, G2992_C59730, G2992_C59731, G2992_C59732, G2992_C59733 FROM ".$BaseDatos.".G2992  ";

        $SQL .= " WHERE G2992_C59733 = '".$numero."'"; 

        $SQL .= " ORDER BY G2992_C59729";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2992_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2992_ConsInte__b)."</cell>"; 
            

                echo "<cell><![CDATA[". ($fila->G2992_C59729)."]]></cell>";

                echo "<cell>". ($fila->G2992_C59730)."</cell>";

                if($fila->G2992_C59731 != ''){
                    echo "<cell>". explode(' ', $fila->G2992_C59731)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2992_C59732 != ''){
                    echo "<cell>". explode(' ', $fila->G2992_C59732)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". $fila->G2992_C59733."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2992 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2992(";
            $LsqlV = " VALUES ("; 
  

                if(isset($_POST["G2992_C59729"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2992_C59729 = '".$_POST["G2992_C59729"]."'";
                    $LsqlI .= $separador."G2992_C59729";
                    $LsqlV .= $separador."'".$_POST["G2992_C59729"]."'";
                    $validar = 1;
                }
                                                                               
 

                if(isset($_POST["G2992_C59730"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2992_C59730 = '".$_POST["G2992_C59730"]."'";
                    $LsqlI .= $separador."G2992_C59730";
                    $LsqlV .= $separador."'".$_POST["G2992_C59730"]."'";
                    $validar = 1;
                }

                                                                               

                $G2992_C59731 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2992_C59731"])){    
                    if($_POST["G2992_C59731"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2992_C59731 = "'".str_replace(' ', '',$_POST["G2992_C59731"])." 00:00:00'";
                        $LsqlU .= $separador." G2992_C59731 = ".$G2992_C59731;
                        $LsqlI .= $separador." G2992_C59731";
                        $LsqlV .= $separador.$G2992_C59731;
                        $validar = 1;
                    }
                }
 
                $G2992_C59732 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2992_C59732"])){    
                    if($_POST["G2992_C59732"] != '' && $_POST["G2992_C59732"] != 'undefined' && $_POST["G2992_C59732"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2992_C59732 = "'".$fecha." ".str_replace(' ', '',$_POST["G2992_C59732"])."'";
                        $LsqlU .= $separador."  G2992_C59732 = ".$G2992_C59732."";
                        $LsqlI .= $separador."  G2992_C59732";
                        $LsqlV .= $separador.$G2992_C59732;
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2992_C59733 = $numero;
                    $LsqlU .= ", G2992_C59733 = ".$G2992_C59733."";
                    $LsqlI .= ", G2992_C59733";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2992_Usuario ,  G2992_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2992_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2992 WHERE  G2992_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
