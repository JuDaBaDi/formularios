<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2997_ConsInte__b, G2997_FechaInsercion , G2997_Usuario ,  G2997_CodigoMiembro  , G2997_PoblacionOrigen , G2997_EstadoDiligenciamiento ,  G2997_IdLlamada , G2997_C59792 as principal ,G2997_C59790,G2997_C59791,G2997_C59792,G2997_C59802,G2997_C59779,G2997_C59780,G2997_C59781,G2997_C59782,G2997_C59783,G2997_C59784,G2997_C59785,G2997_C59786,G2997_C59787,G2997_C59788,G2997_C59789,G2997_C59793,G2997_C59794,G2997_C59795 FROM '.$BaseDatos.'.G2997 WHERE G2997_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2997_C59790'] = $key->G2997_C59790;

                $datos[$i]['G2997_C59791'] = $key->G2997_C59791;

                $datos[$i]['G2997_C59792'] = $key->G2997_C59792;

                $datos[$i]['G2997_C59802'] = $key->G2997_C59802;

                $datos[$i]['G2997_C59779'] = $key->G2997_C59779;

                $datos[$i]['G2997_C59780'] = $key->G2997_C59780;

                $datos[$i]['G2997_C59781'] = explode(' ', $key->G2997_C59781)[0];
  
                $hora = '';
                if(!is_null($key->G2997_C59782)){
                    $hora = explode(' ', $key->G2997_C59782)[1];
                }

                $datos[$i]['G2997_C59782'] = $hora;

                $datos[$i]['G2997_C59783'] = $key->G2997_C59783;

                $datos[$i]['G2997_C59784'] = $key->G2997_C59784;

                $datos[$i]['G2997_C59785'] = $key->G2997_C59785;

                $datos[$i]['G2997_C59786'] = $key->G2997_C59786;

                $datos[$i]['G2997_C59787'] = $key->G2997_C59787;

                $datos[$i]['G2997_C59788'] = $key->G2997_C59788;

                $datos[$i]['G2997_C59789'] = $key->G2997_C59789;

                $datos[$i]['G2997_C59793'] = $key->G2997_C59793;

                $datos[$i]['G2997_C59794'] = $key->G2997_C59794;

                $datos[$i]['G2997_C59795'] = $key->G2997_C59795;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2997";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2997_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2997_ConsInte__b as id,  G2997_C59790 as camp2 , G2997_C59792 as camp1 
                     FROM ".$BaseDatos.".G2997  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2997_ConsInte__b as id,  G2997_C59790 as camp2 , G2997_C59792 as camp1  
                    FROM ".$BaseDatos.".G2997  JOIN ".$BaseDatos.".G2997_M".$_POST['muestra']." ON G2997_ConsInte__b = G2997_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2997_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2997_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2997_C59790 LIKE '%".$B."%' OR G2997_C59792 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2997_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

            if(isset($_GET['MostrarCombo_Guion_2993_C59770'])){
                echo '<select class="form-control input-sm"  name="2993_C59770" id="2993_C59770">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2993_C59770'])){
                $Ysql = "SELECT G2969_ConsInte__b as id,  G2969_C59267 as text FROM ".$BaseDatos.".G2969 WHERE G2969_C59267 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2993_C59770'])){
                $Lsql = "SELECT  G2969_ConsInte__b as id , G2969_C59266, G2969_C59268, G2969_C59267, G2969_C59341, G2969_C59339, G2969_C59340, G2969_C59342, G2969_C59343 FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2993_C59770'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2993_C59771'] = $key->G2969_C59266;

                    $data[$i]['G2993_C59772'] = $key->G2969_C59268;

                    $data[$i]['G2993_C59773'] = $key->G2969_C59267;

                    $data[$i]['G2993_C59774'] = $key->G2969_C59341;

                    $data[$i]['G2993_C59775'] = $key->G2969_C59339;

                    $data[$i]['G2993_C59776'] = $key->G2969_C59340;

                    $data[$i]['G2993_C59777'] = $key->G2969_C59342;

                    $data[$i]['G2993_C59778'] = $key->G2969_C59343;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2997");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2997_ConsInte__b, G2997_FechaInsercion , G2997_Usuario ,  G2997_CodigoMiembro  , G2997_PoblacionOrigen , G2997_EstadoDiligenciamiento ,  G2997_IdLlamada , G2997_C59792 as principal ,G2997_C59790,G2997_C59791,G2997_C59792, a.LISOPC_Nombre____b as G2997_C59802, b.LISOPC_Nombre____b as G2997_C59779, c.LISOPC_Nombre____b as G2997_C59780,G2997_C59781,G2997_C59782,G2997_C59783,G2997_C59784,G2997_C59785,G2997_C59786,G2997_C59787,G2997_C59788,G2997_C59789,G2997_C59793,G2997_C59794,G2997_C59795 FROM '.$BaseDatos.'.G2997 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2997_C59802 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2997_C59779 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2997_C59780';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2997_C59782)){
                    $hora_a = explode(' ', $fila->G2997_C59782)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2997_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2997_ConsInte__b , ($fila->G2997_C59790) , ($fila->G2997_C59791) , ($fila->G2997_C59792) , ($fila->G2997_C59802) , ($fila->G2997_C59779) , ($fila->G2997_C59780) , explode(' ', $fila->G2997_C59781)[0] , $hora_a , ($fila->G2997_C59783) , ($fila->G2997_C59784) , ($fila->G2997_C59785) , ($fila->G2997_C59786) , ($fila->G2997_C59787) , ($fila->G2997_C59788) , ($fila->G2997_C59789) , ($fila->G2997_C59793) , ($fila->G2997_C59794) , ($fila->G2997_C59795) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2997 WHERE G2997_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2997";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2997_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2997_ConsInte__b as id,  G2997_C59790 as camp2 , G2997_C59792 as camp1  FROM '.$BaseDatos.'.G2997 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2997_ConsInte__b as id,  G2997_C59790 as camp2 , G2997_C59792 as camp1  
                    FROM ".$BaseDatos.".G2997  JOIN ".$BaseDatos.".G2997_M".$_POST['muestra']." ON G2997_ConsInte__b = G2997_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2997_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2997_C59790 LIKE "%'.$B.'%" OR G2997_C59792 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2997_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2997 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2997(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2997_C59790"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59790 = '".$_POST["G2997_C59790"]."'";
                $LsqlI .= $separador."G2997_C59790";
                $LsqlV .= $separador."'".$_POST["G2997_C59790"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59791"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59791 = '".$_POST["G2997_C59791"]."'";
                $LsqlI .= $separador."G2997_C59791";
                $LsqlV .= $separador."'".$_POST["G2997_C59791"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59792"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59792 = '".$_POST["G2997_C59792"]."'";
                $LsqlI .= $separador."G2997_C59792";
                $LsqlV .= $separador."'".$_POST["G2997_C59792"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59802"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59802 = '".$_POST["G2997_C59802"]."'";
                $LsqlI .= $separador."G2997_C59802";
                $LsqlV .= $separador."'".$_POST["G2997_C59802"]."'";
                $validar = 1;
            }
             
 
            $G2997_C59779 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2997_C59779 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2997_C59779 = ".$G2997_C59779;
                    $LsqlI .= $separador." G2997_C59779";
                    $LsqlV .= $separador.$G2997_C59779;
                    $validar = 1;

                    
                }
            }
 
            $G2997_C59780 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2997_C59780 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2997_C59780 = ".$G2997_C59780;
                    $LsqlI .= $separador." G2997_C59780";
                    $LsqlV .= $separador.$G2997_C59780;
                    $validar = 1;
                }
            }
 
            $G2997_C59781 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2997_C59781 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2997_C59781 = ".$G2997_C59781;
                    $LsqlI .= $separador." G2997_C59781";
                    $LsqlV .= $separador.$G2997_C59781;
                    $validar = 1;
                }
            }
 
            $G2997_C59782 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2997_C59782 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2997_C59782 = ".$G2997_C59782;
                    $LsqlI .= $separador." G2997_C59782";
                    $LsqlV .= $separador.$G2997_C59782;
                    $validar = 1;
                }
            }
 
            $G2997_C59783 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2997_C59783 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2997_C59783 = ".$G2997_C59783;
                    $LsqlI .= $separador." G2997_C59783";
                    $LsqlV .= $separador.$G2997_C59783;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2997_C59784"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59784 = '".$_POST["G2997_C59784"]."'";
                $LsqlI .= $separador."G2997_C59784";
                $LsqlV .= $separador."'".$_POST["G2997_C59784"]."'";
                $validar = 1;
            }
             
  
            //JDBD - Fecha oculta Script
            if(isset($_POST["G2997_C59785"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date("Y-m-d H:i:s");

                $LsqlU .= $separador."G2997_C59785 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2997_C59785";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            //JDBD - Hora oculta Script
            if(isset($_POST["G2997_C59786"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date("H:i:s");

                $LsqlU .= $separador."G2997_C59786 = '".$strHora_t."'";
                $LsqlI .= $separador."G2997_C59786";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59787"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59787 = '".$_POST["G2997_C59787"]."'";
                $LsqlI .= $separador."G2997_C59787";
                $LsqlV .= $separador."'".$_POST["G2997_C59787"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59788"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59788 = '".$_POST["G2997_C59788"]."'";
                $LsqlI .= $separador."G2997_C59788";
                $LsqlV .= $separador."'".$_POST["G2997_C59788"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59789"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59789 = '".$_POST["G2997_C59789"]."'";
                $LsqlI .= $separador."G2997_C59789";
                $LsqlV .= $separador."'".$_POST["G2997_C59789"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59793"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59793 = '".$_POST["G2997_C59793"]."'";
                $LsqlI .= $separador."G2997_C59793";
                $LsqlV .= $separador."'".$_POST["G2997_C59793"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59794"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59794 = '".$_POST["G2997_C59794"]."'";
                $LsqlI .= $separador."G2997_C59794";
                $LsqlV .= $separador."'".$_POST["G2997_C59794"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59795"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59795 = '".$_POST["G2997_C59795"]."'";
                $LsqlI .= $separador."G2997_C59795";
                $LsqlV .= $separador."'".$_POST["G2997_C59795"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59796"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59796 = '".$_POST["G2997_C59796"]."'";
                $LsqlI .= $separador."G2997_C59796";
                $LsqlV .= $separador."'".$_POST["G2997_C59796"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2997_C59798"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_C59798 = '".$_POST["G2997_C59798"]."'";
                $LsqlI .= $separador."G2997_C59798";
                $LsqlV .= $separador."'".$_POST["G2997_C59798"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2997_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2997_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2997_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2997_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2997_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2997_Usuario , G2997_FechaInsercion, G2997_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2997_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2997 WHERE G2997_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2997_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2997 WHERE G2997_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2993_ConsInte__b, G2993_C59737, G2993_C59738, G2993_C59739, G2993_C59740, G2993_C59741, G2969_C59266 as G2993_C59770, G2993_C59771, G2993_C59772, G2993_C59773, G2993_C59774, G2993_C59775, G2993_C59776, G2993_C59777, G2993_C59778 FROM ".$BaseDatos.".G2993  LEFT JOIN ".$BaseDatos.".G2969 ON G2969_ConsInte__b  =  G2993_C59770 ";

        $SQL .= " WHERE G2993_C59770 = '".$numero."'"; 

        $SQL .= " ORDER BY G2993_C59737";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2993_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2993_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2993_C59737."</cell>"; 

                if($fila->G2993_C59738 != ''){
                    echo "<cell>". explode(' ', $fila->G2993_C59738)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2993_C59739 != ''){
                    echo "<cell>". explode(' ', $fila->G2993_C59739)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2993_C59740 != ''){
                    echo "<cell>". explode(' ', $fila->G2993_C59740)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2993_C59741 != ''){
                    echo "<cell>". explode(' ', $fila->G2993_C59741)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2993_C59770)."</cell>";

                echo "<cell>". ($fila->G2993_C59771)."</cell>";

                echo "<cell>". ($fila->G2993_C59772)."</cell>";

                echo "<cell>". ($fila->G2993_C59773)."</cell>";

                echo "<cell>". ($fila->G2993_C59774)."</cell>";

                echo "<cell>". ($fila->G2993_C59775)."</cell>";

                echo "<cell>". ($fila->G2993_C59776)."</cell>";

                echo "<cell>". ($fila->G2993_C59777)."</cell>";

                echo "<cell>". ($fila->G2993_C59778)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2993 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2993(";
            $LsqlV = " VALUES ("; 
 
                $G2993_C59737= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2993_C59737"])){    
                    if($_POST["G2993_C59737"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2993_C59737 = $_POST["G2993_C59737"];
                        $LsqlU .= $separador." G2993_C59737 = '".$G2993_C59737."'";
                        $LsqlI .= $separador." G2993_C59737";
                        $LsqlV .= $separador."'".$G2993_C59737."'";
                        $validar = 1;
                    }
                }

                $G2993_C59738 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2993_C59738"])){    
                    if($_POST["G2993_C59738"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2993_C59738 = "'".str_replace(' ', '',$_POST["G2993_C59738"])." 00:00:00'";
                        $LsqlU .= $separador." G2993_C59738 = ".$G2993_C59738;
                        $LsqlI .= $separador." G2993_C59738";
                        $LsqlV .= $separador.$G2993_C59738;
                        $validar = 1;
                    }
                }
 
                $G2993_C59739 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2993_C59739"])){    
                    if($_POST["G2993_C59739"] != '' && $_POST["G2993_C59739"] != 'undefined' && $_POST["G2993_C59739"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2993_C59739 = "'".$fecha." ".str_replace(' ', '',$_POST["G2993_C59739"])."'";
                        $LsqlU .= $separador."  G2993_C59739 = ".$G2993_C59739."";
                        $LsqlI .= $separador."  G2993_C59739";
                        $LsqlV .= $separador.$G2993_C59739;
                        $validar = 1;
                    }
                }

                $G2993_C59740 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2993_C59740"])){    
                    if($_POST["G2993_C59740"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2993_C59740 = "'".str_replace(' ', '',$_POST["G2993_C59740"])." 00:00:00'";
                        $LsqlU .= $separador." G2993_C59740 = ".$G2993_C59740;
                        $LsqlI .= $separador." G2993_C59740";
                        $LsqlV .= $separador.$G2993_C59740;
                        $validar = 1;
                    }
                }
 
                $G2993_C59741 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2993_C59741"])){    
                    if($_POST["G2993_C59741"] != '' && $_POST["G2993_C59741"] != 'undefined' && $_POST["G2993_C59741"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2993_C59741 = "'".$fecha." ".str_replace(' ', '',$_POST["G2993_C59741"])."'";
                        $LsqlU .= $separador."  G2993_C59741 = ".$G2993_C59741."";
                        $LsqlI .= $separador."  G2993_C59741";
                        $LsqlV .= $separador.$G2993_C59741;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2993_C59771"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59771 = '".$_POST["G2993_C59771"]."'";
                    $LsqlI .= $separador."G2993_C59771";
                    $LsqlV .= $separador."'".$_POST["G2993_C59771"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59772"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59772 = '".$_POST["G2993_C59772"]."'";
                    $LsqlI .= $separador."G2993_C59772";
                    $LsqlV .= $separador."'".$_POST["G2993_C59772"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59773"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59773 = '".$_POST["G2993_C59773"]."'";
                    $LsqlI .= $separador."G2993_C59773";
                    $LsqlV .= $separador."'".$_POST["G2993_C59773"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59774"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59774 = '".$_POST["G2993_C59774"]."'";
                    $LsqlI .= $separador."G2993_C59774";
                    $LsqlV .= $separador."'".$_POST["G2993_C59774"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59775"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59775 = '".$_POST["G2993_C59775"]."'";
                    $LsqlI .= $separador."G2993_C59775";
                    $LsqlV .= $separador."'".$_POST["G2993_C59775"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59776"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59776 = '".$_POST["G2993_C59776"]."'";
                    $LsqlI .= $separador."G2993_C59776";
                    $LsqlV .= $separador."'".$_POST["G2993_C59776"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59777"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59777 = '".$_POST["G2993_C59777"]."'";
                    $LsqlI .= $separador."G2993_C59777";
                    $LsqlV .= $separador."'".$_POST["G2993_C59777"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2993_C59778"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2993_C59778 = '".$_POST["G2993_C59778"]."'";
                    $LsqlI .= $separador."G2993_C59778";
                    $LsqlV .= $separador."'".$_POST["G2993_C59778"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2993_C59770 = $numero;
                    $LsqlU .= ", G2993_C59770 = ".$G2993_C59770."";
                    $LsqlI .= ", G2993_C59770";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2993_Usuario ,  G2993_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2993_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2993 WHERE  G2993_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
