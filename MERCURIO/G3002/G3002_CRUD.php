<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3002_ConsInte__b, G3002_FechaInsercion , G3002_Usuario ,  G3002_CodigoMiembro  , G3002_PoblacionOrigen , G3002_EstadoDiligenciamiento ,  G3002_IdLlamada , G3002_C59926 as principal ,G3002_C59919,G3002_C59920,G3002_C59921,G3002_C59922,G3002_C59923,G3002_C59924,G3002_C59925,G3002_C59926,G3002_C59927,G3002_C60149,G3002_C59928,G3002_C60150,G3002_C60218,G3002_C59916,G3002_C59917,G3002_C59918,G3002_C60214,G3002_C60215,G3002_C60216,G3002_C60217,G3002_C60315,G3002_C60316,G3002_C60317 FROM '.$BaseDatos.'.G3002 WHERE G3002_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3002_C59919'] = explode(' ', $key->G3002_C59919)[0];
  
                $hora = '';
                if(!is_null($key->G3002_C59920)){
                    $hora = explode(' ', $key->G3002_C59920)[1];
                }

                $datos[$i]['G3002_C59920'] = $hora;

                $datos[$i]['G3002_C59921'] = explode(' ', $key->G3002_C59921)[0];
  
                $hora = '';
                if(!is_null($key->G3002_C59922)){
                    $hora = explode(' ', $key->G3002_C59922)[1];
                }

                $datos[$i]['G3002_C59922'] = $hora;

                $datos[$i]['G3002_C59923'] = $key->G3002_C59923;

                $datos[$i]['G3002_C59924'] = $key->G3002_C59924;

                $datos[$i]['G3002_C59925'] = $key->G3002_C59925;

                $datos[$i]['G3002_C59926'] = $key->G3002_C59926;

                $datos[$i]['G3002_C59927'] = $key->G3002_C59927;

                $datos[$i]['G3002_C60315'] = $key->G3002_C60315;

                $datos[$i]['G3002_C60316'] = $key->G3002_C60316;

                $datos[$i]['G3002_C60149'] = $key->G3002_C60149;

                $datos[$i]['G3002_C59928'] = $key->G3002_C59928;

                $datos[$i]['G3002_C60150'] = $key->G3002_C60150;

                $datos[$i]['G3002_C60218'] = $key->G3002_C60218;

                $datos[$i]['G3002_C59916'] = $key->G3002_C59916;

                $datos[$i]['G3002_C59917'] = $key->G3002_C59917;

                $datos[$i]['G3002_C59918'] = $key->G3002_C59918;

                $datos[$i]['G3002_C60214'] = $key->G3002_C60214;
                $datos[$i]['G3002_C60317'] = $key->G3002_C60317;

                $datos[$i]['G3002_C60215'] = $key->G3002_C60215;

                $datos[$i]['G3002_C60216'] = $key->G3002_C60216;

                $datos[$i]['G3002_C60217'] = $key->G3002_C60217;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3002";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3002_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3002_ConsInte__b as id,  G3002_C59923 as camp2 , G3002_C59926 as camp1 
                     FROM ".$BaseDatos.".G3002  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3002_ConsInte__b as id,  G3002_C59923 as camp2 , G3002_C59926 as camp1  
                    FROM ".$BaseDatos.".G3002  JOIN ".$BaseDatos.".G3002_M".$_POST['muestra']." ON G3002_ConsInte__b = G3002_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3002_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3002_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3002_C59923 LIKE '%".$B."%' OR G3002_C59926 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3002_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

                            if(isset($_GET['CallDatosCombo_Guion_G3002_C60315'])){
                                $Ysql = "SELECT G3021_ConsInte__b as id, G3021_C60307 as text FROM ".$BaseDatos.".G3021 WHERE G3021_C60307 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3002_C60315"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3021 WHERE G3021_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3002_C60315"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3002");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3002_ConsInte__b, G3002_FechaInsercion , G3002_Usuario ,  G3002_CodigoMiembro  , G3002_PoblacionOrigen , G3002_EstadoDiligenciamiento ,  G3002_IdLlamada , G3002_C59926 as principal ,G3002_C59919,G3002_C59920,G3002_C59921,G3002_C59922,G3002_C59923,G3002_C59924,G3002_C59925,G3002_C59926,G3002_C59927,G3002_C60149, a.LISOPC_Nombre____b as G3002_C59928, b.LISOPC_Nombre____b as G3002_C60150,G3002_C60218,G3002_C59916,G3002_C59917, c.LISOPC_Nombre____b as G3002_C59918, d.LISOPC_Nombre____b as G3002_C60214,G3002_C60215,G3002_C60216,G3002_C60217 FROM '.$BaseDatos.'.G3002 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3002_C59928 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3002_C60150 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3002_C59918 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3002_C60214';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3002_C59920)){
                    $hora_a = explode(' ', $fila->G3002_C59920)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3002_C59922)){
                    $hora_b = explode(' ', $fila->G3002_C59922)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3002_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3002_ConsInte__b , explode(' ', $fila->G3002_C59919)[0] , $hora_a , explode(' ', $fila->G3002_C59921)[0] , $hora_b , ($fila->G3002_C59923) , ($fila->G3002_C59924) , ($fila->G3002_C59925) , ($fila->G3002_C59926) , ($fila->G3002_C59927) , ($fila->G3002_C60149) , ($fila->G3002_C59928) , ($fila->G3002_C60150) , ($fila->G3002_C60218) , ($fila->G3002_C59916) , ($fila->G3002_C59917) , ($fila->G3002_C59918) , ($fila->G3002_C60214) , ($fila->G3002_C60215) , ($fila->G3002_C60216) , ($fila->G3002_C60217) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3002 WHERE G3002_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3002";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3002_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3002_ConsInte__b as id,  G3002_C59923 as camp2 , G3002_C59926 as camp1  FROM '.$BaseDatos.'.G3002 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3002_ConsInte__b as id,  G3002_C59923 as camp2 , G3002_C59926 as camp1  
                    FROM ".$BaseDatos.".G3002  JOIN ".$BaseDatos.".G3002_M".$_POST['muestra']." ON G3002_ConsInte__b = G3002_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3002_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3002_C59923 LIKE "%'.$B.'%" OR G3002_C59926 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3002_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3002 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3002(";
            $LsqlV = " VALUES ("; 
 
            $G3002_C59919 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3002_C59919"])){    
                if($_POST["G3002_C59919"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3002_C59919"]);
                    if(count($tieneHora) > 1){
                        $G3002_C59919 = "'".$_POST["G3002_C59919"]."'";
                    }else{
                        $G3002_C59919 = "'".str_replace(' ', '',$_POST["G3002_C59919"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3002_C59919 = ".$G3002_C59919;
                    $LsqlI .= $separador." G3002_C59919";
                    $LsqlV .= $separador.$G3002_C59919;
                    $validar = 1;
                }
            }
  
            $G3002_C59920 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3002_C59920"])){   
                if($_POST["G3002_C59920"] != '' && $_POST["G3002_C59920"] != 'undefined' && $_POST["G3002_C59920"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3002_C59920 = "'".$fecha." ".str_replace(' ', '',$_POST["G3002_C59920"])."'";
                    $LsqlU .= $separador." G3002_C59920 = ".$G3002_C59920."";
                    $LsqlI .= $separador." G3002_C59920";
                    $LsqlV .= $separador.$G3002_C59920;
                    $validar = 1;
                }
            }
 
            $G3002_C59921 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3002_C59921"])){    
                if($_POST["G3002_C59921"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3002_C59921"]);
                    if(count($tieneHora) > 1){
                        $G3002_C59921 = "'".$_POST["G3002_C59921"]."'";
                    }else{
                        $G3002_C59921 = "'".str_replace(' ', '',$_POST["G3002_C59921"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3002_C59921 = ".$G3002_C59921;
                    $LsqlI .= $separador." G3002_C59921";
                    $LsqlV .= $separador.$G3002_C59921;
                    $validar = 1;
                }
            }
  
            $G3002_C59922 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3002_C59922"])){   
                if($_POST["G3002_C59922"] != '' && $_POST["G3002_C59922"] != 'undefined' && $_POST["G3002_C59922"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3002_C59922 = "'".$fecha." ".str_replace(' ', '',$_POST["G3002_C59922"])."'";
                    $LsqlU .= $separador." G3002_C59922 = ".$G3002_C59922."";
                    $LsqlI .= $separador." G3002_C59922";
                    $LsqlV .= $separador.$G3002_C59922;
                    $validar = 1;
                }
            }
  
            $G3002_C59923 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3002_C59923"])){
                if($_POST["G3002_C59923"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3002_C59923 = $_POST["G3002_C59923"];
                    $LsqlU .= $separador." G3002_C59923 = ".$G3002_C59923."";
                    $LsqlI .= $separador." G3002_C59923";
                    $LsqlV .= $separador.$G3002_C59923;
                    $validar = 1;
                }
            }
  
            $G3002_C59924 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3002_C59924"])){
                if($_POST["G3002_C59924"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3002_C59924 = $_POST["G3002_C59924"];
                    $LsqlU .= $separador." G3002_C59924 = ".$G3002_C59924."";
                    $LsqlI .= $separador." G3002_C59924";
                    $LsqlV .= $separador.$G3002_C59924;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3002_C59925"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59925 = '".$_POST["G3002_C59925"]."'";
                $LsqlI .= $separador."G3002_C59925";
                $LsqlV .= $separador."'".$_POST["G3002_C59925"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60315 = '".$_POST["G3002_C60315"]."'";
                $LsqlI .= $separador."G3002_C60315";
                $LsqlV .= $separador."'".$_POST["G3002_C60315"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60316 = '".$_POST["G3002_C60316"]."'";
                $LsqlI .= $separador."G3002_C60316";
                $LsqlV .= $separador."'".$_POST["G3002_C60316"]."'";
                $validar = 1;
            }

            if(isset($_POST["G3002_C59926"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59926 = '".$_POST["G3002_C59926"]."'";
                $LsqlI .= $separador."G3002_C59926";
                $LsqlV .= $separador."'".$_POST["G3002_C59926"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C59927"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59927 = '".$_POST["G3002_C59927"]."'";
                $LsqlI .= $separador."G3002_C59927";
                $LsqlV .= $separador."'".$_POST["G3002_C59927"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60149"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60149 = '".$_POST["G3002_C60149"]."'";
                $LsqlI .= $separador."G3002_C60149";
                $LsqlV .= $separador."'".$_POST["G3002_C60149"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C59928"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59928 = '".$_POST["G3002_C59928"]."'";
                $LsqlI .= $separador."G3002_C59928";
                $LsqlV .= $separador."'".$_POST["G3002_C59928"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60150"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60150 = '".$_POST["G3002_C60150"]."'";
                $LsqlI .= $separador."G3002_C60150";
                $LsqlV .= $separador."'".$_POST["G3002_C60150"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60218"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60218 = '".$_POST["G3002_C60218"]."'";
                $LsqlI .= $separador."G3002_C60218";
                $LsqlV .= $separador."'".$_POST["G3002_C60218"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C59916"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59916 = '".$_POST["G3002_C59916"]."'";
                $LsqlI .= $separador."G3002_C59916";
                $LsqlV .= $separador."'".$_POST["G3002_C59916"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C59917"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59917 = '".$_POST["G3002_C59917"]."'";
                $LsqlI .= $separador."G3002_C59917";
                $LsqlV .= $separador."'".$_POST["G3002_C59917"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C59918"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C59918 = '".$_POST["G3002_C59918"]."'";
                $LsqlI .= $separador."G3002_C59918";
                $LsqlV .= $separador."'".$_POST["G3002_C59918"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60214"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60214 = '".$_POST["G3002_C60214"]."'";
                $LsqlI .= $separador."G3002_C60214";
                $LsqlV .= $separador."'".$_POST["G3002_C60214"]."'";
                $validar = 1;
            }
             

            if(isset($_POST["G3002_C60317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60317 = '".$_POST["G3002_C60317"]."'";
                $LsqlI .= $separador."G3002_C60317";
                $LsqlV .= $separador."'".$_POST["G3002_C60317"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60215"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60215 = '".$_POST["G3002_C60215"]."'";
                $LsqlI .= $separador."G3002_C60215";
                $LsqlV .= $separador."'".$_POST["G3002_C60215"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60216"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60216 = '".$_POST["G3002_C60216"]."'";
                $LsqlI .= $separador."G3002_C60216";
                $LsqlV .= $separador."'".$_POST["G3002_C60216"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3002_C60217"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_C60217 = '".$_POST["G3002_C60217"]."'";
                $LsqlI .= $separador."G3002_C60217";
                $LsqlV .= $separador."'".$_POST["G3002_C60217"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3002_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3002_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3002_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3002_Usuario , G3002_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3002_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3002 WHERE G3002_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3002 SET G3002_UltiGest__b =-14, G3002_GesMasImp_b =-14, G3002_TipoReintentoUG_b =0, G3002_TipoReintentoGMI_b =0, G3002_EstadoUG_b =-14, G3002_EstadoGMI_b =-14, G3002_CantidadIntentos =0, G3002_CantidadIntentosGMI_b =0 WHERE G3002_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
