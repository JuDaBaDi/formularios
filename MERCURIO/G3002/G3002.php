
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3002/G3002_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3002_ConsInte__b as id, G3002_C59923 as camp2 , G3002_C59926 as camp1 FROM ".$BaseDatos.".G3002  WHERE G3002_Usuario = ".$idUsuario." ORDER BY G3002_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3002_ConsInte__b as id, G3002_C59923 as camp2 , G3002_C59926 as camp1 FROM ".$BaseDatos.".G3002  ORDER BY G3002_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3002_ConsInte__b as id, G3002_C59923 as camp2 , G3002_C59926 as camp1 FROM ".$BaseDatos.".G3002 JOIN ".$BaseDatos.".G3002_M".$resultEstpas->muestr." ON G3002_ConsInte__b = G3002_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3002_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3002_ConsInte__b as id, G3002_C59923 as camp2 , G3002_C59926 as camp1 FROM ".$BaseDatos.".G3002 JOIN ".$BaseDatos.".G3002_M".$resultEstpas->muestr." ON G3002_ConsInte__b = G3002_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3002_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3002_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3002_ConsInte__b as id, G3002_C59923 as camp2 , G3002_C59926 as camp1 FROM ".$BaseDatos.".G3002  ORDER BY G3002_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="9165" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9165c">
                DATOS BASICOS DEL CASO 
            </a>
        </h4>
        
    </div>
    <div id="s_9165c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3002_C59919" id="LblG3002_C59919">FECHA INICIO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3002_C59919'])) {
                            echo $_GET['G3002_C59919'];
                        } ?>" readonly name="G3002_C59919" id="G3002_C59919" placeholder="YYYY-MM-DD" nombre="FECHA INICIO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3002_C59920" id="LblG3002_C59920">HORA INICIO</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" readonly name="G3002_C59920" id="G3002_C59920" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3002_C59920">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3002_C59921" id="LblG3002_C59921">FECHA FINALIZACION </label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3002_C59921'])) {
                            echo $_GET['G3002_C59921'];
                        } ?>" readonly name="G3002_C59921" id="G3002_C59921" placeholder="YYYY-MM-DD" nombre="FECHA FINALIZACION ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3002_C59922" id="LblG3002_C59922">HORA FINALIZACION </label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" readonly name="G3002_C59922" id="G3002_C59922" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3002_C59922">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3002_C59923" id="LblG3002_C59923">ID</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3002_C59923'])) {
                            echo $_GET['G3002_C59923'];
                        } ?>" readonly name="G3002_C59923" id="G3002_C59923" placeholder="ID">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3002_C59924" id="LblG3002_C59924">CóDIGO CONSULTOR</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3002_C59924'])) {
                            echo $_GET['G3002_C59924'];
                        } ?>" readonly name="G3002_C59924" id="G3002_C59924" placeholder="CóDIGO CONSULTOR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3002_C60315" id="LblG3002_C60315">CIUDAD</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3002_C60315" id="G3002_C60315">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C60316" id="LblG3002_C60316">DEPARTAMENTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C60316" value="<?php if (isset($_GET['G3002_C60316'])) {
                            echo $_GET['G3002_C60316'];
                        } ?>"  name="G3002_C60316"  placeholder="DEPARTAMENTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C59925" id="LblG3002_C59925">TELÉFONO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C59925" value="<?php if (isset($_GET['G3002_C59925'])) {
                            echo $_GET['G3002_C59925'];
                        } ?>" readonly name="G3002_C59925"  placeholder="TELÉFONO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C60317" id="LblG3002_C60317">GERENCIA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C60317" value="<?php if (isset($_GET['G3002_C60317'])) {
                            echo $_GET['G3002_C60317'];
                        } ?>" readonly name="G3002_C60317"  placeholder="GERENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C59926" id="LblG3002_C59926">NOMBRES</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C59926" value="<?php if (isset($_GET['G3002_C59926'])) {
                            echo $_GET['G3002_C59926'];
                        } ?>" readonly name="G3002_C59926"  placeholder="NOMBRES"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C59927" id="LblG3002_C59927">APELLIDOS</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C59927" value="<?php if (isset($_GET['G3002_C59927'])) {
                            echo $_GET['G3002_C59927'];
                        } ?>" readonly name="G3002_C59927"  placeholder="APELLIDOS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C60149" id="LblG3002_C60149">CAMPOS A ACTUALIZAR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C60149" value="<?php if (isset($_GET['G3002_C60149'])) {
                            echo $_GET['G3002_C60149'];
                        } ?>" readonly name="G3002_C60149"  placeholder="CAMPOS A ACTUALIZAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3002_C59928" id="LblG3002_C59928">ESTADO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3002_C59928" id="G3002_C59928">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3649 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3002_C60150" id="LblG3002_C60150">MOTIVO NO EFECTIVO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3002_C60150" id="G3002_C60150" disabled>
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3674 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3002_C60218" id="LblG3002_C60218">OBSERVACIONES</label>
                        <textarea class="form-control input-sm" name="G3002_C60218" id="G3002_C60218"  value="<?php if (isset($_GET['G3002_C60218'])) {
                            echo $_GET['G3002_C60218'];
                        } ?>" placeholder="OBSERVACIONES"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9166" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9166c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9166c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C59916" id="LblG3002_C59916">ORIGEN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C59916" value="<?php if (isset($_GET['G3002_C59916'])) {
                            echo $_GET['G3002_C59916'];
                        } ?>" readonly name="G3002_C59916"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C59917" id="LblG3002_C59917">OPTIN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C59917" value="<?php if (isset($_GET['G3002_C59917'])) {
                            echo $_GET['G3002_C59917'];
                        } ?>" readonly name="G3002_C59917"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3002_C59918" id="LblG3002_C59918">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3002_C59918" id="G3002_C59918">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3648 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9214" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9214c">
                ENVIO DE CORREOS
            </a>
        </h4>
        
    </div>
    <div id="s_9214c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3002_C60214" id="LblG3002_C60214">ENVIAR CORREO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3002_C60214" id="G3002_C60214">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3692 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==45305){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C60215" id="LblG3002_C60215">PARA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C60215" value="<?php if (isset($_GET['G3002_C60215'])) {
                            echo $_GET['G3002_C60215'];
                        } ?>"  name="G3002_C60215"  placeholder="PARA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3002_C60216" id="LblG3002_C60216">ASUNTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3002_C60216" value="<?php if (isset($_GET['G3002_C60216'])) {
                            echo $_GET['G3002_C60216'];
                        } ?>"  name="G3002_C60216"  placeholder="ASUNTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3002_C60217" id="LblG3002_C60217">CUERPO DEL CORREO</label>
                        <textarea class="form-control input-sm" name="G3002_C60217" id="G3002_C60217"  value="<?php if (isset($_GET['G3002_C60217'])) {
                            echo $_GET['G3002_C60217'];
                        } ?>" placeholder="CUERPO DEL CORREO"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G3002/G3002_eventos.js"></script>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3002_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3002_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3002_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3002_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3002_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3002_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3002_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3002_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3002_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            $("#G3002_C59928").val("0").trigger("change");
            $("#G3002_C60150").val("0").trigger("change");
            $("#G3002_C60214").val("45305").trigger("change");
            $("#G3002_C60150").prop('disabled', true);
        });
    <?php } ?>;
    let arr;
    <?php if(isset($_GET['id_campana_cbx']) && $_GET['id_campana_cbx'] !="" && $_GET['id_campana_cbx'] !=0) :?>
        <?php
        $intHuesped=$mysqli->query("SELECT CAMPAN_ConsInte__PROYEC_b FROM DYALOGOCRM_SISTEMA.CAMPAN WHERE CAMPAN_IdCamCbx__b={$_GET['id_campana_cbx']}");
        if($intHuesped && $intHuesped->num_rows==1){
            $intHuesped=$intHuesped->fetch_object();
            $intHuesped=$intHuesped->CAMPAN_ConsInte__PROYEC_b;
            $patron=ObtenerPatron($intHuesped,$_GET['id_campana_crm']);
        } 
        ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php endif; ?>
    <?php if(isset($_SESSION['HUESPED_CRM']) && $_SESSION['HUESPED_CRM']!=0 && $_SESSION['HUESPED_CRM'] !="") {
        $patron=ObtenerPatron($_SESSION['HUESPED_CRM']); ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php } ?>
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G3002_C59919").val(item.G3002_C59919); 
                $("#G3002_C59920").val(item.G3002_C59920); 
                $("#G3002_C59921").val(item.G3002_C59921); 
                $("#G3002_C59922").val(item.G3002_C59922); 
                $("#G3002_C59923").val(item.G3002_C59923); 
                $("#G3002_C59924").val(item.G3002_C59924); 
                $("#G3002_C59925").val(item.G3002_C59925); 
                $("#G3002_C60317").val(item.G3002_C60317); 
                $("#G3002_C60315").attr("opt",item.G3002_C60315);
                $("#G3002_C60315").val(item.G3002_C60315).trigger("change"); 
                $("#G3002_C60316").val(item.G3002_C60316); 
                $("#G3002_C59926").val(item.G3002_C59926); 
                $("#G3002_C59927").val(item.G3002_C59927); 
                $("#G3002_C60149").val(item.G3002_C60149); 
                $("#G3002_C59928").val(item.G3002_C59928).trigger("change");  
                $("#G3002_C60150").val(item.G3002_C60150).trigger("change");  
                $("#G3002_C60218").val(item.G3002_C60218); 
                $("#G3002_C59916").val(item.G3002_C59916); 
                $("#G3002_C59917").val(item.G3002_C59917); 
                $("#G3002_C59918").val(item.G3002_C59918).trigger("change");  
                $("#G3002_C60214").val(item.G3002_C60214).trigger("change");  
                $("#G3002_C60215").val(item.G3002_C60215); 
                $("#G3002_C60216").val(item.G3002_C60216); 
                $("#G3002_C60217").val(item.G3002_C60217);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        
                            $("#G3002_C60315").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3002_C60315=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G3002_C60315").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3002_C60315 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3002_C60315").html('<option value="'+data.G3021_ConsInte__b+'" >'+data.G3021_C60307+'</option>');
                                        
                                $("#G3002_C60316").val(data.G3021_C60308);
                                    }
                                });
                            });

    $("#G3002_C59928").select2();

    $("#G3002_C60150").select2();

    $("#G3002_C59918").select2();

    $("#G3002_C60214").select2();
        //datepickers
        

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G3002_C59923").numeric();
                
        $("#G3002_C59924").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO 

    $("#G3002_C59928").change(function(){ 
        $("#G3002_C60150").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() != '44977'){
            $("#G3002_C60150").closest(".form-group").removeClass("has-error"); 
            $("#G3002_C60150").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3002_C60150").val("")
        }
     
        //Esto es la parte de las listas dependientes
        

    });

    //function para MOTIVO NO EFECTIVO 

    $("#G3002_C60150").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G3002_C59918").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ENVIAR CORREO 

    $("#G3002_C60214").change(function(){ 
        $("#G3002_C60215").prop('disabled', false);
    
        $("#G3002_C60216").prop('disabled', false);
    
        $("#G3002_C60217").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '45305'){
            $("#G3002_C60215").closest(".form-group").removeClass("has-error"); 
            $("#G3002_C60215").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3002_C60215").val("")
            $("#G3002_C60216").closest(".form-group").removeClass("has-error"); 
            $("#G3002_C60216").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3002_C60216").val("")
            $("#G3002_C60217").closest(".form-group").removeClass("has-error"); 
            $("#G3002_C60217").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3002_C60217").val("")
        }
     
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G3002_C59928").val()==0 || $("#G3002_C59928").val() == null || $("#G3002_C59928").val() == -1) && $("#G3002_C59928").prop("disabled") == false){
                alertify.error('ESTADO debe ser diligenciado');
                $("#G3002_C59928").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            let booValido=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido = 1;
                        alertify.error("El número de teléfono digitado no es valido");
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });            

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){
                    $("#G3002_C59921").val('<?php echo date('Y-m-d');?>');
                    $("#G3002_C59922").val(horas +':'+d.getMinutes()+':'+d.getSeconds());
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3002_C59919").val(item.G3002_C59919);
 
                                                $("#G3002_C59920").val(item.G3002_C59920);
 
                                                $("#G3002_C59921").val(item.G3002_C59921);
 
                                                $("#G3002_C59922").val(item.G3002_C59922);
 
                                                $("#G3002_C59923").val(item.G3002_C59923);
 
                                                $("#G3002_C59924").val(item.G3002_C59924);
 
                                                $("#G3002_C59925").val(item.G3002_C59925);
                                                $("#G3002_C60317").val(item.G3002_C60317);
                 $("#G3002_C60315").attr("opt",item.G3002_C60315);
                $("#G3002_C60315").val(item.G3002_C60315).trigger("change"); 
                $("#G3002_C60316").val(item.G3002_C60316); 
                                                $("#G3002_C59926").val(item.G3002_C59926);
 
                                                $("#G3002_C59927").val(item.G3002_C59927);
 
                                                $("#G3002_C60149").val(item.G3002_C60149);
 
                    $("#G3002_C59928").val(item.G3002_C59928).trigger("change"); 
 
                    $("#G3002_C60150").val(item.G3002_C60150).trigger("change"); 
 
                                                $("#G3002_C60218").val(item.G3002_C60218);
 
                                                $("#G3002_C59916").val(item.G3002_C59916);
 
                                                $("#G3002_C59917").val(item.G3002_C59917);
 
                    $("#G3002_C59918").val(item.G3002_C59918).trigger("change"); 
 
                    $("#G3002_C60214").val(item.G3002_C60214).trigger("change"); 
 
                                                $("#G3002_C60215").val(item.G3002_C60215);
 
                                                $("#G3002_C60216").val(item.G3002_C60216);
 
                                                $("#G3002_C60217").val(item.G3002_C60217);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','FECHA INICIO','HORA INICIO','FECHA FINALIZACION ','HORA FINALIZACION ','ID','CóDIGO CONSULTOR','TELÉFONO','NOMBRES','APELLIDOS','CAMPOS A ACTUALIZAR','ESTADO','MOTIVO NO EFECTIVO','OBSERVACIONES','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','ENVIAR CORREO','PARA','ASUNTO','CUERPO DEL CORREO'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {  
                        name:'G3002_C59919', 
                        index:'G3002_C59919', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3002_C59920', 
                        index:'G3002_C59920', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA INICIO', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3002_C59921', 
                        index:'G3002_C59921', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3002_C59922', 
                        index:'G3002_C59922', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA FINALIZACION ', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G3002_C59923', 
                        index:'G3002_C59923', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3002_C59924', 
                        index:'G3002_C59924', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3002_C59925', 
                        index: 'G3002_C59925', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C59926', 
                        index: 'G3002_C59926', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C59927', 
                        index: 'G3002_C59927', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C60149', 
                        index: 'G3002_C60149', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C59928', 
                        index:'G3002_C59928', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3649&campo=G3002_C59928'
                        }
                    }

                    ,
                    { 
                        name:'G3002_C60150', 
                        index:'G3002_C60150', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3674&campo=G3002_C60150'
                        }
                    }

                    ,
                    { 
                        name:'G3002_C60218', 
                        index:'G3002_C60218', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C59916', 
                        index: 'G3002_C59916', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C59917', 
                        index: 'G3002_C59917', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C59918', 
                        index:'G3002_C59918', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3648&campo=G3002_C59918'
                        }
                    }

                    ,
                    { 
                        name:'G3002_C60214', 
                        index:'G3002_C60214', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3692&campo=G3002_C60214'
                        }
                    }

                    ,
                    { 
                        name:'G3002_C60215', 
                        index: 'G3002_C60215', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C60216', 
                        index: 'G3002_C60216', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3002_C60217', 
                        index:'G3002_C60217', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3002_C59926',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G3002_C59919").val(item.G3002_C59919);

                        $("#G3002_C59920").val(item.G3002_C59920);

                        $("#G3002_C59921").val(item.G3002_C59921);

                        $("#G3002_C59922").val(item.G3002_C59922);

                        $("#G3002_C59923").val(item.G3002_C59923);

                        $("#G3002_C59924").val(item.G3002_C59924);

                        $("#G3002_C59925").val(item.G3002_C59925);
                        $("#G3002_C60317").val(item.G3002_C60317);
                $("#G3002_C60315").attr("opt",item.G3002_C60315);
                $("#G3002_C60315").val(item.G3002_C60315).trigger("change"); 
                $("#G3002_C60316").val(item.G3002_C60316); 
                        $("#G3002_C59926").val(item.G3002_C59926);

                        $("#G3002_C59927").val(item.G3002_C59927);

                        $("#G3002_C60149").val(item.G3002_C60149);
 
                    $("#G3002_C59928").val(item.G3002_C59928).trigger("change"); 
 
                    $("#G3002_C60150").val(item.G3002_C60150).trigger("change"); 

                        $("#G3002_C60218").val(item.G3002_C60218);

                        $("#G3002_C59916").val(item.G3002_C59916);

                        $("#G3002_C59917").val(item.G3002_C59917);
 
                    $("#G3002_C59918").val(item.G3002_C59918).trigger("change"); 
 
                    $("#G3002_C60214").val(item.G3002_C60214).trigger("change"); 

                        $("#G3002_C60215").val(item.G3002_C60215);

                        $("#G3002_C60216").val(item.G3002_C60216);

                        $("#G3002_C60217").val(item.G3002_C60217);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
