<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3046_ConsInte__b, G3046_FechaInsercion , G3046_Usuario ,  G3046_CodigoMiembro  , G3046_PoblacionOrigen , G3046_EstadoDiligenciamiento ,  G3046_IdLlamada , G3046_C61071 as principal ,G3046_C61070,G3046_C61071,G3046_C61073,G3046_C62954,G3046_C62955,G3046_C63064,G3046_C63065,G3046_C63066,G3046_C61059,G3046_C61060,G3046_C61061,G3046_C61062,G3046_C61063,G3046_C61064,G3046_C61065,G3046_C61066,G3046_C61067,G3046_C61105,G3046_C61106,G3046_C61107,G3046_C61108,G3046_C61109,G3046_C61110,G3046_C61111,G3046_C61112 FROM '.$BaseDatos.'.G3046 WHERE G3046_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3046_C61070'] = $key->G3046_C61070;

                $datos[$i]['G3046_C61071'] = $key->G3046_C61071;

                $datos[$i]['G3046_C61073'] = $key->G3046_C61073;

                $datos[$i]['G3046_C62954'] = $key->G3046_C62954;

                $datos[$i]['G3046_C62955'] = $key->G3046_C62955;

                $datos[$i]['G3046_C63064'] = $key->G3046_C63064;

                $datos[$i]['G3046_C63065'] = $key->G3046_C63065;

                $datos[$i]['G3046_C63066'] = $key->G3046_C63066;

                $datos[$i]['G3046_C61059'] = $key->G3046_C61059;

                $datos[$i]['G3046_C61060'] = $key->G3046_C61060;

                $datos[$i]['G3046_C61061'] = explode(' ', $key->G3046_C61061)[0];
  
                $hora = '';
                if(!is_null($key->G3046_C61062)){
                    $hora = explode(' ', $key->G3046_C61062)[1];
                }

                $datos[$i]['G3046_C61062'] = $hora;

                $datos[$i]['G3046_C61063'] = $key->G3046_C61063;

                $datos[$i]['G3046_C61064'] = $key->G3046_C61064;

                $datos[$i]['G3046_C61065'] = $key->G3046_C61065;

                $datos[$i]['G3046_C61066'] = $key->G3046_C61066;

                $datos[$i]['G3046_C61067'] = $key->G3046_C61067;

                $datos[$i]['G3046_C61105'] = $key->G3046_C61105;

                $datos[$i]['G3046_C61106'] = $key->G3046_C61106;

                $datos[$i]['G3046_C61107'] = explode(' ', $key->G3046_C61107)[0];

                $datos[$i]['G3046_C61108'] = $key->G3046_C61108;

                $datos[$i]['G3046_C61109'] = $key->G3046_C61109;

                $datos[$i]['G3046_C61110'] = $key->G3046_C61110;

                $datos[$i]['G3046_C61111'] = $key->G3046_C61111;

                $datos[$i]['G3046_C61112'] = $key->G3046_C61112;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3046";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3046_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3046_ConsInte__b as id,  G3046_C61071 as camp1 , G3046_C61073 as camp2 
                     FROM ".$BaseDatos.".G3046  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3046_ConsInte__b as id,  G3046_C61071 as camp1 , G3046_C61073 as camp2  
                    FROM ".$BaseDatos.".G3046  JOIN ".$BaseDatos.".G3046_M".$_POST['muestra']." ON G3046_ConsInte__b = G3046_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3046_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3046_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3046_C61071 LIKE '%".$B."%' OR G3046_C61073 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3046_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3046");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3046_ConsInte__b, G3046_FechaInsercion , G3046_Usuario ,  G3046_CodigoMiembro  , G3046_PoblacionOrigen , G3046_EstadoDiligenciamiento ,  G3046_IdLlamada , G3046_C61071 as principal ,G3046_C61070,G3046_C61071,G3046_C61073,G3046_C62954,G3046_C62955,G3046_C63064,G3046_C63065,G3046_C63066, a.LISOPC_Nombre____b as G3046_C61059, b.LISOPC_Nombre____b as G3046_C61060,G3046_C61061,G3046_C61062,G3046_C61063,G3046_C61064,G3046_C61065,G3046_C61066,G3046_C61067, c.LISOPC_Nombre____b as G3046_C61105,G3046_C61106,G3046_C61107, d.LISOPC_Nombre____b as G3046_C61108, e.LISOPC_Nombre____b as G3046_C61109,G3046_C61110, f.LISOPC_Nombre____b as G3046_C61111,G3046_C61112 FROM '.$BaseDatos.'.G3046 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3046_C61059 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3046_C61060 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3046_C61105 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3046_C61108 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3046_C61109 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3046_C61111';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3046_C61062)){
                    $hora_a = explode(' ', $fila->G3046_C61062)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3046_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3046_ConsInte__b , ($fila->G3046_C61070) , ($fila->G3046_C61071) , ($fila->G3046_C61073) , ($fila->G3046_C62954) , ($fila->G3046_C62955) , ($fila->G3046_C63064) , ($fila->G3046_C63065) , ($fila->G3046_C63066) , ($fila->G3046_C61059) , ($fila->G3046_C61060) , explode(' ', $fila->G3046_C61061)[0] , $hora_a , ($fila->G3046_C61063) , ($fila->G3046_C61064) , ($fila->G3046_C61065) , ($fila->G3046_C61066) , ($fila->G3046_C61067) , ($fila->G3046_C61105) , ($fila->G3046_C61106) , explode(' ', $fila->G3046_C61107)[0] , ($fila->G3046_C61108) , ($fila->G3046_C61109) , ($fila->G3046_C61110) , ($fila->G3046_C61111) , ($fila->G3046_C61112) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3046 WHERE G3046_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3046";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3046_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3046_ConsInte__b as id,  G3046_C61071 as camp1 , G3046_C61073 as camp2  FROM '.$BaseDatos.'.G3046 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3046_ConsInte__b as id,  G3046_C61071 as camp1 , G3046_C61073 as camp2  
                    FROM ".$BaseDatos.".G3046  JOIN ".$BaseDatos.".G3046_M".$_POST['muestra']." ON G3046_ConsInte__b = G3046_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3046_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3046_C61071 LIKE "%'.$B.'%" OR G3046_C61073 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3046_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3046 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3046(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3046_C61070"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61070 = '".$_POST["G3046_C61070"]."'";
                $LsqlI .= $separador."G3046_C61070";
                $LsqlV .= $separador."'".$_POST["G3046_C61070"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61071"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61071 = '".$_POST["G3046_C61071"]."'";
                $LsqlI .= $separador."G3046_C61071";
                $LsqlV .= $separador."'".$_POST["G3046_C61071"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61073"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61073 = '".$_POST["G3046_C61073"]."'";
                $LsqlI .= $separador."G3046_C61073";
                $LsqlV .= $separador."'".$_POST["G3046_C61073"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C62954"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C62954 = '".$_POST["G3046_C62954"]."'";
                $LsqlI .= $separador."G3046_C62954";
                $LsqlV .= $separador."'".$_POST["G3046_C62954"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C62955"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C62955 = '".$_POST["G3046_C62955"]."'";
                $LsqlI .= $separador."G3046_C62955";
                $LsqlV .= $separador."'".$_POST["G3046_C62955"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C63064"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C63064 = '".$_POST["G3046_C63064"]."'";
                $LsqlI .= $separador."G3046_C63064";
                $LsqlV .= $separador."'".$_POST["G3046_C63064"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C63065"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C63065 = '".$_POST["G3046_C63065"]."'";
                $LsqlI .= $separador."G3046_C63065";
                $LsqlV .= $separador."'".$_POST["G3046_C63065"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C63066"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C63066 = '".$_POST["G3046_C63066"]."'";
                $LsqlI .= $separador."G3046_C63066";
                $LsqlV .= $separador."'".$_POST["G3046_C63066"]."'";
                $validar = 1;
            }
             
 
            $G3046_C61059 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3046_C61059 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G3046_C61059 = ".$G3046_C61059;
                    $LsqlI .= $separador." G3046_C61059";
                    $LsqlV .= $separador.$G3046_C61059;
                    $validar = 1;

                    
                }
            }
 
            $G3046_C61060 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3046_C61060 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G3046_C61060 = ".$G3046_C61060;
                    $LsqlI .= $separador." G3046_C61060";
                    $LsqlV .= $separador.$G3046_C61060;
                    $validar = 1;
                }
            }
 
            $G3046_C61061 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3046_C61061 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G3046_C61061 = ".$G3046_C61061;
                    $LsqlI .= $separador." G3046_C61061";
                    $LsqlV .= $separador.$G3046_C61061;
                    $validar = 1;
                }
            }
 
            $G3046_C61062 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3046_C61062 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G3046_C61062 = ".$G3046_C61062;
                    $LsqlI .= $separador." G3046_C61062";
                    $LsqlV .= $separador.$G3046_C61062;
                    $validar = 1;
                }
            }
 
            $G3046_C61063 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3046_C61063 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G3046_C61063 = ".$G3046_C61063;
                    $LsqlI .= $separador." G3046_C61063";
                    $LsqlV .= $separador.$G3046_C61063;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3046_C61064"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61064 = '".$_POST["G3046_C61064"]."'";
                $LsqlI .= $separador."G3046_C61064";
                $LsqlV .= $separador."'".$_POST["G3046_C61064"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61065"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61065 = '".$_POST["G3046_C61065"]."'";
                $LsqlI .= $separador."G3046_C61065";
                $LsqlV .= $separador."'".$_POST["G3046_C61065"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61066"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61066 = '".$_POST["G3046_C61066"]."'";
                $LsqlI .= $separador."G3046_C61066";
                $LsqlV .= $separador."'".$_POST["G3046_C61066"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61067"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61067 = '".$_POST["G3046_C61067"]."'";
                $LsqlI .= $separador."G3046_C61067";
                $LsqlV .= $separador."'".$_POST["G3046_C61067"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61068"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61068 = '".$_POST["G3046_C61068"]."'";
                $LsqlI .= $separador."G3046_C61068";
                $LsqlV .= $separador."'".$_POST["G3046_C61068"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61118"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61118 = '".$_POST["G3046_C61118"]."'";
                $LsqlI .= $separador."G3046_C61118";
                $LsqlV .= $separador."'".$_POST["G3046_C61118"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61069"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61069 = '".$_POST["G3046_C61069"]."'";
                $LsqlI .= $separador."G3046_C61069";
                $LsqlV .= $separador."'".$_POST["G3046_C61069"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61113 = '".$_POST["G3046_C61113"]."'";
                $LsqlI .= $separador."G3046_C61113";
                $LsqlV .= $separador."'".$_POST["G3046_C61113"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61114"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61114 = '".$_POST["G3046_C61114"]."'";
                $LsqlI .= $separador."G3046_C61114";
                $LsqlV .= $separador."'".$_POST["G3046_C61114"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61115"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61115 = '".$_POST["G3046_C61115"]."'";
                $LsqlI .= $separador."G3046_C61115";
                $LsqlV .= $separador."'".$_POST["G3046_C61115"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61116"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61116 = '".$_POST["G3046_C61116"]."'";
                $LsqlI .= $separador."G3046_C61116";
                $LsqlV .= $separador."'".$_POST["G3046_C61116"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61117"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61117 = '".$_POST["G3046_C61117"]."'";
                $LsqlI .= $separador."G3046_C61117";
                $LsqlV .= $separador."'".$_POST["G3046_C61117"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61105"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61105 = '".$_POST["G3046_C61105"]."'";
                $LsqlI .= $separador."G3046_C61105";
                $LsqlV .= $separador."'".$_POST["G3046_C61105"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61106"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61106 = '".$_POST["G3046_C61106"]."'";
                $LsqlI .= $separador."G3046_C61106";
                $LsqlV .= $separador."'".$_POST["G3046_C61106"]."'";
                $validar = 1;
            }
             
 
            $G3046_C61107 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3046_C61107"])){    
                if($_POST["G3046_C61107"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3046_C61107"]);
                    if(count($tieneHora) > 1){
                        $G3046_C61107 = "'".$_POST["G3046_C61107"]."'";
                    }else{
                        $G3046_C61107 = "'".str_replace(' ', '',$_POST["G3046_C61107"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3046_C61107 = ".$G3046_C61107;
                    $LsqlI .= $separador." G3046_C61107";
                    $LsqlV .= $separador.$G3046_C61107;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3046_C61108"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61108 = '".$_POST["G3046_C61108"]."'";
                $LsqlI .= $separador."G3046_C61108";
                $LsqlV .= $separador."'".$_POST["G3046_C61108"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61109"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61109 = '".$_POST["G3046_C61109"]."'";
                $LsqlI .= $separador."G3046_C61109";
                $LsqlV .= $separador."'".$_POST["G3046_C61109"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61110"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61110 = '".$_POST["G3046_C61110"]."'";
                $LsqlI .= $separador."G3046_C61110";
                $LsqlV .= $separador."'".$_POST["G3046_C61110"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61111"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61111 = '".$_POST["G3046_C61111"]."'";
                $LsqlI .= $separador."G3046_C61111";
                $LsqlV .= $separador."'".$_POST["G3046_C61111"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3046_C61112"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_C61112 = '".$_POST["G3046_C61112"]."'";
                $LsqlI .= $separador."G3046_C61112";
                $LsqlV .= $separador."'".$_POST["G3046_C61112"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G3046_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G3046_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3046_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3046_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3046_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $CoInMi = $_GET['CodigoMiembro'];
                    
                    $strCampana = (isset($_POST["G3046_C61067"]) ? $_POST["G3046_C61067"] : "backoffice");

                    $strSQLUpdate_t = "UPDATE ".$BaseDatos.".G3045_M2369 SET G3045_M2369_Estado____b = 3, G3045_M2369_Comentari_b = 'cambiado a NO REINTENTAR por la campaña ".$strCampana."' WHERE G3045_M2369_CoInMiPo__b = ".$CoInMi;
                    $mysqli->query($strSQLUpdate_t);

                    $strSQLUpdate_t = "UPDATE ".$BaseDatos.".G3045_M2386 SET G3045_M2386_Estado____b = 3, G3045_M2386_Comentari_b = 'cambiado a NO REINTENTAR por la campaña ".$strCampana."' WHERE G3045_M2386_CoInMiPo__b = ".$CoInMi;
                    $mysqli->query($strSQLUpdate_t);

                    $strSQLUpdate_t = "UPDATE ".$BaseDatos.".G3045_M2418 SET G3045_M2418_Estado____b = 3, G3045_M2418_Comentari_b = 'cambiado a NO REINTENTAR por la campaña ".$strCampana."' WHERE G3045_M2418_CoInMiPo__b = ".$CoInMi;
                    $mysqli->query($strSQLUpdate_t);
                    
                    $LsqlI .= ", G3046_Usuario , G3046_FechaInsercion, G3046_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];


                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3046_ConsInte__b =".$_POST["id"]; 
                    $CoInMi = $_POST["id"];
                }else if($_POST["oper"] == 'del' ){
                    $CoInMi = $_POST["id"];
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3046 WHERE G3046_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {




                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3046_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3046 WHERE G3046_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3047_ConsInte__b, G3047_C61077, G3047_C61079, G3047_C61078 FROM ".$BaseDatos.".G3047  ";

        $SQL .= " WHERE G3047_C61079 = '".$numero."'"; 

        $SQL .= " ORDER BY G3047_C61077";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3047_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3047_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G3047_C61077)."</cell>";

                echo "<cell>". ($fila->G3047_C61079)."</cell>";

                echo "<cell>". ($fila->G3047_C61078)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3047 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3047(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G3047_C61077"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3047_C61077 = '".$_POST["G3047_C61077"]."'";
                    $LsqlI .= $separador."G3047_C61077";
                    $LsqlV .= $separador."'".$_POST["G3047_C61077"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G3047_C61078"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3047_C61078 = '".$_POST["G3047_C61078"]."'";
                    $LsqlI .= $separador."G3047_C61078";
                    $LsqlV .= $separador."'".$_POST["G3047_C61078"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3047_C61079 = $numero;
                    $LsqlU .= ", G3047_C61079 = ".$G3047_C61079."";
                    $LsqlI .= ", G3047_C61079";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3047_Usuario ,  G3047_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3047_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3047 WHERE  G3047_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
