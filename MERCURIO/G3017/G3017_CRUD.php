<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3017_ConsInte__b, G3017_FechaInsercion , G3017_Usuario ,  G3017_CodigoMiembro  , G3017_PoblacionOrigen , G3017_EstadoDiligenciamiento ,  G3017_IdLlamada , G3017_C60186 as principal ,G3017_C60186,G3017_C60187,G3017_C60188,G3017_C60181,G3017_C60182,G3017_C60183,G3017_C60185,G3017_C60170,G3017_C60171,G3017_C60172,G3017_C60173,G3017_C60174,G3017_C60175,G3017_C60176,G3017_C60177,G3017_C60178 FROM '.$BaseDatos.'.G3017 WHERE G3017_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3017_C60186'] = $key->G3017_C60186;

                $datos[$i]['G3017_C60187'] = $key->G3017_C60187;

                $datos[$i]['G3017_C60188'] = $key->G3017_C60188;

                $datos[$i]['G3017_C60181'] = explode(' ', $key->G3017_C60181)[0];

                $datos[$i]['G3017_C60182'] = explode(' ', $key->G3017_C60182)[0];

                $datos[$i]['G3017_C60183'] = $key->G3017_C60183;

                $datos[$i]['G3017_C60185'] = $key->G3017_C60185;

                $datos[$i]['G3017_C60170'] = $key->G3017_C60170;

                $datos[$i]['G3017_C60171'] = $key->G3017_C60171;

                $datos[$i]['G3017_C60172'] = explode(' ', $key->G3017_C60172)[0];
  
                $hora = '';
                if(!is_null($key->G3017_C60173)){
                    $hora = explode(' ', $key->G3017_C60173)[1];
                }

                $datos[$i]['G3017_C60173'] = $hora;

                $datos[$i]['G3017_C60174'] = $key->G3017_C60174;

                $datos[$i]['G3017_C60175'] = $key->G3017_C60175;

                $datos[$i]['G3017_C60176'] = $key->G3017_C60176;

                $datos[$i]['G3017_C60177'] = $key->G3017_C60177;

                $datos[$i]['G3017_C60178'] = $key->G3017_C60178;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3017";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3017_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3017_ConsInte__b as id,  G3017_C60186 as camp1 , b.LISOPC_Nombre____b as camp2 
                     FROM ".$BaseDatos.".G3017  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G3017_C60183 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3017_ConsInte__b as id,  G3017_C60186 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G3017  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G3017_C60183 JOIN ".$BaseDatos.".G3017_M".$_POST['muestra']." ON G3017_ConsInte__b = G3017_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3017_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3017_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3017_C60186 LIKE '%".$B."%' OR G3017_C60183 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3017_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3017");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3017_ConsInte__b, G3017_FechaInsercion , G3017_Usuario ,  G3017_CodigoMiembro  , G3017_PoblacionOrigen , G3017_EstadoDiligenciamiento ,  G3017_IdLlamada , G3017_C60186 as principal ,G3017_C60186,G3017_C60187, a.LISOPC_Nombre____b as G3017_C60188,G3017_C60181,G3017_C60182, b.LISOPC_Nombre____b as G3017_C60183,G3017_C60185, c.LISOPC_Nombre____b as G3017_C60170, d.LISOPC_Nombre____b as G3017_C60171,G3017_C60172,G3017_C60173,G3017_C60174,G3017_C60175,G3017_C60176,G3017_C60177,G3017_C60178 FROM '.$BaseDatos.'.G3017 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3017_C60188 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3017_C60183 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3017_C60170 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3017_C60171';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3017_C60173)){
                    $hora_a = explode(' ', $fila->G3017_C60173)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3017_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3017_ConsInte__b , ($fila->G3017_C60186) , ($fila->G3017_C60187) , ($fila->G3017_C60188) , explode(' ', $fila->G3017_C60181)[0] , explode(' ', $fila->G3017_C60182)[0] , ($fila->G3017_C60183) , ($fila->G3017_C60185) , ($fila->G3017_C60170) , ($fila->G3017_C60171) , explode(' ', $fila->G3017_C60172)[0] , $hora_a , ($fila->G3017_C60174) , ($fila->G3017_C60175) , ($fila->G3017_C60176) , ($fila->G3017_C60177) , ($fila->G3017_C60178) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3017 WHERE G3017_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3017";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3017_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3017_ConsInte__b as id,  G3017_C60186 as camp1 , b.LISOPC_Nombre____b as camp2  FROM '.$BaseDatos.'.G3017 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3017_ConsInte__b as id,  G3017_C60186 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G3017  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G3017_C60183 JOIN ".$BaseDatos.".G3017_M".$_POST['muestra']." ON G3017_ConsInte__b = G3017_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3017_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3017_C60186 LIKE "%'.$B.'%" OR G3017_C60183 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3017_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3017 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3017(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3017_C60186"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60186 = '".$_POST["G3017_C60186"]."'";
                $LsqlI .= $separador."G3017_C60186";
                $LsqlV .= $separador."'".$_POST["G3017_C60186"]."'";
                $validar = 1;
            }
             
  
            $G3017_C60187 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3017_C60187"])){
                if($_POST["G3017_C60187"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3017_C60187 = $_POST["G3017_C60187"];
                    $LsqlU .= $separador." G3017_C60187 = ".$G3017_C60187."";
                    $LsqlI .= $separador." G3017_C60187";
                    $LsqlV .= $separador.$G3017_C60187;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3017_C60188"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60188 = '".$_POST["G3017_C60188"]."'";
                $LsqlI .= $separador."G3017_C60188";
                $LsqlV .= $separador."'".$_POST["G3017_C60188"]."'";
                $validar = 1;
            }
             
 
            $G3017_C60181 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3017_C60181"])){    
                if($_POST["G3017_C60181"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3017_C60181"]);
                    if(count($tieneHora) > 1){
                        $G3017_C60181 = "'".$_POST["G3017_C60181"]."'";
                    }else{
                        $G3017_C60181 = "'".str_replace(' ', '',$_POST["G3017_C60181"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3017_C60181 = ".$G3017_C60181;
                    $LsqlI .= $separador." G3017_C60181";
                    $LsqlV .= $separador.$G3017_C60181;
                    $validar = 1;
                }
            }
 
            $G3017_C60182 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3017_C60182"])){    
                if($_POST["G3017_C60182"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3017_C60182"]);
                    if(count($tieneHora) > 1){
                        $G3017_C60182 = "'".$_POST["G3017_C60182"]."'";
                    }else{
                        $G3017_C60182 = "'".str_replace(' ', '',$_POST["G3017_C60182"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3017_C60182 = ".$G3017_C60182;
                    $LsqlI .= $separador." G3017_C60182";
                    $LsqlV .= $separador.$G3017_C60182;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3017_C60183"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60183 = '".$_POST["G3017_C60183"]."'";
                $LsqlI .= $separador."G3017_C60183";
                $LsqlV .= $separador."'".$_POST["G3017_C60183"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3017_C60185"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60185 = '".$_POST["G3017_C60185"]."'";
                $LsqlI .= $separador."G3017_C60185";
                $LsqlV .= $separador."'".$_POST["G3017_C60185"]."'";
                $validar = 1;
            }
             
 
            $G3017_C60170 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3017_C60170 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G3017_C60170 = ".$G3017_C60170;
                    $LsqlI .= $separador." G3017_C60170";
                    $LsqlV .= $separador.$G3017_C60170;
                    $validar = 1;

                    
                }
            }
 
            $G3017_C60171 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3017_C60171 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G3017_C60171 = ".$G3017_C60171;
                    $LsqlI .= $separador." G3017_C60171";
                    $LsqlV .= $separador.$G3017_C60171;
                    $validar = 1;
                }
            }
 
            $G3017_C60172 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3017_C60172 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G3017_C60172 = ".$G3017_C60172;
                    $LsqlI .= $separador." G3017_C60172";
                    $LsqlV .= $separador.$G3017_C60172;
                    $validar = 1;
                }
            }
 
            $G3017_C60173 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3017_C60173 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G3017_C60173 = ".$G3017_C60173;
                    $LsqlI .= $separador." G3017_C60173";
                    $LsqlV .= $separador.$G3017_C60173;
                    $validar = 1;
                }
            }
 
            $G3017_C60174 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3017_C60174 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G3017_C60174 = ".$G3017_C60174;
                    $LsqlI .= $separador." G3017_C60174";
                    $LsqlV .= $separador.$G3017_C60174;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3017_C60175"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60175 = '".$_POST["G3017_C60175"]."'";
                $LsqlI .= $separador."G3017_C60175";
                $LsqlV .= $separador."'".$_POST["G3017_C60175"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3017_C60176"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60176 = '".$_POST["G3017_C60176"]."'";
                $LsqlI .= $separador."G3017_C60176";
                $LsqlV .= $separador."'".$_POST["G3017_C60176"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3017_C60177"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60177 = '".$_POST["G3017_C60177"]."'";
                $LsqlI .= $separador."G3017_C60177";
                $LsqlV .= $separador."'".$_POST["G3017_C60177"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3017_C60178"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60178 = '".$_POST["G3017_C60178"]."'";
                $LsqlI .= $separador."G3017_C60178";
                $LsqlV .= $separador."'".$_POST["G3017_C60178"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3017_C60179"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60179 = '".$_POST["G3017_C60179"]."'";
                $LsqlI .= $separador."G3017_C60179";
                $LsqlV .= $separador."'".$_POST["G3017_C60179"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3017_C60180"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_C60180 = '".$_POST["G3017_C60180"]."'";
                $LsqlI .= $separador."G3017_C60180";
                $LsqlV .= $separador."'".$_POST["G3017_C60180"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G3017_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G3017_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3017_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3017_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3017_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3017_Usuario , G3017_FechaInsercion, G3017_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3017_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3017 WHERE G3017_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
