<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G3003/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G3003/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G3003/".$archivo);
                        $tamaÃ±o = filesize("/Dyalogo/tmp/G3003/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaÃ±o);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G3003/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3003_ConsInte__b, G3003_FechaInsercion , G3003_Usuario ,  G3003_CodigoMiembro  , G3003_PoblacionOrigen , G3003_EstadoDiligenciamiento ,  G3003_IdLlamada , G3003_C59933 as principal ,G3003_C63815,G3003_C59933,G3003_C63816,G3003_C59935,G3003_C63817,G3003_C62858,G3003_C59936,G3003_C63818,G3003_C63819,G3003_C63820,G3003_C63821,G3003_C63822,G3003_C59934,G3003_C60288,G3003_C60289,G3003_C60290,G3003_C60291,G3003_C60292,G3003_C60493,G3003_C63297,G3003_C63823,G3003_C63825,G3003_C63826,G3003_C65698,G3003_C65700,G3003_C59930,G3003_C59931,G3003_C59932 FROM '.$BaseDatos.'.G3003 WHERE G3003_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3003_C63815'] = $key->G3003_C63815;

                $datos[$i]['G3003_C59933'] = $key->G3003_C59933;

                $datos[$i]['G3003_C63816'] = $key->G3003_C63816;

                $datos[$i]['G3003_C59935'] = $key->G3003_C59935;

                $datos[$i]['G3003_C63817'] = $key->G3003_C63817;

                $datos[$i]['G3003_C62858'] = $key->G3003_C62858;

                $datos[$i]['G3003_C59936'] = $key->G3003_C59936;

                $datos[$i]['G3003_C63818'] = $key->G3003_C63818;

                $datos[$i]['G3003_C63819'] = $key->G3003_C63819;

                $datos[$i]['G3003_C63820'] = $key->G3003_C63820;

                $datos[$i]['G3003_C63821'] = $key->G3003_C63821;

                $datos[$i]['G3003_C63822'] = $key->G3003_C63822;

                $datos[$i]['G3003_C59934'] = $key->G3003_C59934;

                $datos[$i]['G3003_C60288'] = $key->G3003_C60288;

                $datos[$i]['G3003_C60289'] = $key->G3003_C60289;

                $datos[$i]['G3003_C60290'] = $key->G3003_C60290;

                $datos[$i]['G3003_C60291'] = $key->G3003_C60291;

                $datos[$i]['G3003_C60292'] = $key->G3003_C60292;

                $datos[$i]['G3003_C60493'] = $key->G3003_C60493;

                $datos[$i]['G3003_C63297'] = $key->G3003_C63297;

                $datos[$i]['G3003_C63823'] = $key->G3003_C63823;

                $datos[$i]['G3003_C63825'] = explode(' ', $key->G3003_C63825)[0];
  
                $hora = '';
                if(!is_null($key->G3003_C63826)){
                    $hora = explode(' ', $key->G3003_C63826)[1];
                }

                $datos[$i]['G3003_C63826'] = $hora;

                $datos[$i]['G3003_C65698'] = $key->G3003_C65698;

                $datos[$i]['G3003_C65700'] = $key->G3003_C65700;

                $datos[$i]['G3003_C59930'] = $key->G3003_C59930;

                $datos[$i]['G3003_C59931'] = $key->G3003_C59931;

                $datos[$i]['G3003_C59932'] = $key->G3003_C59932;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3003";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3003_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3003_ConsInte__b as id,  G3003_C59933 as camp1 , G3003_C59935 as camp2 
                     FROM ".$BaseDatos.".G3003  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3003_ConsInte__b as id,  G3003_C59933 as camp1 , G3003_C59935 as camp2  
                    FROM ".$BaseDatos.".G3003  JOIN ".$BaseDatos.".G3003_M".$_POST['muestra']." ON G3003_ConsInte__b = G3003_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3003_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3003_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3003_C59933 LIKE '%".$B."%' OR G3003_C59935 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3003_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3003");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3003_ConsInte__b, G3003_FechaInsercion , G3003_Usuario ,  G3003_CodigoMiembro  , G3003_PoblacionOrigen , G3003_EstadoDiligenciamiento ,  G3003_IdLlamada , G3003_C59933 as principal ,G3003_C63815,G3003_C59933,G3003_C63816,G3003_C59935, a.LISOPC_Nombre____b as G3003_C63817, b.LISOPC_Nombre____b as G3003_C62858, c.LISOPC_Nombre____b as G3003_C59936, d.LISOPC_Nombre____b as G3003_C63818, e.LISOPC_Nombre____b as G3003_C63819, f.LISOPC_Nombre____b as G3003_C63820, g.LISOPC_Nombre____b as G3003_C63821, h.LISOPC_Nombre____b as G3003_C63822,G3003_C59934,G3003_C60288,G3003_C60289,G3003_C60290,G3003_C60291,G3003_C60292,G3003_C60493,G3003_C63297,G3003_C63823,G3003_C63825,G3003_C63826,G3003_C65698,G3003_C65700,G3003_C59930,G3003_C59931, i.LISOPC_Nombre____b as G3003_C59932 FROM '.$BaseDatos.'.G3003 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3003_C63817 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3003_C62858 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3003_C59936 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3003_C63818 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3003_C63819 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3003_C63820 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G3003_C63821 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G3003_C63822 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G3003_C59932';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3003_C63826)){
                    $hora_a = explode(' ', $fila->G3003_C63826)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3003_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3003_ConsInte__b , ($fila->G3003_C63815) , ($fila->G3003_C59933) , ($fila->G3003_C63816) , ($fila->G3003_C59935) , ($fila->G3003_C63817) , ($fila->G3003_C62858) , ($fila->G3003_C59936) , ($fila->G3003_C63818) , ($fila->G3003_C63819) , ($fila->G3003_C63820) , ($fila->G3003_C63821) , ($fila->G3003_C63822) , ($fila->G3003_C59934) , ($fila->G3003_C60288) , ($fila->G3003_C60289) , ($fila->G3003_C60290) , ($fila->G3003_C60291) , ($fila->G3003_C60292) , ($fila->G3003_C60493) , ($fila->G3003_C63297) , ($fila->G3003_C63823) , explode(' ', $fila->G3003_C63825)[0] , $hora_a , ($fila->G3003_C65698) , ($fila->G3003_C65700) , ($fila->G3003_C59930) , ($fila->G3003_C59931) , ($fila->G3003_C59932) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3003 WHERE G3003_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3003";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3003_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3003_ConsInte__b as id,  G3003_C59933 as camp1 , G3003_C59935 as camp2  FROM '.$BaseDatos.'.G3003 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3003_ConsInte__b as id,  G3003_C59933 as camp1 , G3003_C59935 as camp2  
                    FROM ".$BaseDatos.".G3003  JOIN ".$BaseDatos.".G3003_M".$_POST['muestra']." ON G3003_ConsInte__b = G3003_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3003_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3003_C59933 LIKE "%'.$B.'%" OR G3003_C59935 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3003_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3003 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3003(";
            $LsqlV = " VALUES ("; 
  
            $G3003_C63815 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3003_C63815"])){
                if($_POST["G3003_C63815"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3003_C63815 = $_POST["G3003_C63815"];
                    $LsqlU .= $separador." G3003_C63815 = ".$G3003_C63815."";
                    $LsqlI .= $separador." G3003_C63815";
                    $LsqlV .= $separador.$G3003_C63815;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3003_C59933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59933 = '".$_POST["G3003_C59933"]."'";
                $LsqlI .= $separador."G3003_C59933";
                $LsqlV .= $separador."'".$_POST["G3003_C59933"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63816"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63816 = '".$_POST["G3003_C63816"]."'";
                $LsqlI .= $separador."G3003_C63816";
                $LsqlV .= $separador."'".$_POST["G3003_C63816"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C59935"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59935 = '".$_POST["G3003_C59935"]."'";
                $LsqlI .= $separador."G3003_C59935";
                $LsqlV .= $separador."'".$_POST["G3003_C59935"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63817"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63817 = '".$_POST["G3003_C63817"]."'";
                $LsqlI .= $separador."G3003_C63817";
                $LsqlV .= $separador."'".$_POST["G3003_C63817"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C62858"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C62858 = '".$_POST["G3003_C62858"]."'";
                $LsqlI .= $separador."G3003_C62858";
                $LsqlV .= $separador."'".$_POST["G3003_C62858"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C59936"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59936 = '".$_POST["G3003_C59936"]."'";
                $LsqlI .= $separador."G3003_C59936";
                $LsqlV .= $separador."'".$_POST["G3003_C59936"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63818"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63818 = '".$_POST["G3003_C63818"]."'";
                $LsqlI .= $separador."G3003_C63818";
                $LsqlV .= $separador."'".$_POST["G3003_C63818"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63819"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63819 = '".$_POST["G3003_C63819"]."'";
                $LsqlI .= $separador."G3003_C63819";
                $LsqlV .= $separador."'".$_POST["G3003_C63819"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63820"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63820 = '".$_POST["G3003_C63820"]."'";
                $LsqlI .= $separador."G3003_C63820";
                $LsqlV .= $separador."'".$_POST["G3003_C63820"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63821"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63821 = '".$_POST["G3003_C63821"]."'";
                $LsqlI .= $separador."G3003_C63821";
                $LsqlV .= $separador."'".$_POST["G3003_C63821"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C63822"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C63822 = '".$_POST["G3003_C63822"]."'";
                $LsqlI .= $separador."G3003_C63822";
                $LsqlV .= $separador."'".$_POST["G3003_C63822"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C59934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59934 = '".$_POST["G3003_C59934"]."'";
                $LsqlI .= $separador."G3003_C59934";
                $LsqlV .= $separador."'".$_POST["G3003_C59934"]."'";
                $validar = 1;
            }
             
  
            if (isset($_FILES["FG3003_C60288"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G3003/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G3003")){
                    mkdir("/Dyalogo/tmp/G3003", 0777);
                }
                if ($_FILES["FG3003_C60288"]["size"] != 0) {
                    $G3003_C60288 = $_FILES["FG3003_C60288"]["tmp_name"];
                    $nG3003_C60288 = $fechUp."_".$_FILES["FG3003_C60288"]["name"];
                    $rutaFinal = $destinoFile.$nG3003_C60288;
                    if (is_uploaded_file($G3003_C60288)) {
                        move_uploaded_file($G3003_C60288, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3003_C60288 = '".$nG3003_C60288."'";
                    $LsqlI .= $separador."G3003_C60288";
                    $LsqlV .= $separador."'".$nG3003_C60288."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG3003_C60289"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G3003/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G3003")){
                    mkdir("/Dyalogo/tmp/G3003", 0777);
                }
                if ($_FILES["FG3003_C60289"]["size"] != 0) {
                    $G3003_C60289 = $_FILES["FG3003_C60289"]["tmp_name"];
                    $nG3003_C60289 = $fechUp."_".$_FILES["FG3003_C60289"]["name"];
                    $rutaFinal = $destinoFile.$nG3003_C60289;
                    if (is_uploaded_file($G3003_C60289)) {
                        move_uploaded_file($G3003_C60289, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3003_C60289 = '".$nG3003_C60289."'";
                    $LsqlI .= $separador."G3003_C60289";
                    $LsqlV .= $separador."'".$nG3003_C60289."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG3003_C60290"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G3003/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G3003")){
                    mkdir("/Dyalogo/tmp/G3003", 0777);
                }
                if ($_FILES["FG3003_C60290"]["size"] != 0) {
                    $G3003_C60290 = $_FILES["FG3003_C60290"]["tmp_name"];
                    $nG3003_C60290 = $fechUp."_".$_FILES["FG3003_C60290"]["name"];
                    $rutaFinal = $destinoFile.$nG3003_C60290;
                    if (is_uploaded_file($G3003_C60290)) {
                        move_uploaded_file($G3003_C60290, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3003_C60290 = '".$nG3003_C60290."'";
                    $LsqlI .= $separador."G3003_C60290";
                    $LsqlV .= $separador."'".$nG3003_C60290."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG3003_C60291"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G3003/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G3003")){
                    mkdir("/Dyalogo/tmp/G3003", 0777);
                }
                if ($_FILES["FG3003_C60291"]["size"] != 0) {
                    $G3003_C60291 = $_FILES["FG3003_C60291"]["tmp_name"];
                    $nG3003_C60291 = $fechUp."_".$_FILES["FG3003_C60291"]["name"];
                    $rutaFinal = $destinoFile.$nG3003_C60291;
                    if (is_uploaded_file($G3003_C60291)) {
                        move_uploaded_file($G3003_C60291, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3003_C60291 = '".$nG3003_C60291."'";
                    $LsqlI .= $separador."G3003_C60291";
                    $LsqlV .= $separador."'".$nG3003_C60291."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG3003_C60292"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G3003/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G3003")){
                    mkdir("/Dyalogo/tmp/G3003", 0777);
                }
                if ($_FILES["FG3003_C60292"]["size"] != 0) {
                    $G3003_C60292 = $_FILES["FG3003_C60292"]["tmp_name"];
                    $nG3003_C60292 = $fechUp."_".$_FILES["FG3003_C60292"]["name"];
                    $rutaFinal = $destinoFile.$nG3003_C60292;
                    if (is_uploaded_file($G3003_C60292)) {
                        move_uploaded_file($G3003_C60292, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3003_C60292 = '".$nG3003_C60292."'";
                    $LsqlI .= $separador."G3003_C60292";
                    $LsqlV .= $separador."'".$nG3003_C60292."'";
                    $validar = 1;
                }
            }
            
  

            if(isset($_POST["G3003_C60493"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C60493 = '".$_POST["G3003_C60493"]."'";
                $LsqlI .= $separador."G3003_C60493";
                $LsqlV .= $separador."'".$_POST["G3003_C60493"]."'";
                $validar = 1;
            }
             
  
            $G3003_C63297 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G3003_C63297"])){
                if($_POST["G3003_C63297"] == 'Yes'){
                    $G3003_C63297 = 1;
                }else if($_POST["G3003_C63297"] == 'off'){
                    $G3003_C63297 = 0;
                }else if($_POST["G3003_C63297"] == 'on'){
                    $G3003_C63297 = 1;
                }else if($_POST["G3003_C63297"] == 'No'){
                    $G3003_C63297 = 1;
                }else{
                    $G3003_C63297 = $_POST["G3003_C63297"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G3003_C63297 = ".$G3003_C63297."";
                $LsqlI .= $separador." G3003_C63297";
                $LsqlV .= $separador.$G3003_C63297;

                $validar = 1;
            }
  
            $G3003_C63823 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3003_C63823"])){
                if($_POST["G3003_C63823"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3003_C63823 = $_POST["G3003_C63823"];
                    $LsqlU .= $separador." G3003_C63823 = ".$G3003_C63823."";
                    $LsqlI .= $separador." G3003_C63823";
                    $LsqlV .= $separador.$G3003_C63823;
                    $validar = 1;
                }
            }
 
            $G3003_C63825 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3003_C63825"])){    
                if($_POST["G3003_C63825"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3003_C63825"]);
                    if(count($tieneHora) > 1){
                        $G3003_C63825 = "'".$_POST["G3003_C63825"]."'";
                    }else{
                        $G3003_C63825 = "'".str_replace(' ', '',$_POST["G3003_C63825"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3003_C63825 = ".$G3003_C63825;
                    $LsqlI .= $separador." G3003_C63825";
                    $LsqlV .= $separador.$G3003_C63825;
                    $validar = 1;
                }
            }
  
            $G3003_C63826 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3003_C63826"])){   
                if($_POST["G3003_C63826"] != '' && $_POST["G3003_C63826"] != 'undefined' && $_POST["G3003_C63826"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3003_C63826 = "'".$fecha." ".str_replace(' ', '',$_POST["G3003_C63826"])."'";
                    $LsqlU .= $separador." G3003_C63826 = ".$G3003_C63826."";
                    $LsqlI .= $separador." G3003_C63826";
                    $LsqlV .= $separador.$G3003_C63826;
                    $validar = 1;
                }
            }
  
            if (isset($_FILES["FG3003_C65698"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G3003/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G3003")){
                    mkdir("/Dyalogo/tmp/G3003", 0777);
                }
                if ($_FILES["FG3003_C65698"]["size"] != 0) {
                    $G3003_C65698 = $_FILES["FG3003_C65698"]["tmp_name"];
                    $nG3003_C65698 = $fechUp."_".$_FILES["FG3003_C65698"]["name"];
                    $rutaFinal = $destinoFile.$nG3003_C65698;
                    if (is_uploaded_file($G3003_C65698)) {
                        move_uploaded_file($G3003_C65698, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3003_C65698 = '".$nG3003_C65698."'";
                    $LsqlI .= $separador."G3003_C65698";
                    $LsqlV .= $separador."'".$nG3003_C65698."'";
                    $validar = 1;
                }
            }
            
  

            if(isset($_POST["G3003_C65700"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C65700 = '".$_POST["G3003_C65700"]."'";
                $LsqlI .= $separador."G3003_C65700";
                $LsqlV .= $separador."'".$_POST["G3003_C65700"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C59930"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59930 = '".$_POST["G3003_C59930"]."'";
                $LsqlI .= $separador."G3003_C59930";
                $LsqlV .= $separador."'".$_POST["G3003_C59930"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C59931"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59931 = '".$_POST["G3003_C59931"]."'";
                $LsqlI .= $separador."G3003_C59931";
                $LsqlV .= $separador."'".$_POST["G3003_C59931"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3003_C59932"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_C59932 = '".$_POST["G3003_C59932"]."'";
                $LsqlI .= $separador."G3003_C59932";
                $LsqlV .= $separador."'".$_POST["G3003_C59932"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3003_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3003_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3003_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3003_Usuario , G3003_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3003_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3003 WHERE G3003_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3003 SET G3003_UltiGest__b =-14, G3003_GesMasImp_b =-14, G3003_TipoReintentoUG_b =0, G3003_TipoReintentoGMI_b =0, G3003_ClasificacionUG_b =3, G3003_ClasificacionGMI_b =3, G3003_EstadoUG_b =-14, G3003_EstadoGMI_b =-14, G3003_CantidadIntentos =0, G3003_CantidadIntentosGMI_b =0 WHERE G3003_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
