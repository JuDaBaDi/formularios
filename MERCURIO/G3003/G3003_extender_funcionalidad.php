<?php
include(__DIR__."/../../conexion.php");

//Este archivo es para agregar funcionalidades al G, y que al momento de generar de nuevo no se pierdan

//Cosas como nuevas consultas, nuevos Inserts, Envio de correos_, etc, en fin extender mas los formularios en PHP

insertarLog("webhook-llamado","SUCCESS");

$arrData_t = json_decode(file_get_contents('php://input'), true);

if($arrData_t){

    $lead=$arrData_t['leads'][0];
    $programa=$lead['custom_fields'];
    $programa=$programa['Programa de Interu00e9s'];
    $programa = codigoPrograma($programa);

    $strSQL_t = "SELECT G3003_ConsInte__b AS id FROM ".$BaseDatos.".G3003 WHERE G3003_C59934 = '".$lead["email"]."' LIMIT 1";

    if ($resSQL_t = $mysqli->query($strSQL_t)) {

        if ($resSQL_t->num_rows > 0) {

            $objSQL_t = $resSQL_t->fetch_object();

            $intIdBd_t = $objSQL_t->id;

            $strSQLUpdateBd_t = "UPDATE ".$BaseDatos.".G3003 SET G3003_C59933 = '".$lead["name"]."', G3003_C59934 = '".$lead["email"]."', G3003_C59935 = '".$lead["personal_phone"]."', G3003_C59936 = '".$programa."', G3003_C66275 = '".$lead['uuid']."' WHERE G3003_ConsInte__b = '".$intIdBd_t."'";

            $intIdUpdateBdLog_t = insertarLog("update-bd");

            if ($mysqli->query($strSQLUpdateBd_t)) {

                updateLog($intIdUpdateBdLog_t,$strSQLUpdateBd_t,"SUCCESS","SUCCESS");

            }else{

                updateLog($intIdUpdateBdLog_t,$strSQLUpdateBd_t,$mysqli->error,"ERROR");                

            }

            $strSQLUpdateMt_t = "UPDATE ".$BaseDatos.".G3003_M2424 SET G3003_M2424_Activo____b = -1, G3003_M2424_Estado____b = 2, G3003_M2424_FecHorAge_b = DATE_ADD(NOW(), interval 4 day) WHERE G3003_M2424_CoInMiPo__b = '".$intIdBd_t."'";

            $intIdUpdateMtLog_t = insertarLog("update-mt");

            if ($mysqli->query($strSQLUpdateMt_t)) {

                updateLog($intIdUpdateMtLog_t,$strSQLUpdateMt_t,"SUCCESS","SUCCESS");

            }else{

                updateLog($intIdUpdateMtLog_t,$strSQLUpdateMt_t,$mysqli->error,"ERROR");                

            }

        }else{

            insertarLead($lead['name'],$lead['email'],$lead['personal_phone'],$programa,$lead['uuid']);

        }

    }

}

echo 'exito';

/**
 *JDBD - En esta funcion insertamos el lead.
 *@param string,string,string,string,integer
 *@return null. 
 */
function insertarLead($strNombre_p,$strEmail_p,$strTelefono_p,$strPrograma_p,$intId_p){

    global $mysqli;
    global $BaseDatos;

    $strSQLInsertBd_t = "INSERT INTO ".$BaseDatos.".G3003 (G3003_FechaInsercion,G3003_UltiGest__b,G3003_GesMasImp_b,G3003_TipoReintentoUG_b,G3003_TipoReintentoGMI_b,G3003_ClasificacionUG_b,G3003_ClasificacionGMI_b,G3003_EstadoUG_b,G3003_EstadoGMI_b,G3003_CantidadIntentos,G3003_CantidadIntentosGMI_b,G3003_C59933,G3003_C59934,G3003_C59935,G3003_C59936,G3003_C66275,G3003_C59930) VALUES (NOW(),-14,-14,0,0,3,3,-14,-14,0,0,'".$mysqli->real_escape_string($strNombre_p)."','".$mysqli->real_escape_string($strEmail_p)."','".$mysqli->real_escape_string($strTelefono_p)."','".$mysqli->real_escape_string($strPrograma_p)."','".$intId_p."','rd_google')";

    $intIdInsertBdLog_t = insertarLog("insert-bd");

    if ($mysqli->query($strSQLInsertBd_t)) {

        $intIdInsertBd_t = $mysqli->insert_id;

        updateLog($intIdInsertBdLog_t,$strSQLInsertBd_t,"SUCCESS","SUCCESS");  

        $strSQLInsertMt_t = "INSERT INTO ".$BaseDatos.".G3003_M2424 (G3003_M2424_CoInMiPo__b, G3003_M2424_Activo____b, G3003_M2424_Estado____b, G3003_M2424_TipoReintentoGMI_b, G3003_M2424_NumeInte__b, G3003_M2424_CantidadIntentosGMI_b) VALUES ('".$intIdInsertBd_t."',-1,0,0,0,0)";

        $mysqli->query($strSQLInsertMt_t);

    }else{

        updateLog($intIdInsertBdLog_t,$strSQLInsertBd_t,$mysqli->error,"ERROR");    

    }


}

/**
 *JDBD - En esta funcion actualizamos el log.
 *@param string
 *@return integer. 
 */
function updateLog($intId_p,$strConsulta_p,$strError_p,$strEstado_p){

    global $mysqli;
    global $BaseDatos;

    $strSQL_t = "UPDATE ".$BaseDatos.".log_campo_alto_rd SET consulta = '".$mysqli->real_escape_string($strConsulta_p)."', error = '".$mysqli->real_escape_string($strError_p)."', estado = '".$strEstado_p."' WHERE id = '".$intId_p."'";

    $mysqli->query($strSQL_t);

}

/**
 *JDBD - En esta funcion insertamos el log.
 *@param string
 *@return integer. 
 */
function insertarLog($strTipo_p,$strEstado_p=null){

    global $mysqli;
    global $BaseDatos;

    $strEstado_t = "ERROR";

    if (!is_null($strEstado_p)) {

        $strEstado_t = $strEstado_p;

    }

    $strSQL_t = "INSERT INTO ".$BaseDatos.".log_campo_alto_rd (tipo,estado) VALUES ('".$strTipo_p."','".$strEstado_t."')";

    $intIdLog_t = 0;

    if ($mysqli->query($strSQL_t)) {

        $intIdLog_t = $mysqli->insert_id;

    }

    return $intIdLog_t;

}

/**
 *JDBD - En esta funcion retorna el IdPrograma  segun el programa.
 *@param string
 *@return integer. 
 */
function codigoPrograma($strPrograma_p){

    $intIdPrograma_t = 0;

    switch($strPrograma_p){
        case 'TÉCNICO LABORAL POR COMPETENCIAS DE AUXILIAR EN ENFERMERÍA' :
            $intIdPrograma_t=44991;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR ADMINISTRATIVO' :
            $intIdPrograma_t=44988;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR ADMINISTRATIVO EN HOTELERÍA' :
            $intIdPrograma_t=44986;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR ADMINISTRATIVO EN SALUD' :
            $intIdPrograma_t=44990;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR ATENCION INTEGRAL A LA PRIMERA INFANCIA' :
            $intIdPrograma_t=44987;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR CONTABLE' :
            $intIdPrograma_t=44989;
            break;
        case 'TECNICO LABORAL POR COMPETENCIAS EN AUXILIAR DE ENSAMBLE MANTENIMIENTO Y REPARACIÓN DE HARDWARE' :
            $intIdPrograma_t=44998;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR DE NOMINA Y PRESTACIONES' :
            $intIdPrograma_t=45000;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR DE SALUD PÚBLICA' :
            $intIdPrograma_t=44993;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR EN MERCADEO Y VENTAS' :
            $intIdPrograma_t=44985;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR EN SALUD ORAL' :
            $intIdPrograma_t=44992;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR EN VETERINARIA' :
            $intIdPrograma_t=46508;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN AUXILIAR JUDICIAL' :
            $intIdPrograma_t=44995;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN COCINA' :
            $intIdPrograma_t=45001;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN CONFECCIÓN Y PATRONAJE DE MODAS' :
            $intIdPrograma_t=44996;
            break;
        case 'TECNICO LABORAL POR COMPETENCIAS EN COSMETOLOGIA Y ESTETICA INTEGRAL' :
            $intIdPrograma_t=44997;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN MECANICA AUTOMOTRIZ' :
            $intIdPrograma_t=44999;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN MECANICA DIESEL' :
            $intIdPrograma_t=46509;
            break;
        case 'TÉCNICO LABORAL POR COMPETENCIAS EN SERVICIOS FARMACÉUTICOS' :
            $intIdPrograma_t=44994;
            break;
    }

    return $intIdPrograma_t;

}
?>