<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2969_ConsInte__b, G2969_FechaInsercion , G2969_Usuario ,  G2969_CodigoMiembro  , G2969_PoblacionOrigen , G2969_EstadoDiligenciamiento ,  G2969_IdLlamada , G2969_C59267 as principal ,G2969_C59266,G2969_C59267,G2969_C59268,G2969_C59269,G2969_C59270,G2969_C59271,G2969_C59896,G2969_C60940,G2969_C60941,G2969_C62440,G2969_C59263,G2969_C59264,G2969_C59265,G2969_C59338,G2969_C59339,G2969_C59340,G2969_C59341,G2969_C59342,G2969_C59343,G2969_C60143,G2969_C60144 FROM '.$BaseDatos.'.G2969 WHERE G2969_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2969_C59266'] = $key->G2969_C59266;

                $datos[$i]['G2969_C59267'] = $key->G2969_C59267;

                $datos[$i]['G2969_C59268'] = $key->G2969_C59268;

                $datos[$i]['G2969_C59269'] = $key->G2969_C59269;

                $datos[$i]['G2969_C59270'] = $key->G2969_C59270;

                $datos[$i]['G2969_C59271'] = $key->G2969_C59271;

                $datos[$i]['G2969_C59896'] = $key->G2969_C59896;

                $datos[$i]['G2969_C60940'] = $key->G2969_C60940;

                $datos[$i]['G2969_C60941'] = $key->G2969_C60941;

                $datos[$i]['G2969_C62440'] = $key->G2969_C62440;

                $datos[$i]['G2969_C59263'] = $key->G2969_C59263;

                $datos[$i]['G2969_C59264'] = $key->G2969_C59264;

                $datos[$i]['G2969_C59265'] = $key->G2969_C59265;

                $datos[$i]['G2969_C59338'] = $key->G2969_C59338;

                $datos[$i]['G2969_C59339'] = $key->G2969_C59339;

                $datos[$i]['G2969_C59340'] = $key->G2969_C59340;

                $datos[$i]['G2969_C59341'] = $key->G2969_C59341;

                $datos[$i]['G2969_C59342'] = $key->G2969_C59342;

                $datos[$i]['G2969_C59343'] = $key->G2969_C59343;

                $datos[$i]['G2969_C60143'] = $key->G2969_C60143;

                $datos[$i]['G2969_C60144'] = $key->G2969_C60144;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2969";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2969_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2969_ConsInte__b as id,  G2969_C59266 as camp2 , G2969_C59267 as camp1 
                     FROM ".$BaseDatos.".G2969  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2969_ConsInte__b as id,  G2969_C59266 as camp2 , G2969_C59267 as camp1  
                    FROM ".$BaseDatos.".G2969  JOIN ".$BaseDatos.".G2969_M".$_POST['muestra']." ON G2969_ConsInte__b = G2969_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2969_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2969_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2969_C59266 LIKE '%".$B."%' OR G2969_C59267 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2969_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2969_C60940'])){
                                $Ysql = "SELECT G3038_ConsInte__b as id, G3038_C60938 as text FROM ".$BaseDatos.".G3038 WHERE G3038_C60938 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2969_C60940"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3038 WHERE G3038_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2969_C60940"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2969");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2969_ConsInte__b, G2969_FechaInsercion , G2969_Usuario ,  G2969_CodigoMiembro  , G2969_PoblacionOrigen , G2969_EstadoDiligenciamiento ,  G2969_IdLlamada , G2969_C59267 as principal ,G2969_C59266,G2969_C59267,G2969_C59268,G2969_C59269,G2969_C59270,G2969_C59271, a.LISOPC_Nombre____b as G2969_C59896, G3038_C60938,G2969_C60941,G2969_C62440,G2969_C59263,G2969_C59264, b.LISOPC_Nombre____b as G2969_C59265,G2969_C59338,G2969_C59339,G2969_C59340,G2969_C59341,G2969_C59342,G2969_C59343,G2969_C60143,G2969_C60144 FROM '.$BaseDatos.'.G2969 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2969_C59896 LEFT JOIN '.$BaseDatos.'.G3038 ON G3038_ConsInte__b  =  G2969_C60940 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2969_C59265';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2969_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2969_ConsInte__b , ($fila->G2969_C59266) , ($fila->G2969_C59267) , ($fila->G2969_C59268) , ($fila->G2969_C59269) , ($fila->G2969_C59270) , ($fila->G2969_C59271) , ($fila->G2969_C59896) , ($fila->G3038_C60938) , ($fila->G2969_C60941) , ($fila->G2969_C62440) , ($fila->G2969_C59263) , ($fila->G2969_C59264) , ($fila->G2969_C59265) , ($fila->G2969_C59338) , ($fila->G2969_C59339) , ($fila->G2969_C59340) , ($fila->G2969_C59341) , ($fila->G2969_C59342) , ($fila->G2969_C59343) , ($fila->G2969_C60143) , ($fila->G2969_C60144) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2969";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2969_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2969_ConsInte__b as id,  G2969_C59266 as camp2 , G2969_C59267 as camp1  FROM '.$BaseDatos.'.G2969 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2969_ConsInte__b as id,  G2969_C59266 as camp2 , G2969_C59267 as camp1  
                    FROM ".$BaseDatos.".G2969  JOIN ".$BaseDatos.".G2969_M".$_POST['muestra']." ON G2969_ConsInte__b = G2969_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2969_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2969_C59266 LIKE "%'.$B.'%" OR G2969_C59267 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2969_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2969 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2969(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2969_C59266"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59266 = '".$_POST["G2969_C59266"]."'";
                $LsqlI .= $separador."G2969_C59266";
                $LsqlV .= $separador."'".$_POST["G2969_C59266"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59267"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59267 = '".$_POST["G2969_C59267"]."'";
                $LsqlI .= $separador."G2969_C59267";
                $LsqlV .= $separador."'".$_POST["G2969_C59267"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59268"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59268 = '".$_POST["G2969_C59268"]."'";
                $LsqlI .= $separador."G2969_C59268";
                $LsqlV .= $separador."'".$_POST["G2969_C59268"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59269"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59269 = '".$_POST["G2969_C59269"]."'";
                $LsqlI .= $separador."G2969_C59269";
                $LsqlV .= $separador."'".$_POST["G2969_C59269"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59270"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59270 = '".$_POST["G2969_C59270"]."'";
                $LsqlI .= $separador."G2969_C59270";
                $LsqlV .= $separador."'".$_POST["G2969_C59270"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59271"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59271 = '".$_POST["G2969_C59271"]."'";
                $LsqlI .= $separador."G2969_C59271";
                $LsqlV .= $separador."'".$_POST["G2969_C59271"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59896"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59896 = '".$_POST["G2969_C59896"]."'";
                $LsqlI .= $separador."G2969_C59896";
                $LsqlV .= $separador."'".$_POST["G2969_C59896"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C60940"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C60940 = '".$_POST["G2969_C60940"]."'";
                $LsqlI .= $separador."G2969_C60940";
                $LsqlV .= $separador."'".$_POST["G2969_C60940"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C60941"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C60941 = '".$_POST["G2969_C60941"]."'";
                $LsqlI .= $separador."G2969_C60941";
                $LsqlV .= $separador."'".$_POST["G2969_C60941"]."'";
                $validar = 1;
            }
             
  
            $G2969_C62440 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2969_C62440"])){
                if($_POST["G2969_C62440"] == 'Yes'){
                    $G2969_C62440 = 1;
                }else if($_POST["G2969_C62440"] == 'off'){
                    $G2969_C62440 = 0;
                }else if($_POST["G2969_C62440"] == 'on'){
                    $G2969_C62440 = 1;
                }else if($_POST["G2969_C62440"] == 'No'){
                    $G2969_C62440 = 1;
                }else{
                    $G2969_C62440 = $_POST["G2969_C62440"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2969_C62440 = ".$G2969_C62440."";
                $LsqlI .= $separador." G2969_C62440";
                $LsqlV .= $separador.$G2969_C62440;

                $validar = 1;
            }
  

            if(isset($_POST["G2969_C59263"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59263 = '".$_POST["G2969_C59263"]."'";
                $LsqlI .= $separador."G2969_C59263";
                $LsqlV .= $separador."'".$_POST["G2969_C59263"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59264"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59264 = '".$_POST["G2969_C59264"]."'";
                $LsqlI .= $separador."G2969_C59264";
                $LsqlV .= $separador."'".$_POST["G2969_C59264"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59265"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59265 = '".$_POST["G2969_C59265"]."'";
                $LsqlI .= $separador."G2969_C59265";
                $LsqlV .= $separador."'".$_POST["G2969_C59265"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59338"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59338 = '".$_POST["G2969_C59338"]."'";
                $LsqlI .= $separador."G2969_C59338";
                $LsqlV .= $separador."'".$_POST["G2969_C59338"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59339"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59339 = '".$_POST["G2969_C59339"]."'";
                $LsqlI .= $separador."G2969_C59339";
                $LsqlV .= $separador."'".$_POST["G2969_C59339"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59340"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59340 = '".$_POST["G2969_C59340"]."'";
                $LsqlI .= $separador."G2969_C59340";
                $LsqlV .= $separador."'".$_POST["G2969_C59340"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59341"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59341 = '".$_POST["G2969_C59341"]."'";
                $LsqlI .= $separador."G2969_C59341";
                $LsqlV .= $separador."'".$_POST["G2969_C59341"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59342"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59342 = '".$_POST["G2969_C59342"]."'";
                $LsqlI .= $separador."G2969_C59342";
                $LsqlV .= $separador."'".$_POST["G2969_C59342"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59343"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59343 = '".$_POST["G2969_C59343"]."'";
                $LsqlI .= $separador."G2969_C59343";
                $LsqlV .= $separador."'".$_POST["G2969_C59343"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C60143"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C60143 = '".$_POST["G2969_C60143"]."'";
                $LsqlI .= $separador."G2969_C60143";
                $LsqlV .= $separador."'".$_POST["G2969_C60143"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C60144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C60144 = '".$_POST["G2969_C60144"]."'";
                $LsqlI .= $separador."G2969_C60144";
                $LsqlV .= $separador."'".$_POST["G2969_C60144"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2969_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2969_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2969_Usuario , G2969_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2969_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2969 SET G2969_UltiGest__b =-14, G2969_GesMasImp_b =-14, G2969_TipoReintentoUG_b =0, G2969_TipoReintentoGMI_b =0, G2969_ClasificacionUG_b =3, G2969_ClasificacionGMI_b =3, G2969_EstadoUG_b =-14, G2969_EstadoGMI_b =-14, G2969_CantidadIntentos =0, G2969_CantidadIntentosGMI_b =0 WHERE G2969_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
