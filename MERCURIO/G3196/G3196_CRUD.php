<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3196_ConsInte__b, G3196_FechaInsercion , G3196_Usuario ,  G3196_CodigoMiembro  , G3196_PoblacionOrigen , G3196_EstadoDiligenciamiento ,  G3196_IdLlamada , G3196_C64745 as principal ,G3196_C64753,G3196_C65065,G3196_C64734,G3196_C64735,G3196_C64736,G3196_C64737,G3196_C64738,G3196_C64739,G3196_C64740,G3196_C64741,G3196_C64742,G3196_C64745,G3196_C64746,G3196_C64747,G3196_C64748,G3196_C64749,G3196_C64750,G3196_C64751,G3196_C64752 FROM '.$BaseDatos.'.G3196 WHERE G3196_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3196_C64753'] = $key->G3196_C64753;

                $datos[$i]['G3196_C65065'] = $key->G3196_C65065;

                $datos[$i]['G3196_C64734'] = $key->G3196_C64734;

                $datos[$i]['G3196_C64735'] = $key->G3196_C64735;

                $datos[$i]['G3196_C64736'] = explode(' ', $key->G3196_C64736)[0];
  
                $hora = '';
                if(!is_null($key->G3196_C64737)){
                    $hora = explode(' ', $key->G3196_C64737)[1];
                }

                $datos[$i]['G3196_C64737'] = $hora;

                $datos[$i]['G3196_C64738'] = $key->G3196_C64738;

                $datos[$i]['G3196_C64739'] = $key->G3196_C64739;

                $datos[$i]['G3196_C64740'] = $key->G3196_C64740;

                $datos[$i]['G3196_C64741'] = $key->G3196_C64741;

                $datos[$i]['G3196_C64742'] = $key->G3196_C64742;

                $datos[$i]['G3196_C64745'] = $key->G3196_C64745;

                $datos[$i]['G3196_C64746'] = $key->G3196_C64746;

                $datos[$i]['G3196_C64747'] = $key->G3196_C64747;

                $datos[$i]['G3196_C64748'] = $key->G3196_C64748;

                $datos[$i]['G3196_C64749'] = $key->G3196_C64749;

                $datos[$i]['G3196_C64750'] = $key->G3196_C64750;

                $datos[$i]['G3196_C64751'] = $key->G3196_C64751;

                $datos[$i]['G3196_C64752'] = $key->G3196_C64752;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3196";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3196_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3196_ConsInte__b as id,  G3196_C64745 as camp1 , G3196_C64750 as camp2 
                     FROM ".$BaseDatos.".G3196  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3196_ConsInte__b as id,  G3196_C64745 as camp1 , G3196_C64750 as camp2  
                    FROM ".$BaseDatos.".G3196  JOIN ".$BaseDatos.".G3196_M".$_POST['muestra']." ON G3196_ConsInte__b = G3196_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3196_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3196_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3196_C64745 LIKE '%".$B."%' OR G3196_C64750 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3196_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3196");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3196_ConsInte__b, G3196_FechaInsercion , G3196_Usuario ,  G3196_CodigoMiembro  , G3196_PoblacionOrigen , G3196_EstadoDiligenciamiento ,  G3196_IdLlamada , G3196_C64745 as principal ,G3196_C64753,G3196_C65065, a.LISOPC_Nombre____b as G3196_C64734, b.LISOPC_Nombre____b as G3196_C64735,G3196_C64736,G3196_C64737,G3196_C64738,G3196_C64739,G3196_C64740,G3196_C64741,G3196_C64742,G3196_C64745,G3196_C64746,G3196_C64747,G3196_C64748,G3196_C64749,G3196_C64750,G3196_C64751,G3196_C64752 FROM '.$BaseDatos.'.G3196 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3196_C64734 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3196_C64735';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3196_C64737)){
                    $hora_a = explode(' ', $fila->G3196_C64737)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3196_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3196_ConsInte__b , ($fila->G3196_C64753) , ($fila->G3196_C65065) , ($fila->G3196_C64734) , ($fila->G3196_C64735) , explode(' ', $fila->G3196_C64736)[0] , $hora_a , ($fila->G3196_C64738) , ($fila->G3196_C64739) , ($fila->G3196_C64740) , ($fila->G3196_C64741) , ($fila->G3196_C64742) , ($fila->G3196_C64745) , ($fila->G3196_C64746) , ($fila->G3196_C64747) , ($fila->G3196_C64748) , ($fila->G3196_C64749) , ($fila->G3196_C64750) , ($fila->G3196_C64751) , ($fila->G3196_C64752) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3196 WHERE G3196_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    //echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3196";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3196_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3196_ConsInte__b as id,  G3196_C64745 as camp1 , G3196_C64750 as camp2  FROM '.$BaseDatos.'.G3196 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3196_ConsInte__b as id,  G3196_C64745 as camp1 , G3196_C64750 as camp2  
                    FROM ".$BaseDatos.".G3196  JOIN ".$BaseDatos.".G3196_M".$_POST['muestra']." ON G3196_ConsInte__b = G3196_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3196_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3196_C64745 LIKE "%'.$B.'%" OR G3196_C64750 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3196_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3196 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3196(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3196_C64753"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64753 = '".$_POST["G3196_C64753"]."'";
                $LsqlI .= $separador."G3196_C64753";
                $LsqlV .= $separador."'".$_POST["G3196_C64753"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C65065"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C65065 = '".$_POST["G3196_C65065"]."'";
                $LsqlI .= $separador."G3196_C65065";
                $LsqlV .= $separador."'".$_POST["G3196_C65065"]."'";
                $validar = 1;
            }
             
 
            $G3196_C64734 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3196_C64734 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G3196_C64734 = ".$G3196_C64734;
                    $LsqlI .= $separador." G3196_C64734";
                    $LsqlV .= $separador.$G3196_C64734;
                    $validar = 1;

                    
                }
            }
 
            $G3196_C64735 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3196_C64735 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G3196_C64735 = ".$G3196_C64735;
                    $LsqlI .= $separador." G3196_C64735";
                    $LsqlV .= $separador.$G3196_C64735;
                    $validar = 1;
                }
            }
 
            $G3196_C64736 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3196_C64736 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G3196_C64736 = ".$G3196_C64736;
                    $LsqlI .= $separador." G3196_C64736";
                    $LsqlV .= $separador.$G3196_C64736;
                    $validar = 1;
                }
            }
 
            $G3196_C64737 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3196_C64737 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G3196_C64737 = ".$G3196_C64737;
                    $LsqlI .= $separador." G3196_C64737";
                    $LsqlV .= $separador.$G3196_C64737;
                    $validar = 1;
                }
            }
 
            $G3196_C64738 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3196_C64738 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G3196_C64738 = ".$G3196_C64738;
                    $LsqlI .= $separador." G3196_C64738";
                    $LsqlV .= $separador.$G3196_C64738;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3196_C64739"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64739 = '".$_POST["G3196_C64739"]."'";
                $LsqlI .= $separador."G3196_C64739";
                $LsqlV .= $separador."'".$_POST["G3196_C64739"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64740"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64740 = '".$_POST["G3196_C64740"]."'";
                $LsqlI .= $separador."G3196_C64740";
                $LsqlV .= $separador."'".$_POST["G3196_C64740"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64741"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64741 = '".$_POST["G3196_C64741"]."'";
                $LsqlI .= $separador."G3196_C64741";
                $LsqlV .= $separador."'".$_POST["G3196_C64741"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64742"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64742 = '".$_POST["G3196_C64742"]."'";
                $LsqlI .= $separador."G3196_C64742";
                $LsqlV .= $separador."'".$_POST["G3196_C64742"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64745"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64745 = '".$_POST["G3196_C64745"]."'";
                $LsqlI .= $separador."G3196_C64745";
                $LsqlV .= $separador."'".$_POST["G3196_C64745"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64746"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64746 = '".$_POST["G3196_C64746"]."'";
                $LsqlI .= $separador."G3196_C64746";
                $LsqlV .= $separador."'".$_POST["G3196_C64746"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64747"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64747 = '".$_POST["G3196_C64747"]."'";
                $LsqlI .= $separador."G3196_C64747";
                $LsqlV .= $separador."'".$_POST["G3196_C64747"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64748"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64748 = '".$_POST["G3196_C64748"]."'";
                $LsqlI .= $separador."G3196_C64748";
                $LsqlV .= $separador."'".$_POST["G3196_C64748"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64749"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64749 = '".$_POST["G3196_C64749"]."'";
                $LsqlI .= $separador."G3196_C64749";
                $LsqlV .= $separador."'".$_POST["G3196_C64749"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64750"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64750 = '".$_POST["G3196_C64750"]."'";
                $LsqlI .= $separador."G3196_C64750";
                $LsqlV .= $separador."'".$_POST["G3196_C64750"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64751"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64751 = '".$_POST["G3196_C64751"]."'";
                $LsqlI .= $separador."G3196_C64751";
                $LsqlV .= $separador."'".$_POST["G3196_C64751"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3196_C64752"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_C64752 = '".$_POST["G3196_C64752"]."'";
                $LsqlI .= $separador."G3196_C64752";
                $LsqlV .= $separador."'".$_POST["G3196_C64752"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G3196_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G3196_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3196_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3196_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3196_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3196_Usuario , G3196_FechaInsercion, G3196_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3196_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3196 WHERE G3196_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        //echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3196_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3196 WHERE G3196_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3197_ConsInte__b, G3197_C64757, G3197_C64758, G3197_C64759, G3197_C64760, G3197_C64761, G3197_C64762, G3197_C64763, h.LISOPC_Nombre____b as  G3197_C64764, G3197_C64766, G3197_C64767, G3197_C64768, G3197_C64769, G3197_C64770, G3197_C64771, G3197_C64772, p.LISOPC_Nombre____b as  G3197_C64773, G3197_C64774, r.LISOPC_Nombre____b as  G3197_C64775, s.LISOPC_Nombre____b as  G3197_C64776, t.LISOPC_Nombre____b as  G3197_C64777, G3197_C64778, G3197_C64779, w.LISOPC_Nombre____b as  G3197_C64785 FROM ".$BaseDatos.".G3197  LEFT JOIN ".$BaseDatos_systema.".LISOPC as h ON h.LISOPC_ConsInte__b =  G3197_C64764 LEFT JOIN ".$BaseDatos_systema.".LISOPC as p ON p.LISOPC_ConsInte__b =  G3197_C64773 LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G3197_C64775 LEFT JOIN ".$BaseDatos_systema.".LISOPC as s ON s.LISOPC_ConsInte__b =  G3197_C64776 LEFT JOIN ".$BaseDatos_systema.".LISOPC as t ON t.LISOPC_ConsInte__b =  G3197_C64777 LEFT JOIN ".$BaseDatos_systema.".LISOPC as w ON w.LISOPC_ConsInte__b =  G3197_C64785 ";

        if (isset($_GET["agente"])) {

            $SQL .= " WHERE G3197_C65476 = '".$_GET["agente"]."' AND G3197_C64785 = 44527 "; 
            
        }else{

            $SQL .= " WHERE G3197_C64768 = '".$numero."'"; 

        }
        

        $SQL .= " ORDER BY G3197_C64757";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3197_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3197_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G3197_C64757."</cell>"; 

                if($fila->G3197_C64758 != ''){
                    echo "<cell>". explode(' ', $fila->G3197_C64758)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3197_C64759 != ''){
                    echo "<cell>". explode(' ', $fila->G3197_C64759)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3197_C64760 != ''){
                    echo "<cell>". explode(' ', $fila->G3197_C64760)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3197_C64761 != ''){
                    echo "<cell>". explode(' ', $fila->G3197_C64761)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3197_C64762 != ''){
                    echo "<cell>". explode(' ', $fila->G3197_C64762)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3197_C64763 != ''){
                    echo "<cell>". explode(' ', $fila->G3197_C64763)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G3197_C64764)."</cell>";

                echo "<cell>". ($fila->G3197_C64766)."</cell>";

                echo "<cell>". ($fila->G3197_C64767)."</cell>";

                echo "<cell>". ($fila->G3197_C64768)."</cell>";

                echo "<cell>". ($fila->G3197_C64769)."</cell>";

                echo "<cell>". ($fila->G3197_C64770)."</cell>";

                echo "<cell>". ($fila->G3197_C64771)."</cell>";

                echo "<cell>". ($fila->G3197_C64772)."</cell>";

                echo "<cell>". ($fila->G3197_C64773)."</cell>";

                echo "<cell>". ($fila->G3197_C64774)."</cell>";

                echo "<cell>". ($fila->G3197_C64775)."</cell>";

                echo "<cell>". ($fila->G3197_C64776)."</cell>";

                echo "<cell>". ($fila->G3197_C64777)."</cell>";

                echo "<cell>". ($fila->G3197_C64778)."</cell>";

                echo "<cell><![CDATA[". ($fila->G3197_C64779)."]]></cell>";

                echo "<cell>". ($fila->G3197_C64785)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3197 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3197(";
            $LsqlV = " VALUES ("; 
 
                $G3197_C64757= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G3197_C64757"])){    
                    if($_POST["G3197_C64757"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64757 = $_POST["G3197_C64757"];
                        $LsqlU .= $separador." G3197_C64757 = '".$G3197_C64757."'";
                        $LsqlI .= $separador." G3197_C64757";
                        $LsqlV .= $separador."'".$G3197_C64757."'";
                        $validar = 1;
                    }
                }

                $G3197_C64758 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3197_C64758"])){    
                    if($_POST["G3197_C64758"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64758 = "'".str_replace(' ', '',$_POST["G3197_C64758"])." 00:00:00'";
                        $LsqlU .= $separador." G3197_C64758 = ".$G3197_C64758;
                        $LsqlI .= $separador." G3197_C64758";
                        $LsqlV .= $separador.$G3197_C64758;
                        $validar = 1;
                    }
                }
 
                $G3197_C64759 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G3197_C64759"])){    
                    if($_POST["G3197_C64759"] != '' && $_POST["G3197_C64759"] != 'undefined' && $_POST["G3197_C64759"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64759 = "'".$fecha." ".str_replace(' ', '',$_POST["G3197_C64759"])."'";
                        $LsqlU .= $separador."  G3197_C64759 = ".$G3197_C64759."";
                        $LsqlI .= $separador."  G3197_C64759";
                        $LsqlV .= $separador.$G3197_C64759;
                        $validar = 1;
                    }
                }

                $G3197_C64760 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3197_C64760"])){    
                    if($_POST["G3197_C64760"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64760 = "'".str_replace(' ', '',$_POST["G3197_C64760"])." 00:00:00'";
                        $LsqlU .= $separador." G3197_C64760 = ".$G3197_C64760;
                        $LsqlI .= $separador." G3197_C64760";
                        $LsqlV .= $separador.$G3197_C64760;
                        $validar = 1;
                    }
                }
 
                $G3197_C64761 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G3197_C64761"])){    
                    if($_POST["G3197_C64761"] != '' && $_POST["G3197_C64761"] != 'undefined' && $_POST["G3197_C64761"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64761 = "'".$fecha." ".str_replace(' ', '',$_POST["G3197_C64761"])."'";
                        $LsqlU .= $separador."  G3197_C64761 = ".$G3197_C64761."";
                        $LsqlI .= $separador."  G3197_C64761";
                        $LsqlV .= $separador.$G3197_C64761;
                        $validar = 1;
                    }
                }

                $G3197_C64762 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3197_C64762"])){    
                    if($_POST["G3197_C64762"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64762 = "'".str_replace(' ', '',$_POST["G3197_C64762"])." 00:00:00'";
                        $LsqlU .= $separador." G3197_C64762 = ".$G3197_C64762;
                        $LsqlI .= $separador." G3197_C64762";
                        $LsqlV .= $separador.$G3197_C64762;
                        $validar = 1;
                    }
                }
 
                $G3197_C64763 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G3197_C64763"])){    
                    if($_POST["G3197_C64763"] != '' && $_POST["G3197_C64763"] != 'undefined' && $_POST["G3197_C64763"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3197_C64763 = "'".$fecha." ".str_replace(' ', '',$_POST["G3197_C64763"])."'";
                        $LsqlU .= $separador."  G3197_C64763 = ".$G3197_C64763."";
                        $LsqlI .= $separador."  G3197_C64763";
                        $LsqlV .= $separador.$G3197_C64763;
                        $validar = 1;
                    }
                }
 
                if(isset($_POST["G3197_C64764"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64764 = '".$_POST["G3197_C64764"]."'";
                    $LsqlI .= $separador."G3197_C64764";
                    $LsqlV .= $separador."'".$_POST["G3197_C64764"]."'";
                    $validar = 1;
                }
 

                if(isset($_POST["G3197_C64766"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64766 = '".$_POST["G3197_C64766"]."'";
                    $LsqlI .= $separador."G3197_C64766";
                    $LsqlV .= $separador."'".$_POST["G3197_C64766"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G3197_C64767"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64767 = '".$_POST["G3197_C64767"]."'";
                    $LsqlI .= $separador."G3197_C64767";
                    $LsqlV .= $separador."'".$_POST["G3197_C64767"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G3197_C64769"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64769 = '".$_POST["G3197_C64769"]."'";
                    $LsqlI .= $separador."G3197_C64769";
                    $LsqlV .= $separador."'".$_POST["G3197_C64769"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G3197_C64770"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64770 = '".$_POST["G3197_C64770"]."'";
                    $LsqlI .= $separador."G3197_C64770";
                    $LsqlV .= $separador."'".$_POST["G3197_C64770"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G3197_C64771"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64771 = '".$_POST["G3197_C64771"]."'";
                    $LsqlI .= $separador."G3197_C64771";
                    $LsqlV .= $separador."'".$_POST["G3197_C64771"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G3197_C64772"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64772 = '".$_POST["G3197_C64772"]."'";
                    $LsqlI .= $separador."G3197_C64772";
                    $LsqlV .= $separador."'".$_POST["G3197_C64772"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G3197_C64773"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64773 = '".$_POST["G3197_C64773"]."'";
                    $LsqlI .= $separador."G3197_C64773";
                    $LsqlV .= $separador."'".$_POST["G3197_C64773"]."'";
                    $validar = 1;
                }
 

                if(isset($_POST["G3197_C64774"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64774 = '".$_POST["G3197_C64774"]."'";
                    $LsqlI .= $separador."G3197_C64774";
                    $LsqlV .= $separador."'".$_POST["G3197_C64774"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G3197_C64775"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64775 = '".$_POST["G3197_C64775"]."'";
                    $LsqlI .= $separador."G3197_C64775";
                    $LsqlV .= $separador."'".$_POST["G3197_C64775"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G3197_C64776"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64776 = '".$_POST["G3197_C64776"]."'";
                    $LsqlI .= $separador."G3197_C64776";
                    $LsqlV .= $separador."'".$_POST["G3197_C64776"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G3197_C64777"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64777 = '".$_POST["G3197_C64777"]."'";
                    $LsqlI .= $separador."G3197_C64777";
                    $LsqlV .= $separador."'".$_POST["G3197_C64777"]."'";
                    $validar = 1;
                }
 

                if(isset($_POST["G3197_C64778"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64778 = '".$_POST["G3197_C64778"]."'";
                    $LsqlI .= $separador."G3197_C64778";
                    $LsqlV .= $separador."'".$_POST["G3197_C64778"]."'";
                    $validar = 1;
                }

                                                                               
  

                if(isset($_POST["G3197_C64779"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64779 = '".$_POST["G3197_C64779"]."'";
                    $LsqlI .= $separador."G3197_C64779";
                    $LsqlV .= $separador."'".$_POST["G3197_C64779"]."'";
                    $validar = 1;
                }
                                                                               
 
                if(isset($_POST["G3197_C64785"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3197_C64785 = '".$_POST["G3197_C64785"]."'";
                    $LsqlI .= $separador."G3197_C64785";
                    $LsqlV .= $separador."'".$_POST["G3197_C64785"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3197_C64768 = $numero;
                    $LsqlU .= ", G3197_C64768 = ".$G3197_C64768."";
                    $LsqlI .= ", G3197_C64768";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3197_Usuario ,  G3197_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3197_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3197 WHERE  G3197_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
