<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2993/G2993_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2993_ConsInte__b as id, G2993_C59737 as camp1 , G2993_C59738 as camp2 FROM ".$BaseDatos.".G2993  WHERE G2993_Usuario = ".$idUsuario." ORDER BY G2993_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2993_ConsInte__b as id, G2993_C59737 as camp1 , G2993_C59738 as camp2 FROM ".$BaseDatos.".G2993  ORDER BY G2993_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2993_ConsInte__b as id, G2993_C59737 as camp1 , G2993_C59738 as camp2 FROM ".$BaseDatos.".G2993 JOIN ".$BaseDatos.".G2993_M".$resultEstpas->muestr." ON G2993_ConsInte__b = G2993_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2993_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2993_ConsInte__b as id, G2993_C59737 as camp1 , G2993_C59738 as camp2 FROM ".$BaseDatos.".G2993 JOIN ".$BaseDatos.".G2993_M".$resultEstpas->muestr." ON G2993_ConsInte__b = G2993_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2993_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2993_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G2993_ConsInte__b as id, G2993_C59737 as camp1 , G2993_C59738 as camp2 FROM ".$BaseDatos.".G2993  ORDER BY G2993_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="9135" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9135c">
                DATOS AUTOMATICOS DEL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9135c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2993_C59737" id="LblG2993_C59737">Número de caso</label>
                        <input noEditar="si" type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"]) && isset($_GET["view"])){  if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 59737")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 59737");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }
                             }?>" readonly name="G2993_C59737" id="G2993_C59737" placeholder="Número de caso">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2993_C59738" id="LblG2993_C59738">Fecha creación</label>
                        <input noEditar="si" type="text" readonly class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G2993_C59738" id="G2993_C59738" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G2993_C59739" id="LblG2993_C59739">Hora creación</label>
                            <div class="input-group">
                                <input noEditar="si" type="text" readonly class="form-control input-sm Hora" value="<?=date("H:i:s");?>" name="G2993_C59739" id="G2993_C59739" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G2993_C59739">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2993_C60130" id="LblG2993_C60130">Fecha última gestión</label>
                        <input noEditar="si" type="text" class="form-control input-sm Fecha" readonly name="G2993_C60130" id="G2993_C60130" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G2993_C60131" id="LblG2993_C60131">Hora última gestión</label>
                            <div class="input-group">
                                <input noEditar="si" type="text" readonly class="form-control input-sm Hora"  name="G2993_C60131" id="G2993_C60131" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_GG2993_C60131">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->                       
                        
            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2993_C59740" id="LblG2993_C59740">Fecha cierre</label>
                        <input noEditar="si" type="text" class="form-control input-sm Fecha" readonly name="G2993_C59740" id="G2993_C59740" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->
        
        <div class="row">
        
            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G2993_C59741" id="LblG2993_C59741">Hora cierre</label>
                            <div class="input-group">
                                <input noEditar="si" type="text" readonly class="form-control input-sm Hora"  name="G2993_C59741" id="G2993_C59741" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G2993_C59741">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->    
            
        </div>


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9136" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9136c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9136c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59734" id="LblG2993_C59734">ORIGEN_DY_WF</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59734" value="<?php if (isset($_GET['G2993_C59734'])) {
                            echo $_GET['G2993_C59734'];
                        } ?>" readonly name="G2993_C59734"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59735" id="LblG2993_C59735">OPTIN_DY_WF</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59735" value="<?php if (isset($_GET['G2993_C59735'])) {
                            echo $_GET['G2993_C59735'];
                        } ?>" readonly name="G2993_C59735"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2993_C59736" id="LblG2993_C59736">ESTADO_DY</label>
                        <select noEditar="si"  class="form-control input-sm select2"  style="width: 100%;" name="G2993_C59736" id="G2993_C59736">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3631 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9146" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9146c">
                DATOS A DILIGENCIAR PARA CREAR EL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9146c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G2993_C59770" id="LblG2993_C59770">Selección de consultor</label>
                                <select noEditar="si" class="form-control input-sm select2" style="width: 100%;"  name="G2993_C59770" id="G2993_C59770">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59771" id="LblG2993_C59771">Código consultor</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59771" value="<?php if (isset($_GET['G2993_C59771'])) {
                            echo $_GET['G2993_C59771'];
                        } ?>" readonly name="G2993_C59771"  placeholder="Código consultor"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59772" id="LblG2993_C59772">Cédula</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59772" value="<?php if (isset($_GET['G2993_C59772'])) {
                            echo $_GET['G2993_C59772'];
                        } ?>" readonly name="G2993_C59772"  placeholder="Cédula"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59773" id="LblG2993_C59773">Nombre</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59773" value="<?php if (isset($_GET['G2993_C59773'])) {
                            echo $_GET['G2993_C59773'];
                        } ?>" readonly name="G2993_C59773"  placeholder="Nombre"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C60146" id="LblG2993_C60146">Código Gerencia</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C60146" value="<?php if (isset($_GET['G2993_C60146'])) {
                            echo $_GET['G2993_C60146'];
                        } ?>" readonly name="G2993_C60146"  placeholder="Código Gerencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->            
            
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59774" id="LblG2993_C59774">Gerencia</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59774" value="<?php if (isset($_GET['G2993_C59774'])) {
                            echo $_GET['G2993_C59774'];
                        } ?>" readonly name="G2993_C59774"  placeholder="Gerencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div>         
        
        <div class="row">


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C60145" id="LblG2993_C60145">Código Sector</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C60145" value="<?php if (isset($_GET['G2993_C60145'])) {
                            echo $_GET['G2993_C60145'];
                        } ?>" readonly name="G2993_C60145"  placeholder="Código Sector"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->            
            
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59775" id="LblG2993_C59775">Sector</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59775" value="<?php if (isset($_GET['G2993_C59775'])) {
                            echo $_GET['G2993_C59775'];
                        } ?>" readonly name="G2993_C59775"  placeholder="Sector"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 
        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G2993_C60320" id="LblG2993_C60320">Ciudad</label>
                                <select noEditar="si" class="form-control input-sm select2" style="width: 100%;"  name="G2993_C60320" id="G2993_C60320">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C60321" id="LblG2993_C60321">Departamento</label>
                        <input noEditar="si" readonly type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2993_C60321" value="<?php if (isset($_GET['G2993_C60321'])) {
                            echo $_GET['G2993_C60321'];
                        } ?>"  name="G2993_C60321"  placeholder="Departamento"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2993_C60981" id="LblG2993_C60981">Tipo de cliente</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2993_C60981" id="G2993_C60981">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3636 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59776" id="LblG2993_C59776">Nombre del responsable</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59776" value="<?php if (isset($_GET['G2993_C59776'])) {
                            echo $_GET['G2993_C59776'];
                        } ?>" readonly name="G2993_C59776"  placeholder="Nombre del responsable"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59777" id="LblG2993_C59777">Correo corporativo</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59777" value="<?php if (isset($_GET['G2993_C59777'])) {
                            echo $_GET['G2993_C59777'];
                        } ?>" readonly name="G2993_C59777"  placeholder="Correo corporativo"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C59778" id="LblG2993_C59778">Teléfono</label>
                        <input noEditar="si" type="text" class="form-control input-sm" id="G2993_C59778" value="<?php if (isset($_GET['G2993_C59778'])) {
                            echo $_GET['G2993_C59778'];
                        } ?>" readonly name="G2993_C59778"  placeholder="Teléfono"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2993_C59803" id="LblG2993_C59803">Tipificación</label>
                        <select noEditar="si"  class="form-control input-sm select2"  style="width: 100%;" name="G2993_C59803" id="G2993_C59803">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3637 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C60325" id="LblG2993_C60325">Codigo</label>
                        <input readonly noEditar="si" type="text" class="form-control input-sm" id="G2993_C60325" value="<?php if (isset($_GET['G2993_C60325'])) {
                            echo $_GET['G2993_C60325'];
                        } ?>" readonly name="G2993_C60325"  placeholder="Codigo"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->



        </div> 


        <div class="row">
            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2993_C59804" id="LblG2993_C59804">Subtipificación</label>
                        <select noEditar="si" class="form-control input-sm select2"  style="width: 100%;" name="G2993_C59804" id="G2993_C59804">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3638 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->
            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C60326" id="LblG2993_C60326">Clasificacion</label>
                        <input readonly noEditar="si" type="text" class="form-control input-sm" id="G2993_C60326" value="<?php if (isset($_GET['G2993_C60326'])) {
                            echo $_GET['G2993_C60326'];
                        } ?>" readonly name="G2993_C60326"  placeholder="Clasificacion"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->
        




        </div> <!-- AQUIFINSALDO1 -->

        <div class="row">
            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2993_C59805" id="LblG2993_C59805">Tipo de escalamiento</label>
                        <select noEditar="si" class="form-control input-sm select2"  style="width: 100%;" name="G2993_C59805" id="G2993_C59805">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3639 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->
              <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2993_C60327" id="LblG2993_C60327">SLA</label>
                        <input readonly noEditar="si" type="text" class="form-control input-sm" id="G2993_C60327" value="<?php if (isset($_GET['G2993_C60327'])) {
                            echo $_GET['G2993_C60327'];
                        } ?>" readonly name="G2993_C60327"  placeholder="SLA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->
        



        </div> <!-- AQUIFINSALDO1 -->

        <div class="row">
                
            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2993_C59806" id="LblG2993_C59806">Observaciones</label>
                        <textarea noEditar="si" class="form-control input-sm" name="G2993_C59806" id="G2993_C59806"  value="<?php if (isset($_GET['G2993_C59806'])) {
                            echo $_GET['G2993_C59806'];
                        } ?>" placeholder="Observaciones"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div>


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9152" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9152c">
                DATOS GESTION DEL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9152c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2993_C59897" id="LblG2993_C59897">ESTADO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2993_C59897" id="G2993_C59897">
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3599 AND LISOPC_ConsInte__b != 44526 ORDER BY LISOPC_ConsInte__b DESC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

<div class=row>
                        <div class="col-md-12 col-xs-12">
                            
<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">Gestiones del caso</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear Gestiones del caso" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
                        </div>
                    </div>
                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include("pies.php");?>
<script type="text/javascript" src="formularios/G2993/G2993_eventos.js"></script>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        var data=JSON.parse(e.data);
        console.log(data);
        $("#G2993_C60130").val(data['fecha']);
        $("#G2993_C60131").val(data['hora']); 
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){

        $(".form-control").blur(function(){

            var today = moment().format('YYYY-MM-DD');
            var todayH = moment().format('HH:mm:ss');

            $("#G2993_C60130").val(today);
            $("#G2993_C60131").val(todayH);

        });

    $("#G2993_C59770").change(function(){
        var valor = $(this).attr("opt");
        if ($(this).val()) {
            valor = $(this).val();
        }
        $.ajax({
            url   : "<?php echo $url_crud;?>",
            data  : { dameValoresCamposDinamicos_Guion_G2993_C59770 : valor},
            type  : "post",
            dataType : "json",
            success  : function(data){
                $("#G2993_C59770").html('<option value="'+data.G2969_ConsInte__b+'" >'+data.G2969_C59266+'</option>');
                
                $("#G2993_C59771").val(data.G2969_C59266);
                $("#G2993_C59772").val(data.G2969_C59268);
                $("#G2993_C59773").val(data.G2969_C59267);
                $("#G2993_C59774").val(data.G2969_C59341);
                $("#G2993_C59775").val(data.G2969_C59339);
                $("#G2993_C59776").val(data.G2969_C59340);
                $("#G2993_C59777").val(data.G2969_C59342);
                $("#G2993_C59778").val(data.G2969_C59343);
                $("#G2993_C60146").val(data.G2969_C60144);
                $("#G2993_C60145").val(data.G2969_C60143);

                $("#G2993_C60320").attr("opt",data.G2969_C60940);
                $("#G2993_C60320").val(data.G2969_C60940).trigger("change");
                


            }
        });
    });
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
                $("#G2993_C59770").attr("opt","<?=$_GET['idFather'];?>"); 
                $("#G2993_C59770").val("<?=$_GET['idFather'];?>").trigger("change"); 
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            //JDBD - Creamos un nuevo id incrementable y lo asignamos a este campo
            $.ajax({
                url:'<?=$url_crud;?>',
                type:'POST',
                data:{INCTB:"si"},
                success:function(data){
                    $("#G2993_C59737").val(data);
                }
            });
            //JDBD - Damos el valor fecha actual.
            $("#G2993_C59738").val("<?=date("Y-m-d");?>");
            $("#G2993_C59803").val("0").trigger("change");
            $("#G2993_C59804").val("0").trigger("change");
            $("#G2993_C59805").val("0").trigger("change");
            $("#G2993_C59897").val("0").trigger("change");             
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G2993_C60325").val(item.G2993_C60325); 
                $("#G2993_C60326").val(item.G2993_C60326); 
                $("#G2993_C60327").val(item.G2993_C60327); 

                $("#G2993_C59737").val(item.G2993_C59737); 
                $("#G2993_C60981").val(item.G2993_C60981).trigger("change");  
                $("#G2993_C59738").val(item.G2993_C59738); 
                $("#G2993_C59739").val(item.G2993_C59739); 
                $("#G2993_C60130").val(item.G2993_C60130); 
                $("#G2993_C60131").val(item.G2993_C60131); 
                $("#G2993_C59740").val(item.G2993_C59740); 
                $("#G2993_C59741").val(item.G2993_C59741); 
                $("#G2993_C59734").val(item.G2993_C59734);
                $("#G2993_C60320").attr("opt",item.G2993_C60320);
                $("#G2993_C60320").val(item.G2993_C60320).trigger("change"); 
                $("#G2993_C60321").val(item.G2993_C60321); 
                $("#G2993_C59735").val(item.G2993_C59735); 
                $("#G2993_C59736").val(item.G2993_C59736).trigger("change");  
                $("#G2993_C59770").attr("opt",item.G2993_C59770);
                $("#G2993_C59770").val(item.G2993_C59770).trigger("change"); 
                $("#G2993_C59771").val(item.G2993_C59771); 
                $("#G2993_C59772").val(item.G2993_C59772); 
                $("#G2993_C59773").val(item.G2993_C59773); 
                $("#G2993_C59774").val(item.G2993_C59774); 
                $("#G2993_C60145").val(item.G2993_C60145); 
                $("#G2993_C60146").val(item.G2993_C60146); 
                $("#G2993_C59775").val(item.G2993_C59775); 
                $("#G2993_C59776").val(item.G2993_C59776); 
                $("#G2993_C59777").val(item.G2993_C59777); 
                $("#G2993_C59778").val(item.G2993_C59778); 
                $("#G2993_C59803").val(item.G2993_C59803).trigger("change");  
                $("#G2993_C59804").attr("opt",item.G2993_C59804);  
                $("#G2993_C59805").attr("opt",item.G2993_C59805);   
                $("#G2993_C59806").val(item.G2993_C59806); 
                // $("#G2993_C59897").val(item.G2993_C59897).trigger("change"); 
                $("#G2993_C59897 option[value="+item.G2993_C59897+"]").attr("selected",true);

                if (item.G2993_C59897 == "44527") {

                    $("#edit").hide();
                    $("#delete").hide();

                }else{

                    $("#edit").show();
                    $("#delete").show();

                }
                
                cargarHijos_0(
        $("#G2993_C59737").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G2993_C59737").val());
            var id_0 = $("#G2993_C59737").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G2993_C59737").val());
            var id_0 = $("#G2993_C59737").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G2993_C59737").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2992&view=si&formaDetalle=si&formularioPadre=2993&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=59733<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2992&view=si&formaDetalle=si&formularioPadre=2993&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=59733&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2992&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=2993&pincheCampo=59733&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        
                            $("#G2993_C60320").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2993_C60320=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G2993_C60320").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G2993_C60320 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G2993_C60320").html('<option value="'+data.G3038_ConsInte__b+'" >'+data.G3038_C60938+'</option>');
                                        $("#G2993_C60321").val(data.G3038_C60939);
                                        
                                    }
                                });
                            });

    $("#G2993_C59736").select2();
                           $("#G2993_C59770").select2({
                               placeholder: "Buscar",
                               allowClear: false,
                               minimumInputLength: 3,
                               ajax:{
                                   url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2993_C59770=si',
                                   dataType: 'json',
                                   type : 'post',
                                   delay: 250,
                                   data: function (params) {
                                       return {
                                           q: params.term
                                       };
                                   },
                                   processResults: function(data) {
                                        if (data.length > 0) {
                                            $("#G2993_C59771").attr("readonly",true);
                                            $("#G2993_C59772").attr("readonly",true);
                                            $("#G2993_C59773").attr("readonly",true);
                                            $("#G2993_C59774").attr("readonly",true);
                                            $("#G2993_C59775").attr("readonly",true);
                                            $("#G2993_C59776").attr("readonly",true);
                                            $("#G2993_C59777").attr("readonly",true);
                                            $("#G2993_C59778").attr("readonly",true);
                                            $("#G2993_C60146").attr("readonly",true);
                                            $("#G2993_C60145").attr("readonly",true);
                                            $("#G2993_C60320").attr("readonly",true);
                                        }else{
                                            $("#G2993_C59771").attr("readonly",false);
                                            $("#G2993_C59772").attr("readonly",false);
                                            $("#G2993_C59773").attr("readonly",false);
                                            $("#G2993_C59774").attr("readonly",false);
                                            $("#G2993_C59775").attr("readonly",false);
                                            $("#G2993_C59776").attr("readonly",false);
                                            $("#G2993_C59777").attr("readonly",false);
                                            $("#G2993_C59778").attr("readonly",false);
                                            $("#G2993_C60146").attr("readonly",false);
                                            $("#G2993_C60145").attr("readonly",false);
                                            $("#G2993_C60320").attr("readonly",false);
                                        }
                                       return {
                                           results: $.map(data, function(obj) {
                                               return {id: obj.id,text: obj.text};
                                           })
                                       };
                                   },
                                   cache: true
                               }
                           });



    $("#G2993_C59803").select2();

    $("#G2993_C59804").select2();
        //datepickers
        

/*
        $("#G2993_C59740").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });
*/

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora creación', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2993_C59739").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora cierre', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        // $("#G2993_C59741").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G2993_C59737").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G2993_C59736").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipificación 

    $("#G2993_C59803").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3638' , idPadre : $(this).val() },
            success : function(data){
                var optG2993_C59804 = $("#G2993_C59804").attr("opt");
                $("#G2993_C59804").html(data);
                if (optG2993_C59804 != null) {
                    $("#G2993_C59804").val(optG2993_C59804).trigger("change");
                }
            }
        });
        
    });


    //function para Tipo de escalamiento 

    $("#G2993_C59804").change(function(){  

        var optG2993_C59805 = $("#G2993_C59805").attr("opt");

        var arrN2_t = ["44912","44913","44914","44916","44917","45407","45410","45402","45404","45411","45412","45397","45398"];
        var arrN1N2_t = ["45386","45408","45403","45389","45396","45399","44926"];

        if (arrN2_t.includes($(this).val())) {

            $("#G2993_C59805").html('<option value="44930">N2</option>');
            $("#G2993_C59805").val("44930").trigger("change");

        }else if(arrN1N2_t.includes($(this).val())){

            $("#G2993_C59805").html('<option value="44929">N1</option><option value="44930">N2</option>');
            $("#G2993_C59805").val("44929").trigger("change");

        }else{

            $("#G2993_C59805").html('<option value="44929">N1</option>');
            $("#G2993_C59805").val("44929").trigger("change");

        }

        if (optG2993_C59805 != null) {

            $("#G2993_C59805").val(optG2993_C59805).trigger("change");


        }

        switch($(this).val()){

            case "44911":

                $("#G2993_C60325").val("156");

                break;

            case "44915":

                $("#G2993_C60325").val("157");

                break;

            case "44912":

                $("#G2993_C60325").val("158");

                break;

            case "44913":

                $("#G2993_C60325").val("159");

                break;

            case "44914":

                $("#G2993_C60325").val("160");

                break;

            case "44916":

                $("#G2993_C60325").val("161");

                break;

            case "44917":

                $("#G2993_C60325").val("162");

                break;

            case "45386":

                $("#G2993_C60325").val("163");

                break;

            case "45387":

                $("#G2993_C60325").val("164");

                break;

            case "45388":

                $("#G2993_C60325").val("165");

                break;

            case "45405":

                $("#G2993_C60325").val("166");

                break;

            case "45406":

                $("#G2993_C60325").val("167");

                break;

            case "45407":

                $("#G2993_C60325").val("168");

                break;

            case "45408":

                $("#G2993_C60325").val("169");

                break;

            case "45409":

                $("#G2993_C60325").val("170");

                break;

            case "45410":

                $("#G2993_C60325").val("171");

                break;

            case "45401":

                $("#G2993_C60325").val("172");

                break;

            case "45402":

                $("#G2993_C60325").val("173");

                break;

            case "45403":

                $("#G2993_C60325").val("174");

                break;

            case "45404":

                $("#G2993_C60325").val("175");

                break;

            case "45411":

                $("#G2993_C60325").val("176");

                break;

            case "45412":

                $("#G2993_C60325").val("177");

                break;

            case "45413":

                $("#G2993_C60325").val("178");

                break;

            case "44922":

                $("#G2993_C60325").val("179");

                break;

            case "45389":

                $("#G2993_C60325").val("180");

                break;

            case "45390":

                $("#G2993_C60325").val("181");

                break;

            case "45391":

                $("#G2993_C60325").val("182");

                break;

            case "45392":

                $("#G2993_C60325").val("183");

                break;

            case "45393":

                $("#G2993_C60325").val("184");

                break;

            case "45394":

                $("#G2993_C60325").val("185");

                break;

            case "45395":

                $("#G2993_C60325").val("186");

                break;

            case "44923":

                $("#G2993_C60325").val("187");

                break;

            case "45396":

                $("#G2993_C60325").val("188");

                break;

            case "44924":

                $("#G2993_C60325").val("189");

                break;

            case "45397":

                $("#G2993_C60325").val("190");

                break;

            case "45398":

                $("#G2993_C60325").val("191");

                break;

            case "45399":

                $("#G2993_C60325").val("192");

                break;

            case "44925":

                $("#G2993_C60325").val("193");

                break;

            case "44926":

                $("#G2993_C60325").val("194");

                break;

            case "44927":

                $("#G2993_C60325").val("195");

                break;

            case "44928":

                $("#G2993_C60325").val("196");

                break;

        }

var arrConsulta_t = ["44922","45389","45390","45391","45392","45393","45394","45395","44927","44915"];
var arrReclamo_t = ["45405","45406","45407","45408","45409","45386","44917","44916","44914","44913","44912","45410","45401","45402","45403","45404","45411","45412","45413","45633"];
var arrSolicitud_t = ["45388","45387","44911","44923","45396","44924","45397","45398","45399","44925","44926","44928"];

            if (arrConsulta_t.includes($(this).val())) {

                $("#G2993_C60326").val("Consulta");

            }else if(arrReclamo_t.includes($(this).val())){

                $("#G2993_C60326").val("Reclamo");

            }else if(arrSolicitud_t.includes($(this).val())){
                
                $("#G2993_C60326").val("Solicitud");

            }


var arrSLANull_t = ["45405","45406","45409","45388","45387","44915","44911","45401","45413","44922","45390","45391","45392","45393","45394","45395","44923","44924","44925","44927","44928"];

            if (arrSLANull_t.includes($(this).val())) {


            }else{
                
                $("#G2993_C60327").val("24H");

            }

    });


    $("#G2993_C59805").change(function(){ 

            if ($(this).val() == "44930") {

                $("#G2993_C59897 > option[value='44952']").attr("disabled",false);
                $("#G2993_C59897").val("44952").trigger("change");

            }else if ($(this).val() == "44929"){

                $("#G2993_C59897 > option[value='44527']").attr("disabled",false);
                $("#G2993_C59897 > option[value='44952']").attr("disabled",true);
                $("#G2993_C59897").val("44527").trigger("change");

            }

    });

    //function para ESTADO 

    $("#G2993_C59897").change(function(){  

        if ($(this).val()=="44527") {

            var today = moment().format('YYYY-MM-DD');
            var todayH = moment().format('HH:mm:ss');

            $("#G2993_C59740").val(today);
            $("#G2993_C59741").val(todayH);

        }else{
            
            $("#G2993_C59740").val("");
            $("#G2993_C59741").val("");
        }
        
    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            if ($("#G2993_C60981").val() == 0 || $("#G2993_C60981").val() == -1 || $("#G2993_C60981").val() == null) {

                $("#G2993_C60981").closest(".form-group").addClass("has-error");
                valido = 1;

            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            let booValido=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido = 1;
                        alertify.error("El número de teléfono digitado no es valido");
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });            

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
                                                $("#G2993_C60325").val(item.G2993_C60325); 
                                                $("#G2993_C60326").val(item.G2993_C60326); 
                                                $("#G2993_C60327").val(item.G2993_C60327);

                                                $("#G2993_C59737").val(item.G2993_C59737);
 
                                                $("#G2993_C59738").val(item.G2993_C59738);
 
                                                $("#G2993_C59739").val(item.G2993_C59739);
                                                
                                                $("#G2993_C60130").val(item.G2993_C60130); 
                                                
                                                $("#G2993_C60131").val(item.G2993_C60131);

                                                $("#G2993_C60981").val(item.G2993_C60981).trigger("change");  

                                                $("#G2993_C60320").attr("opt",item.G2993_C60320);
                                                $("#G2993_C60320").val(item.G2993_C60320).trigger("change"); 
                                                $("#G2993_C60321").val(item.G2993_C60321); 
 
                                                $("#G2993_C59740").val(item.G2993_C59740);
 
                                                $("#G2993_C59741").val(item.G2993_C59741);
 
                                                $("#G2993_C59734").val(item.G2993_C59734);
 
                                                $("#G2993_C59735").val(item.G2993_C59735);
 
                                                $("#G2993_C59736").val(item.G2993_C59736).trigger("change"); 
                             
                                                $("#G2993_C59770").attr("opt",item.G2993_C59770);
                                                $("#G2993_C59770").val(item.G2993_C59770).trigger("change");
 
                                                $("#G2993_C59771").val(item.G2993_C59771);
 
                                                $("#G2993_C59772").val(item.G2993_C59772);
 
                                                $("#G2993_C59773").val(item.G2993_C59773);
 
                                                $("#G2993_C59774").val(item.G2993_C59774);
                                                
                                                $("#G2993_C60145").val(item.G2993_C60145);
                                                
                                                $("#G2993_C60146").val(item.G2993_C60146);
 
                                                $("#G2993_C59775").val(item.G2993_C59775);
 
                                                $("#G2993_C59776").val(item.G2993_C59776);
 
                                                $("#G2993_C59777").val(item.G2993_C59777);
 
                                                $("#G2993_C59778").val(item.G2993_C59778);
 
                    $("#G2993_C59803").val(item.G2993_C59803).trigger("change"); 
 
                    $("#G2993_C59804").attr("opt",item.G2993_C59804); 
 
                    $("#G2993_C59805").attr("opt",item.G2993_C59805);
 
                                                $("#G2993_C59806").val(item.G2993_C59806);
 
                    // $("#G2993_C59897").val(item.G2993_C59897).trigger("change");
                    $("#G2993_C59897 option[value="+item.G2993_C59897+"]").attr("selected",true); 

                    if (item.G2993_C59897 == "44527") {

                        $("#edit").hide();
                        $("#delete").hide();

                    }else{

                        $("#edit").show();
                        $("#delete").show();

                    }
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Número de caso','Fecha creación','Hora creación','Fecha cierre','Hora cierre','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','Selección de consultor','Código consultor','Cédula','Nombre','Gerencia','Sector','Nombre del responsable','Correo corporativo','Teléfono','Tipificación','Subtipificación','Tipo de escalamiento','Observaciones','ESTADO'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G2993_C59737', 
                        index:'G2993_C59737', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    {  
                        name:'G2993_C59738', 
                        index:'G2993_C59738', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2993_C59739', 
                        index:'G2993_C59739', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'Hora creación', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2993_C60130', 
                        index:'G2993_C60130G2993_C60131', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2993_C60131', 
                        index:'G2993_C60131', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'Hora última gestión', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }                    
                
                    ,
                    {  
                        name:'G2993_C59740', 
                        index:'G2993_C59740', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2993_C59741', 
                        index:'G2993_C59741', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'Hora cierre', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2993_C59734', 
                        index: 'G2993_C59734', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59735', 
                        index: 'G2993_C59735', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59736', 
                        index:'G2993_C59736', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3631&campo=G2993_C59736'
                        }
                    }

                    ,
                    { 
                        name:'G2993_C59770', 
                        index:'G2993_C59770', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G2993_C59770=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2993_C59771', 
                        index: 'G2993_C59771', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59772', 
                        index: 'G2993_C59772', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59773', 
                        index: 'G2993_C59773', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59774', 
                        index: 'G2993_C59774', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                
                    ,
                    { 
                        name:'G2993_C60145', 
                        index: 'G2993_C60145', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                
                    ,
                    { 
                        name:'G2993_C60146', 
                        index: 'G2993_C60146', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59775', 
                        index: 'G2993_C59775', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59776', 
                        index: 'G2993_C59776', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59777', 
                        index: 'G2993_C59777', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59778', 
                        index: 'G2993_C59778', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59803', 
                        index:'G2993_C59803', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3637&campo=G2993_C59803'
                        }
                    }

                    ,
                    { 
                        name:'G2993_C59804', 
                        index:'G2993_C59804', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3638&campo=G2993_C59804'
                        }
                    }

                    ,
                    { 
                        name:'G2993_C59805', 
                        index:'G2993_C59805', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3639&campo=G2993_C59805'
                        }
                    }

                    ,
                    { 
                        name:'G2993_C59806', 
                        index:'G2993_C59806', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2993_C59897', 
                        index:'G2993_C59897', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3599&campo=G2993_C59897'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2993_C59737',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
            ,subGrid: true,
            subGridRowExpanded: function(subgrid_id, row_id) { 
                // we pass two parameters 
                // subgrid_id is a id of the div tag created whitin a table data 
                // the id of this elemenet is a combination of the "sg_" + id of the row 
                // the row_id is the id of the row 
                // If we wan to pass additinal parameters to the url we can use 
                // a method getRowData(row_id) - which returns associative array in type name-value 
                // here we can easy construct the flowing 
                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Comentario de la gestión','Responsable de la gestión','Fecha gestión','Hora gestión','Número del caso al que pertenece', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G2992_C59729', 
                                index:'G2992_C59729', 
                                width:150, 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2992_C59730', 
                                index: 'G2992_C59730', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            {  
                                name:'G2992_C59731', 
                                index:'G2992_C59731', 
                                width:120 ,
                                editable: true ,
                                formatter: 'text', 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).datepicker({
                                            language: "es",
                                            autoclose: true,
                                            todayHighlight: true
                                        });
                                    },
                                    defaultValue: function(){
                                        var currentTime = new Date();
                                        var month = parseInt(currentTime.getMonth() + 1);
                                        month = month <= 9 ? "0"+month : month;
                                        var day = currentTime.getDate();
                                        day = day <= 9 ? "0"+day : day;
                                        var year = currentTime.getFullYear();
                                        return year+"-"+month + "-"+day;
                                    }
                                }
                            }

                            ,
                            {  
                                name:'G2992_C59732', 
                                index:'G2992_C59732', 
                                width:70 ,
                                editable: true ,
                                formatter: 'text', 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        //Timepicker
                                         var options = {  //hh:mm 24 hour format only, defaults to current time
                                            timeFormat: 'HH:mm:ss',
                                            interval: 5,
                                            minTime: '10',
                                            dynamic: false,
                                            dropdown: true,
                                            scrollbar: true
                                        }; 
                                        $(el).timepicker(options);


                                    }
                                }
                            }
 
                            ,
                            {  
                                name:'G2992_C59733', 
                                index:'G2992_C59733', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                }, 
                subGridRowColapsed: function(subgrid_id, row_id) { 
                    // this function is called before removing the data 
                    //var subgrid_table_id; 
                    //subgrid_table_id = subgrid_id+"_t"; 
                    //jQuery("#"+subgrid_table_id).remove(); 
                }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            
                        $("#G2993_C60325").val(item.G2993_C60325); 
                        $("#G2993_C60326").val(item.G2993_C60326); 
                        $("#G2993_C60327").val(item.G2993_C60327);

                        $("#G2993_C59737").val(item.G2993_C59737);

                        $("#G2993_C59738").val(item.G2993_C59738);

                        $("#G2993_C59739").val(item.G2993_C59739);
                        
                        $("#G2993_C60130").val(item.G2993_C60130);
                        
                        $("#G2993_C60320").attr("opt",item.G2993_C60320);
                        $("#G2993_C60320").val(item.G2993_C60320).trigger("change"); 
                        $("#G2993_C60321").val(item.G2993_C60321); 

                        $("#G2993_C60131").val(item.G2993_C60131);

                        $("#G2993_C59740").val(item.G2993_C59740);

                        $("#G2993_C59741").val(item.G2993_C59741);

                        $("#G2993_C60981").val(item.G2993_C60981).trigger("change");  

                        $("#G2993_C59734").val(item.G2993_C59734);

                        $("#G2993_C59735").val(item.G2993_C59735);
 
                        $("#G2993_C59736").val(item.G2993_C59736).trigger("change"); 
 
                        $("#G2993_C59770").attr("opt",item.G2993_C59770);
                        $("#G2993_C59770").val(item.G2993_C59770).trigger("change");

                        $("#G2993_C59771").val(item.G2993_C59771);

                        $("#G2993_C59772").val(item.G2993_C59772);

                        $("#G2993_C59773").val(item.G2993_C59773);

                        $("#G2993_C59774").val(item.G2993_C59774);
                        
                        $("#G2993_C60145").val(item.G2993_C60145);
                                                
                        $("#G2993_C60146").val(item.G2993_C60146);

                        $("#G2993_C59775").val(item.G2993_C59775);

                        $("#G2993_C59776").val(item.G2993_C59776);

                        $("#G2993_C59777").val(item.G2993_C59777);

                        $("#G2993_C59778").val(item.G2993_C59778);
 
                    $("#G2993_C59803").val(item.G2993_C59803).trigger("change"); 
 
                    $("#G2993_C59804").attr("opt",item.G2993_C59804); 
 
                    $("#G2993_C59805").attr("opt",item.G2993_C59805);

                        $("#G2993_C59806").val(item.G2993_C59806);
 
                    // $("#G2993_C59897").val(item.G2993_C59897).trigger("change");
                    $("#G2993_C59897 option[value="+item.G2993_C59897+"]").attr("selected",true); 

                    if (item.G2993_C59897 == "44527") {

                        $("#edit").hide();
                        $("#delete").hide();

                    }else{

                        $("#edit").show();
                        $("#delete").show();

                    }
                        
            cargarHijos_0(
        $("#G2993_C59737").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Comentario de la gestión','Responsable de la gestión','Fecha gestión','Hora gestión','Número del caso al que pertenece', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {
                        name:'G2992_C59729', 
                        index:'G2992_C59729', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2992_C59730', 
                        index: 'G2992_C59730', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2992_C59731', 
                        index:'G2992_C59731', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2992_C59732', 
                        index:'G2992_C59732', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = {  //hh:mm 24 hour format only, defaults to current time
                                    timeFormat: 'HH:mm:ss',
                                    interval: 5,
                                    minTime: '10',
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                }; 
                                $(el).timepicker(options);
                                $(".timepicker").css("z-index", 99999 );
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G2992_C59733', 
                        index:'G2992_C59733', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G2992_C59729',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Gestiones del caso',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2992&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=59733&formularioPadre=2993<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G2993_C59737").val());
            var id_0 = $("#G2993_C59737").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G2993_C59737").val());
            var id_0 = $("#G2993_C59737").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
