<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2972;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2972
                  SET G2972_C61932 = -201
                  WHERE G2972_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2972 
                    WHERE G2972_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2972_C61931"]) || $gestion["G2972_C61931"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2972_C61931"];
        }

        if (is_null($gestion["G2972_C61933"]) || $gestion["G2972_C61933"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2972_C61933"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2972_FechaInsercion"]."',".$gestion["G2972_Usuario"].",'".$gestion["G2972_C".$P["P"]]."','".$gestion["G2972_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "hermes.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2972 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2972_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2972_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2972_LinkContenido as url FROM ".$BaseDatos.".G2972 WHERE G2972_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2972_ConsInte__b, G2972_FechaInsercion , G2972_Usuario ,  G2972_CodigoMiembro  , G2972_PoblacionOrigen , G2972_EstadoDiligenciamiento ,  G2972_IdLlamada , G2972_C59318 as principal ,G2972_C59318,G2972_C60134,G2972_C59319,G2972_C60135,G2972_C59323,G2972_C59324,G2972_C59325,G2972_C59326,G2972_C59296,G2972_C59297,G2972_C59298,G2972_C59299,G2972_C59300,G2972_C59301,G2972_C59302,G2972_C59303,G2972_C59304,G2972_C59327,G2972_C61937,G2972_C61938,G2972_C61939,G2972_C61940,G2972_C61941,G2972_C61942,G2972_C61943,G2972_C61944,G2972_C61932,G2972_C61931,G2972_C61933,G2972_C61934,G2972_C61935,G2972_C61936,G2972_C61945,G2972_C61946,G2972_C61947,G2972_C61948,G2972_C61949,G2972_C61950,G2972_C61955,G2972_C61956,G2972_C61957,G2972_C61958,G2972_C61959,G2972_C61960 FROM '.$BaseDatos.'.G2972 WHERE G2972_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2972_C59318'] = $key->G2972_C59318;

                $datos[$i]['G2972_C60134'] = $key->G2972_C60134;

                $datos[$i]['G2972_C59319'] = $key->G2972_C59319;

                $datos[$i]['G2972_C60135'] = $key->G2972_C60135;

                $datos[$i]['G2972_C59323'] = $key->G2972_C59323;

                $datos[$i]['G2972_C59324'] = $key->G2972_C59324;

                $datos[$i]['G2972_C59325'] = $key->G2972_C59325;

                $datos[$i]['G2972_C59326'] = $key->G2972_C59326;

                $datos[$i]['G2972_C59296'] = $key->G2972_C59296;

                $datos[$i]['G2972_C59297'] = $key->G2972_C59297;

                $datos[$i]['G2972_C59298'] = explode(' ', $key->G2972_C59298)[0];
  
                $hora = '';
                if(!is_null($key->G2972_C59299)){
                    $hora = explode(' ', $key->G2972_C59299)[1];
                }

                $datos[$i]['G2972_C59299'] = $hora;

                $datos[$i]['G2972_C59300'] = $key->G2972_C59300;

                $datos[$i]['G2972_C59301'] = $key->G2972_C59301;

                $datos[$i]['G2972_C59302'] = $key->G2972_C59302;

                $datos[$i]['G2972_C59303'] = $key->G2972_C59303;

                $datos[$i]['G2972_C59304'] = $key->G2972_C59304;

                $datos[$i]['G2972_C59327'] = $key->G2972_C59327;

                $datos[$i]['G2972_C61937'] = $key->G2972_C61937;

                $datos[$i]['G2972_C61938'] = $key->G2972_C61938;

                $datos[$i]['G2972_C61939'] = $key->G2972_C61939;

                $datos[$i]['G2972_C61940'] = $key->G2972_C61940;

                $datos[$i]['G2972_C61941'] = $key->G2972_C61941;

                $datos[$i]['G2972_C61942'] = $key->G2972_C61942;

                $datos[$i]['G2972_C61943'] = $key->G2972_C61943;

                $datos[$i]['G2972_C61944'] = $key->G2972_C61944;

                $datos[$i]['G2972_C61932'] = $key->G2972_C61932;

                $datos[$i]['G2972_C61931'] = $key->G2972_C61931;

                $datos[$i]['G2972_C61933'] = $key->G2972_C61933;

                $datos[$i]['G2972_C61934'] = $key->G2972_C61934;

                $datos[$i]['G2972_C61935'] = explode(' ', $key->G2972_C61935)[0];

                $datos[$i]['G2972_C61936'] = $key->G2972_C61936;

                $datos[$i]['G2972_C61945'] = $key->G2972_C61945;

                $datos[$i]['G2972_C61946'] = $key->G2972_C61946;

                $datos[$i]['G2972_C61947'] = $key->G2972_C61947;

                $datos[$i]['G2972_C61948'] = $key->G2972_C61948;

                $datos[$i]['G2972_C61949'] = $key->G2972_C61949;

                $datos[$i]['G2972_C61950'] = $key->G2972_C61950;

                $datos[$i]['G2972_C61955'] = $key->G2972_C61955;

                $datos[$i]['G2972_C61956'] = $key->G2972_C61956;

                $datos[$i]['G2972_C61957'] = $key->G2972_C61957;

                $datos[$i]['G2972_C61958'] = $key->G2972_C61958;

                $datos[$i]['G2972_C61959'] = $key->G2972_C61959;

                $datos[$i]['G2972_C61960'] = $key->G2972_C61960;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2972";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2972_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2972_ConsInte__b as id,  G2972_C59318 as camp1 , G2972_C59319 as camp2 
                     FROM ".$BaseDatos.".G2972  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2972_ConsInte__b as id,  G2972_C59318 as camp1 , G2972_C59319 as camp2  
                    FROM ".$BaseDatos.".G2972  JOIN ".$BaseDatos.".G2972_M".$_POST['muestra']." ON G2972_ConsInte__b = G2972_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2972_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2972_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2972_C59318 LIKE '%".$B."%' OR G2972_C59319 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2972_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

            if(isset($_GET['MostrarCombo_Guion_2970_C59281'])){
                echo '<select class="form-control input-sm"  name="2970_C59281" id="2970_C59281">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2970_C59281'])){
                $Ysql = "SELECT G2969_ConsInte__b as id,  G2969_C59267 as text FROM ".$BaseDatos.".G2969 WHERE G2969_C59267 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2970_C59281'])){
                $Lsql = "SELECT  G2969_ConsInte__b as id , G2968_C59253, G2968_C59249, G2968_C59251, G2968_C59254, G2968_C59255, G2968_C59257, G2968_C60138, G2968_C59250, G3038_C60939, G2969_C59268, G2969_C59267, G2969_C60143, G2969_C60144, G2969_C59340, G2969_C59342, G2969_C59270, G2969_C60940, G2969_C59339, G2969_C59341 FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2970_C59281'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2970_C59295'] = $key->G2968_C59253;

                    $data[$i]['G2970_C59345'] = $key->G2968_C59249;

                    $data[$i]['G2970_C59346'] = $key->G2968_C59251;

                    $data[$i]['G2970_C59347'] = $key->G2968_C59254;

                    $data[$i]['G2970_C59348'] = $key->G2968_C59255;

                    $data[$i]['G2970_C59349'] = $key->G2968_C59257;

                    $data[$i]['G2970_C60136'] = $key->G2968_C60138;

                    $data[$i]['G2970_C60137'] = $key->G2968_C59250;

                    $data[$i]['G2970_C60311'] = $key->G3038_C60939;

                    $data[$i]['G2970_C59282'] = $key->G2969_C59268;

                    $data[$i]['G2970_C59283'] = $key->G2969_C59267;

                    $data[$i]['G2970_C59346'] = $key->G2969_C60143;

                    $data[$i]['G2970_C59345'] = $key->G2969_C60144;

                    $data[$i]['G2970_C59347'] = $key->G2969_C59340;

                    $data[$i]['G2970_C59348'] = $key->G2969_C59342;

                    $data[$i]['G2970_C59349'] = $key->G2969_C59270;

                    $data[$i]['G2970_C60310'] = $key->G2969_C60940;

                    $data[$i]['G2970_C59346'] = $key->G2969_C59339;

                    $data[$i]['G2970_C59345'] = $key->G2969_C59341;

                    $i++;
                }

                echo json_encode($data);
            }
            

            if(isset($_GET['MostrarCombo_Guion_2970_C59281'])){
                echo '<select class="form-control input-sm"  name="2970_C59281" id="2970_C59281">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2970_C59281'])){
                $Ysql = "SELECT G2969_ConsInte__b as id,  G2969_C59267 as text FROM ".$BaseDatos.".G2969 WHERE G2969_C59267 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2970_C59281'])){
                $Lsql = "SELECT  G2969_ConsInte__b as id , G2968_C59253, G2968_C59249, G2968_C59251, G2968_C59254, G2968_C59255, G2968_C59257, G2968_C60138, G2968_C59250, G3038_C60939, G2969_C59268, G2969_C59267, G2969_C60143, G2969_C60144, G2969_C59340, G2969_C59342, G2969_C59270, G2969_C60940, G2969_C59339, G2969_C59341 FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2970_C59281'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2970_C59295'] = $key->G2968_C59253;

                    $data[$i]['G2970_C59345'] = $key->G2968_C59249;

                    $data[$i]['G2970_C59346'] = $key->G2968_C59251;

                    $data[$i]['G2970_C59347'] = $key->G2968_C59254;

                    $data[$i]['G2970_C59348'] = $key->G2968_C59255;

                    $data[$i]['G2970_C59349'] = $key->G2968_C59257;

                    $data[$i]['G2970_C60136'] = $key->G2968_C60138;

                    $data[$i]['G2970_C60137'] = $key->G2968_C59250;

                    $data[$i]['G2970_C60311'] = $key->G3038_C60939;

                    $data[$i]['G2970_C59282'] = $key->G2969_C59268;

                    $data[$i]['G2970_C59283'] = $key->G2969_C59267;

                    $data[$i]['G2970_C59346'] = $key->G2969_C60143;

                    $data[$i]['G2970_C59345'] = $key->G2969_C60144;

                    $data[$i]['G2970_C59347'] = $key->G2969_C59340;

                    $data[$i]['G2970_C59348'] = $key->G2969_C59342;

                    $data[$i]['G2970_C59349'] = $key->G2969_C59270;

                    $data[$i]['G2970_C60310'] = $key->G2969_C60940;

                    $data[$i]['G2970_C59346'] = $key->G2969_C59339;

                    $data[$i]['G2970_C59345'] = $key->G2969_C59341;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2972");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2972_ConsInte__b, G2972_FechaInsercion , G2972_Usuario ,  G2972_CodigoMiembro  , G2972_PoblacionOrigen , G2972_EstadoDiligenciamiento ,  G2972_IdLlamada , G2972_C59318 as principal ,G2972_C59318,G2972_C60134,G2972_C59319,G2972_C60135,G2972_C59323,G2972_C59324,G2972_C59325,G2972_C59326, a.LISOPC_Nombre____b as G2972_C59296, b.LISOPC_Nombre____b as G2972_C59297,G2972_C59298,G2972_C59299,G2972_C59300,G2972_C59301,G2972_C59302,G2972_C59303,G2972_C59304,G2972_C59327,G2972_C61937,G2972_C61938,G2972_C61939,G2972_C61940,G2972_C61941,G2972_C61942,G2972_C61943,G2972_C61944, c.LISOPC_Nombre____b as G2972_C61932,G2972_C61931,G2972_C61933,G2972_C61934,G2972_C61935,G2972_C61936,G2972_C61945,G2972_C61946,G2972_C61947,G2972_C61948,G2972_C61949,G2972_C61950,G2972_C61955,G2972_C61956,G2972_C61957,G2972_C61958,G2972_C61959,G2972_C61960 FROM '.$BaseDatos.'.G2972 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2972_C59296 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2972_C59297 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2972_C61932';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2972_C59299)){
                    $hora_a = explode(' ', $fila->G2972_C59299)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2972_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2972_ConsInte__b , ($fila->G2972_C59318) , ($fila->G2972_C60134) , ($fila->G2972_C59319) , ($fila->G2972_C60135) , ($fila->G2972_C59323) , ($fila->G2972_C59324) , ($fila->G2972_C59325) , ($fila->G2972_C59326) , ($fila->G2972_C59296) , ($fila->G2972_C59297) , explode(' ', $fila->G2972_C59298)[0] , $hora_a , ($fila->G2972_C59300) , ($fila->G2972_C59301) , ($fila->G2972_C59302) , ($fila->G2972_C59303) , ($fila->G2972_C59304) , ($fila->G2972_C59327) , ($fila->G2972_C61937) , ($fila->G2972_C61938) , ($fila->G2972_C61939) , ($fila->G2972_C61940) , ($fila->G2972_C61941) , ($fila->G2972_C61942) , ($fila->G2972_C61943) , ($fila->G2972_C61944) , ($fila->G2972_C61932) , ($fila->G2972_C61931) , ($fila->G2972_C61933) , ($fila->G2972_C61934) , explode(' ', $fila->G2972_C61935)[0] , ($fila->G2972_C61936) , ($fila->G2972_C61945) , ($fila->G2972_C61946) , ($fila->G2972_C61947) , ($fila->G2972_C61948) , ($fila->G2972_C61949) , ($fila->G2972_C61950) , ($fila->G2972_C61955) , ($fila->G2972_C61956) , ($fila->G2972_C61957) , ($fila->G2972_C61958) , ($fila->G2972_C61959) , ($fila->G2972_C61960) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2972 WHERE G2972_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2972";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2972_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2972_ConsInte__b as id,  G2972_C59318 as camp1 , G2972_C59319 as camp2  FROM '.$BaseDatos.'.G2972 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2972_ConsInte__b as id,  G2972_C59318 as camp1 , G2972_C59319 as camp2  
                    FROM ".$BaseDatos.".G2972  JOIN ".$BaseDatos.".G2972_M".$_POST['muestra']." ON G2972_ConsInte__b = G2972_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2972_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2972_C59318 LIKE "%'.$B.'%" OR G2972_C59319 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2972_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2972 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2972(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2972_C59318"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59318 = '".$_POST["G2972_C59318"]."'";
                $LsqlI .= $separador."G2972_C59318";
                $LsqlV .= $separador."'".$_POST["G2972_C59318"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C60134"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C60134 = '".$_POST["G2972_C60134"]."'";
                $LsqlI .= $separador."G2972_C60134";
                $LsqlV .= $separador."'".$_POST["G2972_C60134"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59319"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59319 = '".$_POST["G2972_C59319"]."'";
                $LsqlI .= $separador."G2972_C59319";
                $LsqlV .= $separador."'".$_POST["G2972_C59319"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C60135"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C60135 = '".$_POST["G2972_C60135"]."'";
                $LsqlI .= $separador."G2972_C60135";
                $LsqlV .= $separador."'".$_POST["G2972_C60135"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59323"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59323 = '".$_POST["G2972_C59323"]."'";
                $LsqlI .= $separador."G2972_C59323";
                $LsqlV .= $separador."'".$_POST["G2972_C59323"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59324"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59324 = '".$_POST["G2972_C59324"]."'";
                $LsqlI .= $separador."G2972_C59324";
                $LsqlV .= $separador."'".$_POST["G2972_C59324"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59325"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59325 = '".$_POST["G2972_C59325"]."'";
                $LsqlI .= $separador."G2972_C59325";
                $LsqlV .= $separador."'".$_POST["G2972_C59325"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59326"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59326 = '".$_POST["G2972_C59326"]."'";
                $LsqlI .= $separador."G2972_C59326";
                $LsqlV .= $separador."'".$_POST["G2972_C59326"]."'";
                $validar = 1;
            }
             
 
            $G2972_C59296 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2972_C59296 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2972_C59296 = ".$G2972_C59296;
                    $LsqlI .= $separador." G2972_C59296";
                    $LsqlV .= $separador.$G2972_C59296;
                    $validar = 1;

                    
                }
            }
 
            $G2972_C59297 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2972_C59297 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2972_C59297 = ".$G2972_C59297;
                    $LsqlI .= $separador." G2972_C59297";
                    $LsqlV .= $separador.$G2972_C59297;
                    $validar = 1;
                }
            }
 
            $G2972_C59298 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2972_C59298 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2972_C59298 = ".$G2972_C59298;
                    $LsqlI .= $separador." G2972_C59298";
                    $LsqlV .= $separador.$G2972_C59298;
                    $validar = 1;
                }
            }
 
            $G2972_C59299 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2972_C59299 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2972_C59299 = ".$G2972_C59299;
                    $LsqlI .= $separador." G2972_C59299";
                    $LsqlV .= $separador.$G2972_C59299;
                    $validar = 1;
                }
            }
 
            $G2972_C59300 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2972_C59300 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2972_C59300 = ".$G2972_C59300;
                    $LsqlI .= $separador." G2972_C59300";
                    $LsqlV .= $separador.$G2972_C59300;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2972_C59301"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59301 = '".$_POST["G2972_C59301"]."'";
                $LsqlI .= $separador."G2972_C59301";
                $LsqlV .= $separador."'".$_POST["G2972_C59301"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59302"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59302 = '".$_POST["G2972_C59302"]."'";
                $LsqlI .= $separador."G2972_C59302";
                $LsqlV .= $separador."'".$_POST["G2972_C59302"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59303"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59303 = '".$_POST["G2972_C59303"]."'";
                $LsqlI .= $separador."G2972_C59303";
                $LsqlV .= $separador."'".$_POST["G2972_C59303"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59304"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59304 = '".$_POST["G2972_C59304"]."'";
                $LsqlI .= $separador."G2972_C59304";
                $LsqlV .= $separador."'".$_POST["G2972_C59304"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59305"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59305 = '".$_POST["G2972_C59305"]."'";
                $LsqlI .= $separador."G2972_C59305";
                $LsqlV .= $separador."'".$_POST["G2972_C59305"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59306"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59306 = '".$_POST["G2972_C59306"]."'";
                $LsqlI .= $separador."G2972_C59306";
                $LsqlV .= $separador."'".$_POST["G2972_C59306"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C59327"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C59327 = '".$_POST["G2972_C59327"]."'";
                $LsqlI .= $separador."G2972_C59327";
                $LsqlV .= $separador."'".$_POST["G2972_C59327"]."'";
                $validar = 1;
            }
             
  
            $G2972_C61937 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61937"])){
                if($_POST["G2972_C61937"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61937 = $_POST["G2972_C61937"];
                    $LsqlU .= $separador." G2972_C61937 = ".$G2972_C61937."";
                    $LsqlI .= $separador." G2972_C61937";
                    $LsqlV .= $separador.$G2972_C61937;
                    $validar = 1;
                }
            }
  
            $G2972_C61938 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61938"])){
                if($_POST["G2972_C61938"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61938 = $_POST["G2972_C61938"];
                    $LsqlU .= $separador." G2972_C61938 = ".$G2972_C61938."";
                    $LsqlI .= $separador." G2972_C61938";
                    $LsqlV .= $separador.$G2972_C61938;
                    $validar = 1;
                }
            }
  
            $G2972_C61939 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61939"])){
                if($_POST["G2972_C61939"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61939 = $_POST["G2972_C61939"];
                    $LsqlU .= $separador." G2972_C61939 = ".$G2972_C61939."";
                    $LsqlI .= $separador." G2972_C61939";
                    $LsqlV .= $separador.$G2972_C61939;
                    $validar = 1;
                }
            }
  
            $G2972_C61940 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61940"])){
                if($_POST["G2972_C61940"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61940 = $_POST["G2972_C61940"];
                    $LsqlU .= $separador." G2972_C61940 = ".$G2972_C61940."";
                    $LsqlI .= $separador." G2972_C61940";
                    $LsqlV .= $separador.$G2972_C61940;
                    $validar = 1;
                }
            }
  
            $G2972_C61941 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61941"])){
                if($_POST["G2972_C61941"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61941 = $_POST["G2972_C61941"];
                    $LsqlU .= $separador." G2972_C61941 = ".$G2972_C61941."";
                    $LsqlI .= $separador." G2972_C61941";
                    $LsqlV .= $separador.$G2972_C61941;
                    $validar = 1;
                }
            }
  
            $G2972_C61942 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61942"])){
                if($_POST["G2972_C61942"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61942 = $_POST["G2972_C61942"];
                    $LsqlU .= $separador." G2972_C61942 = ".$G2972_C61942."";
                    $LsqlI .= $separador." G2972_C61942";
                    $LsqlV .= $separador.$G2972_C61942;
                    $validar = 1;
                }
            }
  
            $G2972_C61943 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61943"])){
                if($_POST["G2972_C61943"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61943 = $_POST["G2972_C61943"];
                    $LsqlU .= $separador." G2972_C61943 = ".$G2972_C61943."";
                    $LsqlI .= $separador." G2972_C61943";
                    $LsqlV .= $separador.$G2972_C61943;
                    $validar = 1;
                }
            }
  
            $G2972_C61944 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61944"])){
                if($_POST["G2972_C61944"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61944 = $_POST["G2972_C61944"];
                    $LsqlU .= $separador." G2972_C61944 = ".$G2972_C61944."";
                    $LsqlI .= $separador." G2972_C61944";
                    $LsqlV .= $separador.$G2972_C61944;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2972_C61932"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C61932 = '".$_POST["G2972_C61932"]."'";
                $LsqlI .= $separador."G2972_C61932";
                $LsqlV .= $separador."'".$_POST["G2972_C61932"]."'";
                $validar = 1;
            }
             
  
            $G2972_C61931 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61931"])){
                if($_POST["G2972_C61931"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61931 = $_POST["G2972_C61931"];
                    $LsqlU .= $separador." G2972_C61931 = ".$G2972_C61931."";
                    $LsqlI .= $separador." G2972_C61931";
                    $LsqlV .= $separador.$G2972_C61931;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2972_C61933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C61933 = '".$_POST["G2972_C61933"]."'";
                $LsqlI .= $separador."G2972_C61933";
                $LsqlV .= $separador."'".$_POST["G2972_C61933"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2972_C61934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C61934 = '".$_POST["G2972_C61934"]."'";
                $LsqlI .= $separador."G2972_C61934";
                $LsqlV .= $separador."'".$_POST["G2972_C61934"]."'";
                $validar = 1;
            }
             
 
            $G2972_C61935 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2972_C61935"])){    
                if($_POST["G2972_C61935"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2972_C61935"]);
                    if(count($tieneHora) > 1){
                        $G2972_C61935 = "'".$_POST["G2972_C61935"]."'";
                    }else{
                        $G2972_C61935 = "'".str_replace(' ', '',$_POST["G2972_C61935"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2972_C61935 = ".$G2972_C61935;
                    $LsqlI .= $separador." G2972_C61935";
                    $LsqlV .= $separador.$G2972_C61935;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2972_C61936"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_C61936 = '".$_POST["G2972_C61936"]."'";
                $LsqlI .= $separador."G2972_C61936";
                $LsqlV .= $separador."'".$_POST["G2972_C61936"]."'";
                $validar = 1;
            }
             
  
            $G2972_C61945 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61945"])){
                if($_POST["G2972_C61945"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61945 = $_POST["G2972_C61945"];
                    $LsqlU .= $separador." G2972_C61945 = ".$G2972_C61945."";
                    $LsqlI .= $separador." G2972_C61945";
                    $LsqlV .= $separador.$G2972_C61945;
                    $validar = 1;
                }
            }
  
            $G2972_C61946 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61946"])){
                if($_POST["G2972_C61946"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61946 = $_POST["G2972_C61946"];
                    $LsqlU .= $separador." G2972_C61946 = ".$G2972_C61946."";
                    $LsqlI .= $separador." G2972_C61946";
                    $LsqlV .= $separador.$G2972_C61946;
                    $validar = 1;
                }
            }
  
            $G2972_C61947 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61947"])){
                if($_POST["G2972_C61947"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61947 = $_POST["G2972_C61947"];
                    $LsqlU .= $separador." G2972_C61947 = ".$G2972_C61947."";
                    $LsqlI .= $separador." G2972_C61947";
                    $LsqlV .= $separador.$G2972_C61947;
                    $validar = 1;
                }
            }
  
            $G2972_C61948 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61948"])){
                if($_POST["G2972_C61948"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61948 = $_POST["G2972_C61948"];
                    $LsqlU .= $separador." G2972_C61948 = ".$G2972_C61948."";
                    $LsqlI .= $separador." G2972_C61948";
                    $LsqlV .= $separador.$G2972_C61948;
                    $validar = 1;
                }
            }
  
            $G2972_C61949 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61949"])){
                if($_POST["G2972_C61949"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61949 = $_POST["G2972_C61949"];
                    $LsqlU .= $separador." G2972_C61949 = ".$G2972_C61949."";
                    $LsqlI .= $separador." G2972_C61949";
                    $LsqlV .= $separador.$G2972_C61949;
                    $validar = 1;
                }
            }
  
            $G2972_C61950 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61950"])){
                if($_POST["G2972_C61950"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61950 = $_POST["G2972_C61950"];
                    $LsqlU .= $separador." G2972_C61950 = ".$G2972_C61950."";
                    $LsqlI .= $separador." G2972_C61950";
                    $LsqlV .= $separador.$G2972_C61950;
                    $validar = 1;
                }
            }
  
            $G2972_C61955 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61955"])){
                if($_POST["G2972_C61955"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61955 = $_POST["G2972_C61955"];
                    $LsqlU .= $separador." G2972_C61955 = ".$G2972_C61955."";
                    $LsqlI .= $separador." G2972_C61955";
                    $LsqlV .= $separador.$G2972_C61955;
                    $validar = 1;
                }
            }
  
            $G2972_C61956 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61956"])){
                if($_POST["G2972_C61956"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61956 = $_POST["G2972_C61956"];
                    $LsqlU .= $separador." G2972_C61956 = ".$G2972_C61956."";
                    $LsqlI .= $separador." G2972_C61956";
                    $LsqlV .= $separador.$G2972_C61956;
                    $validar = 1;
                }
            }
  
            $G2972_C61957 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61957"])){
                if($_POST["G2972_C61957"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61957 = $_POST["G2972_C61957"];
                    $LsqlU .= $separador." G2972_C61957 = ".$G2972_C61957."";
                    $LsqlI .= $separador." G2972_C61957";
                    $LsqlV .= $separador.$G2972_C61957;
                    $validar = 1;
                }
            }
  
            $G2972_C61958 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61958"])){
                if($_POST["G2972_C61958"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61958 = $_POST["G2972_C61958"];
                    $LsqlU .= $separador." G2972_C61958 = ".$G2972_C61958."";
                    $LsqlI .= $separador." G2972_C61958";
                    $LsqlV .= $separador.$G2972_C61958;
                    $validar = 1;
                }
            }
  
            $G2972_C61959 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61959"])){
                if($_POST["G2972_C61959"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61959 = $_POST["G2972_C61959"];
                    $LsqlU .= $separador." G2972_C61959 = ".$G2972_C61959."";
                    $LsqlI .= $separador." G2972_C61959";
                    $LsqlV .= $separador.$G2972_C61959;
                    $validar = 1;
                }
            }
  
            $G2972_C61960 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2972_C61960"])){
                if($_POST["G2972_C61960"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2972_C61960 = $_POST["G2972_C61960"];
                    $LsqlU .= $separador." G2972_C61960 = ".$G2972_C61960."";
                    $LsqlI .= $separador." G2972_C61960";
                    $LsqlV .= $separador.$G2972_C61960;
                    $validar = 1;
                }
            }

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2972_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2972_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2972_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2972_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2972_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2972_Usuario , G2972_FechaInsercion, G2972_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2972_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2972 WHERE G2972_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2972_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2972 WHERE G2972_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2970_ConsInte__b, G2970_C59276, G2970_C59277, G2970_C59278, G2970_C59279, G2970_C59280, G2970_C59295, G2969_C59266 as G2970_C59281, G2970_C59282, G2970_C59283, j.LISOPC_Nombre____b as  G2970_C59284, k.LISOPC_Nombre____b as  G2970_C59285, l.LISOPC_Nombre____b as  G2970_C59286, G2970_C59287, n.LISOPC_Nombre____b as  G2970_C59288 FROM ".$BaseDatos.".G2970  LEFT JOIN ".$BaseDatos.".G2969 ON G2969_ConsInte__b  =  G2970_C59281 LEFT JOIN ".$BaseDatos_systema.".LISOPC as j ON j.LISOPC_ConsInte__b =  G2970_C59284 LEFT JOIN ".$BaseDatos_systema.".LISOPC as k ON k.LISOPC_ConsInte__b =  G2970_C59285 LEFT JOIN ".$BaseDatos_systema.".LISOPC as l ON l.LISOPC_ConsInte__b =  G2970_C59286 LEFT JOIN ".$BaseDatos_systema.".LISOPC as n ON n.LISOPC_ConsInte__b =  G2970_C59288 ";


        if (isset($_GET["agente"])) {

            $SQL .= " WHERE G2970_C64637 = '".$_GET["agente"]."' AND G2970_C59288 = 44952"; 
            
        }else{

            $SQL .= " WHERE G2970_C59295 = '".$numero."'"; 

        }


        $SQL .= " ORDER BY G2970_C59276";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2970_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2970_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2970_C59276."</cell>"; 

                if($fila->G2970_C59277 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59277)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59278 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59278)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59279 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59279)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59280 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59280)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2970_C59295)."</cell>";

                echo "<cell>". ($fila->G2970_C59281)."</cell>";

                echo "<cell>". ($fila->G2970_C59282)."</cell>";

                echo "<cell>". ($fila->G2970_C59283)."</cell>";

                echo "<cell>". ($fila->G2970_C59284)."</cell>";

                echo "<cell>". ($fila->G2970_C59285)."</cell>";

                echo "<cell>". ($fila->G2970_C59286)."</cell>";

                echo "<cell><![CDATA[". ($fila->G2970_C59287)."]]></cell>";

                echo "<cell>". ($fila->G2970_C59288)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2972_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2972 WHERE G2972_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2970_ConsInte__b, G2970_C59276, G2970_C59277, G2970_C59278, G2970_C59279, G2970_C59280, G2970_C59295, G2969_C59266 as G2970_C59281, G2970_C59282, G2970_C59283, j.LISOPC_Nombre____b as  G2970_C59284, k.LISOPC_Nombre____b as  G2970_C59285, l.LISOPC_Nombre____b as  G2970_C59286, G2970_C59287, n.LISOPC_Nombre____b as  G2970_C59288 FROM ".$BaseDatos.".G2970  LEFT JOIN ".$BaseDatos.".G2969 ON G2969_ConsInte__b  =  G2970_C59281 LEFT JOIN ".$BaseDatos_systema.".LISOPC as j ON j.LISOPC_ConsInte__b =  G2970_C59284 LEFT JOIN ".$BaseDatos_systema.".LISOPC as k ON k.LISOPC_ConsInte__b =  G2970_C59285 LEFT JOIN ".$BaseDatos_systema.".LISOPC as l ON l.LISOPC_ConsInte__b =  G2970_C59286 LEFT JOIN ".$BaseDatos_systema.".LISOPC as n ON n.LISOPC_ConsInte__b =  G2970_C59288 ";

        $SQL .= " WHERE G2970_C59281 = '".$numero."'"; 

        $SQL .= " ORDER BY G2970_C59276";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2970_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2970_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2970_C59276."</cell>"; 

                if($fila->G2970_C59277 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59277)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59278 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59278)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59279 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59279)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59280 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59280)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2970_C59295)."</cell>";

                echo "<cell>". ($fila->G2970_C59281)."</cell>";

                echo "<cell>". ($fila->G2970_C59282)."</cell>";

                echo "<cell>". ($fila->G2970_C59283)."</cell>";

                echo "<cell>". ($fila->G2970_C59284)."</cell>";

                echo "<cell>". ($fila->G2970_C59285)."</cell>";

                echo "<cell>". ($fila->G2970_C59286)."</cell>";

                echo "<cell><![CDATA[". ($fila->G2970_C59287)."]]></cell>";

                echo "<cell>". ($fila->G2970_C59288)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2970 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2970(";
            $LsqlV = " VALUES ("; 
 
                $G2970_C59276= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2970_C59276"])){    
                    if($_POST["G2970_C59276"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59276 = $_POST["G2970_C59276"];
                        $LsqlU .= $separador." G2970_C59276 = '".$G2970_C59276."'";
                        $LsqlI .= $separador." G2970_C59276";
                        $LsqlV .= $separador."'".$G2970_C59276."'";
                        $validar = 1;
                    }
                }

                $G2970_C59277 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2970_C59277"])){    
                    if($_POST["G2970_C59277"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59277 = "'".str_replace(' ', '',$_POST["G2970_C59277"])." 00:00:00'";
                        $LsqlU .= $separador." G2970_C59277 = ".$G2970_C59277;
                        $LsqlI .= $separador." G2970_C59277";
                        $LsqlV .= $separador.$G2970_C59277;
                        $validar = 1;
                    }
                }
 
                $G2970_C59278 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2970_C59278"])){    
                    if($_POST["G2970_C59278"] != '' && $_POST["G2970_C59278"] != 'undefined' && $_POST["G2970_C59278"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59278 = "'".$fecha." ".str_replace(' ', '',$_POST["G2970_C59278"])."'";
                        $LsqlU .= $separador."  G2970_C59278 = ".$G2970_C59278."";
                        $LsqlI .= $separador."  G2970_C59278";
                        $LsqlV .= $separador.$G2970_C59278;
                        $validar = 1;
                    }
                }

                $G2970_C59279 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2970_C59279"])){    
                    if($_POST["G2970_C59279"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59279 = "'".str_replace(' ', '',$_POST["G2970_C59279"])." 00:00:00'";
                        $LsqlU .= $separador." G2970_C59279 = ".$G2970_C59279;
                        $LsqlI .= $separador." G2970_C59279";
                        $LsqlV .= $separador.$G2970_C59279;
                        $validar = 1;
                    }
                }
 
                $G2970_C59280 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2970_C59280"])){    
                    if($_POST["G2970_C59280"] != '' && $_POST["G2970_C59280"] != 'undefined' && $_POST["G2970_C59280"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59280 = "'".$fecha." ".str_replace(' ', '',$_POST["G2970_C59280"])."'";
                        $LsqlU .= $separador."  G2970_C59280 = ".$G2970_C59280."";
                        $LsqlI .= $separador."  G2970_C59280";
                        $LsqlV .= $separador.$G2970_C59280;
                        $validar = 1;
                    }
                }
  
                if(isset($_POST["G2970_C59281"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59281 = '".$_POST["G2970_C59281"]."'";
                    $LsqlI .= $separador."G2970_C59281";
                    $LsqlV .= $separador."'".$_POST["G2970_C59281"]."'";
                    $validar = 1;
                }
                
 

                if(isset($_POST["G2970_C59282"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59282 = '".$_POST["G2970_C59282"]."'";
                    $LsqlI .= $separador."G2970_C59282";
                    $LsqlV .= $separador."'".$_POST["G2970_C59282"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2970_C59283"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59283 = '".$_POST["G2970_C59283"]."'";
                    $LsqlI .= $separador."G2970_C59283";
                    $LsqlV .= $separador."'".$_POST["G2970_C59283"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2970_C59284"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59284 = '".$_POST["G2970_C59284"]."'";
                    $LsqlI .= $separador."G2970_C59284";
                    $LsqlV .= $separador."'".$_POST["G2970_C59284"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G2970_C59285"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59285 = '".$_POST["G2970_C59285"]."'";
                    $LsqlI .= $separador."G2970_C59285";
                    $LsqlV .= $separador."'".$_POST["G2970_C59285"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G2970_C59286"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59286 = '".$_POST["G2970_C59286"]."'";
                    $LsqlI .= $separador."G2970_C59286";
                    $LsqlV .= $separador."'".$_POST["G2970_C59286"]."'";
                    $validar = 1;
                }
  

                if(isset($_POST["G2970_C59287"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59287 = '".$_POST["G2970_C59287"]."'";
                    $LsqlI .= $separador."G2970_C59287";
                    $LsqlV .= $separador."'".$_POST["G2970_C59287"]."'";
                    $validar = 1;
                }
                                                                               
 
                if(isset($_POST["G2970_C59288"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59288 = '".$_POST["G2970_C59288"]."'";
                    $LsqlI .= $separador."G2970_C59288";
                    $LsqlV .= $separador."'".$_POST["G2970_C59288"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2970_C59295 = $numero;
                    $LsqlU .= ", G2970_C59295 = ".$G2970_C59295."";
                    $LsqlI .= ", G2970_C59295";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2970_Usuario ,  G2970_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2970_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2970 WHERE  G2970_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2970 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2970(";
            $LsqlV = " VALUES ("; 
 
                $G2970_C59276= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2970_C59276"])){    
                    if($_POST["G2970_C59276"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59276 = $_POST["G2970_C59276"];
                        $LsqlU .= $separador." G2970_C59276 = '".$G2970_C59276."'";
                        $LsqlI .= $separador." G2970_C59276";
                        $LsqlV .= $separador."'".$G2970_C59276."'";
                        $validar = 1;
                    }
                }

                $G2970_C59277 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2970_C59277"])){    
                    if($_POST["G2970_C59277"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59277 = "'".str_replace(' ', '',$_POST["G2970_C59277"])." 00:00:00'";
                        $LsqlU .= $separador." G2970_C59277 = ".$G2970_C59277;
                        $LsqlI .= $separador." G2970_C59277";
                        $LsqlV .= $separador.$G2970_C59277;
                        $validar = 1;
                    }
                }
 
                $G2970_C59278 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2970_C59278"])){    
                    if($_POST["G2970_C59278"] != '' && $_POST["G2970_C59278"] != 'undefined' && $_POST["G2970_C59278"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59278 = "'".$fecha." ".str_replace(' ', '',$_POST["G2970_C59278"])."'";
                        $LsqlU .= $separador."  G2970_C59278 = ".$G2970_C59278."";
                        $LsqlI .= $separador."  G2970_C59278";
                        $LsqlV .= $separador.$G2970_C59278;
                        $validar = 1;
                    }
                }

                $G2970_C59279 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2970_C59279"])){    
                    if($_POST["G2970_C59279"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59279 = "'".str_replace(' ', '',$_POST["G2970_C59279"])." 00:00:00'";
                        $LsqlU .= $separador." G2970_C59279 = ".$G2970_C59279;
                        $LsqlI .= $separador." G2970_C59279";
                        $LsqlV .= $separador.$G2970_C59279;
                        $validar = 1;
                    }
                }
 
                $G2970_C59280 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2970_C59280"])){    
                    if($_POST["G2970_C59280"] != '' && $_POST["G2970_C59280"] != 'undefined' && $_POST["G2970_C59280"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59280 = "'".$fecha." ".str_replace(' ', '',$_POST["G2970_C59280"])."'";
                        $LsqlU .= $separador."  G2970_C59280 = ".$G2970_C59280."";
                        $LsqlI .= $separador."  G2970_C59280";
                        $LsqlV .= $separador.$G2970_C59280;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2970_C59295"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59295 = '".$_POST["G2970_C59295"]."'";
                    $LsqlI .= $separador."G2970_C59295";
                    $LsqlV .= $separador."'".$_POST["G2970_C59295"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2970_C59282"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59282 = '".$_POST["G2970_C59282"]."'";
                    $LsqlI .= $separador."G2970_C59282";
                    $LsqlV .= $separador."'".$_POST["G2970_C59282"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2970_C59283"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59283 = '".$_POST["G2970_C59283"]."'";
                    $LsqlI .= $separador."G2970_C59283";
                    $LsqlV .= $separador."'".$_POST["G2970_C59283"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2970_C59284"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59284 = '".$_POST["G2970_C59284"]."'";
                    $LsqlI .= $separador."G2970_C59284";
                    $LsqlV .= $separador."'".$_POST["G2970_C59284"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G2970_C59285"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59285 = '".$_POST["G2970_C59285"]."'";
                    $LsqlI .= $separador."G2970_C59285";
                    $LsqlV .= $separador."'".$_POST["G2970_C59285"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G2970_C59286"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59286 = '".$_POST["G2970_C59286"]."'";
                    $LsqlI .= $separador."G2970_C59286";
                    $LsqlV .= $separador."'".$_POST["G2970_C59286"]."'";
                    $validar = 1;
                }
  

                if(isset($_POST["G2970_C59287"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59287 = '".$_POST["G2970_C59287"]."'";
                    $LsqlI .= $separador."G2970_C59287";
                    $LsqlV .= $separador."'".$_POST["G2970_C59287"]."'";
                    $validar = 1;
                }
                                                                               
 
                if(isset($_POST["G2970_C59288"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59288 = '".$_POST["G2970_C59288"]."'";
                    $LsqlI .= $separador."G2970_C59288";
                    $LsqlV .= $separador."'".$_POST["G2970_C59288"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2970_C59281 = $numero;
                    $LsqlU .= ", G2970_C59281 = ".$G2970_C59281."";
                    $LsqlI .= ", G2970_C59281";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2970_Usuario ,  G2970_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2970_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2970 WHERE  G2970_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
