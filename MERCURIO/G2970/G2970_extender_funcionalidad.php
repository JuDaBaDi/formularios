<?php
include(__DIR__."/../../conexion.php");
?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }

    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function(e) {
        var data = JSON.parse(e.data);
        console.log(data);
        $("#G2970_C60126").val(data['fecha']);
        $("#G2970_C60127").val(data['hora']);
        $("#refrescarGrillas").click();
    });

    function after_select_G2970_C59281(data, valor) {
        if (data.length > 0) {
            $("#G2970_C59282").attr("readonly", true);
            $("#G2970_C59283").attr("readonly", true);
            $("#G2970_C60136").attr("readonly", true);
            $("#G2970_C60137").attr("readonly", true);
            $("#G2970_C59345").attr("readonly", true);
            $("#G2970_C59346").attr("readonly", true);
            $("#G2970_C59348").attr("readonly", true);
            $("#G2970_C59349").attr("readonly", true);
            $("#G2970_C59295").attr("readonly", true);
            $("#G2970_C59347").attr("readonly", true);
            $("#G2970_C60310").attr("readonly", true);
        } else {
            swal({
                    html: true,
                    title: "SIN REGISTROS",
                    text: 'No se encontraron consultores con el código ' + valor + '.',
                    type: "warning",
                    confirmButtonText: "Inscribir consultor",
                    cancelButtonText: "Volver a buscar",
                    showCancelButton: true,
                    closeOnConfirm: true
                },
                function(isconfirm) {
                    if (isconfirm) {
                        $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2969&view=si&idFather=854&yourfather=' + valor + '&formaDetalle=si&formularioPadre=2970&action=add&pincheCampo=59266<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                        $("#editarDatos").modal('show');
                    }
                });
        }
    }

    function after_select_G2970_C59344() {
        // <?php //if(isset($_GET['formularioPadre']) && $_GET['formularioPadre']=='3127'){ ?>

        <?php if(isset($_GET['formularioPadre'])){ ?>
        $("#G2970_C59344").attr('opt', '<?=$_GET['yourfather']?>');
        $("#G2970_C59344").val('<?=$_GET['yourfather']?>');
        $("#G2970_C59344").select2({
            placeholder: "Buscar",
            allowClear: false,
            minimumInputLength: 3,
            ajax: {
                url: 'formularios/G2970/eventos.php?getdatosG3124=si',
                dataType: 'json',
                type: 'post',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    try {
                        return {
                            results: $.map(data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.text
                                };
                            })
                        };
                    } catch {
                        console.log('error');
                    }
                },
                cache: true
            }
        });


        $("#G2970_C68646").select2({
            placeholder: "Buscar",
            allowClear: false,
            minimumInputLength: 3,
            ajax:{
                url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2970_C68646=si',
                dataType: 'json',
                type : 'post',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            return {id: obj.id,text: obj.text};
                        })
                    };
                },
                cache: true
            }
        });

        $("#G2970_C59344").change(function() {
            var valor = $(this).attr("opt");
            if ($(this).val()) {
                valor = $(this).val();
            }
            $.ajax({
                url: "formularios/G2970/eventos.php",
                data: {
                    dameValoresCamposDinamicos_Guion_G2970_C59344: valor
                },
                type: "post",
                dataType: "json",
                success: function(data) {
                    $("#G2970_C59344").html('<option value="' + data.G3124_C63192 + '" >' + data.G3124_C63192 + '</option>');
                    $("#G2970_C59295").val(data.G3124_C63192);
                    $("#G2970_C59345").val(data.G3124_C63199);
                    $("#G2970_C59346").val(data.G3124_C63197);
                    $("#G2970_C59282").val(data.G3124_C63194);
                    $("#G2970_C59347").val(data.G3124_C63193);
                    $("#G2970_C59349").val(data.G3124_C63785);
                    $("#G2970_C60136").val(data.G3124_C63198);
                    $("#G2970_C60137").val(data.G3124_C63196);
                    $("#G2970_C63787").val(data.G3124_C63195);
                }
            });
        });

        $("#G2970_C68646").change(function() {
            var valor = $(this).attr("opt");
            if ($(this).val()) {
                valor = $(this).val();
            }
            $.ajax({
                url: "formularios/G2970/eventos.php",
                data: {
                    dameValoresCamposDinamicos_Guion_G2970_C68646: valor
                },
                type: "post",
                dataType: "json",
                success: function(data) {
                    $("#G2970_C59344").html('<option value="' + data.G3124_C63192 + '" >' + data.G3124_C63192 + '</option>');
                    $("#G2970_C59295").val(data.G3124_C63192);
                    $("#G2970_C59345").val(data.G3124_C63199);
                    $("#G2970_C59346").val(data.G3124_C63197);
                    $("#G2970_C59282").val(data.G3124_C63194);
                    $("#G2970_C59347").val(data.G3124_C63193);
                    $("#G2970_C59349").val(data.G3124_C63785);
                    $("#G2970_C60136").val(data.G3124_C63198);
                    $("#G2970_C60137").val(data.G3124_C63196);
                    $("#G2970_C63787").val(data.G3124_C63195);
                }
            });
        });
        $("#G2970_C59344").change();
        setTimeout(function() {
            $("#G2970_C59344").change();
        }, 1000);
        <?php } ?>
    }

    function cambioption(val, texto) {
        setTimeout(function() {

            $("#divG2970_C59281").show();
            $("#divG2970_C68646").hide();
            if (val == '46721') {
                $("#divG2970_C59281").hide();
                $("#divG2970_C68646").show();
                $("#G2970_C61951").parent().remove();
                $("#G2970_C61082").val('45686').trigger('change');
                $("#G2970_C61082").val('46721');
                after_select_G2970_C59344();
            } else {
                $("#G2970_C63787").parent().remove();
                $("#G2970_C61082").val(val).trigger('change');
            }
            $("#G2970_C61082").html('<option value="' + val + '">' + texto + '</option>');
        }, 500);
    }

    <?php 
        if(isset($_GET['formularioPadre'])){
            $campan='';
            $texto='';
            switch($_GET['formularioPadre']){
                case '2972'://ANI
                    $campan='45686';
                    $texto='ANI';
                    break;
                case '3012'://SNAC CF
                    $campan='45831';
                    $texto='SNAC CF';
                    break;
                case '2973'://LIDERES
                    $campan='45687';
                    $texto='LIDERES';
                    break;
                case '3007'://REDES
                    $campan='45833';
                    $texto='REDES';
                    break;
                case '3006'://SNAC INTENCIONES
                    $campan='45834';
                    $texto='SNAC INTENCIONES';
                    break;
                case '2997'://VENTAS ONLINE
                    $campan='45830';
                    $texto='VENTAS ONLINE';
                    break;
                case '3127'://ZAFIRO
                    $campan='46721';
                    $texto='ZAFIRO';
                    break;
                default:
                    $campan='0';
                    $texto='';
                    break;
            }
    ?>
    cambioption('<?=$campan?>', '<?=$texto?>');
    <?php } ?>

    $(function() {
        function fecha(fecha, hora) {
            var meses = new Array(12);
            meses[0] = "01";
            meses[1] = "02";
            meses[2] = "03";
            meses[3] = "04";
            meses[4] = "05";
            meses[5] = "06";
            meses[6] = "07";
            meses[7] = "08";
            meses[8] = "09";
            meses[9] = "10";
            meses[10] = "11";
            meses[11] = "12";

            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var minutos = (d.getMinutes() < 10) ? '0' + d.getMinutes() : d.getMinutes();
            var segundos = (d.getSeconds() < 10) ? '0' + d.getSeconds() : d.getSeconds();
            var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias;
            var horaInicial = horas + ':' + minutos + ':' + segundos;
            if (fecha == true) {
                return fechaInicial;
            }
            if (hora == true) {
                return horaInicial;
            }
        }

        $(".form-control").blur(function() {

            var today = moment().format('YYYY-MM-DD');
            var todayH = moment().format('HH:mm:ss');

            $("#G2970_C60126").val(today);
            $("#G2970_C60127").val(todayH);

        });

        $("#G2970_C59278").val(fecha(false, true));

        $("#add").click(function() {
            $("#G2970_C59278").val(fecha(false, true));
        });

        $("#G2970_C59281").change(function() {
            var valor = $(this).attr("opt");
            if ($(this).val()) {
                valor = $(this).val();
            }
            $.ajax({
                url: "<?php echo $url_crud;?>",
                data: {
                    dameValoresCamposDinamicos_Guion_G2970_C59281: valor
                },
                type: "post",
                dataType: "json",
                success: function(data) {

                    $("#G2970_C59281").html('<option value="' + data.G2969_ConsInte__b + '" >' + data.G2969_C59266 + '</option>');

                    $("#G2970_C59282").val(data.G2969_C59268);
                    $("#G2970_C59283").val(data.G2969_C59267);
                    $("#G2970_C60136").val(data.G2969_C60144);
                    $("#G2970_C60137").val(data.G2969_C60143);
                    $("#G2970_C59345").val(data.G2969_C59341);
                    $("#G2970_C59346").val(data.G2969_C59339);
                    $("#G2970_C59348").val(data.G2969_C59342);
                    $("#G2970_C59349").val(data.G2969_C59343);

                    $("#G2970_C59347").val(data.G2969_C59340);

                    $("#G2970_C60310").attr("opt", data.G2969_C60940);
                    $("#G2970_C60310").val(data.G2969_C60940).trigger("change");

                }
            });
        });

        $("#G2970_C68646").change(function() {
            var valor = $(this).attr("opt");
            if ($(this).val()) {
                valor = $(this).val();
            }
            $.ajax({
                url: "<?php echo $url_crud;?>",
                data: {
                    dameValoresCamposDinamicos_Guion_G2970_C68646: valor
                },
                type: "post",
                dataType: "json",
                success: function(data) {
                    $("#G2970_C68646").html('<option value="'+data.G3124_ConsInte__b+'" >'+data.G3124_C63192+'</option>');
                    
                    $("#G2970_C59282").val(data.G3124_C63194);
                    $("#G2970_C59283").val(data.G3124_C63193);
                    $("#G2970_C59346").val(data.G3124_C63197);
                    $("#G2970_C59345").val(data.G3124_C63199);
                    // $("#G2970_C59347").val(data.G2969_C59340);
                    // $("#G2970_C59348").val(data.G2969_C59342);
                    $("#G2970_C59349").val(data.G3124_C63785);
                    // $("#G2970_C60310").val(data.G2969_C60940);
                    $("#G2970_C59345").val(data.G3124_C63199);

                }
            });
        });

        $("#G2970_C59282").change(function() {

            console.log("hola");

        });

        $("#G2970_C59288").change(function() {

            if ($(this).val() == "44527") {
                $("#G2970_C59279").val(fecha(true, false));
                $("#G2970_C59280").val(fecha(false, true));

            } else {

                $("#G2970_C59279").val("");
                $("#G2970_C59280").val("");
            }
        });

        // FUNCTION PARA SUBTIPIFICACION 
        $("#G2970_C59285").change(function() {

            var optG2970_C59286 = $("#G2970_C59286").attr("opt");

            var arrN2_t = ["44566", "47403", "47404", "47349", "47352", "47395", "47396", "47397", "47398", "47394", "47392", "46374", "46373", "46368", "46369", "46359", "46351", "46352", "46351", "46344", "46130", "46127", "44631", "44632", "44598", "44599", "44600", "45358", "44604", "44605", "44612", "44619", "44623", "44626", "44643", "44671", "44645", "44655", "44656", "44648", "44664", "44666", "44667", "44668", "44669", "44670", "44672", "45848", "45849", "45850", "45851", "45852", "45858", "45861", "45863", "45865", "45866", "45867", "45880", "45881", "45887", "45888", "45889", "45890", "45891", "45892", "45893", "45894", "45898", "45899", "45900", "45901", "45712", "45713", "45714"];

            var arrN1N2_t = ["44564", "47350", "47393", "46380", "46379", "46367", "46364", "46365", "46361", "46358", "46353", "46354", "46355", "46001", "46002", "45998", "45997", "45357", "44571", "44606", "44607", "44608", "44611", "44614", "44618", "44620", "44622", "44665", "44673", "45360", "45361", "45362", "45373", "45375", "45376", "45377", "45378", "45383", "45379", "45380", "45381", "45382", "45853", "45859", "45864", "45870", "45878", "45882", "45884", "45896", "45897", "45903", "45904", "45709", "45715", "45717", "45719"];



            if (arrN2_t.includes($(this).val())) {

                $("#G2970_C59286").html('<option value="44555">N2</option>');
                $("#G2970_C59286").val("44555").trigger("change");

            } else if (arrN1N2_t.includes($(this).val())) {

                $("#G2970_C59286").html('<option value="44554">N1</option><option value="44555">N2</option>');
                $("#G2970_C59286").val("44554").trigger("change");

            } else {

                $("#G2970_C59286").html('<option value="44554">N1</option>');
                $("#G2970_C59286").val("44554").trigger("change");

            }

            if (optG2970_C59286 != null) {

                $("#G2970_C59286").val(optG2970_C59286).trigger("change");

            }

            switch ($(this).val()) {

                case "44542":

                    $("#G2970_C60445").val("1");

                    break;

                case "44543":

                    $("#G2970_C60445").val("2");

                    break;

                case "45996":

                    $("#G2970_C60445").val("3");

                    break;

                case "44568":

                    $("#G2970_C60445").val("4");

                    break;

                case "44544":

                    $("#G2970_C60445").val("5");

                    break;

                case "44545":

                    $("#G2970_C60445").val("6");

                    break;

                case "44546":

                    $("#G2970_C60445").val("7");

                    break;

                case "44547":

                    $("#G2970_C60445").val("8");

                    break;

                case "44548":

                    $("#G2970_C60445").val("9");

                    break;

                case "44549":

                    $("#G2970_C60445").val("10");

                    break;

                case "44550":

                    $("#G2970_C60445").val("11");

                    break;

                case "44551":

                    $("#G2970_C60445").val("12");

                    break;

                case "44552":

                    $("#G2970_C60445").val("13");

                    break;

                case "44553":

                    $("#G2970_C60445").val("14");

                    break;

                case "44564":

                    $("#G2970_C60445").val("15");

                    break;

                case "44566":

                    $("#G2970_C60445").val("16");

                    break;

                case "44567":

                    $("#G2970_C60445").val("17");

                    break;

                case "45357":

                    $("#G2970_C60445").val("18");

                    break;

                case "44569":

                    $("#G2970_C60445").val("19");

                    break;

                case "44570":

                    $("#G2970_C60445").val("20");

                    break;

                case "44571":

                    $("#G2970_C60445").val("21");

                    break;

                case "44572":

                    $("#G2970_C60445").val("22");

                    break;

                case "45997":

                    $("#G2970_C60445").val("23");

                    break;

                case "45998":

                    $("#G2970_C60445").val("24");

                    break;

                case "44574":

                    $("#G2970_C60445").val("25");

                    break;

                case "44630":

                    $("#G2970_C60445").val("26");

                    break;

                case "44575":

                    $("#G2970_C60445").val("27");

                    break;

                case "44576":

                    $("#G2970_C60445").val("28");

                    break;

                case "44577":

                    $("#G2970_C60445").val("29");

                    break;

                case "44578":

                    $("#G2970_C60445").val("30");

                    break;

                case "44579":

                    $("#G2970_C60445").val("31");

                    break;

                case "44580":

                    $("#G2970_C60445").val("32");

                    break;

                case "44581":

                    $("#G2970_C60445").val("33");

                    break;

                case "44629":

                    $("#G2970_C60445").val("34");

                    break;

                case "44633":

                    $("#G2970_C60445").val("35");

                    break;

                case "44631":

                    $("#G2970_C60445").val("36");

                    break;

                case "44632":

                    $("#G2970_C60445").val("37");

                    break;

                case "44582":

                    $("#G2970_C60445").val("38");

                    break;

                case "44583":

                    $("#G2970_C60445").val("39");

                    break;

                case "44584":

                    $("#G2970_C60445").val("40");

                    break;

                case "44585":

                    $("#G2970_C60445").val("41");

                    break;

                case "44586":

                    $("#G2970_C60445").val("42");

                    break;

                case "44587":

                    $("#G2970_C60445").val("43");

                    break;

                case "44588":

                    $("#G2970_C60445").val("44");

                    break;

                case "44589":

                    $("#G2970_C60445").val("45");

                    break;

                case "44590":

                    $("#G2970_C60445").val("46");

                    break;

                case "44591":

                    $("#G2970_C60445").val("47");

                    break;

                case "44592":

                    $("#G2970_C60445").val("48");

                    break;

                case "44593":

                    $("#G2970_C60445").val("49");

                    break;

                case "44594":

                    $("#G2970_C60445").val("50");

                    break;

                case "44595":

                    $("#G2970_C60445").val("51");

                    break;

                case "44596":

                    $("#G2970_C60445").val("52");

                    break;

                case "44597":

                    $("#G2970_C60445").val("53");

                    break;

                case "44598":

                    $("#G2970_C60445").val("54");

                    break;

                case "44599":

                    $("#G2970_C60445").val("55");

                    break;

                case "44600":

                    $("#G2970_C60445").val("56");

                    break;

                case "45358":

                    $("#G2970_C60445").val("57");

                    break;

                case "44601":

                    $("#G2970_C60445").val("58");

                    break;

                case "44602":

                    $("#G2970_C60445").val("59");

                    break;

                case "44603":

                    $("#G2970_C60445").val("60");

                    break;

                case "44604":

                    $("#G2970_C60445").val("61");

                    break;

                case "44605":

                    $("#G2970_C60445").val("62");

                    break;

                case "44606":

                    $("#G2970_C60445").val("63");

                    break;

                case "44607":

                    $("#G2970_C60445").val("64");

                    break;

                case "44608":

                    $("#G2970_C60445").val("65");

                    break;

                case "44609":

                    $("#G2970_C60445").val("66");

                    break;

                case "44610":

                    $("#G2970_C60445").val("67");

                    break;

                case "44611":

                    $("#G2970_C60445").val("68");

                    break;

                case "44612":

                    $("#G2970_C60445").val("69");

                    break;

                case "44613":

                    $("#G2970_C60445").val("70");

                    break;

                case "44614":

                    $("#G2970_C60445").val("71");

                    break;

                case "44615":

                    $("#G2970_C60445").val("72");

                    break;

                case "44616":

                    $("#G2970_C60445").val("73");

                    break;

                case "44617":

                    $("#G2970_C60445").val("74");

                    break;

                case "44618":

                    $("#G2970_C60445").val("75");

                    break;

                case "44619":

                    $("#G2970_C60445").val("76");

                    break;

                case "44620":

                    $("#G2970_C60445").val("77");

                    break;

                case "44621":

                    $("#G2970_C60445").val("78");

                    break;

                case "44622":

                    $("#G2970_C60445").val("79");

                    break;

                case "44623":

                    $("#G2970_C60445").val("80");

                    break;

                case "44624":

                    $("#G2970_C60445").val("81");

                    break;

                case "44625":

                    $("#G2970_C60445").val("82");

                    break;

                case "44626":

                    $("#G2970_C60445").val("83");

                    break;

                case "44627":

                    $("#G2970_C60445").val("84");

                    break;

                case "44628":

                    $("#G2970_C60445").val("85");

                    break;

                case "44634":

                    $("#G2970_C60445").val("86");

                    break;

                case "44635":

                    $("#G2970_C60445").val("87");

                    break;

                case "44636":

                    $("#G2970_C60445").val("88");

                    break;

                case "44653":

                    $("#G2970_C60445").val("89");

                    break;

                case "44640":

                    $("#G2970_C60445").val("90");

                    break;

                case "44641":

                    $("#G2970_C60445").val("91");

                    break;

                case "44642":

                    $("#G2970_C60445").val("92");

                    break;

                case "44643":

                    $("#G2970_C60445").val("93");

                    break;

                case "44644":

                    $("#G2970_C60445").val("94");

                    break;

                case "44649":

                    $("#G2970_C60445").val("95");

                    break;

                case "44658":

                    $("#G2970_C60445").val("96");

                    break;

                case "44659":

                    $("#G2970_C60445").val("97");

                    break;

                case "44660":

                    $("#G2970_C60445").val("98");

                    break;

                case "44671":

                    $("#G2970_C60445").val("99");

                    break;

                case "44639":

                    $("#G2970_C60445").val("100");

                    break;

                case "44645":

                    $("#G2970_C60445").val("101");

                    break;

                case "44650":

                    $("#G2970_C60445").val("103");

                    break;

                case "44651":

                    $("#G2970_C60445").val("104");

                    break;

                case "44652":

                    $("#G2970_C60445").val("105");

                    break;

                case "44654":

                    $("#G2970_C60445").val("106");

                    break;

                case "44655":

                    $("#G2970_C60445").val("107");

                    break;

                case "44656":

                    $("#G2970_C60445").val("108");

                    break;

                case "44657":

                    $("#G2970_C60445").val("109");

                    break;

                case "44648":

                    $("#G2970_C60445").val("110");

                    break;

                case "44661":

                    $("#G2970_C60445").val("111");

                    break;

                case "44662":

                    $("#G2970_C60445").val("112");

                    break;

                case "44663":

                    $("#G2970_C60445").val("113");

                    break;

                case "44664":

                    $("#G2970_C60445").val("114");

                    break;

                case "44665":

                    $("#G2970_C60445").val("115");

                    break;

                case "44666":

                    $("#G2970_C60445").val("116");

                    break;

                case "44667":

                    $("#G2970_C60445").val("117");

                    break;

                case "44668":

                    $("#G2970_C60445").val("118");

                    break;

                case "44669":

                    $("#G2970_C60445").val("119");

                    break;

                case "44670":

                    $("#G2970_C60445").val("120");

                    break;

                case "44672":

                    $("#G2970_C60445").val("121");

                    break;

                case "44673":

                    $("#G2970_C60445").val("122");

                    break;

                case "45359":

                    $("#G2970_C60445").val("123");

                    break;

                case "45360":

                    $("#G2970_C60445").val("124");

                    break;

                case "45362":

                    $("#G2970_C60445").val("126");

                    break;

                case "45363":

                    $("#G2970_C60445").val("127");

                    break;

                case "44679":

                    $("#G2970_C60445").val("182");

                    break;

                case "44680":

                    $("#G2970_C60445").val("129");

                    break;

                case "45373":

                    $("#G2970_C60445").val("130");

                    break;

                case "45374":

                    $("#G2970_C60445").val("131");

                    break;

                case "45375":

                    $("#G2970_C60445").val("132");

                    break;

                case "45376":

                    $("#G2970_C60445").val("133");

                    break;

                case "45377":

                    $("#G2970_C60445").val("134");

                    break;

                case "45378":

                    $("#G2970_C60445").val("135");

                    break;

                case "45383":

                    $("#G2970_C60445").val("136");

                    break;

                case "45379":

                    $("#G2970_C60445").val("137");

                    break;

                case "45380":

                    $("#G2970_C60445").val("138");

                    break;

                case "45381":

                    $("#G2970_C60445").val("139");

                    break;

                case "45382":

                    $("#G2970_C60445").val("140");

                    break;

                case "45846":

                    $("#G2970_C60445").val("156");

                    break;

                case "45847":

                    $("#G2970_C60445").val("157");

                    break;

                case "45848":

                    $("#G2970_C60445").val("158");

                    break;

                case "45849":

                    $("#G2970_C60445").val("159");

                    break;

                case "45850":

                    $("#G2970_C60445").val("160");

                    break;

                case "45851":

                    $("#G2970_C60445").val("161");

                    break;

                case "45852":

                    $("#G2970_C60445").val("162");

                    break;

                case "45853":

                    $("#G2970_C60445").val("163");

                    break;

                case "45854":

                    $("#G2970_C60445").val("164");

                    break;

                case "45855":

                    $("#G2970_C60445").val("165");

                    break;

                case "45856":

                    $("#G2970_C60445").val("166");

                    break;

                case "45857":

                    $("#G2970_C60445").val("167");

                    break;

                case "45858":

                    $("#G2970_C60445").val("168");

                    break;

                case "45859":

                    $("#G2970_C60445").val("169");

                    break;

                case "45860":

                    $("#G2970_C60445").val("170");

                    break;

                case "45861":

                    $("#G2970_C60445").val("171");

                    break;

                case "45862":

                    $("#G2970_C60445").val("172");

                    break;

                case "45863":

                    $("#G2970_C60445").val("173");

                    break;

                case "45864":

                    $("#G2970_C60445").val("174");

                    break;

                case "45865":

                    $("#G2970_C60445").val("175");

                    break;

                case "45866":

                    $("#G2970_C60445").val("176");

                    break;

                case "45867":

                    $("#G2970_C60445").val("177");

                    break;

                case "45868":

                    $("#G2970_C60445").val("178");

                    break;

                case "45869":

                    $("#G2970_C60445").val("179");

                    break;

                case "45870":

                    $("#G2970_C60445").val("180");

                    break;

                case "45871":

                    $("#G2970_C60445").val("181");

                    break;

                case "45872":

                    $("#G2970_C60445").val("182");

                    break;

                case "45873":

                    $("#G2970_C60445").val("183");

                    break;

                case "45874":

                    $("#G2970_C60445").val("184");

                    break;

                case "45875":

                    $("#G2970_C60445").val("185");

                    break;

                case "45876":

                    $("#G2970_C60445").val("186");

                    break;

                case "45877":

                    $("#G2970_C60445").val("187");

                    break;

                case "45878":

                    $("#G2970_C60445").val("188");

                    break;

                case "45879":

                    $("#G2970_C60445").val("189");

                    break;

                case "45880":

                    $("#G2970_C60445").val("190");

                    break;

                case "45881":

                    $("#G2970_C60445").val("191");

                    break;

                case "45882":

                    $("#G2970_C60445").val("192");

                    break;

                case "45883":

                    $("#G2970_C60445").val("193");

                    break;

                case "45884":

                    $("#G2970_C60445").val("194");

                    break;

                case "45885":

                    $("#G2970_C60445").val("195");

                    break;

                case "45886":

                    $("#G2970_C60445").val("196");

                    break;

                case "45887":

                    $("#G2970_C60445").val("197");

                    $("#G2970_C62291").val("45139").trigger("change");

                    break;

                case "45888":

                    $("#G2970_C60445").val("198");

                    break;

                case "45889":

                    $("#G2970_C60445").val("199");

                    $("#G2970_C62291").val("0").trigger("change");

                    break;

                case "45890":

                    $("#G2970_C60445").val("200");

                    $("#G2970_C62291").val("0").trigger("change");

                    break;

                case "45891":

                    $("#G2970_C60445").val("201");

                    $("#G2970_C62291").val("0").trigger("change");

                    break;

                case "45892":

                    $("#G2970_C60445").val("202");

                    $("#G2970_C62291").val("0").trigger("change");

                    break;

                case "45893":

                    $("#G2970_C60445").val("203");

                    $("#G2970_C62291").val("0").trigger("change");

                    break;

                case "45894":

                    $("#G2970_C60445").val("204");

                    $("#G2970_C62291").val("45136").trigger("change");

                    break;

                case "45895":

                    $("#G2970_C60445").val("205");

                    $("#G2970_C62291").val("45307").trigger("change");

                    break;

                case "45896":

                    $("#G2970_C60445").val("206");

                    $("#G2970_C62291").val("45138").trigger("change");

                    break;

                case "45897":

                    $("#G2970_C60445").val("207");


                    $("#G2970_C62291").val("46372").trigger("change");

                    break;

                case "45898":

                    $("#G2970_C60445").val("208");

                    $("#G2970_C62291").val("45140").trigger("change");

                    break;

                case "45899":

                    $("#G2970_C60445").val("209");

                    $("#G2970_C62291").val("45140").trigger("change");

                    break;

                case "45900":

                    $("#G2970_C60445").val("210");

                    $("#G2970_C62291").val("45137").trigger("change");

                    break;

                case "45901":

                    $("#G2970_C60445").val("211");

                    $("#G2970_C62291").val("45137").trigger("change");

                    break;

                case "45902":

                    $("#G2970_C60445").val("212");

                    $("#G2970_C62291").val("0").trigger("change");

                    break;

                case "45903":

                    $("#G2970_C60445").val("213");

                    break;

                case "45904":

                    $("#G2970_C60445").val("214");

                    break;

                case "45709":

                    $("#G2970_C60445").val("141");

                    break;

                case "45710":

                    $("#G2970_C60445").val("142");

                    break;

                case "45711":

                    $("#G2970_C60445").val("143");

                    break;

                case "45712":

                    $("#G2970_C60445").val("144");

                    $("#G2970_C62291").val("46371").trigger("change");

                    break;

                case "45713":

                    $("#G2970_C60445").val("145");

                    $("#G2970_C62291").val("46371").trigger("change");

                    break;

                case "45714":

                    $("#G2970_C60445").val("146");

                    $("#G2970_C62291").val("46371").trigger("change");

                    break;

                case "45715":

                    $("#G2970_C60445").val("147");

                    break;

                case "45716":

                    $("#G2970_C60445").val("148");

                    break;

                case "45717":

                    $("#G2970_C60445").val("149");

                    break;

                case "45718":

                    $("#G2970_C60445").val("150");

                    break;

                case "45719":

                    $("#G2970_C60445").val("151");

                    $("#G2970_C62291").val("46370").trigger("change");

                    break;

                case "45720":

                    $("#G2970_C60445").val("152");

                    break;

                case "45721":

                    $("#G2970_C60445").val("153");

                    break;

                case "45722":

                    $("#G2970_C60445").val("154");

                    break;

                case "45723":

                    $("#G2970_C60445").val("155");

                    break;

                case "46001":

                    $("#G2970_C60445").val("213");

                    $("#G2970_C62291").val("46472").trigger("change");

                    break;

                case "46002":

                    $("#G2970_C60445").val("214");

                    $("#G2970_C62291").val("45306").trigger("change");

                    break;

                case "46122":

                    $("#G2970_C60445").val("225");

                    break;

                case "46123":

                    $("#G2970_C60445").val("228");

                    break;

                case "46124":

                    $("#G2970_C60445").val("229");

                    break;

                case "46125":

                    $("#G2970_C60445").val("224");

                    break;

                case "46126":

                    $("#G2970_C60445").val("35");

                    break;

                case "46127":

                    $("#G2970_C60445").val("230");

                    break;

                case "46129":

                    $("#G2970_C60445").val("226");

                    break;

                case "46130":

                    $("#G2970_C60445").val("227");

                    $("#G2970_C62291").val("46372").trigger("change");

                    break;

                case "46131":

                    $("#G2970_C60445").val("60");

                    break;

                case "46344":

                    $("#G2970_C60445").val("223");

                    $("#G2970_C62291").val("46370").trigger("change");

                    break;

                case "46345":

                    $("#G2970_C60445").val("215");

                    break;

                case "46346":

                    $("#G2970_C60445").val("216");

                    break;

                case "46347":

                    $("#G2970_C60445").val("217");

                    break;

                case "46348":

                    $("#G2970_C60445").val("58");

                    break;

                case "46349":

                    $("#G2970_C60445").val("59");

                    break;

                case "46350":

                    $("#G2970_C60445").val("60");

                    break;

                case "46351":

                    $("#G2970_C60445").val("61");

                    break;

                case "46352":

                    $("#G2970_C60445").val("62");

                    break;

                case "46353":

                    $("#G2970_C60445").val("63");

                    break;

                case "46354":

                    $("#G2970_C60445").val("64");

                    break;

                case "46355":

                    $("#G2970_C60445").val("65");

                    break;

                case "46356":

                    $("#G2970_C60445").val("66");

                    break;

                case "46357":

                    $("#G2970_C60445").val("67");

                    break;

                case "46358":

                    $("#G2970_C60445").val("68");

                    break;

                case "46359":

                    $("#G2970_C60445").val("69");

                    break;

                case "46360":

                    $("#G2970_C60445").val("70");

                    break;

                case "46361":

                    $("#G2970_C60445").val("71");

                    break;

                case "46364":

                    $("#G2970_C60445").val("218");

                    break;

                case "46365":

                    $("#G2970_C60445").val("219");

                    break;

                case "46366":

                    $("#G2970_C60445").val("220");

                    break;

                case "46367":

                    $("#G2970_C60445").val("221");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "46368":

                    $("#G2970_C60445").val("222");

                    $("#G2970_C62291").val("45138").trigger("change");

                    break;

                case "46369":

                    $("#G2970_C60445").val("101");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "46373":

                    $("#G2970_C60445").val("230");

                    $("#G2970_C62291").val("45137").trigger("change");

                    break;

                case "46374":

                    $("#G2970_C60445").val("217");

                    $("#G2970_C62291").val("45308").trigger("change");

                    break;

                case "46378":

                    $("#G2970_C60445").val("231");

                    $("#G2970_C62291").val("45141").trigger("change");

                    break;

                case "46379":

                    $("#G2970_C60445").val("232");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "46380":

                    $("#G2970_C60445").val("233");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "46381":

                    $("#G2970_C60445").val("234");

                    $("#G2970_C62291").val("46382").trigger("change");

                    break;

                case "47378":

                    $("#G2970_C60445").val("235");

                    break;

                case "47379":

                    $("#G2970_C60445").val("236");

                    break;

                case "47380":

                    $("#G2970_C60445").val("237");

                    break;

                case "47384":

                    $("#G2970_C60445").val("238");

                    break;

                case "47385":

                    $("#G2970_C60445").val("239");

                    break;

                case "47392":

                    $("#G2970_C60445").val("109");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47393":

                    $("#G2970_C60445").val("98");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47394":

                    $("#G2970_C60445").val("240");

                    break;

                case "47395":

                    $("#G2970_C60445").val("241");

                    break;

                case "47396":

                    $("#G2970_C60445").val("242");

                    break;

                case "47397":

                    $("#G2970_C60445").val("243");

                    break;

                case "47398":

                    $("#G2970_C60445").val("244");

                    break;

                case "47349":

                    $("#G2970_C60445").val("223");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47350":

                    $("#G2970_C60445").val("229");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47359":

                    $("#G2970_C60445").val("248");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47360":

                    $("#G2970_C60445").val("216");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47351":

                    $("#G2970_C60445").val("249");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47352":

                    $("#G2970_C60445").val("222");

                    $("#G2970_C62291").val("45135").trigger("change");

                    break;

                case "47400":

                    $("#G2970_C60445").val("225");


                    break;

                case "47402":

                    $("#G2970_C60445").val("245");


                    break;

                case "47403":

                    $("#G2970_C60445").val("246");

                    $("#G2970_C62291").val("45137").trigger("change");


                    break;

                case "47404":

                    $("#G2970_C60445").val("247");

                    $("#G2970_C62291").val("46372").trigger("change");


                    break;

                case "47386":

                    $("#G2970_C60445").val("314");

                    break;

                case "47387":

                    $("#G2970_C60445").val("315");

                    break;

                case "47388":

                    $("#G2970_C60445").val("217");

                    break;

                case "47389":

                    $("#G2970_C60445").val("316");

                    break;

                default:

                    $("#G2970_C60445").val("0");

                    break;

            }

            var arrConsulta_t = ["45357", "47402", "47400", "47360", "47359", "47350", "47386", "47388", "47389", "47387", "46374", "46367", "46365", "46366", "46364", "46346", "46347", "46125", "46129", "46126", "46123", "46124", "45998", "45997", "44569", "44570", "44571", "44572", "44575", "44576", "44577", "44578", "44579", "44580", "44581", "44629", "44633", "44615", "44624", "44625", "44627", "44628", "44653", "44680", "45373", "45375", "45376", "45377", "45378", "45383", "45379", "45380", "45381", "45382", "45847", "45869", "45870", "45871", "45872", "45873", "45874", "45875", "45876", "45885", "45894", "45895", "45896", "45897", "45709", "45715", "45716", "45717", "45718"];


            var arrReclamo_t = ["44547", "47351", "47398", "47397", "47396", "47395", "47394", "47393", "47392", "46382", "46379", "46380", "46378", "46369", "46361", "46360", "46357", "46359", "46358", "46356", "46355", "46353", "46354", "46351", "46352", "46349", "46348", "46131", "46130", "44548", "44631", "44632", "44598", "44599", "44600", "45358", "44601", "44602", "44603", "44604", "44605", "44606", "44607", "44608", "44609", "44610", "44611", "44612", "44613", "44614", "44639", "44645", "44650", "44651", "44652", "44654", "44655", "44656", "44657", "44648", "44661", "44662", "44663", "44664", "44665", "44666", "44667", "44668", "44669", "44670", "44672", "44673", "45359", "45360", "45361", "45362", "45363", "45848", "45849", "45850", "45851", "45852", "45853", "45856", "45857", "45858", "45859", "45860", "45861", "45862", "45863", "45864", "45865", "45866", "45867", "45868", "45890", "45891", "45892", "45893", "45898", "45899", "45900", "45901", "45902", "45720", "45721", "45722", "45723"];


            var arrSolicitud_t = ["44542", "47404", "47403", "47352", "47349", "47385", "47384", "47380", "47378", "47379", "46373", "46368", "46345", "46344", "46127", "46122", "46002", "46001", "45996", "44543", "44568", "44544", "44545", "44546", "44549", "44550", "44551", "44552", "44553", "44564", "44566", "44567", "44574", "44630", "44582", "44583", "44584", "44585", "44586", "44587", "44588", "44589", "44590", "44591", "44592", "44593", "44594", "44595", "44596", "44597", "44616", "44617", "44618", "44619", "44620", "44621", "44622", "44623", "44626", "44634", "44635", "44636", "44640", "44641", "44642", "44643", "44644", "44649", "44658", "44659", "44660", "44671", "44679", "45374", "45846", "45854", "45855", "45877", "45878", "45879", "45880", "45881", "45882", "45883", "45884", "45886", "45887", "45888", "45889", "45903", "45904", "45710", "45711", "45712", "45713", "45714", "45719"];

            if (arrConsulta_t.includes($(this).val())) {


                $("#G2970_C60446").val("Consulta");

            } else if (arrReclamo_t.includes($(this).val())) {


                $("#G2970_C60446").val("Reclamo");

            } else if (arrSolicitud_t.includes($(this).val())) {


                $("#G2970_C60446").val("Solicitud");

            }


            var arr24H_t = ["44564", "47404", "47403", "47352", "47350", "47349", "46380", "46379", "46373", "46367", "46368", "46365", "46364", "46352", "46351", "46344", "46002", "46001", "45998", "45997", "44566", "45357", "44571", "44631", "44632", "44598", "44599", "44600", "45358", "44604", "44605", "44606", "44607", "44608", "44611", "44612", "44614", "44618", "44619", "44620", "44622", "44623", "44626", "44643", "44645", "44655", "44656", "45373", "45375", "45376", "45377", "45378", "45383", "45379", "45380", "45381", "45382", "45848", "45849", "45850", "45851", "45852", "45853", "45858", "45859", "45861", "45863", "45864", "45865", "45866", "45867", "45870", "45878", "45880", "45881", "45882", "45884", "45889", "45890", "45891", "45892", "45894", "45896", "45897", "45898", "45899", "45900", "45901", "45903", "45904", "45709", "45712", "45713", "45714", "45715", "45717", "45719"];


            var arr48H_t = ["44649", "47393", "44659", "44660", "44671"];

            var arr72H_t = ["44648", "44664", "44665", "44666", "44667", "44668", "44669", "44670", "44672", "44673", "45360", "45361", "45362"];

            var arr360H_t = ["45887", "45888"];

            var arr504H_t = ["45893"];

            if (arr24H_t.includes($(this).val())) {

                $("#G2970_C60447").val("24H");

            } else if (arr48H_t.includes($(this).val())) {

                $("#G2970_C60447").val("48H");

            } else if (arr72H_t.includes($(this).val())) {

                $("#G2970_C60447").val("72H");

            } else if (arr360H_t.includes($(this).val())) {

                $("#G2970_C60447").val("360H");

            } else if (arr504H_t.includes($(this).val())) {

                $("#G2970_C60447").val("504H");

            } else if (arr360H_t.includes($(this).val())) {

                $("#G2970_C60447").val("504H");

            }

        });

        $("#G2970_C59286").change(function() {
            if ($(this).val() == "44555") {
                $("#G2970_C59288 > option[value='44952']").attr("disabled", false);
                $("#G2970_C59288").val("44952").trigger("change");
            } else if ($(this).val() == "44554") {
                $("#G2970_C59288 > option[value='44527']").attr("disabled", false);
                $("#G2970_C59288 > option[value='44952']").attr("disabled", true);
                $("#G2970_C59288").val("44527").trigger("change");
            }
        });

        $("#G2970_C61082").trigger('change');
    })

</script>
