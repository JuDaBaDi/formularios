<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2973_ConsInte__b, G2973_FechaInsercion , G2973_Usuario ,  G2973_CodigoMiembro  , G2973_PoblacionOrigen , G2973_EstadoDiligenciamiento ,  G2973_IdLlamada , G2973_C59322 as principal ,G2973_C59320,G2973_C59322,G2973_C60318,G2973_C60319,G2973_C59321,G2973_C59544,G2973_C59545,G2973_C59546,G2973_C59307,G2973_C59308,G2973_C59309,G2973_C59310,G2973_C59311,G2973_C59312,G2973_C59313,G2973_C59314,G2973_C59315,G2973_C59547,G2973_C59548,G2973_C59549,G2973_C59550,G2973_C59551,G2973_C59552,G2973_C60139,G2973_C60140 FROM '.$BaseDatos.'.G2973 WHERE G2973_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2973_C59320'] = $key->G2973_C59320;

                $datos[$i]['G2973_C59322'] = $key->G2973_C59322;

                $datos[$i]['G2973_C60318'] = $key->G2973_C60318;

                $datos[$i]['G2973_C60319'] = $key->G2973_C60319;

                $datos[$i]['G2973_C59321'] = $key->G2973_C59321;

                $datos[$i]['G2973_C59544'] = $key->G2973_C59544;

                $datos[$i]['G2973_C59545'] = $key->G2973_C59545;

                $datos[$i]['G2973_C59546'] = $key->G2973_C59546;

                $datos[$i]['G2973_C59307'] = $key->G2973_C59307;

                $datos[$i]['G2973_C59308'] = $key->G2973_C59308;

                $datos[$i]['G2973_C59309'] = explode(' ', $key->G2973_C59309)[0];
  
                $hora = '';
                if(!is_null($key->G2973_C59310)){
                    $hora = explode(' ', $key->G2973_C59310)[1];
                }

                $datos[$i]['G2973_C59310'] = $hora;

                $datos[$i]['G2973_C59311'] = $key->G2973_C59311;

                $datos[$i]['G2973_C59312'] = $key->G2973_C59312;

                $datos[$i]['G2973_C59313'] = $key->G2973_C59313;

                $datos[$i]['G2973_C59314'] = $key->G2973_C59314;

                $datos[$i]['G2973_C59315'] = $key->G2973_C59315;

                $datos[$i]['G2973_C59547'] = $key->G2973_C59547;

                $datos[$i]['G2973_C59548'] = $key->G2973_C59548;

                $datos[$i]['G2973_C59549'] = $key->G2973_C59549;

                $datos[$i]['G2973_C59550'] = $key->G2973_C59550;

                $datos[$i]['G2973_C59551'] = $key->G2973_C59551;

                $datos[$i]['G2973_C59552'] = $key->G2973_C59552;

                $datos[$i]['G2973_C60139'] = $key->G2973_C60139;

                $datos[$i]['G2973_C60140'] = $key->G2973_C60140;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2973";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2973_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1 
                     FROM ".$BaseDatos.".G2973  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1  
                    FROM ".$BaseDatos.".G2973  JOIN ".$BaseDatos.".G2973_M".$_POST['muestra']." ON G2973_ConsInte__b = G2973_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2973_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2973_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2973_C59320 LIKE '%".$B."%' OR G2973_C59322 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2973_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2973_C60318'])){
                                $Ysql = "SELECT G3021_ConsInte__b as id, G3021_C60307 as text FROM ".$BaseDatos.".G3021 WHERE G3021_C60307 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2973_C60318"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3021 WHERE G3021_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2973_C60318"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }

            if(isset($_GET['MostrarCombo_Guion_2970_C59281'])){
                echo '<select class="form-control input-sm"  name="2970_C59281" id="2970_C59281">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2970_C59281'])){
                $Ysql = "SELECT G2969_ConsInte__b as id,  G2969_C59267 as text FROM ".$BaseDatos.".G2969 WHERE G2969_C59267 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2970_C59281'])){
                $Lsql = "SELECT  G2969_ConsInte__b as id , G3038_C60939, G2969_C59268, G2969_C59267, G2969_C60143, G2969_C60144, G2969_C59340, G2969_C59342, G2969_C59270, G2969_C60940, G2968_C59253, G2968_C59249, G2968_C59251, G2968_C59254, G2968_C59255, G2968_C59257, G2968_C60138, G2968_C59250 FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2970_C59281'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2970_C60311'] = $key->G3038_C60939;

                    $data[$i]['G2970_C59282'] = $key->G2969_C59268;

                    $data[$i]['G2970_C59283'] = $key->G2969_C59267;

                    $data[$i]['G2970_C59346'] = $key->G2969_C60143;

                    $data[$i]['G2970_C59345'] = $key->G2969_C60144;

                    $data[$i]['G2970_C59347'] = $key->G2969_C59340;

                    $data[$i]['G2970_C59348'] = $key->G2969_C59342;

                    $data[$i]['G2970_C59349'] = $key->G2969_C59270;

                    $data[$i]['G2970_C60310'] = $key->G2969_C60940;

                    $data[$i]['G2970_C59295'] = $key->G2968_C59253;

                    $data[$i]['G2970_C59345'] = $key->G2968_C59249;

                    $data[$i]['G2970_C59346'] = $key->G2968_C59251;

                    $data[$i]['G2970_C59347'] = $key->G2968_C59254;

                    $data[$i]['G2970_C59348'] = $key->G2968_C59255;

                    $data[$i]['G2970_C59349'] = $key->G2968_C59257;

                    $data[$i]['G2970_C60136'] = $key->G2968_C60138;

                    $data[$i]['G2970_C60137'] = $key->G2968_C59250;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2973");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2973_ConsInte__b, G2973_FechaInsercion , G2973_Usuario ,  G2973_CodigoMiembro  , G2973_PoblacionOrigen , G2973_EstadoDiligenciamiento ,  G2973_IdLlamada , G2973_C59322 as principal ,G2973_C59320,G2973_C59322, G3021_C60307,G2973_C60319,G2973_C59321,G2973_C59544,G2973_C59545,G2973_C59546, a.LISOPC_Nombre____b as G2973_C59307, b.LISOPC_Nombre____b as G2973_C59308,G2973_C59309,G2973_C59310,G2973_C59311,G2973_C59312,G2973_C59313,G2973_C59314,G2973_C59315,G2973_C59547,G2973_C59548,G2973_C59549,G2973_C59550,G2973_C59551,G2973_C59552,G2973_C60139,G2973_C60140 FROM '.$BaseDatos.'.G2973 LEFT JOIN '.$BaseDatos.'.G3021 ON G3021_ConsInte__b  =  G2973_C60318 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2973_C59307 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2973_C59308';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2973_C59310)){
                    $hora_a = explode(' ', $fila->G2973_C59310)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2973_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2973_ConsInte__b , ($fila->G2973_C59320) , ($fila->G2973_C59322) , ($fila->G3021_C60307) , ($fila->G2973_C60319) , ($fila->G2973_C59321) , ($fila->G2973_C59544) , ($fila->G2973_C59545) , ($fila->G2973_C59546) , ($fila->G2973_C59307) , ($fila->G2973_C59308) , explode(' ', $fila->G2973_C59309)[0] , $hora_a , ($fila->G2973_C59311) , ($fila->G2973_C59312) , ($fila->G2973_C59313) , ($fila->G2973_C59314) , ($fila->G2973_C59315) , ($fila->G2973_C59547) , ($fila->G2973_C59548) , ($fila->G2973_C59549) , ($fila->G2973_C59550) , ($fila->G2973_C59551) , ($fila->G2973_C59552) , ($fila->G2973_C60139) , ($fila->G2973_C60140) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2973 WHERE G2973_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2973";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2973_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1  FROM '.$BaseDatos.'.G2973 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2973_ConsInte__b as id,  G2973_C59320 as camp2 , G2973_C59322 as camp1  
                    FROM ".$BaseDatos.".G2973  JOIN ".$BaseDatos.".G2973_M".$_POST['muestra']." ON G2973_ConsInte__b = G2973_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2973_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2973_C59320 LIKE "%'.$B.'%" OR G2973_C59322 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2973_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2973 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2973(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2973_C59320"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59320 = '".$_POST["G2973_C59320"]."'";
                $LsqlI .= $separador."G2973_C59320";
                $LsqlV .= $separador."'".$_POST["G2973_C59320"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59322 = '".$_POST["G2973_C59322"]."'";
                $LsqlI .= $separador."G2973_C59322";
                $LsqlV .= $separador."'".$_POST["G2973_C59322"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C60318"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C60318 = '".$_POST["G2973_C60318"]."'";
                $LsqlI .= $separador."G2973_C60318";
                $LsqlV .= $separador."'".$_POST["G2973_C60318"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C60319"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C60319 = '".$_POST["G2973_C60319"]."'";
                $LsqlI .= $separador."G2973_C60319";
                $LsqlV .= $separador."'".$_POST["G2973_C60319"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59321"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59321 = '".$_POST["G2973_C59321"]."'";
                $LsqlI .= $separador."G2973_C59321";
                $LsqlV .= $separador."'".$_POST["G2973_C59321"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59544"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59544 = '".$_POST["G2973_C59544"]."'";
                $LsqlI .= $separador."G2973_C59544";
                $LsqlV .= $separador."'".$_POST["G2973_C59544"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59545 = '".$_POST["G2973_C59545"]."'";
                $LsqlI .= $separador."G2973_C59545";
                $LsqlV .= $separador."'".$_POST["G2973_C59545"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59546"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59546 = '".$_POST["G2973_C59546"]."'";
                $LsqlI .= $separador."G2973_C59546";
                $LsqlV .= $separador."'".$_POST["G2973_C59546"]."'";
                $validar = 1;
            }
             
 
            $G2973_C59307 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59307 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2973_C59307 = ".$G2973_C59307;
                    $LsqlI .= $separador." G2973_C59307";
                    $LsqlV .= $separador.$G2973_C59307;
                    $validar = 1;

                    
                }
            }
 
            $G2973_C59308 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59308 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2973_C59308 = ".$G2973_C59308;
                    $LsqlI .= $separador." G2973_C59308";
                    $LsqlV .= $separador.$G2973_C59308;
                    $validar = 1;
                }
            }
 
            $G2973_C59309 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59309 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2973_C59309 = ".$G2973_C59309;
                    $LsqlI .= $separador." G2973_C59309";
                    $LsqlV .= $separador.$G2973_C59309;
                    $validar = 1;
                }
            }
 
            $G2973_C59310 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59310 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2973_C59310 = ".$G2973_C59310;
                    $LsqlI .= $separador." G2973_C59310";
                    $LsqlV .= $separador.$G2973_C59310;
                    $validar = 1;
                }
            }
 
            $G2973_C59311 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2973_C59311 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2973_C59311 = ".$G2973_C59311;
                    $LsqlI .= $separador." G2973_C59311";
                    $LsqlV .= $separador.$G2973_C59311;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2973_C59312"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59312 = '".$_POST["G2973_C59312"]."'";
                $LsqlI .= $separador."G2973_C59312";
                $LsqlV .= $separador."'".$_POST["G2973_C59312"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59313 = '".$_POST["G2973_C59313"]."'";
                $LsqlI .= $separador."G2973_C59313";
                $LsqlV .= $separador."'".$_POST["G2973_C59313"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59314 = '".$_POST["G2973_C59314"]."'";
                $LsqlI .= $separador."G2973_C59314";
                $LsqlV .= $separador."'".$_POST["G2973_C59314"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59315 = '".$_POST["G2973_C59315"]."'";
                $LsqlI .= $separador."G2973_C59315";
                $LsqlV .= $separador."'".$_POST["G2973_C59315"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59316 = '".$_POST["G2973_C59316"]."'";
                $LsqlI .= $separador."G2973_C59316";
                $LsqlV .= $separador."'".$_POST["G2973_C59316"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59317 = '".$_POST["G2973_C59317"]."'";
                $LsqlI .= $separador."G2973_C59317";
                $LsqlV .= $separador."'".$_POST["G2973_C59317"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59547"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59547 = '".$_POST["G2973_C59547"]."'";
                $LsqlI .= $separador."G2973_C59547";
                $LsqlV .= $separador."'".$_POST["G2973_C59547"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59548 = '".$_POST["G2973_C59548"]."'";
                $LsqlI .= $separador."G2973_C59548";
                $LsqlV .= $separador."'".$_POST["G2973_C59548"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59549"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59549 = '".$_POST["G2973_C59549"]."'";
                $LsqlI .= $separador."G2973_C59549";
                $LsqlV .= $separador."'".$_POST["G2973_C59549"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59550"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59550 = '".$_POST["G2973_C59550"]."'";
                $LsqlI .= $separador."G2973_C59550";
                $LsqlV .= $separador."'".$_POST["G2973_C59550"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59551 = '".$_POST["G2973_C59551"]."'";
                $LsqlI .= $separador."G2973_C59551";
                $LsqlV .= $separador."'".$_POST["G2973_C59551"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C59552"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C59552 = '".$_POST["G2973_C59552"]."'";
                $LsqlI .= $separador."G2973_C59552";
                $LsqlV .= $separador."'".$_POST["G2973_C59552"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C60139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C60139 = '".$_POST["G2973_C60139"]."'";
                $LsqlI .= $separador."G2973_C60139";
                $LsqlV .= $separador."'".$_POST["G2973_C60139"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2973_C60140"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_C60140 = '".$_POST["G2973_C60140"]."'";
                $LsqlI .= $separador."G2973_C60140";
                $LsqlV .= $separador."'".$_POST["G2973_C60140"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2973_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2973_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2973_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2973_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2973_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2973_Usuario , G2973_FechaInsercion, G2973_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2973_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2973 WHERE G2973_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2973_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2973 WHERE G2973_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2970_ConsInte__b, G2970_C59276, G2970_C59277, G2970_C59278, G2970_C59279, G2970_C59280, G2970_C59295, G2969_C59266 as G2970_C59281, G2970_C59282, G2970_C59283, j.LISOPC_Nombre____b as  G2970_C59284, k.LISOPC_Nombre____b as  G2970_C59285, l.LISOPC_Nombre____b as  G2970_C59286, G2970_C59287, n.LISOPC_Nombre____b as  G2970_C59288 FROM ".$BaseDatos.".G2970  LEFT JOIN ".$BaseDatos.".G2969 ON G2969_ConsInte__b  =  G2970_C59281 LEFT JOIN ".$BaseDatos_systema.".LISOPC as j ON j.LISOPC_ConsInte__b =  G2970_C59284 LEFT JOIN ".$BaseDatos_systema.".LISOPC as k ON k.LISOPC_ConsInte__b =  G2970_C59285 LEFT JOIN ".$BaseDatos_systema.".LISOPC as l ON l.LISOPC_ConsInte__b =  G2970_C59286 LEFT JOIN ".$BaseDatos_systema.".LISOPC as n ON n.LISOPC_ConsInte__b =  G2970_C59288 ";

        if (isset($_GET["agente"])) {

            $SQL .= " WHERE G2970_C64637 = '".$_GET["agente"]."' AND G2970_C59288 = 44952"; 
            
        }else{

            $SQL .= " WHERE G2970_C59281 = '".$numero."'"; 

        }

        $SQL .= " ORDER BY G2970_C59276";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2970_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2970_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2970_C59276."</cell>"; 

                if($fila->G2970_C59277 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59277)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59278 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59278)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59279 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59279)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2970_C59280 != ''){
                    echo "<cell>". explode(' ', $fila->G2970_C59280)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2970_C59295)."</cell>";

                echo "<cell>". ($fila->G2970_C59281)."</cell>";

                echo "<cell>". ($fila->G2970_C59282)."</cell>";

                echo "<cell>". ($fila->G2970_C59283)."</cell>";

                echo "<cell>". ($fila->G2970_C59284)."</cell>";

                echo "<cell>". ($fila->G2970_C59285)."</cell>";

                echo "<cell>". ($fila->G2970_C59286)."</cell>";

                echo "<cell><![CDATA[". ($fila->G2970_C59287)."]]></cell>";

                echo "<cell>". ($fila->G2970_C59288)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2970 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2970(";
            $LsqlV = " VALUES ("; 
 
                $G2970_C59276= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2970_C59276"])){    
                    if($_POST["G2970_C59276"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59276 = $_POST["G2970_C59276"];
                        $LsqlU .= $separador." G2970_C59276 = '".$G2970_C59276."'";
                        $LsqlI .= $separador." G2970_C59276";
                        $LsqlV .= $separador."'".$G2970_C59276."'";
                        $validar = 1;
                    }
                }

                $G2970_C59277 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2970_C59277"])){    
                    if($_POST["G2970_C59277"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59277 = "'".str_replace(' ', '',$_POST["G2970_C59277"])." 00:00:00'";
                        $LsqlU .= $separador." G2970_C59277 = ".$G2970_C59277;
                        $LsqlI .= $separador." G2970_C59277";
                        $LsqlV .= $separador.$G2970_C59277;
                        $validar = 1;
                    }
                }
 
                $G2970_C59278 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2970_C59278"])){    
                    if($_POST["G2970_C59278"] != '' && $_POST["G2970_C59278"] != 'undefined' && $_POST["G2970_C59278"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59278 = "'".$fecha." ".str_replace(' ', '',$_POST["G2970_C59278"])."'";
                        $LsqlU .= $separador."  G2970_C59278 = ".$G2970_C59278."";
                        $LsqlI .= $separador."  G2970_C59278";
                        $LsqlV .= $separador.$G2970_C59278;
                        $validar = 1;
                    }
                }

                $G2970_C59279 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2970_C59279"])){    
                    if($_POST["G2970_C59279"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59279 = "'".str_replace(' ', '',$_POST["G2970_C59279"])." 00:00:00'";
                        $LsqlU .= $separador." G2970_C59279 = ".$G2970_C59279;
                        $LsqlI .= $separador." G2970_C59279";
                        $LsqlV .= $separador.$G2970_C59279;
                        $validar = 1;
                    }
                }
 
                $G2970_C59280 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G2970_C59280"])){    
                    if($_POST["G2970_C59280"] != '' && $_POST["G2970_C59280"] != 'undefined' && $_POST["G2970_C59280"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2970_C59280 = "'".$fecha." ".str_replace(' ', '',$_POST["G2970_C59280"])."'";
                        $LsqlU .= $separador."  G2970_C59280 = ".$G2970_C59280."";
                        $LsqlI .= $separador."  G2970_C59280";
                        $LsqlV .= $separador.$G2970_C59280;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2970_C59295"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59295 = '".$_POST["G2970_C59295"]."'";
                    $LsqlI .= $separador."G2970_C59295";
                    $LsqlV .= $separador."'".$_POST["G2970_C59295"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2970_C59282"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59282 = '".$_POST["G2970_C59282"]."'";
                    $LsqlI .= $separador."G2970_C59282";
                    $LsqlV .= $separador."'".$_POST["G2970_C59282"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2970_C59283"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59283 = '".$_POST["G2970_C59283"]."'";
                    $LsqlI .= $separador."G2970_C59283";
                    $LsqlV .= $separador."'".$_POST["G2970_C59283"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2970_C59284"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59284 = '".$_POST["G2970_C59284"]."'";
                    $LsqlI .= $separador."G2970_C59284";
                    $LsqlV .= $separador."'".$_POST["G2970_C59284"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G2970_C59285"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59285 = '".$_POST["G2970_C59285"]."'";
                    $LsqlI .= $separador."G2970_C59285";
                    $LsqlV .= $separador."'".$_POST["G2970_C59285"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G2970_C59286"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59286 = '".$_POST["G2970_C59286"]."'";
                    $LsqlI .= $separador."G2970_C59286";
                    $LsqlV .= $separador."'".$_POST["G2970_C59286"]."'";
                    $validar = 1;
                }
  

                if(isset($_POST["G2970_C59287"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59287 = '".$_POST["G2970_C59287"]."'";
                    $LsqlI .= $separador."G2970_C59287";
                    $LsqlV .= $separador."'".$_POST["G2970_C59287"]."'";
                    $validar = 1;
                }
                                                                               
 
                if(isset($_POST["G2970_C59288"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_C59288 = '".$_POST["G2970_C59288"]."'";
                    $LsqlI .= $separador."G2970_C59288";
                    $LsqlV .= $separador."'".$_POST["G2970_C59288"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2970_C59281 = $numero;
                    $LsqlU .= ", G2970_C59281 = ".$G2970_C59281."";
                    $LsqlI .= ", G2970_C59281";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2970_Usuario ,  G2970_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2970_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2970 WHERE  G2970_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
