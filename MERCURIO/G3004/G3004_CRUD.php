<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3004_ConsInte__b, G3004_FechaInsercion , G3004_Usuario ,  G3004_CodigoMiembro  , G3004_PoblacionOrigen , G3004_EstadoDiligenciamiento ,  G3004_IdLlamada , G3004_C59946 as principal ,G3004_C59940,G3004_C59941,G3004_C59942,G3004_C59943,G3004_C59944,G3004_C59945,G3004_C59946,G3004_C59947,G3004_C59948,G3004_C59949,G3004_C59950,G3004_C59951,G3004_C59952,G3004_C59953,G3004_C60244,G3004_C60245,G3004_C60246,G3004_C60247,G3004_C59937,G3004_C59938,G3004_C59939 FROM '.$BaseDatos.'.G3004 WHERE G3004_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3004_C59940'] = explode(' ', $key->G3004_C59940)[0];
  
                $hora = '';
                if(!is_null($key->G3004_C59941)){
                    $hora = explode(' ', $key->G3004_C59941)[1];
                }

                $datos[$i]['G3004_C59941'] = $hora;

                $datos[$i]['G3004_C59942'] = $key->G3004_C59942;

                $datos[$i]['G3004_C59943'] = $key->G3004_C59943;

                $datos[$i]['G3004_C59944'] = $key->G3004_C59944;

                $datos[$i]['G3004_C59945'] = $key->G3004_C59945;

                $datos[$i]['G3004_C59946'] = $key->G3004_C59946;

                $datos[$i]['G3004_C59947'] = $key->G3004_C59947;

                $datos[$i]['G3004_C59948'] = $key->G3004_C59948;

                $datos[$i]['G3004_C59949'] = $key->G3004_C59949;

                $datos[$i]['G3004_C59950'] = $key->G3004_C59950;

                $datos[$i]['G3004_C59951'] = $key->G3004_C59951;

                $datos[$i]['G3004_C59952'] = $key->G3004_C59952;

                $datos[$i]['G3004_C59953'] = $key->G3004_C59953;

                $datos[$i]['G3004_C60244'] = $key->G3004_C60244;

                $datos[$i]['G3004_C60245'] = $key->G3004_C60245;

                $datos[$i]['G3004_C60246'] = $key->G3004_C60246;

                $datos[$i]['G3004_C60247'] = $key->G3004_C60247;

                $datos[$i]['G3004_C59937'] = $key->G3004_C59937;

                $datos[$i]['G3004_C59938'] = $key->G3004_C59938;

                $datos[$i]['G3004_C59939'] = $key->G3004_C59939;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3004";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3004_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3004_ConsInte__b as id,  G3004_C59946 as camp1 , G3004_C59950 as camp2 
                     FROM ".$BaseDatos.".G3004  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3004_ConsInte__b as id,  G3004_C59946 as camp1 , G3004_C59950 as camp2  
                    FROM ".$BaseDatos.".G3004  JOIN ".$BaseDatos.".G3004_M".$_POST['muestra']." ON G3004_ConsInte__b = G3004_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3004_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3004_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3004_C59946 LIKE '%".$B."%' OR G3004_C59950 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3004_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3004");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3004_ConsInte__b, G3004_FechaInsercion , G3004_Usuario ,  G3004_CodigoMiembro  , G3004_PoblacionOrigen , G3004_EstadoDiligenciamiento ,  G3004_IdLlamada , G3004_C59946 as principal ,G3004_C59940,G3004_C59941,G3004_C59942,G3004_C59943,G3004_C59944,G3004_C59945,G3004_C59946,G3004_C59947,G3004_C59948,G3004_C59949,G3004_C59950, a.LISOPC_Nombre____b as G3004_C59951, b.LISOPC_Nombre____b as G3004_C59952,G3004_C59953, c.LISOPC_Nombre____b as G3004_C60244,G3004_C60245,G3004_C60246,G3004_C60247,G3004_C59937,G3004_C59938, d.LISOPC_Nombre____b as G3004_C59939 FROM '.$BaseDatos.'.G3004 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3004_C59951 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3004_C59952 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3004_C60244 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3004_C59939';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3004_C59941)){
                    $hora_a = explode(' ', $fila->G3004_C59941)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3004_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3004_ConsInte__b , explode(' ', $fila->G3004_C59940)[0] , $hora_a , ($fila->G3004_C59942) , ($fila->G3004_C59943) , ($fila->G3004_C59944) , ($fila->G3004_C59945) , ($fila->G3004_C59946) , ($fila->G3004_C59947) , ($fila->G3004_C59948) , ($fila->G3004_C59949) , ($fila->G3004_C59950) , ($fila->G3004_C59951) , ($fila->G3004_C59952) , ($fila->G3004_C59953) , ($fila->G3004_C60244) , ($fila->G3004_C60245) , ($fila->G3004_C60246) , ($fila->G3004_C60247) , ($fila->G3004_C59937) , ($fila->G3004_C59938) , ($fila->G3004_C59939) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3004 WHERE G3004_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3004";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3004_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3004_ConsInte__b as id,  G3004_C59946 as camp1 , G3004_C59950 as camp2  FROM '.$BaseDatos.'.G3004 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3004_ConsInte__b as id,  G3004_C59946 as camp1 , G3004_C59950 as camp2  
                    FROM ".$BaseDatos.".G3004  JOIN ".$BaseDatos.".G3004_M".$_POST['muestra']." ON G3004_ConsInte__b = G3004_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3004_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3004_C59946 LIKE "%'.$B.'%" OR G3004_C59950 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3004_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3004 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3004(";
            $LsqlV = " VALUES ("; 
 
            $G3004_C59940 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3004_C59940"])){    
                if($_POST["G3004_C59940"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3004_C59940"]);
                    if(count($tieneHora) > 1){
                        $G3004_C59940 = "'".$_POST["G3004_C59940"]."'";
                    }else{
                        $G3004_C59940 = "'".str_replace(' ', '',$_POST["G3004_C59940"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3004_C59940 = ".$G3004_C59940;
                    $LsqlI .= $separador." G3004_C59940";
                    $LsqlV .= $separador.$G3004_C59940;
                    $validar = 1;
                }
            }
  
            $G3004_C59941 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3004_C59941"])){   
                if($_POST["G3004_C59941"] != '' && $_POST["G3004_C59941"] != 'undefined' && $_POST["G3004_C59941"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3004_C59941 = "'".$fecha." ".str_replace(' ', '',$_POST["G3004_C59941"])."'";
                    $LsqlU .= $separador." G3004_C59941 = ".$G3004_C59941."";
                    $LsqlI .= $separador." G3004_C59941";
                    $LsqlV .= $separador.$G3004_C59941;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3004_C59942"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59942 = '".$_POST["G3004_C59942"]."'";
                $LsqlI .= $separador."G3004_C59942";
                $LsqlV .= $separador."'".$_POST["G3004_C59942"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59943"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59943 = '".$_POST["G3004_C59943"]."'";
                $LsqlI .= $separador."G3004_C59943";
                $LsqlV .= $separador."'".$_POST["G3004_C59943"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59944"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59944 = '".$_POST["G3004_C59944"]."'";
                $LsqlI .= $separador."G3004_C59944";
                $LsqlV .= $separador."'".$_POST["G3004_C59944"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59945"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59945 = '".$_POST["G3004_C59945"]."'";
                $LsqlI .= $separador."G3004_C59945";
                $LsqlV .= $separador."'".$_POST["G3004_C59945"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59946"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59946 = '".$_POST["G3004_C59946"]."'";
                $LsqlI .= $separador."G3004_C59946";
                $LsqlV .= $separador."'".$_POST["G3004_C59946"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59947"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59947 = '".$_POST["G3004_C59947"]."'";
                $LsqlI .= $separador."G3004_C59947";
                $LsqlV .= $separador."'".$_POST["G3004_C59947"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59948"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59948 = '".$_POST["G3004_C59948"]."'";
                $LsqlI .= $separador."G3004_C59948";
                $LsqlV .= $separador."'".$_POST["G3004_C59948"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59949"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59949 = '".$_POST["G3004_C59949"]."'";
                $LsqlI .= $separador."G3004_C59949";
                $LsqlV .= $separador."'".$_POST["G3004_C59949"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59950"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59950 = '".$_POST["G3004_C59950"]."'";
                $LsqlI .= $separador."G3004_C59950";
                $LsqlV .= $separador."'".$_POST["G3004_C59950"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59951"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59951 = '".$_POST["G3004_C59951"]."'";
                $LsqlI .= $separador."G3004_C59951";
                $LsqlV .= $separador."'".$_POST["G3004_C59951"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59952"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59952 = '".$_POST["G3004_C59952"]."'";
                $LsqlI .= $separador."G3004_C59952";
                $LsqlV .= $separador."'".$_POST["G3004_C59952"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59953"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59953 = '".$_POST["G3004_C59953"]."'";
                $LsqlI .= $separador."G3004_C59953";
                $LsqlV .= $separador."'".$_POST["G3004_C59953"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C60244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C60244 = '".$_POST["G3004_C60244"]."'";
                $LsqlI .= $separador."G3004_C60244";
                $LsqlV .= $separador."'".$_POST["G3004_C60244"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C60245"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C60245 = '".$_POST["G3004_C60245"]."'";
                $LsqlI .= $separador."G3004_C60245";
                $LsqlV .= $separador."'".$_POST["G3004_C60245"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C60246"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C60246 = '".$_POST["G3004_C60246"]."'";
                $LsqlI .= $separador."G3004_C60246";
                $LsqlV .= $separador."'".$_POST["G3004_C60246"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C60247"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C60247 = '".$_POST["G3004_C60247"]."'";
                $LsqlI .= $separador."G3004_C60247";
                $LsqlV .= $separador."'".$_POST["G3004_C60247"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59937"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59937 = '".$_POST["G3004_C59937"]."'";
                $LsqlI .= $separador."G3004_C59937";
                $LsqlV .= $separador."'".$_POST["G3004_C59937"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59938"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59938 = '".$_POST["G3004_C59938"]."'";
                $LsqlI .= $separador."G3004_C59938";
                $LsqlV .= $separador."'".$_POST["G3004_C59938"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3004_C59939"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_C59939 = '".$_POST["G3004_C59939"]."'";
                $LsqlI .= $separador."G3004_C59939";
                $LsqlV .= $separador."'".$_POST["G3004_C59939"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3004_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3004_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3004_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3004_Usuario , G3004_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3004_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3004 WHERE G3004_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3004 SET G3004_UltiGest__b =-14, G3004_GesMasImp_b =-14, G3004_TipoReintentoUG_b =0, G3004_TipoReintentoGMI_b =0, G3004_EstadoUG_b =-14, G3004_EstadoGMI_b =-14, G3004_CantidadIntentos =0, G3004_CantidadIntentosGMI_b =0 WHERE G3004_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
