<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		if($_GET['action'] == "GET_DATOS"){

			//JDBD - Busqueda manual.          
            if(isset($_GET["id_campana_crm"]) && $_GET["id_campana_crm"] != 0){

                $querymuestra="SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b FROM ".$BaseDatos_systema.".CAMPAN where CAMPAN_ConsInte__b=".$_GET["id_campana_crm"];

                $querymuestra=$mysqli->query($querymuestra);

                if($querymuestra && $querymuestra->num_rows > 0){
                    $datoCampan=$querymuestra->fetch_array();

                    $str_Pobla_Campan = "G".$datoCampan["CAMPAN_ConsInte__GUION__Pob_b"];
                    $intMuestra= $str_Pobla_Campan."_M".$datoCampan["CAMPAN_ConsInte__MUESTR_b"];
                    
                }        
            }
            $str_Lsql = " SELECT *, ".$intMuestra."_CoInMiPo__b AS id_muestra FROM ".$BaseDatos.".".$str_Pobla_Campan." LEFT JOIN ".$BaseDatos.".".$intMuestra." ON ".$str_Pobla_Campan."_ConsInte__b = ".$intMuestra."_CoInMiPo__b";
                $Where = " WHERE ".$str_Pobla_Campan."_ConsInte__b > 0 AND ";
			$usados = 0;
			if(isset($_POST['G3004_C59946']) && !is_null($_POST['G3004_C59946']) && $_POST['G3004_C59946'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G3004_C59946 LIKE '%". $_POST['G3004_C59946'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G3004_C59947']) && !is_null($_POST['G3004_C59947']) && $_POST['G3004_C59947'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G3004_C59947 LIKE '%". $_POST['G3004_C59947'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G3004_C59948']) && !is_null($_POST['G3004_C59948']) && $_POST['G3004_C59948'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G3004_C59948 LIKE '%". $_POST['G3004_C59948'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G3004_C59949']) && !is_null($_POST['G3004_C59949']) && $_POST['G3004_C59949'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G3004_C59949 LIKE '%". $_POST['G3004_C59949'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G3004_C59950']) && !is_null($_POST['G3004_C59950']) && $_POST['G3004_C59950'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G3004_C59950 LIKE '%". $_POST['G3004_C59950'] ."%' ";
				$usados = 1;
			}
            if($usados==0){
                    $Where = "";
                }
                $str_Lsql .= $Where;
                
            //echo $str_Lsql;
			$resultado = $mysqli->query($str_Lsql);
			$arrayDatos = array();
			while ($key = $resultado->fetch_assoc()) {
				$arrayDatos[] = $key;
			}

			$newJson = array();
			$newJson[0]['cantidad_registros'] = $resultado->num_rows;
			$newJson[0]['registros'] = $arrayDatos;
            if($usados == 0 && $resultado->num_rows == 0){
                $newJson[0]['mensaje'] = "No se encontraron registros para este tipo de busqueda";
            }else{
                if($usados == 1 && $resultado->num_rows == 0){
                    $strSQLValidaReg="SELECT * FROM {$BaseDatos}.{$str_Pobla_Campan}{$Where}";
                       $strSQLValidaReg=$mysqli->query($strSQLValidaReg);
                    if($strSQLValidaReg ->num_rows > 0){
                        $newJson[0]['mensaje'] = "No tienes permiso para acceder a este registro";
                    }
                }
            }

			echo json_encode($newJson);
		}
	}
?>
