<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3007_ConsInte__b, G3007_FechaInsercion , G3007_Usuario ,  G3007_CodigoMiembro  , G3007_PoblacionOrigen , G3007_EstadoDiligenciamiento ,  G3007_IdLlamada , G3007_C60007 as principal ,G3007_C60005,G3007_C60004,G3007_C60006,G3007_C60314,G3007_C60007,G3007_C60008,G3007_C60009,G3007_C60010,G3007_C60011,G3007_C60012,G3007_C60013,G3007_C60195,G3007_C60189,G3007_C60190,G3007_C60191,G3007_C60192,G3007_C60193,G3007_C60194,G3007_C59993,G3007_C59994,G3007_C59995,G3007_C59996,G3007_C59997,G3007_C59998,G3007_C59999,G3007_C60000,G3007_C60001,G3007_C60223,G3007_C60224,G3007_C60225,G3007_C60226, G3007_C64436 FROM '.$BaseDatos.'.G3007 WHERE G3007_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3007_C60005'] = $key->G3007_C60005;

                $datos[$i]['G3007_C60004'] = $key->G3007_C60004;

                $datos[$i]['G3007_C60006'] = $key->G3007_C60006;

                $datos[$i]['G3007_C60314'] = $key->G3007_C60314;

                $datos[$i]['G3007_C60007'] = $key->G3007_C60007;

                $datos[$i]['G3007_C60008'] = $key->G3007_C60008;

                $datos[$i]['G3007_C60009'] = $key->G3007_C60009;

                $datos[$i]['G3007_C60010'] = $key->G3007_C60010;

                $datos[$i]['G3007_C60011'] = $key->G3007_C60011;

                $datos[$i]['G3007_C60012'] = $key->G3007_C60012;

                $datos[$i]['G3007_C60013'] = $key->G3007_C60013;

                $datos[$i]['G3007_C60195'] = $key->G3007_C60195;

                $datos[$i]['G3007_C60189'] = explode(' ', $key->G3007_C60189)[0];
  
                $hora = '';
                if(!is_null($key->G3007_C60190)){
                    $hora = explode(' ', $key->G3007_C60190)[1];
                }

                $datos[$i]['G3007_C60190'] = $hora;

                $datos[$i]['G3007_C60191'] = explode(' ', $key->G3007_C60191)[0];
  
                $hora = '';
                if(!is_null($key->G3007_C60192)){
                    $hora = explode(' ', $key->G3007_C60192)[1];
                }

                $datos[$i]['G3007_C60192'] = $hora;

                $datos[$i]['G3007_C60193'] = explode(' ', $key->G3007_C60193)[0];
  
                $hora = '';
                if(!is_null($key->G3007_C60194)){
                    $hora = explode(' ', $key->G3007_C60194)[1];
                }

                $datos[$i]['G3007_C60194'] = $hora;

                $datos[$i]['G3007_C59993'] = $key->G3007_C59993;

                $datos[$i]['G3007_C59994'] = $key->G3007_C59994;

                $datos[$i]['G3007_C59995'] = explode(' ', $key->G3007_C59995)[0];
  
                $hora = '';
                if(!is_null($key->G3007_C59996)){
                    $hora = explode(' ', $key->G3007_C59996)[1];
                }

                $datos[$i]['G3007_C59996'] = $hora;

                $datos[$i]['G3007_C59997'] = $key->G3007_C59997;

                $datos[$i]['G3007_C59998'] = $key->G3007_C59998;

                $datos[$i]['G3007_C59999'] = $key->G3007_C59999;

                $datos[$i]['G3007_C60000'] = $key->G3007_C60000;

                $datos[$i]['G3007_C60001'] = $key->G3007_C60001;

                $datos[$i]['G3007_C60223'] = $key->G3007_C60223;

                $datos[$i]['G3007_C60224'] = $key->G3007_C60224;

                $datos[$i]['G3007_C60225'] = $key->G3007_C60225;

                $datos[$i]['G3007_C60226'] = $key->G3007_C60226;

                $datos[$i]['G3007_C64436'] = $key->G3007_C64436;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3007";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3007_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3007_ConsInte__b as id,  G3007_C60007 as camp1 , G3007_C60010 as camp2 
                     FROM ".$BaseDatos.".G3007  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3007_ConsInte__b as id,  G3007_C60007 as camp1 , G3007_C60010 as camp2  
                    FROM ".$BaseDatos.".G3007  JOIN ".$BaseDatos.".G3007_M".$_POST['muestra']." ON G3007_ConsInte__b = G3007_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3007_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3007_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3007_C60007 LIKE '%".$B."%' OR G3007_C60010 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3007_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G3007_C60005'])){
                                $Ysql = "SELECT G3038_ConsInte__b as id, G3038_C60938 as text FROM ".$BaseDatos.".G3038 WHERE G3038_C60938 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3007_C60005"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3038 WHERE G3038_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3007_C60005"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3007");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3007_ConsInte__b, G3007_FechaInsercion , G3007_Usuario ,  G3007_CodigoMiembro  , G3007_PoblacionOrigen , G3007_EstadoDiligenciamiento ,  G3007_IdLlamada , G3007_C60007 as principal , G3021_C60307,G3007_C60004,G3007_C60006, a.LISOPC_Nombre____b as G3007_C60314,G3007_C60007,G3007_C60008,G3007_C60009,G3007_C60010, b.LISOPC_Nombre____b as G3007_C60011, c.LISOPC_Nombre____b as G3007_C60012,G3007_C60013, d.LISOPC_Nombre____b as G3007_C60195,G3007_C60189,G3007_C60190,G3007_C60191,G3007_C60192,G3007_C60193,G3007_C60194, e.LISOPC_Nombre____b as G3007_C59993, f.LISOPC_Nombre____b as G3007_C59994,G3007_C59995,G3007_C59996,G3007_C59997,G3007_C59998,G3007_C59999,G3007_C60000,G3007_C60001, g.LISOPC_Nombre____b as G3007_C60223,G3007_C60224,G3007_C60225,G3007_C60226 FROM '.$BaseDatos.'.G3007 LEFT JOIN '.$BaseDatos.'.G3021 ON G3021_ConsInte__b  =  G3007_C60005 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3007_C60314 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3007_C60011 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3007_C60012 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3007_C60195 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3007_C59993 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3007_C59994 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G3007_C60223';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3007_C59996)){
                    $hora_a = explode(' ', $fila->G3007_C59996)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3007_C60190)){
                    $hora_b = explode(' ', $fila->G3007_C60190)[1];
                }

                $hora_c = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3007_C60192)){
                    $hora_c = explode(' ', $fila->G3007_C60192)[1];
                }

                $hora_d = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3007_C60194)){
                    $hora_d = explode(' ', $fila->G3007_C60194)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3007_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3007_ConsInte__b , ($fila->G3021_C60307) , ($fila->G3007_C60004) , ($fila->G3007_C60006) , ($fila->G3007_C60314) , ($fila->G3007_C60007) , ($fila->G3007_C60008) , ($fila->G3007_C60009) , ($fila->G3007_C60010) , ($fila->G3007_C60011) , ($fila->G3007_C60012) , ($fila->G3007_C60013) , ($fila->G3007_C60195) , explode(' ', $fila->G3007_C60189)[0] , $hora_a , explode(' ', $fila->G3007_C60191)[0] , $hora_b , explode(' ', $fila->G3007_C60193)[0] , $hora_c , ($fila->G3007_C59993) , ($fila->G3007_C59994) , explode(' ', $fila->G3007_C59995)[0] , $hora_d , ($fila->G3007_C59997) , ($fila->G3007_C59998) , ($fila->G3007_C59999) , ($fila->G3007_C60000) , ($fila->G3007_C60001) , ($fila->G3007_C60223) , ($fila->G3007_C60224) , ($fila->G3007_C60225) , ($fila->G3007_C60226) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3007 WHERE G3007_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3007";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3007_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3007_ConsInte__b as id,  G3007_C60007 as camp1 , G3007_C60010 as camp2  FROM '.$BaseDatos.'.G3007 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3007_ConsInte__b as id,  G3007_C60007 as camp1 , G3007_C60010 as camp2  
                    FROM ".$BaseDatos.".G3007  JOIN ".$BaseDatos.".G3007_M".$_POST['muestra']." ON G3007_ConsInte__b = G3007_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3007_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3007_C60007 LIKE "%'.$B.'%" OR G3007_C60010 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3007_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3007 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3007(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3007_C60005"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60005 = '".$_POST["G3007_C60005"]."'";
                $LsqlI .= $separador."G3007_C60005";
                $LsqlV .= $separador."'".$_POST["G3007_C60005"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60004"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60004 = '".$_POST["G3007_C60004"]."'";
                $LsqlI .= $separador."G3007_C60004";
                $LsqlV .= $separador."'".$_POST["G3007_C60004"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60006"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60006 = '".$_POST["G3007_C60006"]."'";
                $LsqlI .= $separador."G3007_C60006";
                $LsqlV .= $separador."'".$_POST["G3007_C60006"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60314 = '".$_POST["G3007_C60314"]."'";
                $LsqlI .= $separador."G3007_C60314";
                $LsqlV .= $separador."'".$_POST["G3007_C60314"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60007"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60007 = '".$_POST["G3007_C60007"]."'";
                $LsqlI .= $separador."G3007_C60007";
                $LsqlV .= $separador."'".$_POST["G3007_C60007"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60008"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60008 = '".$_POST["G3007_C60008"]."'";
                $LsqlI .= $separador."G3007_C60008";
                $LsqlV .= $separador."'".$_POST["G3007_C60008"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60009"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60009 = '".$_POST["G3007_C60009"]."'";
                $LsqlI .= $separador."G3007_C60009";
                $LsqlV .= $separador."'".$_POST["G3007_C60009"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60010"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60010 = '".$_POST["G3007_C60010"]."'";
                $LsqlI .= $separador."G3007_C60010";
                $LsqlV .= $separador."'".$_POST["G3007_C60010"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60011"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60011 = '".$_POST["G3007_C60011"]."'";
                $LsqlI .= $separador."G3007_C60011";
                $LsqlV .= $separador."'".$_POST["G3007_C60011"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60012"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60012 = '".$_POST["G3007_C60012"]."'";
                $LsqlI .= $separador."G3007_C60012";
                $LsqlV .= $separador."'".$_POST["G3007_C60012"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60013"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60013 = '".$_POST["G3007_C60013"]."'";
                $LsqlI .= $separador."G3007_C60013";
                $LsqlV .= $separador."'".$_POST["G3007_C60013"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60195"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60195 = '".$_POST["G3007_C60195"]."'";
                $LsqlI .= $separador."G3007_C60195";
                $LsqlV .= $separador."'".$_POST["G3007_C60195"]."'";
                $validar = 1;
            }
             
 
            $G3007_C60189 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3007_C60189"])){    
                if($_POST["G3007_C60189"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3007_C60189"]);
                    if(count($tieneHora) > 1){
                        $G3007_C60189 = "'".$_POST["G3007_C60189"]."'";
                    }else{
                        $G3007_C60189 = "'".str_replace(' ', '',$_POST["G3007_C60189"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3007_C60189 = ".$G3007_C60189;
                    $LsqlI .= $separador." G3007_C60189";
                    $LsqlV .= $separador.$G3007_C60189;
                    $validar = 1;
                }
            }
  
            $G3007_C60190 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3007_C60190"])){   
                if($_POST["G3007_C60190"] != '' && $_POST["G3007_C60190"] != 'undefined' && $_POST["G3007_C60190"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3007_C60190 = "'".$fecha." ".str_replace(' ', '',$_POST["G3007_C60190"])."'";
                    $LsqlU .= $separador." G3007_C60190 = ".$G3007_C60190."";
                    $LsqlI .= $separador." G3007_C60190";
                    $LsqlV .= $separador.$G3007_C60190;
                    $validar = 1;
                }
            }
 
            $G3007_C60191 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3007_C60191"])){    
                if($_POST["G3007_C60191"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3007_C60191"]);
                    if(count($tieneHora) > 1){
                        $G3007_C60191 = "'".$_POST["G3007_C60191"]."'";
                    }else{
                        $G3007_C60191 = "'".str_replace(' ', '',$_POST["G3007_C60191"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3007_C60191 = ".$G3007_C60191;
                    $LsqlI .= $separador." G3007_C60191";
                    $LsqlV .= $separador.$G3007_C60191;
                    $validar = 1;
                }
            }
  
            $G3007_C60192 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3007_C60192"])){   
                if($_POST["G3007_C60192"] != '' && $_POST["G3007_C60192"] != 'undefined' && $_POST["G3007_C60192"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3007_C60192 = "'".$fecha." ".str_replace(' ', '',$_POST["G3007_C60192"])."'";
                    $LsqlU .= $separador." G3007_C60192 = ".$G3007_C60192."";
                    $LsqlI .= $separador." G3007_C60192";
                    $LsqlV .= $separador.$G3007_C60192;
                    $validar = 1;
                }
            }
 
            $G3007_C60193 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3007_C60193"])){    
                if($_POST["G3007_C60193"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3007_C60193"]);
                    if(count($tieneHora) > 1){
                        $G3007_C60193 = "'".$_POST["G3007_C60193"]."'";
                    }else{
                        $G3007_C60193 = "'".str_replace(' ', '',$_POST["G3007_C60193"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3007_C60193 = ".$G3007_C60193;
                    $LsqlI .= $separador." G3007_C60193";
                    $LsqlV .= $separador.$G3007_C60193;
                    $validar = 1;
                }
            }
  
            $G3007_C60194 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3007_C60194"])){   
                if($_POST["G3007_C60194"] != '' && $_POST["G3007_C60194"] != 'undefined' && $_POST["G3007_C60194"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3007_C60194 = "'".$fecha." ".str_replace(' ', '',$_POST["G3007_C60194"])."'";
                    $LsqlU .= $separador." G3007_C60194 = ".$G3007_C60194."";
                    $LsqlI .= $separador." G3007_C60194";
                    $LsqlV .= $separador.$G3007_C60194;
                    $validar = 1;
                }
            }
 
            $G3007_C59993 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3007_C59993 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G3007_C59993 = ".$G3007_C59993;
                    $LsqlI .= $separador." G3007_C59993";
                    $LsqlV .= $separador.$G3007_C59993;
                    $validar = 1;

                    
                }
            }
 
            $G3007_C59994 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3007_C59994 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G3007_C59994 = ".$G3007_C59994;
                    $LsqlI .= $separador." G3007_C59994";
                    $LsqlV .= $separador.$G3007_C59994;
                    $validar = 1;
                }
            }
 
            $G3007_C59995 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3007_C59995 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G3007_C59995 = ".$G3007_C59995;
                    $LsqlI .= $separador." G3007_C59995";
                    $LsqlV .= $separador.$G3007_C59995;
                    $validar = 1;
                }
            }
 
            $G3007_C59996 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3007_C59996 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G3007_C59996 = ".$G3007_C59996;
                    $LsqlI .= $separador." G3007_C59996";
                    $LsqlV .= $separador.$G3007_C59996;
                    $validar = 1;
                }
            }
 
            $G3007_C59997 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3007_C59997 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G3007_C59997 = ".$G3007_C59997;
                    $LsqlI .= $separador." G3007_C59997";
                    $LsqlV .= $separador.$G3007_C59997;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3007_C59998"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C59998 = '".$_POST["G3007_C59998"]."'";
                $LsqlI .= $separador."G3007_C59998";
                $LsqlV .= $separador."'".$_POST["G3007_C59998"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C59999"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C59999 = '".$_POST["G3007_C59999"]."'";
                $LsqlI .= $separador."G3007_C59999";
                $LsqlV .= $separador."'".$_POST["G3007_C59999"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60000"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60000 = '".$_POST["G3007_C60000"]."'";
                $LsqlI .= $separador."G3007_C60000";
                $LsqlV .= $separador."'".$_POST["G3007_C60000"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60001"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60001 = '".$_POST["G3007_C60001"]."'";
                $LsqlI .= $separador."G3007_C60001";
                $LsqlV .= $separador."'".$_POST["G3007_C60001"]."'";
                $validar = 1;
            }

            if(isset($_POST["G3007_C64436"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C64436 = '".$_POST["G3007_C64436"]."'";
                $LsqlI .= $separador."G3007_C64436";
                $LsqlV .= $separador."'".$_POST["G3007_C64436"]."'";
                $validar = 1;
            }

            if(isset($_POST["G3007_C60002"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60002 = '".$_POST["G3007_C60002"]."'";
                $LsqlI .= $separador."G3007_C60002";
                $LsqlV .= $separador."'".$_POST["G3007_C60002"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60003"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60003 = '".$_POST["G3007_C60003"]."'";
                $LsqlI .= $separador."G3007_C60003";
                $LsqlV .= $separador."'".$_POST["G3007_C60003"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60223"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60223 = '".$_POST["G3007_C60223"]."'";
                $LsqlI .= $separador."G3007_C60223";
                $LsqlV .= $separador."'".$_POST["G3007_C60223"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60224"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60224 = '".$_POST["G3007_C60224"]."'";
                $LsqlI .= $separador."G3007_C60224";
                $LsqlV .= $separador."'".$_POST["G3007_C60224"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60225"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60225 = '".$_POST["G3007_C60225"]."'";
                $LsqlI .= $separador."G3007_C60225";
                $LsqlV .= $separador."'".$_POST["G3007_C60225"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3007_C60226"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_C60226 = '".$_POST["G3007_C60226"]."'";
                $LsqlI .= $separador."G3007_C60226";
                $LsqlV .= $separador."'".$_POST["G3007_C60226"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G3007_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G3007_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3007_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3007_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3007_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3007_Usuario , G3007_FechaInsercion, G3007_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3007_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3007 WHERE G3007_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["mis_casos"])){

        $strAgente_t = $_GET['strAgente_t'];

        $SQL = "SELECT G3007_ConsInte__b AS id, G3007_C60007 AS nombre, G.LISOPC_Nombre____b AS gerencia, G3007_C60008 AS telefono, G3007_C60009 AS correo, G3007_C60010 AS cedula, T.LISOPC_Nombre____b AS tipificacion, S.LISOPC_Nombre____b AS subtipificacion FROM ".$BaseDatos.".G3007 LEFT JOIN ".$BaseDatos_systema.".LISOPC G ON G3007_C60314 = G.LISOPC_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".LISOPC T ON G3007_C60011 = T.LISOPC_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".LISOPC S ON G3007_C60012 = S.LISOPC_ConsInte__b  WHERE dyalogo_general.fn_nombre_USUARI(G3007_Usuario) = '".$strAgente_t."' AND S.LISOPC_Nombre____b LIKE 'EN PROCESO%' ORDER BY G3007_ConsInte__b  DESC";



        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->id."'>"; 

                echo "<cell>". ($fila->id)."</cell>"; 
                echo "<cell>". ($fila->nombre)."</cell>"; 
                echo "<cell>". ($fila->gerencia)."</cell>"; 
                echo "<cell>". ($fila->telefono)."</cell>"; 
                echo "<cell>". ($fila->correo)."</cell>"; 
                echo "<cell>". ($fila->cedula)."</cell>"; 
                echo "<cell>". ($fila->tipificacion)."</cell>"; 
                echo "<cell>". ($fila->subtipificacion)."</cell>"; 

            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3007_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3007 WHERE G3007_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3018_ConsInte__b, G3018_C60202, G3018_C60203, G3018_C60204, G3018_C60205, G3018_C60206 FROM ".$BaseDatos.".G3018  ";

        $SQL .= " WHERE G3018_C60206 = '".$numero."'"; 

        $SQL .= " ORDER BY G3018_C60202";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3018_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3018_ConsInte__b)."</cell>"; 
            

                echo "<cell><![CDATA[". ($fila->G3018_C60202)."]]></cell>";

                echo "<cell>". ($fila->G3018_C60203)."</cell>";

                if($fila->G3018_C60204 != ''){
                    echo "<cell>". explode(' ', $fila->G3018_C60204)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3018_C60205 != ''){
                    echo "<cell>". explode(' ', $fila->G3018_C60205)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G3018_C60206)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3018 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3018(";
            $LsqlV = " VALUES ("; 
  

                if(isset($_POST["G3018_C60202"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3018_C60202 = '".$_POST["G3018_C60202"]."'";
                    $LsqlI .= $separador."G3018_C60202";
                    $LsqlV .= $separador."'".$_POST["G3018_C60202"]."'";
                    $validar = 1;
                }
                                                                               
 

                if(isset($_POST["G3018_C60203"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3018_C60203 = '".$_POST["G3018_C60203"]."'";
                    $LsqlI .= $separador."G3018_C60203";
                    $LsqlV .= $separador."'".$_POST["G3018_C60203"]."'";
                    $validar = 1;
                }

                                                                               

                $G3018_C60204 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3018_C60204"])){    
                    if($_POST["G3018_C60204"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3018_C60204 = "'".str_replace(' ', '',$_POST["G3018_C60204"])." 00:00:00'";
                        $LsqlU .= $separador." G3018_C60204 = ".$G3018_C60204;
                        $LsqlI .= $separador." G3018_C60204";
                        $LsqlV .= $separador.$G3018_C60204;
                        $validar = 1;
                    }
                }
 
                $G3018_C60205 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G3018_C60205"])){    
                    if($_POST["G3018_C60205"] != '' && $_POST["G3018_C60205"] != 'undefined' && $_POST["G3018_C60205"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3018_C60205 = "'".$fecha." ".str_replace(' ', '',$_POST["G3018_C60205"])."'";
                        $LsqlU .= $separador."  G3018_C60205 = ".$G3018_C60205."";
                        $LsqlI .= $separador."  G3018_C60205";
                        $LsqlV .= $separador.$G3018_C60205;
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3018_C60206 = $numero;
                    $LsqlU .= ", G3018_C60206 = ".$G3018_C60206."";
                    $LsqlI .= ", G3018_C60206";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3018_Usuario ,  G3018_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3018_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3018 WHERE  G3018_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
