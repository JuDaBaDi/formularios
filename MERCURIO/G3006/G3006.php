
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3006/G3006_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3006_ConsInte__b as id, G3006_C59985 as camp1 , G3006_C59989 as camp2 FROM ".$BaseDatos.".G3006  WHERE G3006_Usuario = ".$idUsuario." ORDER BY G3006_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3006_ConsInte__b as id, G3006_C59985 as camp1 , G3006_C59989 as camp2 FROM ".$BaseDatos.".G3006  ORDER BY G3006_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3006_ConsInte__b as id, G3006_C59985 as camp1 , G3006_C59989 as camp2 FROM ".$BaseDatos.".G3006 JOIN ".$BaseDatos.".G3006_M".$resultEstpas->muestr." ON G3006_ConsInte__b = G3006_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3006_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3006_ConsInte__b as id, G3006_C59985 as camp1 , G3006_C59989 as camp2 FROM ".$BaseDatos.".G3006 JOIN ".$BaseDatos.".G3006_M".$resultEstpas->muestr." ON G3006_ConsInte__b = G3006_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3006_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3006_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3006_ConsInte__b as id, G3006_C59985 as camp1 , G3006_C59989 as camp2 FROM ".$BaseDatos.".G3006  ORDER BY G3006_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    
    //JDBD - Validamos si se pudo obtener el dato principal.
    if ($nombre = $mysqli->query($XLsql)) {

        $nombreUsuario = NULL;
        //echo $XLsql;
        while ($key = $nombre->fetch_object()) {
            echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
            $nombreUsuario = $key->nombre;
            break;
        } 


        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                        
            $data = array(  "strToken_t" => $_GET['token'], 
                            "strIdGestion_t" => $_GET['id_gestion_cbx'],
                            "strDatoPrincipal_t" => $nombreUsuario,
                            "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
            $data_string = json_encode($data);    

            $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                      
            ); 
            //recogemos la respuesta
            $respuesta = curl_exec ($ch);
            //o el error, por si falla
            $error = curl_error($ch);
            //y finalmente cerramos curl
            //echo "Respuesta =>  ". $respuesta;
            //echo "<br/>Error => ".$error;
            //include "Log.class.php";
            //$log = new Log("log", "./Log/");
            //$log->insert($error, $respuesta, false, true, false);
            //echo "nada";
            curl_close ($ch);
        }

    }else{
        echo "<script>alert('NO SE PUDO OBTENER EL DATO PRINCIPAL DEL REGISTRO.');</script>";
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<input type="hidden" name="intConsInteBd" id="intConsInteBd" value="<?php if(isset($_GET["user"])) { echo $_GET["user"]; }else{ echo "-1";  } ?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="9177" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9177c">
                CONVERSACION
            </a>
        </h4>
        
    </div>
    <div id="s_9177c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días|tardes|noches, podría comunicarme con el señor(a) |NombreCliente|</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Mi nombre es |Agente|, le estoy llamando de |Empresa| con el fin de ...</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9174" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9174c">
                DATOS BASICOS DEL CASO 
            </a>
        </h4>
        
    </div>
    <div id="s_9174c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C59979" id="LblG3006_C59979">FECHA REGISTRO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3006_C59979'])) {
                            echo $_GET['G3006_C59979'];
                        } ?>"  name="G3006_C59979" id="G3006_C59979" placeholder="YYYY-MM-DD" nombre="FECHA REGISTRO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3006_C59980" id="LblG3006_C59980">HORA REGISTRO</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3006_C59980'])) {
                            echo $_GET['G3006_C59980'];
                        } ?>"  name="G3006_C59980" id="G3006_C59980" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3006_C59980">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3006_C59982" id="LblG3006_C59982">CIUDAD</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3006_C59982" id="G3006_C59982">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59981" id="LblG3006_C59981">DEPARTAMENTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59981" value="<?php if (isset($_GET['G3006_C59981'])) {
                            echo $_GET['G3006_C59981'];
                        } ?>" readonly name="G3006_C59981"  placeholder="DEPARTAMENTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59983" id="LblG3006_C59983">DIRECCIÓN </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59983" value="<?php if (isset($_GET['G3006_C59983'])) {
                            echo $_GET['G3006_C59983'];
                        } ?>"  name="G3006_C59983"  placeholder="DIRECCIÓN "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59984" id="LblG3006_C59984">BARRIO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59984" value="<?php if (isset($_GET['G3006_C59984'])) {
                            echo $_GET['G3006_C59984'];
                        } ?>"  name="G3006_C59984"  placeholder="BARRIO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59985" id="LblG3006_C59985">NOMBRE PROSPECTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59985" value="<?php if (isset($_GET['G3006_C59985'])) {
                            echo $_GET['G3006_C59985'];
                        } ?>"  name="G3006_C59985"  placeholder="NOMBRE PROSPECTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59986" id="LblG3006_C59986">APELLIDO PROSPECTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59986" value="<?php if (isset($_GET['G3006_C59986'])) {
                            echo $_GET['G3006_C59986'];
                        } ?>"  name="G3006_C59986"  placeholder="APELLIDO PROSPECTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59987" id="LblG3006_C59987">TELÉFONO PROSPECTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59987" value="<?php if (isset($_GET['G3006_C59987'])) {
                            echo $_GET['G3006_C59987'];
                        } ?>"  name="G3006_C59987"  placeholder="TELÉFONO PROSPECTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59988" id="LblG3006_C59988">CORREO ELECTRÓNICO</label>
                        <input type="email" maxlength="253" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id)" class="form-control input-sm" id="G3006_C59988" value="<?php if (isset($_GET['G3006_C59988'])) {
                            echo $_GET['G3006_C59988'];
                        } ?>"  name="G3006_C59988"  placeholder="CORREO ELECTRÓNICO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59989" id="LblG3006_C59989">CÉDULA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59989" value="<?php if (isset($_GET['G3006_C59989'])) {
                            echo $_GET['G3006_C59989'];
                        } ?>"  name="G3006_C59989"  placeholder="CÉDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3006_C59990" id="LblG3006_C59990">TIPIFICACIÓN</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3006_C59990" id="G3006_C59990">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3653 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3006_C59991" id="LblG3006_C59991">SUBTIPIFICACIÓN</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3006_C59991" id="G3006_C59991">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3654 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59992" id="LblG3006_C59992">CUAL</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59992" value="<?php if (isset($_GET['G3006_C59992'])) {
                            echo $_GET['G3006_C59992'];
                        } ?>"  name="G3006_C59992"  placeholder="CUAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60196" id="LblG3006_C60196">FECHA INICIO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G3006_C60196" id="G3006_C60196" placeholder="YYYY-MM-DD" nombre="FECHA INICIO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3006_C60197" id="LblG3006_C60197">HORA INICIO</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3006_C60197'])) {
                            echo $_GET['G3006_C60197'];
                        } ?>" readonly name="G3006_C60197" id="G3006_C60197" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3006_C60197">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60200" id="LblG3006_C60200">FECHA ÚLTIMA GESTIÓN</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3006_C60200'])) {
                            echo $_GET['G3006_C60200'];
                        } ?>" readonly name="G3006_C60200" id="G3006_C60200" placeholder="YYYY-MM-DD" nombre="FECHA ÚLTIMA GESTIÓN">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3006_C60201" id="LblG3006_C60201">HORA ÚLTIMA GESTIÓN</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3006_C60201'])) {
                            echo $_GET['G3006_C60201'];
                        } ?>" readonly name="G3006_C60201" id="G3006_C60201" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3006_C60201">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60198" id="LblG3006_C60198">FECHA CIERRE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3006_C60198'])) {
                            echo $_GET['G3006_C60198'];
                        } ?>" readonly name="G3006_C60198" id="G3006_C60198" placeholder="YYYY-MM-DD" nombre="FECHA CIERRE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3006_C60199" id="LblG3006_C60199">HORA CIERRE</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3006_C60199'])) {
                            echo $_GET['G3006_C60199'];
                        } ?>" readonly name="G3006_C60199" id="G3006_C60199" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3006_C60199">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3006_C60309" id="LblG3006_C60309">GERENCIA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3006_C60309" id="G3006_C60309">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3900 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9176" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9176c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9176c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59973" id="LblG3006_C59973">Agente</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59973" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G3006_C59973"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59974" id="LblG3006_C59974">Fecha</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59974" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G3006_C59974"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59975" id="LblG3006_C59975">Hora</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59975" value="<?php echo date('H:i:s');?>" readonly name="G3006_C59975"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C59976" id="LblG3006_C59976">Campaña</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C59976" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G3006_C59976"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9217" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9217c">
                ENVIO DE CORREOS
            </a>
        </h4>
        
    </div>
    <div id="s_9217c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3006_C60227" id="LblG3006_C60227">ENVIO DE CORREO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3006_C60227" id="G3006_C60227">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3692 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==45305){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C60228" id="LblG3006_C60228">PARA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C60228" value="<?php if (isset($_GET['G3006_C60228'])) {
                            echo $_GET['G3006_C60228'];
                        } ?>"  name="G3006_C60228"  placeholder="PARA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C60229" id="LblG3006_C60229">ASUNTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C60229" value="<?php if (isset($_GET['G3006_C60229'])) {
                            echo $_GET['G3006_C60229'];
                        } ?>"  name="G3006_C60229"  placeholder="ASUNTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3006_C60230" id="LblG3006_C60230">CUERPO DEL CORREO</label>
                        <textarea class="form-control input-sm" name="G3006_C60230" id="G3006_C60230"  value="<?php if (isset($_GET['G3006_C60230'])) {
                            echo $_GET['G3006_C60230'];
                        } ?>" placeholder="CUERPO DEL CORREO"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  id="9175" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="9212" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9212c">
                GESTIONES
            </a>
        </h4>
        
    </div>
    <div id="s_9212c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->

<div class=row>
                        <div class="col-md-12 col-xs-12">
                            
<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">
        <li>
            <a href="#tab_mis_casos" data-toggle="tab" id="mis_casos">MIS CASOS</a>
        </li>
        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">GESTIONES</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane" id="tab_mis_casos"> 
            <table class="table table-hover table-bordered" id="tabla_mis_casos" width="100%">
            </table>
            <div id="pager_mis_casos">
            </div> 
        </div>

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear GESTIONES" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
                        </div>
                    </div>
                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div hidden class="panel box box-primary" id="s_9271" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9271c">
                ERROR CRITICO DE USUARIO FINAL
            </a>
        </h4>
        
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a>
    </div>
    <div id="s_9271c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60860" id="LblG3006_C60860">Brinda información correcta/completa </label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60860" id="G3006_C60860" placeholder="Brinda información correcta/completa ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60861" id="LblG3006_C60861">Aporta alternativas de solución</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60861" id="G3006_C60861" placeholder="Aporta alternativas de solución">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60862" id="LblG3006_C60862">Lectura activa</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60862" id="G3006_C60862" placeholder="Lectura activa">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60863" id="LblG3006_C60863">Redacción y ortografía</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60863" id="G3006_C60863" placeholder="Redacción y ortografía">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60864" id="LblG3006_C60864">Respeto al cliente</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60864" id="G3006_C60864" placeholder="Respeto al cliente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60865" id="LblG3006_C60865">Correcta Expresión </label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60865" id="G3006_C60865" placeholder="Correcta Expresión ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60866" id="LblG3006_C60866">Total Usuario Final</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3006_C60866'])) {
                            echo $_GET['G3006_C60866'];
                        } ?>"  name="G3006_C60866" id="G3006_C60866" placeholder="Total Usuario Final">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3006_C60855" id="LblG3006_C60855">ESTADO_CALIDAD_Q_DY</label>
                        <select disabled class="form-control input-sm select2"  style="width: 100%;" name="G3006_C60855" id="G3006_C60855">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = -3 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==-203){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO DECIMAL -->
                    <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60854" id="LblG3006_C60854">CALIFICACION_Q_DY</label>
                        <input type="text" class="form-control input-sm Decimal" value="<?php if (isset($_GET['G3006_C60854'])) {
                            echo $_GET['G3006_C60854'];
                        } ?>"  name="G3006_C60854" id="G3006_C60854" placeholder="CALIFICACION_Q_DY">
                    </div>
                    <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3006_C60856" id="LblG3006_C60856">COMENTARIO_CALIDAD_Q_DY</label>
                        <textarea class="form-control input-sm" name="G3006_C60856" id="G3006_C60856"  value="<?php if (isset($_GET['G3006_C60856'])) {
                            echo $_GET['G3006_C60856'];
                        } ?>" placeholder="COMENTARIO_CALIDAD_Q_DY"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3006_C60857" id="LblG3006_C60857">COMENTARIO_AGENTE_Q_DY</label>
                        <textarea class="form-control input-sm" name="G3006_C60857" id="G3006_C60857" readonly value="<?php if (isset($_GET['G3006_C60857'])) {
                            echo $_GET['G3006_C60857'];
                        } ?>" placeholder="COMENTARIO_AGENTE_Q_DY"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60858" id="LblG3006_C60858">FECHA_AUDITADO_Q_DY</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3006_C60858'])) {
                            echo $_GET['G3006_C60858'];
                        } ?>" readonly name="G3006_C60858" id="G3006_C60858" placeholder="YYYY-MM-DD" nombre="FECHA_AUDITADO_Q_DY">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3006_C60859" id="LblG3006_C60859">NOMBRE_AUDITOR_Q_DY</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3006_C60859" value="<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>" readonly name="G3006_C60859"  placeholder="NOMBRE_AUDITOR_Q_DY"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_9271" style="width: 100%" controls>
                    <source id="btns_9271" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div hidden class="panel box box-primary" id="s_9272" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9272c">
                ERRORES CRÍTICOS  NEGOCIO
            </a>
        </h4>
        
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a>
    </div>
    <div id="s_9272c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60867" id="LblG3006_C60867">Buen manejo del plazo de atención</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60867" id="G3006_C60867" placeholder="Buen manejo del plazo de atención">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60868" id="LblG3006_C60868">Registro en Aplicativos Natura</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60868" id="G3006_C60868" placeholder="Registro en Aplicativos Natura">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60869" id="LblG3006_C60869">Registro del Temático</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60869" id="G3006_C60869" placeholder="Registro del Temático">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60870" id="LblG3006_C60870">Se identifica con Natura</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60870" id="G3006_C60870" placeholder="Se identifica con Natura">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60871" id="LblG3006_C60871">Confidencialidad</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60871" id="G3006_C60871" placeholder="Confidencialidad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60872" id="LblG3006_C60872">Total Negocio</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3006_C60872'])) {
                            echo $_GET['G3006_C60872'];
                        } ?>"  name="G3006_C60872" id="G3006_C60872" placeholder="Total Negocio">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_9272" style="width: 100%" controls>
                    <source id="btns_9272" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div hidden class="panel box box-primary" id="s_9273" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9273c">
                ERRORES NO CRÍTICOS
            </a>
        </h4>
        
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a>
    </div>
    <div id="s_9273c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60873" id="LblG3006_C60873">Saluda Correctamente </label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60873" id="G3006_C60873" placeholder="Saluda Correctamente ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60874" id="LblG3006_C60874">Usa frases de cortesía y amabilidad</label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60874" id="G3006_C60874" placeholder="Usa frases de cortesía y amabilidad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60875" id="LblG3006_C60875">Despide Correctamente </label>
                        <input type="text" class="form-control input-sm Numerico" value="0"  name="G3006_C60875" id="G3006_C60875" placeholder="Despide Correctamente ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60876" id="LblG3006_C60876">Total No Critico</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3006_C60876'])) {
                            echo $_GET['G3006_C60876'];
                        } ?>"  name="G3006_C60876" id="G3006_C60876" placeholder="Total No Critico">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_9273" style="width: 100%" controls>
                    <source id="btns_9273" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div hidden class="panel box box-primary" id="s_9274" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9274c">
                CALIFICACIÓN
            </a>
        </h4>
        
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a>
    </div>
    <div id="s_9274c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3006_C60877" id="LblG3006_C60877">CALIFICACIÓN TOTAL</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3006_C60877'])) {
                            echo $_GET['G3006_C60877'];
                        } ?>"  name="G3006_C60877" id="G3006_C60877" placeholder="CALIFICACIÓN TOTAL">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_9274" style="width: 100%" controls>
                    <source id="btns_9274" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G3006_C59968">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3658;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G3006_C59968">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3658;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G3006_C59969">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G3006_C59970" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G3006_C59971" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G3006_C59972" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G3006/G3006_eventos.js"></script>
<script type="text/javascript" src="formularios/G3006/G3006_extender_funcionalidad.php"></script><?php require_once "G3006_extender_funcionalidad.php";?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
                //JDBD abrimos el modal de correos.
                $(".FinalizarCalificacion").click(function(){
                    $("#calidad").val("1");
                    $("#enviarCalificacion").modal("show");
                });

                $("#sendEmails").click(function(){
                    $("#Save").click();
                    $("#loading").attr("hidden",false);
                    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
                    var cajaCorreos = $("#cajaCorreos").val();

                    if (cajaCorreos == null || cajaCorreos == "") {
                        cajaCorreos = "";
                    }else{
                        cajaCorreos = cajaCorreos.replace(/ /g, "");
                        cajaCorreos = cajaCorreos.replace(/,,,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,/g, ",");

                        if (cajaCorreos[0] == ",") {
                            cajaCorreos = cajaCorreos.substring(1);
                        }

                        if (cajaCorreos[cajaCorreos.length-1] == ",") {
                            cajaCorreos = cajaCorreos.substring(0,cajaCorreos.length-1);
                        }

                        var porciones = cajaCorreos.split(",");

                        for (var i = 0; i < porciones.length; i++) {
                            if (!emailRegex.test(porciones[i])) {
                                porciones.splice(i, 1);
                            }
                        }

                        cajaCorreos = porciones.join(",");
                    }

                    var formData = new FormData($("#FormularioDatos")[0]);
                    formData.append("IdGestion",$("#IdGestion").val());
                    formData.append("IdGuion",<?=$_GET["formulario"];?>);
                    formData.append("IdCal",<?=$_SESSION["IDENTIFICACION"];?>);
                    formData.append("Correos",cajaCorreos);

                    $.ajax({
                        url: "<?=$url_crud;?>?EnviarCalificacion=si",  
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data){
                            alertify.success("Calificacion enviada.");
                        },
                        error : function(){
                            alertify.error("No se pudo enviar la calificacion.");   
                        },
                        complete : function(){
                            $("#loading").attr("hidden",true);
                            $("#CerrarCalificacion").click();
                        }

                    }); 
                    
                });
                
                $("#s_9271").attr("hidden", false);
                $("#s_9272").attr("hidden", false);
                $("#s_9273").attr("hidden", false);
                $("#s_9274").attr("hidden", false);
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3006_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3006_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3006_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3006_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3006_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3006_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3006_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3006_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3006_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            $("#G3006_C59990").val("0").trigger("change");
            $("#G3006_C59991").val("0").trigger("change");
            //JDBD - Damos el valor fecha actual.
            $("#G3006_C60196").val("<?=date("Y-m-d");?>");
            $("#G3006_C60309").val("0").trigger("change");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C59975").val("<?php echo date('H:i:s');?>");
            $("#G3006_C60227").val("45305").trigger("change");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60860").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60861").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60862").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60863").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60864").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60865").val(0);
            $("#G3006_C60855").val("-203").trigger("change");
            //JDBD - Damos el valor nombre de usuario.
            $("#G3006_C60859").val("<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60867").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60868").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60869").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60870").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60871").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60873").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60874").val(0);
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3006_C60875").val(0);
            
            
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G3006_C59979").val(item.G3006_C59979); 
                $("#G3006_C59980").val(item.G3006_C59980); 
                $("#G3006_C59982").attr("opt",item.G3006_C59982);
                $("#G3006_C59982").val(item.G3006_C59982).trigger("change"); 
                $("#G3006_C59981").val(item.G3006_C59981); 
                $("#G3006_C59983").val(item.G3006_C59983); 
                $("#G3006_C59984").val(item.G3006_C59984); 
                $("#G3006_C59985").val(item.G3006_C59985); 
                $("#G3006_C59986").val(item.G3006_C59986); 
                $("#G3006_C59987").val(item.G3006_C59987); 
                $("#G3006_C59988").val(item.G3006_C59988); 
                $("#G3006_C59989").val(item.G3006_C59989); 
                $("#G3006_C59990").val(item.G3006_C59990).trigger("change");  
                $("#G3006_C59991").attr("opt",item.G3006_C59991);  
                $("#G3006_C59992").val(item.G3006_C59992); 
                $("#G3006_C60196").val(item.G3006_C60196); 
                $("#G3006_C60197").val(item.G3006_C60197); 
                $("#G3006_C60200").val(item.G3006_C60200); 
                $("#G3006_C60201").val(item.G3006_C60201); 
                $("#G3006_C60198").val(item.G3006_C60198); 
                $("#G3006_C60199").val(item.G3006_C60199); 
                $("#G3006_C60309").val(item.G3006_C60309).trigger("change");  
                $("#G3006_C59968").val(item.G3006_C59968).trigger("change");  
                $("#G3006_C59969").val(item.G3006_C59969).trigger("change");  
                $("#G3006_C59970").val(item.G3006_C59970); 
                $("#G3006_C59971").val(item.G3006_C59971); 
                $("#G3006_C59972").val(item.G3006_C59972); 
                $("#G3006_C59973").val(item.G3006_C59973); 
                $("#G3006_C59974").val(item.G3006_C59974); 
                $("#G3006_C59975").val(item.G3006_C59975); 
                $("#G3006_C59976").val(item.G3006_C59976);   
                if(item.G3006_C59977 == 1){
                    $("#G3006_C59977").attr('checked', true);
                }    
                if(item.G3006_C59978 == 1){
                    $("#G3006_C59978").attr('checked', true);
                }  
                $("#G3006_C60227").val(item.G3006_C60227).trigger("change");  
                $("#G3006_C60228").val(item.G3006_C60228); 
                $("#G3006_C60229").val(item.G3006_C60229); 
                $("#G3006_C60230").val(item.G3006_C60230); 
                $("#G3006_C60860").val(item.G3006_C60860); 
                $("#G3006_C60861").val(item.G3006_C60861); 
                $("#G3006_C60862").val(item.G3006_C60862); 
                $("#G3006_C60863").val(item.G3006_C60863); 
                $("#G3006_C60864").val(item.G3006_C60864); 
                $("#G3006_C60865").val(item.G3006_C60865); 
                $("#G3006_C60866").val(item.G3006_C60866); 
                $("#G3006_C60855").val(item.G3006_C60855).trigger("change");  
                $("#G3006_C60854").val(item.G3006_C60854); 
                $("#G3006_C60856").val(item.G3006_C60856); 
                $("#G3006_C60857").val(item.G3006_C60857); 
                $("#G3006_C60858").val(item.G3006_C60858); 
                $("#G3006_C60859").val(item.G3006_C60859); 
                $("#G3006_C60867").val(item.G3006_C60867); 
                $("#G3006_C60868").val(item.G3006_C60868); 
                $("#G3006_C60869").val(item.G3006_C60869); 
                $("#G3006_C60870").val(item.G3006_C60870); 
                $("#G3006_C60871").val(item.G3006_C60871); 
                $("#G3006_C60872").val(item.G3006_C60872); 
                $("#G3006_C60873").val(item.G3006_C60873); 
                $("#G3006_C60874").val(item.G3006_C60874); 
                $("#G3006_C60875").val(item.G3006_C60875); 
                $("#G3006_C60876").val(item.G3006_C60876); 
                $("#G3006_C60877").val(item.G3006_C60877);
                
                cargarHijos_0(
        $("#G3006_C59989").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G3006_C59989").val());
            var id_0 = $("#G3006_C59989").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G3006_C59989").val());
            var id_0 = $("#G3006_C59989").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#mis_casos").click(function(){ 
            $.jgrid.gridUnload('#tabla_mis_casos'); 
            var strAgente_t = '<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>';
            $.jgrid.gridUnload('#tabla_mis_casos'); //funcion Recargar 
            mis_casos(strAgente_t);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G3006_C59989").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3019&view=si&formaDetalle=si&formularioPadre=3006&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=60211<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    

            if(($("#G3006_C59983").val() == "") && $("#G3006_C59983").prop("disabled") == false){
                alertify.error('DIRECCIÓN  debe ser diligenciado');
                $("#G3006_C59983").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59984").val() == "") && $("#G3006_C59984").prop("disabled") == false){
                alertify.error('BARRIO debe ser diligenciado');
                $("#G3006_C59984").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59985").val() == "") && $("#G3006_C59985").prop("disabled") == false){
                alertify.error('NOMBRE PROSPECTO debe ser diligenciado');
                $("#G3006_C59985").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59986").val() == "") && $("#G3006_C59986").prop("disabled") == false){
                alertify.error('APELLIDO PROSPECTO debe ser diligenciado');
                $("#G3006_C59986").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59987").val() == "") && $("#G3006_C59987").prop("disabled") == false){
                alertify.error('TELÉFONO PROSPECTO debe ser diligenciado');
                $("#G3006_C59987").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59989").val() == "") && $("#G3006_C59989").prop("disabled") == false){
                alertify.error('CÉDULA debe ser diligenciado');
                $("#G3006_C59989").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59990").val()==0 || $("#G3006_C59990").val() == null || $("#G3006_C59990").val() == -1) && $("#G3006_C59990").prop("disabled") == false){
                alertify.error('TIPIFICACIÓN debe ser diligenciado');
                $("#G3006_C59990").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59991").val()==0 || $("#G3006_C59991").val() == null || $("#G3006_C59991").val() == -1) && $("#G3006_C59991").prop("disabled") == false){
                alertify.error('SUBTIPIFICACIÓN debe ser diligenciado');
                $("#G3006_C59991").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59992").val() == "") && $("#G3006_C59992").prop("disabled") == false){
                alertify.error('CUAL debe ser diligenciado');
                $("#G3006_C59992").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C60309").val()==0 || $("#G3006_C60309").val() == null || $("#G3006_C60309").val() == -1) && $("#G3006_C60309").prop("disabled") == false){
                alertify.error('GERENCIA debe ser diligenciado');
                $("#G3006_C60309").closest(".form-group").addClass("has-error");
                valido = 1;
            }
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3019&view=si&formaDetalle=si&formularioPadre=3006&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=60211&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3019&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=3006&pincheCampo=60211&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        

                            $("#G3006_C59982").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3006_C59982=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3006_C59982(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3006_C59982").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3006_C59982 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3006_C59982").html('<option value="'+data.G3038_ConsInte__b+'" >'+data.G3038_C60938+'</option>');
                                        
                                $("#G3006_C59981").val(data.G3038_C60939);
                                    }
                                });
                            });

    $("#G3006_C59990").select2();

    $("#G3006_C59991").select2();

    $("#G3006_C60309").select2();

    $("#G3006_C60227").select2();

    $("#G3006_C60855").select2();
        //datepickers
        

        $("#G3006_C59979").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G3006_C59970").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'HORA REGISTRO', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G3006_C59980").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G3006_C59971").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G3006_C60860").numeric();
                
        $("#G3006_C60861").numeric();
                
        $("#G3006_C60862").numeric();
                
        $("#G3006_C60863").numeric();
                
        $("#G3006_C60864").numeric();
                
        $("#G3006_C60865").numeric();
                
        $("#G3006_C60866").numeric();
                
        $("#G3006_C60867").numeric();
                
        $("#G3006_C60868").numeric();
                
        $("#G3006_C60869").numeric();
                
        $("#G3006_C60870").numeric();
                
        $("#G3006_C60871").numeric();
                
        $("#G3006_C60872").numeric();
                
        $("#G3006_C60873").numeric();
                
        $("#G3006_C60874").numeric();
                
        $("#G3006_C60875").numeric();
                
        $("#G3006_C60876").numeric();
                
        $("#G3006_C60877").numeric();
                

        //Validaciones numeros Decimales
        

        $("#G3006_C60854").numeric({ decimal : ".",  negative : false, scale: 4 });
                

        /* Si son d formulas */
        


        //function para Brinda información correcta/completa  

    $("#G3006_C60860").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60854").val())+Number($("#G3006_C60860").val())+Number($("#G3006_C60861").val())+Number($("#G3006_C60862").val())+Number($("#G3006_C60863").val())+Number($("#G3006_C60864").val())+Number($("#G3006_C60865").val()))/6;

        $("#G3006_C60866").val(intCalculo_t.toFixed(2));

    });

        //function para Aporta alternativas de solución 

    $("#G3006_C60861").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60854").val())+Number($("#G3006_C60860").val())+Number($("#G3006_C60861").val())+Number($("#G3006_C60862").val())+Number($("#G3006_C60863").val())+Number($("#G3006_C60864").val())+Number($("#G3006_C60865").val()))/6;

        $("#G3006_C60866").val(intCalculo_t.toFixed(2));

    });

        //function para Lectura activa 

    $("#G3006_C60862").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60854").val())+Number($("#G3006_C60860").val())+Number($("#G3006_C60861").val())+Number($("#G3006_C60862").val())+Number($("#G3006_C60863").val())+Number($("#G3006_C60864").val())+Number($("#G3006_C60865").val()))/6;

        $("#G3006_C60866").val(intCalculo_t.toFixed(2));

    });

        //function para Redacción y ortografía 

    $("#G3006_C60863").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60854").val())+Number($("#G3006_C60860").val())+Number($("#G3006_C60861").val())+Number($("#G3006_C60862").val())+Number($("#G3006_C60863").val())+Number($("#G3006_C60864").val())+Number($("#G3006_C60865").val()))/6;

        $("#G3006_C60866").val(intCalculo_t.toFixed(2));

    });

        //function para Respeto al cliente 

    $("#G3006_C60864").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60854").val())+Number($("#G3006_C60860").val())+Number($("#G3006_C60861").val())+Number($("#G3006_C60862").val())+Number($("#G3006_C60863").val())+Number($("#G3006_C60864").val())+Number($("#G3006_C60865").val()))/6;

        $("#G3006_C60866").val(intCalculo_t.toFixed(2));

    });

        //function para Total Usuario Final 

    $("#G3006_C60866").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = Number($("#G3006_C60866").val())+Number($("#G3006_C60872").val())+Number($("#G3006_C60876").val());

        $("#G3006_C60877").val(intCalculo_t.toFixed(2));

    });

        //function para CALIFICACION_Q_DY 

    $("#G3006_C60854").on('blur',function(e){

        //JDBD - 2
        var intCalculo_t = (Number($("#G3006_C60854").val())+Number($("#G3006_C60860").val())+Number($("#G3006_C60861").val())+Number($("#G3006_C60862").val())+Number($("#G3006_C60863").val())+Number($("#G3006_C60864").val())+$("#G3006_C60865").val())/6;

        $("#G3006_C60866").val(intCalculo_t.toFixed(2));

    });

        //function para Buen manejo del plazo de atención 

    $("#G3006_C60867").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60867").val())+Number($("#G3006_C60868").val())+Number($("#G3006_C60869").val())+Number($("#G3006_C60870").val())+Number($("#G3006_C60871").val()))/5;

        $("#G3006_C60872").val(intCalculo_t.toFixed(2));

    });

        //function para Registro en Aplicativos Natura 

    $("#G3006_C60868").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60867").val())+Number($("#G3006_C60868").val())+Number($("#G3006_C60869").val())+Number($("#G3006_C60870").val())+Number($("#G3006_C60871").val()))/5;

        $("#G3006_C60872").val(intCalculo_t.toFixed(2));

    });

        //function para Registro del Temático 

    $("#G3006_C60869").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60867").val())+Number($("#G3006_C60868").val())+Number($("#G3006_C60869").val())+Number($("#G3006_C60870").val())+Number($("#G3006_C60871").val()))/5;

        $("#G3006_C60872").val(intCalculo_t.toFixed(2));

    });

        //function para Se identifica con Natura 

    $("#G3006_C60870").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60867").val())+Number($("#G3006_C60868").val())+Number($("#G3006_C60869").val())+Number($("#G3006_C60870").val())+Number($("#G3006_C60871").val()))/5;

        $("#G3006_C60872").val(intCalculo_t.toFixed(2));

    });

        //function para Confidencialidad 

    $("#G3006_C60871").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60867").val())+Number($("#G3006_C60868").val())+Number($("#G3006_C60869").val())+Number($("#G3006_C60870").val())+Number($("#G3006_C60871").val()))/5;

        $("#G3006_C60872").val(intCalculo_t.toFixed(2));

    });

        //function para Total Negocio 

    $("#G3006_C60872").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = Number($("#G3006_C60866").val())+Number($("#G3006_C60872").val())+Number($("#G3006_C60876").val());

        $("#G3006_C60877").val(intCalculo_t.toFixed(2));

    });

        //function para Saluda Correctamente  

    $("#G3006_C60873").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60873").val())+Number($("#G3006_C60874").val())+Number($("#G3006_C60875").val()));

        $("#G3006_C60876").val(intCalculo_t.toFixed(2));

    });

        //function para Usa frases de cortesía y amabilidad 

    $("#G3006_C60874").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60873").val())+Number($("#G3006_C60874").val())+Number($("#G3006_C60875").val()));

        $("#G3006_C60876").val(intCalculo_t.toFixed(2));

    });

        //function para Despide Correctamente  

    $("#G3006_C60875").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = (Number($("#G3006_C60873").val())+Number($("#G3006_C60874").val())+Number($("#G3006_C60875").val()));

        $("#G3006_C60876").val(intCalculo_t.toFixed(2));

    });

        //function para Total No Critico 

    $("#G3006_C60876").on('blur',function(e){

        //JDBD - 1
        var intCalculo_t = Number($("#G3006_C60866").val())+Number($("#G3006_C60872").val())+Number($("#G3006_C60876").val());

        $("#G3006_C60877").val(intCalculo_t.toFixed(2));

    });

        //Si tienen dependencias

        


    //function para TIPIFICACIÓN 

    $("#G3006_C59990").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3654' , idPadre : $(this).val() },
            success : function(data){
                var optG3006_C59991 = $("#G3006_C59991").attr("opt");
                $("#G3006_C59991").html(data);
                if (optG3006_C59991 != null) {
                    $("#G3006_C59991").val(optG3006_C59991).trigger("change");
                }
            }
        });
        
    });

    //function para SUBTIPIFICACIÓN 

    $("#G3006_C59991").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para GERENCIA 

    $("#G3006_C60309").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ENVIO DE CORREO 

    $("#G3006_C60227").change(function(){ 
        $("#G3006_C60228").prop('disabled', false);
    
        $("#G3006_C60229").prop('disabled', false);
    
        $("#G3006_C60230").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '45305'){
            $("#G3006_C60228").closest(".form-group").removeClass("has-error"); 
            $("#G3006_C60228").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3006_C60228").val("")
            $("#G3006_C60229").closest(".form-group").removeClass("has-error"); 
            $("#G3006_C60229").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3006_C60229").val("")
            $("#G3006_C60230").closest(".form-group").removeClass("has-error"); 
            $("#G3006_C60230").prop('disabled', true); 
          //JDBD-20-05-17: Clareamos segun salto definido.
            $("#G3006_C60230").val("")
        }
     
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_CALIDAD_Q_DY 

    $("#G3006_C60855").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3006_C59979").val(item.G3006_C59979);
 
                                                $("#G3006_C59980").val(item.G3006_C59980);
 
                    $("#G3006_C59982").attr("opt",item.G3006_C59982);
                    $("#G3006_C59982").val(item.G3006_C59982).trigger("change");
 
                                                $("#G3006_C59981").val(item.G3006_C59981);
 
                                                $("#G3006_C59983").val(item.G3006_C59983);
 
                                                $("#G3006_C59984").val(item.G3006_C59984);
 
                                                $("#G3006_C59985").val(item.G3006_C59985);
 
                                                $("#G3006_C59986").val(item.G3006_C59986);
 
                                                $("#G3006_C59987").val(item.G3006_C59987);
 
                                                $("#G3006_C59988").val(item.G3006_C59988);
 
                                                $("#G3006_C59989").val(item.G3006_C59989);
 
                    $("#G3006_C59990").val(item.G3006_C59990).trigger("change"); 
 
                    $("#G3006_C59991").attr("opt",item.G3006_C59991); 
 
                                                $("#G3006_C59992").val(item.G3006_C59992);
 
                                                $("#G3006_C60196").val(item.G3006_C60196);
 
                                                $("#G3006_C60197").val(item.G3006_C60197);
 
                                                $("#G3006_C60200").val(item.G3006_C60200);
 
                                                $("#G3006_C60201").val(item.G3006_C60201);
 
                                                $("#G3006_C60198").val(item.G3006_C60198);
 
                                                $("#G3006_C60199").val(item.G3006_C60199);
 
                    $("#G3006_C60309").val(item.G3006_C60309).trigger("change"); 
 
                    $("#G3006_C59968").val(item.G3006_C59968).trigger("change"); 
 
                    $("#G3006_C59969").val(item.G3006_C59969).trigger("change"); 
 
                                                $("#G3006_C59970").val(item.G3006_C59970);
 
                                                $("#G3006_C59971").val(item.G3006_C59971);
 
                                                $("#G3006_C59972").val(item.G3006_C59972);
 
                                                $("#G3006_C59973").val(item.G3006_C59973);
 
                                                $("#G3006_C59974").val(item.G3006_C59974);
 
                                                $("#G3006_C59975").val(item.G3006_C59975);
 
                                                $("#G3006_C59976").val(item.G3006_C59976);
      
                                                if(item.G3006_C59977 == 1){
                                                   $("#G3006_C59977").attr('checked', true);
                                                } 
      
                                                if(item.G3006_C59978 == 1){
                                                   $("#G3006_C59978").attr('checked', true);
                                                } 
 
                    $("#G3006_C60227").val(item.G3006_C60227).trigger("change"); 
 
                                                $("#G3006_C60228").val(item.G3006_C60228);
 
                                                $("#G3006_C60229").val(item.G3006_C60229);
 
                                                $("#G3006_C60230").val(item.G3006_C60230);
 
                                                $("#G3006_C60860").val(item.G3006_C60860);
 
                                                $("#G3006_C60861").val(item.G3006_C60861);
 
                                                $("#G3006_C60862").val(item.G3006_C60862);
 
                                                $("#G3006_C60863").val(item.G3006_C60863);
 
                                                $("#G3006_C60864").val(item.G3006_C60864);
 
                                                $("#G3006_C60865").val(item.G3006_C60865);
 
                                                $("#G3006_C60866").val(item.G3006_C60866);
 
                    $("#G3006_C60855").val(item.G3006_C60855).trigger("change"); 
 
                                                $("#G3006_C60854").val(item.G3006_C60854);
 
                                                $("#G3006_C60856").val(item.G3006_C60856);
 
                                                $("#G3006_C60857").val(item.G3006_C60857);
 
                                                $("#G3006_C60858").val(item.G3006_C60858);
 
                                                $("#G3006_C60859").val(item.G3006_C60859);
 
                                                $("#G3006_C60867").val(item.G3006_C60867);
 
                                                $("#G3006_C60868").val(item.G3006_C60868);
 
                                                $("#G3006_C60869").val(item.G3006_C60869);
 
                                                $("#G3006_C60870").val(item.G3006_C60870);
 
                                                $("#G3006_C60871").val(item.G3006_C60871);
 
                                                $("#G3006_C60872").val(item.G3006_C60872);
 
                                                $("#G3006_C60873").val(item.G3006_C60873);
 
                                                $("#G3006_C60874").val(item.G3006_C60874);
 
                                                $("#G3006_C60875").val(item.G3006_C60875);
 
                                                $("#G3006_C60876").val(item.G3006_C60876);
 
                                                $("#G3006_C60877").val(item.G3006_C60877);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G3006_C59983").val() == "") && $("#G3006_C59983").prop("disabled") == false){
                alertify.error('DIRECCIÓN  debe ser diligenciado');
                $("#G3006_C59983").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59984").val() == "") && $("#G3006_C59984").prop("disabled") == false){
                alertify.error('BARRIO debe ser diligenciado');
                $("#G3006_C59984").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59985").val() == "") && $("#G3006_C59985").prop("disabled") == false){
                alertify.error('NOMBRE PROSPECTO debe ser diligenciado');
                $("#G3006_C59985").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59986").val() == "") && $("#G3006_C59986").prop("disabled") == false){
                alertify.error('APELLIDO PROSPECTO debe ser diligenciado');
                $("#G3006_C59986").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59987").val() == "") && $("#G3006_C59987").prop("disabled") == false){
                alertify.error('TELÉFONO PROSPECTO debe ser diligenciado');
                $("#G3006_C59987").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59989").val() == "") && $("#G3006_C59989").prop("disabled") == false){
                alertify.error('CÉDULA debe ser diligenciado');
                $("#G3006_C59989").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59990").val()==0 || $("#G3006_C59990").val() == null || $("#G3006_C59990").val() == -1) && $("#G3006_C59990").prop("disabled") == false){
                alertify.error('TIPIFICACIÓN debe ser diligenciado');
                $("#G3006_C59990").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59991").val()==0 || $("#G3006_C59991").val() == null || $("#G3006_C59991").val() == -1) && $("#G3006_C59991").prop("disabled") == false){
                alertify.error('SUBTIPIFICACIÓN debe ser diligenciado');
                $("#G3006_C59991").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C59992").val() == "") && $("#G3006_C59992").prop("disabled") == false){
                alertify.error('CUAL debe ser diligenciado');
                $("#G3006_C59992").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3006_C60309").val()==0 || $("#G3006_C60309").val() == null || $("#G3006_C60309").val() == -1) && $("#G3006_C60309").prop("disabled") == false){
                alertify.error('GERENCIA debe ser diligenciado');
                $("#G3006_C60309").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            /*let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });

            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }*/

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','FECHA REGISTRO','HORA REGISTRO','CIUDAD','DEPARTAMENTO','DIRECCIÓN ','BARRIO','NOMBRE PROSPECTO','APELLIDO PROSPECTO','TELÉFONO PROSPECTO','CORREO ELECTRÓNICO','CÉDULA','TIPIFICACIÓN','SUBTIPIFICACIÓN','CUAL','FECHA INICIO','HORA INICIO','FECHA ÚLTIMA GESTIÓN','HORA ÚLTIMA GESTIÓN','FECHA CIERRE','HORA CIERRE','GERENCIA','Agente','Fecha','Hora','Campaña','ENVIO DE CORREO','PARA','ASUNTO','CUERPO DEL CORREO','Brinda información correcta/completa ','Aporta alternativas de solución','Lectura activa','Redacción y ortografía','Respeto al cliente','Correcta Expresión ','Total Usuario Final','ESTADO_CALIDAD_Q_DY','CALIFICACION_Q_DY','COMENTARIO_CALIDAD_Q_DY','COMENTARIO_AGENTE_Q_DY','FECHA_AUDITADO_Q_DY','NOMBRE_AUDITOR_Q_DY','Buen manejo del plazo de atención','Registro en Aplicativos Natura','Registro del Temático','Se identifica con Natura','Confidencialidad','Total Negocio','Saluda Correctamente ','Usa frases de cortesía y amabilidad','Despide Correctamente ','Total No Critico','CALIFICACIÓN TOTAL'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {  
                        name:'G3006_C59979', 
                        index:'G3006_C59979', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3006_C59980', 
                        index:'G3006_C59980', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA REGISTRO', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3006_C59982', 
                        index:'G3006_C59982', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3006_C59982=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3006_C59981', 
                        index: 'G3006_C59981', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59983', 
                        index: 'G3006_C59983', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59984', 
                        index: 'G3006_C59984', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59985', 
                        index: 'G3006_C59985', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59986', 
                        index: 'G3006_C59986', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59987', 
                        index: 'G3006_C59987', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59989', 
                        index: 'G3006_C59989', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59990', 
                        index:'G3006_C59990', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3653&campo=G3006_C59990'
                        }
                    }

                    ,
                    { 
                        name:'G3006_C59991', 
                        index:'G3006_C59991', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3654&campo=G3006_C59991'
                        }
                    }

                    ,
                    { 
                        name:'G3006_C59992', 
                        index: 'G3006_C59992', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G3006_C60196', 
                        index:'G3006_C60196', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3006_C60197', 
                        index:'G3006_C60197', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA INICIO', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3006_C60200', 
                        index:'G3006_C60200', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3006_C60201', 
                        index:'G3006_C60201', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA ÚLTIMA GESTIÓN', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3006_C60198', 
                        index:'G3006_C60198', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3006_C60199', 
                        index:'G3006_C60199', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA CIERRE', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3006_C60309', 
                        index:'G3006_C60309', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3900&campo=G3006_C60309'
                        }
                    }

                    ,
                    { 
                        name:'G3006_C59973', 
                        index: 'G3006_C59973', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59974', 
                        index: 'G3006_C59974', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59975', 
                        index: 'G3006_C59975', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C59976', 
                        index: 'G3006_C59976', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C60227', 
                        index:'G3006_C60227', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3692&campo=G3006_C60227'
                        }
                    }

                    ,
                    { 
                        name:'G3006_C60228', 
                        index: 'G3006_C60228', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C60229', 
                        index: 'G3006_C60229', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C60230', 
                        index:'G3006_C60230', 
                        width:150, 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G3006_C60860', 
                        index:'G3006_C60860', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60861', 
                        index:'G3006_C60861', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60862', 
                        index:'G3006_C60862', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60863', 
                        index:'G3006_C60863', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60864', 
                        index:'G3006_C60864', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60865', 
                        index:'G3006_C60865', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60866', 
                        index:'G3006_C60866', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3006_C60855', 
                        index:'G3006_C60855', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=-3&campo=G3006_C60855'
                        }
                    }

                    ,
                    {  
                        name:'G3006_C60854', 
                        index:'G3006_C60854', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3006_C60856', 
                        index:'G3006_C60856', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3006_C60857', 
                        index:'G3006_C60857', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G3006_C60858', 
                        index:'G3006_C60858', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3006_C60859', 
                        index: 'G3006_C60859', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G3006_C60867', 
                        index:'G3006_C60867', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60868', 
                        index:'G3006_C60868', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60869', 
                        index:'G3006_C60869', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60870', 
                        index:'G3006_C60870', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60871', 
                        index:'G3006_C60871', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60872', 
                        index:'G3006_C60872', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60873', 
                        index:'G3006_C60873', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60874', 
                        index:'G3006_C60874', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60875', 
                        index:'G3006_C60875', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60876', 
                        index:'G3006_C60876', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G3006_C60877', 
                        index:'G3006_C60877', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3006_C59985',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
            ,subGrid: true,
            subGridRowExpanded: function(subgrid_id, row_id) { 
                // we pass two parameters 
                // subgrid_id is a id of the div tag created whitin a table data 
                // the id of this elemenet is a combination of the "sg_" + id of the row 
                // the row_id is the id of the row 
                // If we wan to pass additinal parameters to the url we can use 
                // a method getRowData(row_id) - which returns associative array in type name-value 
                // here we can easy construct the flowing 
                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Comentario de la gestión','Responsable gestión','Fecha gestión','Hora gestión','Cedula', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G3019_C60207', 
                                index:'G3019_C60207', 
                                width:150, 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G3019_C60208', 
                                index: 'G3019_C60208', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            {  
                                name:'G3019_C60209', 
                                index:'G3019_C60209', 
                                width:120 ,
                                editable: true ,
                                formatter: 'text', 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).datepicker({
                                            language: "es",
                                            autoclose: true,
                                            todayHighlight: true
                                        });
                                    },
                                    defaultValue: function(){
                                        var currentTime = new Date();
                                        var month = parseInt(currentTime.getMonth() + 1);
                                        month = month <= 9 ? "0"+month : month;
                                        var day = currentTime.getDate();
                                        day = day <= 9 ? "0"+day : day;
                                        var year = currentTime.getFullYear();
                                        return year+"-"+month + "-"+day;
                                    }
                                }
                            }

                            ,
                            {  
                                name:'G3019_C60210', 
                                index:'G3019_C60210', 
                                width:70 ,
                                editable: true ,
                                formatter: 'text', 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        //Timepicker
                                         var options = {  //hh:mm 24 hour format only, defaults to current time
                                            timeFormat: 'HH:mm:ss',
                                            interval: 5,
                                            minTime: '10',
                                            dynamic: false,
                                            dropdown: true,
                                            scrollbar: true
                                        }; 
                                        $(el).timepicker(options);


                                    }
                                }
                            }

                            ,
                            { 
                                name:'G3019_C60211', 
                                index: 'G3019_C60211', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                }, 
                subGridRowColapsed: function(subgrid_id, row_id) { 
                    // this function is called before removing the data 
                    //var subgrid_table_id; 
                    //subgrid_table_id = subgrid_id+"_t"; 
                    //jQuery("#"+subgrid_table_id).remove(); 
                }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G3006_C59979").val(item.G3006_C59979);

                        $("#G3006_C59980").val(item.G3006_C59980);
 
                    $("#G3006_C59982").attr("opt",item.G3006_C59982);
                    $("#G3006_C59982").val(item.G3006_C59982).trigger("change");

                        $("#G3006_C59981").val(item.G3006_C59981);

                        $("#G3006_C59983").val(item.G3006_C59983);

                        $("#G3006_C59984").val(item.G3006_C59984);

                        $("#G3006_C59985").val(item.G3006_C59985);

                        $("#G3006_C59986").val(item.G3006_C59986);

                        $("#G3006_C59987").val(item.G3006_C59987);

                        $("#G3006_C59988").val(item.G3006_C59988);

                        $("#G3006_C59989").val(item.G3006_C59989);
 
                    $("#G3006_C59990").val(item.G3006_C59990).trigger("change"); 
 
                    $("#G3006_C59991").attr("opt",item.G3006_C59991); 

                        $("#G3006_C59992").val(item.G3006_C59992);

                        $("#G3006_C60196").val(item.G3006_C60196);

                        $("#G3006_C60197").val(item.G3006_C60197);

                        $("#G3006_C60200").val(item.G3006_C60200);

                        $("#G3006_C60201").val(item.G3006_C60201);

                        $("#G3006_C60198").val(item.G3006_C60198);

                        $("#G3006_C60199").val(item.G3006_C60199);
 
                    $("#G3006_C60309").val(item.G3006_C60309).trigger("change"); 
 
                    $("#G3006_C59968").val(item.G3006_C59968).trigger("change"); 
 
                    $("#G3006_C59969").val(item.G3006_C59969).trigger("change"); 

                        $("#G3006_C59970").val(item.G3006_C59970);

                        $("#G3006_C59971").val(item.G3006_C59971);

                        $("#G3006_C59972").val(item.G3006_C59972);

                        $("#G3006_C59973").val(item.G3006_C59973);

                        $("#G3006_C59974").val(item.G3006_C59974);

                        $("#G3006_C59975").val(item.G3006_C59975);

                        $("#G3006_C59976").val(item.G3006_C59976);
    
                        if(item.G3006_C59977 == 1){
                           $("#G3006_C59977").attr('checked', true);
                        } 
    
                        if(item.G3006_C59978 == 1){
                           $("#G3006_C59978").attr('checked', true);
                        } 
 
                    $("#G3006_C60227").val(item.G3006_C60227).trigger("change"); 

                        $("#G3006_C60228").val(item.G3006_C60228);

                        $("#G3006_C60229").val(item.G3006_C60229);

                        $("#G3006_C60230").val(item.G3006_C60230);

                        $("#G3006_C60860").val(item.G3006_C60860);

                        $("#G3006_C60861").val(item.G3006_C60861);

                        $("#G3006_C60862").val(item.G3006_C60862);

                        $("#G3006_C60863").val(item.G3006_C60863);

                        $("#G3006_C60864").val(item.G3006_C60864);

                        $("#G3006_C60865").val(item.G3006_C60865);

                        $("#G3006_C60866").val(item.G3006_C60866);
 
                    $("#G3006_C60855").val(item.G3006_C60855).trigger("change"); 

                        $("#G3006_C60854").val(item.G3006_C60854);

                        $("#G3006_C60856").val(item.G3006_C60856);

                        $("#G3006_C60857").val(item.G3006_C60857);

                        $("#G3006_C60858").val(item.G3006_C60858);

                        $("#G3006_C60859").val(item.G3006_C60859);

                        $("#G3006_C60867").val(item.G3006_C60867);

                        $("#G3006_C60868").val(item.G3006_C60868);

                        $("#G3006_C60869").val(item.G3006_C60869);

                        $("#G3006_C60870").val(item.G3006_C60870);

                        $("#G3006_C60871").val(item.G3006_C60871);

                        $("#G3006_C60872").val(item.G3006_C60872);

                        $("#G3006_C60873").val(item.G3006_C60873);

                        $("#G3006_C60874").val(item.G3006_C60874);

                        $("#G3006_C60875").val(item.G3006_C60875);

                        $("#G3006_C60876").val(item.G3006_C60876);

                        $("#G3006_C60877").val(item.G3006_C60877);
                        
            cargarHijos_0(
        $("#G3006_C59989").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_9271");
                        $("#btns_9271").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_9272");
                        $("#btns_9272").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_9273");
                        $("#btns_9273").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_9274");
                        $("#btns_9274").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    function mis_casos(strAgente_p){

        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tabla_mis_casos").jqGrid({
            url:'<?=$url_crud;?>?mis_casos=si&strAgente_t='+strAgente_p,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','NOMBRE','GERENCIA','TELEFONO','CORREO','CEDULA','TIPIFICACION','SUBTIPIFICACION', 'padre'],
            colModel:[

                        {
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        },
                        {
                            name:'nombre', 
                            index:'nombre', 
                            width:150, 
                            editable: true 
                        },
                        {
                            name:'gerencia', 
                            index:'gerencia', 
                            width:150, 
                            editable: true 
                        },
                        {
                            name:'telefono', 
                            index:'telefono', 
                            width:150, 
                            editable: true 
                        },
                        {
                            name:'correo', 
                            index:'correo', 
                            width:150, 
                            editable: true 
                        },
                        {
                            name:'cedula', 
                            index:'cedula', 
                            width:150, 
                            editable: true 
                        },
                        {
                            name:'tipificacion', 
                            index:'tipificacion', 
                            width:150, 
                            editable: true 
                        },
                        {
                            name:'subtipificacion', 
                            index:'subtipificacion', 
                            width:150, 
                            editable: true 
                        },
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: true, 
                            editrules: {
                                edithidden:true
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id_0); 
                                } 
                            }
                        }
            ],
            rowNum: 500,
            pager: "#pager_mis_casos",
            rowList: [500,80],
            sortable: true,
            sortname: 'id',
            sortorder: 'desc',
            viewrecords: true,
            caption: 'MIS CASOS',
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,
            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3006&view=si&registroId='+ rowId);
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tabla_mis_casos").setGridWidth($(window).width());
        }).trigger('resize');

    }

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Comentario de la gestión','Responsable gestión','Fecha gestión','Hora gestión','Cedula', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {
                        name:'G3019_C60207', 
                        index:'G3019_C60207', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3019_C60208', 
                        index: 'G3019_C60208', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G3019_C60209', 
                        index:'G3019_C60209', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3019_C60210', 
                        index:'G3019_C60210', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = {  //hh:mm 24 hour format only, defaults to current time
                                    timeFormat: 'HH:mm:ss',
                                    interval: 5,
                                    minTime: '10',
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                }; 
                                $(el).timepicker(options);
                                $(".timepicker").css("z-index", 99999 );
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3019_C60211', 
                        index: 'G3019_C60211', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G3019_C60207',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'GESTIONES',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3019&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=60211&formularioPadre=3006<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G3006_C59989").val());
            var id_0 = $("#G3006_C59989").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G3006_C59989").val());
            var id_0 = $("#G3006_C59989").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
