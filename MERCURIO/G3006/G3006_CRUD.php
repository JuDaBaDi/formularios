<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 3006;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G3006
                  SET G3006_C60855 = -201
                  WHERE G3006_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G3006 
                    WHERE G3006_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G3006_C60854"]) || $gestion["G3006_C60854"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G3006_C60854"];
        }

        if (is_null($gestion["G3006_C60856"]) || $gestion["G3006_C60856"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G3006_C60856"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G3006_FechaInsercion"]."',".$gestion["G3006_Usuario"].",'".$gestion["G3006_C".$P["P"]]."','".$gestion["G3006_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "mercurio.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 3006 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G3006_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G3006_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G3006_LinkContenido as url FROM ".$BaseDatos.".G3006 WHERE G3006_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G3006_LinkContenido as url FROM ".$BaseDatos.".G3006 WHERE G3006_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G3006_LinkContenido as url FROM ".$BaseDatos.".G3006 WHERE G3006_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G3006_LinkContenido as url FROM ".$BaseDatos.".G3006 WHERE G3006_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3006_ConsInte__b, G3006_FechaInsercion , G3006_Usuario ,  G3006_CodigoMiembro  , G3006_PoblacionOrigen , G3006_EstadoDiligenciamiento ,  G3006_IdLlamada , G3006_C59985 as principal ,G3006_C59979,G3006_C59980,G3006_C59982,G3006_C59981,G3006_C59983,G3006_C59984,G3006_C59985,G3006_C59986,G3006_C59987,G3006_C59988,G3006_C59989,G3006_C59990,G3006_C59991,G3006_C59992,G3006_C60196,G3006_C60197,G3006_C60200,G3006_C60201,G3006_C60198,G3006_C60199,G3006_C60309,G3006_C59968,G3006_C59969,G3006_C59970,G3006_C59971,G3006_C59972,G3006_C59973,G3006_C59974,G3006_C59975,G3006_C59976,G3006_C60227,G3006_C60228,G3006_C60229,G3006_C60230,G3006_C60860,G3006_C60861,G3006_C60862,G3006_C60863,G3006_C60864,G3006_C60865,G3006_C60866,G3006_C60855,G3006_C60854,G3006_C60856,G3006_C60857,G3006_C60858,G3006_C60859,G3006_C60867,G3006_C60868,G3006_C60869,G3006_C60870,G3006_C60871,G3006_C60872,G3006_C60873,G3006_C60874,G3006_C60875,G3006_C60876,G3006_C60877 FROM '.$BaseDatos.'.G3006 WHERE G3006_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3006_C59979'] = explode(' ', $key->G3006_C59979)[0];
  
                $hora = '';
                if(!is_null($key->G3006_C59980)){
                    $hora = explode(' ', $key->G3006_C59980)[1];
                }

                $datos[$i]['G3006_C59980'] = $hora;

                $datos[$i]['G3006_C59982'] = $key->G3006_C59982;

                $datos[$i]['G3006_C59981'] = $key->G3006_C59981;

                $datos[$i]['G3006_C59983'] = $key->G3006_C59983;

                $datos[$i]['G3006_C59984'] = $key->G3006_C59984;

                $datos[$i]['G3006_C59985'] = $key->G3006_C59985;

                $datos[$i]['G3006_C59986'] = $key->G3006_C59986;

                $datos[$i]['G3006_C59987'] = $key->G3006_C59987;

                $datos[$i]['G3006_C59988'] = $key->G3006_C59988;

                $datos[$i]['G3006_C59989'] = $key->G3006_C59989;

                $datos[$i]['G3006_C59990'] = $key->G3006_C59990;

                $datos[$i]['G3006_C59991'] = $key->G3006_C59991;

                $datos[$i]['G3006_C59992'] = $key->G3006_C59992;

                $datos[$i]['G3006_C60196'] = explode(' ', $key->G3006_C60196)[0];
  
                $hora = '';
                if(!is_null($key->G3006_C60197)){
                    $hora = explode(' ', $key->G3006_C60197)[1];
                }

                $datos[$i]['G3006_C60197'] = $hora;

                $datos[$i]['G3006_C60200'] = explode(' ', $key->G3006_C60200)[0];
  
                $hora = '';
                if(!is_null($key->G3006_C60201)){
                    $hora = explode(' ', $key->G3006_C60201)[1];
                }

                $datos[$i]['G3006_C60201'] = $hora;

                $datos[$i]['G3006_C60198'] = explode(' ', $key->G3006_C60198)[0];
  
                $hora = '';
                if(!is_null($key->G3006_C60199)){
                    $hora = explode(' ', $key->G3006_C60199)[1];
                }

                $datos[$i]['G3006_C60199'] = $hora;

                $datos[$i]['G3006_C60309'] = $key->G3006_C60309;

                $datos[$i]['G3006_C59968'] = $key->G3006_C59968;

                $datos[$i]['G3006_C59969'] = $key->G3006_C59969;

                $datos[$i]['G3006_C59970'] = explode(' ', $key->G3006_C59970)[0];
  
                $hora = '';
                if(!is_null($key->G3006_C59971)){
                    $hora = explode(' ', $key->G3006_C59971)[1];
                }

                $datos[$i]['G3006_C59971'] = $hora;

                $datos[$i]['G3006_C59972'] = $key->G3006_C59972;

                $datos[$i]['G3006_C59973'] = $key->G3006_C59973;

                $datos[$i]['G3006_C59974'] = $key->G3006_C59974;

                $datos[$i]['G3006_C59975'] = $key->G3006_C59975;

                $datos[$i]['G3006_C59976'] = $key->G3006_C59976;

                $datos[$i]['G3006_C60227'] = $key->G3006_C60227;

                $datos[$i]['G3006_C60228'] = $key->G3006_C60228;

                $datos[$i]['G3006_C60229'] = $key->G3006_C60229;

                $datos[$i]['G3006_C60230'] = $key->G3006_C60230;

                $datos[$i]['G3006_C60860'] = $key->G3006_C60860;

                $datos[$i]['G3006_C60861'] = $key->G3006_C60861;

                $datos[$i]['G3006_C60862'] = $key->G3006_C60862;

                $datos[$i]['G3006_C60863'] = $key->G3006_C60863;

                $datos[$i]['G3006_C60864'] = $key->G3006_C60864;

                $datos[$i]['G3006_C60865'] = $key->G3006_C60865;

                $datos[$i]['G3006_C60866'] = $key->G3006_C60866;

                $datos[$i]['G3006_C60855'] = $key->G3006_C60855;

                $datos[$i]['G3006_C60854'] = $key->G3006_C60854;

                $datos[$i]['G3006_C60856'] = $key->G3006_C60856;

                $datos[$i]['G3006_C60857'] = $key->G3006_C60857;

                $datos[$i]['G3006_C60858'] = explode(' ', $key->G3006_C60858)[0];

                $datos[$i]['G3006_C60859'] = $key->G3006_C60859;

                $datos[$i]['G3006_C60867'] = $key->G3006_C60867;

                $datos[$i]['G3006_C60868'] = $key->G3006_C60868;

                $datos[$i]['G3006_C60869'] = $key->G3006_C60869;

                $datos[$i]['G3006_C60870'] = $key->G3006_C60870;

                $datos[$i]['G3006_C60871'] = $key->G3006_C60871;

                $datos[$i]['G3006_C60872'] = $key->G3006_C60872;

                $datos[$i]['G3006_C60873'] = $key->G3006_C60873;

                $datos[$i]['G3006_C60874'] = $key->G3006_C60874;

                $datos[$i]['G3006_C60875'] = $key->G3006_C60875;

                $datos[$i]['G3006_C60876'] = $key->G3006_C60876;

                $datos[$i]['G3006_C60877'] = $key->G3006_C60877;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3006";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3006_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3006_ConsInte__b as id,  G3006_C59985 as camp1 , G3006_C59989 as camp2 
                     FROM ".$BaseDatos.".G3006  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3006_ConsInte__b as id,  G3006_C59985 as camp1 , G3006_C59989 as camp2  
                    FROM ".$BaseDatos.".G3006  JOIN ".$BaseDatos.".G3006_M".$_POST['muestra']." ON G3006_ConsInte__b = G3006_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3006_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3006_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3006_C59985 LIKE '%".$B."%' OR G3006_C59989 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3006_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G3006_C59982'])){
                                $Ysql = "SELECT G3038_ConsInte__b as id, G3038_C60938 as text FROM ".$BaseDatos.".G3038 WHERE G3038_C60938 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3006_C59982"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3038 WHERE G3038_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3006_C59982"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3006");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3006_ConsInte__b, G3006_FechaInsercion , G3006_Usuario ,  G3006_CodigoMiembro  , G3006_PoblacionOrigen , G3006_EstadoDiligenciamiento ,  G3006_IdLlamada , G3006_C59985 as principal ,G3006_C59979,G3006_C59980, G3021_C60307,G3006_C59981,G3006_C59983,G3006_C59984,G3006_C59985,G3006_C59986,G3006_C59987,G3006_C59988,G3006_C59989, a.LISOPC_Nombre____b as G3006_C59990, b.LISOPC_Nombre____b as G3006_C59991,G3006_C59992,G3006_C60196,G3006_C60197,G3006_C60200,G3006_C60201,G3006_C60198,G3006_C60199, c.LISOPC_Nombre____b as G3006_C60309, d.LISOPC_Nombre____b as G3006_C59968, e.LISOPC_Nombre____b as G3006_C59969,G3006_C59970,G3006_C59971,G3006_C59972,G3006_C59973,G3006_C59974,G3006_C59975,G3006_C59976, f.LISOPC_Nombre____b as G3006_C60227,G3006_C60228,G3006_C60229,G3006_C60230,G3006_C60860,G3006_C60861,G3006_C60862,G3006_C60863,G3006_C60864,G3006_C60865,G3006_C60866, g.LISOPC_Nombre____b as G3006_C60855,G3006_C60854,G3006_C60856,G3006_C60857,G3006_C60858,G3006_C60859,G3006_C60867,G3006_C60868,G3006_C60869,G3006_C60870,G3006_C60871,G3006_C60872,G3006_C60873,G3006_C60874,G3006_C60875,G3006_C60876,G3006_C60877 FROM '.$BaseDatos.'.G3006 LEFT JOIN '.$BaseDatos.'.G3021 ON G3021_ConsInte__b  =  G3006_C59982 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3006_C59990 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3006_C59991 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3006_C60309 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3006_C59968 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3006_C59969 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3006_C60227 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G3006_C60855';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3006_C59971)){
                    $hora_a = explode(' ', $fila->G3006_C59971)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3006_C59980)){
                    $hora_b = explode(' ', $fila->G3006_C59980)[1];
                }

                $hora_c = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3006_C60197)){
                    $hora_c = explode(' ', $fila->G3006_C60197)[1];
                }

                $hora_d = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3006_C60201)){
                    $hora_d = explode(' ', $fila->G3006_C60201)[1];
                }

                $hora_e = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3006_C60199)){
                    $hora_e = explode(' ', $fila->G3006_C60199)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3006_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3006_ConsInte__b , explode(' ', $fila->G3006_C59979)[0] , $hora_a , ($fila->G3021_C60307) , ($fila->G3006_C59981) , ($fila->G3006_C59983) , ($fila->G3006_C59984) , ($fila->G3006_C59985) , ($fila->G3006_C59986) , ($fila->G3006_C59987) , ($fila->G3006_C59988) , ($fila->G3006_C59989) , ($fila->G3006_C59990) , ($fila->G3006_C59991) , ($fila->G3006_C59992) , explode(' ', $fila->G3006_C60196)[0] , $hora_b , explode(' ', $fila->G3006_C60200)[0] , $hora_c , explode(' ', $fila->G3006_C60198)[0] , $hora_d , ($fila->G3006_C60309) , ($fila->G3006_C59968) , ($fila->G3006_C59969) , explode(' ', $fila->G3006_C59970)[0] , $hora_e , ($fila->G3006_C59972) , ($fila->G3006_C59973) , ($fila->G3006_C59974) , ($fila->G3006_C59975) , ($fila->G3006_C59976) , ($fila->G3006_C60227) , ($fila->G3006_C60228) , ($fila->G3006_C60229) , ($fila->G3006_C60230) , ($fila->G3006_C60860) , ($fila->G3006_C60861) , ($fila->G3006_C60862) , ($fila->G3006_C60863) , ($fila->G3006_C60864) , ($fila->G3006_C60865) , ($fila->G3006_C60866) , ($fila->G3006_C60855) , ($fila->G3006_C60854) , ($fila->G3006_C60856) , ($fila->G3006_C60857) , explode(' ', $fila->G3006_C60858)[0] , ($fila->G3006_C60859) , ($fila->G3006_C60867) , ($fila->G3006_C60868) , ($fila->G3006_C60869) , ($fila->G3006_C60870) , ($fila->G3006_C60871) , ($fila->G3006_C60872) , ($fila->G3006_C60873) , ($fila->G3006_C60874) , ($fila->G3006_C60875) , ($fila->G3006_C60876) , ($fila->G3006_C60877) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3006 WHERE G3006_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3006";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3006_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3006_ConsInte__b as id,  G3006_C59985 as camp1 , G3006_C59989 as camp2  FROM '.$BaseDatos.'.G3006 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3006_ConsInte__b as id,  G3006_C59985 as camp1 , G3006_C59989 as camp2  
                    FROM ".$BaseDatos.".G3006  JOIN ".$BaseDatos.".G3006_M".$_POST['muestra']." ON G3006_ConsInte__b = G3006_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3006_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3006_C59985 LIKE "%'.$B.'%" OR G3006_C59989 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3006_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3006 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3006(";
            $LsqlV = " VALUES ("; 
 
            $G3006_C59979 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3006_C59979"])){    
                if($_POST["G3006_C59979"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3006_C59979"]);
                    if(count($tieneHora) > 1){
                        $G3006_C59979 = "'".$_POST["G3006_C59979"]."'";
                    }else{
                        $G3006_C59979 = "'".str_replace(' ', '',$_POST["G3006_C59979"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3006_C59979 = ".$G3006_C59979;
                    $LsqlI .= $separador." G3006_C59979";
                    $LsqlV .= $separador.$G3006_C59979;
                    $validar = 1;
                }
            }
  
            $G3006_C59980 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3006_C59980"])){   
                if($_POST["G3006_C59980"] != '' && $_POST["G3006_C59980"] != 'undefined' && $_POST["G3006_C59980"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C59980 = "'".$fecha." ".str_replace(' ', '',$_POST["G3006_C59980"])."'";
                    $LsqlU .= $separador." G3006_C59980 = ".$G3006_C59980."";
                    $LsqlI .= $separador." G3006_C59980";
                    $LsqlV .= $separador.$G3006_C59980;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3006_C59982"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59982 = '".$_POST["G3006_C59982"]."'";
                $LsqlI .= $separador."G3006_C59982";
                $LsqlV .= $separador."'".$_POST["G3006_C59982"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59981"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59981 = '".$_POST["G3006_C59981"]."'";
                $LsqlI .= $separador."G3006_C59981";
                $LsqlV .= $separador."'".$_POST["G3006_C59981"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59983"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59983 = '".$_POST["G3006_C59983"]."'";
                $LsqlI .= $separador."G3006_C59983";
                $LsqlV .= $separador."'".$_POST["G3006_C59983"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59984"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59984 = '".$_POST["G3006_C59984"]."'";
                $LsqlI .= $separador."G3006_C59984";
                $LsqlV .= $separador."'".$_POST["G3006_C59984"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59985"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59985 = '".$_POST["G3006_C59985"]."'";
                $LsqlI .= $separador."G3006_C59985";
                $LsqlV .= $separador."'".$_POST["G3006_C59985"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59986"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59986 = '".$_POST["G3006_C59986"]."'";
                $LsqlI .= $separador."G3006_C59986";
                $LsqlV .= $separador."'".$_POST["G3006_C59986"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59987"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59987 = '".$_POST["G3006_C59987"]."'";
                $LsqlI .= $separador."G3006_C59987";
                $LsqlV .= $separador."'".$_POST["G3006_C59987"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59988"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59988 = '".$_POST["G3006_C59988"]."'";
                $LsqlI .= $separador."G3006_C59988";
                $LsqlV .= $separador."'".$_POST["G3006_C59988"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59989"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59989 = '".$_POST["G3006_C59989"]."'";
                $LsqlI .= $separador."G3006_C59989";
                $LsqlV .= $separador."'".$_POST["G3006_C59989"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59990"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59990 = '".$_POST["G3006_C59990"]."'";
                $LsqlI .= $separador."G3006_C59990";
                $LsqlV .= $separador."'".$_POST["G3006_C59990"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59991"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59991 = '".$_POST["G3006_C59991"]."'";
                $LsqlI .= $separador."G3006_C59991";
                $LsqlV .= $separador."'".$_POST["G3006_C59991"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59992"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59992 = '".$_POST["G3006_C59992"]."'";
                $LsqlI .= $separador."G3006_C59992";
                $LsqlV .= $separador."'".$_POST["G3006_C59992"]."'";
                $validar = 1;
            }
             
 
            $G3006_C60196 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3006_C60196"])){    
                if($_POST["G3006_C60196"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3006_C60196"]);
                    if(count($tieneHora) > 1){
                        $G3006_C60196 = "'".$_POST["G3006_C60196"]."'";
                    }else{
                        $G3006_C60196 = "'".str_replace(' ', '',$_POST["G3006_C60196"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3006_C60196 = ".$G3006_C60196;
                    $LsqlI .= $separador." G3006_C60196";
                    $LsqlV .= $separador.$G3006_C60196;
                    $validar = 1;
                }
            }
  
            $G3006_C60197 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3006_C60197"])){   
                if($_POST["G3006_C60197"] != '' && $_POST["G3006_C60197"] != 'undefined' && $_POST["G3006_C60197"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60197 = "'".$fecha." ".str_replace(' ', '',$_POST["G3006_C60197"])."'";
                    $LsqlU .= $separador." G3006_C60197 = ".$G3006_C60197."";
                    $LsqlI .= $separador." G3006_C60197";
                    $LsqlV .= $separador.$G3006_C60197;
                    $validar = 1;
                }
            }
 
            $G3006_C60200 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3006_C60200"])){    
                if($_POST["G3006_C60200"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3006_C60200"]);
                    if(count($tieneHora) > 1){
                        $G3006_C60200 = "'".$_POST["G3006_C60200"]."'";
                    }else{
                        $G3006_C60200 = "'".str_replace(' ', '',$_POST["G3006_C60200"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3006_C60200 = ".$G3006_C60200;
                    $LsqlI .= $separador." G3006_C60200";
                    $LsqlV .= $separador.$G3006_C60200;
                    $validar = 1;
                }
            }
  
            $G3006_C60201 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3006_C60201"])){   
                if($_POST["G3006_C60201"] != '' && $_POST["G3006_C60201"] != 'undefined' && $_POST["G3006_C60201"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60201 = "'".$fecha." ".str_replace(' ', '',$_POST["G3006_C60201"])."'";
                    $LsqlU .= $separador." G3006_C60201 = ".$G3006_C60201."";
                    $LsqlI .= $separador." G3006_C60201";
                    $LsqlV .= $separador.$G3006_C60201;
                    $validar = 1;
                }
            }
 
            $G3006_C60198 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3006_C60198"])){    
                if($_POST["G3006_C60198"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3006_C60198"]);
                    if(count($tieneHora) > 1){
                        $G3006_C60198 = "'".$_POST["G3006_C60198"]."'";
                    }else{
                        $G3006_C60198 = "'".str_replace(' ', '',$_POST["G3006_C60198"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3006_C60198 = ".$G3006_C60198;
                    $LsqlI .= $separador." G3006_C60198";
                    $LsqlV .= $separador.$G3006_C60198;
                    $validar = 1;
                }
            }
  
            $G3006_C60199 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3006_C60199"])){   
                if($_POST["G3006_C60199"] != '' && $_POST["G3006_C60199"] != 'undefined' && $_POST["G3006_C60199"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60199 = "'".$fecha." ".str_replace(' ', '',$_POST["G3006_C60199"])."'";
                    $LsqlU .= $separador." G3006_C60199 = ".$G3006_C60199."";
                    $LsqlI .= $separador." G3006_C60199";
                    $LsqlV .= $separador.$G3006_C60199;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3006_C60309"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60309 = '".$_POST["G3006_C60309"]."'";
                $LsqlI .= $separador."G3006_C60309";
                $LsqlV .= $separador."'".$_POST["G3006_C60309"]."'";
                $validar = 1;
            }
             
 
            $G3006_C59968 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3006_C59968 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G3006_C59968 = ".$G3006_C59968;
                    $LsqlI .= $separador." G3006_C59968";
                    $LsqlV .= $separador.$G3006_C59968;
                    $validar = 1;

                    
                }
            }
 
            $G3006_C59969 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3006_C59969 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G3006_C59969 = ".$G3006_C59969;
                    $LsqlI .= $separador." G3006_C59969";
                    $LsqlV .= $separador.$G3006_C59969;
                    $validar = 1;
                }
            }
 
            $G3006_C59970 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3006_C59970 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G3006_C59970 = ".$G3006_C59970;
                    $LsqlI .= $separador." G3006_C59970";
                    $LsqlV .= $separador.$G3006_C59970;
                    $validar = 1;
                }
            }
 
            $G3006_C59971 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3006_C59971 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G3006_C59971 = ".$G3006_C59971;
                    $LsqlI .= $separador." G3006_C59971";
                    $LsqlV .= $separador.$G3006_C59971;
                    $validar = 1;
                }
            }
 
            $G3006_C59972 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3006_C59972 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G3006_C59972 = ".$G3006_C59972;
                    $LsqlI .= $separador." G3006_C59972";
                    $LsqlV .= $separador.$G3006_C59972;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3006_C59973"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59973 = '".$_POST["G3006_C59973"]."'";
                $LsqlI .= $separador."G3006_C59973";
                $LsqlV .= $separador."'".$_POST["G3006_C59973"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59974"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59974 = '".$_POST["G3006_C59974"]."'";
                $LsqlI .= $separador."G3006_C59974";
                $LsqlV .= $separador."'".$_POST["G3006_C59974"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59975"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59975 = '".$_POST["G3006_C59975"]."'";
                $LsqlI .= $separador."G3006_C59975";
                $LsqlV .= $separador."'".$_POST["G3006_C59975"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59976"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59976 = '".$_POST["G3006_C59976"]."'";
                $LsqlI .= $separador."G3006_C59976";
                $LsqlV .= $separador."'".$_POST["G3006_C59976"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59977"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59977 = '".$_POST["G3006_C59977"]."'";
                $LsqlI .= $separador."G3006_C59977";
                $LsqlV .= $separador."'".$_POST["G3006_C59977"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C59978"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C59978 = '".$_POST["G3006_C59978"]."'";
                $LsqlI .= $separador."G3006_C59978";
                $LsqlV .= $separador."'".$_POST["G3006_C59978"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C60227"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60227 = '".$_POST["G3006_C60227"]."'";
                $LsqlI .= $separador."G3006_C60227";
                $LsqlV .= $separador."'".$_POST["G3006_C60227"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C60228"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60228 = '".$_POST["G3006_C60228"]."'";
                $LsqlI .= $separador."G3006_C60228";
                $LsqlV .= $separador."'".$_POST["G3006_C60228"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C60229"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60229 = '".$_POST["G3006_C60229"]."'";
                $LsqlI .= $separador."G3006_C60229";
                $LsqlV .= $separador."'".$_POST["G3006_C60229"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C60230"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60230 = '".$_POST["G3006_C60230"]."'";
                $LsqlI .= $separador."G3006_C60230";
                $LsqlV .= $separador."'".$_POST["G3006_C60230"]."'";
                $validar = 1;
            }
             
  
            $G3006_C60860 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60860"])){
                if($_POST["G3006_C60860"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60860 = $_POST["G3006_C60860"];
                    $LsqlU .= $separador." G3006_C60860 = ".$G3006_C60860."";
                    $LsqlI .= $separador." G3006_C60860";
                    $LsqlV .= $separador.$G3006_C60860;
                    $validar = 1;
                }
            }
  
            $G3006_C60861 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60861"])){
                if($_POST["G3006_C60861"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60861 = $_POST["G3006_C60861"];
                    $LsqlU .= $separador." G3006_C60861 = ".$G3006_C60861."";
                    $LsqlI .= $separador." G3006_C60861";
                    $LsqlV .= $separador.$G3006_C60861;
                    $validar = 1;
                }
            }
  
            $G3006_C60862 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60862"])){
                if($_POST["G3006_C60862"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60862 = $_POST["G3006_C60862"];
                    $LsqlU .= $separador." G3006_C60862 = ".$G3006_C60862."";
                    $LsqlI .= $separador." G3006_C60862";
                    $LsqlV .= $separador.$G3006_C60862;
                    $validar = 1;
                }
            }
  
            $G3006_C60863 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60863"])){
                if($_POST["G3006_C60863"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60863 = $_POST["G3006_C60863"];
                    $LsqlU .= $separador." G3006_C60863 = ".$G3006_C60863."";
                    $LsqlI .= $separador." G3006_C60863";
                    $LsqlV .= $separador.$G3006_C60863;
                    $validar = 1;
                }
            }
  
            $G3006_C60864 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60864"])){
                if($_POST["G3006_C60864"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60864 = $_POST["G3006_C60864"];
                    $LsqlU .= $separador." G3006_C60864 = ".$G3006_C60864."";
                    $LsqlI .= $separador." G3006_C60864";
                    $LsqlV .= $separador.$G3006_C60864;
                    $validar = 1;
                }
            }
  
            $G3006_C60865 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60865"])){
                if($_POST["G3006_C60865"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60865 = $_POST["G3006_C60865"];
                    $LsqlU .= $separador." G3006_C60865 = ".$G3006_C60865."";
                    $LsqlI .= $separador." G3006_C60865";
                    $LsqlV .= $separador.$G3006_C60865;
                    $validar = 1;
                }
            }
  
            $G3006_C60866 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60866"])){
                if($_POST["G3006_C60866"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60866 = $_POST["G3006_C60866"];
                    $LsqlU .= $separador." G3006_C60866 = ".$G3006_C60866."";
                    $LsqlI .= $separador." G3006_C60866";
                    $LsqlV .= $separador.$G3006_C60866;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3006_C60855"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60855 = '".$_POST["G3006_C60855"]."'";
                $LsqlI .= $separador."G3006_C60855";
                $LsqlV .= $separador."'".$_POST["G3006_C60855"]."'";
                $validar = 1;
            }
             
  
            $G3006_C60854 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60854"])){
                if($_POST["G3006_C60854"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60854 = $_POST["G3006_C60854"];
                    $LsqlU .= $separador." G3006_C60854 = ".$G3006_C60854."";
                    $LsqlI .= $separador." G3006_C60854";
                    $LsqlV .= $separador.$G3006_C60854;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3006_C60856"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60856 = '".$_POST["G3006_C60856"]."'";
                $LsqlI .= $separador."G3006_C60856";
                $LsqlV .= $separador."'".$_POST["G3006_C60856"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3006_C60857"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60857 = '".$_POST["G3006_C60857"]."'";
                $LsqlI .= $separador."G3006_C60857";
                $LsqlV .= $separador."'".$_POST["G3006_C60857"]."'";
                $validar = 1;
            }
             
 
            $G3006_C60858 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3006_C60858"])){    
                if($_POST["G3006_C60858"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3006_C60858"]);
                    if(count($tieneHora) > 1){
                        $G3006_C60858 = "'".$_POST["G3006_C60858"]."'";
                    }else{
                        $G3006_C60858 = "'".str_replace(' ', '',$_POST["G3006_C60858"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3006_C60858 = ".$G3006_C60858;
                    $LsqlI .= $separador." G3006_C60858";
                    $LsqlV .= $separador.$G3006_C60858;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3006_C60859"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_C60859 = '".$_POST["G3006_C60859"]."'";
                $LsqlI .= $separador."G3006_C60859";
                $LsqlV .= $separador."'".$_POST["G3006_C60859"]."'";
                $validar = 1;
            }
             
  
            $G3006_C60867 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60867"])){
                if($_POST["G3006_C60867"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60867 = $_POST["G3006_C60867"];
                    $LsqlU .= $separador." G3006_C60867 = ".$G3006_C60867."";
                    $LsqlI .= $separador." G3006_C60867";
                    $LsqlV .= $separador.$G3006_C60867;
                    $validar = 1;
                }
            }
  
            $G3006_C60868 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60868"])){
                if($_POST["G3006_C60868"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60868 = $_POST["G3006_C60868"];
                    $LsqlU .= $separador." G3006_C60868 = ".$G3006_C60868."";
                    $LsqlI .= $separador." G3006_C60868";
                    $LsqlV .= $separador.$G3006_C60868;
                    $validar = 1;
                }
            }
  
            $G3006_C60869 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60869"])){
                if($_POST["G3006_C60869"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60869 = $_POST["G3006_C60869"];
                    $LsqlU .= $separador." G3006_C60869 = ".$G3006_C60869."";
                    $LsqlI .= $separador." G3006_C60869";
                    $LsqlV .= $separador.$G3006_C60869;
                    $validar = 1;
                }
            }
  
            $G3006_C60870 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60870"])){
                if($_POST["G3006_C60870"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60870 = $_POST["G3006_C60870"];
                    $LsqlU .= $separador." G3006_C60870 = ".$G3006_C60870."";
                    $LsqlI .= $separador." G3006_C60870";
                    $LsqlV .= $separador.$G3006_C60870;
                    $validar = 1;
                }
            }
  
            $G3006_C60871 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60871"])){
                if($_POST["G3006_C60871"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60871 = $_POST["G3006_C60871"];
                    $LsqlU .= $separador." G3006_C60871 = ".$G3006_C60871."";
                    $LsqlI .= $separador." G3006_C60871";
                    $LsqlV .= $separador.$G3006_C60871;
                    $validar = 1;
                }
            }
  
            $G3006_C60872 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60872"])){
                if($_POST["G3006_C60872"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60872 = $_POST["G3006_C60872"];
                    $LsqlU .= $separador." G3006_C60872 = ".$G3006_C60872."";
                    $LsqlI .= $separador." G3006_C60872";
                    $LsqlV .= $separador.$G3006_C60872;
                    $validar = 1;
                }
            }
  
            $G3006_C60873 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60873"])){
                if($_POST["G3006_C60873"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60873 = $_POST["G3006_C60873"];
                    $LsqlU .= $separador." G3006_C60873 = ".$G3006_C60873."";
                    $LsqlI .= $separador." G3006_C60873";
                    $LsqlV .= $separador.$G3006_C60873;
                    $validar = 1;
                }
            }
  
            $G3006_C60874 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60874"])){
                if($_POST["G3006_C60874"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60874 = $_POST["G3006_C60874"];
                    $LsqlU .= $separador." G3006_C60874 = ".$G3006_C60874."";
                    $LsqlI .= $separador." G3006_C60874";
                    $LsqlV .= $separador.$G3006_C60874;
                    $validar = 1;
                }
            }
  
            $G3006_C60875 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60875"])){
                if($_POST["G3006_C60875"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60875 = $_POST["G3006_C60875"];
                    $LsqlU .= $separador." G3006_C60875 = ".$G3006_C60875."";
                    $LsqlI .= $separador." G3006_C60875";
                    $LsqlV .= $separador.$G3006_C60875;
                    $validar = 1;
                }
            }
  
            $G3006_C60876 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60876"])){
                if($_POST["G3006_C60876"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60876 = $_POST["G3006_C60876"];
                    $LsqlU .= $separador." G3006_C60876 = ".$G3006_C60876."";
                    $LsqlI .= $separador." G3006_C60876";
                    $LsqlV .= $separador.$G3006_C60876;
                    $validar = 1;
                }
            }
  
            $G3006_C60877 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3006_C60877"])){
                if($_POST["G3006_C60877"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3006_C60877 = $_POST["G3006_C60877"];
                    $LsqlU .= $separador." G3006_C60877 = ".$G3006_C60877."";
                    $LsqlI .= $separador." G3006_C60877";
                    $LsqlV .= $separador.$G3006_C60877;
                    $validar = 1;
                }
            }

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G3006_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G3006_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3006_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3006_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3006_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3006_Usuario , G3006_FechaInsercion, G3006_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3006_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3006 WHERE G3006_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["mis_casos"])){

        $strAgente_t = $_GET['strAgente_t'];

        $SQL = "SELECT G3006_ConsInte__b AS id, G3006_C59985 AS nombre, G.LISOPC_Nombre____b AS gerencia, G3006_C59987 AS telefono, G3006_C59988 AS correo, G3006_C59989 AS cedula, T.LISOPC_Nombre____b AS tipificacion, S.LISOPC_Nombre____b AS subtipificacion FROM ".$BaseDatos.".G3006 LEFT JOIN ".$BaseDatos_systema.".LISOPC G ON G3006_C60309 = G.LISOPC_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".LISOPC T ON G3006_C59990 = T.LISOPC_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".LISOPC S ON G3006_C59991 = S.LISOPC_ConsInte__b  WHERE dyalogo_general.fn_nombre_USUARI(G3006_Usuario) = '".$strAgente_t."' AND  S.LISOPC_Nombre____b LIKE 'EN PROCESO%' ORDER BY G3006_ConsInte__b  DESC";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->id."'>"; 

                echo "<cell>". ($fila->id)."</cell>"; 
                echo "<cell>". ($fila->nombre)."</cell>"; 
                echo "<cell>". ($fila->gerencia)."</cell>"; 
                echo "<cell>". ($fila->telefono)."</cell>"; 
                echo "<cell>". ($fila->correo)."</cell>"; 
                echo "<cell>". ($fila->cedula)."</cell>"; 
                echo "<cell>". ($fila->tipificacion)."</cell>"; 
                echo "<cell>". ($fila->subtipificacion)."</cell>"; 

            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3006_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3006 WHERE G3006_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3019_ConsInte__b, G3019_C60207, G3019_C60208, G3019_C60209, G3019_C60210, G3019_C60211 FROM ".$BaseDatos.".G3019  ";

        $SQL .= " WHERE G3019_C60211 = '".$numero."'"; 

        $SQL .= " ORDER BY G3019_C60207";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3019_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3019_ConsInte__b)."</cell>"; 
            

                echo "<cell><![CDATA[". ($fila->G3019_C60207)."]]></cell>";

                echo "<cell>". ($fila->G3019_C60208)."</cell>";

                if($fila->G3019_C60209 != ''){
                    echo "<cell>". explode(' ', $fila->G3019_C60209)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3019_C60210 != ''){
                    echo "<cell>". explode(' ', $fila->G3019_C60210)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G3019_C60211)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3019 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3019(";
            $LsqlV = " VALUES ("; 
  

                if(isset($_POST["G3019_C60207"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3019_C60207 = '".$_POST["G3019_C60207"]."'";
                    $LsqlI .= $separador."G3019_C60207";
                    $LsqlV .= $separador."'".$_POST["G3019_C60207"]."'";
                    $validar = 1;
                }
                                                                               
 

                if(isset($_POST["G3019_C60208"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3019_C60208 = '".$_POST["G3019_C60208"]."'";
                    $LsqlI .= $separador."G3019_C60208";
                    $LsqlV .= $separador."'".$_POST["G3019_C60208"]."'";
                    $validar = 1;
                }

                                                                               

                $G3019_C60209 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3019_C60209"])){    
                    if($_POST["G3019_C60209"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3019_C60209 = "'".str_replace(' ', '',$_POST["G3019_C60209"])." 00:00:00'";
                        $LsqlU .= $separador." G3019_C60209 = ".$G3019_C60209;
                        $LsqlI .= $separador." G3019_C60209";
                        $LsqlV .= $separador.$G3019_C60209;
                        $validar = 1;
                    }
                }
 
                $G3019_C60210 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G3019_C60210"])){    
                    if($_POST["G3019_C60210"] != '' && $_POST["G3019_C60210"] != 'undefined' && $_POST["G3019_C60210"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3019_C60210 = "'".$fecha." ".str_replace(' ', '',$_POST["G3019_C60210"])."'";
                        $LsqlU .= $separador."  G3019_C60210 = ".$G3019_C60210."";
                        $LsqlI .= $separador."  G3019_C60210";
                        $LsqlV .= $separador.$G3019_C60210;
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3019_C60211 = $numero;
                    $LsqlU .= ", G3019_C60211 = ".$G3019_C60211."";
                    $LsqlI .= ", G3019_C60211";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3019_Usuario ,  G3019_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3019_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3019 WHERE  G3019_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
