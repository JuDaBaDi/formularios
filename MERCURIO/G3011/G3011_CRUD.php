<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 60082")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 60082");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3011_ConsInte__b, G3011_FechaInsercion , G3011_Usuario ,  G3011_CodigoMiembro  , G3011_PoblacionOrigen , G3011_EstadoDiligenciamiento ,  G3011_IdLlamada , G3011_C60089 as principal ,G3011_C60082,G3011_C60083,G3011_C60084,G3011_C60132,G3011_C60133,G3011_C60085,G3011_C60086,G3011_C60062,G3011_C60063,G3011_C60064,G3011_C60110,G3011_C60235,G3011_C60236,G3011_C60237,G3011_C60238,G3011_C60087,G3011_C60089,G3011_C60090,G3011_C60312,G3011_C60313,G3011_C60093,G3011_C60100,G3011_C60322,G3011_C60102,G3011_C60324,G3011_C60323,G3011_C60103,G3011_C60104,G3011_C60151,G3011_C60105,G3011_C60239,G3011_C60108 FROM '.$BaseDatos.'.G3011 WHERE G3011_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3011_C60082'] = $key->G3011_C60082;

                $datos[$i]['G3011_C60083'] = explode(' ', $key->G3011_C60083)[0];
  
                $hora = '';
                if(!is_null($key->G3011_C60084)){
                    $hora = explode(' ', $key->G3011_C60084)[1];
                }

                $datos[$i]['G3011_C60084'] = $hora;

                $datos[$i]['G3011_C60132'] = explode(' ', $key->G3011_C60132)[0];
  
                $hora = '';
                if(!is_null($key->G3011_C60133)){
                    $hora = explode(' ', $key->G3011_C60133)[1];
                }

                $datos[$i]['G3011_C60133'] = $hora;

                $datos[$i]['G3011_C60085'] = explode(' ', $key->G3011_C60085)[0];
  
                $hora = '';
                if(!is_null($key->G3011_C60086)){
                    $hora = explode(' ', $key->G3011_C60086)[1];
                }

                $datos[$i]['G3011_C60086'] = $hora;

                $datos[$i]['G3011_C60062'] = $key->G3011_C60062;

                $datos[$i]['G3011_C60063'] = $key->G3011_C60063;

                $datos[$i]['G3011_C60064'] = $key->G3011_C60064;

                $datos[$i]['G3011_C60110'] = $key->G3011_C60110;

                $datos[$i]['G3011_C60235'] = $key->G3011_C60235;

                $datos[$i]['G3011_C60236'] = $key->G3011_C60236;

                $datos[$i]['G3011_C60237'] = $key->G3011_C60237;

                $datos[$i]['G3011_C60238'] = $key->G3011_C60238;

                $datos[$i]['G3011_C60087'] = $key->G3011_C60087;

                $datos[$i]['G3011_C60089'] = $key->G3011_C60089;

                $datos[$i]['G3011_C60090'] = $key->G3011_C60090;

                $datos[$i]['G3011_C60312'] = $key->G3011_C60312;

                $datos[$i]['G3011_C60313'] = $key->G3011_C60313;

                $datos[$i]['G3011_C60093'] = $key->G3011_C60093;

                $datos[$i]['G3011_C60100'] = $key->G3011_C60100;

                $datos[$i]['G3011_C60322'] = $key->G3011_C60322;

                $datos[$i]['G3011_C60102'] = $key->G3011_C60102;

                $datos[$i]['G3011_C60324'] = $key->G3011_C60324;

                $datos[$i]['G3011_C60323'] = $key->G3011_C60323;

                $datos[$i]['G3011_C60103'] = $key->G3011_C60103;

                $datos[$i]['G3011_C60104'] = $key->G3011_C60104;

                $datos[$i]['G3011_C60151'] = $key->G3011_C60151;

                $datos[$i]['G3011_C60105'] = $key->G3011_C60105;

                $datos[$i]['G3011_C60239'] = $key->G3011_C60239;

                $datos[$i]['G3011_C60108'] = $key->G3011_C60108;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3011";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3011_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3011_ConsInte__b as id,  G3011_C60087 as camp2 , G3011_C60089 as camp1 
                     FROM ".$BaseDatos.".G3011  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3011_ConsInte__b as id,  G3011_C60087 as camp2 , G3011_C60089 as camp1  
                    FROM ".$BaseDatos.".G3011  JOIN ".$BaseDatos.".G3011_M".$_POST['muestra']." ON G3011_ConsInte__b = G3011_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3011_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3011_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3011_C60087 LIKE '%".$B."%' OR G3011_C60089 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3011_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G3011_C60110'])){
                                $Ysql = "SELECT G3010_ConsInte__b as id, G3010_C60057 as text FROM ".$BaseDatos.".G3010 WHERE G3010_C60057 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3011_C60110"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3010 WHERE G3010_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3011_C60110"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G3011_C60312'])){
                                $Ysql = "SELECT G3021_ConsInte__b as id, G3021_C60307 as text FROM ".$BaseDatos.".G3021 WHERE G3021_C60307 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3011_C60312"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3021 WHERE G3021_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3011_C60312"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3011");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3011_ConsInte__b, G3011_FechaInsercion , G3011_Usuario ,  G3011_CodigoMiembro  , G3011_PoblacionOrigen , G3011_EstadoDiligenciamiento ,  G3011_IdLlamada , G3011_C60089 as principal ,G3011_C60082,G3011_C60083,G3011_C60084,G3011_C60132,G3011_C60133,G3011_C60085,G3011_C60086,G3011_C60062,G3011_C60063, a.LISOPC_Nombre____b as G3011_C60064, G3010_C60057,G3011_C60235,G3011_C60236,G3011_C60237,G3011_C60238,G3011_C60087,G3011_C60089,G3011_C60090, G3021_C60307,G3011_C60313,G3011_C60093, b.LISOPC_Nombre____b as G3011_C60100,G3011_C60322, c.LISOPC_Nombre____b as G3011_C60102,G3011_C60324,G3011_C60323, d.LISOPC_Nombre____b as G3011_C60103, e.LISOPC_Nombre____b as G3011_C60104, f.LISOPC_Nombre____b as G3011_C60151,G3011_C60105,G3011_C60239, g.LISOPC_Nombre____b as G3011_C60108 FROM '.$BaseDatos.'.G3011 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3011_C60064 LEFT JOIN '.$BaseDatos.'.G3010 ON G3010_ConsInte__b  =  G3011_C60110 LEFT JOIN '.$BaseDatos.'.G3021 ON G3021_ConsInte__b  =  G3011_C60312 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3011_C60100 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3011_C60102 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3011_C60103 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3011_C60104 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3011_C60151 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G3011_C60108';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3011_C60084)){
                    $hora_a = explode(' ', $fila->G3011_C60084)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3011_C60133)){
                    $hora_b = explode(' ', $fila->G3011_C60133)[1];
                }

                $hora_c = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3011_C60086)){
                    $hora_c = explode(' ', $fila->G3011_C60086)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3011_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3011_ConsInte__b , ($fila->G3011_C60082) , explode(' ', $fila->G3011_C60083)[0] , $hora_a , explode(' ', $fila->G3011_C60132)[0] , $hora_b , explode(' ', $fila->G3011_C60085)[0] , $hora_c , ($fila->G3011_C60062) , ($fila->G3011_C60063) , ($fila->G3011_C60064) , ($fila->G3010_C60057) , ($fila->G3011_C60235) , ($fila->G3011_C60236) , ($fila->G3011_C60237) , ($fila->G3011_C60238) , ($fila->G3011_C60087) , ($fila->G3011_C60089) , ($fila->G3011_C60090) , ($fila->G3021_C60307) , ($fila->G3011_C60313) , ($fila->G3011_C60093) , ($fila->G3011_C60100) , ($fila->G3011_C60322) , ($fila->G3011_C60102) , ($fila->G3011_C60324) , ($fila->G3011_C60323) , ($fila->G3011_C60103) , ($fila->G3011_C60104) , ($fila->G3011_C60151) , ($fila->G3011_C60105) , ($fila->G3011_C60239) , ($fila->G3011_C60108) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3011 WHERE G3011_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3011";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3011_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3011_ConsInte__b as id,  G3011_C60087 as camp2 , G3011_C60089 as camp1  FROM '.$BaseDatos.'.G3011 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3011_ConsInte__b as id,  G3011_C60087 as camp2 , G3011_C60089 as camp1  
                    FROM ".$BaseDatos.".G3011  JOIN ".$BaseDatos.".G3011_M".$_POST['muestra']." ON G3011_ConsInte__b = G3011_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3011_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3011_C60087 LIKE "%'.$B.'%" OR G3011_C60089 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3011_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3011 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3011(";
            $LsqlV = " VALUES ("; 
  
            $G3011_C60082 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3011_C60082"])){
                if($_POST["G3011_C60082"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3011_C60082 = $_POST["G3011_C60082"];
                    $LsqlU .= $separador." G3011_C60082 = ".$G3011_C60082."";
                    $LsqlI .= $separador." G3011_C60082";
                    $LsqlV .= $separador.$G3011_C60082;
                    $validar = 1;
                }
            }
 
            $G3011_C60083 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3011_C60083"])){    
                if($_POST["G3011_C60083"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3011_C60083"]);
                    if(count($tieneHora) > 1){
                        $G3011_C60083 = "'".$_POST["G3011_C60083"]."'";
                    }else{
                        $G3011_C60083 = "'".str_replace(' ', '',$_POST["G3011_C60083"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3011_C60083 = ".$G3011_C60083;
                    $LsqlI .= $separador." G3011_C60083";
                    $LsqlV .= $separador.$G3011_C60083;
                    $validar = 1;
                }
            }
  
            $G3011_C60084 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3011_C60084"])){   
                if($_POST["G3011_C60084"] != '' && $_POST["G3011_C60084"] != 'undefined' && $_POST["G3011_C60084"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3011_C60084 = "'".$fecha." ".str_replace(' ', '',$_POST["G3011_C60084"])."'";
                    $LsqlU .= $separador." G3011_C60084 = ".$G3011_C60084."";
                    $LsqlI .= $separador." G3011_C60084";
                    $LsqlV .= $separador.$G3011_C60084;
                    $validar = 1;
                }
            }
 
            $G3011_C60132 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3011_C60132"])){    
                if($_POST["G3011_C60132"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3011_C60132"]);
                    if(count($tieneHora) > 1){
                        $G3011_C60132 = "'".$_POST["G3011_C60132"]."'";
                    }else{
                        $G3011_C60132 = "'".str_replace(' ', '',$_POST["G3011_C60132"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3011_C60132 = ".$G3011_C60132;
                    $LsqlI .= $separador." G3011_C60132";
                    $LsqlV .= $separador.$G3011_C60132;
                    $validar = 1;
                }
            }
  
            $G3011_C60133 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3011_C60133"])){   
                if($_POST["G3011_C60133"] != '' && $_POST["G3011_C60133"] != 'undefined' && $_POST["G3011_C60133"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3011_C60133 = "'".$fecha." ".str_replace(' ', '',$_POST["G3011_C60133"])."'";
                    $LsqlU .= $separador." G3011_C60133 = ".$G3011_C60133."";
                    $LsqlI .= $separador." G3011_C60133";
                    $LsqlV .= $separador.$G3011_C60133;
                    $validar = 1;
                }
            }
 
            $G3011_C60085 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3011_C60085"])){    
                if($_POST["G3011_C60085"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3011_C60085"]);
                    if(count($tieneHora) > 1){
                        $G3011_C60085 = "'".$_POST["G3011_C60085"]."'";
                    }else{
                        $G3011_C60085 = "'".str_replace(' ', '',$_POST["G3011_C60085"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3011_C60085 = ".$G3011_C60085;
                    $LsqlI .= $separador." G3011_C60085";
                    $LsqlV .= $separador.$G3011_C60085;
                    $validar = 1;
                }
            }
  
            $G3011_C60086 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G3011_C60086"])){   
                if($_POST["G3011_C60086"] != '' && $_POST["G3011_C60086"] != 'undefined' && $_POST["G3011_C60086"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3011_C60086 = "'".$fecha." ".str_replace(' ', '',$_POST["G3011_C60086"])."'";
                    $LsqlU .= $separador." G3011_C60086 = ".$G3011_C60086."";
                    $LsqlI .= $separador." G3011_C60086";
                    $LsqlV .= $separador.$G3011_C60086;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3011_C60062"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60062 = '".$_POST["G3011_C60062"]."'";
                $LsqlI .= $separador."G3011_C60062";
                $LsqlV .= $separador."'".$_POST["G3011_C60062"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60063"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60063 = '".$_POST["G3011_C60063"]."'";
                $LsqlI .= $separador."G3011_C60063";
                $LsqlV .= $separador."'".$_POST["G3011_C60063"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60064"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60064 = '".$_POST["G3011_C60064"]."'";
                $LsqlI .= $separador."G3011_C60064";
                $LsqlV .= $separador."'".$_POST["G3011_C60064"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60110"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60110 = '".$_POST["G3011_C60110"]."'";
                $LsqlI .= $separador."G3011_C60110";
                $LsqlV .= $separador."'".$_POST["G3011_C60110"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60235 = '".$_POST["G3011_C60235"]."'";
                $LsqlI .= $separador."G3011_C60235";
                $LsqlV .= $separador."'".$_POST["G3011_C60235"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60236"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60236 = '".$_POST["G3011_C60236"]."'";
                $LsqlI .= $separador."G3011_C60236";
                $LsqlV .= $separador."'".$_POST["G3011_C60236"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60237"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60237 = '".$_POST["G3011_C60237"]."'";
                $LsqlI .= $separador."G3011_C60237";
                $LsqlV .= $separador."'".$_POST["G3011_C60237"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60238"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60238 = '".$_POST["G3011_C60238"]."'";
                $LsqlI .= $separador."G3011_C60238";
                $LsqlV .= $separador."'".$_POST["G3011_C60238"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60087"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60087 = '".$_POST["G3011_C60087"]."'";
                $LsqlI .= $separador."G3011_C60087";
                $LsqlV .= $separador."'".$_POST["G3011_C60087"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60089"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60089 = '".$_POST["G3011_C60089"]."'";
                $LsqlI .= $separador."G3011_C60089";
                $LsqlV .= $separador."'".$_POST["G3011_C60089"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60090"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60090 = '".$_POST["G3011_C60090"]."'";
                $LsqlI .= $separador."G3011_C60090";
                $LsqlV .= $separador."'".$_POST["G3011_C60090"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60312"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60312 = '".$_POST["G3011_C60312"]."'";
                $LsqlI .= $separador."G3011_C60312";
                $LsqlV .= $separador."'".$_POST["G3011_C60312"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60313 = '".$_POST["G3011_C60313"]."'";
                $LsqlI .= $separador."G3011_C60313";
                $LsqlV .= $separador."'".$_POST["G3011_C60313"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60093"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60093 = '".$_POST["G3011_C60093"]."'";
                $LsqlI .= $separador."G3011_C60093";
                $LsqlV .= $separador."'".$_POST["G3011_C60093"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60100"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60100 = '".$_POST["G3011_C60100"]."'";
                $LsqlI .= $separador."G3011_C60100";
                $LsqlV .= $separador."'".$_POST["G3011_C60100"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60322 = '".$_POST["G3011_C60322"]."'";
                $LsqlI .= $separador."G3011_C60322";
                $LsqlV .= $separador."'".$_POST["G3011_C60322"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60102"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60102 = '".$_POST["G3011_C60102"]."'";
                $LsqlI .= $separador."G3011_C60102";
                $LsqlV .= $separador."'".$_POST["G3011_C60102"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60324"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60324 = '".$_POST["G3011_C60324"]."'";
                $LsqlI .= $separador."G3011_C60324";
                $LsqlV .= $separador."'".$_POST["G3011_C60324"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60323"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60323 = '".$_POST["G3011_C60323"]."'";
                $LsqlI .= $separador."G3011_C60323";
                $LsqlV .= $separador."'".$_POST["G3011_C60323"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60103"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60103 = '".$_POST["G3011_C60103"]."'";
                $LsqlI .= $separador."G3011_C60103";
                $LsqlV .= $separador."'".$_POST["G3011_C60103"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60104"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60104 = '".$_POST["G3011_C60104"]."'";
                $LsqlI .= $separador."G3011_C60104";
                $LsqlV .= $separador."'".$_POST["G3011_C60104"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60151"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60151 = '".$_POST["G3011_C60151"]."'";
                $LsqlI .= $separador."G3011_C60151";
                $LsqlV .= $separador."'".$_POST["G3011_C60151"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60105"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60105 = '".$_POST["G3011_C60105"]."'";
                $LsqlI .= $separador."G3011_C60105";
                $LsqlV .= $separador."'".$_POST["G3011_C60105"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60239"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60239 = '".$_POST["G3011_C60239"]."'";
                $LsqlI .= $separador."G3011_C60239";
                $LsqlV .= $separador."'".$_POST["G3011_C60239"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3011_C60108"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_C60108 = '".$_POST["G3011_C60108"]."'";
                $LsqlI .= $separador."G3011_C60108";
                $LsqlV .= $separador."'".$_POST["G3011_C60108"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3011_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3011_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3011_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3011_Usuario , G3011_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3011_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3011 WHERE G3011_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3011 SET G3011_UltiGest__b =-14, G3011_GesMasImp_b =-14, G3011_TipoReintentoUG_b =0, G3011_TipoReintentoGMI_b =0, G3011_ClasificacionUG_b =3, G3011_ClasificacionGMI_b =3, G3011_EstadoUG_b =-14, G3011_EstadoGMI_b =-14, G3011_CantidadIntentos =0, G3011_CantidadIntentosGMI_b =0 WHERE G3011_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3011_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3011 WHERE G3011_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3013_ConsInte__b, G3013_C60094, G3013_C60095, G3013_C60096, G3013_C60097, G3013_C60098 FROM ".$BaseDatos.".G3013  ";

        $SQL .= " WHERE G3013_C60098 = '".$numero."'"; 

        $SQL .= " ORDER BY G3013_C60094";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3013_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3013_ConsInte__b)."</cell>"; 
            

                echo "<cell><![CDATA[". ($fila->G3013_C60094)."]]></cell>";

                echo "<cell>". ($fila->G3013_C60095)."</cell>";

                if($fila->G3013_C60096 != ''){
                    echo "<cell>". explode(' ', $fila->G3013_C60096)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G3013_C60097 != ''){
                    echo "<cell>". explode(' ', $fila->G3013_C60097)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". $fila->G3013_C60098."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3013 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3013(";
            $LsqlV = " VALUES ("; 
  

                if(isset($_POST["G3013_C60094"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3013_C60094 = '".$_POST["G3013_C60094"]."'";
                    $LsqlI .= $separador."G3013_C60094";
                    $LsqlV .= $separador."'".$_POST["G3013_C60094"]."'";
                    $validar = 1;
                }
                                                                               
 

                if(isset($_POST["G3013_C60095"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3013_C60095 = '".$_POST["G3013_C60095"]."'";
                    $LsqlI .= $separador."G3013_C60095";
                    $LsqlV .= $separador."'".$_POST["G3013_C60095"]."'";
                    $validar = 1;
                }

                                                                               

                $G3013_C60096 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3013_C60096"])){    
                    if($_POST["G3013_C60096"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3013_C60096 = "'".str_replace(' ', '',$_POST["G3013_C60096"])." 00:00:00'";
                        $LsqlU .= $separador." G3013_C60096 = ".$G3013_C60096;
                        $LsqlI .= $separador." G3013_C60096";
                        $LsqlV .= $separador.$G3013_C60096;
                        $validar = 1;
                    }
                }
 
                $G3013_C60097 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G3013_C60097"])){    
                    if($_POST["G3013_C60097"] != '' && $_POST["G3013_C60097"] != 'undefined' && $_POST["G3013_C60097"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3013_C60097 = "'".$fecha." ".str_replace(' ', '',$_POST["G3013_C60097"])."'";
                        $LsqlU .= $separador."  G3013_C60097 = ".$G3013_C60097."";
                        $LsqlI .= $separador."  G3013_C60097";
                        $LsqlV .= $separador.$G3013_C60097;
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3013_C60098 = $numero;
                    $LsqlU .= ", G3013_C60098 = ".$G3013_C60098."";
                    $LsqlI .= ", G3013_C60098";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3013_Usuario ,  G3013_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3013_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3013 WHERE  G3013_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
