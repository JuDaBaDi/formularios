
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3011/G3011_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3011_ConsInte__b as id, G3011_C60087 as camp2 , G3011_C60089 as camp1 FROM ".$BaseDatos.".G3011  WHERE G3011_Usuario = ".$idUsuario." ORDER BY G3011_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3011_ConsInte__b as id, G3011_C60087 as camp2 , G3011_C60089 as camp1 FROM ".$BaseDatos.".G3011  ORDER BY G3011_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3011_ConsInte__b as id, G3011_C60087 as camp2 , G3011_C60089 as camp1 FROM ".$BaseDatos.".G3011 JOIN ".$BaseDatos.".G3011_M".$resultEstpas->muestr." ON G3011_ConsInte__b = G3011_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3011_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3011_ConsInte__b as id, G3011_C60087 as camp2 , G3011_C60089 as camp1 FROM ".$BaseDatos.".G3011 JOIN ".$BaseDatos.".G3011_M".$resultEstpas->muestr." ON G3011_ConsInte__b = G3011_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3011_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3011_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3011_ConsInte__b as id, G3011_C60087 as camp2 , G3011_C60089 as camp1 FROM ".$BaseDatos.".G3011  ORDER BY G3011_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="9190" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9190c">
                DATOS AUTOMATICOS DEL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9190c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3011_C60082" id="LblG3011_C60082">NÚMERO CASO</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"]) && isset($_GET["view"])){  if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 60082")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 60082");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }
                             }?>" readonly name="G3011_C60082" id="G3011_C60082" placeholder="NÚMERO CASO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3011_C60083" id="LblG3011_C60083">FECHA CREACIÓN</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>" readonly name="G3011_C60083" id="G3011_C60083" placeholder="YYYY-MM-DD" nombre="FECHA CREACIÓN">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3011_C60084" id="LblG3011_C60084">HORA CREACIÓN</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3011_C60084'])) {
                            echo $_GET['G3011_C60084'];
                        } ?>" readonly name="G3011_C60084" id="G3011_C60084" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3011_C60084">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3011_C60132" id="LblG3011_C60132">FECHA ÚLTIMA GESTIÓN</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3011_C60132'])) {
                            echo $_GET['G3011_C60132'];
                        } ?>" readonly name="G3011_C60132" id="G3011_C60132" placeholder="YYYY-MM-DD" nombre="FECHA ÚLTIMA GESTIÓN">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3011_C60133" id="LblG3011_C60133">HORA ÚLTIMA GESTIÓN</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3011_C60133'])) {
                            echo $_GET['G3011_C60133'];
                        } ?>" readonly name="G3011_C60133" id="G3011_C60133" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3011_C60133">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3011_C60085" id="LblG3011_C60085">FECHA CIERRE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3011_C60085'])) {
                            echo $_GET['G3011_C60085'];
                        } ?>" readonly name="G3011_C60085" id="G3011_C60085" placeholder="YYYY-MM-DD" nombre="FECHA CIERRE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G3011_C60086" id="LblG3011_C60086">HORA CIERRE</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora" value="<?php if (isset($_GET['G3011_C60086'])) {
                            echo $_GET['G3011_C60086'];
                        } ?>" readonly name="G3011_C60086" id="G3011_C60086" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G3011_C60086">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9191" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9191c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9191c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60062" id="LblG3011_C60062">ORIGEN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60062" value="<?php if (isset($_GET['G3011_C60062'])) {
                            echo $_GET['G3011_C60062'];
                        } ?>" readonly name="G3011_C60062"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60063" id="LblG3011_C60063">OPTIN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60063" value="<?php if (isset($_GET['G3011_C60063'])) {
                            echo $_GET['G3011_C60063'];
                        } ?>" readonly name="G3011_C60063"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60064" id="LblG3011_C60064">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60064" id="G3011_C60064">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3666 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9196" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9196c">
                DATOS A DILIGENCIAR PARA CREAR EL CASO
            </a>
        </h4>
        
    </div>
    <div id="s_9196c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3011_C60110" id="LblG3011_C60110">Consumidor Final</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3011_C60110" id="G3011_C60110">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60235" id="LblG3011_C60235">CÓDIGO GERENCIA DE VENTAS</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60235" value="<?php if (isset($_GET['G3011_C60235'])) {
                            echo $_GET['G3011_C60235'];
                        } ?>" readonly name="G3011_C60235"  placeholder="CÓDIGO GERENCIA DE VENTAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60236" id="LblG3011_C60236">GERENCIA DE VENTAS</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60236" value="<?php if (isset($_GET['G3011_C60236'])) {
                            echo $_GET['G3011_C60236'];
                        } ?>" readonly name="G3011_C60236"  placeholder="GERENCIA DE VENTAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60237" id="LblG3011_C60237">CÓDIGO SECTOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60237" value="<?php if (isset($_GET['G3011_C60237'])) {
                            echo $_GET['G3011_C60237'];
                        } ?>" readonly name="G3011_C60237"  placeholder="CÓDIGO SECTOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60238" id="LblG3011_C60238">SECTOR</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60238" value="<?php if (isset($_GET['G3011_C60238'])) {
                            echo $_GET['G3011_C60238'];
                        } ?>" readonly name="G3011_C60238"  placeholder="SECTOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60087" id="LblG3011_C60087">Cedula</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60087" value="<?php if (isset($_GET['G3011_C60087'])) {
                            echo $_GET['G3011_C60087'];
                        } ?>" readonly name="G3011_C60087"  placeholder="Cedula"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60089" id="LblG3011_C60089">Nombre y apellido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60089" value="<?php if (isset($_GET['G3011_C60089'])) {
                            echo $_GET['G3011_C60089'];
                        } ?>" readonly name="G3011_C60089"  placeholder="Nombre y apellido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60090" id="LblG3011_C60090">Correo</label>
                        <input type="email" maxlength="253" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id)" class="form-control input-sm" id="G3011_C60090" value="<?php if (isset($_GET['G3011_C60090'])) {
                            echo $_GET['G3011_C60090'];
                        } ?>" readonly name="G3011_C60090"  placeholder="Correo">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3011_C60312" id="LblG3011_C60312">CIUDAD</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3011_C60312" id="G3011_C60312">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60313" id="LblG3011_C60313">DEPARTAMENTO</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60313" value="<?php if (isset($_GET['G3011_C60313'])) {
                            echo $_GET['G3011_C60313'];
                        } ?>"  name="G3011_C60313"  placeholder="DEPARTAMENTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60093" id="LblG3011_C60093">Celular</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60093" value="<?php if (isset($_GET['G3011_C60093'])) {
                            echo $_GET['G3011_C60093'];
                        } ?>" readonly name="G3011_C60093"  placeholder="Celular"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60100" id="LblG3011_C60100">Tipo cliente</label>
                        <select readonly class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60100" id="G3011_C60100">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3668 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60322" id="LblG3011_C60322">Codigo</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60322" value="<?php if (isset($_GET['G3011_C60322'])) {
                            echo $_GET['G3011_C60322'];
                        } ?>" readonly name="G3011_C60322"  placeholder="Codigo"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60102" id="LblG3011_C60102">Tipificación SNAC CF</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60102" id="G3011_C60102">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3669 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60324" id="LblG3011_C60324">SLA</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60324" value="<?php if (isset($_GET['G3011_C60324'])) {
                            echo $_GET['G3011_C60324'];
                        } ?>" readonly name="G3011_C60324"  placeholder="SLA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3011_C60323" id="LblG3011_C60323">Clasificacion</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3011_C60323" value="<?php if (isset($_GET['G3011_C60323'])) {
                            echo $_GET['G3011_C60323'];
                        } ?>" readonly name="G3011_C60323"  placeholder="Clasificacion"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60103" id="LblG3011_C60103">Subtipificación SNAC CF</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60103" id="G3011_C60103">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3670 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60104" id="LblG3011_C60104">Tipo escalamiento SNAC CF</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60104" id="G3011_C60104">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3671 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60151" id="LblG3011_C60151">Escalado a</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60151" id="G3011_C60151">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3675 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3011_C60105" id="LblG3011_C60105">Observaciones</label>
                        <textarea class="form-control input-sm" name="G3011_C60105" id="G3011_C60105"  value="<?php if (isset($_GET['G3011_C60105'])) {
                            echo $_GET['G3011_C60105'];
                        } ?>" placeholder="Observaciones"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3011_C60239" id="LblG3011_C60239">Texto a agregar en el correo</label>
                        <textarea class="form-control input-sm" name="G3011_C60239" id="G3011_C60239" readonly value="<?php if (isset($_GET['G3011_C60239'])) {
                            echo $_GET['G3011_C60239'];
                        } ?>" placeholder="Texto a agregar en el correo"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9198" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9198c">
                DATOS GESTION DEL CASO SNAC CF
            </a>
        </h4>
        
    </div>
    <div id="s_9198c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3011_C60108" id="LblG3011_C60108">Estado del caso</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3011_C60108" id="G3011_C60108">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3672 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==45120){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

<div class=row>
                        <div class="col-md-12 col-xs-12">
                            
<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">Gestiones</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear Gestiones" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
                        </div>
                    </div>
                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G3011/G3011_eventos.js"></script>
<script type="text/javascript" src="formularios/G3011/G3011_extender_funcionalidad.php"></script><?php require_once "G3011_extender_funcionalidad.php";?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje =
        sendMessage('' + mensaje);
    });

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3011_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3011_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3011_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3011_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3011_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3011_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3011_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3011_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3011_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            //JDBD - Creamos un nuevo id incrementable y lo asignamos a este campo
            $.ajax({
                url:'<?=$url_crud;?>',
                type:'POST',
                data:{INCTB:"si"},
                success:function(data){
                    $("#G3011_C60082").val(data);
                }
            });
            //JDBD - Damos el valor fecha actual.
            $("#G3011_C60083").val("<?=date("Y-m-d");?>");
            $("#G3011_C60100").val("0").trigger("change");
            $("#G3011_C60102").val("0").trigger("change");
            $("#G3011_C60103").val("0").trigger("change");
            $("#G3011_C60104").val("0").trigger("change");
            $("#G3011_C60151").val("0").trigger("change");
            $("#G3011_C60108").val("45120").trigger("change");
            
            
        });
    <?php } ?>;
    let arr;
    <?php if(isset($_GET['id_campana_cbx']) && $_GET['id_campana_cbx'] !="" && $_GET['id_campana_cbx'] !=0) :?>
        <?php
        $intHuesped=$mysqli->query("SELECT CAMPAN_ConsInte__PROYEC_b FROM DYALOGOCRM_SISTEMA.CAMPAN WHERE CAMPAN_IdCamCbx__b={$_GET['id_campana_cbx']}");
        if($intHuesped && $intHuesped->num_rows==1){
            $intHuesped=$intHuesped->fetch_object();
            $intHuesped=$intHuesped->CAMPAN_ConsInte__PROYEC_b;
            if($_GET['sentido'] ==1){
                $patron=ObtenerPatron($intHuesped,$_GET['campana_crm']);
            }else{
                $patron=ObtenerPatron($intHuesped);
            }
        } 
        ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php endif; ?>
    <?php if(isset($_SESSION['HUESPED_CRM']) && $_SESSION['HUESPED_CRM']!=0 && $_SESSION['HUESPED_CRM'] !="") {
        $patron=ObtenerPatron($_SESSION['HUESPED_CRM']); ?>
        arr=<?php if($patron && $patron !="" && !is_null($patron)){echo $patron;}else{echo "";}?>;
    <?php } ?>
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G3011_C60082").val(item.G3011_C60082); 
                $("#G3011_C60083").val(item.G3011_C60083); 
                $("#G3011_C60084").val(item.G3011_C60084); 
                $("#G3011_C60132").val(item.G3011_C60132); 
                $("#G3011_C60133").val(item.G3011_C60133); 
                $("#G3011_C60085").val(item.G3011_C60085); 
                $("#G3011_C60086").val(item.G3011_C60086); 
                $("#G3011_C60062").val(item.G3011_C60062); 
                $("#G3011_C60063").val(item.G3011_C60063); 
                $("#G3011_C60064").val(item.G3011_C60064).trigger("change");  
                $("#G3011_C60110").attr("opt",item.G3011_C60110);
                $("#G3011_C60110").val(item.G3011_C60110).trigger("change"); 
                $("#G3011_C60235").val(item.G3011_C60235); 
                $("#G3011_C60236").val(item.G3011_C60236); 
                $("#G3011_C60237").val(item.G3011_C60237); 
                $("#G3011_C60238").val(item.G3011_C60238); 
                $("#G3011_C60087").val(item.G3011_C60087); 
                $("#G3011_C60089").val(item.G3011_C60089); 
                $("#G3011_C60090").val(item.G3011_C60090); 
                $("#G3011_C60312").attr("opt",item.G3011_C60312);
                $("#G3011_C60312").val(item.G3011_C60312).trigger("change"); 
                $("#G3011_C60313").val(item.G3011_C60313); 
                $("#G3011_C60093").val(item.G3011_C60093); 
                $("#G3011_C60100").val(item.G3011_C60100).trigger("change");  
                $("#G3011_C60322").val(item.G3011_C60322); 
                $("#G3011_C60102").val(item.G3011_C60102).trigger("change");  
                $("#G3011_C60324").val(item.G3011_C60324); 
                $("#G3011_C60323").val(item.G3011_C60323); 
                $("#G3011_C60103").attr("opt",item.G3011_C60103);  
                $("#G3011_C60104").val(item.G3011_C60104).trigger("change");  
                $("#G3011_C60151").val(item.G3011_C60151).trigger("change");  
                $("#G3011_C60105").val(item.G3011_C60105); 
                $("#G3011_C60239").val(item.G3011_C60239); 
                $("#G3011_C60108").val(item.G3011_C60108).trigger("change"); 
                
                cargarHijos_0(
        $("#G3011_C60082").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G3011_C60082").val());
            var id_0 = $("#G3011_C60082").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G3011_C60082").val());
            var id_0 = $("#G3011_C60082").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G3011_C60082").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3013&view=si&formaDetalle=si&formularioPadre=3011&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=60098<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3013&view=si&formaDetalle=si&formularioPadre=3011&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=60098&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3013&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=3011&pincheCampo=60098&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G3011_C60064").select2();
                            $("#G3011_C60110").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3011_C60110=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3011_C60110(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3011_C60110").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3011_C60110 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3011_C60110").html('<option value="'+data.G3010_ConsInte__b+'" >'+data.G3010_C60057+'</option>');
                                        
                                $("#G3011_C60087").val(data.G3010_C60056);
                                $("#G3011_C60089").val(data.G3010_C60057);
                                $("#G3011_C60093").val(data.G3010_C60060);
                                $("#G3011_C60100").val(data.G3010_C60061);
                                $("#G3011_C60235").val(data.G3010_C60231);
                                $("#G3011_C60236").val(data.G3010_C60232);
                                $("#G3011_C60237").val(data.G3010_C60233);
                                $("#G3011_C60238").val(data.G3010_C60234);
                                    }
                                });
                            });
                            $("#G3011_C60312").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3011_C60312=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3011_C60312(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3011_C60312").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3011_C60312 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3011_C60312").html('<option value="'+data.G3021_ConsInte__b+'" >'+data.G3021_C60307+'</option>');
                                        
                                $("#G3011_C60313").val(data.G3021_C60308);
                                    }
                                });
                            });

    $("#G3011_C60100").select2();

    $("#G3011_C60102").select2();

    $("#G3011_C60103").select2();

    $("#G3011_C60104").select2();

    $("#G3011_C60151").select2();

    $("#G3011_C60108").select2();
        //datepickers
        

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G3011_C60082").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G3011_C60064").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo cliente 

    $("#G3011_C60100").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipificación SNAC CF 

    $("#G3011_C60102").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3670' , idPadre : $(this).val() },
            success : function(data){
                var optG3011_C60103 = $("#G3011_C60103").attr("opt");
                $("#G3011_C60103").html(data);
                if (optG3011_C60103 != null) {
                    $("#G3011_C60103").val(optG3011_C60103).trigger("change");
                }
            }
        });
        
    });

    //function para Subtipificación SNAC CF 

    $("#G3011_C60103").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo escalamiento SNAC CF 

    $("#G3011_C60104").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Escalado a 

    $("#G3011_C60151").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Estado del caso 

    $("#G3011_C60108").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3011_C60082").val(item.G3011_C60082);
 
                                                $("#G3011_C60083").val(item.G3011_C60083);
 
                                                $("#G3011_C60084").val(item.G3011_C60084);
 
                                                $("#G3011_C60132").val(item.G3011_C60132);
 
                                                $("#G3011_C60133").val(item.G3011_C60133);
 
                                                $("#G3011_C60085").val(item.G3011_C60085);
 
                                                $("#G3011_C60086").val(item.G3011_C60086);
 
                                                $("#G3011_C60062").val(item.G3011_C60062);
 
                                                $("#G3011_C60063").val(item.G3011_C60063);
 
                    $("#G3011_C60064").val(item.G3011_C60064).trigger("change"); 
 
                    $("#G3011_C60110").attr("opt",item.G3011_C60110);
                    $("#G3011_C60110").val(item.G3011_C60110).trigger("change");
 
                                                $("#G3011_C60235").val(item.G3011_C60235);
 
                                                $("#G3011_C60236").val(item.G3011_C60236);
 
                                                $("#G3011_C60237").val(item.G3011_C60237);
 
                                                $("#G3011_C60238").val(item.G3011_C60238);
 
                                                $("#G3011_C60087").val(item.G3011_C60087);
 
                                                $("#G3011_C60089").val(item.G3011_C60089);
 
                                                $("#G3011_C60090").val(item.G3011_C60090);
 
                    $("#G3011_C60312").attr("opt",item.G3011_C60312);
                    $("#G3011_C60312").val(item.G3011_C60312).trigger("change");
 
                                                $("#G3011_C60313").val(item.G3011_C60313);
 
                                                $("#G3011_C60093").val(item.G3011_C60093);
 
                    $("#G3011_C60100").val(item.G3011_C60100).trigger("change"); 
 
                                                $("#G3011_C60322").val(item.G3011_C60322);
 
                    $("#G3011_C60102").val(item.G3011_C60102).trigger("change"); 
 
                                                $("#G3011_C60324").val(item.G3011_C60324);
 
                                                $("#G3011_C60323").val(item.G3011_C60323);
 
                    $("#G3011_C60103").attr("opt",item.G3011_C60103); 
 
                    $("#G3011_C60104").val(item.G3011_C60104).trigger("change"); 
 
                    $("#G3011_C60151").val(item.G3011_C60151).trigger("change"); 
 
                                                $("#G3011_C60105").val(item.G3011_C60105);
 
                                                $("#G3011_C60239").val(item.G3011_C60239);
 
                    $("#G3011_C60108").val(item.G3011_C60108).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });
            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','NÚMERO CASO','FECHA CREACIÓN','HORA CREACIÓN','FECHA ÚLTIMA GESTIÓN','HORA ÚLTIMA GESTIÓN','FECHA CIERRE','HORA CIERRE','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','Consumidor Final','CÓDIGO GERENCIA DE VENTAS','GERENCIA DE VENTAS','CÓDIGO SECTOR','SECTOR','Cedula','Nombre y apellido','Correo','CIUDAD','DEPARTAMENTO','Celular','Tipo cliente','Codigo','Tipificación SNAC CF','SLA','Clasificacion','Subtipificación SNAC CF','Tipo escalamiento SNAC CF','Escalado a','Observaciones','Texto a agregar en el correo','Estado del caso'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G3011_C60082', 
                        index:'G3011_C60082', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    {  
                        name:'G3011_C60083', 
                        index:'G3011_C60083', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3011_C60084', 
                        index:'G3011_C60084', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA CREACIÓN', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3011_C60132', 
                        index:'G3011_C60132', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3011_C60133', 
                        index:'G3011_C60133', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA ÚLTIMA GESTIÓN', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3011_C60085', 
                        index:'G3011_C60085', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3011_C60086', 
                        index:'G3011_C60086', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA CIERRE', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60062', 
                        index: 'G3011_C60062', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60063', 
                        index: 'G3011_C60063', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60064', 
                        index:'G3011_C60064', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3666&campo=G3011_C60064'
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60110', 
                        index:'G3011_C60110', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3011_C60110=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60235', 
                        index: 'G3011_C60235', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60236', 
                        index: 'G3011_C60236', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60237', 
                        index: 'G3011_C60237', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60238', 
                        index: 'G3011_C60238', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60087', 
                        index: 'G3011_C60087', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60089', 
                        index: 'G3011_C60089', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60312', 
                        index:'G3011_C60312', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3011_C60312=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60313', 
                        index: 'G3011_C60313', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60093', 
                        index: 'G3011_C60093', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60100', 
                        index:'G3011_C60100', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3668&campo=G3011_C60100'
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60322', 
                        index: 'G3011_C60322', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60102', 
                        index:'G3011_C60102', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3669&campo=G3011_C60102'
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60324', 
                        index: 'G3011_C60324', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60323', 
                        index: 'G3011_C60323', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60103', 
                        index:'G3011_C60103', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3670&campo=G3011_C60103'
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60104', 
                        index:'G3011_C60104', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3671&campo=G3011_C60104'
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60151', 
                        index:'G3011_C60151', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3675&campo=G3011_C60151'
                        }
                    }

                    ,
                    { 
                        name:'G3011_C60105', 
                        index:'G3011_C60105', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60239', 
                        index:'G3011_C60239', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3011_C60108', 
                        index:'G3011_C60108', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3672&campo=G3011_C60108'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3011_C60089',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
            ,subGrid: true,
            subGridRowExpanded: function(subgrid_id, row_id) { 
                // we pass two parameters 
                // subgrid_id is a id of the div tag created whitin a table data 
                // the id of this elemenet is a combination of the "sg_" + id of the row 
                // the row_id is the id of the row 
                // If we wan to pass additinal parameters to the url we can use 
                // a method getRowData(row_id) - which returns associative array in type name-value 
                // here we can easy construct the flowing 
                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Comentario de la gestión','Responsable gestión','Fecha gestión','Hora gestión','Número de caso al que pertenece', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G3013_C60094', 
                                index:'G3013_C60094', 
                                width:150, 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G3013_C60095', 
                                index: 'G3013_C60095', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            {  
                                name:'G3013_C60096', 
                                index:'G3013_C60096', 
                                width:120 ,
                                editable: true ,
                                formatter: 'text', 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).datepicker({
                                            language: "es",
                                            autoclose: true,
                                            todayHighlight: true
                                        });
                                    },
                                    defaultValue: function(){
                                        var currentTime = new Date();
                                        var month = parseInt(currentTime.getMonth() + 1);
                                        month = month <= 9 ? "0"+month : month;
                                        var day = currentTime.getDate();
                                        day = day <= 9 ? "0"+day : day;
                                        var year = currentTime.getFullYear();
                                        return year+"-"+month + "-"+day;
                                    }
                                }
                            }

                            ,
                            {  
                                name:'G3013_C60097', 
                                index:'G3013_C60097', 
                                width:70 ,
                                editable: true ,
                                formatter: 'text', 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        //Timepicker
                                         var options = {  //hh:mm 24 hour format only, defaults to current time
                                            timeFormat: 'HH:mm:ss',
                                            interval: 5,
                                            minTime: '10',
                                            dynamic: false,
                                            dropdown: true,
                                            scrollbar: true
                                        }; 
                                        $(el).timepicker(options);


                                    }
                                }
                            }
 
                            ,
                            {  
                                name:'G3013_C60098', 
                                index:'G3013_C60098', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                }, 
                subGridRowColapsed: function(subgrid_id, row_id) { 
                    // this function is called before removing the data 
                    //var subgrid_table_id; 
                    //subgrid_table_id = subgrid_id+"_t"; 
                    //jQuery("#"+subgrid_table_id).remove(); 
                }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G3011_C60082").val(item.G3011_C60082);

                        $("#G3011_C60083").val(item.G3011_C60083);

                        $("#G3011_C60084").val(item.G3011_C60084);

                        $("#G3011_C60132").val(item.G3011_C60132);

                        $("#G3011_C60133").val(item.G3011_C60133);

                        $("#G3011_C60085").val(item.G3011_C60085);

                        $("#G3011_C60086").val(item.G3011_C60086);

                        $("#G3011_C60062").val(item.G3011_C60062);

                        $("#G3011_C60063").val(item.G3011_C60063);
 
                    $("#G3011_C60064").val(item.G3011_C60064).trigger("change"); 
 
                    $("#G3011_C60110").attr("opt",item.G3011_C60110);
                    $("#G3011_C60110").val(item.G3011_C60110).trigger("change");

                        $("#G3011_C60235").val(item.G3011_C60235);

                        $("#G3011_C60236").val(item.G3011_C60236);

                        $("#G3011_C60237").val(item.G3011_C60237);

                        $("#G3011_C60238").val(item.G3011_C60238);

                        $("#G3011_C60087").val(item.G3011_C60087);

                        $("#G3011_C60089").val(item.G3011_C60089);

                        $("#G3011_C60090").val(item.G3011_C60090);
 
                    $("#G3011_C60312").attr("opt",item.G3011_C60312);
                    $("#G3011_C60312").val(item.G3011_C60312).trigger("change");

                        $("#G3011_C60313").val(item.G3011_C60313);

                        $("#G3011_C60093").val(item.G3011_C60093);
 
                    $("#G3011_C60100").val(item.G3011_C60100).trigger("change"); 

                        $("#G3011_C60322").val(item.G3011_C60322);
 
                    $("#G3011_C60102").val(item.G3011_C60102).trigger("change"); 

                        $("#G3011_C60324").val(item.G3011_C60324);

                        $("#G3011_C60323").val(item.G3011_C60323);
 
                    $("#G3011_C60103").attr("opt",item.G3011_C60103); 
 
                    $("#G3011_C60104").val(item.G3011_C60104).trigger("change"); 
 
                    $("#G3011_C60151").val(item.G3011_C60151).trigger("change"); 

                        $("#G3011_C60105").val(item.G3011_C60105);

                        $("#G3011_C60239").val(item.G3011_C60239);
 
                    $("#G3011_C60108").val(item.G3011_C60108).trigger("change"); 
                        
            cargarHijos_0(
        $("#G3011_C60082").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Comentario de la gestión','Responsable gestión','Fecha gestión','Hora gestión','Número de caso al que pertenece', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {
                        name:'G3013_C60094', 
                        index:'G3013_C60094', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3013_C60095', 
                        index: 'G3013_C60095', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G3013_C60096', 
                        index:'G3013_C60096', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G3013_C60097', 
                        index:'G3013_C60097', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = {  //hh:mm 24 hour format only, defaults to current time
                                    timeFormat: 'HH:mm:ss',
                                    interval: 5,
                                    minTime: '10',
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                }; 
                                $(el).timepicker(options);
                                $(".timepicker").css("z-index", 99999 );
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G3013_C60098', 
                        index:'G3013_C60098', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G3013_C60094',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Gestiones',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3013&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=60098&formularioPadre=3011<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G3011_C60082").val());
            var id_0 = $("#G3011_C60082").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G3011_C60082").val());
            var id_0 = $("#G3011_C60082").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
