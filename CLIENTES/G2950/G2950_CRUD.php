<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 59037")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 59037");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2950_ConsInte__b, G2950_FechaInsercion , G2950_Usuario ,  G2950_CodigoMiembro  , G2950_PoblacionOrigen , G2950_EstadoDiligenciamiento ,  G2950_IdLlamada , G2950_C59014 as principal ,G2950_C59076,G2950_C59016,G2950_C59017,G2950_C59018,G2950_C59019,G2950_C59020,G2950_C59003,G2950_C59004,G2950_C59005,G2950_C59006,G2950_C59007,G2950_C59008,G2950_C59009,G2950_C59010,G2950_C59011,G2950_C59014,G2950_C59015,G2950_C59023,G2950_C59037,G2950_C59531 FROM '.$BaseDatos.'.G2950 WHERE G2950_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2950_C59076'] = $key->G2950_C59076;

                $datos[$i]['G2950_C59016'] = $key->G2950_C59016;

                $datos[$i]['G2950_C59017'] = $key->G2950_C59017;

                $datos[$i]['G2950_C59018'] = $key->G2950_C59018;

                $datos[$i]['G2950_C59019'] = $key->G2950_C59019;

                $datos[$i]['G2950_C59020'] = $key->G2950_C59020;

                $datos[$i]['G2950_C59003'] = $key->G2950_C59003;

                $datos[$i]['G2950_C59004'] = $key->G2950_C59004;

                $datos[$i]['G2950_C59005'] = explode(' ', $key->G2950_C59005)[0];
  
                $hora = '';
                if(!is_null($key->G2950_C59006)){
                    $hora = explode(' ', $key->G2950_C59006)[1];
                }

                $datos[$i]['G2950_C59006'] = $hora;

                $datos[$i]['G2950_C59007'] = $key->G2950_C59007;

                $datos[$i]['G2950_C59008'] = $key->G2950_C59008;

                $datos[$i]['G2950_C59009'] = $key->G2950_C59009;

                $datos[$i]['G2950_C59010'] = $key->G2950_C59010;

                $datos[$i]['G2950_C59011'] = $key->G2950_C59011;

                $datos[$i]['G2950_C59014'] = $key->G2950_C59014;

                $datos[$i]['G2950_C59015'] = $key->G2950_C59015;

                $datos[$i]['G2950_C59023'] = $key->G2950_C59023;

                $datos[$i]['G2950_C59037'] = $key->G2950_C59037;

                $datos[$i]['G2950_C59531'] = $key->G2950_C59531;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2950";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2950_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2950_ConsInte__b as id,  G2950_C59014 as camp1 , G2950_C59015 as camp2 
                     FROM ".$BaseDatos.".G2950  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2950_ConsInte__b as id,  G2950_C59014 as camp1 , G2950_C59015 as camp2  
                    FROM ".$BaseDatos.".G2950  JOIN ".$BaseDatos.".G2950_M".$_POST['muestra']." ON G2950_ConsInte__b = G2950_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2950_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2950_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2950_C59014 LIKE '%".$B."%' OR G2950_C59015 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2950_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

            if(isset($_GET['MostrarCombo_Guion_2967_C59386'])){
                echo '<select class="form-control input-sm"  name="2967_C59386" id="2967_C59386">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2967_C59386'])){
                $Ysql = "SELECT G2951_ConsInte__b as id,  G2951_C59033 as text FROM ".$BaseDatos.".G2951 WHERE G2951_C59034 LIKE '%".$_POST['q']."%' OR G2951_C59033 = '".$_POST['q']."' ";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2967_C59386'])){
                $Lsql = "SELECT  G2951_ConsInte__b as id , G2951_C59034, G2951_C59035, G2951_C59528 FROM ".$BaseDatos.".G2951 WHERE G2951_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2967_C59386'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2967_C59382'] = $key->G2951_C59034;

                    $data[$i]['G2967_C59384'] = $key->G2951_C59035;

                    $data[$i]['G2967_C59529'] = $key->G2951_C59528;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2950");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2950_ConsInte__b, G2950_FechaInsercion , G2950_Usuario ,  G2950_CodigoMiembro  , G2950_PoblacionOrigen , G2950_EstadoDiligenciamiento ,  G2950_IdLlamada , G2950_C59014 as principal ,G2950_C59076,G2950_C59016,G2950_C59017,G2950_C59018,G2950_C59019,G2950_C59020, a.LISOPC_Nombre____b as G2950_C59003, b.LISOPC_Nombre____b as G2950_C59004,G2950_C59005,G2950_C59006,G2950_C59007,G2950_C59008,G2950_C59009,G2950_C59010,G2950_C59011,G2950_C59014,G2950_C59015,G2950_C59023,G2950_C59037,G2950_C59531 FROM '.$BaseDatos.'.G2950 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2950_C59003 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2950_C59004';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2950_C59006)){
                    $hora_a = explode(' ', $fila->G2950_C59006)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2950_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2950_ConsInte__b , ($fila->G2950_C59076) , ($fila->G2950_C59016) , ($fila->G2950_C59017) , ($fila->G2950_C59018) , ($fila->G2950_C59019) , ($fila->G2950_C59020) , ($fila->G2950_C59003) , ($fila->G2950_C59004) , explode(' ', $fila->G2950_C59005)[0] , $hora_a , ($fila->G2950_C59007) , ($fila->G2950_C59008) , ($fila->G2950_C59009) , ($fila->G2950_C59010) , ($fila->G2950_C59011) , ($fila->G2950_C59014) , ($fila->G2950_C59015) , ($fila->G2950_C59023) , ($fila->G2950_C59037) , ($fila->G2950_C59531) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2950 WHERE G2950_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2950";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2950_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2950_ConsInte__b as id,  G2950_C59014 as camp1 , G2950_C59015 as camp2  FROM '.$BaseDatos.'.G2950 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2950_ConsInte__b as id,  G2950_C59014 as camp1 , G2950_C59015 as camp2  
                    FROM ".$BaseDatos.".G2950  JOIN ".$BaseDatos.".G2950_M".$_POST['muestra']." ON G2950_ConsInte__b = G2950_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2950_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2950_C59014 LIKE "%'.$B.'%" OR G2950_C59015 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2950_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2950 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2950(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2950_C59076"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59076 = '".$_POST["G2950_C59076"]."'";
                $LsqlI .= $separador."G2950_C59076";
                $LsqlV .= $separador."'".$_POST["G2950_C59076"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59016"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59016 = '".$_POST["G2950_C59016"]."'";
                $LsqlI .= $separador."G2950_C59016";
                $LsqlV .= $separador."'".$_POST["G2950_C59016"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59017"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59017 = '".$_POST["G2950_C59017"]."'";
                $LsqlI .= $separador."G2950_C59017";
                $LsqlV .= $separador."'".$_POST["G2950_C59017"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59018"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59018 = '".$_POST["G2950_C59018"]."'";
                $LsqlI .= $separador."G2950_C59018";
                $LsqlV .= $separador."'".$_POST["G2950_C59018"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59019"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59019 = '".$_POST["G2950_C59019"]."'";
                $LsqlI .= $separador."G2950_C59019";
                $LsqlV .= $separador."'".$_POST["G2950_C59019"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59020"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59020 = '".$_POST["G2950_C59020"]."'";
                $LsqlI .= $separador."G2950_C59020";
                $LsqlV .= $separador."'".$_POST["G2950_C59020"]."'";
                $validar = 1;
            }
             
 
            $G2950_C59003 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2950_C59003 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2950_C59003 = ".$G2950_C59003;
                    $LsqlI .= $separador." G2950_C59003";
                    $LsqlV .= $separador.$G2950_C59003;
                    $validar = 1;

                    
                }
            }
 
            $G2950_C59004 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2950_C59004 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2950_C59004 = ".$G2950_C59004;
                    $LsqlI .= $separador." G2950_C59004";
                    $LsqlV .= $separador.$G2950_C59004;
                    $validar = 1;
                }
            }
 
            $G2950_C59005 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2950_C59005 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2950_C59005 = ".$G2950_C59005;
                    $LsqlI .= $separador." G2950_C59005";
                    $LsqlV .= $separador.$G2950_C59005;
                    $validar = 1;
                }
            }
 
            $G2950_C59006 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2950_C59006 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2950_C59006 = ".$G2950_C59006;
                    $LsqlI .= $separador." G2950_C59006";
                    $LsqlV .= $separador.$G2950_C59006;
                    $validar = 1;
                }
            }
 
            $G2950_C59007 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2950_C59007 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2950_C59007 = ".$G2950_C59007;
                    $LsqlI .= $separador." G2950_C59007";
                    $LsqlV .= $separador.$G2950_C59007;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2950_C59008"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59008 = '".$_POST["G2950_C59008"]."'";
                $LsqlI .= $separador."G2950_C59008";
                $LsqlV .= $separador."'".$_POST["G2950_C59008"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59009"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59009 = '".$_POST["G2950_C59009"]."'";
                $LsqlI .= $separador."G2950_C59009";
                $LsqlV .= $separador."'".$_POST["G2950_C59009"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59010"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59010 = '".$_POST["G2950_C59010"]."'";
                $LsqlI .= $separador."G2950_C59010";
                $LsqlV .= $separador."'".$_POST["G2950_C59010"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59011"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59011 = '".$_POST["G2950_C59011"]."'";
                $LsqlI .= $separador."G2950_C59011";
                $LsqlV .= $separador."'".$_POST["G2950_C59011"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59012"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59012 = '".$_POST["G2950_C59012"]."'";
                $LsqlI .= $separador."G2950_C59012";
                $LsqlV .= $separador."'".$_POST["G2950_C59012"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59014"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59014 = '".$_POST["G2950_C59014"]."'";
                $LsqlI .= $separador."G2950_C59014";
                $LsqlV .= $separador."'".$_POST["G2950_C59014"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59015"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59015 = '".$_POST["G2950_C59015"]."'";
                $LsqlI .= $separador."G2950_C59015";
                $LsqlV .= $separador."'".$_POST["G2950_C59015"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59021"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59021 = '".$_POST["G2950_C59021"]."'";
                $LsqlI .= $separador."G2950_C59021";
                $LsqlV .= $separador."'".$_POST["G2950_C59021"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59024"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59024 = '".$_POST["G2950_C59024"]."'";
                $LsqlI .= $separador."G2950_C59024";
                $LsqlV .= $separador."'".$_POST["G2950_C59024"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59025"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59025 = '".$_POST["G2950_C59025"]."'";
                $LsqlI .= $separador."G2950_C59025";
                $LsqlV .= $separador."'".$_POST["G2950_C59025"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59026"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59026 = '".$_POST["G2950_C59026"]."'";
                $LsqlI .= $separador."G2950_C59026";
                $LsqlV .= $separador."'".$_POST["G2950_C59026"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59027"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59027 = '".$_POST["G2950_C59027"]."'";
                $LsqlI .= $separador."G2950_C59027";
                $LsqlV .= $separador."'".$_POST["G2950_C59027"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2950_C59023"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59023 = '".$_POST["G2950_C59023"]."'";
                $LsqlI .= $separador."G2950_C59023";
                $LsqlV .= $separador."'".$_POST["G2950_C59023"]."'";
                $validar = 1;
            }
             
  
            $G2950_C59037 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2950_C59037"])){
                if($_POST["G2950_C59037"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2950_C59037 = $_POST["G2950_C59037"];
                    $LsqlU .= $separador." G2950_C59037 = ".$G2950_C59037."";
                    $LsqlI .= $separador." G2950_C59037";
                    $LsqlV .= $separador.$G2950_C59037;
                    $validar = 1;
                }
            }
  
            $G2950_C59531 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2950_C59531"])){
                if($_POST["G2950_C59531"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2950_C59531 = $_POST["G2950_C59531"];
                    $LsqlU .= $separador." G2950_C59531 = ".$G2950_C59531."";
                    $LsqlI .= $separador." G2950_C59531";
                    $LsqlV .= $separador.$G2950_C59531;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2950_C59039"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_C59039 = '".$_POST["G2950_C59039"]."'";
                $LsqlI .= $separador."G2950_C59039";
                $LsqlV .= $separador."'".$_POST["G2950_C59039"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2950_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2950_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2950_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2950_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2950_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2950_Usuario , G2950_FechaInsercion, G2950_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2950_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2950 WHERE G2950_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if (isset($_GET["SumarPedidos"])) {

        $idPedido = $_POST["idPedido"];

        $strSum_t = "SELECT SUM(G2967_C59385) AS suma FROM ".$BaseDatos.".G2967 WHERE G2967_C59381 = ".$idPedido;
        $resSum_t = $mysqli->query($strSum_t);

        if ($resSum_t && $resSum_t->num_rows > 0) {
            echo $resSum_t->fetch_object()->suma;;
        }

    }

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2967_ConsInte__b, G2967_C59381, G2967_C59529, G2951_C59033 as G2967_C59386, G2967_C59382, G2967_C59383, G2967_C59385, G2967_C59384,G2967_C60292 FROM ".$BaseDatos.".G2967  LEFT JOIN ".$BaseDatos.".G2951 ON G2951_ConsInte__b  =  G2967_C59386 ";

        $SQL .= " WHERE G2967_C59381 = '".$numero."'"; 

        $SQL .= " ORDER BY G2967_C59381";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2967_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2967_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2967_C59381."</cell>"; 

                echo "<cell>". $fila->G2967_C59529."</cell>"; 

                echo "<cell>". ($fila->G2967_C59386)."</cell>";

                echo "<cell>". ($fila->G2967_C59382)."</cell>";

                echo "<cell>". $fila->G2967_C59383."</cell>"; 

                echo "<cell>". $fila->G2967_C59385."</cell>"; 

                echo "<cell>". $fila->G2967_C59384."</cell>";

                echo "<cell>". $fila->G2967_C60292."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2967 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2967(";
            $LsqlV = " VALUES ("; 
  
                $G2967_C59529 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59529"])){    
                    if($_POST["G2967_C59529"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59529 = $_POST["G2967_C59529"];
                        $LsqlU .= $separador." G2967_C59529 = '".$G2967_C59529."'";
                        $LsqlI .= $separador." G2967_C59529";
                        $LsqlV .= $separador."'".$G2967_C59529."'";
                        $validar = 1;
                    }
                }
  
                if(isset($_POST["G2967_C59386"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2967_C59386 = '".$_POST["G2967_C59386"]."'";
                    $LsqlI .= $separador."G2967_C59386";
                    $LsqlV .= $separador."'".$_POST["G2967_C59386"]."'";
                    $validar = 1;
                }


                if(isset($_POST["G2967_C60292"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2967_C60292 = '".$_POST["G2967_C60292"]."'";
                    $LsqlI .= $separador."G2967_C60292";
                    $LsqlV .= $separador."'".$_POST["G2967_C60292"]."'";
                    $validar = 1;
                }
 

                if(isset($_POST["G2967_C59382"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2967_C59382 = '".$_POST["G2967_C59382"]."'";
                    $LsqlI .= $separador."G2967_C59382";
                    $LsqlV .= $separador."'".$_POST["G2967_C59382"]."'";
                    $validar = 1;
                }

                                                                               
 
                $G2967_C59383= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59383"])){    
                    if($_POST["G2967_C59383"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59383 = $_POST["G2967_C59383"];
                        $LsqlU .= $separador." G2967_C59383 = '".$G2967_C59383."'";
                        $LsqlI .= $separador." G2967_C59383";
                        $LsqlV .= $separador."'".$G2967_C59383."'";
                        $validar = 1;
                    }
                }
  
                $G2967_C59385 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59385"])){    
                    if($_POST["G2967_C59385"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59385 = $_POST["G2967_C59385"];
                        $LsqlU .= $separador." G2967_C59385 = '".$G2967_C59385."'";
                        $LsqlI .= $separador." G2967_C59385";
                        $LsqlV .= $separador."'".$G2967_C59385."'";
                        $validar = 1;
                    }
                }
  
                $G2967_C59384 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59384"])){    
                    if($_POST["G2967_C59384"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59384 = $_POST["G2967_C59384"];
                        $LsqlU .= $separador." G2967_C59384 = '".$G2967_C59384."'";
                        $LsqlI .= $separador." G2967_C59384";
                        $LsqlV .= $separador."'".$G2967_C59384."'";
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2967_C59381 = $numero;
                    $LsqlU .= ", G2967_C59381 = ".$G2967_C59381."";
                    $LsqlI .= ", G2967_C59381";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2967_Usuario ,  G2967_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2967_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2967 WHERE  G2967_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
