<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55285")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55285");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2414_ConsInte__b, G2414_FechaInsercion , G2414_Usuario ,  G2414_CodigoMiembro  , G2414_PoblacionOrigen , G2414_EstadoDiligenciamiento ,  G2414_IdLlamada , G2414_C47452 as principal ,G2414_C47451,G2414_C47452,G2414_C47453,G2414_C47454,G2414_C47455,G2414_C47456,G2414_C47457,G2414_C47458,G2414_C55284,G2414_C55285,G2414_C47440,G2414_C47441,G2414_C47442,G2414_C47443,G2414_C47444,G2414_C47445,G2414_C47446,G2414_C47447,G2414_C47448 FROM '.$BaseDatos.'.G2414 WHERE G2414_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2414_C47451'] = $key->G2414_C47451;

                $datos[$i]['G2414_C47452'] = $key->G2414_C47452;

                $datos[$i]['G2414_C47453'] = $key->G2414_C47453;

                $datos[$i]['G2414_C47454'] = $key->G2414_C47454;

                $datos[$i]['G2414_C47455'] = $key->G2414_C47455;

                $datos[$i]['G2414_C47456'] = $key->G2414_C47456;

                $datos[$i]['G2414_C47457'] = $key->G2414_C47457;

                $datos[$i]['G2414_C47458'] = $key->G2414_C47458;

                $datos[$i]['G2414_C55284'] = $key->G2414_C55284;

                $datos[$i]['G2414_C55285'] = $key->G2414_C55285;

                $datos[$i]['G2414_C47440'] = $key->G2414_C47440;

                $datos[$i]['G2414_C47441'] = $key->G2414_C47441;

                $datos[$i]['G2414_C47442'] = explode(' ', $key->G2414_C47442)[0];
  
                $hora = '';
                if(!is_null($key->G2414_C47443)){
                    $hora = explode(' ', $key->G2414_C47443)[1];
                }

                $datos[$i]['G2414_C47443'] = $hora;

                $datos[$i]['G2414_C47444'] = $key->G2414_C47444;

                $datos[$i]['G2414_C47445'] = $key->G2414_C47445;

                $datos[$i]['G2414_C47446'] = $key->G2414_C47446;

                $datos[$i]['G2414_C47447'] = $key->G2414_C47447;

                $datos[$i]['G2414_C47448'] = $key->G2414_C47448;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2414";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2414_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2414_ConsInte__b as id,  G2414_C47452 as camp1 , G2414_C55285 as camp2 
                     FROM ".$BaseDatos.".G2414  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2414_ConsInte__b as id,  G2414_C47452 as camp1 , G2414_C55285 as camp2  
                    FROM ".$BaseDatos.".G2414  JOIN ".$BaseDatos.".G2414_M".$_POST['muestra']." ON G2414_ConsInte__b = G2414_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2414_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2414_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2414_C47452 LIKE '%".$B."%' OR G2414_C55285 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2414_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2414");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2414_ConsInte__b, G2414_FechaInsercion , G2414_Usuario ,  G2414_CodigoMiembro  , G2414_PoblacionOrigen , G2414_EstadoDiligenciamiento ,  G2414_IdLlamada , G2414_C47452 as principal ,G2414_C47451,G2414_C47452,G2414_C47453,G2414_C47454,G2414_C47455, a.LISOPC_Nombre____b as G2414_C47456, b.LISOPC_Nombre____b as G2414_C47457,G2414_C47458,G2414_C55284,G2414_C55285, c.LISOPC_Nombre____b as G2414_C47440, d.LISOPC_Nombre____b as G2414_C47441,G2414_C47442,G2414_C47443,G2414_C47444,G2414_C47445,G2414_C47446,G2414_C47447,G2414_C47448 FROM '.$BaseDatos.'.G2414 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2414_C47456 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2414_C47457 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2414_C47440 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2414_C47441';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2414_C47443)){
                    $hora_a = explode(' ', $fila->G2414_C47443)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2414_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2414_ConsInte__b , ($fila->G2414_C47451) , ($fila->G2414_C47452) , ($fila->G2414_C47453) , ($fila->G2414_C47454) , ($fila->G2414_C47455) , ($fila->G2414_C47456) , ($fila->G2414_C47457) , ($fila->G2414_C47458) , ($fila->G2414_C55284) , ($fila->G2414_C55285) , ($fila->G2414_C47440) , ($fila->G2414_C47441) , explode(' ', $fila->G2414_C47442)[0] , $hora_a , ($fila->G2414_C47444) , ($fila->G2414_C47445) , ($fila->G2414_C47446) , ($fila->G2414_C47447) , ($fila->G2414_C47448) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2414 WHERE G2414_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2414";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2414_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2414_ConsInte__b as id,  G2414_C47452 as camp1 , G2414_C55285 as camp2  FROM '.$BaseDatos.'.G2414 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2414_ConsInte__b as id,  G2414_C47452 as camp1 , G2414_C55285 as camp2  
                    FROM ".$BaseDatos.".G2414  JOIN ".$BaseDatos.".G2414_M".$_POST['muestra']." ON G2414_ConsInte__b = G2414_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2414_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2414_C47452 LIKE "%'.$B.'%" OR G2414_C55285 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2414_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2414 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2414(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2414_C47451"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47451 = '".$_POST["G2414_C47451"]."'";
                $LsqlI .= $separador."G2414_C47451";
                $LsqlV .= $separador."'".$_POST["G2414_C47451"]."'";
                $validar = 1;
            }
             
  
            $G2414_C47452 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2414_C47452"])){
                if($_POST["G2414_C47452"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2414_C47452 = $_POST["G2414_C47452"];
                    $LsqlU .= $separador." G2414_C47452 = ".$G2414_C47452."";
                    $LsqlI .= $separador." G2414_C47452";
                    $LsqlV .= $separador.$G2414_C47452;
                    $validar = 1;
                }
            }
  
            $G2414_C47453 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2414_C47453"])){
                if($_POST["G2414_C47453"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2414_C47453 = $_POST["G2414_C47453"];
                    $LsqlU .= $separador." G2414_C47453 = ".$G2414_C47453."";
                    $LsqlI .= $separador." G2414_C47453";
                    $LsqlV .= $separador.$G2414_C47453;
                    $validar = 1;
                }
            }
  
            $G2414_C47454 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2414_C47454"])){
                if($_POST["G2414_C47454"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2414_C47454 = $_POST["G2414_C47454"];
                    $LsqlU .= $separador." G2414_C47454 = ".$G2414_C47454."";
                    $LsqlI .= $separador." G2414_C47454";
                    $LsqlV .= $separador.$G2414_C47454;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2414_C47455"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47455 = '".$_POST["G2414_C47455"]."'";
                $LsqlI .= $separador."G2414_C47455";
                $LsqlV .= $separador."'".$_POST["G2414_C47455"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C47456"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47456 = '".$_POST["G2414_C47456"]."'";
                $LsqlI .= $separador."G2414_C47456";
                $LsqlV .= $separador."'".$_POST["G2414_C47456"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C47457"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47457 = '".$_POST["G2414_C47457"]."'";
                $LsqlI .= $separador."G2414_C47457";
                $LsqlV .= $separador."'".$_POST["G2414_C47457"]."'";
                $validar = 1;
            }
             
  
            $G2414_C47458 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2414_C47458"])){
                if($_POST["G2414_C47458"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2414_C47458 = $_POST["G2414_C47458"];
                    $LsqlU .= $separador." G2414_C47458 = ".$G2414_C47458."";
                    $LsqlI .= $separador." G2414_C47458";
                    $LsqlV .= $separador.$G2414_C47458;
                    $validar = 1;
                }
            }
  
            $G2414_C55284 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2414_C55284"])){
                if($_POST["G2414_C55284"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2414_C55284 = $_POST["G2414_C55284"];
                    $LsqlU .= $separador." G2414_C55284 = ".$G2414_C55284."";
                    $LsqlI .= $separador." G2414_C55284";
                    $LsqlV .= $separador.$G2414_C55284;
                    $validar = 1;
                }
            }
  
            $G2414_C55285 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2414_C55285"])){
                if($_POST["G2414_C55285"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2414_C55285 = $_POST["G2414_C55285"];
                    $LsqlU .= $separador." G2414_C55285 = ".$G2414_C55285."";
                    $LsqlI .= $separador." G2414_C55285";
                    $LsqlV .= $separador.$G2414_C55285;
                    $validar = 1;
                }
            }
 
            $G2414_C47440 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2414_C47440 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2414_C47440 = ".$G2414_C47440;
                    $LsqlI .= $separador." G2414_C47440";
                    $LsqlV .= $separador.$G2414_C47440;
                    $validar = 1;

                    
                }
            }
 
            $G2414_C47441 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2414_C47441 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2414_C47441 = ".$G2414_C47441;
                    $LsqlI .= $separador." G2414_C47441";
                    $LsqlV .= $separador.$G2414_C47441;
                    $validar = 1;
                }
            }
 
            $G2414_C47442 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2414_C47442 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2414_C47442 = ".$G2414_C47442;
                    $LsqlI .= $separador." G2414_C47442";
                    $LsqlV .= $separador.$G2414_C47442;
                    $validar = 1;
                }
            }
 
            $G2414_C47443 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2414_C47443 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2414_C47443 = ".$G2414_C47443;
                    $LsqlI .= $separador." G2414_C47443";
                    $LsqlV .= $separador.$G2414_C47443;
                    $validar = 1;
                }
            }
 
            $G2414_C47444 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2414_C47444 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2414_C47444 = ".$G2414_C47444;
                    $LsqlI .= $separador." G2414_C47444";
                    $LsqlV .= $separador.$G2414_C47444;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2414_C47445"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47445 = '".$_POST["G2414_C47445"]."'";
                $LsqlI .= $separador."G2414_C47445";
                $LsqlV .= $separador."'".$_POST["G2414_C47445"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C47446"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47446 = '".$_POST["G2414_C47446"]."'";
                $LsqlI .= $separador."G2414_C47446";
                $LsqlV .= $separador."'".$_POST["G2414_C47446"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C47447"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47447 = '".$_POST["G2414_C47447"]."'";
                $LsqlI .= $separador."G2414_C47447";
                $LsqlV .= $separador."'".$_POST["G2414_C47447"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C47448"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47448 = '".$_POST["G2414_C47448"]."'";
                $LsqlI .= $separador."G2414_C47448";
                $LsqlV .= $separador."'".$_POST["G2414_C47448"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C47449"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C47449 = '".$_POST["G2414_C47449"]."'";
                $LsqlI .= $separador."G2414_C47449";
                $LsqlV .= $separador."'".$_POST["G2414_C47449"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2414_C55943"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_C55943 = '".$_POST["G2414_C55943"]."'";
                $LsqlI .= $separador."G2414_C55943";
                $LsqlV .= $separador."'".$_POST["G2414_C55943"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2414_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2414_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2414_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2414_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2414_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2414_Usuario , G2414_FechaInsercion, G2414_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2414_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2414 WHERE G2414_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
