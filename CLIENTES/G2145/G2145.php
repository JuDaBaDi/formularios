<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2145/G2145_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2145_ConsInte__b as id, G2145_C41248 as camp1 , G2145_C41249 as camp2 FROM ".$BaseDatos.".G2145  WHERE G2145_Usuario = ".$idUsuario." ORDER BY G2145_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2145_ConsInte__b as id, G2145_C41248 as camp1 , G2145_C41249 as camp2 FROM ".$BaseDatos.".G2145  ORDER BY G2145_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2145_ConsInte__b as id, G2145_C41248 as camp1 , G2145_C41249 as camp2 FROM ".$BaseDatos.".G2145 JOIN ".$BaseDatos.".G2145_M".$resultEstpas->muestr." ON G2145_ConsInte__b = G2145_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2145_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2145_ConsInte__b as id, G2145_C41248 as camp1 , G2145_C41249 as camp2 FROM ".$BaseDatos.".G2145 JOIN ".$BaseDatos.".G2145_M".$resultEstpas->muestr." ON G2145_ConsInte__b = G2145_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2145_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2145_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2145_ConsInte__b as id, G2145_C41248 as camp1 , G2145_C41249 as camp2 FROM ".$BaseDatos.".G2145  ORDER BY G2145_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="5921" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5921c">
                CONVERSACION
            </a>
        </h4>
        
    </div>
    <div id="s_5921c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41248" id="LblG2145_C41248">NOMBRE</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41248" value="<?php if (isset($_GET['G2145_C41248'])) {
                            echo $_GET['G2145_C41248'];
                        } ?>"  name="G2145_C41248"  placeholder="NOMBRE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41249" id="LblG2145_C41249">CEDULA</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41249" value="<?php if (isset($_GET['G2145_C41249'])) {
                            echo $_GET['G2145_C41249'];
                        } ?>"  name="G2145_C41249"  placeholder="CEDULA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41250" id="LblG2145_C41250">CELULAR</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41250" value="<?php if (isset($_GET['G2145_C41250'])) {
                            echo $_GET['G2145_C41250'];
                        } ?>"  name="G2145_C41250"  placeholder="CELULAR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41251" id="LblG2145_C41251">CIUDAD 1</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41251" id="G2145_C41251">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2291 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41252" id="LblG2145_C41252">FIJO</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41252" value="<?php if (isset($_GET['G2145_C41252'])) {
                            echo $_GET['G2145_C41252'];
                        } ?>"  name="G2145_C41252"  placeholder="FIJO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

                     <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41254" id="LblG2145_C41254">TIPO DE CLIENTE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41254" id="G2145_C41254">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2270 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C52335" id="LblG2145_C52335">Numero Grabacion</label>
                        <input type="text" class="form-control input-sm" id="G2145_C52335" value="<?php if (isset($_GET['G2145_C52335'])) {
                            echo $_GET['G2145_C52335'];
                        } ?>"  name="G2145_C52335"  placeholder="Numero Grabacion">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->
  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41253" id="LblG2145_C41253">DIRECCION</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41253" value="<?php if (isset($_GET['G2145_C41253'])) {
                            echo $_GET['G2145_C41253'];
                        } ?>"  name="G2145_C41253"  placeholder="DIRECCION">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->

  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->

        <div class="row">

            <div class="col-md-6 col-xs-6">
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C52828" id="LblG2145_C52828">Fecha de Creación</label>
                        <input type="text" class="form-control input-sm" value="<?php if (isset($_GET['G2145_C52828'])) {
                            echo $_GET['G2145_C52828'];
                        } ?>"  name="G2145_C52828" id="G2145_C52828" placeholder="YYYY-MM-DD" readonly>
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->

                   
  
            </div> <!-- AQUIFINCAMPO -->
        </div>

        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="5922" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41159" id="LblG2145_C41159">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41159" value="<?php if (isset($_GET['G2145_C41159'])) {
                            echo $_GET['G2145_C41159'];
                        } ?>" readonly name="G2145_C41159"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41160" id="LblG2145_C41160">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41160" value="<?php if (isset($_GET['G2145_C41160'])) {
                            echo $_GET['G2145_C41160'];
                        } ?>" readonly name="G2145_C41160"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="5946" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5946c">
                PERSONA JURIDICA
            </a>
        </h4>
        
    </div>
    <div id="s_5946c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41255" id="LblG2145_C41255">TITULAR LINEA (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41255" value="<?php if (isset($_GET['G2145_C41255'])) {
                            echo $_GET['G2145_C41255'];
                        } ?>"  name="G2145_C41255"  placeholder="TITULAR LINEA (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41256" id="LblG2145_C41256">CEDULA TITULAR (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41256" value="<?php if (isset($_GET['G2145_C41256'])) {
                            echo $_GET['G2145_C41256'];
                        } ?>"  name="G2145_C41256"  placeholder="CEDULA TITULAR (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41257" id="LblG2145_C41257">FECHA DE EXPEDICION (JURIDICO)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41257'])) {
                            echo $_GET['G2145_C41257'];
                        } ?>"  name="G2145_C41257" id="G2145_C41257" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41258" id="LblG2145_C41258">FECHA DE NACIMIENTO (JURIDICO)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41258'])) {
                            echo $_GET['G2145_C41258'];
                        } ?>"  name="G2145_C41258" id="G2145_C41258" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41259" id="LblG2145_C41259">CORREO (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41259" value="<?php if (isset($_GET['G2145_C41259'])) {
                            echo $_GET['G2145_C41259'];
                        } ?>"  name="G2145_C41259"  placeholder="CORREO (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41260" id="LblG2145_C41260">SCORE (JURIDICO)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41260" id="G2145_C41260">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2275 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5947" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5947c">
                PERSONA NATURAL
            </a>
        </h4>
        
    </div>
    <div id="s_5947c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41261" id="LblG2145_C41261">TITULAR LINEA (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41261" value="<?php if (isset($_GET['G2145_C41261'])) {
                            echo $_GET['G2145_C41261'];
                        } ?>"  name="G2145_C41261"  placeholder="TITULAR LINEA (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41262" id="LblG2145_C41262">CEDULA TITULAR (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41262" value="<?php if (isset($_GET['G2145_C41262'])) {
                            echo $_GET['G2145_C41262'];
                        } ?>"  name="G2145_C41262"  placeholder="CEDULA TITULAR (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41263" id="LblG2145_C41263">FECHA DE EXPEDICION (NATURAL)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41263'])) {
                            echo $_GET['G2145_C41263'];
                        } ?>"  name="G2145_C41263" id="G2145_C41263" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41264" id="LblG2145_C41264">FECHA DE NACIMIENTO (NATURAL)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41264'])) {
                            echo $_GET['G2145_C41264'];
                        } ?>"  name="G2145_C41264" id="G2145_C41264" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41265" id="LblG2145_C41265">CORREO (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41265" value="<?php if (isset($_GET['G2145_C41265'])) {
                            echo $_GET['G2145_C41265'];
                        } ?>"  name="G2145_C41265"  placeholder="CORREO (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41266" id="LblG2145_C41266">SCORE (NATURAL)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41266" id="G2145_C41266">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2275 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="5948" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41267" id="LblG2145_C41267">CANTIDAD DE LINEAS</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41267" value="<?php if (isset($_GET['G2145_C41267'])) {
                            echo $_GET['G2145_C41267'];
                        } ?>"  name="G2145_C41267"  placeholder="CANTIDAD DE LINEAS">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41268" id="LblG2145_C41268">FECHA DE CORTE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41268'])) {
                            echo $_GET['G2145_C41268'];
                        } ?>"  name="G2145_C41268" id="G2145_C41268" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">

            <div class="col-md-6 col-xs-6">
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41269" id="LblG2145_C41269">FECHA DE ENTREGA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41269'])) {
                            echo $_GET['G2145_C41269'];
                        } ?>"  name="G2145_C41269" id="G2145_C41269" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->

                   
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

 <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C49291" id="LblG2145_C49291">FRANJA HORARIA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C49291" id="G2145_C49291">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2824 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

  
            </div> <!-- AQUIFINCAMPO -->

        </div> <!-- AQUIFINSALDO1 -->

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2145_C41306" id="LblG2145_C41306">OBSERVACIONES GESTION</label>
                        <textarea class="form-control input-sm" name="G2145_C41306" id="G2145_C41306"  value="<?php if (isset($_GET['G2145_C41306'])) {
                            echo $_GET['G2145_C41306'];
                        } ?>" placeholder="OBSERVACIONES GESTION"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

        </div> <!-- AQUIFINSALDO1 -->


</div>

<div  class="panel box box-primary" id="5949" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5949c">
                LINEA 1
            </a>
        </h4>
        
    </div>
    <div id="s_5949c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41270" id="LblG2145_C41270">OPERADOR ACTUAL (LINEA 1)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41270" id="G2145_C41270">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41271" id="LblG2145_C41271">PLAN (LINEA 1)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41271" id="G2145_C41271">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 AND LISOPC_ConsInte__b >= 53397 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41272" id="LblG2145_C41272">NIP (LINEA 1)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41272" value="<?php if (isset($_GET['G2145_C41272'])) {
                            echo $_GET['G2145_C41272'];
                        } ?>"  name="G2145_C41272"  placeholder="NIP (LINEA 1)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41273" id="LblG2145_C41273">VALOR PLAN (LINEA 1)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41273" id="G2145_C41273">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 AND LISOPC_ConsInte__b > 30390 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div>
        
        <div class="row">
            <div class="col-md-3 col-xs-3">
                <div class="form-group">
                        <label for="G2145_C56103" id="LblG2145_C56103">CONTRA ENTREGA O POSFECHADO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C56103" id="G2145_C56103">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3380 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>                    
                </div>
            </div>
        </div>


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5950" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5950c">
                LINEA 2
            </a>
        </h4>
        
    </div>
    <div id="s_5950c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41274" id="LblG2145_C41274">OPERADOR ACTUAL (LINEA 2)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41274" id="G2145_C41274">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41275" id="LblG2145_C41275">PLAN (LINEA 2)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41275" id="G2145_C41275">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41276" id="LblG2145_C41276">NIP (LINEA 2)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41276" value="<?php if (isset($_GET['G2145_C41276'])) {
                            echo $_GET['G2145_C41276'];
                        } ?>"  name="G2145_C41276"  placeholder="NIP (LINEA 2)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41277" id="LblG2145_C41277">VALOR DEL PLAN (LINEA 2)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41277" id="G2145_C41277">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5951" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5951c">
                LINEA 3
            </a>
        </h4>
        
    </div>
    <div id="s_5951c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41278" id="LblG2145_C41278">OPERADOR ACTUAL (LINEA 3)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41278" id="G2145_C41278">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41279" id="LblG2145_C41279">PLAN (LINEA 3)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41279" id="G2145_C41279">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41280" id="LblG2145_C41280">NIP (LINEA 3)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41280" value="<?php if (isset($_GET['G2145_C41280'])) {
                            echo $_GET['G2145_C41280'];
                        } ?>"  name="G2145_C41280"  placeholder="NIP (LINEA 3)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41281" id="LblG2145_C41281">VALOR DEL PLAN (LINEA 3)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41281" id="G2145_C41281">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5952" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5952c">
                LINEA 4
            </a>
        </h4>
        
    </div>
    <div id="s_5952c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41282" id="LblG2145_C41282">OPERADOR ACTUAL (LINEA 4)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41282" id="G2145_C41282">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41283" id="LblG2145_C41283">PLAN (LINEA 4)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41283" id="G2145_C41283">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41284" id="LblG2145_C41284">NIP (LINEA 4)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41284" value="<?php if (isset($_GET['G2145_C41284'])) {
                            echo $_GET['G2145_C41284'];
                        } ?>"  name="G2145_C41284"  placeholder="NIP (LINEA 4)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41285" id="LblG2145_C41285">VALOR DEL PLAN (LINEA 4)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41285" id="G2145_C41285">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5954" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5954c">
                LINEA 5
            </a>
        </h4>
        
    </div>
    <div id="s_5954c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41302" id="LblG2145_C41302">OPERADOR ACTUAL (LINEA 5)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41302" id="G2145_C41302">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41303" id="LblG2145_C41303">PLAN (LINEA 5)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41303" id="G2145_C41303">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41304" id="LblG2145_C41304">NIP (LINEA 5)</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41304" value="<?php if (isset($_GET['G2145_C41304'])) {
                            echo $_GET['G2145_C41304'];
                        } ?>"  name="G2145_C41304"  placeholder="NIP (LINEA 5)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41305" id="LblG2145_C41305">VALOR DEL PLAN (LINEA 5)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41305" id="G2145_C41305">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5953" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5953c">
                DATOS DIRECCION
            </a>
        </h4>
        
    </div>
    <div id="s_5953c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41286" id="LblG2145_C41286">PERSONA DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41286" value="<?php if (isset($_GET['G2145_C41286'])) {
                            echo $_GET['G2145_C41286'];
                        } ?>"  name="G2145_C41286"  placeholder="PERSONA DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41287" id="LblG2145_C41287">NUMERO DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41287" value="<?php if (isset($_GET['G2145_C41287'])) {
                            echo $_GET['G2145_C41287'];
                        } ?>"  name="G2145_C41287"  placeholder="NUMERO DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41298" id="LblG2145_C41298">CIUDAD</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41298" id="G2145_C41298">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2291 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41299" id="LblG2145_C41299">DEPARTAMENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41299" id="G2145_C41299">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2292 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41300" id="LblG2145_C41300">RECUADO CLIENTE</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41300" value="<?php if (isset($_GET['G2145_C41300'])) {
                            echo $_GET['G2145_C41300'];
                        } ?>"  name="G2145_C41300"  placeholder="RECUADO CLIENTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41914" id="LblG2145_C41914">NOMBRE DEL AGENTE</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41914" value="<?php if (isset($_GET['G2145_C41914'])) {
                            echo $_GET['G2145_C41914'];
                        } ?>"  name="G2145_C41914"  placeholder="NOMBRE DEL AGENTE" readonly>
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2145_C41301" id="LblG2145_C41301">OBSERVACION DIRECCION</label>
                        <textarea class="form-control input-sm" name="G2145_C41301" id="G2145_C41301"  value="<?php if (isset($_GET['G2145_C41301'])) {
                            echo $_GET['G2145_C41301'];
                        } ?>" placeholder="OBSERVACION DIRECCION"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary"  id="5989">
    <div class="box-header with-border">
        <h4 class="box-title">
            BACKOFFICE
        </h4>
        
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">SECCION DE BACKOFFICE</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


    </div>
</div>

<div  class="panel box box-primary" id="5992" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5992c">
                BACK RUTA
            </a>
        </h4>
        
    </div>
    <div id="s_5992c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41568" id="LblG2145_C41568">BACKOFFICE RUTA</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41568" value="<?php if (isset($_GET['G2145_C41568'])) {
                            echo $_GET['G2145_C41568'];
                        } ?>"  name="G2145_C41568"  placeholder="BACKOFFICE RUTA" readonly>
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41564" id="LblG2145_C41564">ENTREGA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41564" id="G2145_C41564">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2288 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41567" id="LblG2145_C41567">RECAUDO REAL</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41567" value="<?php if (isset($_GET['G2145_C41567'])) {
                            echo $_GET['G2145_C41567'];
                        } ?>"  name="G2145_C41567"  placeholder="RECAUDO REAL">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41570" id="LblG2145_C41570">VALOR DE ENVÍO</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41570" value="<?php if (isset($_GET['G2145_C41570'])) {
                            echo $_GET['G2145_C41570'];
                        } ?>"  name="G2145_C41570"  placeholder="VALOR DE ENVÍO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41601" id="LblG2145_C41601">MENSAJERO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41601" id="G2145_C41601">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2274 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41572" id="LblG2145_C41572">FECHA B.O</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41572'])) {
                            echo $_GET['G2145_C41572'];
                        } ?>"  name="G2145_C41572" id="G2145_C41572" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C49281" id="LblG2145_C49281">SIM CARD</label>
                        <input type="text" class="form-control input-sm" id="G2145_C49281" value="<?php if (isset($_GET['G2145_C49281'])) {
                            echo $_GET['G2145_C49281'];
                        } ?>"  name="G2145_C49281"  placeholder="SIM CARD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->
            
            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C55958" id="LblG2145_C55958">NÚMERO DE GUÍA</label>
                        <input type="text" class="form-control input-sm" id="G2145_C55958" value="<?php if (isset($_GET['G2145_C55958'])) {
                            echo $_GET['G2145_C55958'];
                        } ?>"  name="G2145_C55958"  placeholder="NÚMERO DE GUÍA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->
            
            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C55959" id="LblG2145_C55959">OPERADOR LOGÍSTICO</label>
                        <input type="text" class="form-control input-sm" id="G2145_C55959" value="<?php if (isset($_GET['G2145_C55959'])) {
                            echo $_GET['G2145_C55959'];
                        } ?>"  name="G2145_C55959"  placeholder="OPERADOR LOGÍSTICO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

        </div> <!-- AQUIFINSALDO1 -->
        
        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C55960" id="LblG2145_C55960">OBSERVACIONES RUTA</label>
                        <input type="text" class="form-control input-sm" id="G2145_C55960" value="<?php if (isset($_GET['G2145_C55960'])) {
                            echo $_GET['G2145_C55960'];
                        } ?>"  name="G2145_C55960"  placeholder="OBSERVACIONES RUTA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

        </div> <!-- AQUIFINSALDO1 -->        


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5993" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5993c">
                BACK FACTURACION
            </a>
        </h4>
        
    </div>
    <div id="s_5993c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C49283" id="LblG2145_C49283">BACKOFFICE FACTURACION</label>
                        <input type="text" class="form-control input-sm" id="G2145_C49283" value="<?php if (isset($_GET['G2145_C49283'])) {
                            echo $_GET['G2145_C49283'];
                        } ?>"  name="G2145_C49283"  placeholder="BACKOFFICE FACTURACION" readonly>
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41565" id="LblG2145_C41565">TIPIFICACION BO </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41565" id="G2145_C41565">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2287 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2145_C41573" id="LblG2145_C41573">CALIDAD</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2145_C41573" id="G2145_C41573">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2289 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41566" id="LblG2145_C41566">NUMERO DE FACTURA</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41566" value="<?php if (isset($_GET['G2145_C41566'])) {
                            echo $_GET['G2145_C41566'];
                        } ?>"  name="G2145_C41566"  placeholder="NUMERO DE FACTURA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41574" id="LblG2145_C41574">VALOR FACTURA</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41574" value="<?php if (isset($_GET['G2145_C41574'])) {
                            echo $_GET['G2145_C41574'];
                        } ?>"  name="G2145_C41574"  placeholder="VALOR FACTURA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41571" id="LblG2145_C41571">OPORTUNIDAD</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41571" value="<?php if (isset($_GET['G2145_C41571'])) {
                            echo $_GET['G2145_C41571'];
                        } ?>"  name="G2145_C41571"  placeholder="OPORTUNIDAD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41576" id="LblG2145_C41576">NUMERO A PORTAR</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41576" value="<?php if (isset($_GET['G2145_C41576'])) {
                            echo $_GET['G2145_C41576'];
                        } ?>"  name="G2145_C41576"  placeholder="NUMERO A PORTAR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41577" id="LblG2145_C41577">PORTABILIDAD</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41577" value="<?php if (isset($_GET['G2145_C41577'])) {
                            echo $_GET['G2145_C41577'];
                        } ?>"  name="G2145_C41577"  placeholder="PORTABILIDAD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C41578" id="LblG2145_C41578">FECHA DE PORTABILIDAD</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2145_C41578'])) {
                            echo $_GET['G2145_C41578'];
                        } ?>"  name="G2145_C41578" id="G2145_C41578" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2145_C41579" id="LblG2145_C41579">ID FACTURA</label>
                        <textarea class="form-control input-sm" name="G2145_C41579" id="G2145_C41579"  value="<?php if (isset($_GET['G2145_C41579'])) {
                            echo $_GET['G2145_C41579'];
                        } ?>" placeholder="ID FACTURA"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41581" id="LblG2145_C41581">ANEXO</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41581" value="<?php if (isset($_GET['G2145_C41581'])) {
                            echo $_GET['G2145_C41581'];
                        } ?>"  name="G2145_C41581"  placeholder="ANEXO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2145_C41583" id="LblG2145_C41583">350</label>
                        <input type="text" class="form-control input-sm" id="G2145_C41583" value="<?php if (isset($_GET['G2145_C41583'])) {
                            echo $_GET['G2145_C41583'];
                        } ?>"  name="G2145_C41583"  placeholder="350">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2145_C49282" id="LblG2145_C49282">FECHA B.O</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?=date("Y-m-d");?>"  name="G2145_C49282" id="G2145_C49282" placeholder="YYYY-MM-DD" readonly>
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->

        <div class="row">
            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2145_C41600" id="LblG2145_C41600">OBSERVACIONES BACKOFFICE</label>
                        <textarea class="form-control input-sm" name="G2145_C41600" id="G2145_C41600"  value="<?php if (isset($_GET['G2145_C41600'])) {
                            echo $_GET['G2145_C41600'];
                        } ?>" placeholder="OBSERVACIONES BACKOFFICE"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->
        </div>

        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2145/G2145_eventos.js"></script> 
<script type="text/javascript">

    function listBackChange(){

        var strNombreBack_t = '<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>';

        $("#G2145_C41564").change(function(){

            $("#G2145_C41568").val(strNombreBack_t);

        }); 

        $("#G2145_C41565").change(function(){

            $("#G2145_C49283").val(strNombreBack_t);

        });

        $("#G2145_C41573").change(function(){

            $("#G2145_C49282").val('<?php echo date('Y-m-d'); ?>');

        });

    }

    $(function(){

        <?php if (!isset($_GET["view"])) {?>
            $("#G2145_C41571").change(function(){
                if($(this).val() !=''){
                    $("#G2145_C49282").val("<?=date("Y-m-d");?>");             
                }else{
                    $("#G2145_C49282").val('');
                }
            });
        <?php } ?>; 

        var meses = new Array(12);
        meses[0] = "01";
        meses[1] = "02";
        meses[2] = "03";
        meses[3] = "04";
        meses[4] = "05";
        meses[5] = "06";
        meses[6] = "07";
        meses[7] = "08";
        meses[8] = "09";
        meses[9] = "10";
        meses[10] = "11";
        meses[11] = "12";

        var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            
        $("#G2145_C52335").numeric();
        $("#G2145_C41251").select2();
        $("#G2145_C41254").select2();
        $("#G2145_C41260").select2();
        $("#G2145_C41266").select2();
        $("#G2145_C49291").select2();
        $("#G2145_C41270").select2();
        $("#G2145_C41271").select2();
        $("#G2145_C41273").select2();
        $("#G2145_C56103").select2();
        $("#G2145_C41274").select2();
        $("#G2145_C41275").select2();
        $("#G2145_C41277").select2();
        $("#G2145_C41278").select2();
        $("#G2145_C41279").select2();
        $("#G2145_C41281").select2();
        $("#G2145_C41282").select2();
        $("#G2145_C41283").select2();
        $("#G2145_C41285").select2();
        $("#G2145_C41302").select2();
        $("#G2145_C41303").select2();
        $("#G2145_C41305").select2();
        $("#G2145_C41298").select2();
        $("#G2145_C41299").select2();
        // $("#G2145_C41564").select2();
        $("#G2145_C41601").select2();
        // $("#G2145_C41565").select2();
        // $("#G2145_C41573").select2();

            
        var arrDatePicker_t = {language: "es",autoclose: true,todayHighlight: true};

        $("#G2145_C41257").datepicker(arrDatePicker_t);$("#G2145_C41258").datepicker(arrDatePicker_t);$("#G2145_C41263").datepicker(arrDatePicker_t);$("#G2145_C41264").datepicker(arrDatePicker_t);$("#G2145_C41268").datepicker(arrDatePicker_t);$("#G2145_C41269").datepicker(arrDatePicker_t);$("#G2145_C41572").datepicker(arrDatePicker_t);$("#G2145_C41578").datepicker(arrDatePicker_t);


        //function para CIUDAD 1 

        $("#G2145_C41251").change(function(){});$("#G2145_C41254").change(function(){});$("#G2145_C41260").change(function(){});$("#G2145_C41266").change(function(){});$("#G2145_C49291").change(function(){});$("#G2145_C41270").change(function(){});$("#G2145_C41271").change(function(){});$("#G2145_C41273").change(function(){});$("#G2145_C56103").change(function(){});$("#G2145_C41274").change(function(){});$("#G2145_C41275").change(function(){});$("#G2145_C41277").change(function(){});$("#G2145_C41278").change(function(){});$("#G2145_C41279").change(function(){});$("#G2145_C41281").change(function(){});$("#G2145_C41282").change(function(){});$("#G2145_C41283").change(function(){});$("#G2145_C41285").change(function(){});$("#G2145_C41302").change(function(){});$("#G2145_C41303").change(function(){});$("#G2145_C41305").change(function(){});$("#G2145_C41298").change(function(){});

        //function para DEPARTAMENTO 

        $("#G2145_C41299").change(function(){  
            //Esto es la parte de las listas dependientes
            

            $.ajax({
                url    : '<?php echo $url_crud; ?>',
                type   : 'post',
                data   : { getListaHija : true , opcionID : '2291' , idPadre : $(this).val() },
                success : function(data){
                    var optG2145_C41298 = $("#G2145_C41298").attr("opt");
                    $("#G2145_C41298").html(data);
                    if (optG2145_C41298 != null) {
                        $("#G2145_C41298").val(optG2145_C41298).trigger("change");
                    }
                }
            });
            
        });

        //function para ENTREGA 

        $("#G2145_C41601").change(function(){});

        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            

                                                $("#G2145_C41248").val(item.G2145_C41248);

                                                $("#G2145_C41249").val(item.G2145_C41249);

                                                $("#G2145_C41250").val(item.G2145_C41250);

                                                $("#G2145_C41251").val(item.G2145_C41251).trigger("change"); 

                                                $("#G2145_C41252").val(item.G2145_C41252);

                                                $("#G2145_C41253").val(item.G2145_C41253);

                                                $("#G2145_C41254").val(item.G2145_C41254).trigger("change"); 

                                                $("#G2145_C41159").val(item.G2145_C41159);

                                                $("#G2145_C41160").val(item.G2145_C41160);

                                                $("#G2145_C41255").val(item.G2145_C41255);

                                                $("#G2145_C41256").val(item.G2145_C41256);

                                                $("#G2145_C41257").val(item.G2145_C41257);

                                                $("#G2145_C41258").val(item.G2145_C41258);

                                                $("#G2145_C41259").val(item.G2145_C41259);

                                                $("#G2145_C41260").val(item.G2145_C41260).trigger("change"); 

                                                $("#G2145_C41261").val(item.G2145_C41261);

                                                $("#G2145_C41262").val(item.G2145_C41262);

                                                $("#G2145_C41263").val(item.G2145_C41263);

                                                $("#G2145_C41264").val(item.G2145_C41264);

                                                $("#G2145_C41265").val(item.G2145_C41265);

                                                $("#G2145_C41266").val(item.G2145_C41266).trigger("change"); 

                                                $("#G2145_C41267").val(item.G2145_C41267);

                                                $("#G2145_C41268").val(item.G2145_C41268);

                                                $("#G2145_C49291").val(item.G2145_C49291).trigger("change"); 

                                                $("#G2145_C41269").val(item.G2145_C41269);

                                                $("#G2145_C41306").val(item.G2145_C41306);

                                                $("#G2145_C41270").val(item.G2145_C41270).trigger("change"); 
                             
                                                $("#G2145_C41271").val(item.G2145_C41271).trigger("change"); 

                                                $("#G2145_C41272").val(item.G2145_C41272);

                                                $("#G2145_C41273").val(item.G2145_C41273).trigger("change");
                                                                            
                                                $("#G2145_C56103").val(item.G2145_C56103).trigger("change"); 
                             
                                                $("#G2145_C41274").val(item.G2145_C41274).trigger("change"); 
                             
                                                $("#G2145_C41275").val(item.G2145_C41275).trigger("change"); 

                                                $("#G2145_C41276").val(item.G2145_C41276);

                                                $("#G2145_C41277").val(item.G2145_C41277).trigger("change"); 
                             
                                                $("#G2145_C41278").val(item.G2145_C41278).trigger("change"); 
                             
                                                $("#G2145_C41279").val(item.G2145_C41279).trigger("change"); 

                                                $("#G2145_C41280").val(item.G2145_C41280);

                                                $("#G2145_C41281").val(item.G2145_C41281).trigger("change"); 
                             
                                                $("#G2145_C41282").val(item.G2145_C41282).trigger("change"); 
                             
                                                $("#G2145_C41283").val(item.G2145_C41283).trigger("change"); 

                                                $("#G2145_C41284").val(item.G2145_C41284);

                                                $("#G2145_C41285").val(item.G2145_C41285).trigger("change"); 

                                                $("#G2145_C41286").val(item.G2145_C41286);

                                                $("#G2145_C41287").val(item.G2145_C41287);

                                                $("#G2145_C41298").attr("opt",item.G2145_C41298); 
                             
                                                $("#G2145_C41299").val(item.G2145_C41299).trigger("change"); 

                                                $("#G2145_C41300").val(item.G2145_C41300);

                                                $("#G2145_C41301").val(item.G2145_C41301);

                                                $("#G2145_C41914").val(item.G2145_C41914);

                                                $("#G2145_C49283").val(item.G2145_C49283);

                                                $("#G2145_C41302").val(item.G2145_C41302).trigger("change"); 
                             
                                                $("#G2145_C41303").val(item.G2145_C41303).trigger("change"); 

                                                $("#G2145_C41304").val(item.G2145_C41304);

                                                $("#G2145_C41305").val(item.G2145_C41305).trigger("change"); 
      
                                                if(item.G2145_C41563 == 1){
                                                   $("#G2145_C41563").attr('checked', true);
                                                } 

                                                $("#G2145_C41568").val(item.G2145_C41568);

                                                $("#G2145_C41564").val(item.G2145_C41564); 

                                                $("#G2145_C41567").val(item.G2145_C41567);

                                                $("#G2145_C41570").val(item.G2145_C41570);

                                                $("#G2145_C41601").val(item.G2145_C41601).trigger("change"); 

                                                $("#G2145_C41572").val(item.G2145_C41572);

                                                $("#G2145_C49281").val(item.G2145_C49281);
                                                
                                                $("#G2145_C55958").val(item.G2145_C55958);
                                                
                                                $("#G2145_C55959").val(item.G2145_C55959);
                                                
                                                $("#G2145_C55960").val(item.G2145_C55960);


                                                $("#G2145_C41565").val(item.G2145_C41565); 
                                                // $("#G2145_C41565 option[value="+item.G2145_C41565+"]").attr("selected",true); 

                             
                                                $("#G2145_C41573").val(item.G2145_C41573); 

                                                $("#G2145_C41566").val(item.G2145_C41566);

                                                $("#G2145_C41574").val(item.G2145_C41574);

                                                $("#G2145_C41571").val(item.G2145_C41571);

                                                $("#G2145_C41576").val(item.G2145_C41576);

                                                $("#G2145_C41577").val(item.G2145_C41577);

                                                $("#G2145_C41578").val(item.G2145_C41578);

                                                $("#G2145_C41579").val(item.G2145_C41579);

                                                $("#G2145_C41581").val(item.G2145_C41581);

                                                $("#G2145_C41583").val(item.G2145_C41583);

                                                $("#G2145_C49282").val(item.G2145_C49282);

                                                $("#G2145_C41600").val(item.G2145_C41600);

                                                $("#G2145_C52335").val(item.G2145_C52335);
                                                $("#G2145_C52828").val(item.G2145_C52828);

                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });

    });


    <?php if(!isset($_GET['view'])) { ?>

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                        $.each(data, function(i, item) {
                            

                            $("#G2145_C41248").val(item.G2145_C41248);

                            $("#G2145_C41249").val(item.G2145_C41249);

                            $("#G2145_C41250").val(item.G2145_C41250);
     
                            $("#G2145_C41251").val(item.G2145_C41251).trigger("change"); 

                            $("#G2145_C41252").val(item.G2145_C41252);

                            $("#G2145_C41253").val(item.G2145_C41253);
     
                            $("#G2145_C41254").val(item.G2145_C41254).trigger("change"); 

                            $("#G2145_C41159").val(item.G2145_C41159);

                            $("#G2145_C41160").val(item.G2145_C41160);

                            $("#G2145_C41255").val(item.G2145_C41255);

                            $("#G2145_C41256").val(item.G2145_C41256);

                            $("#G2145_C41257").val(item.G2145_C41257);

                            $("#G2145_C41258").val(item.G2145_C41258);

                            $("#G2145_C41259").val(item.G2145_C41259);
     
                            $("#G2145_C41260").val(item.G2145_C41260).trigger("change"); 

                            $("#G2145_C41261").val(item.G2145_C41261);

                            $("#G2145_C41262").val(item.G2145_C41262);

                            $("#G2145_C41263").val(item.G2145_C41263);

                            $("#G2145_C41264").val(item.G2145_C41264);

                            $("#G2145_C41265").val(item.G2145_C41265);
     
                            $("#G2145_C41266").val(item.G2145_C41266).trigger("change"); 

                            $("#G2145_C41267").val(item.G2145_C41267);

                            $("#G2145_C41268").val(item.G2145_C41268);
     
                            $("#G2145_C49291").val(item.G2145_C49291).trigger("change"); 

                            $("#G2145_C41269").val(item.G2145_C41269);

                            $("#G2145_C41306").val(item.G2145_C41306);
     
                            $("#G2145_C41270").val(item.G2145_C41270).trigger("change"); 
         
                            $("#G2145_C41271").val(item.G2145_C41271).trigger("change"); 

                            $("#G2145_C41272").val(item.G2145_C41272);
     
                            $("#G2145_C41273").val(item.G2145_C41273).trigger("change");
                                
                            $("#G2145_C56103").val(item.G2145_C56103).trigger("change"); 
         
                            $("#G2145_C41274").val(item.G2145_C41274).trigger("change"); 
         
                            $("#G2145_C41275").val(item.G2145_C41275).trigger("change"); 

                            $("#G2145_C41276").val(item.G2145_C41276);
     
                            $("#G2145_C41277").val(item.G2145_C41277).trigger("change"); 
         
                            $("#G2145_C41278").val(item.G2145_C41278).trigger("change"); 
         
                            $("#G2145_C41279").val(item.G2145_C41279).trigger("change"); 

                            $("#G2145_C41280").val(item.G2145_C41280);
     
                            $("#G2145_C41281").val(item.G2145_C41281).trigger("change"); 
         
                            $("#G2145_C41282").val(item.G2145_C41282).trigger("change"); 
         
                            $("#G2145_C41283").val(item.G2145_C41283).trigger("change"); 

                            $("#G2145_C41284").val(item.G2145_C41284);
     
                            $("#G2145_C41285").val(item.G2145_C41285).trigger("change"); 

                            $("#G2145_C41286").val(item.G2145_C41286);

                            $("#G2145_C41287").val(item.G2145_C41287);
     
                            $("#G2145_C41298").attr("opt",item.G2145_C41298); 
         
                            $("#G2145_C41299").val(item.G2145_C41299).trigger("change"); 

                            $("#G2145_C41300").val(item.G2145_C41300);

                            $("#G2145_C41301").val(item.G2145_C41301);

                            $("#G2145_C41914").val(item.G2145_C41914);
     
                            $("#G2145_C41302").val(item.G2145_C41302).trigger("change"); 
         
                            $("#G2145_C41303").val(item.G2145_C41303).trigger("change"); 

                            $("#G2145_C41304").val(item.G2145_C41304);
     
                            $("#G2145_C41305").val(item.G2145_C41305).trigger("change"); 
        
                            if(item.G2145_C41563 == 1){
                               $("#G2145_C41563").attr('checked', true);
                            } 

                            $("#G2145_C41568").val(item.G2145_C41568);
     
                            // $("#G2145_C41564").val(item.G2145_C41564).trigger("change"); 
                            $("#G2145_C41564").val(item.G2145_C41564);
                            // $("#G2145_C41564 option[value='"+item.G2145_C41564+"']").attr("selected",true); 

                            $("#G2145_C41567").val(item.G2145_C41567);

                            $("#G2145_C41570").val(item.G2145_C41570);
     
                            $("#G2145_C41601").val(item.G2145_C41601).trigger("change"); 

                            $("#G2145_C41572").val(item.G2145_C41572);

                            $("#G2145_C49281").val(item.G2145_C49281);
                            
                            $("#G2145_C55958").val(item.G2145_C55958);
                            
                            $("#G2145_C55959").val(item.G2145_C55959);
                            
                            $("#G2145_C55960").val(item.G2145_C55960);

                            $("#G2145_C49283").val(item.G2145_C49283);
     
                            $("#G2145_C41565").val(item.G2145_C41565);
                            // $("#G2145_C41565").val(item.G2145_C41565).trigger("change"); 
                            // $("#G2145_C41565 option[value='0']").attr("selected",true); 
                            // $("#G2145_C41565 option[value='"+item.G2145_C41565+"']").attr("selected",true); 
     
                            $("#G2145_C41573").val(item.G2145_C41573); 

                            $("#G2145_C41566").val(item.G2145_C41566);

                            $("#G2145_C41574").val(item.G2145_C41574);

                            $("#G2145_C41571").val(item.G2145_C41571);

                            $("#G2145_C41576").val(item.G2145_C41576);

                            $("#G2145_C41577").val(item.G2145_C41577);

                            $("#G2145_C41578").val(item.G2145_C41578);

                            $("#G2145_C41579").val(item.G2145_C41579);

                            $("#G2145_C41581").val(item.G2145_C41581);

                            $("#G2145_C41583").val(item.G2145_C41583);

                            $("#G2145_C49282").val(item.G2145_C49282);

                            $("#G2145_C41600").val(item.G2145_C41600);

                            $("#G2145_C52335").val(item.G2145_C52335);

                            $("#G2145_C52828").val(item.G2145_C52828);
                            
                            $("#h3mio").html(item.principal);
                            
                        });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    listBackChange();
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }
    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
