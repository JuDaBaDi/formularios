<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2655;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2655
                  SET G2655_C = -201
                  WHERE G2655_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2655 
                    WHERE G2655_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2655_C"]) || $gestion["G2655_C"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2655_C"];
        }

        if (is_null($gestion["G2655_C"]) || $gestion["G2655_C"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2655_C"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2655_FechaInsercion"]."',".$gestion["G2655_Usuario"].",'".$gestion["G2655_C".$P["P"]]."','".$gestion["G2655_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "clientes.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2655 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2655_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2655_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 51843")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 51843");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2655_LinkContenido as url FROM ".$BaseDatos.".G2655 WHERE G2655_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2655_ConsInte__b, G2655_FechaInsercion , G2655_Usuario ,  G2655_CodigoMiembro  , G2655_PoblacionOrigen , G2655_EstadoDiligenciamiento ,  G2655_IdLlamada , G2655_C51827 as principal ,G2655_C51827,G2655_C51828,G2655_C51829,G2655_C51830,G2655_C51831,G2655_C51832,G2655_C51833,G2655_C51834,G2655_C51835,G2655_C52261,G2655_C52263,G2655_C52264,G2655_C52266,G2655_C52267,G2655_C52269,G2655_C52270,G2655_C52272,G2655_C52333,G2655_C51837,G2655_C51838,G2655_C51839,G2655_C51841,G2655_C51842,G2655_C51843,G2655_C51845,G2655_C51846,G2655_C51847,G2655_C51848,G2655_C51849,G2655_C51850,G2655_C51851,G2655_C51852,G2655_C51853 FROM '.$BaseDatos.'.G2655 WHERE G2655_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2655_C51827'] = $key->G2655_C51827;

                $datos[$i]['G2655_C51828'] = $key->G2655_C51828;

                $datos[$i]['G2655_C51829'] = $key->G2655_C51829;

                $datos[$i]['G2655_C51830'] = $key->G2655_C51830;

                $datos[$i]['G2655_C51831'] = $key->G2655_C51831;

                $datos[$i]['G2655_C51832'] = $key->G2655_C51832;

                $datos[$i]['G2655_C51833'] = $key->G2655_C51833;

                $datos[$i]['G2655_C51834'] = $key->G2655_C51834;

                $datos[$i]['G2655_C51835'] = $key->G2655_C51835;

                $datos[$i]['G2655_C52261'] = $key->G2655_C52261;

                $datos[$i]['G2655_C52263'] = $key->G2655_C52263;

                $datos[$i]['G2655_C52264'] = $key->G2655_C52264;

                $datos[$i]['G2655_C52266'] = $key->G2655_C52266;

                $datos[$i]['G2655_C52267'] = $key->G2655_C52267;

                $datos[$i]['G2655_C52269'] = $key->G2655_C52269;

                $datos[$i]['G2655_C52270'] = $key->G2655_C52270;

                $datos[$i]['G2655_C52272'] = $key->G2655_C52272;

                $datos[$i]['G2655_C52333'] = $key->G2655_C52333;

                $datos[$i]['G2655_C51837'] = $key->G2655_C51837;

                $datos[$i]['G2655_C51838'] = $key->G2655_C51838;

                $datos[$i]['G2655_C51839'] = $key->G2655_C51839;

                $datos[$i]['G2655_C51841'] = $key->G2655_C51841;

                $datos[$i]['G2655_C51842'] = $key->G2655_C51842;

                $datos[$i]['G2655_C51843'] = $key->G2655_C51843;

                $datos[$i]['G2655_C51845'] = $key->G2655_C51845;

                $datos[$i]['G2655_C51846'] = $key->G2655_C51846;

                $datos[$i]['G2655_C51847'] = explode(' ', $key->G2655_C51847)[0];
  
                $hora = '';
                if(!is_null($key->G2655_C51848)){
                    $hora = explode(' ', $key->G2655_C51848)[1];
                }

                $datos[$i]['G2655_C51848'] = $hora;

                $datos[$i]['G2655_C51849'] = $key->G2655_C51849;

                $datos[$i]['G2655_C51850'] = $key->G2655_C51850;

                $datos[$i]['G2655_C51851'] = $key->G2655_C51851;

                $datos[$i]['G2655_C51852'] = $key->G2655_C51852;

                $datos[$i]['G2655_C51853'] = $key->G2655_C51853;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2655";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2655_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2655_ConsInte__b as id,  G2655_C51827 as camp1 , G2655_C51828 as camp2 
                     FROM ".$BaseDatos.".G2655  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2655_ConsInte__b as id,  G2655_C51827 as camp1 , G2655_C51828 as camp2  
                    FROM ".$BaseDatos.".G2655  JOIN ".$BaseDatos.".G2655_M".$_POST['muestra']." ON G2655_ConsInte__b = G2655_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2655_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2655_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2655_C51827 LIKE '%".$B."%' OR G2655_C51828 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2655_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2655");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2655_ConsInte__b, G2655_FechaInsercion , G2655_Usuario ,  G2655_CodigoMiembro  , G2655_PoblacionOrigen , G2655_EstadoDiligenciamiento ,  G2655_IdLlamada , G2655_C51827 as principal ,G2655_C51827,G2655_C51828,G2655_C51829,G2655_C51830,G2655_C51831,G2655_C51832,G2655_C51833,G2655_C51834,G2655_C51835,G2655_C52261, a.LISOPC_Nombre____b as G2655_C52263,G2655_C52264, b.LISOPC_Nombre____b as G2655_C52266,G2655_C52267, c.LISOPC_Nombre____b as G2655_C52269,G2655_C52270, d.LISOPC_Nombre____b as G2655_C52272,G2655_C52333,G2655_C51837,G2655_C51838,G2655_C51839,G2655_C51841,G2655_C51842,G2655_C51843, e.LISOPC_Nombre____b as G2655_C51845, f.LISOPC_Nombre____b as G2655_C51846,G2655_C51847,G2655_C51848,G2655_C51849,G2655_C51850,G2655_C51851,G2655_C51852,G2655_C51853 FROM '.$BaseDatos.'.G2655 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2655_C52263 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2655_C52266 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2655_C52269 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2655_C52272 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2655_C51845 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2655_C51846';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2655_C51848)){
                    $hora_a = explode(' ', $fila->G2655_C51848)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2655_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2655_ConsInte__b , ($fila->G2655_C51827) , ($fila->G2655_C51828) , ($fila->G2655_C51829) , ($fila->G2655_C51830) , ($fila->G2655_C51831) , ($fila->G2655_C51832) , ($fila->G2655_C51833) , ($fila->G2655_C51834) , ($fila->G2655_C51835) , ($fila->G2655_C52261) , ($fila->G2655_C52263) , ($fila->G2655_C52264) , ($fila->G2655_C52266) , ($fila->G2655_C52267) , ($fila->G2655_C52269) , ($fila->G2655_C52270) , ($fila->G2655_C52272) , ($fila->G2655_C52333) , ($fila->G2655_C51837) , ($fila->G2655_C51838) , ($fila->G2655_C51839) , ($fila->G2655_C51841) , ($fila->G2655_C51842) , ($fila->G2655_C51843) , ($fila->G2655_C51845) , ($fila->G2655_C51846) , explode(' ', $fila->G2655_C51847)[0] , $hora_a , ($fila->G2655_C51849) , ($fila->G2655_C51850) , ($fila->G2655_C51851) , ($fila->G2655_C51852) , ($fila->G2655_C51853) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2655 WHERE G2655_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2655";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2655_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2655_ConsInte__b as id,  G2655_C51827 as camp1 , G2655_C51828 as camp2  FROM '.$BaseDatos.'.G2655 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2655_ConsInte__b as id,  G2655_C51827 as camp1 , G2655_C51828 as camp2  
                    FROM ".$BaseDatos.".G2655  JOIN ".$BaseDatos.".G2655_M".$_POST['muestra']." ON G2655_ConsInte__b = G2655_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2655_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2655_C51827 LIKE "%'.$B.'%" OR G2655_C51828 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2655_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2655 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2655(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2655_C51827"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51827 = '".$_POST["G2655_C51827"]."'";
                $LsqlI .= $separador."G2655_C51827";
                $LsqlV .= $separador."'".$_POST["G2655_C51827"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51828"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51828 = '".$_POST["G2655_C51828"]."'";
                $LsqlI .= $separador."G2655_C51828";
                $LsqlV .= $separador."'".$_POST["G2655_C51828"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51829"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51829 = '".$_POST["G2655_C51829"]."'";
                $LsqlI .= $separador."G2655_C51829";
                $LsqlV .= $separador."'".$_POST["G2655_C51829"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51830"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51830 = '".$_POST["G2655_C51830"]."'";
                $LsqlI .= $separador."G2655_C51830";
                $LsqlV .= $separador."'".$_POST["G2655_C51830"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51831"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51831 = '".$_POST["G2655_C51831"]."'";
                $LsqlI .= $separador."G2655_C51831";
                $LsqlV .= $separador."'".$_POST["G2655_C51831"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51832"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51832 = '".$_POST["G2655_C51832"]."'";
                $LsqlI .= $separador."G2655_C51832";
                $LsqlV .= $separador."'".$_POST["G2655_C51832"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51833"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51833 = '".$_POST["G2655_C51833"]."'";
                $LsqlI .= $separador."G2655_C51833";
                $LsqlV .= $separador."'".$_POST["G2655_C51833"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51834"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51834 = '".$_POST["G2655_C51834"]."'";
                $LsqlI .= $separador."G2655_C51834";
                $LsqlV .= $separador."'".$_POST["G2655_C51834"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51835"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51835 = '".$_POST["G2655_C51835"]."'";
                $LsqlI .= $separador."G2655_C51835";
                $LsqlV .= $separador."'".$_POST["G2655_C51835"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52261"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52261 = '".$_POST["G2655_C52261"]."'";
                $LsqlI .= $separador."G2655_C52261";
                $LsqlV .= $separador."'".$_POST["G2655_C52261"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52263"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52263 = '".$_POST["G2655_C52263"]."'";
                $LsqlI .= $separador."G2655_C52263";
                $LsqlV .= $separador."'".$_POST["G2655_C52263"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52264"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52264 = '".$_POST["G2655_C52264"]."'";
                $LsqlI .= $separador."G2655_C52264";
                $LsqlV .= $separador."'".$_POST["G2655_C52264"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52266"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52266 = '".$_POST["G2655_C52266"]."'";
                $LsqlI .= $separador."G2655_C52266";
                $LsqlV .= $separador."'".$_POST["G2655_C52266"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52267"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52267 = '".$_POST["G2655_C52267"]."'";
                $LsqlI .= $separador."G2655_C52267";
                $LsqlV .= $separador."'".$_POST["G2655_C52267"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52269"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52269 = '".$_POST["G2655_C52269"]."'";
                $LsqlI .= $separador."G2655_C52269";
                $LsqlV .= $separador."'".$_POST["G2655_C52269"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52270"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52270 = '".$_POST["G2655_C52270"]."'";
                $LsqlI .= $separador."G2655_C52270";
                $LsqlV .= $separador."'".$_POST["G2655_C52270"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52272"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52272 = '".$_POST["G2655_C52272"]."'";
                $LsqlI .= $separador."G2655_C52272";
                $LsqlV .= $separador."'".$_POST["G2655_C52272"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C52333"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C52333 = '".$_POST["G2655_C52333"]."'";
                $LsqlI .= $separador."G2655_C52333";
                $LsqlV .= $separador."'".$_POST["G2655_C52333"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51837"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51837 = '".$_POST["G2655_C51837"]."'";
                $LsqlI .= $separador."G2655_C51837";
                $LsqlV .= $separador."'".$_POST["G2655_C51837"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51838"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51838 = '".$_POST["G2655_C51838"]."'";
                $LsqlI .= $separador."G2655_C51838";
                $LsqlV .= $separador."'".$_POST["G2655_C51838"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51839"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51839 = '".$_POST["G2655_C51839"]."'";
                $LsqlI .= $separador."G2655_C51839";
                $LsqlV .= $separador."'".$_POST["G2655_C51839"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51841"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51841 = '".$_POST["G2655_C51841"]."'";
                $LsqlI .= $separador."G2655_C51841";
                $LsqlV .= $separador."'".$_POST["G2655_C51841"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51842"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51842 = '".$_POST["G2655_C51842"]."'";
                $LsqlI .= $separador."G2655_C51842";
                $LsqlV .= $separador."'".$_POST["G2655_C51842"]."'";
                $validar = 1;
            }
             
  
            $G2655_C51843 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2655_C51843"])){
                if($_POST["G2655_C51843"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2655_C51843 = $_POST["G2655_C51843"];
                    $LsqlU .= $separador." G2655_C51843 = ".$G2655_C51843."";
                    $LsqlI .= $separador." G2655_C51843";
                    $LsqlV .= $separador.$G2655_C51843;
                    $validar = 1;
                }
            }
 
            $G2655_C51845 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2655_C51845 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2655_C51845 = ".$G2655_C51845;
                    $LsqlI .= $separador." G2655_C51845";
                    $LsqlV .= $separador.$G2655_C51845;
                    $validar = 1;

                    
                }
            }
 
            $G2655_C51846 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2655_C51846 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2655_C51846 = ".$G2655_C51846;
                    $LsqlI .= $separador." G2655_C51846";
                    $LsqlV .= $separador.$G2655_C51846;
                    $validar = 1;
                }
            }
 
            $G2655_C51847 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2655_C51847 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2655_C51847 = ".$G2655_C51847;
                    $LsqlI .= $separador." G2655_C51847";
                    $LsqlV .= $separador.$G2655_C51847;
                    $validar = 1;
                }
            }
 
            $G2655_C51848 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2655_C51848 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2655_C51848 = ".$G2655_C51848;
                    $LsqlI .= $separador." G2655_C51848";
                    $LsqlV .= $separador.$G2655_C51848;
                    $validar = 1;
                }
            }
 
            $G2655_C51849 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2655_C51849 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2655_C51849 = ".$G2655_C51849;
                    $LsqlI .= $separador." G2655_C51849";
                    $LsqlV .= $separador.$G2655_C51849;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2655_C51850"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51850 = '".$_POST["G2655_C51850"]."'";
                $LsqlI .= $separador."G2655_C51850";
                $LsqlV .= $separador."'".$_POST["G2655_C51850"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51851"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51851 = '".$_POST["G2655_C51851"]."'";
                $LsqlI .= $separador."G2655_C51851";
                $LsqlV .= $separador."'".$_POST["G2655_C51851"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51852"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51852 = '".$_POST["G2655_C51852"]."'";
                $LsqlI .= $separador."G2655_C51852";
                $LsqlV .= $separador."'".$_POST["G2655_C51852"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51853"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51853 = '".$_POST["G2655_C51853"]."'";
                $LsqlI .= $separador."G2655_C51853";
                $LsqlV .= $separador."'".$_POST["G2655_C51853"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51854"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51854 = '".$_POST["G2655_C51854"]."'";
                $LsqlI .= $separador."G2655_C51854";
                $LsqlV .= $separador."'".$_POST["G2655_C51854"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2655_C51855"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_C51855 = '".$_POST["G2655_C51855"]."'";
                $LsqlI .= $separador."G2655_C51855";
                $LsqlV .= $separador."'".$_POST["G2655_C51855"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2655_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2655_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2655_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2655_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2655_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2655_Usuario , G2655_FechaInsercion, G2655_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2655_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2655 WHERE G2655_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){

                //JDBD - Arreglar links.

                $strUpd_t = "update dyalogo_telefonia.dy_llamadas_espejo join DYALOGOCRM_WEB.G2655 on unique_id = G2655_IdLlamada join DYALOGOCRM_SISTEMA.USUARI on USUARI_ConsInte__b = G2655_Usuario join DYALOGOCRM_SISTEMA.ESTPAS on ESTPAS_ConsInte__b = G2655_Paso set agente = USUARI_Nombre____b, campana = ESTPAS_Comentari_b where agente is null or agente is null";

                $mysqli->query($strUpd_t);

                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $UltimoID;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
