<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2646_ConsInte__b, G2646_FechaInsercion , G2646_Usuario ,  G2646_CodigoMiembro  , G2646_PoblacionOrigen , G2646_EstadoDiligenciamiento ,  G2646_IdLlamada , G2646_C51643 as principal ,G2646_C51643,G2646_C51644,G2646_C51645,G2646_C51646,G2646_C51647,G2646_C51648,G2646_C51649,G2646_C51650,G2646_C51651,G2646_C51652,G2646_C51653,G2646_C51654,G2646_C51637,G2646_C51638,G2646_C51639,G2646_C51668,G2646_C51669,G2646_C51670,G2646_C51671,G2646_C51672,G2646_C51673,G2646_C51674,G2646_C55955,G2646_C55956,G2646_C55957,G2646_C52386,G2646_C52387,G2646_C52388,G2646_C52389,G2646_C52390,G2646_C52391,G2646_C55902 FROM '.$BaseDatos.'.G2646 WHERE G2646_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2646_C51643'] = $key->G2646_C51643;

                $datos[$i]['G2646_C51644'] = $key->G2646_C51644;

                $datos[$i]['G2646_C51645'] = $key->G2646_C51645;

                $datos[$i]['G2646_C51646'] = $key->G2646_C51646;

                $datos[$i]['G2646_C51647'] = $key->G2646_C51647;

                $datos[$i]['G2646_C51648'] = $key->G2646_C51648;

                $datos[$i]['G2646_C51649'] = $key->G2646_C51649;

                $datos[$i]['G2646_C51650'] = $key->G2646_C51650;

                $datos[$i]['G2646_C51651'] = $key->G2646_C51651;

                $datos[$i]['G2646_C51652'] = $key->G2646_C51652;

                $datos[$i]['G2646_C51653'] = $key->G2646_C51653;

                $datos[$i]['G2646_C51654'] = $key->G2646_C51654;

                $datos[$i]['G2646_C51637'] = $key->G2646_C51637;

                $datos[$i]['G2646_C51638'] = $key->G2646_C51638;

                $datos[$i]['G2646_C51639'] = $key->G2646_C51639;

                $datos[$i]['G2646_C51668'] = $key->G2646_C51668;

                $datos[$i]['G2646_C51669'] = $key->G2646_C51669;

                $datos[$i]['G2646_C51670'] = $key->G2646_C51670;

                $datos[$i]['G2646_C51671'] = $key->G2646_C51671;

                $datos[$i]['G2646_C51672'] = $key->G2646_C51672;

                $datos[$i]['G2646_C51673'] = $key->G2646_C51673;

                $datos[$i]['G2646_C51674'] = $key->G2646_C51674;

                $datos[$i]['G2646_C55955'] = $key->G2646_C55955;

                $datos[$i]['G2646_C55956'] = $key->G2646_C55956;

                $datos[$i]['G2646_C55957'] = $key->G2646_C55957;

                $datos[$i]['G2646_C52386'] = $key->G2646_C52386;

                $datos[$i]['G2646_C52387'] = $key->G2646_C52387;

                $datos[$i]['G2646_C52388'] = $key->G2646_C52388;

                $datos[$i]['G2646_C52389'] = $key->G2646_C52389;

                $datos[$i]['G2646_C52390'] = explode(' ', $key->G2646_C52390)[0];

                $datos[$i]['G2646_C52391'] = explode(' ', $key->G2646_C52391)[0];

                $datos[$i]['G2646_C55902'] = $key->G2646_C55902;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2646";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2646_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2646_ConsInte__b as id,  G2646_C51643 as camp1 , G2646_C51643 as camp2 
                     FROM ".$BaseDatos.".G2646  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2646_ConsInte__b as id,  G2646_C51643 as camp1 , G2646_C51643 as camp2  
                    FROM ".$BaseDatos.".G2646  JOIN ".$BaseDatos.".G2646_M".$_POST['muestra']." ON G2646_ConsInte__b = G2646_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2646_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2646_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2646_C51643 LIKE '%".$B."%' OR G2646_C51643 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2646_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2646");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2646_ConsInte__b, G2646_FechaInsercion , G2646_Usuario ,  G2646_CodigoMiembro  , G2646_PoblacionOrigen , G2646_EstadoDiligenciamiento ,  G2646_IdLlamada , G2646_C51643 as principal ,G2646_C51643,G2646_C51644,G2646_C51645,G2646_C51646,G2646_C51647,G2646_C51648,G2646_C51649,G2646_C51650,G2646_C51651,G2646_C51652,G2646_C51653,G2646_C51654,G2646_C51637,G2646_C51638, a.LISOPC_Nombre____b as G2646_C51639, b.LISOPC_Nombre____b as G2646_C51668,G2646_C51669,G2646_C51670,G2646_C51671,G2646_C51672,G2646_C51673,G2646_C51674,G2646_C55955,G2646_C55956,G2646_C55957, c.LISOPC_Nombre____b as G2646_C52386, d.LISOPC_Nombre____b as G2646_C52387, e.LISOPC_Nombre____b as G2646_C52388,G2646_C52389,G2646_C52390,G2646_C52391,G2646_C55902 FROM '.$BaseDatos.'.G2646 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2646_C51639 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2646_C51668 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2646_C52386 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2646_C52387 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2646_C52388';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2646_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2646_ConsInte__b , ($fila->G2646_C51643) , ($fila->G2646_C51644) , ($fila->G2646_C51645) , ($fila->G2646_C51646) , ($fila->G2646_C51647) , ($fila->G2646_C51648) , ($fila->G2646_C51649) , ($fila->G2646_C51650) , ($fila->G2646_C51651) , ($fila->G2646_C51652) , ($fila->G2646_C51653) , ($fila->G2646_C51654) , ($fila->G2646_C51637) , ($fila->G2646_C51638) , ($fila->G2646_C51639) , ($fila->G2646_C51668) , ($fila->G2646_C51669) , ($fila->G2646_C51670) , ($fila->G2646_C51671) , ($fila->G2646_C51672) , ($fila->G2646_C51673) , ($fila->G2646_C51674) , ($fila->G2646_C55955) , ($fila->G2646_C55956) , ($fila->G2646_C55957) , ($fila->G2646_C52386) , ($fila->G2646_C52387) , ($fila->G2646_C52388) , ($fila->G2646_C52389) , explode(' ', $fila->G2646_C52390)[0] , explode(' ', $fila->G2646_C52391)[0] , ($fila->G2646_C55902) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2646 WHERE G2646_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2646";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2646_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2646_ConsInte__b as id,  G2646_C51643 as camp1 , G2646_C51643 as camp2  FROM '.$BaseDatos.'.G2646 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2646_ConsInte__b as id,  G2646_C51643 as camp1 , G2646_C51643 as camp2  
                    FROM ".$BaseDatos.".G2646  JOIN ".$BaseDatos.".G2646_M".$_POST['muestra']." ON G2646_ConsInte__b = G2646_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2646_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2646_C51643 LIKE "%'.$B.'%" OR G2646_C51643 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2646_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2646 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2646(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2646_C51643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51643 = '".$_POST["G2646_C51643"]."'";
                $LsqlI .= $separador."G2646_C51643";
                $LsqlV .= $separador."'".$_POST["G2646_C51643"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51644"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51644 = '".$_POST["G2646_C51644"]."'";
                $LsqlI .= $separador."G2646_C51644";
                $LsqlV .= $separador."'".$_POST["G2646_C51644"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51645 = '".$_POST["G2646_C51645"]."'";
                $LsqlI .= $separador."G2646_C51645";
                $LsqlV .= $separador."'".$_POST["G2646_C51645"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51646 = '".$_POST["G2646_C51646"]."'";
                $LsqlI .= $separador."G2646_C51646";
                $LsqlV .= $separador."'".$_POST["G2646_C51646"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51647 = '".$_POST["G2646_C51647"]."'";
                $LsqlI .= $separador."G2646_C51647";
                $LsqlV .= $separador."'".$_POST["G2646_C51647"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51648"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51648 = '".$_POST["G2646_C51648"]."'";
                $LsqlI .= $separador."G2646_C51648";
                $LsqlV .= $separador."'".$_POST["G2646_C51648"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51649 = '".$_POST["G2646_C51649"]."'";
                $LsqlI .= $separador."G2646_C51649";
                $LsqlV .= $separador."'".$_POST["G2646_C51649"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51650"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51650 = '".$_POST["G2646_C51650"]."'";
                $LsqlI .= $separador."G2646_C51650";
                $LsqlV .= $separador."'".$_POST["G2646_C51650"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51651"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51651 = '".$_POST["G2646_C51651"]."'";
                $LsqlI .= $separador."G2646_C51651";
                $LsqlV .= $separador."'".$_POST["G2646_C51651"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51652"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51652 = '".$_POST["G2646_C51652"]."'";
                $LsqlI .= $separador."G2646_C51652";
                $LsqlV .= $separador."'".$_POST["G2646_C51652"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51653"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51653 = '".$_POST["G2646_C51653"]."'";
                $LsqlI .= $separador."G2646_C51653";
                $LsqlV .= $separador."'".$_POST["G2646_C51653"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51654"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51654 = '".$_POST["G2646_C51654"]."'";
                $LsqlI .= $separador."G2646_C51654";
                $LsqlV .= $separador."'".$_POST["G2646_C51654"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51637 = '".$_POST["G2646_C51637"]."'";
                $LsqlI .= $separador."G2646_C51637";
                $LsqlV .= $separador."'".$_POST["G2646_C51637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51638 = '".$_POST["G2646_C51638"]."'";
                $LsqlI .= $separador."G2646_C51638";
                $LsqlV .= $separador."'".$_POST["G2646_C51638"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51639"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51639 = '".$_POST["G2646_C51639"]."'";
                $LsqlI .= $separador."G2646_C51639";
                $LsqlV .= $separador."'".$_POST["G2646_C51639"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51668"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51668 = '".$_POST["G2646_C51668"]."'";
                $LsqlI .= $separador."G2646_C51668";
                $LsqlV .= $separador."'".$_POST["G2646_C51668"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51669"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51669 = '".$_POST["G2646_C51669"]."'";
                $LsqlI .= $separador."G2646_C51669";
                $LsqlV .= $separador."'".$_POST["G2646_C51669"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51670"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51670 = '".$_POST["G2646_C51670"]."'";
                $LsqlI .= $separador."G2646_C51670";
                $LsqlV .= $separador."'".$_POST["G2646_C51670"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51671"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51671 = '".$_POST["G2646_C51671"]."'";
                $LsqlI .= $separador."G2646_C51671";
                $LsqlV .= $separador."'".$_POST["G2646_C51671"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51672"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51672 = '".$_POST["G2646_C51672"]."'";
                $LsqlI .= $separador."G2646_C51672";
                $LsqlV .= $separador."'".$_POST["G2646_C51672"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51673"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51673 = '".$_POST["G2646_C51673"]."'";
                $LsqlI .= $separador."G2646_C51673";
                $LsqlV .= $separador."'".$_POST["G2646_C51673"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C51674"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C51674 = '".$_POST["G2646_C51674"]."'";
                $LsqlI .= $separador."G2646_C51674";
                $LsqlV .= $separador."'".$_POST["G2646_C51674"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C55955"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C55955 = '".$_POST["G2646_C55955"]."'";
                $LsqlI .= $separador."G2646_C55955";
                $LsqlV .= $separador."'".$_POST["G2646_C55955"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C55956"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C55956 = '".$_POST["G2646_C55956"]."'";
                $LsqlI .= $separador."G2646_C55956";
                $LsqlV .= $separador."'".$_POST["G2646_C55956"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C55957"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C55957 = '".$_POST["G2646_C55957"]."'";
                $LsqlI .= $separador."G2646_C55957";
                $LsqlV .= $separador."'".$_POST["G2646_C55957"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C52386"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C52386 = '".$_POST["G2646_C52386"]."'";
                $LsqlI .= $separador."G2646_C52386";
                $LsqlV .= $separador."'".$_POST["G2646_C52386"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C52387"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C52387 = '".$_POST["G2646_C52387"]."'";
                $LsqlI .= $separador."G2646_C52387";
                $LsqlV .= $separador."'".$_POST["G2646_C52387"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C52388"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C52388 = '".$_POST["G2646_C52388"]."'";
                $LsqlI .= $separador."G2646_C52388";
                $LsqlV .= $separador."'".$_POST["G2646_C52388"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2646_C52389"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C52389 = '".$_POST["G2646_C52389"]."'";
                $LsqlI .= $separador."G2646_C52389";
                $LsqlV .= $separador."'".$_POST["G2646_C52389"]."'";
                $validar = 1;
            }
             
 
            $G2646_C52390 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2646_C52390"])){    
                if($_POST["G2646_C52390"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2646_C52390"]);
                    if(count($tieneHora) > 1){
                        $G2646_C52390 = "'".$_POST["G2646_C52390"]."'";
                    }else{
                        $G2646_C52390 = "'".str_replace(' ', '',$_POST["G2646_C52390"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2646_C52390 = ".$G2646_C52390;
                    $LsqlI .= $separador." G2646_C52390";
                    $LsqlV .= $separador.$G2646_C52390;
                    $validar = 1;
                }
            }
 
            $G2646_C52391 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2646_C52391"])){    
                if($_POST["G2646_C52391"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2646_C52391"]);
                    if(count($tieneHora) > 1){
                        $G2646_C52391 = "'".$_POST["G2646_C52391"]."'";
                    }else{
                        $G2646_C52391 = "'".str_replace(' ', '',$_POST["G2646_C52391"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2646_C52391 = ".$G2646_C52391;
                    $LsqlI .= $separador." G2646_C52391";
                    $LsqlV .= $separador.$G2646_C52391;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2646_C55902"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_C55902 = '".$_POST["G2646_C55902"]."'";
                $LsqlI .= $separador."G2646_C55902";
                $LsqlV .= $separador."'".$_POST["G2646_C55902"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2646_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2646_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2646_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2646_Usuario , G2646_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2646_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2646 WHERE G2646_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2646 SET G2646_UltiGest__b =-14, G2646_GesMasImp_b =-14, G2646_TipoReintentoUG_b =0, G2646_TipoReintentoGMI_b =0, G2646_EstadoUG_b =-14, G2646_EstadoGMI_b =-14, G2646_CantidadIntentos =0, G2646_CantidadIntentosGMI_b =0 WHERE G2646_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2677_ConsInte__b, G2677_C52341, G2677_C52343, G2677_C52344, G2677_C52350, G2677_C52352, f.LISOPC_Nombre____b as  G2677_C52354, G2677_C52359, G2677_C52360 FROM ".$BaseDatos.".G2677  LEFT JOIN ".$BaseDatos_systema.".LISOPC as f ON f.LISOPC_ConsInte__b =  G2677_C52354 ";

        $SQL .= " WHERE G2677_C52342 = '".$numero."'"; 

        $SQL .= " ORDER BY G2677_C52341";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2677_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2677_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2677_C52341)."</cell>";

                echo "<cell>". ($fila->G2677_C52343)."</cell>";

                if($fila->G2677_C52344 != ''){
                    echo "<cell>". explode(' ', $fila->G2677_C52344)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2677_C52350)."</cell>";

                echo "<cell>". ($fila->G2677_C52352)."</cell>";

                echo "<cell>". ($fila->G2677_C52354)."</cell>";

                if($fila->G2677_C52359 != ''){
                    echo "<cell>". explode(' ', $fila->G2677_C52359)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G2677_C52360 != ''){
                    echo "<cell>". explode(' ', $fila->G2677_C52360)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2677 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2677(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2677_C52341"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2677_C52341 = '".$_POST["G2677_C52341"]."'";
                    $LsqlI .= $separador."G2677_C52341";
                    $LsqlV .= $separador."'".$_POST["G2677_C52341"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2677_C52343"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2677_C52343 = '".$_POST["G2677_C52343"]."'";
                    $LsqlI .= $separador."G2677_C52343";
                    $LsqlV .= $separador."'".$_POST["G2677_C52343"]."'";
                    $validar = 1;
                }

                                                                               

                $G2677_C52344 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2677_C52344"])){    
                    if($_POST["G2677_C52344"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2677_C52344 = "'".str_replace(' ', '',$_POST["G2677_C52344"])." 00:00:00'";
                        $LsqlU .= $separador." G2677_C52344 = ".$G2677_C52344;
                        $LsqlI .= $separador." G2677_C52344";
                        $LsqlV .= $separador.$G2677_C52344;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2677_C52350"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2677_C52350 = '".$_POST["G2677_C52350"]."'";
                    $LsqlI .= $separador."G2677_C52350";
                    $LsqlV .= $separador."'".$_POST["G2677_C52350"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2677_C52352"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2677_C52352 = '".$_POST["G2677_C52352"]."'";
                    $LsqlI .= $separador."G2677_C52352";
                    $LsqlV .= $separador."'".$_POST["G2677_C52352"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2677_C52354"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2677_C52354 = '".$_POST["G2677_C52354"]."'";
                    $LsqlI .= $separador."G2677_C52354";
                    $LsqlV .= $separador."'".$_POST["G2677_C52354"]."'";
                    $validar = 1;
                }

                $G2677_C52359 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2677_C52359"])){    
                    if($_POST["G2677_C52359"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2677_C52359 = "'".str_replace(' ', '',$_POST["G2677_C52359"])." 00:00:00'";
                        $LsqlU .= $separador." G2677_C52359 = ".$G2677_C52359;
                        $LsqlI .= $separador." G2677_C52359";
                        $LsqlV .= $separador.$G2677_C52359;
                        $validar = 1;
                    }
                }

                $G2677_C52360 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2677_C52360"])){    
                    if($_POST["G2677_C52360"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2677_C52360 = "'".str_replace(' ', '',$_POST["G2677_C52360"])." 00:00:00'";
                        $LsqlU .= $separador." G2677_C52360 = ".$G2677_C52360;
                        $LsqlI .= $separador." G2677_C52360";
                        $LsqlV .= $separador.$G2677_C52360;
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2677_C52342 = $numero;
                    $LsqlU .= ", G2677_C52342 = ".$G2677_C52342."";
                    $LsqlI .= ", G2677_C52342";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2677_Usuario ,  G2677_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2677_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2677 WHERE  G2677_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
