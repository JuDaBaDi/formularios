<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2969_ConsInte__b, G2969_FechaInsercion , G2969_Usuario ,  G2969_CodigoMiembro  , G2969_PoblacionOrigen , G2969_EstadoDiligenciamiento ,  G2969_IdLlamada , G2969_C59406 as principal ,G2969_C59406,G2969_C59407,G2969_C59408,G2969_C59409,G2969_C59410,G2969_C59413,G2969_C59411,G2969_C59412,G2969_C59403,G2969_C59404,G2969_C59405 FROM '.$BaseDatos.'.G2969 WHERE G2969_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2969_C59406'] = $key->G2969_C59406;

                $datos[$i]['G2969_C59407'] = $key->G2969_C59407;

                $datos[$i]['G2969_C59408'] = $key->G2969_C59408;

                $datos[$i]['G2969_C59409'] = $key->G2969_C59409;

                $datos[$i]['G2969_C59410'] = $key->G2969_C59410;

                $datos[$i]['G2969_C59413'] = $key->G2969_C59413;

                $datos[$i]['G2969_C59411'] = explode(' ', $key->G2969_C59411)[0];

                $datos[$i]['G2969_C59412'] = $key->G2969_C59412;

                $datos[$i]['G2969_C59403'] = $key->G2969_C59403;

                $datos[$i]['G2969_C59404'] = $key->G2969_C59404;

                $datos[$i]['G2969_C59405'] = $key->G2969_C59405;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2969";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2969_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2969_ConsInte__b as id,  G2969_C59406 as camp1 , G2969_C59406 as camp2 
                     FROM ".$BaseDatos.".G2969  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2969_ConsInte__b as id,  G2969_C59406 as camp1 , G2969_C59406 as camp2  
                    FROM ".$BaseDatos.".G2969  JOIN ".$BaseDatos.".G2969_M".$_POST['muestra']." ON G2969_ConsInte__b = G2969_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2969_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2969_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2969_C59406 LIKE '%".$B."%' OR G2969_C59406 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2969_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2969");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2969_ConsInte__b, G2969_FechaInsercion , G2969_Usuario ,  G2969_CodigoMiembro  , G2969_PoblacionOrigen , G2969_EstadoDiligenciamiento ,  G2969_IdLlamada , G2969_C59406 as principal ,G2969_C59406,G2969_C59407,G2969_C59408,G2969_C59409,G2969_C59410, a.LISOPC_Nombre____b as G2969_C59413,G2969_C59411,G2969_C59412,G2969_C59403,G2969_C59404, b.LISOPC_Nombre____b as G2969_C59405 FROM '.$BaseDatos.'.G2969 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2969_C59413 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2969_C59405';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2969_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2969_ConsInte__b , ($fila->G2969_C59406) , ($fila->G2969_C59407) , ($fila->G2969_C59408) , ($fila->G2969_C59409) , ($fila->G2969_C59410) , ($fila->G2969_C59413) , explode(' ', $fila->G2969_C59411)[0] , ($fila->G2969_C59412) , ($fila->G2969_C59403) , ($fila->G2969_C59404) , ($fila->G2969_C59405) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2969";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2969_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2969_ConsInte__b as id,  G2969_C59406 as camp1 , G2969_C59406 as camp2  FROM '.$BaseDatos.'.G2969 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2969_ConsInte__b as id,  G2969_C59406 as camp1 , G2969_C59406 as camp2  
                    FROM ".$BaseDatos.".G2969  JOIN ".$BaseDatos.".G2969_M".$_POST['muestra']." ON G2969_ConsInte__b = G2969_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2969_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2969_C59406 LIKE "%'.$B.'%" OR G2969_C59406 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2969_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2969 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2969(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2969_C59406"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59406 = '".$_POST["G2969_C59406"]."'";
                $LsqlI .= $separador."G2969_C59406";
                $LsqlV .= $separador."'".$_POST["G2969_C59406"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59407"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59407 = '".$_POST["G2969_C59407"]."'";
                $LsqlI .= $separador."G2969_C59407";
                $LsqlV .= $separador."'".$_POST["G2969_C59407"]."'";
                $validar = 1;
            }
             
  
            $G2969_C59408 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2969_C59408"])){
                if($_POST["G2969_C59408"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2969_C59408 = $_POST["G2969_C59408"];
                    $LsqlU .= $separador." G2969_C59408 = ".$G2969_C59408."";
                    $LsqlI .= $separador." G2969_C59408";
                    $LsqlV .= $separador.$G2969_C59408;
                    $validar = 1;
                }
            }
  
            $G2969_C59409 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2969_C59409"])){
                if($_POST["G2969_C59409"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2969_C59409 = $_POST["G2969_C59409"];
                    $LsqlU .= $separador." G2969_C59409 = ".$G2969_C59409."";
                    $LsqlI .= $separador." G2969_C59409";
                    $LsqlV .= $separador.$G2969_C59409;
                    $validar = 1;
                }
            }
  
            $G2969_C59410 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2969_C59410"])){
                if($_POST["G2969_C59410"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2969_C59410 = $_POST["G2969_C59410"];
                    $LsqlU .= $separador." G2969_C59410 = ".$G2969_C59410."";
                    $LsqlI .= $separador." G2969_C59410";
                    $LsqlV .= $separador.$G2969_C59410;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2969_C59413"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59413 = '".$_POST["G2969_C59413"]."'";
                $LsqlI .= $separador."G2969_C59413";
                $LsqlV .= $separador."'".$_POST["G2969_C59413"]."'";
                $validar = 1;
            }
             
 
            $G2969_C59411 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2969_C59411"])){    
                if($_POST["G2969_C59411"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2969_C59411"]);
                    if(count($tieneHora) > 1){
                        $G2969_C59411 = "'".$_POST["G2969_C59411"]."'";
                    }else{
                        $G2969_C59411 = "'".str_replace(' ', '',$_POST["G2969_C59411"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2969_C59411 = ".$G2969_C59411;
                    $LsqlI .= $separador." G2969_C59411";
                    $LsqlV .= $separador.$G2969_C59411;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2969_C59412"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59412 = '".$_POST["G2969_C59412"]."'";
                $LsqlI .= $separador."G2969_C59412";
                $LsqlV .= $separador."'".$_POST["G2969_C59412"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59403"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59403 = '".$_POST["G2969_C59403"]."'";
                $LsqlI .= $separador."G2969_C59403";
                $LsqlV .= $separador."'".$_POST["G2969_C59403"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59404"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59404 = '".$_POST["G2969_C59404"]."'";
                $LsqlI .= $separador."G2969_C59404";
                $LsqlV .= $separador."'".$_POST["G2969_C59404"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2969_C59405"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_C59405 = '".$_POST["G2969_C59405"]."'";
                $LsqlI .= $separador."G2969_C59405";
                $LsqlV .= $separador."'".$_POST["G2969_C59405"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2969_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2969_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2969_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2969_Usuario , G2969_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2969_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2969 WHERE G2969_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2969 SET G2969_UltiGest__b =-14, G2969_GesMasImp_b =-14, G2969_TipoReintentoUG_b =0, G2969_TipoReintentoGMI_b =0, G2969_EstadoUG_b =-14, G2969_EstadoGMI_b =-14, G2969_CantidadIntentos =0, G2969_CantidadIntentosGMI_b =0 WHERE G2969_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
