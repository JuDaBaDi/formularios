
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2727/G2727_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2727_ConsInte__b as id, G2727_C53693 as camp1 , G2727_C53694 as camp2 FROM ".$BaseDatos.".G2727  WHERE G2727_Usuario = ".$idUsuario." ORDER BY G2727_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2727_ConsInte__b as id, G2727_C53693 as camp1 , G2727_C53694 as camp2 FROM ".$BaseDatos.".G2727  ORDER BY G2727_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2727_ConsInte__b as id, G2727_C53693 as camp1 , G2727_C53694 as camp2 FROM ".$BaseDatos.".G2727 JOIN ".$BaseDatos.".G2727_M".$resultEstpas->muestr." ON G2727_ConsInte__b = G2727_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2727_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2727_ConsInte__b as id, G2727_C53693 as camp1 , G2727_C53694 as camp2 FROM ".$BaseDatos.".G2727 JOIN ".$BaseDatos.".G2727_M".$resultEstpas->muestr." ON G2727_ConsInte__b = G2727_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2727_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2727_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G2727_ConsInte__b as id, G2727_C53693 as camp1 , G2727_C53694 as camp2 FROM ".$BaseDatos.".G2727  ORDER BY G2727_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    
    //JDBD - Validamos si se pudo obtener el dato principal.
    if ($nombre = $mysqli->query($XLsql)) {

        $nombreUsuario = NULL;
        //echo $XLsql;
        while ($key = $nombre->fetch_object()) {
            echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
            $nombreUsuario = $key->nombre;
            break;
        } 


        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                        
            $data = array(  "strToken_t" => $_GET['token'], 
                            "strIdGestion_t" => $_GET['id_gestion_cbx'],
                            "strDatoPrincipal_t" => $nombreUsuario,
                            "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
            $data_string = json_encode($data);    

            $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                      
            ); 
            //recogemos la respuesta
            $respuesta = curl_exec ($ch);
            //o el error, por si falla
            $error = curl_error($ch);
            //y finalmente cerramos curl
            //echo "Respuesta =>  ". $respuesta;
            //echo "<br/>Error => ".$error;
            //include "Log.class.php";
            //$log = new Log("log", "./Log/");
            //$log->insert($error, $respuesta, false, true, false);
            //echo "nada";
            curl_close ($ch);
        }

    }else{
        echo "<script>alert('NO SE PUDO OBTENER EL DATO PRINCIPAL DEL REGISTRO.');</script>";
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary"  id="8157">
    <div class="box-header with-border">
        <h4 class="box-title">
            SALUDO
        </h4>
        
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días, tardes, noches, me comunico con la sr@ ____le habla ____operador logístico autorizado por el BANCO de OCCIDENTE. El motivo de mi llamada es agendar una cita para la entrega de la renovación de su tarjeta crédito BANCO OCCIDENTE. </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Para confirma que hablo con la sr@_____me indica los tres últimos dígitos de la cedula.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


    </div>
</div>

<div  class="panel box box-primary"  id="8154">
    <div class="box-header with-border">
        <h4 class="box-title">
            DATOS DEL CLIENTE
        </h4>
        
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2727_C53693" id="LblG2727_C53693">NOMBRE DEL TITULAR</label>
                        <textarea class="form-control input-sm" name="G2727_C53693" id="G2727_C53693"  value="<?php if (isset($_GET['G2727_C53693'])) {
                            echo $_GET['G2727_C53693'];
                        } ?>" placeholder="NOMBRE DEL TITULAR"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53694" id="LblG2727_C53694">CEDULA</label><input type="text" class="form-control input-sm" id="G2727_C53694" value="<?php if (isset($_GET['G2727_C53694'])) {
                            echo $_GET['G2727_C53694'];
                        } ?>"  name="G2727_C53694"  placeholder="CEDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53695" id="LblG2727_C53695">GUIA</label><input type="text" class="form-control input-sm" id="G2727_C53695" value="<?php if (isset($_GET['G2727_C53695'])) {
                            echo $_GET['G2727_C53695'];
                        } ?>"  name="G2727_C53695"  placeholder="GUIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53696" id="LblG2727_C53696">CAMPANA</label><input type="text" class="form-control input-sm" id="G2727_C53696" value="<?php if (isset($_GET['G2727_C53696'])) {
                            echo $_GET['G2727_C53696'];
                        } ?>"  name="G2727_C53696"  placeholder="CAMPANA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53698" id="LblG2727_C53698">BIN</label><input type="text" class="form-control input-sm" id="G2727_C53698" value="<?php if (isset($_GET['G2727_C53698'])) {
                            echo $_GET['G2727_C53698'];
                        } ?>"  name="G2727_C53698"  placeholder="BIN"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53699" id="LblG2727_C53699">AMPARADOR</label><input type="text" class="form-control input-sm" id="G2727_C53699" value="<?php if (isset($_GET['G2727_C53699'])) {
                            echo $_GET['G2727_C53699'];
                        } ?>"  name="G2727_C53699"  placeholder="AMPARADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


    </div>
</div>

<div  id="8156" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53687" id="LblG2727_C53687">Agente</label><input type="text" class="form-control input-sm" id="G2727_C53687" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G2727_C53687"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53688" id="LblG2727_C53688">Fecha</label><input type="text" class="form-control input-sm" id="G2727_C53688" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G2727_C53688"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53689" id="LblG2727_C53689">Hora</label><input type="text" class="form-control input-sm" id="G2727_C53689" value="<?php echo date('H:i:s');?>" readonly name="G2727_C53689"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53690" id="LblG2727_C53690">Campaña</label><input type="text" class="form-control input-sm" id="G2727_C53690" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G2727_C53690"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8158" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53700" id="LblG2727_C53700">CELULAR</label><div class="input-group">
                            <input type="text" class="form-control input-sm" id="G2727_C53700" value="<?php if (isset($_GET['G2727_C53700'])) {
                            echo $_GET['G2727_C53700'];
                        } ?>"  name="G2727_C53700"  placeholder="CELULAR">
                            <div class="input-group-addon telefono" style="cursor:pointer" id="TLF_G2727_C53700">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53703" id="LblG2727_C53703">TELEFONO 2</label><div class="input-group">
                            <input type="text" class="form-control input-sm" id="G2727_C53703" value="<?php if (isset($_GET['G2727_C53703'])) {
                            echo $_GET['G2727_C53703'];
                        } ?>"  name="G2727_C53703"  placeholder="TELEFONO 2">
                            <div class="input-group-addon telefono" style="cursor:pointer" id="TLF_G2727_C53703">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53706" id="LblG2727_C53706">TELEFONO 3</label><div class="input-group">
                            <input type="text" class="form-control input-sm" id="G2727_C53706" value="<?php if (isset($_GET['G2727_C53706'])) {
                            echo $_GET['G2727_C53706'];
                        } ?>"  name="G2727_C53706"  placeholder="TELEFONO 3">
                            <div class="input-group-addon telefono" style="cursor:pointer" id="TLF_G2727_C53706">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53709" id="LblG2727_C53709">TELEFONO 4</label><div class="input-group">
                            <input type="text" class="form-control input-sm" id="G2727_C53709" value="<?php if (isset($_GET['G2727_C53709'])) {
                            echo $_GET['G2727_C53709'];
                        } ?>"  name="G2727_C53709"  placeholder="TELEFONO 4">
                            <div class="input-group-addon telefono" style="cursor:pointer" id="TLF_G2727_C53709">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


</div>

<div  id="8155" >
<h3 class="box box-title"></h3>

</div>

<div  id="8159" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53712" id="LblG2727_C53712">DIRECCION 1</label><input type="text" class="form-control input-sm" id="G2727_C53712" value="<?php if (isset($_GET['G2727_C53712'])) {
                            echo $_GET['G2727_C53712'];
                        } ?>"  name="G2727_C53712"  placeholder="DIRECCION 1"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53713" id="LblG2727_C53713">CIUDAD 1</label><input type="text" class="form-control input-sm" id="G2727_C53713" value="<?php if (isset($_GET['G2727_C53713'])) {
                            echo $_GET['G2727_C53713'];
                        } ?>"  name="G2727_C53713"  placeholder="CIUDAD 1"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53715" id="LblG2727_C53715">DIRECCION 2</label><input type="text" class="form-control input-sm" id="G2727_C53715" value="<?php if (isset($_GET['G2727_C53715'])) {
                            echo $_GET['G2727_C53715'];
                        } ?>"  name="G2727_C53715"  placeholder="DIRECCION 2"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53716" id="LblG2727_C53716">CIUDAD 2</label><input type="text" class="form-control input-sm" id="G2727_C53716" value="<?php if (isset($_GET['G2727_C53716'])) {
                            echo $_GET['G2727_C53716'];
                        } ?>"  name="G2727_C53716"  placeholder="CIUDAD 2"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53718" id="LblG2727_C53718">DIRECCION 3</label><input type="text" class="form-control input-sm" id="G2727_C53718" value="<?php if (isset($_GET['G2727_C53718'])) {
                            echo $_GET['G2727_C53718'];
                        } ?>"  name="G2727_C53718"  placeholder="DIRECCION 3"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53719" id="LblG2727_C53719">CIUDAD 3</label><input type="text" class="form-control input-sm" id="G2727_C53719" value="<?php if (isset($_GET['G2727_C53719'])) {
                            echo $_GET['G2727_C53719'];
                        } ?>"  name="G2727_C53719"  placeholder="CIUDAD 3"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53721" id="LblG2727_C53721">DIRECCION 4</label><input type="text" class="form-control input-sm" id="G2727_C53721" value="<?php if (isset($_GET['G2727_C53721'])) {
                            echo $_GET['G2727_C53721'];
                        } ?>"  name="G2727_C53721"  placeholder="DIRECCION 4"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53722" id="LblG2727_C53722">CIUDAD 4</label><input type="text" class="form-control input-sm" id="G2727_C53722" value="<?php if (isset($_GET['G2727_C53722'])) {
                            echo $_GET['G2727_C53722'];
                        } ?>"  name="G2727_C53722"  placeholder="CIUDAD 4"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53724" id="LblG2727_C53724">DIRECCION 5</label><input type="text" class="form-control input-sm" id="G2727_C53724" value="<?php if (isset($_GET['G2727_C53724'])) {
                            echo $_GET['G2727_C53724'];
                        } ?>"  name="G2727_C53724"  placeholder="DIRECCION 5"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53725" id="LblG2727_C53725">CIUDAD 5</label><input type="text" class="form-control input-sm" id="G2727_C53725" value="<?php if (isset($_GET['G2727_C53725'])) {
                            echo $_GET['G2727_C53725'];
                        } ?>"  name="G2727_C53725"  placeholder="CIUDAD 5"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53727" id="LblG2727_C53727">DIRECCION 6</label><input type="text" class="form-control input-sm" id="G2727_C53727" value="<?php if (isset($_GET['G2727_C53727'])) {
                            echo $_GET['G2727_C53727'];
                        } ?>"  name="G2727_C53727"  placeholder="DIRECCION 6"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53728" id="LblG2727_C53728">CIUDAD 6</label><input type="text" class="form-control input-sm" id="G2727_C53728" value="<?php if (isset($_GET['G2727_C53728'])) {
                            echo $_GET['G2727_C53728'];
                        } ?>"  name="G2727_C53728"  placeholder="CIUDAD 6"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8203" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2727_C54123" id="LblG2727_C54123">GESTION</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2727_C54123" id="G2727_C54123">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3384 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2727_C59533" id="LblG2727_C59533">ESTADO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2727_C59533" id="G2727_C59533">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3613 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8165" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53765" id="LblG2727_C53765">DIRECCIÓN NUEVA DE ENTREGA</label><input type="text" class="form-control input-sm" id="G2727_C53765" value="<?php if (isset($_GET['G2727_C53765'])) {
                            echo $_GET['G2727_C53765'];
                        } ?>"  name="G2727_C53765"  placeholder="DIRECCIÓN NUEVA DE ENTREGA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53766" id="LblG2727_C53766">COMPLEMENTOS</label><input type="text" class="form-control input-sm" id="G2727_C53766" value="<?php if (isset($_GET['G2727_C53766'])) {
                            echo $_GET['G2727_C53766'];
                        } ?>"  name="G2727_C53766"  placeholder="COMPLEMENTOS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2727_C53767" id="LblG2727_C53767">BARRIO</label><input type="text" class="form-control input-sm" id="G2727_C53767" value="<?php if (isset($_GET['G2727_C53767'])) {
                            echo $_GET['G2727_C53767'];
                        } ?>"  name="G2727_C53767"  placeholder="BARRIO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G2727_C53768" id="LblG2727_C53768">CIUDAD</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G2727_C53768" id="G2727_C53768">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G2727_C53769" id="LblG2727_C53769">DEPARTAMENTO</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G2727_C53769" id="G2727_C53769">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2727_C53770" id="LblG2727_C53770">CELULAR NUEVO</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2727_C53770'])) {
                            echo $_GET['G2727_C53770'];
                        } ?>"  name="G2727_C53770" id="G2727_C53770" placeholder="CELULAR NUEVO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2727_C53771" id="LblG2727_C53771">FRANJA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2727_C53771" id="G2727_C53771">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3203 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2727_C53772" id="LblG2727_C53772">FECHA DE ENTREGA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2727_C53772'])) {
                            echo $_GET['G2727_C53772'];
                        } ?>"  name="G2727_C53772" id="G2727_C53772" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2727_C53773" id="LblG2727_C53773">OBSERVACIONES</label>
                        <textarea class="form-control input-sm" name="G2727_C53773" id="G2727_C53773"  value="<?php if (isset($_GET['G2727_C53773'])) {
                            echo $_GET['G2727_C53773'];
                        } ?>" placeholder="OBSERVACIONES"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2727_C53774" id="LblG2727_C53774">PERSONA CONTACTADA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2727_C53774" id="G2727_C53774">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3204 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary"  id="8173">
    <div class="box-header with-border">
        <h4 class="box-title">
            DESPEDIDA
        </h4>
        
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Recuerde, para la entrega deberá presentarse su cedula original, Por efectos de calidad y seguridad en el servicio, la llamada fue grabada y monitoreada. Le agradezco por haber atendido mi llamada le hablo __Que tenga buena (día, tarde, noche). </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


    </div>
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">GUIA</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear GUIA" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2727_C53682">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3189;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2727_C53682">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3189;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2727_C53683">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2727_C53684" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2727_C53685" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2727_C53686" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2727/G2727_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2727_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2727_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2727_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G2727_C53693").val(item.G2727_C53693); 
                $("#G2727_C53694").val(item.G2727_C53694); 
                $("#G2727_C53695").val(item.G2727_C53695); 
                $("#G2727_C53696").val(item.G2727_C53696); 
                $("#G2727_C53698").val(item.G2727_C53698); 
                $("#G2727_C53699").val(item.G2727_C53699); 
                $("#G2727_C53682").val(item.G2727_C53682).trigger("change");  
                $("#G2727_C53683").val(item.G2727_C53683).trigger("change");  
                $("#G2727_C53684").val(item.G2727_C53684); 
                $("#G2727_C53685").val(item.G2727_C53685); 
                $("#G2727_C53686").val(item.G2727_C53686); 
                $("#G2727_C53687").val(item.G2727_C53687); 
                $("#G2727_C53688").val(item.G2727_C53688); 
                $("#G2727_C53689").val(item.G2727_C53689); 
                $("#G2727_C53690").val(item.G2727_C53690);   
                if(item.G2727_C53691 == 1){
                    $("#G2727_C53691").attr('checked', true);
                }    
                if(item.G2727_C53692 == 1){
                    $("#G2727_C53692").attr('checked', true);
                }  
                $("#G2727_C53700").val(item.G2727_C53700); 
                $("#G2727_C53703").val(item.G2727_C53703); 
                $("#G2727_C53706").val(item.G2727_C53706); 
                $("#G2727_C53709").val(item.G2727_C53709); 
                $("#G2727_C53712").val(item.G2727_C53712); 
                $("#G2727_C53713").val(item.G2727_C53713); 
                $("#G2727_C53715").val(item.G2727_C53715); 
                $("#G2727_C53716").val(item.G2727_C53716); 
                $("#G2727_C53718").val(item.G2727_C53718); 
                $("#G2727_C53719").val(item.G2727_C53719); 
                $("#G2727_C53721").val(item.G2727_C53721); 
                $("#G2727_C53722").val(item.G2727_C53722); 
                $("#G2727_C53724").val(item.G2727_C53724); 
                $("#G2727_C53725").val(item.G2727_C53725); 
                $("#G2727_C53727").val(item.G2727_C53727); 
                $("#G2727_C53728").val(item.G2727_C53728); 
                $("#G2727_C53765").val(item.G2727_C53765); 
                $("#G2727_C53766").val(item.G2727_C53766); 
                $("#G2727_C53767").val(item.G2727_C53767); 
                $("#G2727_C53768").attr("opt",item.G2727_C53768);
                $("#G2727_C53768").val(item.G2727_C53768).trigger("change"); 
                $("#G2727_C53769").attr("opt",item.G2727_C53769);
                $("#G2727_C53769").val(item.G2727_C53769).trigger("change"); 
                $("#G2727_C53770").val(item.G2727_C53770); 
                $("#G2727_C53771").val(item.G2727_C53771).trigger("change");  
                $("#G2727_C53772").val(item.G2727_C53772); 
                $("#G2727_C53773").val(item.G2727_C53773); 
                $("#G2727_C53774").val(item.G2727_C53774).trigger("change");    
                if(item.G2727_C53881 == 1){
                    $("#G2727_C53881").attr('checked', true);
                }  
                $("#G2727_C54123").val(item.G2727_C54123).trigger("change");  
                $("#G2727_C59533").val(item.G2727_C59533).trigger("change"); 
                
                cargarHijos_0(
        $("#G2727_C53694").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G2727_C53694").val());
            var id_0 = $("#G2727_C53694").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G2727_C53694").val());
            var id_0 = $("#G2727_C53694").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G2727_C53694").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2751&view=si&formaDetalle=si&formularioPadre=2727&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=54347<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2751&view=si&formaDetalle=si&formularioPadre=2727&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=54347&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2751&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=2727&pincheCampo=54347&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G2727_C54123").select2();

    $("#G2727_C59533").select2();
                            $("#G2727_C53768").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2727_C53768=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G2727_C53768").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G2727_C53768 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G2727_C53768").html('<option value="'+data.G2781_ConsInte__b+'" >'+data.G2781_C55143+'</option>');
                                        
                                $("#G2727_C53769").val(data.G2781_C55144);
                                    }
                                });
                            });
                            $("#G2727_C53769").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2727_C53769=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G2727_C53769").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G2727_C53769 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        // $("#G2727_C53769").html('<option value="'+data.G2781_ConsInte__b+'" >'+data.+'</option>');
                                        
                                    }
                                });
                            });

    $("#G2727_C53771").select2();

    $("#G2727_C53774").select2();
                $("#G2727_C53682").change(function(){
                    $(".ReqForTip").closest(".form-group").removeClass("has-error");
                    $(".ReqForTip").removeClass("ReqForTip");
                        
                    if($(this).val() == "38632"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "38633"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "38634"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39165"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39166"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39167"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39535"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53765").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53766").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53767").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53768").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53769").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53770").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53771").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53772").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53774").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C54123").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "38632"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "38633"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "38634"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "39165"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "39166"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "39167"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "39535"){
                        $("#G2727_C59533").addClass("ReqForTip");
                    }
                    if($(this).val() == "38632"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "38633"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "38634"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "39164"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "39165"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "39166"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "39167"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                    if($(this).val() == "39535"){
                        $("#G2727_C53773").addClass("ReqForTip");
                    }
                });
        //datepickers
        

        $("#G2727_C53684").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2727_C53772").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2727_C53685").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G2727_C53770").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para CELULAR 

    $("#TLF_G2727_C53700").click(function(){
        strTel_t=$("#G2727_C53700").val();
        llamarDesdeBtnTelefono(strTel_t);
    });

    //function para TELEFONO 2 

    $("#TLF_G2727_C53703").click(function(){
        strTel_t=$("#G2727_C53703").val();
        llamarDesdeBtnTelefono(strTel_t);
    });

    //function para TELEFONO 3 

    $("#TLF_G2727_C53706").click(function(){
        strTel_t=$("#G2727_C53706").val();
        llamarDesdeBtnTelefono(strTel_t);
    });

    //function para TELEFONO 4 

    $("#TLF_G2727_C53709").click(function(){
        strTel_t=$("#G2727_C53709").val();
        llamarDesdeBtnTelefono(strTel_t);
    });

    //function para GESTION 

    $("#G2727_C54123").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO 

    $("#G2727_C59533").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para FRANJA 

    $("#G2727_C53771").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PERSONA CONTACTADA 

    $("#G2727_C53774").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2727_C53693").val(item.G2727_C53693);
 
                                                $("#G2727_C53694").val(item.G2727_C53694);
 
                                                $("#G2727_C53695").val(item.G2727_C53695);
 
                                                $("#G2727_C53696").val(item.G2727_C53696);
 
                                                $("#G2727_C53698").val(item.G2727_C53698);
 
                                                $("#G2727_C53699").val(item.G2727_C53699);
 
                    $("#G2727_C53682").val(item.G2727_C53682).trigger("change"); 
 
                    $("#G2727_C53683").val(item.G2727_C53683).trigger("change"); 
 
                                                $("#G2727_C53684").val(item.G2727_C53684);
 
                                                $("#G2727_C53685").val(item.G2727_C53685);
 
                                                $("#G2727_C53686").val(item.G2727_C53686);
 
                                                $("#G2727_C53687").val(item.G2727_C53687);
 
                                                $("#G2727_C53688").val(item.G2727_C53688);
 
                                                $("#G2727_C53689").val(item.G2727_C53689);
 
                                                $("#G2727_C53690").val(item.G2727_C53690);
      
                                                if(item.G2727_C53691 == 1){
                                                   $("#G2727_C53691").attr('checked', true);
                                                } 
      
                                                if(item.G2727_C53692 == 1){
                                                   $("#G2727_C53692").attr('checked', true);
                                                } 
 
                                                $("#G2727_C53700").val(item.G2727_C53700);
 
                                                $("#G2727_C53703").val(item.G2727_C53703);
 
                                                $("#G2727_C53706").val(item.G2727_C53706);
 
                                                $("#G2727_C53709").val(item.G2727_C53709);
 
                                                $("#G2727_C53712").val(item.G2727_C53712);
 
                                                $("#G2727_C53713").val(item.G2727_C53713);
 
                                                $("#G2727_C53715").val(item.G2727_C53715);
 
                                                $("#G2727_C53716").val(item.G2727_C53716);
 
                                                $("#G2727_C53718").val(item.G2727_C53718);
 
                                                $("#G2727_C53719").val(item.G2727_C53719);
 
                                                $("#G2727_C53721").val(item.G2727_C53721);
 
                                                $("#G2727_C53722").val(item.G2727_C53722);
 
                                                $("#G2727_C53724").val(item.G2727_C53724);
 
                                                $("#G2727_C53725").val(item.G2727_C53725);
 
                                                $("#G2727_C53727").val(item.G2727_C53727);
 
                                                $("#G2727_C53728").val(item.G2727_C53728);
 
                                                $("#G2727_C53765").val(item.G2727_C53765);
 
                                                $("#G2727_C53766").val(item.G2727_C53766);
 
                                                $("#G2727_C53767").val(item.G2727_C53767);
 
                    $("#G2727_C53768").attr("opt",item.G2727_C53768);
                    $("#G2727_C53768").val(item.G2727_C53768).trigger("change");
 
                    $("#G2727_C53769").attr("opt",item.G2727_C53769);
                    $("#G2727_C53769").val(item.G2727_C53769).trigger("change");
 
                                                $("#G2727_C53770").val(item.G2727_C53770);
 
                    $("#G2727_C53771").val(item.G2727_C53771).trigger("change"); 
 
                                                $("#G2727_C53772").val(item.G2727_C53772);
 
                                                $("#G2727_C53773").val(item.G2727_C53773);
 
                    $("#G2727_C53774").val(item.G2727_C53774).trigger("change"); 
      
                                                if(item.G2727_C53881 == 1){
                                                   $("#G2727_C53881").attr('checked', true);
                                                } 
 
                    $("#G2727_C54123").val(item.G2727_C54123).trigger("change"); 
 
                    $("#G2727_C59533").val(item.G2727_C59533).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','NOMBRE DEL TITULAR','CEDULA','GUIA','CAMPANA','BIN','AMPARADOR','Agente','Fecha','Hora','Campaña','CELULAR','TELEFONO 2','TELEFONO 3','TELEFONO 4','DIRECCION 1','CIUDAD 1','DIRECCION 2','CIUDAD 2','DIRECCION 3','CIUDAD 3','DIRECCION 4','CIUDAD 4','DIRECCION 5','CIUDAD 5','DIRECCION 6','CIUDAD 6','DIRECCIÓN NUEVA DE ENTREGA','COMPLEMENTOS','BARRIO','CIUDAD','DEPARTAMENTO','CELULAR NUEVO','FRANJA','FECHA DE ENTREGA','OBSERVACIONES','PERSONA CONTACTADA','GESTION','ESTADO'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2727_C53693', 
                        index:'G2727_C53693', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53694', 
                        index: 'G2727_C53694', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53695', 
                        index: 'G2727_C53695', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53696', 
                        index: 'G2727_C53696', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53698', 
                        index: 'G2727_C53698', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53699', 
                        index: 'G2727_C53699', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53687', 
                        index: 'G2727_C53687', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53688', 
                        index: 'G2727_C53688', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53689', 
                        index: 'G2727_C53689', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53690', 
                        index: 'G2727_C53690', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53700', 
                        index: 'G2727_C53700', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53703', 
                        index: 'G2727_C53703', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53706', 
                        index: 'G2727_C53706', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53709', 
                        index: 'G2727_C53709', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53712', 
                        index: 'G2727_C53712', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53713', 
                        index: 'G2727_C53713', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53715', 
                        index: 'G2727_C53715', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53716', 
                        index: 'G2727_C53716', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53718', 
                        index: 'G2727_C53718', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53719', 
                        index: 'G2727_C53719', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53721', 
                        index: 'G2727_C53721', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53722', 
                        index: 'G2727_C53722', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53724', 
                        index: 'G2727_C53724', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53725', 
                        index: 'G2727_C53725', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53727', 
                        index: 'G2727_C53727', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53728', 
                        index: 'G2727_C53728', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53765', 
                        index: 'G2727_C53765', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53766', 
                        index: 'G2727_C53766', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53767', 
                        index: 'G2727_C53767', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53768', 
                        index:'G2727_C53768', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G2727_C53768=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2727_C53769', 
                        index:'G2727_C53769', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G2727_C53769=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G2727_C53770', 
                        index:'G2727_C53770', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2727_C53771', 
                        index:'G2727_C53771', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3203&campo=G2727_C53771'
                        }
                    }

                    ,
                    {  
                        name:'G2727_C53772', 
                        index:'G2727_C53772', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2727_C53773', 
                        index:'G2727_C53773', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2727_C53774', 
                        index:'G2727_C53774', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3204&campo=G2727_C53774'
                        }
                    }

                    ,
                    { 
                        name:'G2727_C54123', 
                        index:'G2727_C54123', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3384&campo=G2727_C54123'
                        }
                    }

                    ,
                    { 
                        name:'G2727_C59533', 
                        index:'G2727_C59533', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3613&campo=G2727_C59533'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2727_C53693',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
                ,subGrid: true,
                subGridRowExpanded: function(subgrid_id, row_id) { 
                    // we pass two parameters 
                    // subgrid_id is a id of the div tag created whitin a table data 
                    // the id of this elemenet is a combination of the "sg_" + id of the row 
                    // the row_id is the id of the row 
                    // If we wan to pass additinal parameters to the url we can use 
                    // a method getRowData(row_id) - which returns associative array in type name-value 
                    // here we can easy construct the flowing 
                    $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','CAMPANA','GUIA','CEDULA','NOMBRE DEL TITULAR','BIN','AMPARADOR','CELULAR','TELEFONO 2','TELEFONO 3','TELEFONO 4','DIRECCION 1','CIUDAD 1','DEPARTAMENTO 1','DIRECCION 2','CIUDAD 2','DEPARTAMENTO 2','DIRECCION 3','CIUDAD 3','DEPARTAMENTO 3','DIRECCION 4','CIUDAD 4','DEPARTAMENTO 4','DIRECCION 5','CIUDAD 5','DEPARTAMENTO 5','DIRECCION 6','CIUDAD 6','DEPARTAMENTO 6', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G2751_C54331', 
                                index: 'G2751_C54331', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }
 
                            ,
                            {  
                                name:'G2751_C54332', 
                                index:'G2751_C54332', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }
 
                            ,
                            {  
                                name:'G2751_C54347', 
                                index:'G2751_C54347', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                            ,
                            { 
                                name:'G2751_C54348', 
                                index: 'G2751_C54348', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }
 
                            ,
                            {  
                                name:'G2751_C54349', 
                                index:'G2751_C54349', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                            ,
                            { 
                                name:'G2751_C54350', 
                                index: 'G2751_C54350', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54351', 
                                index: 'G2751_C54351', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54352', 
                                index: 'G2751_C54352', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54353', 
                                index: 'G2751_C54353', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54354', 
                                index: 'G2751_C54354', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54355', 
                                index: 'G2751_C54355', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54356', 
                                index: 'G2751_C54356', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54357', 
                                index: 'G2751_C54357', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54358', 
                                index: 'G2751_C54358', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54359', 
                                index: 'G2751_C54359', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54360', 
                                index: 'G2751_C54360', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54361', 
                                index: 'G2751_C54361', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54362', 
                                index: 'G2751_C54362', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54363', 
                                index: 'G2751_C54363', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54364', 
                                index: 'G2751_C54364', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54365', 
                                index: 'G2751_C54365', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54366', 
                                index: 'G2751_C54366', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54367', 
                                index: 'G2751_C54367', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54368', 
                                index: 'G2751_C54368', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54369', 
                                index: 'G2751_C54369', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54370', 
                                index: 'G2751_C54370', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54371', 
                                index: 'G2751_C54371', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2751_C54372', 
                                index: 'G2751_C54372', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G2727_C53693").val(item.G2727_C53693);

                        $("#G2727_C53694").val(item.G2727_C53694);

                        $("#G2727_C53695").val(item.G2727_C53695);

                        $("#G2727_C53696").val(item.G2727_C53696);

                        $("#G2727_C53698").val(item.G2727_C53698);

                        $("#G2727_C53699").val(item.G2727_C53699);
 
                    $("#G2727_C53682").val(item.G2727_C53682).trigger("change"); 
 
                    $("#G2727_C53683").val(item.G2727_C53683).trigger("change"); 

                        $("#G2727_C53684").val(item.G2727_C53684);

                        $("#G2727_C53685").val(item.G2727_C53685);

                        $("#G2727_C53686").val(item.G2727_C53686);

                        $("#G2727_C53687").val(item.G2727_C53687);

                        $("#G2727_C53688").val(item.G2727_C53688);

                        $("#G2727_C53689").val(item.G2727_C53689);

                        $("#G2727_C53690").val(item.G2727_C53690);
    
                        if(item.G2727_C53691 == 1){
                           $("#G2727_C53691").attr('checked', true);
                        } 
    
                        if(item.G2727_C53692 == 1){
                           $("#G2727_C53692").attr('checked', true);
                        } 

                        $("#G2727_C53700").val(item.G2727_C53700);

                        $("#G2727_C53703").val(item.G2727_C53703);

                        $("#G2727_C53706").val(item.G2727_C53706);

                        $("#G2727_C53709").val(item.G2727_C53709);

                        $("#G2727_C53712").val(item.G2727_C53712);

                        $("#G2727_C53713").val(item.G2727_C53713);

                        $("#G2727_C53715").val(item.G2727_C53715);

                        $("#G2727_C53716").val(item.G2727_C53716);

                        $("#G2727_C53718").val(item.G2727_C53718);

                        $("#G2727_C53719").val(item.G2727_C53719);

                        $("#G2727_C53721").val(item.G2727_C53721);

                        $("#G2727_C53722").val(item.G2727_C53722);

                        $("#G2727_C53724").val(item.G2727_C53724);

                        $("#G2727_C53725").val(item.G2727_C53725);

                        $("#G2727_C53727").val(item.G2727_C53727);

                        $("#G2727_C53728").val(item.G2727_C53728);

                        $("#G2727_C53765").val(item.G2727_C53765);

                        $("#G2727_C53766").val(item.G2727_C53766);

                        $("#G2727_C53767").val(item.G2727_C53767);
 
                    $("#G2727_C53768").attr("opt",item.G2727_C53768);
                    $("#G2727_C53768").val(item.G2727_C53768).trigger("change");
 
                    $("#G2727_C53769").attr("opt",item.G2727_C53769);
                    $("#G2727_C53769").val(item.G2727_C53769).trigger("change");

                        $("#G2727_C53770").val(item.G2727_C53770);
 
                    $("#G2727_C53771").val(item.G2727_C53771).trigger("change"); 

                        $("#G2727_C53772").val(item.G2727_C53772);

                        $("#G2727_C53773").val(item.G2727_C53773);
 
                    $("#G2727_C53774").val(item.G2727_C53774).trigger("change"); 
    
                        if(item.G2727_C53881 == 1){
                           $("#G2727_C53881").attr('checked', true);
                        } 
 
                    $("#G2727_C54123").val(item.G2727_C54123).trigger("change"); 
 
                    $("#G2727_C59533").val(item.G2727_C59533).trigger("change"); 
                        
            cargarHijos_0(
        $("#G2727_C53694").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','CAMPANA','GUIA','CEDULA','NOMBRE DEL TITULAR','BIN','AMPARADOR','CELULAR','TELEFONO 2','TELEFONO 3','TELEFONO 4','DIRECCION 1','CIUDAD 1','DEPARTAMENTO 1','DIRECCION 2','CIUDAD 2','DEPARTAMENTO 2','DIRECCION 3','CIUDAD 3','DEPARTAMENTO 3','DIRECCION 4','CIUDAD 4','DEPARTAMENTO 4','DIRECCION 5','CIUDAD 5','DEPARTAMENTO 5','DIRECCION 6','CIUDAD 6','DEPARTAMENTO 6', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2751_C54331', 
                        index: 'G2751_C54331', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2751_C54332', 
                        index:'G2751_C54332', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }
 
                    ,
                    {  
                        name:'G2751_C54347', 
                        index:'G2751_C54347', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }

                    ,
                    { 
                        name:'G2751_C54348', 
                        index: 'G2751_C54348', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2751_C54349', 
                        index:'G2751_C54349', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }

                    ,
                    { 
                        name:'G2751_C54350', 
                        index: 'G2751_C54350', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54351', 
                        index: 'G2751_C54351', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54352', 
                        index: 'G2751_C54352', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54353', 
                        index: 'G2751_C54353', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54354', 
                        index: 'G2751_C54354', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54355', 
                        index: 'G2751_C54355', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54356', 
                        index: 'G2751_C54356', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54357', 
                        index: 'G2751_C54357', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54358', 
                        index: 'G2751_C54358', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54359', 
                        index: 'G2751_C54359', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54360', 
                        index: 'G2751_C54360', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54361', 
                        index: 'G2751_C54361', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54362', 
                        index: 'G2751_C54362', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54363', 
                        index: 'G2751_C54363', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54364', 
                        index: 'G2751_C54364', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54365', 
                        index: 'G2751_C54365', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54366', 
                        index: 'G2751_C54366', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54367', 
                        index: 'G2751_C54367', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54368', 
                        index: 'G2751_C54368', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54369', 
                        index: 'G2751_C54369', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54370', 
                        index: 'G2751_C54370', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54371', 
                        index: 'G2751_C54371', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2751_C54372', 
                        index: 'G2751_C54372', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G2751_C54331',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'GUIA',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2751&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=54347&formularioPadre=2727<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G2727_C53694").val());
            var id_0 = $("#G2727_C53694").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G2727_C53694").val());
            var id_0 = $("#G2727_C53694").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
