<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2727_ConsInte__b, G2727_FechaInsercion , G2727_Usuario ,  G2727_CodigoMiembro  , G2727_PoblacionOrigen , G2727_EstadoDiligenciamiento ,  G2727_IdLlamada , G2727_C53693 as principal ,G2727_C53693,G2727_C53694,G2727_C53695,G2727_C53696,G2727_C53698,G2727_C53699,G2727_C53682,G2727_C53683,G2727_C53684,G2727_C53685,G2727_C53686,G2727_C53687,G2727_C53688,G2727_C53689,G2727_C53690,G2727_C53700,G2727_C53703,G2727_C53706,G2727_C53709,G2727_C53712,G2727_C53713,G2727_C53715,G2727_C53716,G2727_C53718,G2727_C53719,G2727_C53721,G2727_C53722,G2727_C53724,G2727_C53725,G2727_C53727,G2727_C53728,G2727_C53765,G2727_C53766,G2727_C53767,G2727_C53768,G2727_C53769,G2727_C53770,G2727_C53771,G2727_C53772,G2727_C53773,G2727_C53774,G2727_C54123,G2727_C59533 FROM '.$BaseDatos.'.G2727 WHERE G2727_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2727_C53693'] = $key->G2727_C53693;

                $datos[$i]['G2727_C53694'] = $key->G2727_C53694;

                $datos[$i]['G2727_C53695'] = $key->G2727_C53695;

                $datos[$i]['G2727_C53696'] = $key->G2727_C53696;

                $datos[$i]['G2727_C53698'] = $key->G2727_C53698;

                $datos[$i]['G2727_C53699'] = $key->G2727_C53699;

                $datos[$i]['G2727_C53682'] = $key->G2727_C53682;

                $datos[$i]['G2727_C53683'] = $key->G2727_C53683;

                $datos[$i]['G2727_C53684'] = explode(' ', $key->G2727_C53684)[0];
  
                $hora = '';
                if(!is_null($key->G2727_C53685)){
                    $hora = explode(' ', $key->G2727_C53685)[1];
                }

                $datos[$i]['G2727_C53685'] = $hora;

                $datos[$i]['G2727_C53686'] = $key->G2727_C53686;

                $datos[$i]['G2727_C53687'] = $key->G2727_C53687;

                $datos[$i]['G2727_C53688'] = $key->G2727_C53688;

                $datos[$i]['G2727_C53689'] = $key->G2727_C53689;

                $datos[$i]['G2727_C53690'] = $key->G2727_C53690;

                $datos[$i]['G2727_C53700'] = $key->G2727_C53700;

                $datos[$i]['G2727_C53703'] = $key->G2727_C53703;

                $datos[$i]['G2727_C53706'] = $key->G2727_C53706;

                $datos[$i]['G2727_C53709'] = $key->G2727_C53709;

                $datos[$i]['G2727_C53712'] = $key->G2727_C53712;

                $datos[$i]['G2727_C53713'] = $key->G2727_C53713;

                $datos[$i]['G2727_C53715'] = $key->G2727_C53715;

                $datos[$i]['G2727_C53716'] = $key->G2727_C53716;

                $datos[$i]['G2727_C53718'] = $key->G2727_C53718;

                $datos[$i]['G2727_C53719'] = $key->G2727_C53719;

                $datos[$i]['G2727_C53721'] = $key->G2727_C53721;

                $datos[$i]['G2727_C53722'] = $key->G2727_C53722;

                $datos[$i]['G2727_C53724'] = $key->G2727_C53724;

                $datos[$i]['G2727_C53725'] = $key->G2727_C53725;

                $datos[$i]['G2727_C53727'] = $key->G2727_C53727;

                $datos[$i]['G2727_C53728'] = $key->G2727_C53728;

                $datos[$i]['G2727_C53765'] = $key->G2727_C53765;

                $datos[$i]['G2727_C53766'] = $key->G2727_C53766;

                $datos[$i]['G2727_C53767'] = $key->G2727_C53767;

                $datos[$i]['G2727_C53768'] = $key->G2727_C53768;

                $datos[$i]['G2727_C53769'] = $key->G2727_C53769;

                $datos[$i]['G2727_C53770'] = $key->G2727_C53770;

                $datos[$i]['G2727_C53771'] = $key->G2727_C53771;

                $datos[$i]['G2727_C53772'] = explode(' ', $key->G2727_C53772)[0];

                $datos[$i]['G2727_C53773'] = $key->G2727_C53773;

                $datos[$i]['G2727_C53774'] = $key->G2727_C53774;

                $datos[$i]['G2727_C54123'] = $key->G2727_C54123;

                $datos[$i]['G2727_C59533'] = $key->G2727_C59533;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2727";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2727_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2727_ConsInte__b as id,  G2727_C53693 as camp1 , G2727_C53694 as camp2 
                     FROM ".$BaseDatos.".G2727  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2727_ConsInte__b as id,  G2727_C53693 as camp1 , G2727_C53694 as camp2  
                    FROM ".$BaseDatos.".G2727  JOIN ".$BaseDatos.".G2727_M".$_POST['muestra']." ON G2727_ConsInte__b = G2727_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2727_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2727_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2727_C53693 LIKE '%".$B."%' OR G2727_C53694 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2727_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2727_C53768'])){
                                $Ysql = "SELECT G2781_ConsInte__b as id, G2781_C55143 as text FROM ".$BaseDatos.".G2781 WHERE G2781_C55143 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2727_C53768"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2781 WHERE G2781_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2727_C53768"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G2727_C53769'])){
                                $Ysql = "SELECT G2781_ConsInte__b as id,  as text FROM ".$BaseDatos.".G2781 WHERE  LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2727_C53769"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G WHERE G_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2727_C53769"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2727");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2727_ConsInte__b, G2727_FechaInsercion , G2727_Usuario ,  G2727_CodigoMiembro  , G2727_PoblacionOrigen , G2727_EstadoDiligenciamiento ,  G2727_IdLlamada , G2727_C53693 as principal ,G2727_C53693,G2727_C53694,G2727_C53695,G2727_C53696,G2727_C53698,G2727_C53699, a.LISOPC_Nombre____b as G2727_C53682, b.LISOPC_Nombre____b as G2727_C53683,G2727_C53684,G2727_C53685,G2727_C53686,G2727_C53687,G2727_C53688,G2727_C53689,G2727_C53690,G2727_C53700,G2727_C53703,G2727_C53706,G2727_C53709,G2727_C53712,G2727_C53713,G2727_C53715,G2727_C53716,G2727_C53718,G2727_C53719,G2727_C53721,G2727_C53722,G2727_C53724,G2727_C53725,G2727_C53727,G2727_C53728,G2727_C53765,G2727_C53766,G2727_C53767, G2781_C55143,G2727_C53770, c.LISOPC_Nombre____b as G2727_C53771,G2727_C53772,G2727_C53773, d.LISOPC_Nombre____b as G2727_C53774, e.LISOPC_Nombre____b as G2727_C54123, f.LISOPC_Nombre____b as G2727_C59533 FROM '.$BaseDatos.'.G2727 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2727_C53682 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2727_C53683 LEFT JOIN '.$BaseDatos.'.G2781 ON G2781_ConsInte__b  =  G2727_C53768 LEFT JOIN '.$BaseDatos.'.G2781 ON G2781_ConsInte__b  =  G2727_C53769 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2727_C53771 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2727_C53774 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2727_C54123 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2727_C59533';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2727_C53685)){
                    $hora_a = explode(' ', $fila->G2727_C53685)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2727_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2727_ConsInte__b , ($fila->G2727_C53693) , ($fila->G2727_C53694) , ($fila->G2727_C53695) , ($fila->G2727_C53696) , ($fila->G2727_C53698) , ($fila->G2727_C53699) , ($fila->G2727_C53682) , ($fila->G2727_C53683) , explode(' ', $fila->G2727_C53684)[0] , $hora_a , ($fila->G2727_C53686) , ($fila->G2727_C53687) , ($fila->G2727_C53688) , ($fila->G2727_C53689) , ($fila->G2727_C53690) , ($fila->G2727_C53700) , ($fila->G2727_C53703) , ($fila->G2727_C53706) , ($fila->G2727_C53709) , ($fila->G2727_C53712) , ($fila->G2727_C53713) , ($fila->G2727_C53715) , ($fila->G2727_C53716) , ($fila->G2727_C53718) , ($fila->G2727_C53719) , ($fila->G2727_C53721) , ($fila->G2727_C53722) , ($fila->G2727_C53724) , ($fila->G2727_C53725) , ($fila->G2727_C53727) , ($fila->G2727_C53728) , ($fila->G2727_C53765) , ($fila->G2727_C53766) , ($fila->G2727_C53767) , ($fila->G2781_C55143) , ($fila->G2727_C53770) , ($fila->G2727_C53771) , explode(' ', $fila->G2727_C53772)[0] , ($fila->G2727_C53773) , ($fila->G2727_C53774) , ($fila->G2727_C54123) , ($fila->G2727_C59533) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2727 WHERE G2727_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2727";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2727_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2727_ConsInte__b as id,  G2727_C53693 as camp1 , G2727_C53694 as camp2  FROM '.$BaseDatos.'.G2727 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2727_ConsInte__b as id,  G2727_C53693 as camp1 , G2727_C53694 as camp2  
                    FROM ".$BaseDatos.".G2727  JOIN ".$BaseDatos.".G2727_M".$_POST['muestra']." ON G2727_ConsInte__b = G2727_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2727_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2727_C53693 LIKE "%'.$B.'%" OR G2727_C53694 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2727_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2727 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2727(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2727_C53693"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53693 = '".$_POST["G2727_C53693"]."'";
                $LsqlI .= $separador."G2727_C53693";
                $LsqlV .= $separador."'".$_POST["G2727_C53693"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53694"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53694 = '".$_POST["G2727_C53694"]."'";
                $LsqlI .= $separador."G2727_C53694";
                $LsqlV .= $separador."'".$_POST["G2727_C53694"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53695"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53695 = '".$_POST["G2727_C53695"]."'";
                $LsqlI .= $separador."G2727_C53695";
                $LsqlV .= $separador."'".$_POST["G2727_C53695"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53696"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53696 = '".$_POST["G2727_C53696"]."'";
                $LsqlI .= $separador."G2727_C53696";
                $LsqlV .= $separador."'".$_POST["G2727_C53696"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53698"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53698 = '".$_POST["G2727_C53698"]."'";
                $LsqlI .= $separador."G2727_C53698";
                $LsqlV .= $separador."'".$_POST["G2727_C53698"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53699"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53699 = '".$_POST["G2727_C53699"]."'";
                $LsqlI .= $separador."G2727_C53699";
                $LsqlV .= $separador."'".$_POST["G2727_C53699"]."'";
                $validar = 1;
            }
             
 
            $G2727_C53682 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2727_C53682 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2727_C53682 = ".$G2727_C53682;
                    $LsqlI .= $separador." G2727_C53682";
                    $LsqlV .= $separador.$G2727_C53682;
                    $validar = 1;

                    
                }
            }
 
            $G2727_C53683 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2727_C53683 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2727_C53683 = ".$G2727_C53683;
                    $LsqlI .= $separador." G2727_C53683";
                    $LsqlV .= $separador.$G2727_C53683;
                    $validar = 1;
                }
            }
 
            $G2727_C53684 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2727_C53684 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2727_C53684 = ".$G2727_C53684;
                    $LsqlI .= $separador." G2727_C53684";
                    $LsqlV .= $separador.$G2727_C53684;
                    $validar = 1;
                }
            }
 
            $G2727_C53685 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2727_C53685 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2727_C53685 = ".$G2727_C53685;
                    $LsqlI .= $separador." G2727_C53685";
                    $LsqlV .= $separador.$G2727_C53685;
                    $validar = 1;
                }
            }
 
            $G2727_C53686 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2727_C53686 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2727_C53686 = ".$G2727_C53686;
                    $LsqlI .= $separador." G2727_C53686";
                    $LsqlV .= $separador.$G2727_C53686;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2727_C53687"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53687 = '".$_POST["G2727_C53687"]."'";
                $LsqlI .= $separador."G2727_C53687";
                $LsqlV .= $separador."'".$_POST["G2727_C53687"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53688"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53688 = '".$_POST["G2727_C53688"]."'";
                $LsqlI .= $separador."G2727_C53688";
                $LsqlV .= $separador."'".$_POST["G2727_C53688"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53689"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53689 = '".$_POST["G2727_C53689"]."'";
                $LsqlI .= $separador."G2727_C53689";
                $LsqlV .= $separador."'".$_POST["G2727_C53689"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53690"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53690 = '".$_POST["G2727_C53690"]."'";
                $LsqlI .= $separador."G2727_C53690";
                $LsqlV .= $separador."'".$_POST["G2727_C53690"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53691"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53691 = '".$_POST["G2727_C53691"]."'";
                $LsqlI .= $separador."G2727_C53691";
                $LsqlV .= $separador."'".$_POST["G2727_C53691"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53692"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53692 = '".$_POST["G2727_C53692"]."'";
                $LsqlI .= $separador."G2727_C53692";
                $LsqlV .= $separador."'".$_POST["G2727_C53692"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53700"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53700 = '".$_POST["G2727_C53700"]."'";
                $LsqlI .= $separador."G2727_C53700";
                $LsqlV .= $separador."'".$_POST["G2727_C53700"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53703"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53703 = '".$_POST["G2727_C53703"]."'";
                $LsqlI .= $separador."G2727_C53703";
                $LsqlV .= $separador."'".$_POST["G2727_C53703"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53706"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53706 = '".$_POST["G2727_C53706"]."'";
                $LsqlI .= $separador."G2727_C53706";
                $LsqlV .= $separador."'".$_POST["G2727_C53706"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53709"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53709 = '".$_POST["G2727_C53709"]."'";
                $LsqlI .= $separador."G2727_C53709";
                $LsqlV .= $separador."'".$_POST["G2727_C53709"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53712"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53712 = '".$_POST["G2727_C53712"]."'";
                $LsqlI .= $separador."G2727_C53712";
                $LsqlV .= $separador."'".$_POST["G2727_C53712"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53713"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53713 = '".$_POST["G2727_C53713"]."'";
                $LsqlI .= $separador."G2727_C53713";
                $LsqlV .= $separador."'".$_POST["G2727_C53713"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53715 = '".$_POST["G2727_C53715"]."'";
                $LsqlI .= $separador."G2727_C53715";
                $LsqlV .= $separador."'".$_POST["G2727_C53715"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53716"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53716 = '".$_POST["G2727_C53716"]."'";
                $LsqlI .= $separador."G2727_C53716";
                $LsqlV .= $separador."'".$_POST["G2727_C53716"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53718"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53718 = '".$_POST["G2727_C53718"]."'";
                $LsqlI .= $separador."G2727_C53718";
                $LsqlV .= $separador."'".$_POST["G2727_C53718"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53719"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53719 = '".$_POST["G2727_C53719"]."'";
                $LsqlI .= $separador."G2727_C53719";
                $LsqlV .= $separador."'".$_POST["G2727_C53719"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53721"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53721 = '".$_POST["G2727_C53721"]."'";
                $LsqlI .= $separador."G2727_C53721";
                $LsqlV .= $separador."'".$_POST["G2727_C53721"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53722"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53722 = '".$_POST["G2727_C53722"]."'";
                $LsqlI .= $separador."G2727_C53722";
                $LsqlV .= $separador."'".$_POST["G2727_C53722"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53724"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53724 = '".$_POST["G2727_C53724"]."'";
                $LsqlI .= $separador."G2727_C53724";
                $LsqlV .= $separador."'".$_POST["G2727_C53724"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53725"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53725 = '".$_POST["G2727_C53725"]."'";
                $LsqlI .= $separador."G2727_C53725";
                $LsqlV .= $separador."'".$_POST["G2727_C53725"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53727"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53727 = '".$_POST["G2727_C53727"]."'";
                $LsqlI .= $separador."G2727_C53727";
                $LsqlV .= $separador."'".$_POST["G2727_C53727"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53728"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53728 = '".$_POST["G2727_C53728"]."'";
                $LsqlI .= $separador."G2727_C53728";
                $LsqlV .= $separador."'".$_POST["G2727_C53728"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53765"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53765 = '".$_POST["G2727_C53765"]."'";
                $LsqlI .= $separador."G2727_C53765";
                $LsqlV .= $separador."'".$_POST["G2727_C53765"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53766"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53766 = '".$_POST["G2727_C53766"]."'";
                $LsqlI .= $separador."G2727_C53766";
                $LsqlV .= $separador."'".$_POST["G2727_C53766"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53767"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53767 = '".$_POST["G2727_C53767"]."'";
                $LsqlI .= $separador."G2727_C53767";
                $LsqlV .= $separador."'".$_POST["G2727_C53767"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53768"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53768 = '".$_POST["G2727_C53768"]."'";
                $LsqlI .= $separador."G2727_C53768";
                $LsqlV .= $separador."'".$_POST["G2727_C53768"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53769"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53769 = '".$_POST["G2727_C53769"]."'";
                $LsqlI .= $separador."G2727_C53769";
                $LsqlV .= $separador."'".$_POST["G2727_C53769"]."'";
                $validar = 1;
            }
             
  
            $G2727_C53770 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2727_C53770"])){
                if($_POST["G2727_C53770"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2727_C53770 = $_POST["G2727_C53770"];
                    $LsqlU .= $separador." G2727_C53770 = ".$G2727_C53770."";
                    $LsqlI .= $separador." G2727_C53770";
                    $LsqlV .= $separador.$G2727_C53770;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2727_C53771"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53771 = '".$_POST["G2727_C53771"]."'";
                $LsqlI .= $separador."G2727_C53771";
                $LsqlV .= $separador."'".$_POST["G2727_C53771"]."'";
                $validar = 1;
            }
             
 
            $G2727_C53772 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2727_C53772"])){    
                if($_POST["G2727_C53772"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2727_C53772"]);
                    if(count($tieneHora) > 1){
                        $G2727_C53772 = "'".$_POST["G2727_C53772"]."'";
                    }else{
                        $G2727_C53772 = "'".str_replace(' ', '',$_POST["G2727_C53772"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2727_C53772 = ".$G2727_C53772;
                    $LsqlI .= $separador." G2727_C53772";
                    $LsqlV .= $separador.$G2727_C53772;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2727_C53773"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53773 = '".$_POST["G2727_C53773"]."'";
                $LsqlI .= $separador."G2727_C53773";
                $LsqlV .= $separador."'".$_POST["G2727_C53773"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53774"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53774 = '".$_POST["G2727_C53774"]."'";
                $LsqlI .= $separador."G2727_C53774";
                $LsqlV .= $separador."'".$_POST["G2727_C53774"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C53881"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C53881 = '".$_POST["G2727_C53881"]."'";
                $LsqlI .= $separador."G2727_C53881";
                $LsqlV .= $separador."'".$_POST["G2727_C53881"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C54123"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C54123 = '".$_POST["G2727_C54123"]."'";
                $LsqlI .= $separador."G2727_C54123";
                $LsqlV .= $separador."'".$_POST["G2727_C54123"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2727_C59533"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_C59533 = '".$_POST["G2727_C59533"]."'";
                $LsqlI .= $separador."G2727_C59533";
                $LsqlV .= $separador."'".$_POST["G2727_C59533"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2727_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2727_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2727_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2727_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2727_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2727_Usuario , G2727_FechaInsercion, G2727_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2727_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2727 WHERE G2727_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2751_ConsInte__b, G2751_C54331, G2751_C54332, G2751_C54347, G2751_C54348, G2751_C54349, G2751_C54350, G2751_C54351, G2751_C54352, G2751_C54353, G2751_C54354, G2751_C54355, G2751_C54356, G2751_C54357, G2751_C54358, G2751_C54359, G2751_C54360, G2751_C54361, G2751_C54362, G2751_C54363, G2751_C54364, G2751_C54365, G2751_C54366, G2751_C54367, G2751_C54368, G2751_C54369, G2751_C54370, G2751_C54371, G2751_C54372 FROM ".$BaseDatos.".G2751  ";

        $SQL .= " WHERE G2751_C54347 = '".$numero."'"; 

        $SQL .= " ORDER BY G2751_C54331";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2751_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2751_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2751_C54331)."</cell>";

                echo "<cell>". $fila->G2751_C54332."</cell>"; 

                echo "<cell>". $fila->G2751_C54347."</cell>"; 

                echo "<cell>". ($fila->G2751_C54348)."</cell>";

                echo "<cell>". $fila->G2751_C54349."</cell>"; 

                echo "<cell>". ($fila->G2751_C54350)."</cell>";

                echo "<cell>". ($fila->G2751_C54351)."</cell>";

                echo "<cell>". ($fila->G2751_C54352)."</cell>";

                echo "<cell>". ($fila->G2751_C54353)."</cell>";

                echo "<cell>". ($fila->G2751_C54354)."</cell>";

                echo "<cell>". ($fila->G2751_C54355)."</cell>";

                echo "<cell>". ($fila->G2751_C54356)."</cell>";

                echo "<cell>". ($fila->G2751_C54357)."</cell>";

                echo "<cell>". ($fila->G2751_C54358)."</cell>";

                echo "<cell>". ($fila->G2751_C54359)."</cell>";

                echo "<cell>". ($fila->G2751_C54360)."</cell>";

                echo "<cell>". ($fila->G2751_C54361)."</cell>";

                echo "<cell>". ($fila->G2751_C54362)."</cell>";

                echo "<cell>". ($fila->G2751_C54363)."</cell>";

                echo "<cell>". ($fila->G2751_C54364)."</cell>";

                echo "<cell>". ($fila->G2751_C54365)."</cell>";

                echo "<cell>". ($fila->G2751_C54366)."</cell>";

                echo "<cell>". ($fila->G2751_C54367)."</cell>";

                echo "<cell>". ($fila->G2751_C54368)."</cell>";

                echo "<cell>". ($fila->G2751_C54369)."</cell>";

                echo "<cell>". ($fila->G2751_C54370)."</cell>";

                echo "<cell>". ($fila->G2751_C54371)."</cell>";

                echo "<cell>". ($fila->G2751_C54372)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2751 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2751(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2751_C54331"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54331 = '".$_POST["G2751_C54331"]."'";
                    $LsqlI .= $separador."G2751_C54331";
                    $LsqlV .= $separador."'".$_POST["G2751_C54331"]."'";
                    $validar = 1;
                }

                                                                               
 
                $G2751_C54332= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2751_C54332"])){    
                    if($_POST["G2751_C54332"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2751_C54332 = $_POST["G2751_C54332"];
                        $LsqlU .= $separador." G2751_C54332 = '".$G2751_C54332."'";
                        $LsqlI .= $separador." G2751_C54332";
                        $LsqlV .= $separador."'".$G2751_C54332."'";
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2751_C54348"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54348 = '".$_POST["G2751_C54348"]."'";
                    $LsqlI .= $separador."G2751_C54348";
                    $LsqlV .= $separador."'".$_POST["G2751_C54348"]."'";
                    $validar = 1;
                }

                                                                               
 
                $G2751_C54349= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2751_C54349"])){    
                    if($_POST["G2751_C54349"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2751_C54349 = $_POST["G2751_C54349"];
                        $LsqlU .= $separador." G2751_C54349 = '".$G2751_C54349."'";
                        $LsqlI .= $separador." G2751_C54349";
                        $LsqlV .= $separador."'".$G2751_C54349."'";
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2751_C54350"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54350 = '".$_POST["G2751_C54350"]."'";
                    $LsqlI .= $separador."G2751_C54350";
                    $LsqlV .= $separador."'".$_POST["G2751_C54350"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54351"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54351 = '".$_POST["G2751_C54351"]."'";
                    $LsqlI .= $separador."G2751_C54351";
                    $LsqlV .= $separador."'".$_POST["G2751_C54351"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54352"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54352 = '".$_POST["G2751_C54352"]."'";
                    $LsqlI .= $separador."G2751_C54352";
                    $LsqlV .= $separador."'".$_POST["G2751_C54352"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54353"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54353 = '".$_POST["G2751_C54353"]."'";
                    $LsqlI .= $separador."G2751_C54353";
                    $LsqlV .= $separador."'".$_POST["G2751_C54353"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54354"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54354 = '".$_POST["G2751_C54354"]."'";
                    $LsqlI .= $separador."G2751_C54354";
                    $LsqlV .= $separador."'".$_POST["G2751_C54354"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54355"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54355 = '".$_POST["G2751_C54355"]."'";
                    $LsqlI .= $separador."G2751_C54355";
                    $LsqlV .= $separador."'".$_POST["G2751_C54355"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54356"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54356 = '".$_POST["G2751_C54356"]."'";
                    $LsqlI .= $separador."G2751_C54356";
                    $LsqlV .= $separador."'".$_POST["G2751_C54356"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54357"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54357 = '".$_POST["G2751_C54357"]."'";
                    $LsqlI .= $separador."G2751_C54357";
                    $LsqlV .= $separador."'".$_POST["G2751_C54357"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54358"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54358 = '".$_POST["G2751_C54358"]."'";
                    $LsqlI .= $separador."G2751_C54358";
                    $LsqlV .= $separador."'".$_POST["G2751_C54358"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54359"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54359 = '".$_POST["G2751_C54359"]."'";
                    $LsqlI .= $separador."G2751_C54359";
                    $LsqlV .= $separador."'".$_POST["G2751_C54359"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54360"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54360 = '".$_POST["G2751_C54360"]."'";
                    $LsqlI .= $separador."G2751_C54360";
                    $LsqlV .= $separador."'".$_POST["G2751_C54360"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54361"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54361 = '".$_POST["G2751_C54361"]."'";
                    $LsqlI .= $separador."G2751_C54361";
                    $LsqlV .= $separador."'".$_POST["G2751_C54361"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54362"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54362 = '".$_POST["G2751_C54362"]."'";
                    $LsqlI .= $separador."G2751_C54362";
                    $LsqlV .= $separador."'".$_POST["G2751_C54362"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54363"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54363 = '".$_POST["G2751_C54363"]."'";
                    $LsqlI .= $separador."G2751_C54363";
                    $LsqlV .= $separador."'".$_POST["G2751_C54363"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54364"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54364 = '".$_POST["G2751_C54364"]."'";
                    $LsqlI .= $separador."G2751_C54364";
                    $LsqlV .= $separador."'".$_POST["G2751_C54364"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54365"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54365 = '".$_POST["G2751_C54365"]."'";
                    $LsqlI .= $separador."G2751_C54365";
                    $LsqlV .= $separador."'".$_POST["G2751_C54365"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54366"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54366 = '".$_POST["G2751_C54366"]."'";
                    $LsqlI .= $separador."G2751_C54366";
                    $LsqlV .= $separador."'".$_POST["G2751_C54366"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54367"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54367 = '".$_POST["G2751_C54367"]."'";
                    $LsqlI .= $separador."G2751_C54367";
                    $LsqlV .= $separador."'".$_POST["G2751_C54367"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54368"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54368 = '".$_POST["G2751_C54368"]."'";
                    $LsqlI .= $separador."G2751_C54368";
                    $LsqlV .= $separador."'".$_POST["G2751_C54368"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54369"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54369 = '".$_POST["G2751_C54369"]."'";
                    $LsqlI .= $separador."G2751_C54369";
                    $LsqlV .= $separador."'".$_POST["G2751_C54369"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54370"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54370 = '".$_POST["G2751_C54370"]."'";
                    $LsqlI .= $separador."G2751_C54370";
                    $LsqlV .= $separador."'".$_POST["G2751_C54370"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54371"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54371 = '".$_POST["G2751_C54371"]."'";
                    $LsqlI .= $separador."G2751_C54371";
                    $LsqlV .= $separador."'".$_POST["G2751_C54371"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2751_C54372"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2751_C54372 = '".$_POST["G2751_C54372"]."'";
                    $LsqlI .= $separador."G2751_C54372";
                    $LsqlV .= $separador."'".$_POST["G2751_C54372"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2751_C54347 = $numero;
                    $LsqlU .= ", G2751_C54347 = ".$G2751_C54347."";
                    $LsqlI .= ", G2751_C54347";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2751_Usuario ,  G2751_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2751_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2751 WHERE  G2751_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
