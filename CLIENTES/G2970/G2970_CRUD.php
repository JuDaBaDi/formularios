<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2970_ConsInte__b, G2970_FechaInsercion , G2970_Usuario ,  G2970_CodigoMiembro  , G2970_PoblacionOrigen , G2970_EstadoDiligenciamiento ,  G2970_IdLlamada , G2970_C59425 as principal ,G2970_C59425,G2970_C59426,G2970_C59427,G2970_C59428,G2970_C59429,G2970_C59430,G2970_C59431,G2970_C59432,G2970_C59433,G2970_C59414,G2970_C59415,G2970_C59416,G2970_C59417,G2970_C59418,G2970_C59419,G2970_C59420,G2970_C59421,G2970_C59422,G2970_C59434,G2970_C59435,G2970_C59436 FROM '.$BaseDatos.'.G2970 WHERE G2970_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2970_C59425'] = $key->G2970_C59425;

                $datos[$i]['G2970_C59426'] = $key->G2970_C59426;

                $datos[$i]['G2970_C59427'] = $key->G2970_C59427;

                $datos[$i]['G2970_C59428'] = $key->G2970_C59428;

                $datos[$i]['G2970_C59429'] = $key->G2970_C59429;

                $datos[$i]['G2970_C59430'] = $key->G2970_C59430;

                $datos[$i]['G2970_C59431'] = $key->G2970_C59431;

                $datos[$i]['G2970_C59432'] = $key->G2970_C59432;

                $datos[$i]['G2970_C59433'] = $key->G2970_C59433;

                $datos[$i]['G2970_C59414'] = $key->G2970_C59414;

                $datos[$i]['G2970_C59415'] = $key->G2970_C59415;

                $datos[$i]['G2970_C59416'] = explode(' ', $key->G2970_C59416)[0];
  
                $hora = '';
                if(!is_null($key->G2970_C59417)){
                    $hora = explode(' ', $key->G2970_C59417)[1];
                }

                $datos[$i]['G2970_C59417'] = $hora;

                $datos[$i]['G2970_C59418'] = $key->G2970_C59418;

                $datos[$i]['G2970_C59419'] = $key->G2970_C59419;

                $datos[$i]['G2970_C59420'] = $key->G2970_C59420;

                $datos[$i]['G2970_C59421'] = $key->G2970_C59421;

                $datos[$i]['G2970_C59422'] = $key->G2970_C59422;

                $datos[$i]['G2970_C59434'] = $key->G2970_C59434;

                $datos[$i]['G2970_C59435'] = explode(' ', $key->G2970_C59435)[0];

                $datos[$i]['G2970_C59436'] = $key->G2970_C59436;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }

        if (isset($_GET["intValorTotal"])) {

            $intValorTotal = $_POST["intValorTotal"];

            $strSQL_t = "SELECT SUM(G2969_C59410) AS cantidad FROM ".$BaseDatos.".G2969 WHERE G2969_C59406 = '".$intValorTotal."'";

            if ($resSQL_t = $mysqli->query($strSQL_t)) {

                $objSQL_t = $resSQL_t->fetch_object();

                echo $objSQL_t->cantidad;

            }else{

                echo "0";

            }

        }
        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2970";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2970_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2970_ConsInte__b as id,  G2970_C59425 as camp1 , G2970_C59434 as camp2 
                     FROM ".$BaseDatos.".G2970  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2970_ConsInte__b as id,  G2970_C59425 as camp1 , G2970_C59434 as camp2  
                    FROM ".$BaseDatos.".G2970  JOIN ".$BaseDatos.".G2970_M".$_POST['muestra']." ON G2970_ConsInte__b = G2970_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2970_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2970_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2970_C59425 LIKE '%".$B."%' OR G2970_C59434 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2970_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2970");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2970_ConsInte__b, G2970_FechaInsercion , G2970_Usuario ,  G2970_CodigoMiembro  , G2970_PoblacionOrigen , G2970_EstadoDiligenciamiento ,  G2970_IdLlamada , G2970_C59425 as principal ,G2970_C59425,G2970_C59426,G2970_C59427,G2970_C59428,G2970_C59429,G2970_C59430,G2970_C59431,G2970_C59432,G2970_C59433, a.LISOPC_Nombre____b as G2970_C59414, b.LISOPC_Nombre____b as G2970_C59415,G2970_C59416,G2970_C59417,G2970_C59418,G2970_C59419,G2970_C59420,G2970_C59421,G2970_C59422,G2970_C59434,G2970_C59435,G2970_C59436 FROM '.$BaseDatos.'.G2970 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2970_C59414 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2970_C59415';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2970_C59417)){
                    $hora_a = explode(' ', $fila->G2970_C59417)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2970_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2970_ConsInte__b , ($fila->G2970_C59425) , ($fila->G2970_C59426) , ($fila->G2970_C59427) , ($fila->G2970_C59428) , ($fila->G2970_C59429) , ($fila->G2970_C59430) , ($fila->G2970_C59431) , ($fila->G2970_C59432) , ($fila->G2970_C59433) , ($fila->G2970_C59414) , ($fila->G2970_C59415) , explode(' ', $fila->G2970_C59416)[0] , $hora_a , ($fila->G2970_C59418) , ($fila->G2970_C59419) , ($fila->G2970_C59420) , ($fila->G2970_C59421) , ($fila->G2970_C59422) , ($fila->G2970_C59434) , explode(' ', $fila->G2970_C59435)[0] , ($fila->G2970_C59436) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2970 WHERE G2970_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2970";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2970_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2970_ConsInte__b as id,  G2970_C59425 as camp1 , G2970_C59434 as camp2  FROM '.$BaseDatos.'.G2970 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2970_ConsInte__b as id,  G2970_C59425 as camp1 , G2970_C59434 as camp2  
                    FROM ".$BaseDatos.".G2970  JOIN ".$BaseDatos.".G2970_M".$_POST['muestra']." ON G2970_ConsInte__b = G2970_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2970_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2970_C59425 LIKE "%'.$B.'%" OR G2970_C59434 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2970_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2970 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2970(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2970_C59425"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59425 = '".$_POST["G2970_C59425"]."'";
                $LsqlI .= $separador."G2970_C59425";
                $LsqlV .= $separador."'".$_POST["G2970_C59425"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59426"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59426 = '".$_POST["G2970_C59426"]."'";
                $LsqlI .= $separador."G2970_C59426";
                $LsqlV .= $separador."'".$_POST["G2970_C59426"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59427"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59427 = '".$_POST["G2970_C59427"]."'";
                $LsqlI .= $separador."G2970_C59427";
                $LsqlV .= $separador."'".$_POST["G2970_C59427"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59428"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59428 = '".$_POST["G2970_C59428"]."'";
                $LsqlI .= $separador."G2970_C59428";
                $LsqlV .= $separador."'".$_POST["G2970_C59428"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59429"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59429 = '".$_POST["G2970_C59429"]."'";
                $LsqlI .= $separador."G2970_C59429";
                $LsqlV .= $separador."'".$_POST["G2970_C59429"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59430"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59430 = '".$_POST["G2970_C59430"]."'";
                $LsqlI .= $separador."G2970_C59430";
                $LsqlV .= $separador."'".$_POST["G2970_C59430"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59431"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59431 = '".$_POST["G2970_C59431"]."'";
                $LsqlI .= $separador."G2970_C59431";
                $LsqlV .= $separador."'".$_POST["G2970_C59431"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59432"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59432 = '".$_POST["G2970_C59432"]."'";
                $LsqlI .= $separador."G2970_C59432";
                $LsqlV .= $separador."'".$_POST["G2970_C59432"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59433"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59433 = '".$_POST["G2970_C59433"]."'";
                $LsqlI .= $separador."G2970_C59433";
                $LsqlV .= $separador."'".$_POST["G2970_C59433"]."'";
                $validar = 1;
            }
             
 
            $G2970_C59414 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2970_C59414 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2970_C59414 = ".$G2970_C59414;
                    $LsqlI .= $separador." G2970_C59414";
                    $LsqlV .= $separador.$G2970_C59414;
                    $validar = 1;

                    
                }
            }
 
            $G2970_C59415 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2970_C59415 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2970_C59415 = ".$G2970_C59415;
                    $LsqlI .= $separador." G2970_C59415";
                    $LsqlV .= $separador.$G2970_C59415;
                    $validar = 1;
                }
            }
 
            $G2970_C59416 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2970_C59416 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2970_C59416 = ".$G2970_C59416;
                    $LsqlI .= $separador." G2970_C59416";
                    $LsqlV .= $separador.$G2970_C59416;
                    $validar = 1;
                }
            }
 
            $G2970_C59417 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2970_C59417 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2970_C59417 = ".$G2970_C59417;
                    $LsqlI .= $separador." G2970_C59417";
                    $LsqlV .= $separador.$G2970_C59417;
                    $validar = 1;
                }
            }
 
            $G2970_C59418 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2970_C59418 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2970_C59418 = ".$G2970_C59418;
                    $LsqlI .= $separador." G2970_C59418";
                    $LsqlV .= $separador.$G2970_C59418;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2970_C59419"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59419 = '".$_POST["G2970_C59419"]."'";
                $LsqlI .= $separador."G2970_C59419";
                $LsqlV .= $separador."'".$_POST["G2970_C59419"]."'";
                $validar = 1;
            }
             
  
            //JDBD - Fecha oculta Script
            if(isset($_POST["G2970_C59420"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date("Y-m-d H:i:s");

                $LsqlU .= $separador."G2970_C59420 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2970_C59420";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            //JDBD - Hora oculta Script
            if(isset($_POST["G2970_C59421"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date("H:i:s");

                $LsqlU .= $separador."G2970_C59421 = '".$strHora_t."'";
                $LsqlI .= $separador."G2970_C59421";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59422"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59422 = '".$_POST["G2970_C59422"]."'";
                $LsqlI .= $separador."G2970_C59422";
                $LsqlV .= $separador."'".$_POST["G2970_C59422"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59434"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59434 = '".$_POST["G2970_C59434"]."'";
                $LsqlI .= $separador."G2970_C59434";
                $LsqlV .= $separador."'".$_POST["G2970_C59434"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2970_C59502"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_C59502 = '".$_POST["G2970_C59502"]."'";
                $LsqlI .= $separador."G2970_C59502";
                $LsqlV .= $separador."'".$_POST["G2970_C59502"]."'";
                $validar = 1;
            }
             
 
            $G2970_C59435 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2970_C59435"])){    
                if($_POST["G2970_C59435"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2970_C59435"]);
                    if(count($tieneHora) > 1){
                        $G2970_C59435 = "'".$_POST["G2970_C59435"]."'";
                    }else{
                        $G2970_C59435 = "'".str_replace(' ', '',$_POST["G2970_C59435"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2970_C59435 = ".$G2970_C59435;
                    $LsqlI .= $separador." G2970_C59435";
                    $LsqlV .= $separador.$G2970_C59435;
                    $validar = 1;
                }
            }
  
            $G2970_C59436 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2970_C59436"])){
                if($_POST["G2970_C59436"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2970_C59436 = $_POST["G2970_C59436"];
                    $LsqlU .= $separador." G2970_C59436 = ".$G2970_C59436."";
                    $LsqlI .= $separador." G2970_C59436";
                    $LsqlV .= $separador.$G2970_C59436;
                    $validar = 1;
                }
            }

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2970_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2970_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2970_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2970_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2970_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2970_Usuario , G2970_FechaInsercion, G2970_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2970_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2970 WHERE G2970_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2970_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2970 WHERE G2970_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2969_ConsInte__b, G2969_C59406, G2969_C59407, G2969_C59408, G2969_C59409, G2969_C59410, f.LISOPC_Nombre____b as  G2969_C59413, G2969_C59411, G2969_C59412 FROM ".$BaseDatos.".G2969  LEFT JOIN ".$BaseDatos_systema.".LISOPC as f ON f.LISOPC_ConsInte__b =  G2969_C59413 ";

        $SQL .= " WHERE G2969_C59406 = '".$numero."'"; 

        $SQL .= " ORDER BY G2969_C59406";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2969_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2969_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2969_C59406)."</cell>";

                echo "<cell>". ($fila->G2969_C59407)."</cell>";

                echo "<cell>". $fila->G2969_C59408."</cell>"; 

                echo "<cell>". $fila->G2969_C59409."</cell>"; 

                echo "<cell>". $fila->G2969_C59410."</cell>"; 

                echo "<cell>". ($fila->G2969_C59413)."</cell>";

                if($fila->G2969_C59411 != ''){
                    echo "<cell>". explode(' ', $fila->G2969_C59411)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2969_C59412)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2969 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2969(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2969_C59407"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2969_C59407 = '".$_POST["G2969_C59407"]."'";
                    $LsqlI .= $separador."G2969_C59407";
                    $LsqlV .= $separador."'".$_POST["G2969_C59407"]."'";
                    $validar = 1;
                }

                                                                               
  
                $G2969_C59408 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2969_C59408"])){    
                    if($_POST["G2969_C59408"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2969_C59408 = $_POST["G2969_C59408"];
                        $LsqlU .= $separador." G2969_C59408 = '".$G2969_C59408."'";
                        $LsqlI .= $separador." G2969_C59408";
                        $LsqlV .= $separador."'".$G2969_C59408."'";
                        $validar = 1;
                    }
                }
  
                $G2969_C59409 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2969_C59409"])){    
                    if($_POST["G2969_C59409"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2969_C59409 = $_POST["G2969_C59409"];
                        $LsqlU .= $separador." G2969_C59409 = '".$G2969_C59409."'";
                        $LsqlI .= $separador." G2969_C59409";
                        $LsqlV .= $separador."'".$G2969_C59409."'";
                        $validar = 1;
                    }
                }
  
                $G2969_C59410 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2969_C59410"])){    
                    if($_POST["G2969_C59410"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2969_C59410 = $_POST["G2969_C59410"];
                        $LsqlU .= $separador." G2969_C59410 = '".$G2969_C59410."'";
                        $LsqlI .= $separador." G2969_C59410";
                        $LsqlV .= $separador."'".$G2969_C59410."'";
                        $validar = 1;
                    }
                }
 
                if(isset($_POST["G2969_C59413"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2969_C59413 = '".$_POST["G2969_C59413"]."'";
                    $LsqlI .= $separador."G2969_C59413";
                    $LsqlV .= $separador."'".$_POST["G2969_C59413"]."'";
                    $validar = 1;
                }

                $G2969_C59411 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2969_C59411"])){    
                    if($_POST["G2969_C59411"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2969_C59411 = "'".str_replace(' ', '',$_POST["G2969_C59411"])." 00:00:00'";
                        $LsqlU .= $separador." G2969_C59411 = ".$G2969_C59411;
                        $LsqlI .= $separador." G2969_C59411";
                        $LsqlV .= $separador.$G2969_C59411;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2969_C59412"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2969_C59412 = '".$_POST["G2969_C59412"]."'";
                    $LsqlI .= $separador."G2969_C59412";
                    $LsqlV .= $separador."'".$_POST["G2969_C59412"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2969_C59406 = $numero;
                    $LsqlU .= ", G2969_C59406 = ".$G2969_C59406."";
                    $LsqlI .= ", G2969_C59406";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2969_Usuario ,  G2969_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2969_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2969 WHERE  G2969_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
