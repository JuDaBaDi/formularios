<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2974_ConsInte__b, G2974_FechaInsercion , G2974_Usuario ,  G2974_CodigoMiembro  , G2974_PoblacionOrigen , G2974_EstadoDiligenciamiento ,  G2974_IdLlamada , G2974_C59865 as principal ,G2974_C59472,G2974_C59473,G2974_C59474,G2974_C59475,G2974_C59476,G2974_C59477,G2974_C59478,G2974_C59479,G2974_C59480,G2974_C59481,G2974_C59498,G2974_C59499,G2974_C59500,G2974_C59871,G2974_C59866,G2974_C59873,G2974_C59865,G2974_C59872,G2974_C59867,G2974_C59874,G2974_C59868,G2974_C59875,G2974_C59869,G2974_C59876,G2974_C59870,G2974_C59877 FROM '.$BaseDatos.'.G2974 WHERE G2974_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2974_C59472'] = $key->G2974_C59472;

                $datos[$i]['G2974_C59473'] = $key->G2974_C59473;

                $datos[$i]['G2974_C59474'] = explode(' ', $key->G2974_C59474)[0];
  
                $hora = '';
                if(!is_null($key->G2974_C59475)){
                    $hora = explode(' ', $key->G2974_C59475)[1];
                }

                $datos[$i]['G2974_C59475'] = $hora;

                $datos[$i]['G2974_C59476'] = $key->G2974_C59476;

                $datos[$i]['G2974_C59477'] = $key->G2974_C59477;

                $datos[$i]['G2974_C59478'] = $key->G2974_C59478;

                $datos[$i]['G2974_C59479'] = $key->G2974_C59479;

                $datos[$i]['G2974_C59480'] = $key->G2974_C59480;

                $datos[$i]['G2974_C59481'] = $key->G2974_C59481;

                $datos[$i]['G2974_C59498'] = $key->G2974_C59498;

                $datos[$i]['G2974_C59499'] = explode(' ', $key->G2974_C59499)[0];

                $datos[$i]['G2974_C59500'] = $key->G2974_C59500;

                $datos[$i]['G2974_C59871'] = $key->G2974_C59871;

                $datos[$i]['G2974_C59866'] = $key->G2974_C59866;

                $datos[$i]['G2974_C59873'] = $key->G2974_C59873;

                $datos[$i]['G2974_C59865'] = $key->G2974_C59865;

                $datos[$i]['G2974_C59872'] = $key->G2974_C59872;

                $datos[$i]['G2974_C59867'] = $key->G2974_C59867;

                $datos[$i]['G2974_C59874'] = $key->G2974_C59874;

                $datos[$i]['G2974_C59868'] = $key->G2974_C59868;

                $datos[$i]['G2974_C59875'] = $key->G2974_C59875;

                $datos[$i]['G2974_C59869'] = $key->G2974_C59869;

                $datos[$i]['G2974_C59876'] = explode(' ', $key->G2974_C59876)[0];

                $datos[$i]['G2974_C59870'] = $key->G2974_C59870;

                $datos[$i]['G2974_C59877'] = $key->G2974_C59877;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2974";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2974_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2974_ConsInte__b as id,  G2974_C59871 as camp2 , b.LISOPC_Nombre____b as camp1 
                     FROM ".$BaseDatos.".G2974  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G2974_C59865 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2974_ConsInte__b as id,  G2974_C59871 as camp2 , b.LISOPC_Nombre____b as camp1  
                    FROM ".$BaseDatos.".G2974  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G2974_C59865 JOIN ".$BaseDatos.".G2974_M".$_POST['muestra']." ON G2974_ConsInte__b = G2974_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2974_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2974_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2974_C59871 LIKE '%".$B."%' OR G2974_C59865 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2974_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2974");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2974_ConsInte__b, G2974_FechaInsercion , G2974_Usuario ,  G2974_CodigoMiembro  , G2974_PoblacionOrigen , G2974_EstadoDiligenciamiento ,  G2974_IdLlamada , G2974_C59865 as principal , a.LISOPC_Nombre____b as G2974_C59472, b.LISOPC_Nombre____b as G2974_C59473,G2974_C59474,G2974_C59475,G2974_C59476,G2974_C59477,G2974_C59478,G2974_C59479,G2974_C59480,G2974_C59481, c.LISOPC_Nombre____b as G2974_C59498,G2974_C59499,G2974_C59500,G2974_C59871,G2974_C59866, d.LISOPC_Nombre____b as G2974_C59873, e.LISOPC_Nombre____b as G2974_C59865,G2974_C59872,G2974_C59867,G2974_C59874,G2974_C59868,G2974_C59875,G2974_C59869,G2974_C59876,G2974_C59870,G2974_C59877 FROM '.$BaseDatos.'.G2974 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2974_C59472 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2974_C59473 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2974_C59498 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2974_C59873 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2974_C59865';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2974_C59475)){
                    $hora_a = explode(' ', $fila->G2974_C59475)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2974_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2974_ConsInte__b , ($fila->G2974_C59472) , ($fila->G2974_C59473) , explode(' ', $fila->G2974_C59474)[0] , $hora_a , ($fila->G2974_C59476) , ($fila->G2974_C59477) , ($fila->G2974_C59478) , ($fila->G2974_C59479) , ($fila->G2974_C59480) , ($fila->G2974_C59481) , ($fila->G2974_C59498) , explode(' ', $fila->G2974_C59499)[0] , ($fila->G2974_C59500) , ($fila->G2974_C59871) , ($fila->G2974_C59866) , ($fila->G2974_C59873) , ($fila->G2974_C59865) , ($fila->G2974_C59872) , ($fila->G2974_C59867) , ($fila->G2974_C59874) , ($fila->G2974_C59868) , ($fila->G2974_C59875) , ($fila->G2974_C59869) , explode(' ', $fila->G2974_C59876)[0] , ($fila->G2974_C59870) , ($fila->G2974_C59877) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2974 WHERE G2974_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2974";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2974_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2974_ConsInte__b as id,  G2974_C59871 as camp2 , b.LISOPC_Nombre____b as camp1  FROM '.$BaseDatos.'.G2974 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2974_ConsInte__b as id,  G2974_C59871 as camp2 , b.LISOPC_Nombre____b as camp1  
                    FROM ".$BaseDatos.".G2974  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G2974_C59865 JOIN ".$BaseDatos.".G2974_M".$_POST['muestra']." ON G2974_ConsInte__b = G2974_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2974_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2974_C59871 LIKE "%'.$B.'%" OR G2974_C59865 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2974_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2974 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2974(";
            $LsqlV = " VALUES ("; 
 
            $G2974_C59472 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2974_C59472 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2974_C59472 = ".$G2974_C59472;
                    $LsqlI .= $separador." G2974_C59472";
                    $LsqlV .= $separador.$G2974_C59472;
                    $validar = 1;

                    
                }
            }
 
            $G2974_C59473 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2974_C59473 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2974_C59473 = ".$G2974_C59473;
                    $LsqlI .= $separador." G2974_C59473";
                    $LsqlV .= $separador.$G2974_C59473;
                    $validar = 1;
                }
            }
 
            $G2974_C59474 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2974_C59474 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2974_C59474 = ".$G2974_C59474;
                    $LsqlI .= $separador." G2974_C59474";
                    $LsqlV .= $separador.$G2974_C59474;
                    $validar = 1;
                }
            }
 
            $G2974_C59475 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2974_C59475 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2974_C59475 = ".$G2974_C59475;
                    $LsqlI .= $separador." G2974_C59475";
                    $LsqlV .= $separador.$G2974_C59475;
                    $validar = 1;
                }
            }
 
            $G2974_C59476 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2974_C59476 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2974_C59476 = ".$G2974_C59476;
                    $LsqlI .= $separador." G2974_C59476";
                    $LsqlV .= $separador.$G2974_C59476;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2974_C59477"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59477 = '".$_POST["G2974_C59477"]."'";
                $LsqlI .= $separador."G2974_C59477";
                $LsqlV .= $separador."'".$_POST["G2974_C59477"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59478"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59478 = '".$_POST["G2974_C59478"]."'";
                $LsqlI .= $separador."G2974_C59478";
                $LsqlV .= $separador."'".$_POST["G2974_C59478"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59479"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59479 = '".$_POST["G2974_C59479"]."'";
                $LsqlI .= $separador."G2974_C59479";
                $LsqlV .= $separador."'".$_POST["G2974_C59479"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59480"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59480 = '".$_POST["G2974_C59480"]."'";
                $LsqlI .= $separador."G2974_C59480";
                $LsqlV .= $separador."'".$_POST["G2974_C59480"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59481"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59481 = '".$_POST["G2974_C59481"]."'";
                $LsqlI .= $separador."G2974_C59481";
                $LsqlV .= $separador."'".$_POST["G2974_C59481"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59482"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59482 = '".$_POST["G2974_C59482"]."'";
                $LsqlI .= $separador."G2974_C59482";
                $LsqlV .= $separador."'".$_POST["G2974_C59482"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59498"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59498 = '".$_POST["G2974_C59498"]."'";
                $LsqlI .= $separador."G2974_C59498";
                $LsqlV .= $separador."'".$_POST["G2974_C59498"]."'";
                $validar = 1;
            }
             
 
            $G2974_C59499 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2974_C59499"])){    
                if($_POST["G2974_C59499"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2974_C59499"]);
                    if(count($tieneHora) > 1){
                        $G2974_C59499 = "'".$_POST["G2974_C59499"]."'";
                    }else{
                        $G2974_C59499 = "'".str_replace(' ', '',$_POST["G2974_C59499"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2974_C59499 = ".$G2974_C59499;
                    $LsqlI .= $separador." G2974_C59499";
                    $LsqlV .= $separador.$G2974_C59499;
                    $validar = 1;
                }
            }
  
            $G2974_C59500 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2974_C59500"])){
                if($_POST["G2974_C59500"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2974_C59500 = $_POST["G2974_C59500"];
                    $LsqlU .= $separador." G2974_C59500 = ".$G2974_C59500."";
                    $LsqlI .= $separador." G2974_C59500";
                    $LsqlV .= $separador.$G2974_C59500;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2974_C59871"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59871 = '".$_POST["G2974_C59871"]."'";
                $LsqlI .= $separador."G2974_C59871";
                $LsqlV .= $separador."'".$_POST["G2974_C59871"]."'";
                $validar = 1;
            }
             
  
            $G2974_C59866 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2974_C59866"])){
                if($_POST["G2974_C59866"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2974_C59866 = $_POST["G2974_C59866"];
                    $LsqlU .= $separador." G2974_C59866 = ".$G2974_C59866."";
                    $LsqlI .= $separador." G2974_C59866";
                    $LsqlV .= $separador.$G2974_C59866;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2974_C59873"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59873 = '".$_POST["G2974_C59873"]."'";
                $LsqlI .= $separador."G2974_C59873";
                $LsqlV .= $separador."'".$_POST["G2974_C59873"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59865"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59865 = '".$_POST["G2974_C59865"]."'";
                $LsqlI .= $separador."G2974_C59865";
                $LsqlV .= $separador."'".$_POST["G2974_C59865"]."'";
                $validar = 1;
            }
             
  
            $G2974_C59872 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2974_C59872"])){
                if($_POST["G2974_C59872"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2974_C59872 = $_POST["G2974_C59872"];
                    $LsqlU .= $separador." G2974_C59872 = ".$G2974_C59872."";
                    $LsqlI .= $separador." G2974_C59872";
                    $LsqlV .= $separador.$G2974_C59872;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2974_C59867"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59867 = '".$_POST["G2974_C59867"]."'";
                $LsqlI .= $separador."G2974_C59867";
                $LsqlV .= $separador."'".$_POST["G2974_C59867"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59874"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59874 = '".$_POST["G2974_C59874"]."'";
                $LsqlI .= $separador."G2974_C59874";
                $LsqlV .= $separador."'".$_POST["G2974_C59874"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59868"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59868 = '".$_POST["G2974_C59868"]."'";
                $LsqlI .= $separador."G2974_C59868";
                $LsqlV .= $separador."'".$_POST["G2974_C59868"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59875"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59875 = '".$_POST["G2974_C59875"]."'";
                $LsqlI .= $separador."G2974_C59875";
                $LsqlV .= $separador."'".$_POST["G2974_C59875"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59869"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59869 = '".$_POST["G2974_C59869"]."'";
                $LsqlI .= $separador."G2974_C59869";
                $LsqlV .= $separador."'".$_POST["G2974_C59869"]."'";
                $validar = 1;
            }
             
 
            $G2974_C59876 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2974_C59876"])){    
                if($_POST["G2974_C59876"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2974_C59876"]);
                    if(count($tieneHora) > 1){
                        $G2974_C59876 = "'".$_POST["G2974_C59876"]."'";
                    }else{
                        $G2974_C59876 = "'".str_replace(' ', '',$_POST["G2974_C59876"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2974_C59876 = ".$G2974_C59876;
                    $LsqlI .= $separador." G2974_C59876";
                    $LsqlV .= $separador.$G2974_C59876;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2974_C59870"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59870 = '".$_POST["G2974_C59870"]."'";
                $LsqlI .= $separador."G2974_C59870";
                $LsqlV .= $separador."'".$_POST["G2974_C59870"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2974_C59877"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_C59877 = '".$_POST["G2974_C59877"]."'";
                $LsqlI .= $separador."G2974_C59877";
                $LsqlV .= $separador."'".$_POST["G2974_C59877"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2974_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2974_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2974_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2974_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2974_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2974_Usuario , G2974_FechaInsercion, G2974_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2974_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2974 WHERE G2974_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  
    if (isset($_GET["intValorTotal"])) {

        $intValorTotal = $_POST["intValorTotal"];

        $strSQL_t = "SELECT SUM(G2973_C59470) AS cantidad FROM ".$BaseDatos.".G2973 WHERE G2973_C59465 = '".$intValorTotal."'";

        if ($resSQL_t = $mysqli->query($strSQL_t)) {

            $objSQL_t = $resSQL_t->fetch_object();

            echo $objSQL_t->cantidad;

        }else{

            echo "0";

        }

    }

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2974_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2974 WHERE G2974_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2973_ConsInte__b, G2973_C59465, G2973_C59466, G2973_C59467, G2973_C59468, G2973_C59469, G2973_C59503, G2973_C59470, G2973_C59471 FROM ".$BaseDatos.".G2973  ";

        $SQL .= " WHERE G2973_C59465 = '".$numero."'"; 

        $SQL .= " ORDER BY G2973_C62058";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2973_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2973_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2973_C59465)."</cell>";

                echo "<cell>". ($fila->G2973_C59466)."</cell>";

                if($fila->G2973_C59467 != ''){
                    echo "<cell>". explode(' ', $fila->G2973_C59467)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". $fila->G2973_C59468."</cell>"; 

                echo "<cell>". $fila->G2973_C59469."</cell>"; 

                echo "<cell>". $fila->G2973_C59470."</cell>"; 

                echo "<cell>". ($fila->G2973_C59471)."</cell>";
            echo "</row>"; 
        }
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2973 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2973(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2973_C59466"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2973_C59466 = '".$_POST["G2973_C59466"]."'";
                    $LsqlI .= $separador."G2973_C59466";
                    $LsqlV .= $separador."'".$_POST["G2973_C59466"]."'";
                    $validar = 1;
                }

                                                                               

                $G2973_C59467 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2973_C59467"])){    
                    if($_POST["G2973_C59467"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2973_C59467 = "'".str_replace(' ', '',$_POST["G2973_C59467"])." 00:00:00'";
                        $LsqlU .= $separador." G2973_C59467 = ".$G2973_C59467;
                        $LsqlI .= $separador." G2973_C59467";
                        $LsqlV .= $separador.$G2973_C59467;
                        $validar = 1;
                    }
                }
  
                $G2973_C59468 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2973_C59468"])){    
                    if($_POST["G2973_C59468"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2973_C59468 = $_POST["G2973_C59468"];
                        $LsqlU .= $separador." G2973_C59468 = '".$G2973_C59468."'";
                        $LsqlI .= $separador." G2973_C59468";
                        $LsqlV .= $separador."'".$G2973_C59468."'";
                        $validar = 1;
                    }
                }
  
                $G2973_C59469 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2973_C59469"])){    
                    if($_POST["G2973_C59469"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2973_C59469 = $_POST["G2973_C59469"];
                        $LsqlU .= $separador." G2973_C59469 = '".$G2973_C59469."'";
                        $LsqlI .= $separador." G2973_C59469";
                        $LsqlV .= $separador."'".$G2973_C59469."'";
                        $validar = 1;
                    }
                }
  
                $G2973_C59503 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2973_C59503"])){    
                    if($_POST["G2973_C59503"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2973_C59503 = $_POST["G2973_C59503"];
                        $LsqlU .= $separador." G2973_C59503 = '".$G2973_C59503."'";
                        $LsqlI .= $separador." G2973_C59503";
                        $LsqlV .= $separador."'".$G2973_C59503."'";
                        $validar = 1;
                    }
                }
  
                $G2973_C59470 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2973_C59470"])){    
                    if($_POST["G2973_C59470"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2973_C59470 = $_POST["G2973_C59470"];
                        $LsqlU .= $separador." G2973_C59470 = '".$G2973_C59470."'";
                        $LsqlI .= $separador." G2973_C59470";
                        $LsqlV .= $separador."'".$G2973_C59470."'";
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2973_C59471"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2973_C59471 = '".$_POST["G2973_C59471"]."'";
                    $LsqlI .= $separador."G2973_C59471";
                    $LsqlV .= $separador."'".$_POST["G2973_C59471"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2973_C59465 = $numero;
                    $LsqlU .= ", G2973_C59465 = ".$G2973_C59465."";
                    $LsqlI .= ", G2973_C59465";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2973_Usuario ,  G2973_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2973_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2973 WHERE  G2973_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
