
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
            <div class="modal-footer">
                <div class="pull-left">

                        <button class="btn btn-default" id="deleteModal" onclick="enviarAcciones('delete')">
                            <i class="fa fa-trash"></i> 
                        </button>
                        <button class="btn btn-default" id="editModal" onclick="enviarAcciones('edit')">
                            <i class="fa fa-edit"></i>
                        </button>
                                                        
                        <button class="btn btn-default" id="SaveModal" onclick="enviarAcciones('Save')">
                            <i class="fa fa-save"></i>
                        </button>
                                        
                </div>
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
            </div>

        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3148/G3148_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : $_SESSION["IDENTIFICACION"];
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3148_ConsInte__b as id, G3148_C62805 as camp1 , G3148_C63858 as camp2 FROM ".$BaseDatos.".G3148  WHERE G3148_Usuario = ".$idUsuario." ORDER BY G3148_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3148_ConsInte__b as id, G3148_C62805 as camp1 , G3148_C63858 as camp2 FROM ".$BaseDatos.".G3148  ORDER BY G3148_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3148_ConsInte__b as id, G3148_C62805 as camp1 , G3148_C63858 as camp2 FROM ".$BaseDatos.".G3148 JOIN ".$BaseDatos.".G3148_M".$resultEstpas->muestr." ON G3148_ConsInte__b = G3148_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3148_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3148_ConsInte__b as id, G3148_C62805 as camp1 , G3148_C63858 as camp2 FROM ".$BaseDatos.".G3148 JOIN ".$BaseDatos.".G3148_M".$resultEstpas->muestr." ON G3148_ConsInte__b = G3148_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3148_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3148_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3148_ConsInte__b as id, G3148_C62805 as camp1 , G3148_C63858 as camp2 FROM ".$BaseDatos.".G3148  ORDER BY G3148_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    
    //JDBD - Validamos si se pudo obtener el dato principal.
    if ($nombre = $mysqli->query($XLsql)) {

        $nombreUsuario = NULL;
        //echo $XLsql;
        while ($key = $nombre->fetch_object()) {
            echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
            $nombreUsuario = $key->nombre;
            break;
        } 


        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                        
            $data = array(  "strToken_t" => $_GET['token'], 
                            "strIdGestion_t" => $_GET['id_gestion_cbx'],
                            "strDatoPrincipal_t" => $nombreUsuario,
                            "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
            $data_string = json_encode($data);    

            $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                      
            ); 
            //recogemos la respuesta
            $respuesta = curl_exec ($ch);
            //o el error, por si falla
            $error = curl_error($ch);
            //y finalmente cerramos curl
            //echo "Respuesta =>  ". $respuesta;
            //echo "<br/>Error => ".$error;
            //include "Log.class.php";
            //$log = new Log("log", "./Log/");
            //$log->insert($error, $respuesta, false, true, false);
            //echo "nada";
            curl_close ($ch);
        }

    }else{
        echo "<script>alert('NO SE PUDO OBTENER EL DATO PRINCIPAL DEL REGISTRO.');</script>";
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<input type="hidden" name="intConsInteBd" id="intConsInteBd" value="<?php if(isset($_GET["user"])) { echo $_GET["user"]; }else{ echo "-1";  } ?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover" id="tbHistorico">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="9786" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9786c">
                Saludo Inicial
            </a>
        </h4>
        
    </div>
    <div id="s_9786c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr. Sra (Nombre del Paciente) Autoriza usted de manera voluntaria a SES, para el tratamiento de sus datos personales, de acuerdo  a nuestra politica, la cual puede consultar en la pagina www. Hospitaldecaldas.com.co</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9734" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9734c">
                GENERAL
            </a>
        </h4>
        
    </div>
    <div id="s_9734c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62805" id="LblG3148_C62805">Numero de documento</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62805" value="<?php if (isset($_GET['G3148_C62805'])) {
                            echo $_GET['G3148_C62805'];
                        } ?>"  name="G3148_C62805"  placeholder="Numero de documento"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3148_C62806" id="LblG3148_C62806">tipo de identificación</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3148_C62806" id="G3148_C62806">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3871 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62807" id="LblG3148_C62807">Nombre completo?</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62807" value="<?php if (isset($_GET['G3148_C62807'])) {
                            echo $_GET['G3148_C62807'];
                        } ?>"  name="G3148_C62807"  placeholder="Nombre completo?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3148_C62808" id="LblG3148_C62808">Eps o entindad por la que consulta</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3148_C62808" id="G3148_C62808">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62809" id="LblG3148_C62809">teléfono de contacto 1</label><div class="input-group">
                            <input type="text" maxlength="253" maxlength="253" class="form-control input-sm" id="G3148_C62809" value="<?php if (isset($_GET['G3148_C62809'])) {
                            echo $_GET['G3148_C62809'];
                        } ?>"  name="G3148_C62809"  placeholder="teléfono de contacto 1">
                            <div class="input-group-addon telefono" style="cursor:pointer" id="TLF_G3148_C62809" title="Click para llamar">
                        <i class="fa fa-phone"></i>
                    </div>
                            
                            
                        </div></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62904" id="LblG3148_C62904">teléfono de contacto 2</label><div class="input-group">
                            <input type="text" maxlength="253" maxlength="253" class="form-control input-sm" id="G3148_C62904" value="<?php if (isset($_GET['G3148_C62904'])) {
                            echo $_GET['G3148_C62904'];
                        } ?>"  name="G3148_C62904"  placeholder="teléfono de contacto 2">
                            <div class="input-group-addon telefono" style="cursor:pointer" id="TLF_G3148_C62904" title="Click para llamar">
                        <i class="fa fa-phone"></i>
                    </div>
                            
                            
                        </div></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62812" id="LblG3148_C62812">correo electronico? </label><input type="email" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62812" value="<?php if (isset($_GET['G3148_C62812'])) {
                            echo $_GET['G3148_C62812'];
                        } ?>"  name="G3148_C62812"  placeholder="correo electronico? "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G3148_C63070" id="LblG3148_C63070">Ciudad de donde se comunica?</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G3148_C63070" id="G3148_C63070">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9736" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9736c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9736c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62798" id="LblG3148_C62798">Agente</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62798" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G3148_C62798"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62799" id="LblG3148_C62799">Fecha</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62799" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G3148_C62799"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62800" id="LblG3148_C62800">Hora</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62800" value="<?php echo date('H:i:s');?>" readonly name="G3148_C62800"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3148_C62801" id="LblG3148_C62801">Campaña</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3148_C62801" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G3148_C62801"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9894" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9894c">
                CANAL DE ATENCION
            </a>
        </h4>
        
    </div>
    <div id="s_9894c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3148_C64115" id="LblG3148_C64115">CANAL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3148_C64115" id="G3148_C64115">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3935 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra. (Nombre del paciente) Esta usted interesado en su consulta por telemedicina o presencial?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  id="9735" >
<h3 class="box box-title"></h3>

</div>

<div  id="9739" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3148_C63858" id="LblG3148_C63858">ID GESTION</label><input type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"]) && isset($_GET["view"])){  if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 63858")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 63858");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }
                             }?>" readonly name="G3148_C63858" id="G3148_C63858" placeholder="ID GESTION"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 

<div class=row>
                        <div class="col-md-12 col-xs-12">
                            
<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">CITA PRESENCIAL</a>
        </li>

        <li class="">
            <a href="#tab_1" data-toggle="tab" id="tabs_click_1">CITA TELEMEDICINA</a>
        </li>

        <li class="">
            <a href="#tab_2" data-toggle="tab" id="tabs_click_2">INFORMACIÓN GENERAL</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear CITA PRESENCIAL" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_1"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless1" width="100%">
            </table>
            <div id="pagerDetalles1">
            </div> 
            <button title="Crear CITA TELEMEDICINA" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_1"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_2"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless2" width="100%">
            </table>
            <div id="pagerDetalles2">
            </div> 
            <button title="Crear INFORMACIÓN GENERAL" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_2"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
                        </div>
                    </div>
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G3148_C62793">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3870;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G3148_C62793">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3870;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G3148_C62794">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G3148_C62795" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G3148_C62796" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G3148_C62797" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G3148/G3148_eventos.js"></script>
<script type="text/javascript" src="formularios/G3148/G3148_extender_funcionalidad.php"></script><?php require_once "G3148_extender_funcionalidad.php";?><?php require_once "formularios/enviarSms_Mail.php";?>
<script type="text/javascript">

    function scrollTopUp(){

        $("#G3148_C62805").focus();

    }

    function enviarAcciones(tipo){

        sendMessage2(tipo);


    }    

  function habilitarBotonesModal(tipo){

    $("#editModal").hide();
    $("#deleteModal").hide();
    $("#SaveModal").hide();

    if (tipo == "add") {

        $("#SaveModal").show();
        $("#deleteModal").show();

    }else if(tipo == "edit"){

        $("#editModal").show();
        $("#deleteModal").show();
        $("#SaveModal").show();

    }

  }

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        
        if (e.data == "Cierrame") {
            $("#refrescarGrillas").click();
        }
        
        
    });

    var sendMessage2 = function(msg) {

        var iframeEl = document.getElementById('frameContenedor');
        // Make sure you are sending a string, and to stringify JSON
        iframeEl.contentWindow.postMessage(msg, '*');
    };
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje;
        sendMessage('' + mensaje);
    });

    //JDBD - Funcion para descargar los adjuntos
    function bajarAdjunto(id){

        var strURL_t = $("#"+id).attr("adjunto");

        if (strURL_t != "") {

            location.href='<?=$url_crud;?>?adjunto='+strURL_t;
            
        }


    }
    
    

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3148_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3148_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3148_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3148_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3148_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3148_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3148_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3148_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3148_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
        
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            $("#G3148_C62806").val("0").trigger("change");
            //JDBD - Damos el valor asignado por defecto a este campo.
            $("#G3148_C62800").val("<?php echo date('H:i:s');?>");
            $("#G3148_C64115").val("0").trigger("change");
            //JDBD - Creamos un nuevo id incrementable y lo asignamos a este campo
            $.ajax({
                url:'<?=$url_crud;?>',
                type:'POST',
                data:{INCTB:"si"},
                success:function(data){
                    $("#G3148_C63858").val(data);
                }
            });
            
            
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
             
                $("#G3148_C62805").val(item.G3148_C62805); 
                $("#G3148_C62806").val(item.G3148_C62806).trigger("change");  
                $("#G3148_C62807").val(item.G3148_C62807); 
                $("#G3148_C62808").attr("opt",item.G3148_C62808);
                $("#G3148_C62808").val(item.G3148_C62808).trigger("change"); 
                $("#G3148_C62809").val(item.G3148_C62809); 
                $("#G3148_C62904").val(item.G3148_C62904); 
                $("#G3148_C62812").val(item.G3148_C62812); 
                $("#G3148_C63070").attr("opt",item.G3148_C63070);
                $("#G3148_C63070").val(item.G3148_C63070).trigger("change"); 
                $("#G3148_C62793").val(item.G3148_C62793).trigger("change");  
                $("#G3148_C62794").val(item.G3148_C62794).trigger("change");  
                $("#G3148_C62795").val(item.G3148_C62795); 
                $("#G3148_C62796").val(item.G3148_C62796); 
                $("#G3148_C62797").val(item.G3148_C62797); 
                $("#G3148_C62798").val(item.G3148_C62798); 
                $("#G3148_C62799").val(item.G3148_C62799); 
                $("#G3148_C62800").val(item.G3148_C62800); 
                $("#G3148_C62801").val(item.G3148_C62801); 
                $("#G3148_C63858").val(item.G3148_C63858);   
                if(item.G3148_C63069 == 1){
                    $("#G3148_C63069").attr('checked', true);
                }  
                $("#G3148_C64115").val(item.G3148_C64115).trigger("change");    
                if(item.G3148_C64116 == 1){
                    $("#G3148_C64116").attr('checked', true);
                } 
                
                cargarHijos_0(
        $("#G3148_C63858").val());
                cargarHijos_1(
        $("#G3148_C63858").val());
                cargarHijos_2(
        $("#G3148_C63858").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_1").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_2").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_1").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_2").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');

            $.jgrid.gridUnload('#tablaDatosDetalless1');

            $.jgrid.gridUnload('#tablaDatosDetalless2');
            
        $("#btnLlamar_0").attr('padre', $("#G3148_C63858").val());
            var id_0 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        $("#btnLlamar_1").attr('padre', $("#G3148_C63858").val());
            var id_1 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            cargarHijos_1(id_1);
        $("#btnLlamar_2").attr('padre', $("#G3148_C63858").val());
            var id_2 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            cargarHijos_2(id_2);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G3148_C63858").val());
            var id_0 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {

            habilitarBotonesModal("add");

            event.preventDefault(); 
            var padre = $("#G3148_C63858").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&formaDetalle=si&formularioPadre=3148&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64154<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    

                    if(($("#G3148_C62805").val() == "") && $("#G3148_C62805").prop("disabled") == false){
                        alertify.error('Numero de documento debe ser diligenciado');
                        $("#G3148_C62805").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }

                    if(($("#G3148_C62806").val()==0 || $("#G3148_C62806").val() == null || $("#G3148_C62806").val() == -1) && $("#G3148_C62806").prop("disabled") == false){
                        alertify.error('tipo de identificación debe ser diligenciado');
                        $("#G3148_C62806").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }

                    if(($("#G3148_C62807").val() == "") && $("#G3148_C62807").prop("disabled") == false){
                        alertify.error('Nombre completo? debe ser diligenciado');
                        $("#G3148_C62807").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }

                    if(($("#G3148_C62808").val()==0 || $("#G3148_C62808").val() == null || $("#G3148_C62808").val() == -1) && $("#G3148_C62808").prop("disabled") == false){
                        alertify.error('Eps o entindad por la que consulta debe ser diligenciado');
                        $("#G3148_C62808").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }

                    if(($("#G3148_C62809").val() == "") && $("#G3148_C62809").prop("disabled") == false){
                        alertify.error('teléfono de contacto 1 debe ser diligenciado');
                        $("#G3148_C62809").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }

                    if(($("#G3148_C63070").val()==0 || $("#G3148_C63070").val() == null || $("#G3148_C63070").val() == -1) && $("#G3148_C63070").prop("disabled") == false){
                        alertify.error('Ciudad de donde se comunica? debe ser diligenciado');
                        $("#G3148_C63070").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }

                    if(($("#G3148_C64115").val()==0 || $("#G3148_C64115").val() == null || $("#G3148_C64115").val() == -1) && $("#G3148_C64115").prop("disabled") == false){
                        alertify.error('CANAL debe ser diligenciado');
                        $("#G3148_C64115").closest(".form-group").addClass("has-error");
                        valido = 1;
                    }
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&formaDetalle=si&formularioPadre=3148&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64154&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=3148&pincheCampo=64154&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }

            scrollTopUp();
        });

        $("#tabs_click_1").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless1'); 
            $("#btnLlamar_1").attr('padre', $("#G3148_C63858").val());
            var id_1 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            cargarHijos_1(id_1);
        });

        $("#btnLlamar_1").click(function( event ) {

            habilitarBotonesModal("add");
            event.preventDefault(); 
            var padre = $("#G3148_C63858").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3199&view=si&formaDetalle=si&formularioPadre=3148&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64194<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    

            if(($("#G3148_C62805").val() == "") && $("#G3148_C62805").prop("disabled") == false){
                alertify.error('Numero de documento debe ser diligenciado');
                $("#G3148_C62805").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62806").val()==0 || $("#G3148_C62806").val() == null || $("#G3148_C62806").val() == -1) && $("#G3148_C62806").prop("disabled") == false){
                alertify.error('tipo de identificación debe ser diligenciado');
                $("#G3148_C62806").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62807").val() == "") && $("#G3148_C62807").prop("disabled") == false){
                alertify.error('Nombre completo? debe ser diligenciado');
                $("#G3148_C62807").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62808").val()==0 || $("#G3148_C62808").val() == null || $("#G3148_C62808").val() == -1) && $("#G3148_C62808").prop("disabled") == false){
                alertify.error('Eps o entindad por la que consulta debe ser diligenciado');
                $("#G3148_C62808").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62809").val() == "") && $("#G3148_C62809").prop("disabled") == false){
                alertify.error('teléfono de contacto 1 debe ser diligenciado');
                $("#G3148_C62809").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C63070").val()==0 || $("#G3148_C63070").val() == null || $("#G3148_C63070").val() == -1) && $("#G3148_C63070").prop("disabled") == false){
                alertify.error('Ciudad de donde se comunica? debe ser diligenciado');
                $("#G3148_C63070").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C64115").val()==0 || $("#G3148_C64115").val() == null || $("#G3148_C64115").val() == -1) && $("#G3148_C64115").prop("disabled") == false){
                alertify.error('CANAL debe ser diligenciado');
                $("#G3148_C64115").closest(".form-group").addClass("has-error");
                valido = 1;
            }
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3199&view=si&formaDetalle=si&formularioPadre=3148&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64194&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3199&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=3148&pincheCampo=64194&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }

            scrollTopUp();
        });

        $("#tabs_click_2").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless2'); 
            $("#btnLlamar_2").attr('padre', $("#G3148_C63858").val());
            var id_2 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            cargarHijos_2(id_2);
        });

        $("#btnLlamar_2").click(function( event ) {

            habilitarBotonesModal("add");
            event.preventDefault(); 
            var padre = $("#G3148_C63858").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3190&view=si&formaDetalle=si&formularioPadre=3148&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64047<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    

            if(($("#G3148_C62805").val() == "") && $("#G3148_C62805").prop("disabled") == false){
                alertify.error('Numero de documento debe ser diligenciado');
                $("#G3148_C62805").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62806").val()==0 || $("#G3148_C62806").val() == null || $("#G3148_C62806").val() == -1) && $("#G3148_C62806").prop("disabled") == false){
                alertify.error('tipo de identificación debe ser diligenciado');
                $("#G3148_C62806").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62807").val() == "") && $("#G3148_C62807").prop("disabled") == false){
                alertify.error('Nombre completo? debe ser diligenciado');
                $("#G3148_C62807").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62808").val()==0 || $("#G3148_C62808").val() == null || $("#G3148_C62808").val() == -1) && $("#G3148_C62808").prop("disabled") == false){
                alertify.error('Eps o entindad por la que consulta debe ser diligenciado');
                $("#G3148_C62808").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62809").val() == "") && $("#G3148_C62809").prop("disabled") == false){
                alertify.error('teléfono de contacto 1 debe ser diligenciado');
                $("#G3148_C62809").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C63070").val()==0 || $("#G3148_C63070").val() == null || $("#G3148_C63070").val() == -1) && $("#G3148_C63070").prop("disabled") == false){
                alertify.error('Ciudad de donde se comunica? debe ser diligenciado');
                $("#G3148_C63070").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C64115").val()==0 || $("#G3148_C64115").val() == null || $("#G3148_C64115").val() == -1) && $("#G3148_C64115").prop("disabled") == false){
                alertify.error('CANAL debe ser diligenciado');
                $("#G3148_C64115").closest(".form-group").addClass("has-error");
                valido = 1;
            }
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3190&view=si&formaDetalle=si&formularioPadre=3148&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=64047&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3190&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=3148&pincheCampo=64047&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?><?php if(isset($_GET['consinte'])){ echo "&consinte=".$_GET['consinte']; }?>');
                $("#editarDatos").modal('show');
            }

            scrollTopUp();
        });
        //Select2 estos son los guiones
        


    $("#G3148_C62806").select2();
                            $("#G3148_C62808").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3148_C62808=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3148_C62808(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3148_C62808").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3148_C62808 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3148_C62808").html('<option value="'+data.G3163_ConsInte__b+'" >'+data.G3163_C63597+'</option>');
                                        
                                    }
                                });
                            });
                            $("#G3148_C63070").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G3148_C63070=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G3148_C63070(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G3148_C63070").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G3148_C63070 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G3148_C63070").html('<option value="'+data.G3159_ConsInte__b+'" >'+data.G3159_C63073+'</option>');
                                        
                                    }
                                });
                            });

    $("#G3148_C64115").select2();
        //datepickers
        

        $("#G3148_C62795").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G3148_C62796").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G3148_C63858").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para tipo de identificación 

    $("#G3148_C62806").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para teléfono de contacto 1 

    $("#TLF_G3148_C62809").click(function(){
        strTel_t=$("#G3148_C62809").val();
        llamarDesdeBtnTelefono(strTel_t);
    });

    //function para teléfono de contacto 2 

    $("#TLF_G3148_C62904").click(function(){
        strTel_t=$("#G3148_C62904").val();
        llamarDesdeBtnTelefono(strTel_t);
    });

    //function para CANAL 

    $("#G3148_C64115").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3148_C62805").val(item.G3148_C62805);
 
                    $("#G3148_C62806").val(item.G3148_C62806).trigger("change"); 
 
                                                $("#G3148_C62807").val(item.G3148_C62807);
 
                    $("#G3148_C62808").attr("opt",item.G3148_C62808);
                    $("#G3148_C62808").val(item.G3148_C62808).trigger("change");
 
                                                $("#G3148_C62809").val(item.G3148_C62809);
 
                                                $("#G3148_C62904").val(item.G3148_C62904);
 
                                                $("#G3148_C62812").val(item.G3148_C62812);
 
                    $("#G3148_C63070").attr("opt",item.G3148_C63070);
                    $("#G3148_C63070").val(item.G3148_C63070).trigger("change");
 
                    $("#G3148_C62793").val(item.G3148_C62793).trigger("change"); 
 
                    $("#G3148_C62794").val(item.G3148_C62794).trigger("change"); 
 
                                                $("#G3148_C62795").val(item.G3148_C62795);
 
                                                $("#G3148_C62796").val(item.G3148_C62796);
 
                                                $("#G3148_C62797").val(item.G3148_C62797);
 
                                                $("#G3148_C62798").val(item.G3148_C62798);
 
                                                $("#G3148_C62799").val(item.G3148_C62799);
 
                                                $("#G3148_C62800").val(item.G3148_C62800);
 
                                                $("#G3148_C62801").val(item.G3148_C62801);
 
                                                $("#G3148_C63858").val(item.G3148_C63858);
      
                                                if(item.G3148_C63069 == 1){
                                                   $("#G3148_C63069").attr('checked', true);
                                                } 
 
                    $("#G3148_C64115").val(item.G3148_C64115).trigger("change"); 
      
                                                if(item.G3148_C64116 == 1){
                                                   $("#G3148_C64116").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G3148_C62805").val() == "") && $("#G3148_C62805").prop("disabled") == false){
                alertify.error('Numero de documento debe ser diligenciado');
                $("#G3148_C62805").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62806").val()==0 || $("#G3148_C62806").val() == null || $("#G3148_C62806").val() == -1) && $("#G3148_C62806").prop("disabled") == false){
                alertify.error('tipo de identificación debe ser diligenciado');
                $("#G3148_C62806").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62807").val() == "") && $("#G3148_C62807").prop("disabled") == false){
                alertify.error('Nombre completo? debe ser diligenciado');
                $("#G3148_C62807").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62808").val()==0 || $("#G3148_C62808").val() == null || $("#G3148_C62808").val() == -1) && $("#G3148_C62808").prop("disabled") == false){
                alertify.error('Eps o entindad por la que consulta debe ser diligenciado');
                $("#G3148_C62808").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C62809").val() == "") && $("#G3148_C62809").prop("disabled") == false){
                alertify.error('teléfono de contacto 1 debe ser diligenciado');
                $("#G3148_C62809").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C63070").val()==0 || $("#G3148_C63070").val() == null || $("#G3148_C63070").val() == -1) && $("#G3148_C63070").prop("disabled") == false){
                alertify.error('Ciudad de donde se comunica? debe ser diligenciado');
                $("#G3148_C63070").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3148_C64115").val()==0 || $("#G3148_C64115").val() == null || $("#G3148_C64115").val() == -1) && $("#G3148_C64115").prop("disabled") == false){
                alertify.error('CANAL debe ser diligenciado');
                $("#G3148_C64115").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            /*let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });

            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }*/

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Numero de documento','tipo de identificación','Nombre completo?','Eps o entindad por la que consulta','teléfono de contacto 1','teléfono de contacto 2','correo electronico? ','Ciudad de donde se comunica?','Agente','Fecha','Hora','Campaña','ID GESTION','CANAL'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G3148_C62805', 
                        index: 'G3148_C62805', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C62806', 
                        index:'G3148_C62806', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3871&campo=G3148_C62806'
                        }
                    }

                    ,
                    { 
                        name:'G3148_C62807', 
                        index: 'G3148_C62807', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C62808', 
                        index:'G3148_C62808', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3148_C62808=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3148_C62809', 
                        index: 'G3148_C62809', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C62904', 
                        index: 'G3148_C62904', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C63070', 
                        index:'G3148_C63070', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G3148_C63070=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3148_C62798', 
                        index: 'G3148_C62798', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C62799', 
                        index: 'G3148_C62799', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C62800', 
                        index: 'G3148_C62800', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3148_C62801', 
                        index: 'G3148_C62801', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G3148_C63858', 
                        index:'G3148_C63858', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3148_C64115', 
                        index:'G3148_C64115', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3935&campo=G3148_C64115'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3148_C62805',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
            ,subGrid: true,
            subGridRowExpanded: function(subgrid_id, row_id) { 
                // we pass two parameters 
                // subgrid_id is a id of the div tag created whitin a table data 
                // the id of this elemenet is a combination of the "sg_" + id of the row 
                // the row_id is the id of the row 
                // If we wan to pass additinal parameters to the url we can use 
                // a method getRowData(row_id) - which returns associative array in type name-value 
                // here we can easy construct the flowing 
                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID GESTION','Especialidad','Especialista que requiere',' 1 vez /control', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }
 
                            ,
                            {  
                                name:'G3198_C64154', 
                                index:'G3198_C64154', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                            ,
                            {  
                                name:'G3198_C64147', 
                                index:'G3198_C64147', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3945&campo=G3198_C64147'
                                }
                            }

                            ,
                            {  
                                name:'G3198_C64148', 
                                index:'G3198_C64148', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3946&campo=G3198_C64148'
                                }
                            }

                            ,
                            {  
                                name:'G3198_C64149', 
                                index:'G3198_C64149', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3895&campo=G3198_C64149'
                                }
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                var subgrid_table_id_1, pager_id_1; 

                subgrid_table_id_1 = subgrid_id+"_t_1"; 

                pager_id_ = "p_"+subgrid_table_id_1; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_1+"' class='scroll'></table><div id='"+pager_id_1+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_1).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID_GESTION','• Especialidad','• Especialista que requiere','• 1 vez /control', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }
 
                            ,
                            {  
                                name:'G3199_C64194', 
                                index:'G3199_C64194', 
                                width:80 ,
                                editable: true, 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                                }

                            }

                            ,
                            {  
                                name:'G3199_C64197', 
                                index:'G3199_C64197', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3945&campo=G3199_C64197'
                                }
                            }

                            ,
                            {  
                                name:'G3199_C64198', 
                                index:'G3199_C64198', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3946&campo=G3199_C64198'
                                }
                            }

                            ,
                            {  
                                name:'G3199_C64199', 
                                index:'G3199_C64199', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3895&campo=G3199_C64199'
                                }
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_1, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_1).jqGrid('navGrid',"#"+pager_id_1,{edit:false,add:false,del:false}) 

                var subgrid_table_id_2, pager_id_2; 

                subgrid_table_id_2 = subgrid_id+"_t_2"; 

                pager_id_ = "p_"+subgrid_table_id_2; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_2+"' class='scroll'></table><div id='"+pager_id_2+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_2).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID_GESTION','TIPO DE CONSULTA', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G3190_C64047', 
                                index: 'G3190_C64047', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            {  
                                name:'G3190_C64117', 
                                index:'G3190_C64117', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3936&campo=G3190_C64117'
                                }
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_2, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_2).jqGrid('navGrid',"#"+pager_id_2,{edit:false,add:false,del:false}) 

                }, 
                subGridRowColapsed: function(subgrid_id, row_id) { 
                    // this function is called before removing the data 
                    //var subgrid_table_id; 
                    //subgrid_table_id = subgrid_id+"_t"; 
                    //jQuery("#"+subgrid_table_id).remove(); 
                }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);$("#btnLlamar_1").attr('padre', id);$("#btnLlamar_2").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            

                        $("#G3148_C62805").val(item.G3148_C62805);
 
                    $("#G3148_C62806").val(item.G3148_C62806).trigger("change"); 

                        $("#G3148_C62807").val(item.G3148_C62807);
 
                    $("#G3148_C62808").attr("opt",item.G3148_C62808);
                    $("#G3148_C62808").val(item.G3148_C62808).trigger("change");

                        $("#G3148_C62809").val(item.G3148_C62809);

                        $("#G3148_C62904").val(item.G3148_C62904);

                        $("#G3148_C62812").val(item.G3148_C62812);
 
                    $("#G3148_C63070").attr("opt",item.G3148_C63070);
                    $("#G3148_C63070").val(item.G3148_C63070).trigger("change");
 
                    $("#G3148_C62793").val(item.G3148_C62793).trigger("change"); 
 
                    $("#G3148_C62794").val(item.G3148_C62794).trigger("change"); 

                        $("#G3148_C62795").val(item.G3148_C62795);

                        $("#G3148_C62796").val(item.G3148_C62796);

                        $("#G3148_C62797").val(item.G3148_C62797);

                        $("#G3148_C62798").val(item.G3148_C62798);

                        $("#G3148_C62799").val(item.G3148_C62799);

                        $("#G3148_C62800").val(item.G3148_C62800);

                        $("#G3148_C62801").val(item.G3148_C62801);

                        $("#G3148_C63858").val(item.G3148_C63858);
    
                        if(item.G3148_C63069 == 1){
                           $("#G3148_C63069").attr('checked', true);
                        } 
 
                    $("#G3148_C64115").val(item.G3148_C64115).trigger("change"); 
    
                        if(item.G3148_C64116 == 1){
                           $("#G3148_C64116").attr('checked', true);
                        } 
                        
            cargarHijos_0(
        $("#G3148_C63858").val());
            cargarHijos_1(
        $("#G3148_C63858").val());
            cargarHijos_2(
        $("#G3148_C63858").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion descargar 
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID GESTION','Especialidad','Especialista que requiere',' 1 vez /control', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G3198_C64154', 
                        index:'G3198_C64154', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }

                    ,
                    {  
                        name:'G3198_C64147', 
                        index:'G3198_C64147', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3945&campo=G3198_C64147'
                        }
                    }

                    ,
                    {  
                        name:'G3198_C64148', 
                        index:'G3198_C64148', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3946&campo=G3198_C64148'
                        }
                    }

                    ,
                    {  
                        name:'G3198_C64149', 
                        index:'G3198_C64149', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3895&campo=G3198_C64149'
                        }
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G3198_C64154',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'CITA PRESENCIAL',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {

                habilitarBotonesModal("edit");

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3198&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=64154&formularioPadre=3148<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

                scrollTopUp();

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_1(id_1){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless1").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+id_1,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID_GESTION','• Especialidad','• Especialista que requiere','• 1 vez /control', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G3199_C64194', 
                        index:'G3199_C64194', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,

                                dataInit:function(el){
                                    $(el).numeric();
                                }
                        }

                    }

                    ,
                    {  
                        name:'G3199_C64197', 
                        index:'G3199_C64197', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3945&campo=G3199_C64197'
                        }
                    }

                    ,
                    {  
                        name:'G3199_C64198', 
                        index:'G3199_C64198', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3946&campo=G3199_C64198'
                        }
                    }

                    ,
                    {  
                        name:'G3199_C64199', 
                        index:'G3199_C64199', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3895&campo=G3199_C64199'
                        }
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_1); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles1",
            rowList: [40,80],
            sortable: true,
            sortname: 'G3199_C64194',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'CITA TELEMEDICINA',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_1=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {

                habilitarBotonesModal("edit");

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3199&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=64194&formularioPadre=3148<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

                scrollTopUp();

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless1").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_2(id_2){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless2").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+id_2,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID_GESTION','TIPO DE CONSULTA', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G3190_C64047', 
                        index: 'G3190_C64047', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G3190_C64117', 
                        index:'G3190_C64117', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3936&campo=G3190_C64117'
                        }
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_2); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles2",
            rowList: [40,80],
            sortable: true,
            sortname: 'G3190_C64047',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'INFORMACIÓN GENERAL',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_2=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {

                habilitarBotonesModal("edit");

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=3190&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=64047&formularioPadre=3148<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

                scrollTopUp();

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless2").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G3148_C63858").val());
            var id_0 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        $("#btnLlamar_1").attr('padre', $("#G3148_C63858").val());
            var id_1 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            cargarHijos_1(id_1);
        $("#btnLlamar_2").attr('padre', $("#G3148_C63858").val());
            var id_2 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            cargarHijos_2(id_2);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G3148_C63858").val());
            var id_0 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        $("#btnLlamar_1").attr('padre', $("#G3148_C63858").val());
            var id_1 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            cargarHijos_1(id_1);
        $("#btnLlamar_2").attr('padre', $("#G3148_C63858").val());
            var id_2 = $("#G3148_C63858").val();
            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            cargarHijos_2(id_2);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
