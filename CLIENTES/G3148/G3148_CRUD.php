<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 63858")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 63858");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3148_ConsInte__b, G3148_FechaInsercion , G3148_Usuario ,  G3148_CodigoMiembro  , G3148_PoblacionOrigen , G3148_EstadoDiligenciamiento ,  G3148_IdLlamada , G3148_C62805 as principal ,G3148_C62805,G3148_C62806,G3148_C62807,G3148_C62808,G3148_C62809,G3148_C62904,G3148_C62812,G3148_C63070,G3148_C62793,G3148_C62794,G3148_C62795,G3148_C62796,G3148_C62797,G3148_C62798,G3148_C62799,G3148_C62800,G3148_C62801,G3148_C63858,G3148_C64115 FROM '.$BaseDatos.'.G3148 WHERE G3148_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3148_C62805'] = $key->G3148_C62805;

                $datos[$i]['G3148_C62806'] = $key->G3148_C62806;

                $datos[$i]['G3148_C62807'] = $key->G3148_C62807;

                $datos[$i]['G3148_C62808'] = $key->G3148_C62808;

                $datos[$i]['G3148_C62809'] = $key->G3148_C62809;

                $datos[$i]['G3148_C62904'] = $key->G3148_C62904;

                $datos[$i]['G3148_C62812'] = $key->G3148_C62812;

                $datos[$i]['G3148_C63070'] = $key->G3148_C63070;

                $datos[$i]['G3148_C62793'] = $key->G3148_C62793;

                $datos[$i]['G3148_C62794'] = $key->G3148_C62794;

                $datos[$i]['G3148_C62795'] = explode(' ', $key->G3148_C62795)[0];
  
                $hora = '';
                if(!is_null($key->G3148_C62796)){
                    $hora = explode(' ', $key->G3148_C62796)[1];
                }

                $datos[$i]['G3148_C62796'] = $hora;

                $datos[$i]['G3148_C62797'] = $key->G3148_C62797;

                $datos[$i]['G3148_C62798'] = $key->G3148_C62798;

                $datos[$i]['G3148_C62799'] = $key->G3148_C62799;

                $datos[$i]['G3148_C62800'] = $key->G3148_C62800;

                $datos[$i]['G3148_C62801'] = $key->G3148_C62801;

                $datos[$i]['G3148_C63858'] = $key->G3148_C63858;

                $datos[$i]['G3148_C64115'] = $key->G3148_C64115;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3148";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3148_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3148_ConsInte__b as id,  G3148_C62805 as camp1 , G3148_C63858 as camp2 
                     FROM ".$BaseDatos.".G3148  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3148_ConsInte__b as id,  G3148_C62805 as camp1 , G3148_C63858 as camp2  
                    FROM ".$BaseDatos.".G3148  JOIN ".$BaseDatos.".G3148_M".$_POST['muestra']." ON G3148_ConsInte__b = G3148_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3148_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3148_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3148_C62805 LIKE '%".$B."%' OR G3148_C63858 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3148_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G3148_C62808'])){
                                $Ysql = "SELECT G3163_ConsInte__b as id, G3163_C63597 as text FROM ".$BaseDatos.".G3163 WHERE G3163_C63597 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3148_C62808"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3163 WHERE G3163_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3148_C62808"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G3148_C63070'])){
                                $Ysql = "SELECT G3159_ConsInte__b as id, G3159_C63073 as text FROM ".$BaseDatos.".G3159 WHERE G3159_C63073 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3148_C63070"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3159 WHERE G3159_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3148_C63070"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3148");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3148_ConsInte__b, G3148_FechaInsercion , G3148_Usuario ,  G3148_CodigoMiembro  , G3148_PoblacionOrigen , G3148_EstadoDiligenciamiento ,  G3148_IdLlamada , G3148_C62805 as principal ,G3148_C62805, a.LISOPC_Nombre____b as G3148_C62806,G3148_C62807, G3163_C63597,G3148_C62809,G3148_C62904,G3148_C62812, G3159_C63073, b.LISOPC_Nombre____b as G3148_C62793, c.LISOPC_Nombre____b as G3148_C62794,G3148_C62795,G3148_C62796,G3148_C62797,G3148_C62798,G3148_C62799,G3148_C62800,G3148_C62801,G3148_C63858, d.LISOPC_Nombre____b as G3148_C64115 FROM '.$BaseDatos.'.G3148 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3148_C62806 LEFT JOIN '.$BaseDatos.'.G3163 ON G3163_ConsInte__b  =  G3148_C62808 LEFT JOIN '.$BaseDatos.'.G3159 ON G3159_ConsInte__b  =  G3148_C63070 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3148_C62793 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3148_C62794 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3148_C64115';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G3148_C62796)){
                    $hora_a = explode(' ', $fila->G3148_C62796)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G3148_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3148_ConsInte__b , ($fila->G3148_C62805) , ($fila->G3148_C62806) , ($fila->G3148_C62807) , ($fila->G3163_C63597) , ($fila->G3148_C62809) , ($fila->G3148_C62904) , ($fila->G3148_C62812) , ($fila->G3159_C63073) , ($fila->G3148_C62793) , ($fila->G3148_C62794) , explode(' ', $fila->G3148_C62795)[0] , $hora_a , ($fila->G3148_C62797) , ($fila->G3148_C62798) , ($fila->G3148_C62799) , ($fila->G3148_C62800) , ($fila->G3148_C62801) , ($fila->G3148_C63858) , ($fila->G3148_C64115) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3148 WHERE G3148_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3148";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3148_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3148_ConsInte__b as id,  G3148_C62805 as camp1 , G3148_C63858 as camp2  FROM '.$BaseDatos.'.G3148 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3148_ConsInte__b as id,  G3148_C62805 as camp1 , G3148_C63858 as camp2  
                    FROM ".$BaseDatos.".G3148  JOIN ".$BaseDatos.".G3148_M".$_POST['muestra']." ON G3148_ConsInte__b = G3148_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3148_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3148_C62805 LIKE "%'.$B.'%" OR G3148_C63858 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3148_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3148 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3148(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3148_C62805"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62805 = '".$_POST["G3148_C62805"]."'";
                $LsqlI .= $separador."G3148_C62805";
                $LsqlV .= $separador."'".$_POST["G3148_C62805"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62806"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62806 = '".$_POST["G3148_C62806"]."'";
                $LsqlI .= $separador."G3148_C62806";
                $LsqlV .= $separador."'".$_POST["G3148_C62806"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62807"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62807 = '".$_POST["G3148_C62807"]."'";
                $LsqlI .= $separador."G3148_C62807";
                $LsqlV .= $separador."'".$_POST["G3148_C62807"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62808"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62808 = '".$_POST["G3148_C62808"]."'";
                $LsqlI .= $separador."G3148_C62808";
                $LsqlV .= $separador."'".$_POST["G3148_C62808"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62809"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62809 = '".$_POST["G3148_C62809"]."'";
                $LsqlI .= $separador."G3148_C62809";
                $LsqlV .= $separador."'".$_POST["G3148_C62809"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62904"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62904 = '".$_POST["G3148_C62904"]."'";
                $LsqlI .= $separador."G3148_C62904";
                $LsqlV .= $separador."'".$_POST["G3148_C62904"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62812"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62812 = '".$_POST["G3148_C62812"]."'";
                $LsqlI .= $separador."G3148_C62812";
                $LsqlV .= $separador."'".$_POST["G3148_C62812"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C63070"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C63070 = '".$_POST["G3148_C63070"]."'";
                $LsqlI .= $separador."G3148_C63070";
                $LsqlV .= $separador."'".$_POST["G3148_C63070"]."'";
                $validar = 1;
            }
             
 
            $G3148_C62793 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3148_C62793 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G3148_C62793 = ".$G3148_C62793;
                    $LsqlI .= $separador." G3148_C62793";
                    $LsqlV .= $separador.$G3148_C62793;
                    $validar = 1;

                    
                }
            }
 
            $G3148_C62794 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3148_C62794 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G3148_C62794 = ".$G3148_C62794;
                    $LsqlI .= $separador." G3148_C62794";
                    $LsqlV .= $separador.$G3148_C62794;
                    $validar = 1;
                }
            }
 
            $G3148_C62795 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3148_C62795 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G3148_C62795 = ".$G3148_C62795;
                    $LsqlI .= $separador." G3148_C62795";
                    $LsqlV .= $separador.$G3148_C62795;
                    $validar = 1;
                }
            }
 
            $G3148_C62796 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3148_C62796 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G3148_C62796 = ".$G3148_C62796;
                    $LsqlI .= $separador." G3148_C62796";
                    $LsqlV .= $separador.$G3148_C62796;
                    $validar = 1;
                }
            }
 
            $G3148_C62797 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G3148_C62797 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G3148_C62797 = ".$G3148_C62797;
                    $LsqlI .= $separador." G3148_C62797";
                    $LsqlV .= $separador.$G3148_C62797;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3148_C62798"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62798 = '".$_POST["G3148_C62798"]."'";
                $LsqlI .= $separador."G3148_C62798";
                $LsqlV .= $separador."'".$_POST["G3148_C62798"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62799"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62799 = '".$_POST["G3148_C62799"]."'";
                $LsqlI .= $separador."G3148_C62799";
                $LsqlV .= $separador."'".$_POST["G3148_C62799"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62800"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62800 = '".$_POST["G3148_C62800"]."'";
                $LsqlI .= $separador."G3148_C62800";
                $LsqlV .= $separador."'".$_POST["G3148_C62800"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C62801"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C62801 = '".$_POST["G3148_C62801"]."'";
                $LsqlI .= $separador."G3148_C62801";
                $LsqlV .= $separador."'".$_POST["G3148_C62801"]."'";
                $validar = 1;
            }
             
  
            $G3148_C63858 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3148_C63858"])){
                if($_POST["G3148_C63858"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3148_C63858 = $_POST["G3148_C63858"];
                    $LsqlU .= $separador." G3148_C63858 = ".$G3148_C63858."";
                    $LsqlI .= $separador." G3148_C63858";
                    $LsqlV .= $separador.$G3148_C63858;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3148_C63069"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C63069 = '".$_POST["G3148_C63069"]."'";
                $LsqlI .= $separador."G3148_C63069";
                $LsqlV .= $separador."'".$_POST["G3148_C63069"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C64115"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C64115 = '".$_POST["G3148_C64115"]."'";
                $LsqlI .= $separador."G3148_C64115";
                $LsqlV .= $separador."'".$_POST["G3148_C64115"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3148_C64116"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_C64116 = '".$_POST["G3148_C64116"]."'";
                $LsqlI .= $separador."G3148_C64116";
                $LsqlV .= $separador."'".$_POST["G3148_C64116"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G3148_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G3148_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3148_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3148_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3148_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3148_Usuario , G3148_FechaInsercion, G3148_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3148_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3148 WHERE G3148_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                            $UltimoID = $_POST["id"];
                            echo $UltimoID;
                        }
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3148_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3148 WHERE G3148_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3198_ConsInte__b, G3198_C64154, b.LISOPC_Nombre____b as  G3198_C64147, c.LISOPC_Nombre____b as  G3198_C64148, d.LISOPC_Nombre____b as  G3198_C64149 FROM ".$BaseDatos.".G3198  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G3198_C64147 LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G3198_C64148 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G3198_C64149 ";

        $SQL .= " WHERE G3198_C64154 = '".$numero."'"; 

        $SQL .= " ORDER BY G3198_C64154";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3198_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3198_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G3198_C64154."</cell>"; 

                echo "<cell>". ($fila->G3198_C64147)."</cell>";

                echo "<cell>". ($fila->G3198_C64148)."</cell>";

                echo "<cell>". ($fila->G3198_C64149)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3148_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3148 WHERE G3148_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3199_ConsInte__b, G3199_C64194, b.LISOPC_Nombre____b as  G3199_C64197, c.LISOPC_Nombre____b as  G3199_C64198, d.LISOPC_Nombre____b as  G3199_C64199 FROM ".$BaseDatos.".G3199  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G3199_C64197 LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G3199_C64198 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G3199_C64199 ";

        $SQL .= " WHERE G3199_C64194 = '".$numero."'"; 

        $SQL .= " ORDER BY G3199_C64194";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3199_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3199_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G3199_C64194."</cell>"; 

                echo "<cell>". ($fila->G3199_C64197)."</cell>";

                echo "<cell>". ($fila->G3199_C64198)."</cell>";

                echo "<cell>". ($fila->G3199_C64199)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_2"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3148_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3148 WHERE G3148_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3190_ConsInte__b, G3190_C64047, b.LISOPC_Nombre____b as  G3190_C64117 FROM ".$BaseDatos.".G3190  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G3190_C64117 ";

        $SQL .= " WHERE G3190_C64047 = '".$numero."'"; 

        $SQL .= " ORDER BY G3190_C64047";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3190_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3190_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G3190_C64047)."</cell>";

                echo "<cell>". ($fila->G3190_C64117)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3198 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3198(";
            $LsqlV = " VALUES ("; 
 
                if(isset($_POST["G3198_C64147"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3198_C64147 = '".$_POST["G3198_C64147"]."'";
                    $LsqlI .= $separador."G3198_C64147";
                    $LsqlV .= $separador."'".$_POST["G3198_C64147"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G3198_C64148"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3198_C64148 = '".$_POST["G3198_C64148"]."'";
                    $LsqlI .= $separador."G3198_C64148";
                    $LsqlV .= $separador."'".$_POST["G3198_C64148"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G3198_C64149"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3198_C64149 = '".$_POST["G3198_C64149"]."'";
                    $LsqlI .= $separador."G3198_C64149";
                    $LsqlV .= $separador."'".$_POST["G3198_C64149"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3198_C64154 = $numero;
                    $LsqlU .= ", G3198_C64154 = ".$G3198_C64154."";
                    $LsqlI .= ", G3198_C64154";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3198_Usuario ,  G3198_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3198_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3198 WHERE  G3198_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3199 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3199(";
            $LsqlV = " VALUES ("; 
 
                if(isset($_POST["G3199_C64197"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3199_C64197 = '".$_POST["G3199_C64197"]."'";
                    $LsqlI .= $separador."G3199_C64197";
                    $LsqlV .= $separador."'".$_POST["G3199_C64197"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G3199_C64198"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3199_C64198 = '".$_POST["G3199_C64198"]."'";
                    $LsqlI .= $separador."G3199_C64198";
                    $LsqlV .= $separador."'".$_POST["G3199_C64198"]."'";
                    $validar = 1;
                }
 
                if(isset($_POST["G3199_C64199"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3199_C64199 = '".$_POST["G3199_C64199"]."'";
                    $LsqlI .= $separador."G3199_C64199";
                    $LsqlV .= $separador."'".$_POST["G3199_C64199"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3199_C64194 = $numero;
                    $LsqlU .= ", G3199_C64194 = ".$G3199_C64194."";
                    $LsqlI .= ", G3199_C64194";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3199_Usuario ,  G3199_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3199_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3199 WHERE  G3199_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_2"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3190 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3190(";
            $LsqlV = " VALUES ("; 
 
                if(isset($_POST["G3190_C64117"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3190_C64117 = '".$_POST["G3190_C64117"]."'";
                    $LsqlI .= $separador."G3190_C64117";
                    $LsqlV .= $separador."'".$_POST["G3190_C64117"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3190_C64047 = $numero;
                    $LsqlU .= ", G3190_C64047 = ".$G3190_C64047."";
                    $LsqlI .= ", G3190_C64047";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3190_Usuario ,  G3190_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3190_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3190 WHERE  G3190_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
