<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2629_ConsInte__b, G2629_FechaInsercion , G2629_Usuario ,  G2629_CodigoMiembro  , G2629_PoblacionOrigen , G2629_EstadoDiligenciamiento ,  G2629_IdLlamada , G2629_C51312 as principal ,G2629_C51312,G2629_C51313,G2629_C51314,G2629_C51315,G2629_C51316,G2629_C51317,G2629_C51559,G2629_C55064,G2629_C55065,G2629_C55066,G2629_C55067,G2629_C55072,G2629_C56099,G2629_C56637,G2629_C57026,G2629_C51294,G2629_C51295,G2629_C51296,G2629_C51348,G2629_C51349,G2629_C51350,G2629_C51351,G2629_C51352,G2629_C51353,G2629_C51354,G2629_C51355,G2629_C51356,G2629_C51374,G2629_C51375,G2629_C51376,G2629_C51377,G2629_C51378,G2629_C51379,G2629_C51580 FROM '.$BaseDatos.'.G2629 WHERE G2629_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2629_C51312'] = $key->G2629_C51312;

                $datos[$i]['G2629_C51313'] = $key->G2629_C51313;

                $datos[$i]['G2629_C51314'] = $key->G2629_C51314;

                $datos[$i]['G2629_C51315'] = $key->G2629_C51315;

                $datos[$i]['G2629_C51316'] = $key->G2629_C51316;

                $datos[$i]['G2629_C51317'] = $key->G2629_C51317;

                $datos[$i]['G2629_C51559'] = $key->G2629_C51559;

                $datos[$i]['G2629_C55064'] = $key->G2629_C55064;

                $datos[$i]['G2629_C55065'] = $key->G2629_C55065;

                $datos[$i]['G2629_C55066'] = $key->G2629_C55066;

                $datos[$i]['G2629_C55067'] = $key->G2629_C55067;

                $datos[$i]['G2629_C55072'] = $key->G2629_C55072;

                $datos[$i]['G2629_C56099'] = $key->G2629_C56099;

                $datos[$i]['G2629_C56637'] = $key->G2629_C56637;

                $datos[$i]['G2629_C57026'] = $key->G2629_C57026;

                $datos[$i]['G2629_C51294'] = $key->G2629_C51294;

                $datos[$i]['G2629_C51295'] = $key->G2629_C51295;

                $datos[$i]['G2629_C51296'] = $key->G2629_C51296;

                $datos[$i]['G2629_C51348'] = $key->G2629_C51348;

                $datos[$i]['G2629_C51349'] = $key->G2629_C51349;

                $datos[$i]['G2629_C51350'] = $key->G2629_C51350;

                $datos[$i]['G2629_C51351'] = $key->G2629_C51351;

                $datos[$i]['G2629_C51352'] = $key->G2629_C51352;

                $datos[$i]['G2629_C51353'] = $key->G2629_C51353;

                $datos[$i]['G2629_C51354'] = $key->G2629_C51354;

                $datos[$i]['G2629_C51355'] = $key->G2629_C51355;

                $datos[$i]['G2629_C51356'] = $key->G2629_C51356;

                $datos[$i]['G2629_C51374'] = $key->G2629_C51374;

                $datos[$i]['G2629_C51375'] = $key->G2629_C51375;

                $datos[$i]['G2629_C51376'] = $key->G2629_C51376;

                $datos[$i]['G2629_C51377'] = $key->G2629_C51377;

                $datos[$i]['G2629_C51378'] = explode(' ', $key->G2629_C51378)[0];

                $datos[$i]['G2629_C51379'] = explode(' ', $key->G2629_C51379)[0];

                $datos[$i]['G2629_C51580'] = $key->G2629_C51580;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2629";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2629_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2629_ConsInte__b as id,  G2629_C51312 as camp1 , G2629_C51313 as camp2 
                     FROM ".$BaseDatos.".G2629  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2629_ConsInte__b as id,  G2629_C51312 as camp1 , G2629_C51313 as camp2  
                    FROM ".$BaseDatos.".G2629  JOIN ".$BaseDatos.".G2629_M".$_POST['muestra']." ON G2629_ConsInte__b = G2629_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2629_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2629_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2629_C51312 LIKE '%".$B."%' OR G2629_C51313 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2629_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2629");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2629_ConsInte__b, G2629_FechaInsercion , G2629_Usuario ,  G2629_CodigoMiembro  , G2629_PoblacionOrigen , G2629_EstadoDiligenciamiento ,  G2629_IdLlamada , G2629_C51312 as principal ,G2629_C51312,G2629_C51313,G2629_C51314,G2629_C51315,G2629_C51316,G2629_C51317,G2629_C51559,G2629_C55064,G2629_C55065,G2629_C55066,G2629_C55067,G2629_C55072,G2629_C56099,G2629_C56637,G2629_C57026,G2629_C51294,G2629_C51295, a.LISOPC_Nombre____b as G2629_C51296, b.LISOPC_Nombre____b as G2629_C51348,G2629_C51349,G2629_C51350,G2629_C51351,G2629_C51352,G2629_C51353,G2629_C51354,G2629_C51355,G2629_C51356, c.LISOPC_Nombre____b as G2629_C51374, d.LISOPC_Nombre____b as G2629_C51375, e.LISOPC_Nombre____b as G2629_C51376,G2629_C51377,G2629_C51378,G2629_C51379,G2629_C51580 FROM '.$BaseDatos.'.G2629 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2629_C51296 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2629_C51348 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2629_C51374 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2629_C51375 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2629_C51376';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2629_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2629_ConsInte__b , ($fila->G2629_C51312) , ($fila->G2629_C51313) , ($fila->G2629_C51314) , ($fila->G2629_C51315) , ($fila->G2629_C51316) , ($fila->G2629_C51317) , ($fila->G2629_C51559) , ($fila->G2629_C55064) , ($fila->G2629_C55065) , ($fila->G2629_C55066) , ($fila->G2629_C55067) , ($fila->G2629_C55072) , ($fila->G2629_C56099) , ($fila->G2629_C56637) , ($fila->G2629_C57026) , ($fila->G2629_C51294) , ($fila->G2629_C51295) , ($fila->G2629_C51296) , ($fila->G2629_C51348) , ($fila->G2629_C51349) , ($fila->G2629_C51350) , ($fila->G2629_C51351) , ($fila->G2629_C51352) , ($fila->G2629_C51353) , ($fila->G2629_C51354) , ($fila->G2629_C51355) , ($fila->G2629_C51356) , ($fila->G2629_C51374) , ($fila->G2629_C51375) , ($fila->G2629_C51376) , ($fila->G2629_C51377) , explode(' ', $fila->G2629_C51378)[0] , explode(' ', $fila->G2629_C51379)[0] , ($fila->G2629_C51580) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2629 WHERE G2629_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2629";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2629_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2629_ConsInte__b as id,  G2629_C51312 as camp1 , G2629_C51313 as camp2  FROM '.$BaseDatos.'.G2629 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2629_ConsInte__b as id,  G2629_C51312 as camp1 , G2629_C51313 as camp2  
                    FROM ".$BaseDatos.".G2629  JOIN ".$BaseDatos.".G2629_M".$_POST['muestra']." ON G2629_ConsInte__b = G2629_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2629_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2629_C51312 LIKE "%'.$B.'%" OR G2629_C51313 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2629_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2629 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2629(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2629_C51312"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51312 = '".$_POST["G2629_C51312"]."'";
                $LsqlI .= $separador."G2629_C51312";
                $LsqlV .= $separador."'".$_POST["G2629_C51312"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51313 = '".$_POST["G2629_C51313"]."'";
                $LsqlI .= $separador."G2629_C51313";
                $LsqlV .= $separador."'".$_POST["G2629_C51313"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51314 = '".$_POST["G2629_C51314"]."'";
                $LsqlI .= $separador."G2629_C51314";
                $LsqlV .= $separador."'".$_POST["G2629_C51314"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51315 = '".$_POST["G2629_C51315"]."'";
                $LsqlI .= $separador."G2629_C51315";
                $LsqlV .= $separador."'".$_POST["G2629_C51315"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51316 = '".$_POST["G2629_C51316"]."'";
                $LsqlI .= $separador."G2629_C51316";
                $LsqlV .= $separador."'".$_POST["G2629_C51316"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51317 = '".$_POST["G2629_C51317"]."'";
                $LsqlI .= $separador."G2629_C51317";
                $LsqlV .= $separador."'".$_POST["G2629_C51317"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51559"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51559 = '".$_POST["G2629_C51559"]."'";
                $LsqlI .= $separador."G2629_C51559";
                $LsqlV .= $separador."'".$_POST["G2629_C51559"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C55064"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C55064 = '".$_POST["G2629_C55064"]."'";
                $LsqlI .= $separador."G2629_C55064";
                $LsqlV .= $separador."'".$_POST["G2629_C55064"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C55065"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C55065 = '".$_POST["G2629_C55065"]."'";
                $LsqlI .= $separador."G2629_C55065";
                $LsqlV .= $separador."'".$_POST["G2629_C55065"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C55066"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C55066 = '".$_POST["G2629_C55066"]."'";
                $LsqlI .= $separador."G2629_C55066";
                $LsqlV .= $separador."'".$_POST["G2629_C55066"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C55067"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C55067 = '".$_POST["G2629_C55067"]."'";
                $LsqlI .= $separador."G2629_C55067";
                $LsqlV .= $separador."'".$_POST["G2629_C55067"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C55072"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C55072 = '".$_POST["G2629_C55072"]."'";
                $LsqlI .= $separador."G2629_C55072";
                $LsqlV .= $separador."'".$_POST["G2629_C55072"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C56099"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C56099 = '".$_POST["G2629_C56099"]."'";
                $LsqlI .= $separador."G2629_C56099";
                $LsqlV .= $separador."'".$_POST["G2629_C56099"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C56637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C56637 = '".$_POST["G2629_C56637"]."'";
                $LsqlI .= $separador."G2629_C56637";
                $LsqlV .= $separador."'".$_POST["G2629_C56637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C57026"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C57026 = '".$_POST["G2629_C57026"]."'";
                $LsqlI .= $separador."G2629_C57026";
                $LsqlV .= $separador."'".$_POST["G2629_C57026"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51294"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51294 = '".$_POST["G2629_C51294"]."'";
                $LsqlI .= $separador."G2629_C51294";
                $LsqlV .= $separador."'".$_POST["G2629_C51294"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51295 = '".$_POST["G2629_C51295"]."'";
                $LsqlI .= $separador."G2629_C51295";
                $LsqlV .= $separador."'".$_POST["G2629_C51295"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51296 = '".$_POST["G2629_C51296"]."'";
                $LsqlI .= $separador."G2629_C51296";
                $LsqlV .= $separador."'".$_POST["G2629_C51296"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51348"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51348 = '".$_POST["G2629_C51348"]."'";
                $LsqlI .= $separador."G2629_C51348";
                $LsqlV .= $separador."'".$_POST["G2629_C51348"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51349"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51349 = '".$_POST["G2629_C51349"]."'";
                $LsqlI .= $separador."G2629_C51349";
                $LsqlV .= $separador."'".$_POST["G2629_C51349"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51350"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51350 = '".$_POST["G2629_C51350"]."'";
                $LsqlI .= $separador."G2629_C51350";
                $LsqlV .= $separador."'".$_POST["G2629_C51350"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51351"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51351 = '".$_POST["G2629_C51351"]."'";
                $LsqlI .= $separador."G2629_C51351";
                $LsqlV .= $separador."'".$_POST["G2629_C51351"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51352"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51352 = '".$_POST["G2629_C51352"]."'";
                $LsqlI .= $separador."G2629_C51352";
                $LsqlV .= $separador."'".$_POST["G2629_C51352"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51353"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51353 = '".$_POST["G2629_C51353"]."'";
                $LsqlI .= $separador."G2629_C51353";
                $LsqlV .= $separador."'".$_POST["G2629_C51353"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51354"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51354 = '".$_POST["G2629_C51354"]."'";
                $LsqlI .= $separador."G2629_C51354";
                $LsqlV .= $separador."'".$_POST["G2629_C51354"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51355"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51355 = '".$_POST["G2629_C51355"]."'";
                $LsqlI .= $separador."G2629_C51355";
                $LsqlV .= $separador."'".$_POST["G2629_C51355"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51356"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51356 = '".$_POST["G2629_C51356"]."'";
                $LsqlI .= $separador."G2629_C51356";
                $LsqlV .= $separador."'".$_POST["G2629_C51356"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51374 = '".$_POST["G2629_C51374"]."'";
                $LsqlI .= $separador."G2629_C51374";
                $LsqlV .= $separador."'".$_POST["G2629_C51374"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51375"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51375 = '".$_POST["G2629_C51375"]."'";
                $LsqlI .= $separador."G2629_C51375";
                $LsqlV .= $separador."'".$_POST["G2629_C51375"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51376 = '".$_POST["G2629_C51376"]."'";
                $LsqlI .= $separador."G2629_C51376";
                $LsqlV .= $separador."'".$_POST["G2629_C51376"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2629_C51377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51377 = '".$_POST["G2629_C51377"]."'";
                $LsqlI .= $separador."G2629_C51377";
                $LsqlV .= $separador."'".$_POST["G2629_C51377"]."'";
                $validar = 1;
            }
             
 
            $G2629_C51378 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2629_C51378"])){    
                if($_POST["G2629_C51378"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2629_C51378"]);
                    if(count($tieneHora) > 1){
                        $G2629_C51378 = "'".$_POST["G2629_C51378"]."'";
                    }else{
                        $G2629_C51378 = "'".str_replace(' ', '',$_POST["G2629_C51378"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2629_C51378 = ".$G2629_C51378;
                    $LsqlI .= $separador." G2629_C51378";
                    $LsqlV .= $separador.$G2629_C51378;
                    $validar = 1;
                }
            }
 
            $G2629_C51379 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2629_C51379"])){    
                if($_POST["G2629_C51379"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2629_C51379"]);
                    if(count($tieneHora) > 1){
                        $G2629_C51379 = "'".$_POST["G2629_C51379"]."'";
                    }else{
                        $G2629_C51379 = "'".str_replace(' ', '',$_POST["G2629_C51379"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2629_C51379 = ".$G2629_C51379;
                    $LsqlI .= $separador." G2629_C51379";
                    $LsqlV .= $separador.$G2629_C51379;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2629_C51580"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_C51580 = '".$_POST["G2629_C51580"]."'";
                $LsqlI .= $separador."G2629_C51580";
                $LsqlV .= $separador."'".$_POST["G2629_C51580"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2629_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2629_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2629_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2629_Usuario , G2629_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2629_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2629 WHERE G2629_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2629 SET G2629_UltiGest__b =-14, G2629_GesMasImp_b =-14, G2629_TipoReintentoUG_b =0, G2629_TipoReintentoGMI_b =0, G2629_EstadoUG_b =-14, G2629_EstadoGMI_b =-14, G2629_CantidadIntentos =0, G2629_CantidadIntentosGMI_b =0 WHERE G2629_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
