
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2629/G2629_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2629_ConsInte__b as id, G2629_C51312 as camp1 , G2629_C51313 as camp2 FROM ".$BaseDatos.".G2629  WHERE G2629_Usuario = ".$idUsuario." ORDER BY G2629_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2629_ConsInte__b as id, G2629_C51312 as camp1 , G2629_C51313 as camp2 FROM ".$BaseDatos.".G2629  ORDER BY G2629_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2629_ConsInte__b as id, G2629_C51312 as camp1 , G2629_C51313 as camp2 FROM ".$BaseDatos.".G2629 JOIN ".$BaseDatos.".G2629_M".$resultEstpas->muestr." ON G2629_ConsInte__b = G2629_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2629_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2629_ConsInte__b as id, G2629_C51312 as camp1 , G2629_C51313 as camp2 FROM ".$BaseDatos.".G2629 JOIN ".$BaseDatos.".G2629_M".$resultEstpas->muestr." ON G2629_ConsInte__b = G2629_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2629_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2629_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2629_ConsInte__b as id, G2629_C51312 as camp1 , G2629_C51313 as camp2 FROM ".$BaseDatos.".G2629  ORDER BY G2629_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="7799" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7799c">
                GENERAL
            </a>
        </h4>
        
    </div>
    <div id="s_7799c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51312" id="LblG2629_C51312">CEDULA</label><input type="text" class="form-control input-sm" id="G2629_C51312" value="<?php if (isset($_GET['G2629_C51312'])) {
                            echo $_GET['G2629_C51312'];
                        } ?>"  name="G2629_C51312"  placeholder="CEDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51313" id="LblG2629_C51313">NOMBRE TITULAR</label><input type="text" class="form-control input-sm" id="G2629_C51313" value="<?php if (isset($_GET['G2629_C51313'])) {
                            echo $_GET['G2629_C51313'];
                        } ?>"  name="G2629_C51313"  placeholder="NOMBRE TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51314" id="LblG2629_C51314">AÑO MORA</label><input type="text" class="form-control input-sm" id="G2629_C51314" value="<?php if (isset($_GET['G2629_C51314'])) {
                            echo $_GET['G2629_C51314'];
                        } ?>"  name="G2629_C51314"  placeholder="AÑO MORA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51315" id="LblG2629_C51315">TELEFONO 3</label><input type="text" class="form-control input-sm" id="G2629_C51315" value="<?php if (isset($_GET['G2629_C51315'])) {
                            echo $_GET['G2629_C51315'];
                        } ?>"  name="G2629_C51315"  placeholder="TELEFONO 3"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51316" id="LblG2629_C51316"> CAPITAL </label><input type="text" class="form-control input-sm" id="G2629_C51316" value="<?php if (isset($_GET['G2629_C51316'])) {
                            echo $_GET['G2629_C51316'];
                        } ?>"  name="G2629_C51316"  placeholder=" CAPITAL "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51317" id="LblG2629_C51317"> TOTAL A PAGAR </label><input type="text" class="form-control input-sm" id="G2629_C51317" value="<?php if (isset($_GET['G2629_C51317'])) {
                            echo $_GET['G2629_C51317'];
                        } ?>"  name="G2629_C51317"  placeholder=" TOTAL A PAGAR "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51559" id="LblG2629_C51559"> AGENTE </label><input type="text" class="form-control input-sm" id="G2629_C51559" value="<?php if (isset($_GET['G2629_C51559'])) {
                            echo $_GET['G2629_C51559'];
                        } ?>"  name="G2629_C51559"  placeholder=" AGENTE "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C55064" id="LblG2629_C55064">TELEFONO1</label><input type="text" class="form-control input-sm" id="G2629_C55064" value="<?php if (isset($_GET['G2629_C55064'])) {
                            echo $_GET['G2629_C55064'];
                        } ?>"  name="G2629_C55064"  placeholder="TELEFONO1"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C55065" id="LblG2629_C55065">TELEFONO2</label><input type="text" class="form-control input-sm" id="G2629_C55065" value="<?php if (isset($_GET['G2629_C55065'])) {
                            echo $_GET['G2629_C55065'];
                        } ?>"  name="G2629_C55065"  placeholder="TELEFONO2"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C55066" id="LblG2629_C55066">DIRECCION</label><input type="text" class="form-control input-sm" id="G2629_C55066" value="<?php if (isset($_GET['G2629_C55066'])) {
                            echo $_GET['G2629_C55066'];
                        } ?>"  name="G2629_C55066"  placeholder="DIRECCION"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C55067" id="LblG2629_C55067">TIPO DE SEGUIMIENTO</label><input type="text" class="form-control input-sm" id="G2629_C55067" value="<?php if (isset($_GET['G2629_C55067'])) {
                            echo $_GET['G2629_C55067'];
                        } ?>"  name="G2629_C55067"  placeholder="TIPO DE SEGUIMIENTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C55072" id="LblG2629_C55072">CIUDAD</label><input type="text" class="form-control input-sm" id="G2629_C55072" value="<?php if (isset($_GET['G2629_C55072'])) {
                            echo $_GET['G2629_C55072'];
                        } ?>"  name="G2629_C55072"  placeholder="CIUDAD"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C56099" id="LblG2629_C56099">ASESOR</label><input type="text" class="form-control input-sm" id="G2629_C56099" value="<?php if (isset($_GET['G2629_C56099'])) {
                            echo $_GET['G2629_C56099'];
                        } ?>"  name="G2629_C56099"  placeholder="ASESOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C56637" id="LblG2629_C56637">ESTADO(cargue)</label><input type="text" class="form-control input-sm" id="G2629_C56637" value="<?php if (isset($_GET['G2629_C56637'])) {
                            echo $_GET['G2629_C56637'];
                        } ?>"  name="G2629_C56637"  placeholder="ESTADO(cargue)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C57026" id="LblG2629_C57026">TELEFONO 4</label><input type="text" class="form-control input-sm" id="G2629_C57026" value="<?php if (isset($_GET['G2629_C57026'])) {
                            echo $_GET['G2629_C57026'];
                        } ?>"  name="G2629_C57026"  placeholder="TELEFONO 4"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="7800" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51294" id="LblG2629_C51294">ORIGEN_DY_WF</label><input type="text" class="form-control input-sm" id="G2629_C51294" value="<?php if (isset($_GET['G2629_C51294'])) {
                            echo $_GET['G2629_C51294'];
                        } ?>" readonly name="G2629_C51294"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51295" id="LblG2629_C51295">OPTIN_DY_WF</label><input type="text" class="form-control input-sm" id="G2629_C51295" value="<?php if (isset($_GET['G2629_C51295'])) {
                            echo $_GET['G2629_C51295'];
                        } ?>" readonly name="G2629_C51295"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2629_C51296" id="LblG2629_C51296">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2629_C51296" id="G2629_C51296">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3003 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="7809" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7809c">
                PERFIL DEUDOR
            </a>
        </h4>
        
    </div>
    <div id="s_7809c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2629_C51348" id="LblG2629_C51348">ESTADO ACTUAL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2629_C51348" id="G2629_C51348">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2575 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51349" id="LblG2629_C51349">LUGAR DE TRABAJO</label><input type="text" class="form-control input-sm" id="G2629_C51349" value="<?php if (isset($_GET['G2629_C51349'])) {
                            echo $_GET['G2629_C51349'];
                        } ?>"  name="G2629_C51349"  placeholder="LUGAR DE TRABAJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51350" id="LblG2629_C51350">CARGO O ACTIVIDAD ECONOMICA</label><input type="text" class="form-control input-sm" id="G2629_C51350" value="<?php if (isset($_GET['G2629_C51350'])) {
                            echo $_GET['G2629_C51350'];
                        } ?>"  name="G2629_C51350"  placeholder="CARGO O ACTIVIDAD ECONOMICA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51351" id="LblG2629_C51351">NOMBRE DE LA EMPRESA</label><input type="text" class="form-control input-sm" id="G2629_C51351" value="<?php if (isset($_GET['G2629_C51351'])) {
                            echo $_GET['G2629_C51351'];
                        } ?>"  name="G2629_C51351"  placeholder="NOMBRE DE LA EMPRESA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51352" id="LblG2629_C51352">DIRECCIÓN ACTUALIZADA</label><input type="text" class="form-control input-sm" id="G2629_C51352" value="<?php if (isset($_GET['G2629_C51352'])) {
                            echo $_GET['G2629_C51352'];
                        } ?>"  name="G2629_C51352"  placeholder="DIRECCIÓN ACTUALIZADA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51353" id="LblG2629_C51353">TELEFONO-WHATSAPP</label><input type="text" class="form-control input-sm" id="G2629_C51353" value="<?php if (isset($_GET['G2629_C51353'])) {
                            echo $_GET['G2629_C51353'];
                        } ?>"  name="G2629_C51353"  placeholder="TELEFONO-WHATSAPP"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51354" id="LblG2629_C51354">CORREO ELECTRONICO</label><input type="text" class="form-control input-sm" id="G2629_C51354" value="<?php if (isset($_GET['G2629_C51354'])) {
                            echo $_GET['G2629_C51354'];
                        } ?>"  name="G2629_C51354"  placeholder="CORREO ELECTRONICO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51355" id="LblG2629_C51355">REFERENCIA</label><input type="text" class="form-control input-sm" id="G2629_C51355" value="<?php if (isset($_GET['G2629_C51355'])) {
                            echo $_GET['G2629_C51355'];
                        } ?>"  name="G2629_C51355"  placeholder="REFERENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51356" id="LblG2629_C51356">TELEFONO REFERENCIA</label><input type="text" class="form-control input-sm" id="G2629_C51356" value="<?php if (isset($_GET['G2629_C51356'])) {
                            echo $_GET['G2629_C51356'];
                        } ?>"  name="G2629_C51356"  placeholder="TELEFONO REFERENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="7812" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7812c">
                CONTACTO
            </a>
        </h4>
        
    </div>
    <div id="s_7812c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2629_C51374" id="LblG2629_C51374">TIPO DE CONTACTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2629_C51374" id="G2629_C51374">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2903 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2629_C51375" id="LblG2629_C51375">RESULTADO  CONTACTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2629_C51375" id="G2629_C51375">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2590 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2629_C51376" id="LblG2629_C51376">TIPO NEGOCIACIÓN </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2629_C51376" id="G2629_C51376">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3008 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51377" id="LblG2629_C51377">VALOR ACUERDO</label><input type="text" class="form-control input-sm" id="G2629_C51377" value="<?php if (isset($_GET['G2629_C51377'])) {
                            echo $_GET['G2629_C51377'];
                        } ?>"  name="G2629_C51377"  placeholder="VALOR ACUERDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2629_C51378" id="LblG2629_C51378">FECHA PAGO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2629_C51378'])) {
                            echo $_GET['G2629_C51378'];
                        } ?>"  name="G2629_C51378" id="G2629_C51378" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2629_C51379" id="LblG2629_C51379">FECHA PRÓXIMO PAGO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2629_C51379'])) {
                            echo $_GET['G2629_C51379'];
                        } ?>"  name="G2629_C51379" id="G2629_C51379" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2629_C51580" id="LblG2629_C51580">NUMERO DE CUOTAS</label><input type="text" class="form-control input-sm" id="G2629_C51580" value="<?php if (isset($_GET['G2629_C51580'])) {
                            echo $_GET['G2629_C51580'];
                        } ?>"  name="G2629_C51580"  placeholder="NUMERO DE CUOTAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2629/G2629_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2629_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2629_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2629_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2629_C51312").val(item.G2629_C51312); 
                $("#G2629_C51313").val(item.G2629_C51313); 
                $("#G2629_C51314").val(item.G2629_C51314); 
                $("#G2629_C51315").val(item.G2629_C51315); 
                $("#G2629_C51316").val(item.G2629_C51316); 
                $("#G2629_C51317").val(item.G2629_C51317); 
                $("#G2629_C51559").val(item.G2629_C51559); 
                $("#G2629_C55064").val(item.G2629_C55064); 
                $("#G2629_C55065").val(item.G2629_C55065); 
                $("#G2629_C55066").val(item.G2629_C55066); 
                $("#G2629_C55067").val(item.G2629_C55067); 
                $("#G2629_C55072").val(item.G2629_C55072); 
                $("#G2629_C56099").val(item.G2629_C56099); 
                $("#G2629_C56637").val(item.G2629_C56637); 
                $("#G2629_C57026").val(item.G2629_C57026); 
                $("#G2629_C51294").val(item.G2629_C51294); 
                $("#G2629_C51295").val(item.G2629_C51295); 
                $("#G2629_C51296").val(item.G2629_C51296).trigger("change");  
                $("#G2629_C51348").val(item.G2629_C51348).trigger("change");  
                $("#G2629_C51349").val(item.G2629_C51349); 
                $("#G2629_C51350").val(item.G2629_C51350); 
                $("#G2629_C51351").val(item.G2629_C51351); 
                $("#G2629_C51352").val(item.G2629_C51352); 
                $("#G2629_C51353").val(item.G2629_C51353); 
                $("#G2629_C51354").val(item.G2629_C51354); 
                $("#G2629_C51355").val(item.G2629_C51355); 
                $("#G2629_C51356").val(item.G2629_C51356); 
                $("#G2629_C51374").attr("opt",item.G2629_C51374);  
                $("#G2629_C51375").val(item.G2629_C51375).trigger("change");  
                $("#G2629_C51376").val(item.G2629_C51376).trigger("change");  
                $("#G2629_C51377").val(item.G2629_C51377); 
                $("#G2629_C51378").val(item.G2629_C51378); 
                $("#G2629_C51379").val(item.G2629_C51379); 
                $("#G2629_C51580").val(item.G2629_C51580);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2629_C51296").select2();

    $("#G2629_C51348").select2();

    $("#G2629_C51374").select2();

    $("#G2629_C51375").select2();

    $("#G2629_C51376").select2();
        //datepickers
        

        $("#G2629_C51378").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2629_C51379").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G2629_C51296").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO ACTUAL 

    $("#G2629_C51348").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2903' , idPadre : $(this).val() },
            success : function(data){
                var optG2629_C51374 = $("#G2629_C51374").attr("opt");
                $("#G2629_C51374").html(data);
                if (optG2629_C51374 != null) {
                    $("#G2629_C51374").val(optG2629_C51374).trigger("change");
                }
            }
        });
        
    });

    //function para TIPO DE CONTACTO 

    $("#G2629_C51374").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para RESULTADO  CONTACTO 

    $("#G2629_C51375").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO NEGOCIACIÓN  

    $("#G2629_C51376").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2629_C51312").val(item.G2629_C51312);
 
                                                $("#G2629_C51313").val(item.G2629_C51313);
 
                                                $("#G2629_C51314").val(item.G2629_C51314);
 
                                                $("#G2629_C51315").val(item.G2629_C51315);
 
                                                $("#G2629_C51316").val(item.G2629_C51316);
 
                                                $("#G2629_C51317").val(item.G2629_C51317);
 
                                                $("#G2629_C51559").val(item.G2629_C51559);
 
                                                $("#G2629_C55064").val(item.G2629_C55064);
 
                                                $("#G2629_C55065").val(item.G2629_C55065);
 
                                                $("#G2629_C55066").val(item.G2629_C55066);
 
                                                $("#G2629_C55067").val(item.G2629_C55067);
 
                                                $("#G2629_C55072").val(item.G2629_C55072);
 
                                                $("#G2629_C56099").val(item.G2629_C56099);
 
                                                $("#G2629_C56637").val(item.G2629_C56637);
 
                                                $("#G2629_C57026").val(item.G2629_C57026);
 
                                                $("#G2629_C51294").val(item.G2629_C51294);
 
                                                $("#G2629_C51295").val(item.G2629_C51295);
 
                    $("#G2629_C51296").val(item.G2629_C51296).trigger("change"); 
 
                    $("#G2629_C51348").val(item.G2629_C51348).trigger("change"); 
 
                                                $("#G2629_C51349").val(item.G2629_C51349);
 
                                                $("#G2629_C51350").val(item.G2629_C51350);
 
                                                $("#G2629_C51351").val(item.G2629_C51351);
 
                                                $("#G2629_C51352").val(item.G2629_C51352);
 
                                                $("#G2629_C51353").val(item.G2629_C51353);
 
                                                $("#G2629_C51354").val(item.G2629_C51354);
 
                                                $("#G2629_C51355").val(item.G2629_C51355);
 
                                                $("#G2629_C51356").val(item.G2629_C51356);
 
                    $("#G2629_C51374").attr("opt",item.G2629_C51374); 
 
                    $("#G2629_C51375").val(item.G2629_C51375).trigger("change"); 
 
                    $("#G2629_C51376").val(item.G2629_C51376).trigger("change"); 
 
                                                $("#G2629_C51377").val(item.G2629_C51377);
 
                                                $("#G2629_C51378").val(item.G2629_C51378);
 
                                                $("#G2629_C51379").val(item.G2629_C51379);
 
                                                $("#G2629_C51580").val(item.G2629_C51580);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CEDULA','NOMBRE TITULAR','AÑO MORA','TELEFONO 3',' CAPITAL ',' TOTAL A PAGAR ',' AGENTE ','TELEFONO1','TELEFONO2','DIRECCION','TIPO DE SEGUIMIENTO','CIUDAD','ASESOR','ESTADO(cargue)','TELEFONO 4','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','ESTADO ACTUAL','LUGAR DE TRABAJO','CARGO O ACTIVIDAD ECONOMICA','NOMBRE DE LA EMPRESA','DIRECCIÓN ACTUALIZADA','TELEFONO-WHATSAPP','CORREO ELECTRONICO','REFERENCIA','TELEFONO REFERENCIA','TIPO DE CONTACTO','RESULTADO  CONTACTO','TIPO NEGOCIACIÓN ','VALOR ACUERDO','FECHA PAGO','FECHA PRÓXIMO PAGO','NUMERO DE CUOTAS'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2629_C51312', 
                        index: 'G2629_C51312', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51313', 
                        index: 'G2629_C51313', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51314', 
                        index: 'G2629_C51314', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51315', 
                        index: 'G2629_C51315', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51316', 
                        index: 'G2629_C51316', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51317', 
                        index: 'G2629_C51317', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51559', 
                        index: 'G2629_C51559', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C55064', 
                        index: 'G2629_C55064', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C55065', 
                        index: 'G2629_C55065', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C55066', 
                        index: 'G2629_C55066', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C55067', 
                        index: 'G2629_C55067', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C55072', 
                        index: 'G2629_C55072', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C56099', 
                        index: 'G2629_C56099', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C56637', 
                        index: 'G2629_C56637', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C57026', 
                        index: 'G2629_C57026', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51294', 
                        index: 'G2629_C51294', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51295', 
                        index: 'G2629_C51295', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51296', 
                        index:'G2629_C51296', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3003&campo=G2629_C51296'
                        }
                    }

                    ,
                    { 
                        name:'G2629_C51348', 
                        index:'G2629_C51348', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2575&campo=G2629_C51348'
                        }
                    }

                    ,
                    { 
                        name:'G2629_C51349', 
                        index: 'G2629_C51349', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51350', 
                        index: 'G2629_C51350', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51351', 
                        index: 'G2629_C51351', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51352', 
                        index: 'G2629_C51352', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51353', 
                        index: 'G2629_C51353', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51354', 
                        index: 'G2629_C51354', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51355', 
                        index: 'G2629_C51355', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51356', 
                        index: 'G2629_C51356', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2629_C51374', 
                        index:'G2629_C51374', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2903&campo=G2629_C51374'
                        }
                    }

                    ,
                    { 
                        name:'G2629_C51375', 
                        index:'G2629_C51375', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2590&campo=G2629_C51375'
                        }
                    }

                    ,
                    { 
                        name:'G2629_C51376', 
                        index:'G2629_C51376', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3008&campo=G2629_C51376'
                        }
                    }

                    ,
                    { 
                        name:'G2629_C51377', 
                        index: 'G2629_C51377', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2629_C51378', 
                        index:'G2629_C51378', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2629_C51379', 
                        index:'G2629_C51379', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2629_C51580', 
                        index: 'G2629_C51580', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2629_C51312',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2629_C51312").val(item.G2629_C51312);

                        $("#G2629_C51313").val(item.G2629_C51313);

                        $("#G2629_C51314").val(item.G2629_C51314);

                        $("#G2629_C51315").val(item.G2629_C51315);

                        $("#G2629_C51316").val(item.G2629_C51316);

                        $("#G2629_C51317").val(item.G2629_C51317);

                        $("#G2629_C51559").val(item.G2629_C51559);

                        $("#G2629_C55064").val(item.G2629_C55064);

                        $("#G2629_C55065").val(item.G2629_C55065);

                        $("#G2629_C55066").val(item.G2629_C55066);

                        $("#G2629_C55067").val(item.G2629_C55067);

                        $("#G2629_C55072").val(item.G2629_C55072);

                        $("#G2629_C56099").val(item.G2629_C56099);

                        $("#G2629_C56637").val(item.G2629_C56637);

                        $("#G2629_C57026").val(item.G2629_C57026);

                        $("#G2629_C51294").val(item.G2629_C51294);

                        $("#G2629_C51295").val(item.G2629_C51295);
 
                    $("#G2629_C51296").val(item.G2629_C51296).trigger("change"); 
 
                    $("#G2629_C51348").val(item.G2629_C51348).trigger("change"); 

                        $("#G2629_C51349").val(item.G2629_C51349);

                        $("#G2629_C51350").val(item.G2629_C51350);

                        $("#G2629_C51351").val(item.G2629_C51351);

                        $("#G2629_C51352").val(item.G2629_C51352);

                        $("#G2629_C51353").val(item.G2629_C51353);

                        $("#G2629_C51354").val(item.G2629_C51354);

                        $("#G2629_C51355").val(item.G2629_C51355);

                        $("#G2629_C51356").val(item.G2629_C51356);
 
                    $("#G2629_C51374").attr("opt",item.G2629_C51374); 
 
                    $("#G2629_C51375").val(item.G2629_C51375).trigger("change"); 
 
                    $("#G2629_C51376").val(item.G2629_C51376).trigger("change"); 

                        $("#G2629_C51377").val(item.G2629_C51377);

                        $("#G2629_C51378").val(item.G2629_C51378);

                        $("#G2629_C51379").val(item.G2629_C51379);

                        $("#G2629_C51580").val(item.G2629_C51580);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
