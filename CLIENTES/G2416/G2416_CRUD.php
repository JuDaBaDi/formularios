<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55287")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55287");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2416_ConsInte__b, G2416_FechaInsercion , G2416_Usuario ,  G2416_CodigoMiembro  , G2416_PoblacionOrigen , G2416_EstadoDiligenciamiento ,  G2416_IdLlamada , G2416_C47482 as principal ,G2416_C47481,G2416_C47482,G2416_C47483,G2416_C47484,G2416_C47485,G2416_C47486,G2416_C47487,G2416_C47488,G2416_C55286,G2416_C55287,G2416_C47470,G2416_C47471,G2416_C47472,G2416_C47473,G2416_C47474,G2416_C47475,G2416_C47476,G2416_C47477,G2416_C47478 FROM '.$BaseDatos.'.G2416 WHERE G2416_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2416_C47481'] = $key->G2416_C47481;

                $datos[$i]['G2416_C47482'] = $key->G2416_C47482;

                $datos[$i]['G2416_C47483'] = $key->G2416_C47483;

                $datos[$i]['G2416_C47484'] = $key->G2416_C47484;

                $datos[$i]['G2416_C47485'] = $key->G2416_C47485;

                $datos[$i]['G2416_C47486'] = $key->G2416_C47486;

                $datos[$i]['G2416_C47487'] = $key->G2416_C47487;

                $datos[$i]['G2416_C47488'] = $key->G2416_C47488;

                $datos[$i]['G2416_C55286'] = $key->G2416_C55286;

                $datos[$i]['G2416_C55287'] = $key->G2416_C55287;

                $datos[$i]['G2416_C47470'] = $key->G2416_C47470;

                $datos[$i]['G2416_C47471'] = $key->G2416_C47471;

                $datos[$i]['G2416_C47472'] = explode(' ', $key->G2416_C47472)[0];
  
                $hora = '';
                if(!is_null($key->G2416_C47473)){
                    $hora = explode(' ', $key->G2416_C47473)[1];
                }

                $datos[$i]['G2416_C47473'] = $hora;

                $datos[$i]['G2416_C47474'] = $key->G2416_C47474;

                $datos[$i]['G2416_C47475'] = $key->G2416_C47475;

                $datos[$i]['G2416_C47476'] = $key->G2416_C47476;

                $datos[$i]['G2416_C47477'] = $key->G2416_C47477;

                $datos[$i]['G2416_C47478'] = $key->G2416_C47478;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2416";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2416_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2416_ConsInte__b as id,  G2416_C47482 as camp1 , G2416_C55287 as camp2 
                     FROM ".$BaseDatos.".G2416  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2416_ConsInte__b as id,  G2416_C47482 as camp1 , G2416_C55287 as camp2  
                    FROM ".$BaseDatos.".G2416  JOIN ".$BaseDatos.".G2416_M".$_POST['muestra']." ON G2416_ConsInte__b = G2416_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2416_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2416_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2416_C47482 LIKE '%".$B."%' OR G2416_C55287 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2416_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2416");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2416_ConsInte__b, G2416_FechaInsercion , G2416_Usuario ,  G2416_CodigoMiembro  , G2416_PoblacionOrigen , G2416_EstadoDiligenciamiento ,  G2416_IdLlamada , G2416_C47482 as principal ,G2416_C47481,G2416_C47482,G2416_C47483,G2416_C47484,G2416_C47485, a.LISOPC_Nombre____b as G2416_C47486, b.LISOPC_Nombre____b as G2416_C47487,G2416_C47488,G2416_C55286,G2416_C55287, c.LISOPC_Nombre____b as G2416_C47470, d.LISOPC_Nombre____b as G2416_C47471,G2416_C47472,G2416_C47473,G2416_C47474,G2416_C47475,G2416_C47476,G2416_C47477,G2416_C47478 FROM '.$BaseDatos.'.G2416 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2416_C47486 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2416_C47487 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2416_C47470 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2416_C47471';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2416_C47473)){
                    $hora_a = explode(' ', $fila->G2416_C47473)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2416_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2416_ConsInte__b , ($fila->G2416_C47481) , ($fila->G2416_C47482) , ($fila->G2416_C47483) , ($fila->G2416_C47484) , ($fila->G2416_C47485) , ($fila->G2416_C47486) , ($fila->G2416_C47487) , ($fila->G2416_C47488) , ($fila->G2416_C55286) , ($fila->G2416_C55287) , ($fila->G2416_C47470) , ($fila->G2416_C47471) , explode(' ', $fila->G2416_C47472)[0] , $hora_a , ($fila->G2416_C47474) , ($fila->G2416_C47475) , ($fila->G2416_C47476) , ($fila->G2416_C47477) , ($fila->G2416_C47478) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2416 WHERE G2416_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2416";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2416_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2416_ConsInte__b as id,  G2416_C47482 as camp1 , G2416_C55287 as camp2  FROM '.$BaseDatos.'.G2416 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2416_ConsInte__b as id,  G2416_C47482 as camp1 , G2416_C55287 as camp2  
                    FROM ".$BaseDatos.".G2416  JOIN ".$BaseDatos.".G2416_M".$_POST['muestra']." ON G2416_ConsInte__b = G2416_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2416_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2416_C47482 LIKE "%'.$B.'%" OR G2416_C55287 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2416_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2416 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2416(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2416_C47481"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47481 = '".$_POST["G2416_C47481"]."'";
                $LsqlI .= $separador."G2416_C47481";
                $LsqlV .= $separador."'".$_POST["G2416_C47481"]."'";
                $validar = 1;
            }
             
  
            $G2416_C47482 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2416_C47482"])){
                if($_POST["G2416_C47482"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2416_C47482 = $_POST["G2416_C47482"];
                    $LsqlU .= $separador." G2416_C47482 = ".$G2416_C47482."";
                    $LsqlI .= $separador." G2416_C47482";
                    $LsqlV .= $separador.$G2416_C47482;
                    $validar = 1;
                }
            }
  
            $G2416_C47483 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2416_C47483"])){
                if($_POST["G2416_C47483"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2416_C47483 = $_POST["G2416_C47483"];
                    $LsqlU .= $separador." G2416_C47483 = ".$G2416_C47483."";
                    $LsqlI .= $separador." G2416_C47483";
                    $LsqlV .= $separador.$G2416_C47483;
                    $validar = 1;
                }
            }
  
            $G2416_C47484 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2416_C47484"])){
                if($_POST["G2416_C47484"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2416_C47484 = $_POST["G2416_C47484"];
                    $LsqlU .= $separador." G2416_C47484 = ".$G2416_C47484."";
                    $LsqlI .= $separador." G2416_C47484";
                    $LsqlV .= $separador.$G2416_C47484;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2416_C47485"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47485 = '".$_POST["G2416_C47485"]."'";
                $LsqlI .= $separador."G2416_C47485";
                $LsqlV .= $separador."'".$_POST["G2416_C47485"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C47486"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47486 = '".$_POST["G2416_C47486"]."'";
                $LsqlI .= $separador."G2416_C47486";
                $LsqlV .= $separador."'".$_POST["G2416_C47486"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C47487"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47487 = '".$_POST["G2416_C47487"]."'";
                $LsqlI .= $separador."G2416_C47487";
                $LsqlV .= $separador."'".$_POST["G2416_C47487"]."'";
                $validar = 1;
            }
             
  
            $G2416_C47488 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2416_C47488"])){
                if($_POST["G2416_C47488"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2416_C47488 = $_POST["G2416_C47488"];
                    $LsqlU .= $separador." G2416_C47488 = ".$G2416_C47488."";
                    $LsqlI .= $separador." G2416_C47488";
                    $LsqlV .= $separador.$G2416_C47488;
                    $validar = 1;
                }
            }
  
            $G2416_C55286 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2416_C55286"])){
                if($_POST["G2416_C55286"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2416_C55286 = $_POST["G2416_C55286"];
                    $LsqlU .= $separador." G2416_C55286 = ".$G2416_C55286."";
                    $LsqlI .= $separador." G2416_C55286";
                    $LsqlV .= $separador.$G2416_C55286;
                    $validar = 1;
                }
            }
  
            $G2416_C55287 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2416_C55287"])){
                if($_POST["G2416_C55287"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2416_C55287 = $_POST["G2416_C55287"];
                    $LsqlU .= $separador." G2416_C55287 = ".$G2416_C55287."";
                    $LsqlI .= $separador." G2416_C55287";
                    $LsqlV .= $separador.$G2416_C55287;
                    $validar = 1;
                }
            }
 
            $G2416_C47470 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2416_C47470 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2416_C47470 = ".$G2416_C47470;
                    $LsqlI .= $separador." G2416_C47470";
                    $LsqlV .= $separador.$G2416_C47470;
                    $validar = 1;

                    
                }
            }
 
            $G2416_C47471 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2416_C47471 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2416_C47471 = ".$G2416_C47471;
                    $LsqlI .= $separador." G2416_C47471";
                    $LsqlV .= $separador.$G2416_C47471;
                    $validar = 1;
                }
            }
 
            $G2416_C47472 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2416_C47472 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2416_C47472 = ".$G2416_C47472;
                    $LsqlI .= $separador." G2416_C47472";
                    $LsqlV .= $separador.$G2416_C47472;
                    $validar = 1;
                }
            }
 
            $G2416_C47473 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2416_C47473 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2416_C47473 = ".$G2416_C47473;
                    $LsqlI .= $separador." G2416_C47473";
                    $LsqlV .= $separador.$G2416_C47473;
                    $validar = 1;
                }
            }
 
            $G2416_C47474 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2416_C47474 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2416_C47474 = ".$G2416_C47474;
                    $LsqlI .= $separador." G2416_C47474";
                    $LsqlV .= $separador.$G2416_C47474;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2416_C47475"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47475 = '".$_POST["G2416_C47475"]."'";
                $LsqlI .= $separador."G2416_C47475";
                $LsqlV .= $separador."'".$_POST["G2416_C47475"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C47476"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47476 = '".$_POST["G2416_C47476"]."'";
                $LsqlI .= $separador."G2416_C47476";
                $LsqlV .= $separador."'".$_POST["G2416_C47476"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C47477"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47477 = '".$_POST["G2416_C47477"]."'";
                $LsqlI .= $separador."G2416_C47477";
                $LsqlV .= $separador."'".$_POST["G2416_C47477"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C47478"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47478 = '".$_POST["G2416_C47478"]."'";
                $LsqlI .= $separador."G2416_C47478";
                $LsqlV .= $separador."'".$_POST["G2416_C47478"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C47479"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C47479 = '".$_POST["G2416_C47479"]."'";
                $LsqlI .= $separador."G2416_C47479";
                $LsqlV .= $separador."'".$_POST["G2416_C47479"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2416_C55944"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_C55944 = '".$_POST["G2416_C55944"]."'";
                $LsqlI .= $separador."G2416_C55944";
                $LsqlV .= $separador."'".$_POST["G2416_C55944"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2416_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2416_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2416_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2416_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2416_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2416_Usuario , G2416_FechaInsercion, G2416_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2416_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2416 WHERE G2416_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
