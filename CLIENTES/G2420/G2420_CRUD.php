<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2420_ConsInte__b, G2420_FechaInsercion , G2420_Usuario ,  G2420_CodigoMiembro  , G2420_PoblacionOrigen , G2420_EstadoDiligenciamiento ,  G2420_IdLlamada , G2420_C47541 as principal ,G2420_C47541,G2420_C47542,G2420_C47543,G2420_C47544,G2420_C47545,G2420_C47546,G2420_C47547,G2420_C47548,G2420_C47530,G2420_C47531,G2420_C47532,G2420_C47533,G2420_C47534,G2420_C47535,G2420_C47536,G2420_C47537,G2420_C47538 FROM '.$BaseDatos.'.G2420 WHERE G2420_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2420_C47541'] = $key->G2420_C47541;

                $datos[$i]['G2420_C47542'] = $key->G2420_C47542;

                $datos[$i]['G2420_C47543'] = $key->G2420_C47543;

                $datos[$i]['G2420_C47544'] = $key->G2420_C47544;

                $datos[$i]['G2420_C47545'] = $key->G2420_C47545;

                $datos[$i]['G2420_C47546'] = $key->G2420_C47546;

                $datos[$i]['G2420_C47547'] = $key->G2420_C47547;

                $datos[$i]['G2420_C47548'] = $key->G2420_C47548;

                $datos[$i]['G2420_C47530'] = $key->G2420_C47530;

                $datos[$i]['G2420_C47531'] = $key->G2420_C47531;

                $datos[$i]['G2420_C47532'] = explode(' ', $key->G2420_C47532)[0];
  
                $hora = '';
                if(!is_null($key->G2420_C47533)){
                    $hora = explode(' ', $key->G2420_C47533)[1];
                }

                $datos[$i]['G2420_C47533'] = $hora;

                $datos[$i]['G2420_C47534'] = $key->G2420_C47534;

                $datos[$i]['G2420_C47535'] = $key->G2420_C47535;

                $datos[$i]['G2420_C47536'] = $key->G2420_C47536;

                $datos[$i]['G2420_C47537'] = $key->G2420_C47537;

                $datos[$i]['G2420_C47538'] = $key->G2420_C47538;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2420";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2420_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2420_ConsInte__b as id,  G2420_C47541 as camp1 , G2420_C47542 as camp2 
                     FROM ".$BaseDatos.".G2420  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2420_ConsInte__b as id,  G2420_C47541 as camp1 , G2420_C47542 as camp2  
                    FROM ".$BaseDatos.".G2420  JOIN ".$BaseDatos.".G2420_M".$_POST['muestra']." ON G2420_ConsInte__b = G2420_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2420_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2420_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2420_C47541 LIKE '%".$B."%' OR G2420_C47542 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2420_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2420");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2420_ConsInte__b, G2420_FechaInsercion , G2420_Usuario ,  G2420_CodigoMiembro  , G2420_PoblacionOrigen , G2420_EstadoDiligenciamiento ,  G2420_IdLlamada , G2420_C47541 as principal ,G2420_C47541,G2420_C47542,G2420_C47543,G2420_C47544,G2420_C47545,G2420_C47546,G2420_C47547,G2420_C47548, a.LISOPC_Nombre____b as G2420_C47530, b.LISOPC_Nombre____b as G2420_C47531,G2420_C47532,G2420_C47533,G2420_C47534,G2420_C47535,G2420_C47536,G2420_C47537,G2420_C47538 FROM '.$BaseDatos.'.G2420 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2420_C47530 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2420_C47531';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2420_C47533)){
                    $hora_a = explode(' ', $fila->G2420_C47533)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2420_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2420_ConsInte__b , ($fila->G2420_C47541) , ($fila->G2420_C47542) , ($fila->G2420_C47543) , ($fila->G2420_C47544) , ($fila->G2420_C47545) , ($fila->G2420_C47546) , ($fila->G2420_C47547) , ($fila->G2420_C47548) , ($fila->G2420_C47530) , ($fila->G2420_C47531) , explode(' ', $fila->G2420_C47532)[0] , $hora_a , ($fila->G2420_C47534) , ($fila->G2420_C47535) , ($fila->G2420_C47536) , ($fila->G2420_C47537) , ($fila->G2420_C47538) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2420 WHERE G2420_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2420";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2420_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2420_ConsInte__b as id,  G2420_C47541 as camp1 , G2420_C47542 as camp2  FROM '.$BaseDatos.'.G2420 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2420_ConsInte__b as id,  G2420_C47541 as camp1 , G2420_C47542 as camp2  
                    FROM ".$BaseDatos.".G2420  JOIN ".$BaseDatos.".G2420_M".$_POST['muestra']." ON G2420_ConsInte__b = G2420_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2420_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2420_C47541 LIKE "%'.$B.'%" OR G2420_C47542 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2420_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2420 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2420(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2420_C47541"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47541 = '".$_POST["G2420_C47541"]."'";
                $LsqlI .= $separador."G2420_C47541";
                $LsqlV .= $separador."'".$_POST["G2420_C47541"]."'";
                $validar = 1;
            }
             
  
            $G2420_C47542 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2420_C47542"])){
                if($_POST["G2420_C47542"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2420_C47542 = $_POST["G2420_C47542"];
                    $LsqlU .= $separador." G2420_C47542 = ".$G2420_C47542."";
                    $LsqlI .= $separador." G2420_C47542";
                    $LsqlV .= $separador.$G2420_C47542;
                    $validar = 1;
                }
            }
  
            $G2420_C47543 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2420_C47543"])){
                if($_POST["G2420_C47543"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2420_C47543 = $_POST["G2420_C47543"];
                    $LsqlU .= $separador." G2420_C47543 = ".$G2420_C47543."";
                    $LsqlI .= $separador." G2420_C47543";
                    $LsqlV .= $separador.$G2420_C47543;
                    $validar = 1;
                }
            }
  
            $G2420_C47544 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2420_C47544"])){
                if($_POST["G2420_C47544"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2420_C47544 = $_POST["G2420_C47544"];
                    $LsqlU .= $separador." G2420_C47544 = ".$G2420_C47544."";
                    $LsqlI .= $separador." G2420_C47544";
                    $LsqlV .= $separador.$G2420_C47544;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2420_C47545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47545 = '".$_POST["G2420_C47545"]."'";
                $LsqlI .= $separador."G2420_C47545";
                $LsqlV .= $separador."'".$_POST["G2420_C47545"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47546"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47546 = '".$_POST["G2420_C47546"]."'";
                $LsqlI .= $separador."G2420_C47546";
                $LsqlV .= $separador."'".$_POST["G2420_C47546"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47547"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47547 = '".$_POST["G2420_C47547"]."'";
                $LsqlI .= $separador."G2420_C47547";
                $LsqlV .= $separador."'".$_POST["G2420_C47547"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47548 = '".$_POST["G2420_C47548"]."'";
                $LsqlI .= $separador."G2420_C47548";
                $LsqlV .= $separador."'".$_POST["G2420_C47548"]."'";
                $validar = 1;
            }
             
 
            $G2420_C47530 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2420_C47530 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2420_C47530 = ".$G2420_C47530;
                    $LsqlI .= $separador." G2420_C47530";
                    $LsqlV .= $separador.$G2420_C47530;
                    $validar = 1;

                    
                }
            }
 
            $G2420_C47531 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2420_C47531 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2420_C47531 = ".$G2420_C47531;
                    $LsqlI .= $separador." G2420_C47531";
                    $LsqlV .= $separador.$G2420_C47531;
                    $validar = 1;
                }
            }
 
            $G2420_C47532 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2420_C47532 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2420_C47532 = ".$G2420_C47532;
                    $LsqlI .= $separador." G2420_C47532";
                    $LsqlV .= $separador.$G2420_C47532;
                    $validar = 1;
                }
            }
 
            $G2420_C47533 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2420_C47533 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2420_C47533 = ".$G2420_C47533;
                    $LsqlI .= $separador." G2420_C47533";
                    $LsqlV .= $separador.$G2420_C47533;
                    $validar = 1;
                }
            }
 
            $G2420_C47534 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2420_C47534 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2420_C47534 = ".$G2420_C47534;
                    $LsqlI .= $separador." G2420_C47534";
                    $LsqlV .= $separador.$G2420_C47534;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2420_C47535"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47535 = '".$_POST["G2420_C47535"]."'";
                $LsqlI .= $separador."G2420_C47535";
                $LsqlV .= $separador."'".$_POST["G2420_C47535"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47536"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47536 = '".$_POST["G2420_C47536"]."'";
                $LsqlI .= $separador."G2420_C47536";
                $LsqlV .= $separador."'".$_POST["G2420_C47536"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47537"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47537 = '".$_POST["G2420_C47537"]."'";
                $LsqlI .= $separador."G2420_C47537";
                $LsqlV .= $separador."'".$_POST["G2420_C47537"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47538"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47538 = '".$_POST["G2420_C47538"]."'";
                $LsqlI .= $separador."G2420_C47538";
                $LsqlV .= $separador."'".$_POST["G2420_C47538"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47539"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47539 = '".$_POST["G2420_C47539"]."'";
                $LsqlI .= $separador."G2420_C47539";
                $LsqlV .= $separador."'".$_POST["G2420_C47539"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2420_C47540"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_C47540 = '".$_POST["G2420_C47540"]."'";
                $LsqlI .= $separador."G2420_C47540";
                $LsqlV .= $separador."'".$_POST["G2420_C47540"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2420_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2420_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2420_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2420_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2420_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2420_Usuario , G2420_FechaInsercion, G2420_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2420_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2420 WHERE G2420_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
