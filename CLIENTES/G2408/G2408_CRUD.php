<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55279")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55279");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2408_ConsInte__b, G2408_FechaInsercion , G2408_Usuario ,  G2408_CodigoMiembro  , G2408_PoblacionOrigen , G2408_EstadoDiligenciamiento ,  G2408_IdLlamada , G2408_C47362 as principal ,G2408_C47361,G2408_C47362,G2408_C47363,G2408_C47364,G2408_C47365,G2408_C47366,G2408_C47367,G2408_C47368,G2408_C55278,G2408_C55279,G2408_C47350,G2408_C47351,G2408_C47352,G2408_C47353,G2408_C47354,G2408_C47355,G2408_C47356,G2408_C47357,G2408_C47358 FROM '.$BaseDatos.'.G2408 WHERE G2408_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2408_C47361'] = $key->G2408_C47361;

                $datos[$i]['G2408_C47362'] = $key->G2408_C47362;

                $datos[$i]['G2408_C47363'] = $key->G2408_C47363;

                $datos[$i]['G2408_C47364'] = $key->G2408_C47364;

                $datos[$i]['G2408_C47365'] = $key->G2408_C47365;

                $datos[$i]['G2408_C47366'] = $key->G2408_C47366;

                $datos[$i]['G2408_C47367'] = $key->G2408_C47367;

                $datos[$i]['G2408_C47368'] = $key->G2408_C47368;

                $datos[$i]['G2408_C55278'] = $key->G2408_C55278;

                $datos[$i]['G2408_C55279'] = $key->G2408_C55279;

                $datos[$i]['G2408_C47350'] = $key->G2408_C47350;

                $datos[$i]['G2408_C47351'] = $key->G2408_C47351;

                $datos[$i]['G2408_C47352'] = explode(' ', $key->G2408_C47352)[0];
  
                $hora = '';
                if(!is_null($key->G2408_C47353)){
                    $hora = explode(' ', $key->G2408_C47353)[1];
                }

                $datos[$i]['G2408_C47353'] = $hora;

                $datos[$i]['G2408_C47354'] = $key->G2408_C47354;

                $datos[$i]['G2408_C47355'] = $key->G2408_C47355;

                $datos[$i]['G2408_C47356'] = $key->G2408_C47356;

                $datos[$i]['G2408_C47357'] = $key->G2408_C47357;

                $datos[$i]['G2408_C47358'] = $key->G2408_C47358;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2408";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2408_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2408_ConsInte__b as id,  G2408_C47362 as camp1 , G2408_C55279 as camp2 
                     FROM ".$BaseDatos.".G2408  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2408_ConsInte__b as id,  G2408_C47362 as camp1 , G2408_C55279 as camp2  
                    FROM ".$BaseDatos.".G2408  JOIN ".$BaseDatos.".G2408_M".$_POST['muestra']." ON G2408_ConsInte__b = G2408_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2408_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2408_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2408_C47362 LIKE '%".$B."%' OR G2408_C55279 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2408_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2408");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2408_ConsInte__b, G2408_FechaInsercion , G2408_Usuario ,  G2408_CodigoMiembro  , G2408_PoblacionOrigen , G2408_EstadoDiligenciamiento ,  G2408_IdLlamada , G2408_C47362 as principal ,G2408_C47361,G2408_C47362,G2408_C47363,G2408_C47364,G2408_C47365, a.LISOPC_Nombre____b as G2408_C47366, b.LISOPC_Nombre____b as G2408_C47367,G2408_C47368,G2408_C55278,G2408_C55279, c.LISOPC_Nombre____b as G2408_C47350, d.LISOPC_Nombre____b as G2408_C47351,G2408_C47352,G2408_C47353,G2408_C47354,G2408_C47355,G2408_C47356,G2408_C47357,G2408_C47358 FROM '.$BaseDatos.'.G2408 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2408_C47366 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2408_C47367 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2408_C47350 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2408_C47351';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2408_C47353)){
                    $hora_a = explode(' ', $fila->G2408_C47353)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2408_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2408_ConsInte__b , ($fila->G2408_C47361) , ($fila->G2408_C47362) , ($fila->G2408_C47363) , ($fila->G2408_C47364) , ($fila->G2408_C47365) , ($fila->G2408_C47366) , ($fila->G2408_C47367) , ($fila->G2408_C47368) , ($fila->G2408_C55278) , ($fila->G2408_C55279) , ($fila->G2408_C47350) , ($fila->G2408_C47351) , explode(' ', $fila->G2408_C47352)[0] , $hora_a , ($fila->G2408_C47354) , ($fila->G2408_C47355) , ($fila->G2408_C47356) , ($fila->G2408_C47357) , ($fila->G2408_C47358) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2408 WHERE G2408_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2408";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2408_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2408_ConsInte__b as id,  G2408_C47362 as camp1 , G2408_C55279 as camp2  FROM '.$BaseDatos.'.G2408 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2408_ConsInte__b as id,  G2408_C47362 as camp1 , G2408_C55279 as camp2  
                    FROM ".$BaseDatos.".G2408  JOIN ".$BaseDatos.".G2408_M".$_POST['muestra']." ON G2408_ConsInte__b = G2408_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2408_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2408_C47362 LIKE "%'.$B.'%" OR G2408_C55279 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2408_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2408 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2408(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2408_C47361"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47361 = '".$_POST["G2408_C47361"]."'";
                $LsqlI .= $separador."G2408_C47361";
                $LsqlV .= $separador."'".$_POST["G2408_C47361"]."'";
                $validar = 1;
            }
             
  
            $G2408_C47362 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2408_C47362"])){
                if($_POST["G2408_C47362"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2408_C47362 = $_POST["G2408_C47362"];
                    $LsqlU .= $separador." G2408_C47362 = ".$G2408_C47362."";
                    $LsqlI .= $separador." G2408_C47362";
                    $LsqlV .= $separador.$G2408_C47362;
                    $validar = 1;
                }
            }
  
            $G2408_C47363 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2408_C47363"])){
                if($_POST["G2408_C47363"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2408_C47363 = $_POST["G2408_C47363"];
                    $LsqlU .= $separador." G2408_C47363 = ".$G2408_C47363."";
                    $LsqlI .= $separador." G2408_C47363";
                    $LsqlV .= $separador.$G2408_C47363;
                    $validar = 1;
                }
            }
  
            $G2408_C47364 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2408_C47364"])){
                if($_POST["G2408_C47364"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2408_C47364 = $_POST["G2408_C47364"];
                    $LsqlU .= $separador." G2408_C47364 = ".$G2408_C47364."";
                    $LsqlI .= $separador." G2408_C47364";
                    $LsqlV .= $separador.$G2408_C47364;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2408_C47365"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47365 = '".$_POST["G2408_C47365"]."'";
                $LsqlI .= $separador."G2408_C47365";
                $LsqlV .= $separador."'".$_POST["G2408_C47365"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C47366"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47366 = '".$_POST["G2408_C47366"]."'";
                $LsqlI .= $separador."G2408_C47366";
                $LsqlV .= $separador."'".$_POST["G2408_C47366"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C47367"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47367 = '".$_POST["G2408_C47367"]."'";
                $LsqlI .= $separador."G2408_C47367";
                $LsqlV .= $separador."'".$_POST["G2408_C47367"]."'";
                $validar = 1;
            }
             
  
            $G2408_C47368 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2408_C47368"])){
                if($_POST["G2408_C47368"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2408_C47368 = $_POST["G2408_C47368"];
                    $LsqlU .= $separador." G2408_C47368 = ".$G2408_C47368."";
                    $LsqlI .= $separador." G2408_C47368";
                    $LsqlV .= $separador.$G2408_C47368;
                    $validar = 1;
                }
            }
  
            $G2408_C55278 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2408_C55278"])){
                if($_POST["G2408_C55278"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2408_C55278 = $_POST["G2408_C55278"];
                    $LsqlU .= $separador." G2408_C55278 = ".$G2408_C55278."";
                    $LsqlI .= $separador." G2408_C55278";
                    $LsqlV .= $separador.$G2408_C55278;
                    $validar = 1;
                }
            }
  
            $G2408_C55279 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2408_C55279"])){
                if($_POST["G2408_C55279"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2408_C55279 = $_POST["G2408_C55279"];
                    $LsqlU .= $separador." G2408_C55279 = ".$G2408_C55279."";
                    $LsqlI .= $separador." G2408_C55279";
                    $LsqlV .= $separador.$G2408_C55279;
                    $validar = 1;
                }
            }
 
            $G2408_C47350 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2408_C47350 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2408_C47350 = ".$G2408_C47350;
                    $LsqlI .= $separador." G2408_C47350";
                    $LsqlV .= $separador.$G2408_C47350;
                    $validar = 1;

                    
                }
            }
 
            $G2408_C47351 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2408_C47351 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2408_C47351 = ".$G2408_C47351;
                    $LsqlI .= $separador." G2408_C47351";
                    $LsqlV .= $separador.$G2408_C47351;
                    $validar = 1;
                }
            }
 
            $G2408_C47352 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2408_C47352 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2408_C47352 = ".$G2408_C47352;
                    $LsqlI .= $separador." G2408_C47352";
                    $LsqlV .= $separador.$G2408_C47352;
                    $validar = 1;
                }
            }
 
            $G2408_C47353 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2408_C47353 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2408_C47353 = ".$G2408_C47353;
                    $LsqlI .= $separador." G2408_C47353";
                    $LsqlV .= $separador.$G2408_C47353;
                    $validar = 1;
                }
            }
 
            $G2408_C47354 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2408_C47354 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2408_C47354 = ".$G2408_C47354;
                    $LsqlI .= $separador." G2408_C47354";
                    $LsqlV .= $separador.$G2408_C47354;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2408_C47355"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47355 = '".$_POST["G2408_C47355"]."'";
                $LsqlI .= $separador."G2408_C47355";
                $LsqlV .= $separador."'".$_POST["G2408_C47355"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C47356"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47356 = '".$_POST["G2408_C47356"]."'";
                $LsqlI .= $separador."G2408_C47356";
                $LsqlV .= $separador."'".$_POST["G2408_C47356"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C47357"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47357 = '".$_POST["G2408_C47357"]."'";
                $LsqlI .= $separador."G2408_C47357";
                $LsqlV .= $separador."'".$_POST["G2408_C47357"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C47358"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47358 = '".$_POST["G2408_C47358"]."'";
                $LsqlI .= $separador."G2408_C47358";
                $LsqlV .= $separador."'".$_POST["G2408_C47358"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C47360"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C47360 = '".$_POST["G2408_C47360"]."'";
                $LsqlI .= $separador."G2408_C47360";
                $LsqlV .= $separador."'".$_POST["G2408_C47360"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2408_C55940"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_C55940 = '".$_POST["G2408_C55940"]."'";
                $LsqlI .= $separador."G2408_C55940";
                $LsqlV .= $separador."'".$_POST["G2408_C55940"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2408_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2408_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2408_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2408_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2408_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2408_Usuario , G2408_FechaInsercion, G2408_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2408_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2408 WHERE G2408_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
