<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55928")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55928");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if (isset($_GET["cargarHistorico"])) {

            $intIdRegistro_t = $_GET["intIdRegistro_t"];

            $strSQLGestiones_t = "SELECT G2786_ConsInte__b AS id, dyalogo_general.fn_item_lisopc(G2786_C55220) AS gestion, G2786_C55224 AS comentario, (CASE WHEN numero_telefonico IS NULL THEN CONCAT('A1477',G2786_C55235) ELSE numero_telefonico END) AS telefono, G2786_IdLlamada AS id_llamada, G2786_C55226 AS fecha, dyalogo_general.fn_nombre_USUARI(G2786_Usuario) AS agente FROM ".$BaseDatos.".G2786 LEFT JOIN dyalogo_telefonia.dy_llamadas_espejo ON G2786_IdLlamada = unique_id WHERE G2786_CodigoMiembro = ".$intIdRegistro_t." ORDER BY G2786_ConsInte__b DESC";

            if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
                header("Content-type: application/xhtml+xml;charset=utf-8"); 
            } else { 
                header("Content-type: text/xml;charset=utf-8"); 
            } 

            $et = ">"; 
            echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
            echo "<rows>"; // be sure to put text data in CDATA
            $result = $mysqli->query($strSQLGestiones_t);
            while( $fila = $result->fetch_object() ) {
                echo "<row asin='".$fila->id."'>"; 
                echo "<cell>". ($fila->id)."</cell>"; 

                    $fila->comentario = str_replace("<", "-", $fila->comentario);
                    $fila->comentario = str_replace(">", "-", $fila->comentario);

                    echo "<cell>". ($fila->gestion)."</cell>";
                    echo "<cell>". ($fila->comentario)."</cell>";
                    echo "<cell>". ($fila->telefono)."</cell>";
                    echo "<cell>". ($fila->id_llamada)."</cell>";
                    echo "<cell>". ($fila->fecha)."</cell>";
                    echo "<cell>". ($fila->agente)."</cell>";

                echo "</row>"; 
            } 
            echo "</rows>"; 
        }

      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2786_ConsInte__b, G2786_FechaInsercion , G2786_Usuario ,  G2786_CodigoMiembro  , G2786_PoblacionOrigen , G2786_EstadoDiligenciamiento ,  G2786_IdLlamada , G2786_C55232 as principal ,G2786_C55231,G2786_C55232,G2786_C55233,G2786_C55234,G2786_C55235,G2786_C55236,G2786_C55237,G2786_C55238,G2786_C55239,G2786_C55240,G2786_C55241,G2786_C55242,G2786_C55243,G2786_C55244,G2786_C55245,G2786_C55220,G2786_C55221,G2786_C55222,G2786_C55223,G2786_C55224,G2786_C55225,G2786_C55226,G2786_C55227,G2786_C55228,G2786_C55928,G2786_C55249,G2786_C55250,G2786_C55251,G2786_C55252,G2786_C55253,G2786_C55254,G2786_C55255,G2786_C55256 FROM '.$BaseDatos.'.G2786 WHERE G2786_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2786_C55231'] = $key->G2786_C55231;

                $datos[$i]['G2786_C55232'] = $key->G2786_C55232;

                $datos[$i]['G2786_C55233'] = explode(' ', $key->G2786_C55233)[0];

                $datos[$i]['G2786_C55234'] = $key->G2786_C55234;

                $datos[$i]['G2786_C55235'] = $key->G2786_C55235;

                $datos[$i]['G2786_C55236'] = $key->G2786_C55236;

                $datos[$i]['G2786_C55237'] = $key->G2786_C55237;

                $datos[$i]['G2786_C55238'] = $key->G2786_C55238;

                $datos[$i]['G2786_C55239'] = $key->G2786_C55239;

                $datos[$i]['G2786_C55240'] = $key->G2786_C55240;

                $datos[$i]['G2786_C55241'] = $key->G2786_C55241;

                $datos[$i]['G2786_C55242'] = $key->G2786_C55242;

                $datos[$i]['G2786_C55243'] = $key->G2786_C55243;

                $datos[$i]['G2786_C55244'] = $key->G2786_C55244;

                $datos[$i]['G2786_C55245'] = $key->G2786_C55245;

                $datos[$i]['G2786_C55220'] = $key->G2786_C55220;

                $datos[$i]['G2786_C55221'] = $key->G2786_C55221;

                $datos[$i]['G2786_C55222'] = explode(' ', $key->G2786_C55222)[0];
  
                $hora = '';
                if(!is_null($key->G2786_C55223)){
                    $hora = explode(' ', $key->G2786_C55223)[1];
                }

                $datos[$i]['G2786_C55223'] = $hora;

                $datos[$i]['G2786_C55224'] = $key->G2786_C55224;

                $datos[$i]['G2786_C55225'] = $key->G2786_C55225;

                $datos[$i]['G2786_C55226'] = $key->G2786_C55226;

                $datos[$i]['G2786_C55227'] = $key->G2786_C55227;

                $datos[$i]['G2786_C55228'] = $key->G2786_C55228;

                $datos[$i]['G2786_C55928'] = $key->G2786_C55928;

                $datos[$i]['G2786_C55249'] = $key->G2786_C55249;

                $datos[$i]['G2786_C55250'] = $key->G2786_C55250;

                $datos[$i]['G2786_C55251'] = $key->G2786_C55251;

                $datos[$i]['G2786_C55252'] = $key->G2786_C55252;

                $datos[$i]['G2786_C55253'] = $key->G2786_C55253;

                $datos[$i]['G2786_C55254'] = $key->G2786_C55254;

                $datos[$i]['G2786_C55255'] = explode(' ', $key->G2786_C55255)[0];

                $datos[$i]['G2786_C55256'] = $key->G2786_C55256;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2786";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2786_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2786_ConsInte__b as id,  G2786_C55231 as camp2 , G2786_C55232 as camp1 
                     FROM ".$BaseDatos.".G2786  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2786_ConsInte__b as id,  G2786_C55231 as camp2 , G2786_C55232 as camp1  
                    FROM ".$BaseDatos.".G2786  JOIN ".$BaseDatos.".G2786_M".$_POST['muestra']." ON G2786_ConsInte__b = G2786_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2786_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2786_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2786_C55231 LIKE '%".$B."%' OR G2786_C55232 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2786_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2786");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2786_ConsInte__b, G2786_FechaInsercion , G2786_Usuario ,  G2786_CodigoMiembro  , G2786_PoblacionOrigen , G2786_EstadoDiligenciamiento ,  G2786_IdLlamada , G2786_C55232 as principal ,G2786_C55231,G2786_C55232,G2786_C55233,G2786_C55234,G2786_C55235,G2786_C55236,G2786_C55237,G2786_C55238,G2786_C55239,G2786_C55240,G2786_C55241,G2786_C55242,G2786_C55243,G2786_C55244,G2786_C55245, a.LISOPC_Nombre____b as G2786_C55220, b.LISOPC_Nombre____b as G2786_C55221,G2786_C55222,G2786_C55223,G2786_C55224,G2786_C55225,G2786_C55226,G2786_C55227,G2786_C55228,G2786_C55928,G2786_C55249,G2786_C55250,G2786_C55251,G2786_C55252, c.LISOPC_Nombre____b as G2786_C55253,G2786_C55254,G2786_C55255, d.LISOPC_Nombre____b as G2786_C55256 FROM '.$BaseDatos.'.G2786 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2786_C55220 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2786_C55221 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2786_C55253 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2786_C55256';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2786_C55223)){
                    $hora_a = explode(' ', $fila->G2786_C55223)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2786_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2786_ConsInte__b , ($fila->G2786_C55231) , ($fila->G2786_C55232) , explode(' ', $fila->G2786_C55233)[0] , ($fila->G2786_C55234) , ($fila->G2786_C55235) , ($fila->G2786_C55236) , ($fila->G2786_C55237) , ($fila->G2786_C55238) , ($fila->G2786_C55239) , ($fila->G2786_C55240) , ($fila->G2786_C55241) , ($fila->G2786_C55242) , ($fila->G2786_C55243) , ($fila->G2786_C55244) , ($fila->G2786_C55245) , ($fila->G2786_C55220) , ($fila->G2786_C55221) , explode(' ', $fila->G2786_C55222)[0] , $hora_a , ($fila->G2786_C55224) , ($fila->G2786_C55225) , ($fila->G2786_C55226) , ($fila->G2786_C55227) , ($fila->G2786_C55228) , ($fila->G2786_C55928) , ($fila->G2786_C55249) , ($fila->G2786_C55250) , ($fila->G2786_C55251) , ($fila->G2786_C55252) , ($fila->G2786_C55253) , ($fila->G2786_C55254) , explode(' ', $fila->G2786_C55255)[0] , ($fila->G2786_C55256) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2786 WHERE G2786_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2786";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2786_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2786_ConsInte__b as id,  G2786_C55231 as camp2 , G2786_C55232 as camp1  FROM '.$BaseDatos.'.G2786 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2786_ConsInte__b as id,  G2786_C55231 as camp2 , G2786_C55232 as camp1  
                    FROM ".$BaseDatos.".G2786  JOIN ".$BaseDatos.".G2786_M".$_POST['muestra']." ON G2786_ConsInte__b = G2786_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2786_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2786_C55231 LIKE "%'.$B.'%" OR G2786_C55232 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2786_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2786 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2786(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2786_C55231"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55231 = '".$_POST["G2786_C55231"]."'";
                $LsqlI .= $separador."G2786_C55231";
                $LsqlV .= $separador."'".$_POST["G2786_C55231"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55232"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55232 = '".$_POST["G2786_C55232"]."'";
                $LsqlI .= $separador."G2786_C55232";
                $LsqlV .= $separador."'".$_POST["G2786_C55232"]."'";
                $validar = 1;
            }
             
 
            $G2786_C55233 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2786_C55233"])){    
                if($_POST["G2786_C55233"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2786_C55233"]);
                    if(count($tieneHora) > 1){
                        $G2786_C55233 = "'".$_POST["G2786_C55233"]."'";
                    }else{
                        $G2786_C55233 = "'".str_replace(' ', '',$_POST["G2786_C55233"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2786_C55233 = ".$G2786_C55233;
                    $LsqlI .= $separador." G2786_C55233";
                    $LsqlV .= $separador.$G2786_C55233;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2786_C55234"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55234 = '".$_POST["G2786_C55234"]."'";
                $LsqlI .= $separador."G2786_C55234";
                $LsqlV .= $separador."'".$_POST["G2786_C55234"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55235 = '".$_POST["G2786_C55235"]."'";
                $LsqlI .= $separador."G2786_C55235";
                $LsqlV .= $separador."'".$_POST["G2786_C55235"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55236"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55236 = '".$_POST["G2786_C55236"]."'";
                $LsqlI .= $separador."G2786_C55236";
                $LsqlV .= $separador."'".$_POST["G2786_C55236"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55237"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55237 = '".$_POST["G2786_C55237"]."'";
                $LsqlI .= $separador."G2786_C55237";
                $LsqlV .= $separador."'".$_POST["G2786_C55237"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55238"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55238 = '".$_POST["G2786_C55238"]."'";
                $LsqlI .= $separador."G2786_C55238";
                $LsqlV .= $separador."'".$_POST["G2786_C55238"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55239"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55239 = '".$_POST["G2786_C55239"]."'";
                $LsqlI .= $separador."G2786_C55239";
                $LsqlV .= $separador."'".$_POST["G2786_C55239"]."'";
                $validar = 1;
            }
             
  
            $G2786_C55240 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2786_C55240"])){
                if($_POST["G2786_C55240"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2786_C55240 = $_POST["G2786_C55240"];
                    $LsqlU .= $separador." G2786_C55240 = ".$G2786_C55240."";
                    $LsqlI .= $separador." G2786_C55240";
                    $LsqlV .= $separador.$G2786_C55240;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2786_C55241"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55241 = '".$_POST["G2786_C55241"]."'";
                $LsqlI .= $separador."G2786_C55241";
                $LsqlV .= $separador."'".$_POST["G2786_C55241"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55242"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55242 = '".$_POST["G2786_C55242"]."'";
                $LsqlI .= $separador."G2786_C55242";
                $LsqlV .= $separador."'".$_POST["G2786_C55242"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55243"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55243 = '".$_POST["G2786_C55243"]."'";
                $LsqlI .= $separador."G2786_C55243";
                $LsqlV .= $separador."'".$_POST["G2786_C55243"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55244 = '".$_POST["G2786_C55244"]."'";
                $LsqlI .= $separador."G2786_C55244";
                $LsqlV .= $separador."'".$_POST["G2786_C55244"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55245"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55245 = '".$_POST["G2786_C55245"]."'";
                $LsqlI .= $separador."G2786_C55245";
                $LsqlV .= $separador."'".$_POST["G2786_C55245"]."'";
                $validar = 1;
            }
             
 
            $G2786_C55220 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2786_C55220 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2786_C55220 = ".$G2786_C55220;
                    $LsqlI .= $separador." G2786_C55220";
                    $LsqlV .= $separador.$G2786_C55220;
                    $validar = 1;

                    
                }
            }
 
            $G2786_C55221 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2786_C55221 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2786_C55221 = ".$G2786_C55221;
                    $LsqlI .= $separador." G2786_C55221";
                    $LsqlV .= $separador.$G2786_C55221;
                    $validar = 1;
                }
            }
 
            $G2786_C55222 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2786_C55222 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2786_C55222 = ".$G2786_C55222;
                    $LsqlI .= $separador." G2786_C55222";
                    $LsqlV .= $separador.$G2786_C55222;
                    $validar = 1;
                }
            }
 
            $G2786_C55223 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2786_C55223 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2786_C55223 = ".$G2786_C55223;
                    $LsqlI .= $separador." G2786_C55223";
                    $LsqlV .= $separador.$G2786_C55223;
                    $validar = 1;
                }
            }
 
            $G2786_C55224 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2786_C55224 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2786_C55224 = ".$G2786_C55224;
                    $LsqlI .= $separador." G2786_C55224";
                    $LsqlV .= $separador.$G2786_C55224;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2786_C55225"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55225 = '".$_POST["G2786_C55225"]."'";
                $LsqlI .= $separador."G2786_C55225";
                $LsqlV .= $separador."'".$_POST["G2786_C55225"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55226"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55226 = '".$_POST["G2786_C55226"]."'";
                $LsqlI .= $separador."G2786_C55226";
                $LsqlV .= $separador."'".$_POST["G2786_C55226"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55227"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55227 = '".$_POST["G2786_C55227"]."'";
                $LsqlI .= $separador."G2786_C55227";
                $LsqlV .= $separador."'".$_POST["G2786_C55227"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55228"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55228 = '".$_POST["G2786_C55228"]."'";
                $LsqlI .= $separador."G2786_C55228";
                $LsqlV .= $separador."'".$_POST["G2786_C55228"]."'";
                $validar = 1;
            }
             
  
            $G2786_C55928 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2786_C55928"])){
                if($_POST["G2786_C55928"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2786_C55928 = $_POST["G2786_C55928"];
                    $LsqlU .= $separador." G2786_C55928 = ".$G2786_C55928."";
                    $LsqlI .= $separador." G2786_C55928";
                    $LsqlV .= $separador.$G2786_C55928;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2786_C55249"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55249 = '".$_POST["G2786_C55249"]."'";
                $LsqlI .= $separador."G2786_C55249";
                $LsqlV .= $separador."'".$_POST["G2786_C55249"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55250"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55250 = '".$_POST["G2786_C55250"]."'";
                $LsqlI .= $separador."G2786_C55250";
                $LsqlV .= $separador."'".$_POST["G2786_C55250"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55251"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55251 = '".$_POST["G2786_C55251"]."'";
                $LsqlI .= $separador."G2786_C55251";
                $LsqlV .= $separador."'".$_POST["G2786_C55251"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55252"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55252 = '".$_POST["G2786_C55252"]."'";
                $LsqlI .= $separador."G2786_C55252";
                $LsqlV .= $separador."'".$_POST["G2786_C55252"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55253"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55253 = '".$_POST["G2786_C55253"]."'";
                $LsqlI .= $separador."G2786_C55253";
                $LsqlV .= $separador."'".$_POST["G2786_C55253"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2786_C55254"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55254 = '".$_POST["G2786_C55254"]."'";
                $LsqlI .= $separador."G2786_C55254";
                $LsqlV .= $separador."'".$_POST["G2786_C55254"]."'";
                $validar = 1;
            }
             
 
            $G2786_C55255 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2786_C55255"])){    
                if($_POST["G2786_C55255"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2786_C55255"]);
                    if(count($tieneHora) > 1){
                        $G2786_C55255 = "'".$_POST["G2786_C55255"]."'";
                    }else{
                        $G2786_C55255 = "'".str_replace(' ', '',$_POST["G2786_C55255"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2786_C55255 = ".$G2786_C55255;
                    $LsqlI .= $separador." G2786_C55255";
                    $LsqlV .= $separador.$G2786_C55255;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2786_C55256"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_C55256 = '".$_POST["G2786_C55256"]."'";
                $LsqlI .= $separador."G2786_C55256";
                $LsqlV .= $separador."'".$_POST["G2786_C55256"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2786_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2786_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2786_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2786_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2786_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2786_Usuario , G2786_FechaInsercion, G2786_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2786_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2786 WHERE G2786_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2810_ConsInte__b, G2810_C55730, G2810_C55731, G2810_C55732 FROM ".$BaseDatos.".G2810  ";

        $SQL .= " WHERE G2810_C55730 = '".$numero."'"; 

        $SQL .= " ORDER BY G2810_C55730";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2810_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2810_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2810_C55730)."</cell>";

                if($fila->G2810_C55731 != ''){
                    echo "<cell>". explode(' ', $fila->G2810_C55731)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell><![CDATA[". ($fila->G2810_C55732)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2810 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2810(";
            $LsqlV = " VALUES ("; 

                $G2810_C55731 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2810_C55731"])){    
                    if($_POST["G2810_C55731"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2810_C55731 = "'".str_replace(' ', '',$_POST["G2810_C55731"])." 00:00:00'";
                        $LsqlU .= $separador." G2810_C55731 = ".$G2810_C55731;
                        $LsqlI .= $separador." G2810_C55731";
                        $LsqlV .= $separador.$G2810_C55731;
                        $validar = 1;
                    }
                }
  

                if(isset($_POST["G2810_C55732"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2810_C55732 = '".$_POST["G2810_C55732"]."'";
                    $LsqlI .= $separador."G2810_C55732";
                    $LsqlV .= $separador."'".$_POST["G2810_C55732"]."'";
                    $validar = 1;
                }
                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2810_C55730 = $numero;
                    $LsqlU .= ", G2810_C55730 = ".$G2810_C55730."";
                    $LsqlI .= ", G2810_C55730";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2810_Usuario ,  G2810_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2810_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2810 WHERE  G2810_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
