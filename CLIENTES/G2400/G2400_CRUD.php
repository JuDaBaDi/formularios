<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55271")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55271");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2400_ConsInte__b, G2400_FechaInsercion , G2400_Usuario ,  G2400_CodigoMiembro  , G2400_PoblacionOrigen , G2400_EstadoDiligenciamiento ,  G2400_IdLlamada , G2400_C47240 as principal ,G2400_C47239,G2400_C47240,G2400_C47241,G2400_C47242,G2400_C47243,G2400_C47244,G2400_C47245,G2400_C47246,G2400_C55270,G2400_C55271,G2400_C47228,G2400_C47229,G2400_C47230,G2400_C47231,G2400_C47232,G2400_C47233,G2400_C47234,G2400_C47235,G2400_C47236 FROM '.$BaseDatos.'.G2400 WHERE G2400_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2400_C47239'] = $key->G2400_C47239;

                $datos[$i]['G2400_C47240'] = $key->G2400_C47240;

                $datos[$i]['G2400_C47241'] = $key->G2400_C47241;

                $datos[$i]['G2400_C47242'] = $key->G2400_C47242;

                $datos[$i]['G2400_C47243'] = $key->G2400_C47243;

                $datos[$i]['G2400_C47244'] = $key->G2400_C47244;

                $datos[$i]['G2400_C47245'] = $key->G2400_C47245;

                $datos[$i]['G2400_C47246'] = $key->G2400_C47246;

                $datos[$i]['G2400_C55270'] = $key->G2400_C55270;

                $datos[$i]['G2400_C55271'] = $key->G2400_C55271;

                $datos[$i]['G2400_C47228'] = $key->G2400_C47228;

                $datos[$i]['G2400_C47229'] = $key->G2400_C47229;

                $datos[$i]['G2400_C47230'] = explode(' ', $key->G2400_C47230)[0];
  
                $hora = '';
                if(!is_null($key->G2400_C47231)){
                    $hora = explode(' ', $key->G2400_C47231)[1];
                }

                $datos[$i]['G2400_C47231'] = $hora;

                $datos[$i]['G2400_C47232'] = $key->G2400_C47232;

                $datos[$i]['G2400_C47233'] = $key->G2400_C47233;

                $datos[$i]['G2400_C47234'] = $key->G2400_C47234;

                $datos[$i]['G2400_C47235'] = $key->G2400_C47235;

                $datos[$i]['G2400_C47236'] = $key->G2400_C47236;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2400";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2400_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2400_ConsInte__b as id,  G2400_C47240 as camp1 , G2400_C55271 as camp2 
                     FROM ".$BaseDatos.".G2400  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2400_ConsInte__b as id,  G2400_C47240 as camp1 , G2400_C55271 as camp2  
                    FROM ".$BaseDatos.".G2400  JOIN ".$BaseDatos.".G2400_M".$_POST['muestra']." ON G2400_ConsInte__b = G2400_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2400_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2400_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2400_C47240 LIKE '%".$B."%' OR G2400_C55271 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2400_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2400");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2400_ConsInte__b, G2400_FechaInsercion , G2400_Usuario ,  G2400_CodigoMiembro  , G2400_PoblacionOrigen , G2400_EstadoDiligenciamiento ,  G2400_IdLlamada , G2400_C47240 as principal ,G2400_C47239,G2400_C47240,G2400_C47241,G2400_C47242,G2400_C47243, a.LISOPC_Nombre____b as G2400_C47244, b.LISOPC_Nombre____b as G2400_C47245,G2400_C47246,G2400_C55270,G2400_C55271, c.LISOPC_Nombre____b as G2400_C47228, d.LISOPC_Nombre____b as G2400_C47229,G2400_C47230,G2400_C47231,G2400_C47232,G2400_C47233,G2400_C47234,G2400_C47235,G2400_C47236 FROM '.$BaseDatos.'.G2400 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2400_C47244 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2400_C47245 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2400_C47228 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2400_C47229';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2400_C47231)){
                    $hora_a = explode(' ', $fila->G2400_C47231)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2400_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2400_ConsInte__b , ($fila->G2400_C47239) , ($fila->G2400_C47240) , ($fila->G2400_C47241) , ($fila->G2400_C47242) , ($fila->G2400_C47243) , ($fila->G2400_C47244) , ($fila->G2400_C47245) , ($fila->G2400_C47246) , ($fila->G2400_C55270) , ($fila->G2400_C55271) , ($fila->G2400_C47228) , ($fila->G2400_C47229) , explode(' ', $fila->G2400_C47230)[0] , $hora_a , ($fila->G2400_C47232) , ($fila->G2400_C47233) , ($fila->G2400_C47234) , ($fila->G2400_C47235) , ($fila->G2400_C47236) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2400 WHERE G2400_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2400";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2400_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2400_ConsInte__b as id,  G2400_C47240 as camp1 , G2400_C55271 as camp2  FROM '.$BaseDatos.'.G2400 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2400_ConsInte__b as id,  G2400_C47240 as camp1 , G2400_C55271 as camp2  
                    FROM ".$BaseDatos.".G2400  JOIN ".$BaseDatos.".G2400_M".$_POST['muestra']." ON G2400_ConsInte__b = G2400_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2400_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2400_C47240 LIKE "%'.$B.'%" OR G2400_C55271 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2400_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2400 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2400(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2400_C47239"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47239 = '".$_POST["G2400_C47239"]."'";
                $LsqlI .= $separador."G2400_C47239";
                $LsqlV .= $separador."'".$_POST["G2400_C47239"]."'";
                $validar = 1;
            }
             
  
            $G2400_C47240 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2400_C47240"])){
                if($_POST["G2400_C47240"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2400_C47240 = $_POST["G2400_C47240"];
                    $LsqlU .= $separador." G2400_C47240 = ".$G2400_C47240."";
                    $LsqlI .= $separador." G2400_C47240";
                    $LsqlV .= $separador.$G2400_C47240;
                    $validar = 1;
                }
            }
  
            $G2400_C47241 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2400_C47241"])){
                if($_POST["G2400_C47241"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2400_C47241 = $_POST["G2400_C47241"];
                    $LsqlU .= $separador." G2400_C47241 = ".$G2400_C47241."";
                    $LsqlI .= $separador." G2400_C47241";
                    $LsqlV .= $separador.$G2400_C47241;
                    $validar = 1;
                }
            }
  
            $G2400_C47242 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2400_C47242"])){
                if($_POST["G2400_C47242"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2400_C47242 = $_POST["G2400_C47242"];
                    $LsqlU .= $separador." G2400_C47242 = ".$G2400_C47242."";
                    $LsqlI .= $separador." G2400_C47242";
                    $LsqlV .= $separador.$G2400_C47242;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2400_C47243"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47243 = '".$_POST["G2400_C47243"]."'";
                $LsqlI .= $separador."G2400_C47243";
                $LsqlV .= $separador."'".$_POST["G2400_C47243"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C47244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47244 = '".$_POST["G2400_C47244"]."'";
                $LsqlI .= $separador."G2400_C47244";
                $LsqlV .= $separador."'".$_POST["G2400_C47244"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C47245"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47245 = '".$_POST["G2400_C47245"]."'";
                $LsqlI .= $separador."G2400_C47245";
                $LsqlV .= $separador."'".$_POST["G2400_C47245"]."'";
                $validar = 1;
            }
             
  
            $G2400_C47246 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2400_C47246"])){
                if($_POST["G2400_C47246"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2400_C47246 = $_POST["G2400_C47246"];
                    $LsqlU .= $separador." G2400_C47246 = ".$G2400_C47246."";
                    $LsqlI .= $separador." G2400_C47246";
                    $LsqlV .= $separador.$G2400_C47246;
                    $validar = 1;
                }
            }
  
            $G2400_C55270 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2400_C55270"])){
                if($_POST["G2400_C55270"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2400_C55270 = $_POST["G2400_C55270"];
                    $LsqlU .= $separador." G2400_C55270 = ".$G2400_C55270."";
                    $LsqlI .= $separador." G2400_C55270";
                    $LsqlV .= $separador.$G2400_C55270;
                    $validar = 1;
                }
            }
  
            $G2400_C55271 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2400_C55271"])){
                if($_POST["G2400_C55271"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2400_C55271 = $_POST["G2400_C55271"];
                    $LsqlU .= $separador." G2400_C55271 = ".$G2400_C55271."";
                    $LsqlI .= $separador." G2400_C55271";
                    $LsqlV .= $separador.$G2400_C55271;
                    $validar = 1;
                }
            }
 
            $G2400_C47228 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2400_C47228 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2400_C47228 = ".$G2400_C47228;
                    $LsqlI .= $separador." G2400_C47228";
                    $LsqlV .= $separador.$G2400_C47228;
                    $validar = 1;

                    
                }
            }
 
            $G2400_C47229 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2400_C47229 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2400_C47229 = ".$G2400_C47229;
                    $LsqlI .= $separador." G2400_C47229";
                    $LsqlV .= $separador.$G2400_C47229;
                    $validar = 1;
                }
            }
 
            $G2400_C47230 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2400_C47230 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2400_C47230 = ".$G2400_C47230;
                    $LsqlI .= $separador." G2400_C47230";
                    $LsqlV .= $separador.$G2400_C47230;
                    $validar = 1;
                }
            }
 
            $G2400_C47231 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2400_C47231 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2400_C47231 = ".$G2400_C47231;
                    $LsqlI .= $separador." G2400_C47231";
                    $LsqlV .= $separador.$G2400_C47231;
                    $validar = 1;
                }
            }
 
            $G2400_C47232 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2400_C47232 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2400_C47232 = ".$G2400_C47232;
                    $LsqlI .= $separador." G2400_C47232";
                    $LsqlV .= $separador.$G2400_C47232;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2400_C47233"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47233 = '".$_POST["G2400_C47233"]."'";
                $LsqlI .= $separador."G2400_C47233";
                $LsqlV .= $separador."'".$_POST["G2400_C47233"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C47234"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47234 = '".$_POST["G2400_C47234"]."'";
                $LsqlI .= $separador."G2400_C47234";
                $LsqlV .= $separador."'".$_POST["G2400_C47234"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C47235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47235 = '".$_POST["G2400_C47235"]."'";
                $LsqlI .= $separador."G2400_C47235";
                $LsqlV .= $separador."'".$_POST["G2400_C47235"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C47236"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47236 = '".$_POST["G2400_C47236"]."'";
                $LsqlI .= $separador."G2400_C47236";
                $LsqlV .= $separador."'".$_POST["G2400_C47236"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C47237"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C47237 = '".$_POST["G2400_C47237"]."'";
                $LsqlI .= $separador."G2400_C47237";
                $LsqlV .= $separador."'".$_POST["G2400_C47237"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2400_C55936"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_C55936 = '".$_POST["G2400_C55936"]."'";
                $LsqlI .= $separador."G2400_C55936";
                $LsqlV .= $separador."'".$_POST["G2400_C55936"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2400_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2400_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2400_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2400_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2400_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2400_Usuario , G2400_FechaInsercion, G2400_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2400_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2400 WHERE G2400_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
