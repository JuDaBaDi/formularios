<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3178_ConsInte__b, G3178_FechaInsercion , G3178_Usuario ,  G3178_CodigoMiembro  , G3178_PoblacionOrigen , G3178_EstadoDiligenciamiento ,  G3178_IdLlamada , G3178_C63849 as principal ,G3178_C63849,G3178_C63850,G3178_C63851,G3178_C63852,G3178_C63853,G3178_C63854,G3178_C63855,G3178_C63856,G3178_C64114,G3178_C63804,G3178_C63805,G3178_C63806 FROM '.$BaseDatos.'.G3178 WHERE G3178_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3178_C63849'] = $key->G3178_C63849;

                $datos[$i]['G3178_C63850'] = $key->G3178_C63850;

                $datos[$i]['G3178_C63851'] = $key->G3178_C63851;

                $datos[$i]['G3178_C63852'] = $key->G3178_C63852;

                $datos[$i]['G3178_C63853'] = $key->G3178_C63853;

                $datos[$i]['G3178_C63854'] = $key->G3178_C63854;

                $datos[$i]['G3178_C63855'] = $key->G3178_C63855;

                $datos[$i]['G3178_C63856'] = $key->G3178_C63856;

                $datos[$i]['G3178_C64114'] = $key->G3178_C64114;

                $datos[$i]['G3178_C63804'] = $key->G3178_C63804;

                $datos[$i]['G3178_C63805'] = $key->G3178_C63805;

                $datos[$i]['G3178_C63806'] = $key->G3178_C63806;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3178";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3178_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3178_ConsInte__b as id,  G3178_C63849 as camp1 , G3178_C63853 as camp2 
                     FROM ".$BaseDatos.".G3178  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3178_ConsInte__b as id,  G3178_C63849 as camp1 , G3178_C63853 as camp2  
                    FROM ".$BaseDatos.".G3178  JOIN ".$BaseDatos.".G3178_M".$_POST['muestra']." ON G3178_ConsInte__b = G3178_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3178_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3178_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3178_C63849 LIKE '%".$B."%' OR G3178_C63853 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3178_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G3178_C63852'])){
                                $Ysql = "SELECT G3163_ConsInte__b as id,  as text FROM ".$BaseDatos.".G3163 WHERE  LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3178_C63852"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G WHERE G_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3178_C63852"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G3178_C63856'])){
                                $Ysql = "SELECT G3159_ConsInte__b as id, G3159_C63073 as text FROM ".$BaseDatos.".G3159 WHERE G3159_C63073 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G3178_C63856"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G3159 WHERE G3159_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G3178_C63856"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3178");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3178_ConsInte__b, G3178_FechaInsercion , G3178_Usuario ,  G3178_CodigoMiembro  , G3178_PoblacionOrigen , G3178_EstadoDiligenciamiento ,  G3178_IdLlamada , G3178_C63849 as principal ,G3178_C63849, a.LISOPC_Nombre____b as G3178_C63850,G3178_C63851,G3178_C63853,G3178_C63854,G3178_C63855, G3159_C63073, b.LISOPC_Nombre____b as G3178_C64114,G3178_C63804,G3178_C63805, c.LISOPC_Nombre____b as G3178_C63806 FROM '.$BaseDatos.'.G3178 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3178_C63850 LEFT JOIN '.$BaseDatos.'.G3163 ON G3163_ConsInte__b  =  G3178_C63852 LEFT JOIN '.$BaseDatos.'.G3159 ON G3159_ConsInte__b  =  G3178_C63856 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3178_C64114 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3178_C63806';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G3178_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3178_ConsInte__b , ($fila->G3178_C63849) , ($fila->G3178_C63850) , ($fila->G3178_C63851) , ($fila->G3178_C63853) , ($fila->G3178_C63854) , ($fila->G3178_C63855) , ($fila->G3159_C63073) , ($fila->G3178_C64114) , ($fila->G3178_C63804) , ($fila->G3178_C63805) , ($fila->G3178_C63806) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3178 WHERE G3178_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3178";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3178_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3178_ConsInte__b as id,  G3178_C63849 as camp1 , G3178_C63853 as camp2  FROM '.$BaseDatos.'.G3178 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3178_ConsInte__b as id,  G3178_C63849 as camp1 , G3178_C63853 as camp2  
                    FROM ".$BaseDatos.".G3178  JOIN ".$BaseDatos.".G3178_M".$_POST['muestra']." ON G3178_ConsInte__b = G3178_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3178_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3178_C63849 LIKE "%'.$B.'%" OR G3178_C63853 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3178_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3178 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3178(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3178_C64112"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C64112 = '".$_POST["G3178_C64112"]."'";
                $LsqlI .= $separador."G3178_C64112";
                $LsqlV .= $separador."'".$_POST["G3178_C64112"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C64113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C64113 = '".$_POST["G3178_C64113"]."'";
                $LsqlI .= $separador."G3178_C64113";
                $LsqlV .= $separador."'".$_POST["G3178_C64113"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63849"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63849 = '".$_POST["G3178_C63849"]."'";
                $LsqlI .= $separador."G3178_C63849";
                $LsqlV .= $separador."'".$_POST["G3178_C63849"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63850"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63850 = '".$_POST["G3178_C63850"]."'";
                $LsqlI .= $separador."G3178_C63850";
                $LsqlV .= $separador."'".$_POST["G3178_C63850"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63851"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63851 = '".$_POST["G3178_C63851"]."'";
                $LsqlI .= $separador."G3178_C63851";
                $LsqlV .= $separador."'".$_POST["G3178_C63851"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63852"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63852 = '".$_POST["G3178_C63852"]."'";
                $LsqlI .= $separador."G3178_C63852";
                $LsqlV .= $separador."'".$_POST["G3178_C63852"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63853"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63853 = '".$_POST["G3178_C63853"]."'";
                $LsqlI .= $separador."G3178_C63853";
                $LsqlV .= $separador."'".$_POST["G3178_C63853"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63854"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63854 = '".$_POST["G3178_C63854"]."'";
                $LsqlI .= $separador."G3178_C63854";
                $LsqlV .= $separador."'".$_POST["G3178_C63854"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63855"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63855 = '".$_POST["G3178_C63855"]."'";
                $LsqlI .= $separador."G3178_C63855";
                $LsqlV .= $separador."'".$_POST["G3178_C63855"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63856"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63856 = '".$_POST["G3178_C63856"]."'";
                $LsqlI .= $separador."G3178_C63856";
                $LsqlV .= $separador."'".$_POST["G3178_C63856"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C64114"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C64114 = '".$_POST["G3178_C64114"]."'";
                $LsqlI .= $separador."G3178_C64114";
                $LsqlV .= $separador."'".$_POST["G3178_C64114"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63804"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63804 = '".$_POST["G3178_C63804"]."'";
                $LsqlI .= $separador."G3178_C63804";
                $LsqlV .= $separador."'".$_POST["G3178_C63804"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63805"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63805 = '".$_POST["G3178_C63805"]."'";
                $LsqlI .= $separador."G3178_C63805";
                $LsqlV .= $separador."'".$_POST["G3178_C63805"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3178_C63806"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_C63806 = '".$_POST["G3178_C63806"]."'";
                $LsqlI .= $separador."G3178_C63806";
                $LsqlV .= $separador."'".$_POST["G3178_C63806"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3178_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3178_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3178_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3178_Usuario , G3178_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3178_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3178 WHERE G3178_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3178 SET G3178_UltiGest__b =-14, G3178_GesMasImp_b =-14, G3178_TipoReintentoUG_b =0, G3178_TipoReintentoGMI_b =0, G3178_ClasificacionUG_b =3, G3178_ClasificacionGMI_b =3, G3178_EstadoUG_b =-14, G3178_EstadoGMI_b =-14, G3178_CantidadIntentos =0, G3178_CantidadIntentosGMI_b =0 WHERE G3178_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
