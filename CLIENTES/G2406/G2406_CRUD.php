<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55275")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55275");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2406_ConsInte__b, G2406_FechaInsercion , G2406_Usuario ,  G2406_CodigoMiembro  , G2406_PoblacionOrigen , G2406_EstadoDiligenciamiento ,  G2406_IdLlamada , G2406_C47332 as principal ,G2406_C47331,G2406_C47332,G2406_C47333,G2406_C47334,G2406_C47335,G2406_C47336,G2406_C47337,G2406_C47338,G2406_C55274,G2406_C55275,G2406_C47320,G2406_C47321,G2406_C47322,G2406_C47323,G2406_C47324,G2406_C47325,G2406_C47326,G2406_C47327,G2406_C47328,G2406_C55938 FROM '.$BaseDatos.'.G2406 WHERE G2406_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2406_C47331'] = $key->G2406_C47331;

                $datos[$i]['G2406_C47332'] = $key->G2406_C47332;

                $datos[$i]['G2406_C47333'] = $key->G2406_C47333;

                $datos[$i]['G2406_C47334'] = $key->G2406_C47334;

                $datos[$i]['G2406_C47335'] = $key->G2406_C47335;

                $datos[$i]['G2406_C47336'] = $key->G2406_C47336;

                $datos[$i]['G2406_C47337'] = $key->G2406_C47337;

                $datos[$i]['G2406_C47338'] = $key->G2406_C47338;

                $datos[$i]['G2406_C55274'] = $key->G2406_C55274;

                $datos[$i]['G2406_C55275'] = $key->G2406_C55275;

                $datos[$i]['G2406_C47320'] = $key->G2406_C47320;

                $datos[$i]['G2406_C47321'] = $key->G2406_C47321;

                $datos[$i]['G2406_C47322'] = explode(' ', $key->G2406_C47322)[0];
  
                $hora = '';
                if(!is_null($key->G2406_C47323)){
                    $hora = explode(' ', $key->G2406_C47323)[1];
                }

                $datos[$i]['G2406_C47323'] = $hora;

                $datos[$i]['G2406_C47324'] = $key->G2406_C47324;

                $datos[$i]['G2406_C47325'] = $key->G2406_C47325;

                $datos[$i]['G2406_C47326'] = $key->G2406_C47326;

                $datos[$i]['G2406_C47327'] = $key->G2406_C47327;

                $datos[$i]['G2406_C47328'] = $key->G2406_C47328;

                $datos[$i]['G2406_C55938'] = $key->G2406_C55938;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2406";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2406_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2406_ConsInte__b as id,  G2406_C47332 as camp1 , G2406_C55275 as camp2 
                     FROM ".$BaseDatos.".G2406  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2406_ConsInte__b as id,  G2406_C47332 as camp1 , G2406_C55275 as camp2  
                    FROM ".$BaseDatos.".G2406  JOIN ".$BaseDatos.".G2406_M".$_POST['muestra']." ON G2406_ConsInte__b = G2406_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2406_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2406_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2406_C47332 LIKE '%".$B."%' OR G2406_C55275 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2406_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2406");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2406_ConsInte__b, G2406_FechaInsercion , G2406_Usuario ,  G2406_CodigoMiembro  , G2406_PoblacionOrigen , G2406_EstadoDiligenciamiento ,  G2406_IdLlamada , G2406_C47332 as principal ,G2406_C47331,G2406_C47332,G2406_C47333,G2406_C47334,G2406_C47335, a.LISOPC_Nombre____b as G2406_C47336, b.LISOPC_Nombre____b as G2406_C47337,G2406_C47338,G2406_C55274,G2406_C55275, c.LISOPC_Nombre____b as G2406_C47320, d.LISOPC_Nombre____b as G2406_C47321,G2406_C47322,G2406_C47323,G2406_C47324,G2406_C47325,G2406_C47326,G2406_C47327,G2406_C47328,G2406_C55938 FROM '.$BaseDatos.'.G2406 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2406_C47336 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2406_C47337 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2406_C47320 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2406_C47321';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2406_C47323)){
                    $hora_a = explode(' ', $fila->G2406_C47323)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2406_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2406_ConsInte__b , ($fila->G2406_C47331) , ($fila->G2406_C47332) , ($fila->G2406_C47333) , ($fila->G2406_C47334) , ($fila->G2406_C47335) , ($fila->G2406_C47336) , ($fila->G2406_C47337) , ($fila->G2406_C47338) , ($fila->G2406_C55274) , ($fila->G2406_C55275) , ($fila->G2406_C47320) , ($fila->G2406_C47321) , explode(' ', $fila->G2406_C47322)[0] , $hora_a , ($fila->G2406_C47324) , ($fila->G2406_C47325) , ($fila->G2406_C47326) , ($fila->G2406_C47327) , ($fila->G2406_C47328) , ($fila->G2406_C55938) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2406 WHERE G2406_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2406";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2406_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2406_ConsInte__b as id,  G2406_C47332 as camp1 , G2406_C55275 as camp2  FROM '.$BaseDatos.'.G2406 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2406_ConsInte__b as id,  G2406_C47332 as camp1 , G2406_C55275 as camp2  
                    FROM ".$BaseDatos.".G2406  JOIN ".$BaseDatos.".G2406_M".$_POST['muestra']." ON G2406_ConsInte__b = G2406_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2406_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2406_C47332 LIKE "%'.$B.'%" OR G2406_C55275 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2406_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2406 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2406(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2406_C47331"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47331 = '".$_POST["G2406_C47331"]."'";
                $LsqlI .= $separador."G2406_C47331";
                $LsqlV .= $separador."'".$_POST["G2406_C47331"]."'";
                $validar = 1;
            }
             
  
            $G2406_C47332 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2406_C47332"])){
                if($_POST["G2406_C47332"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2406_C47332 = $_POST["G2406_C47332"];
                    $LsqlU .= $separador." G2406_C47332 = ".$G2406_C47332."";
                    $LsqlI .= $separador." G2406_C47332";
                    $LsqlV .= $separador.$G2406_C47332;
                    $validar = 1;
                }
            }
  
            $G2406_C47333 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2406_C47333"])){
                if($_POST["G2406_C47333"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2406_C47333 = $_POST["G2406_C47333"];
                    $LsqlU .= $separador." G2406_C47333 = ".$G2406_C47333."";
                    $LsqlI .= $separador." G2406_C47333";
                    $LsqlV .= $separador.$G2406_C47333;
                    $validar = 1;
                }
            }
  
            $G2406_C47334 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2406_C47334"])){
                if($_POST["G2406_C47334"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2406_C47334 = $_POST["G2406_C47334"];
                    $LsqlU .= $separador." G2406_C47334 = ".$G2406_C47334."";
                    $LsqlI .= $separador." G2406_C47334";
                    $LsqlV .= $separador.$G2406_C47334;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2406_C47335"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47335 = '".$_POST["G2406_C47335"]."'";
                $LsqlI .= $separador."G2406_C47335";
                $LsqlV .= $separador."'".$_POST["G2406_C47335"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C47336"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47336 = '".$_POST["G2406_C47336"]."'";
                $LsqlI .= $separador."G2406_C47336";
                $LsqlV .= $separador."'".$_POST["G2406_C47336"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C47337"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47337 = '".$_POST["G2406_C47337"]."'";
                $LsqlI .= $separador."G2406_C47337";
                $LsqlV .= $separador."'".$_POST["G2406_C47337"]."'";
                $validar = 1;
            }
             
  
            $G2406_C47338 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2406_C47338"])){
                if($_POST["G2406_C47338"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2406_C47338 = $_POST["G2406_C47338"];
                    $LsqlU .= $separador." G2406_C47338 = ".$G2406_C47338."";
                    $LsqlI .= $separador." G2406_C47338";
                    $LsqlV .= $separador.$G2406_C47338;
                    $validar = 1;
                }
            }
  
            $G2406_C55274 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2406_C55274"])){
                if($_POST["G2406_C55274"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2406_C55274 = $_POST["G2406_C55274"];
                    $LsqlU .= $separador." G2406_C55274 = ".$G2406_C55274."";
                    $LsqlI .= $separador." G2406_C55274";
                    $LsqlV .= $separador.$G2406_C55274;
                    $validar = 1;
                }
            }
  
            $G2406_C55275 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2406_C55275"])){
                if($_POST["G2406_C55275"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2406_C55275 = $_POST["G2406_C55275"];
                    $LsqlU .= $separador." G2406_C55275 = ".$G2406_C55275."";
                    $LsqlI .= $separador." G2406_C55275";
                    $LsqlV .= $separador.$G2406_C55275;
                    $validar = 1;
                }
            }
 
            $G2406_C47320 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2406_C47320 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2406_C47320 = ".$G2406_C47320;
                    $LsqlI .= $separador." G2406_C47320";
                    $LsqlV .= $separador.$G2406_C47320;
                    $validar = 1;

                    
                }
            }
 
            $G2406_C47321 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2406_C47321 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2406_C47321 = ".$G2406_C47321;
                    $LsqlI .= $separador." G2406_C47321";
                    $LsqlV .= $separador.$G2406_C47321;
                    $validar = 1;
                }
            }
 
            $G2406_C47322 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2406_C47322 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2406_C47322 = ".$G2406_C47322;
                    $LsqlI .= $separador." G2406_C47322";
                    $LsqlV .= $separador.$G2406_C47322;
                    $validar = 1;
                }
            }
 
            $G2406_C47323 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2406_C47323 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2406_C47323 = ".$G2406_C47323;
                    $LsqlI .= $separador." G2406_C47323";
                    $LsqlV .= $separador.$G2406_C47323;
                    $validar = 1;
                }
            }
 
            $G2406_C47324 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2406_C47324 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2406_C47324 = ".$G2406_C47324;
                    $LsqlI .= $separador." G2406_C47324";
                    $LsqlV .= $separador.$G2406_C47324;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2406_C47325"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47325 = '".$_POST["G2406_C47325"]."'";
                $LsqlI .= $separador."G2406_C47325";
                $LsqlV .= $separador."'".$_POST["G2406_C47325"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C47326"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47326 = '".$_POST["G2406_C47326"]."'";
                $LsqlI .= $separador."G2406_C47326";
                $LsqlV .= $separador."'".$_POST["G2406_C47326"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C47327"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47327 = '".$_POST["G2406_C47327"]."'";
                $LsqlI .= $separador."G2406_C47327";
                $LsqlV .= $separador."'".$_POST["G2406_C47327"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C47328"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47328 = '".$_POST["G2406_C47328"]."'";
                $LsqlI .= $separador."G2406_C47328";
                $LsqlV .= $separador."'".$_POST["G2406_C47328"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C47329"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C47329 = '".$_POST["G2406_C47329"]."'";
                $LsqlI .= $separador."G2406_C47329";
                $LsqlV .= $separador."'".$_POST["G2406_C47329"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2406_C55938"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_C55938 = '".$_POST["G2406_C55938"]."'";
                $LsqlI .= $separador."G2406_C55938";
                $LsqlV .= $separador."'".$_POST["G2406_C55938"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2406_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2406_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2406_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2406_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2406_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2406_Usuario , G2406_FechaInsercion, G2406_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2406_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2406 WHERE G2406_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
