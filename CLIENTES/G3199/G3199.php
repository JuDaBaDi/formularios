
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3199/G3199_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : $_SESSION["IDENTIFICACION"];
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3199_ConsInte__b as id, G3199_C64194 as camp1 , G3199_C64194 as camp2 FROM ".$BaseDatos.".G3199  WHERE G3199_Usuario = ".$idUsuario." ORDER BY G3199_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3199_ConsInte__b as id, G3199_C64194 as camp1 , G3199_C64194 as camp2 FROM ".$BaseDatos.".G3199  ORDER BY G3199_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3199_ConsInte__b as id, G3199_C64194 as camp1 , G3199_C64194 as camp2 FROM ".$BaseDatos.".G3199 JOIN ".$BaseDatos.".G3199_M".$resultEstpas->muestr." ON G3199_ConsInte__b = G3199_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3199_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3199_ConsInte__b as id, G3199_C64194 as camp1 , G3199_C64194 as camp2 FROM ".$BaseDatos.".G3199 JOIN ".$BaseDatos.".G3199_M".$resultEstpas->muestr." ON G3199_ConsInte__b = G3199_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3199_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3199_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3199_ConsInte__b as id, G3199_C64194 as camp1 , G3199_C64194 as camp2 FROM ".$BaseDatos.".G3199  ORDER BY G3199_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="9908" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3199_C64194" id="LblG3199_C64194">ID_GESTION</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3199_C64194'])) {
                            echo $_GET['G3199_C64194'];
                        } ?>" readonly name="G3199_C64194" id="G3199_C64194" placeholder="ID_GESTION"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra. (Nombre del paciente) Con que especialidad o especialista requiere su cita?       </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="9909" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9909c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9909c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3199_C64191" id="LblG3199_C64191">ORIGEN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3199_C64191" value="<?php if (isset($_GET['G3199_C64191'])) {
                            echo $_GET['G3199_C64191'];
                        } ?>" readonly name="G3199_C64191"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3199_C64192" id="LblG3199_C64192">OPTIN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3199_C64192" value="<?php if (isset($_GET['G3199_C64192'])) {
                            echo $_GET['G3199_C64192'];
                        } ?>" readonly name="G3199_C64192"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64193" id="LblG3199_C64193">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64193" id="G3199_C64193">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3943 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  id="9910" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el paciente solicita consulta por primera vez SE LE DEBEN NOMBRAR TODOS LOS ESPECIALISTAS, dándole el derecho al paciente de elegir, en los casos de Ortopedia, Cx general, Ginecologia se deben nombrar todos y recomendar el médico especializado según diagnostico.        </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64197" id="LblG3199_C64197">• Especialidad</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64197" id="G3199_C64197">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3945 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64198" id="LblG3199_C64198">• Especialista que requiere</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64198" id="G3199_C64198">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3946 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64199" id="LblG3199_C64199">• 1 vez /control</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64199" id="G3199_C64199">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3895 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64250" id="LblG3199_C64250">DISPONIBILIDAD DE AGENDAMIENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64250" id="G3199_C64250">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3892 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si no se tiene disponibilidad de agendamiento le informo al paicente lo siguiente       </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Vamos a tomar sus datos (Cedula- Nombre. Teléfono, Eps y especialidad - especialista que requiere, 1 vez/ control), para poder agendarla cuanto antes, Uno de mis compañeros le estara contactando para indicarle fecha y hora de la misma. </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G3199_C64203" id="LblG3199_C64203">¿Señor(a), usted tiene a mano Autorización y Orden medica?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64203" id="G3199_C64203">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3942 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G3199_C64203" id="respuesta_LblG3199_C64203">Respuesta</label>
                        <textarea id="respuesta_G3199_C64203" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9911" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra (Nombre del paciente) vamos a confirmar los datos que tiene su autorizacion.    </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3199_C64205" id="LblG3199_C64205">Fecha de autorización </label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3199_C64205'])) {
                            echo $_GET['G3199_C64205'];
                        } ?>"  name="G3199_C64205" id="G3199_C64205" placeholder="YYYY-MM-DD" nombre="Fecha de autorización ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64206" id="LblG3199_C64206">Remitido A </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64206" id="G3199_C64206">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3955 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3199_C64207" id="LblG3199_C64207">Codigo de autorización </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3199_C64207" value="<?php if (isset($_GET['G3199_C64207'])) {
                            echo $_GET['G3199_C64207'];
                        } ?>"  name="G3199_C64207"  placeholder="Codigo de autorización "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3199_C64208" id="LblG3199_C64208">Copago </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3199_C64208" value="<?php if (isset($_GET['G3199_C64208'])) {
                            echo $_GET['G3199_C64208'];
                        } ?>"  name="G3199_C64208"  placeholder="Copago "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64209" id="LblG3199_C64209">Vigencia de autorización </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64209" id="G3199_C64209">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3894 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el paciente no tiene orden médica, se debe de confirmar con el área de Facturación si se puede autorizar dependiendo el procedimiento o consulta que solicite, esto en los casos de MP - PARTICULAR   </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si esta información esta correcta se le indica al paciente lo siguiente</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra. A continuacion le voy a informar los documentos que debe tener escaneados para poder realizar el agendamiento de su cita </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64213" id="LblG3199_C64213">1. AUTORIZACIÓN (Se debe escanear individual)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64213" id="G3199_C64213">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64214" id="LblG3199_C64214">2. ORDEN MEDICA (Se puede escanear con la historia clínica)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64214" id="G3199_C64214">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64215" id="LblG3199_C64215">3. HISTORIA CLINICA (Se puede escanear con la orden medica)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64215" id="G3199_C64215">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64216" id="LblG3199_C64216">4. EXAMENES (Se debe escanear individual)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64216" id="G3199_C64216">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64217" id="LblG3199_C64217">5. COPAGO (SI LO REQUIERE) (Se debe escanear individual)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64217" id="G3199_C64217">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Con MP y Particular, solicitamos solo los documentos que se requiere.       </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el paciente no ha realizado este proceso se le debe explicar bien, para que en su próxima llamada el paciente adjunte los documentos en línea, dado el caso de que el paciente le sea difícil realizar este proceso se le indica el correo asignado al asesor que tiene la llamada para que le haga el envió correspondiente de la documentación y poder realizar el agendamiento , haciéndole seguimiento al mismo, esto solo para casos especiales Una vez los tenga escaneados se comunica de nuevo con nosotros para asignar su cita.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9912" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra. (Nombre del paciente) En este momento le acabo de enviar un mensaje de texto, también un correo con un link, puede abrir cualquiera de los dos ya que tienen la misma información, selecciona el link que aparece subrayado y lo lleva directamente a el consentimiento, el cual usted debe de leer y firmar, tambien puede adjuntar los documentos, yo estare con usted guiando su proceso.  </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64221" id="LblG3199_C64221">Paciente firmo el consentimiento ? </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64221" id="G3199_C64221">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3944 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64222" id="LblG3199_C64222">Paciente adjunto los documentos ? </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64222" id="G3199_C64222">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3944 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9913" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra. (Nombre del paciente) Le confirmo su cita de Telemedicina, le queda programada para el día (Fecha y Hora) , Le recordamos tener a mano la documentación que nos envía en caso tal de que el medico requiera confirmar algún documento, en caso de no poder atender su cita de Telemedicina debe comunicarse al 8849999 para cancelar o reasignar la misma.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3199_C64224" id="LblG3199_C64224">Está satisfecho con la informacion brindada?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3199_C64224" id="G3199_C64224">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3199_C64225" id="LblG3199_C64225">Observaciones</label>
                        <textarea class="form-control input-sm" name="G3199_C64225" id="G3199_C64225"  value="<?php if (isset($_GET['G3199_C64225'])) {
                            echo $_GET['G3199_C64225'];
                        } ?>" placeholder="Observaciones"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Gracias por llamar a SES HOSPITAL UNIVERSITARIO DE CALDAS, recuerde que lo atendió (Nombre completo del asesor). </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G3199/G3199_eventos.js"></script>
<script type="text/javascript" src="formularios/G3199/G3199_extender_funcionalidad.php"></script><?php require_once "G3199_extender_funcionalidad.php";?><?php require_once "formularios/enviarSms_Mail.php";?>
<script type="text/javascript">

function ocultarBotones(){

    $("#add").hide();
    $("#delete").hide();
    $("#edit").hide();
    $("#Save").hide();
    $("#cancel").hide();

}

ocultarBotones();

function mostrarBotones(){

    $("#add").show();
    $("#delete").show();
    $("#edit").show();
    $("#Save").show();
    $("#cancel").show();

}
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {

        switch(e.data){

            case 'edit':
                    $("#edit").click();
                break;
            case 'delete':
                    $("#delete").click();
                break;
            case 'Save':
                    $("#Save").click();
                break;

        }
        
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje;
        sendMessage('' + mensaje);
    });

    //JDBD - Funcion para descargar los adjuntos
    function bajarAdjunto(id){

        var strURL_t = $("#"+id).attr("adjunto");

        if (strURL_t != "") {

            location.href='<?=$url_crud;?>?adjunto='+strURL_t;
            
        }


    }
    
    

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3199_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3199_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3199_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3199_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3199_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3199_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3199_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3199_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3199_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
        
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            $("#G3199_C64197").val("0").trigger("change");
            $("#G3199_C64198").val("0").trigger("change");
            $("#G3199_C64199").val("0").trigger("change");
            $("#G3199_C64250").val("0").trigger("change");
            $("#G3199_C64206").val("0").trigger("change");
            $("#G3199_C64209").val("0").trigger("change");
            $("#G3199_C64213").val("0").trigger("change");
            $("#G3199_C64214").val("0").trigger("change");
            $("#G3199_C64215").val("0").trigger("change");
            $("#G3199_C64216").val("0").trigger("change");
            $("#G3199_C64217").val("0").trigger("change");
            $("#G3199_C64221").val("0").trigger("change");
            $("#G3199_C64222").val("0").trigger("change");
            $("#G3199_C64224").val("0").trigger("change");
            
            
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G3199_C64194").val(item.G3199_C64194);   
                if(item.G3199_C64195 == 1){
                    $("#G3199_C64195").attr('checked', true);
                }  
                $("#G3199_C64191").val(item.G3199_C64191); 
                $("#G3199_C64192").val(item.G3199_C64192); 
                $("#G3199_C64193").val(item.G3199_C64193).trigger("change");    
                if(item.G3199_C64200 == 1){
                    $("#G3199_C64200").attr('checked', true);
                }  
                $("#G3199_C64197").val(item.G3199_C64197).trigger("change");  
                $("#G3199_C64198").attr("opt",item.G3199_C64198);  
                $("#G3199_C64199").val(item.G3199_C64199).trigger("change");  
                $("#G3199_C64250").val(item.G3199_C64250).trigger("change");    
                if(item.G3199_C64201 == 1){
                    $("#G3199_C64201").attr('checked', true);
                }    
                if(item.G3199_C64202 == 1){
                    $("#G3199_C64202").attr('checked', true);
                }  
                $("#G3199_C64203").val(item.G3199_C64203).trigger("change");    
                if(item.G3199_C64204 == 1){
                    $("#G3199_C64204").attr('checked', true);
                }  
                $("#G3199_C64205").val(item.G3199_C64205); 
                $("#G3199_C64206").val(item.G3199_C64206).trigger("change");  
                $("#G3199_C64207").val(item.G3199_C64207); 
                $("#G3199_C64208").val(item.G3199_C64208); 
                $("#G3199_C64209").val(item.G3199_C64209).trigger("change");    
                if(item.G3199_C64210 == 1){
                    $("#G3199_C64210").attr('checked', true);
                }    
                if(item.G3199_C64211 == 1){
                    $("#G3199_C64211").attr('checked', true);
                }    
                if(item.G3199_C64212 == 1){
                    $("#G3199_C64212").attr('checked', true);
                }  
                $("#G3199_C64213").val(item.G3199_C64213).trigger("change");  
                $("#G3199_C64214").val(item.G3199_C64214).trigger("change");  
                $("#G3199_C64215").val(item.G3199_C64215).trigger("change");  
                $("#G3199_C64216").val(item.G3199_C64216).trigger("change");  
                $("#G3199_C64217").val(item.G3199_C64217).trigger("change");    
                if(item.G3199_C64218 == 1){
                    $("#G3199_C64218").attr('checked', true);
                }    
                if(item.G3199_C64219 == 1){
                    $("#G3199_C64219").attr('checked', true);
                }    
                if(item.G3199_C64220 == 1){
                    $("#G3199_C64220").attr('checked', true);
                }  
                $("#G3199_C64221").val(item.G3199_C64221).trigger("change");  
                $("#G3199_C64222").val(item.G3199_C64222).trigger("change");    
                if(item.G3199_C64223 == 1){
                    $("#G3199_C64223").attr('checked', true);
                }  
                $("#G3199_C64224").val(item.G3199_C64224).trigger("change");  
                $("#G3199_C64225").val(item.G3199_C64225);   
                if(item.G3199_C64226 == 1){
                    $("#G3199_C64226").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G3199_C64193").select2();

    $("#G3199_C64197").select2();

    $("#G3199_C64198").select2();

    $("#G3199_C64199").select2();

    $("#G3199_C64250").select2();

    $("#G3199_C64203").select2();

    $("#G3199_C64206").select2();

    $("#G3199_C64209").select2();

    $("#G3199_C64213").select2();

    $("#G3199_C64214").select2();

    $("#G3199_C64215").select2();

    $("#G3199_C64216").select2();

    $("#G3199_C64217").select2();

    $("#G3199_C64221").select2();

    $("#G3199_C64222").select2();

    $("#G3199_C64224").select2();
        //datepickers
        

        $("#G3199_C64205").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G3199_C64194").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G3199_C64193").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para • Especialidad 

    $("#G3199_C64197").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3946' , idPadre : $(this).val() },
            success : function(data){
                var optG3199_C64198 = $("#G3199_C64198").attr("opt");
                $("#G3199_C64198").html(data);
                if (optG3199_C64198 != null) {
                    $("#G3199_C64198").val(optG3199_C64198).trigger("change");
                }
            }
        });
        
    });

    //function para • Especialista que requiere 

    $("#G3199_C64198").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para • 1 vez /control 

    $("#G3199_C64199").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DISPONIBILIDAD DE AGENDAMIENTO 

    $("#G3199_C64250").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Señor(a), usted tiene a mano Autorización y Orden medica? 

    $("#G3199_C64203").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G3199_C64203 option:selected").attr('respuesta');
        $("#respuesta_G3199_C64203").val(respuesta);
        

    });

    //function para Remitido A  

    $("#G3199_C64206").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Vigencia de autorización  

    $("#G3199_C64209").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 1. AUTORIZACIÓN (Se debe escanear individual) 

    $("#G3199_C64213").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 2. ORDEN MEDICA (Se puede escanear con la historia clínica) 

    $("#G3199_C64214").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 3. HISTORIA CLINICA (Se puede escanear con la orden medica) 

    $("#G3199_C64215").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 4. EXAMENES (Se debe escanear individual) 

    $("#G3199_C64216").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 5. COPAGO (SI LO REQUIERE) (Se debe escanear individual) 

    $("#G3199_C64217").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Paciente firmo el consentimiento ?  

    $("#G3199_C64221").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Paciente adjunto los documentos ?  

    $("#G3199_C64222").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Está satisfecho con la informacion brindada? 

    $("#G3199_C64224").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3199_C64194").val(item.G3199_C64194);
      
                                                if(item.G3199_C64195 == 1){
                                                   $("#G3199_C64195").attr('checked', true);
                                                } 
 
                                                $("#G3199_C64191").val(item.G3199_C64191);
 
                                                $("#G3199_C64192").val(item.G3199_C64192);
 
                    $("#G3199_C64193").val(item.G3199_C64193).trigger("change"); 
      
                                                if(item.G3199_C64200 == 1){
                                                   $("#G3199_C64200").attr('checked', true);
                                                } 
 
                    $("#G3199_C64197").val(item.G3199_C64197).trigger("change"); 
 
                    $("#G3199_C64198").attr("opt",item.G3199_C64198); 
 
                    $("#G3199_C64199").val(item.G3199_C64199).trigger("change"); 
 
                    $("#G3199_C64250").val(item.G3199_C64250).trigger("change"); 
      
                                                if(item.G3199_C64201 == 1){
                                                   $("#G3199_C64201").attr('checked', true);
                                                } 
      
                                                if(item.G3199_C64202 == 1){
                                                   $("#G3199_C64202").attr('checked', true);
                                                } 
 
                    $("#G3199_C64203").val(item.G3199_C64203).trigger("change"); 
      
                                                if(item.G3199_C64204 == 1){
                                                   $("#G3199_C64204").attr('checked', true);
                                                } 
 
                                                $("#G3199_C64205").val(item.G3199_C64205);
 
                    $("#G3199_C64206").val(item.G3199_C64206).trigger("change"); 
 
                                                $("#G3199_C64207").val(item.G3199_C64207);
 
                                                $("#G3199_C64208").val(item.G3199_C64208);
 
                    $("#G3199_C64209").val(item.G3199_C64209).trigger("change"); 
      
                                                if(item.G3199_C64210 == 1){
                                                   $("#G3199_C64210").attr('checked', true);
                                                } 
      
                                                if(item.G3199_C64211 == 1){
                                                   $("#G3199_C64211").attr('checked', true);
                                                } 
      
                                                if(item.G3199_C64212 == 1){
                                                   $("#G3199_C64212").attr('checked', true);
                                                } 
 
                    $("#G3199_C64213").val(item.G3199_C64213).trigger("change"); 
 
                    $("#G3199_C64214").val(item.G3199_C64214).trigger("change"); 
 
                    $("#G3199_C64215").val(item.G3199_C64215).trigger("change"); 
 
                    $("#G3199_C64216").val(item.G3199_C64216).trigger("change"); 
 
                    $("#G3199_C64217").val(item.G3199_C64217).trigger("change"); 
      
                                                if(item.G3199_C64218 == 1){
                                                   $("#G3199_C64218").attr('checked', true);
                                                } 
      
                                                if(item.G3199_C64219 == 1){
                                                   $("#G3199_C64219").attr('checked', true);
                                                } 
      
                                                if(item.G3199_C64220 == 1){
                                                   $("#G3199_C64220").attr('checked', true);
                                                } 
 
                    $("#G3199_C64221").val(item.G3199_C64221).trigger("change"); 
 
                    $("#G3199_C64222").val(item.G3199_C64222).trigger("change"); 
      
                                                if(item.G3199_C64223 == 1){
                                                   $("#G3199_C64223").attr('checked', true);
                                                } 
 
                    $("#G3199_C64224").val(item.G3199_C64224).trigger("change"); 
 
                                                $("#G3199_C64225").val(item.G3199_C64225);
      
                                                if(item.G3199_C64226 == 1){
                                                   $("#G3199_C64226").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }    
                sendMessage('Cierrame');      
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G3199_C64197").val()==0 || $("#G3199_C64197").val() == null || $("#G3199_C64197").val() == -1) && $("#G3199_C64197").prop("disabled") == false){
                alertify.error('• Especialidad debe ser diligenciado');
                $("#G3199_C64197").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64198").val()==0 || $("#G3199_C64198").val() == null || $("#G3199_C64198").val() == -1) && $("#G3199_C64198").prop("disabled") == false){
                alertify.error('• Especialista que requiere debe ser diligenciado');
                $("#G3199_C64198").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64199").val()==0 || $("#G3199_C64199").val() == null || $("#G3199_C64199").val() == -1) && $("#G3199_C64199").prop("disabled") == false){
                alertify.error('• 1 vez /control debe ser diligenciado');
                $("#G3199_C64199").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64250").val()==0 || $("#G3199_C64250").val() == null || $("#G3199_C64250").val() == -1) && $("#G3199_C64250").prop("disabled") == false){
                alertify.error('DISPONIBILIDAD DE AGENDAMIENTO debe ser diligenciado');
                $("#G3199_C64250").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64203").val()==0 || $("#G3199_C64203").val() == null || $("#G3199_C64203").val() == -1) && $("#G3199_C64203").prop("disabled") == false){
                alertify.error('¿Señor(a), usted tiene a mano Autorización y Orden medica? debe ser diligenciado');
                $("#G3199_C64203").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64205").val() == "") && $("#G3199_C64205").prop("disabled") == false){
                alertify.error('Fecha de autorización  debe ser diligenciado');
                $("#G3199_C64205").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64207").val() == "") && $("#G3199_C64207").prop("disabled") == false){
                alertify.error('Codigo de autorización  debe ser diligenciado');
                $("#G3199_C64207").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64208").val() == "") && $("#G3199_C64208").prop("disabled") == false){
                alertify.error('Copago  debe ser diligenciado');
                $("#G3199_C64208").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64209").val()==0 || $("#G3199_C64209").val() == null || $("#G3199_C64209").val() == -1) && $("#G3199_C64209").prop("disabled") == false){
                alertify.error('Vigencia de autorización  debe ser diligenciado');
                $("#G3199_C64209").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3199_C64224").val()==0 || $("#G3199_C64224").val() == null || $("#G3199_C64224").val() == -1) && $("#G3199_C64224").prop("disabled") == false){
                alertify.error('Está satisfecho con la informacion brindada? debe ser diligenciado');
                $("#G3199_C64224").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            /*let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });

            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }*/

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>

         mostrarBotones();
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','ID_GESTION','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','• Especialidad','• Especialista que requiere','• 1 vez /control','DISPONIBILIDAD DE AGENDAMIENTO','¿Señor(a), usted tiene a mano Autorización y Orden medica?','Fecha de autorización ','Remitido A ','Codigo de autorización ','Copago ','Vigencia de autorización ','1. AUTORIZACIÓN (Se debe escanear individual)','2. ORDEN MEDICA (Se puede escanear con la historia clínica)','3. HISTORIA CLINICA (Se puede escanear con la orden medica)','4. EXAMENES (Se debe escanear individual)','5. COPAGO (SI LO REQUIERE) (Se debe escanear individual)','Paciente firmo el consentimiento ? ','Paciente adjunto los documentos ? ','Está satisfecho con la informacion brindada?','Observaciones'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G3199_C64194', 
                        index:'G3199_C64194', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3199_C64191', 
                        index: 'G3199_C64191', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3199_C64192', 
                        index: 'G3199_C64192', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3199_C64193', 
                        index:'G3199_C64193', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3943&campo=G3199_C64193'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64197', 
                        index:'G3199_C64197', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3945&campo=G3199_C64197'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64198', 
                        index:'G3199_C64198', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3946&campo=G3199_C64198'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64199', 
                        index:'G3199_C64199', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3895&campo=G3199_C64199'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64250', 
                        index:'G3199_C64250', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3892&campo=G3199_C64250'
                        }
                    }

                    ,
                    {  
                        name:'G3199_C64205', 
                        index:'G3199_C64205', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64206', 
                        index:'G3199_C64206', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3955&campo=G3199_C64206'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64207', 
                        index: 'G3199_C64207', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3199_C64208', 
                        index: 'G3199_C64208', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3199_C64209', 
                        index:'G3199_C64209', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3894&campo=G3199_C64209'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64213', 
                        index:'G3199_C64213', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3199_C64213'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64214', 
                        index:'G3199_C64214', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3199_C64214'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64215', 
                        index:'G3199_C64215', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3199_C64215'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64216', 
                        index:'G3199_C64216', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3199_C64216'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64217', 
                        index:'G3199_C64217', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3199_C64217'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64221', 
                        index:'G3199_C64221', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3944&campo=G3199_C64221'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64222', 
                        index:'G3199_C64222', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3944&campo=G3199_C64222'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64224', 
                        index:'G3199_C64224', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3199_C64224'
                        }
                    }

                    ,
                    { 
                        name:'G3199_C64225', 
                        index:'G3199_C64225', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3199_C64194',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G3199_C64194").val(item.G3199_C64194);
    
                        if(item.G3199_C64195 == 1){
                           $("#G3199_C64195").attr('checked', true);
                        } 

                        $("#G3199_C64191").val(item.G3199_C64191);

                        $("#G3199_C64192").val(item.G3199_C64192);
 
                    $("#G3199_C64193").val(item.G3199_C64193).trigger("change"); 
    
                        if(item.G3199_C64200 == 1){
                           $("#G3199_C64200").attr('checked', true);
                        } 
 
                    $("#G3199_C64197").val(item.G3199_C64197).trigger("change"); 
 
                    $("#G3199_C64198").attr("opt",item.G3199_C64198); 
 
                    $("#G3199_C64199").val(item.G3199_C64199).trigger("change"); 
 
                    $("#G3199_C64250").val(item.G3199_C64250).trigger("change"); 
    
                        if(item.G3199_C64201 == 1){
                           $("#G3199_C64201").attr('checked', true);
                        } 
    
                        if(item.G3199_C64202 == 1){
                           $("#G3199_C64202").attr('checked', true);
                        } 
 
                    $("#G3199_C64203").val(item.G3199_C64203).trigger("change"); 
    
                        if(item.G3199_C64204 == 1){
                           $("#G3199_C64204").attr('checked', true);
                        } 

                        $("#G3199_C64205").val(item.G3199_C64205);
 
                    $("#G3199_C64206").val(item.G3199_C64206).trigger("change"); 

                        $("#G3199_C64207").val(item.G3199_C64207);

                        $("#G3199_C64208").val(item.G3199_C64208);
 
                    $("#G3199_C64209").val(item.G3199_C64209).trigger("change"); 
    
                        if(item.G3199_C64210 == 1){
                           $("#G3199_C64210").attr('checked', true);
                        } 
    
                        if(item.G3199_C64211 == 1){
                           $("#G3199_C64211").attr('checked', true);
                        } 
    
                        if(item.G3199_C64212 == 1){
                           $("#G3199_C64212").attr('checked', true);
                        } 
 
                    $("#G3199_C64213").val(item.G3199_C64213).trigger("change"); 
 
                    $("#G3199_C64214").val(item.G3199_C64214).trigger("change"); 
 
                    $("#G3199_C64215").val(item.G3199_C64215).trigger("change"); 
 
                    $("#G3199_C64216").val(item.G3199_C64216).trigger("change"); 
 
                    $("#G3199_C64217").val(item.G3199_C64217).trigger("change"); 
    
                        if(item.G3199_C64218 == 1){
                           $("#G3199_C64218").attr('checked', true);
                        } 
    
                        if(item.G3199_C64219 == 1){
                           $("#G3199_C64219").attr('checked', true);
                        } 
    
                        if(item.G3199_C64220 == 1){
                           $("#G3199_C64220").attr('checked', true);
                        } 
 
                    $("#G3199_C64221").val(item.G3199_C64221).trigger("change"); 
 
                    $("#G3199_C64222").val(item.G3199_C64222).trigger("change"); 
    
                        if(item.G3199_C64223 == 1){
                           $("#G3199_C64223").attr('checked', true);
                        } 
 
                    $("#G3199_C64224").val(item.G3199_C64224).trigger("change"); 

                        $("#G3199_C64225").val(item.G3199_C64225);
    
                        if(item.G3199_C64226 == 1){
                           $("#G3199_C64226").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
