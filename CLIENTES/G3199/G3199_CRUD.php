<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3199_ConsInte__b, G3199_FechaInsercion , G3199_Usuario ,  G3199_CodigoMiembro  , G3199_PoblacionOrigen , G3199_EstadoDiligenciamiento ,  G3199_IdLlamada , G3199_C64194 as principal ,G3199_C64194,G3199_C64191,G3199_C64192,G3199_C64193,G3199_C64197,G3199_C64198,G3199_C64199,G3199_C64250,G3199_C64203,G3199_C64205,G3199_C64206,G3199_C64207,G3199_C64208,G3199_C64209,G3199_C64213,G3199_C64214,G3199_C64215,G3199_C64216,G3199_C64217,G3199_C64221,G3199_C64222,G3199_C64224,G3199_C64225 FROM '.$BaseDatos.'.G3199 WHERE G3199_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3199_C64194'] = $key->G3199_C64194;

                $datos[$i]['G3199_C64191'] = $key->G3199_C64191;

                $datos[$i]['G3199_C64192'] = $key->G3199_C64192;

                $datos[$i]['G3199_C64193'] = $key->G3199_C64193;

                $datos[$i]['G3199_C64197'] = $key->G3199_C64197;

                $datos[$i]['G3199_C64198'] = $key->G3199_C64198;

                $datos[$i]['G3199_C64199'] = $key->G3199_C64199;

                $datos[$i]['G3199_C64250'] = $key->G3199_C64250;

                $datos[$i]['G3199_C64203'] = $key->G3199_C64203;

                $datos[$i]['G3199_C64205'] = explode(' ', $key->G3199_C64205)[0];

                $datos[$i]['G3199_C64206'] = $key->G3199_C64206;

                $datos[$i]['G3199_C64207'] = $key->G3199_C64207;

                $datos[$i]['G3199_C64208'] = $key->G3199_C64208;

                $datos[$i]['G3199_C64209'] = $key->G3199_C64209;

                $datos[$i]['G3199_C64213'] = $key->G3199_C64213;

                $datos[$i]['G3199_C64214'] = $key->G3199_C64214;

                $datos[$i]['G3199_C64215'] = $key->G3199_C64215;

                $datos[$i]['G3199_C64216'] = $key->G3199_C64216;

                $datos[$i]['G3199_C64217'] = $key->G3199_C64217;

                $datos[$i]['G3199_C64221'] = $key->G3199_C64221;

                $datos[$i]['G3199_C64222'] = $key->G3199_C64222;

                $datos[$i]['G3199_C64224'] = $key->G3199_C64224;

                $datos[$i]['G3199_C64225'] = $key->G3199_C64225;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3199";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3199_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3199_ConsInte__b as id,  G3199_C64194 as camp1 , G3199_C64194 as camp2 
                     FROM ".$BaseDatos.".G3199  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3199_ConsInte__b as id,  G3199_C64194 as camp1 , G3199_C64194 as camp2  
                    FROM ".$BaseDatos.".G3199  JOIN ".$BaseDatos.".G3199_M".$_POST['muestra']." ON G3199_ConsInte__b = G3199_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3199_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3199_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3199_C64194 LIKE '%".$B."%' OR G3199_C64194 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3199_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3199");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3199_ConsInte__b, G3199_FechaInsercion , G3199_Usuario ,  G3199_CodigoMiembro  , G3199_PoblacionOrigen , G3199_EstadoDiligenciamiento ,  G3199_IdLlamada , G3199_C64194 as principal ,G3199_C64194,G3199_C64191,G3199_C64192, a.LISOPC_Nombre____b as G3199_C64193, b.LISOPC_Nombre____b as G3199_C64197, c.LISOPC_Nombre____b as G3199_C64198, d.LISOPC_Nombre____b as G3199_C64199, e.LISOPC_Nombre____b as G3199_C64250,G3199_C64203,G3199_C64205, f.LISOPC_Nombre____b as G3199_C64206,G3199_C64207,G3199_C64208, g.LISOPC_Nombre____b as G3199_C64209, h.LISOPC_Nombre____b as G3199_C64213, i.LISOPC_Nombre____b as G3199_C64214, j.LISOPC_Nombre____b as G3199_C64215, k.LISOPC_Nombre____b as G3199_C64216, l.LISOPC_Nombre____b as G3199_C64217, m.LISOPC_Nombre____b as G3199_C64221, n.LISOPC_Nombre____b as G3199_C64222, o.LISOPC_Nombre____b as G3199_C64224,G3199_C64225 FROM '.$BaseDatos.'.G3199 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3199_C64193 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3199_C64197 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3199_C64198 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3199_C64199 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3199_C64250 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3199_C64206 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G3199_C64209 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G3199_C64213 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G3199_C64214 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G3199_C64215 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G3199_C64216 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G3199_C64217 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G3199_C64221 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G3199_C64222 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G3199_C64224';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G3199_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3199_ConsInte__b , ($fila->G3199_C64194) , ($fila->G3199_C64191) , ($fila->G3199_C64192) , ($fila->G3199_C64193) , ($fila->G3199_C64197) , ($fila->G3199_C64198) , ($fila->G3199_C64199) , ($fila->G3199_C64250) , ($fila->G3199_C64203) , explode(' ', $fila->G3199_C64205)[0] , ($fila->G3199_C64206) , ($fila->G3199_C64207) , ($fila->G3199_C64208) , ($fila->G3199_C64209) , ($fila->G3199_C64213) , ($fila->G3199_C64214) , ($fila->G3199_C64215) , ($fila->G3199_C64216) , ($fila->G3199_C64217) , ($fila->G3199_C64221) , ($fila->G3199_C64222) , ($fila->G3199_C64224) , ($fila->G3199_C64225) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3199 WHERE G3199_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3199";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3199_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3199_ConsInte__b as id,  G3199_C64194 as camp1 , G3199_C64194 as camp2  FROM '.$BaseDatos.'.G3199 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3199_ConsInte__b as id,  G3199_C64194 as camp1 , G3199_C64194 as camp2  
                    FROM ".$BaseDatos.".G3199  JOIN ".$BaseDatos.".G3199_M".$_POST['muestra']." ON G3199_ConsInte__b = G3199_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3199_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3199_C64194 LIKE "%'.$B.'%" OR G3199_C64194 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3199_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3199 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3199(";
            $LsqlV = " VALUES ("; 
  
            $G3199_C64194 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3199_C64194"])){
                if($_POST["G3199_C64194"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3199_C64194 = $_POST["G3199_C64194"];
                    $LsqlU .= $separador." G3199_C64194 = ".$G3199_C64194."";
                    $LsqlI .= $separador." G3199_C64194";
                    $LsqlV .= $separador.$G3199_C64194;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3199_C64195"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64195 = '".$_POST["G3199_C64195"]."'";
                $LsqlI .= $separador."G3199_C64195";
                $LsqlV .= $separador."'".$_POST["G3199_C64195"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64191"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64191 = '".$_POST["G3199_C64191"]."'";
                $LsqlI .= $separador."G3199_C64191";
                $LsqlV .= $separador."'".$_POST["G3199_C64191"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64192"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64192 = '".$_POST["G3199_C64192"]."'";
                $LsqlI .= $separador."G3199_C64192";
                $LsqlV .= $separador."'".$_POST["G3199_C64192"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64193"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64193 = '".$_POST["G3199_C64193"]."'";
                $LsqlI .= $separador."G3199_C64193";
                $LsqlV .= $separador."'".$_POST["G3199_C64193"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64200"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64200 = '".$_POST["G3199_C64200"]."'";
                $LsqlI .= $separador."G3199_C64200";
                $LsqlV .= $separador."'".$_POST["G3199_C64200"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64197"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64197 = '".$_POST["G3199_C64197"]."'";
                $LsqlI .= $separador."G3199_C64197";
                $LsqlV .= $separador."'".$_POST["G3199_C64197"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64198"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64198 = '".$_POST["G3199_C64198"]."'";
                $LsqlI .= $separador."G3199_C64198";
                $LsqlV .= $separador."'".$_POST["G3199_C64198"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64199"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64199 = '".$_POST["G3199_C64199"]."'";
                $LsqlI .= $separador."G3199_C64199";
                $LsqlV .= $separador."'".$_POST["G3199_C64199"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64250"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64250 = '".$_POST["G3199_C64250"]."'";
                $LsqlI .= $separador."G3199_C64250";
                $LsqlV .= $separador."'".$_POST["G3199_C64250"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64201"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64201 = '".$_POST["G3199_C64201"]."'";
                $LsqlI .= $separador."G3199_C64201";
                $LsqlV .= $separador."'".$_POST["G3199_C64201"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64202"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64202 = '".$_POST["G3199_C64202"]."'";
                $LsqlI .= $separador."G3199_C64202";
                $LsqlV .= $separador."'".$_POST["G3199_C64202"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64203"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64203 = '".$_POST["G3199_C64203"]."'";
                $LsqlI .= $separador."G3199_C64203";
                $LsqlV .= $separador."'".$_POST["G3199_C64203"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64204"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64204 = '".$_POST["G3199_C64204"]."'";
                $LsqlI .= $separador."G3199_C64204";
                $LsqlV .= $separador."'".$_POST["G3199_C64204"]."'";
                $validar = 1;
            }
             
 
            $G3199_C64205 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3199_C64205"])){    
                if($_POST["G3199_C64205"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3199_C64205"]);
                    if(count($tieneHora) > 1){
                        $G3199_C64205 = "'".$_POST["G3199_C64205"]."'";
                    }else{
                        $G3199_C64205 = "'".str_replace(' ', '',$_POST["G3199_C64205"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3199_C64205 = ".$G3199_C64205;
                    $LsqlI .= $separador." G3199_C64205";
                    $LsqlV .= $separador.$G3199_C64205;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3199_C64206"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64206 = '".$_POST["G3199_C64206"]."'";
                $LsqlI .= $separador."G3199_C64206";
                $LsqlV .= $separador."'".$_POST["G3199_C64206"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64207"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64207 = '".$_POST["G3199_C64207"]."'";
                $LsqlI .= $separador."G3199_C64207";
                $LsqlV .= $separador."'".$_POST["G3199_C64207"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64208"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64208 = '".$_POST["G3199_C64208"]."'";
                $LsqlI .= $separador."G3199_C64208";
                $LsqlV .= $separador."'".$_POST["G3199_C64208"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64209"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64209 = '".$_POST["G3199_C64209"]."'";
                $LsqlI .= $separador."G3199_C64209";
                $LsqlV .= $separador."'".$_POST["G3199_C64209"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64210"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64210 = '".$_POST["G3199_C64210"]."'";
                $LsqlI .= $separador."G3199_C64210";
                $LsqlV .= $separador."'".$_POST["G3199_C64210"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64211"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64211 = '".$_POST["G3199_C64211"]."'";
                $LsqlI .= $separador."G3199_C64211";
                $LsqlV .= $separador."'".$_POST["G3199_C64211"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64212"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64212 = '".$_POST["G3199_C64212"]."'";
                $LsqlI .= $separador."G3199_C64212";
                $LsqlV .= $separador."'".$_POST["G3199_C64212"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64213"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64213 = '".$_POST["G3199_C64213"]."'";
                $LsqlI .= $separador."G3199_C64213";
                $LsqlV .= $separador."'".$_POST["G3199_C64213"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64214"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64214 = '".$_POST["G3199_C64214"]."'";
                $LsqlI .= $separador."G3199_C64214";
                $LsqlV .= $separador."'".$_POST["G3199_C64214"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64215"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64215 = '".$_POST["G3199_C64215"]."'";
                $LsqlI .= $separador."G3199_C64215";
                $LsqlV .= $separador."'".$_POST["G3199_C64215"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64216"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64216 = '".$_POST["G3199_C64216"]."'";
                $LsqlI .= $separador."G3199_C64216";
                $LsqlV .= $separador."'".$_POST["G3199_C64216"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64217"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64217 = '".$_POST["G3199_C64217"]."'";
                $LsqlI .= $separador."G3199_C64217";
                $LsqlV .= $separador."'".$_POST["G3199_C64217"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64218"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64218 = '".$_POST["G3199_C64218"]."'";
                $LsqlI .= $separador."G3199_C64218";
                $LsqlV .= $separador."'".$_POST["G3199_C64218"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64219"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64219 = '".$_POST["G3199_C64219"]."'";
                $LsqlI .= $separador."G3199_C64219";
                $LsqlV .= $separador."'".$_POST["G3199_C64219"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64220"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64220 = '".$_POST["G3199_C64220"]."'";
                $LsqlI .= $separador."G3199_C64220";
                $LsqlV .= $separador."'".$_POST["G3199_C64220"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64221"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64221 = '".$_POST["G3199_C64221"]."'";
                $LsqlI .= $separador."G3199_C64221";
                $LsqlV .= $separador."'".$_POST["G3199_C64221"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64222"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64222 = '".$_POST["G3199_C64222"]."'";
                $LsqlI .= $separador."G3199_C64222";
                $LsqlV .= $separador."'".$_POST["G3199_C64222"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64223"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64223 = '".$_POST["G3199_C64223"]."'";
                $LsqlI .= $separador."G3199_C64223";
                $LsqlV .= $separador."'".$_POST["G3199_C64223"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64224"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64224 = '".$_POST["G3199_C64224"]."'";
                $LsqlI .= $separador."G3199_C64224";
                $LsqlV .= $separador."'".$_POST["G3199_C64224"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64225"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64225 = '".$_POST["G3199_C64225"]."'";
                $LsqlI .= $separador."G3199_C64225";
                $LsqlV .= $separador."'".$_POST["G3199_C64225"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3199_C64226"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_C64226 = '".$_POST["G3199_C64226"]."'";
                $LsqlI .= $separador."G3199_C64226";
                $LsqlV .= $separador."'".$_POST["G3199_C64226"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3199_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3199_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3199_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3199_Usuario , G3199_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3199_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3199 WHERE G3199_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3199 SET G3199_UltiGest__b =-14, G3199_GesMasImp_b =-14, G3199_TipoReintentoUG_b =0, G3199_TipoReintentoGMI_b =0, G3199_ClasificacionUG_b =3, G3199_ClasificacionGMI_b =3, G3199_EstadoUG_b =-14, G3199_EstadoGMI_b =-14, G3199_CantidadIntentos =0, G3199_CantidadIntentosGMI_b =0 WHERE G3199_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
