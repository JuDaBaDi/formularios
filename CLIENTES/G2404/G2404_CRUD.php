<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55277")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55277");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2404_ConsInte__b, G2404_FechaInsercion , G2404_Usuario ,  G2404_CodigoMiembro  , G2404_PoblacionOrigen , G2404_EstadoDiligenciamiento ,  G2404_IdLlamada , G2404_C47302 as principal ,G2404_C47301,G2404_C47302,G2404_C47303,G2404_C47304,G2404_C47305,G2404_C47306,G2404_C47307,G2404_C47308,G2404_C55276,G2404_C55277,G2404_C47290,G2404_C47291,G2404_C47292,G2404_C47293,G2404_C47294,G2404_C47295,G2404_C47296,G2404_C47297,G2404_C47298 FROM '.$BaseDatos.'.G2404 WHERE G2404_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2404_C47301'] = $key->G2404_C47301;

                $datos[$i]['G2404_C47302'] = $key->G2404_C47302;

                $datos[$i]['G2404_C47303'] = $key->G2404_C47303;

                $datos[$i]['G2404_C47304'] = $key->G2404_C47304;

                $datos[$i]['G2404_C47305'] = $key->G2404_C47305;

                $datos[$i]['G2404_C47306'] = $key->G2404_C47306;

                $datos[$i]['G2404_C47307'] = $key->G2404_C47307;

                $datos[$i]['G2404_C47308'] = $key->G2404_C47308;

                $datos[$i]['G2404_C55276'] = $key->G2404_C55276;

                $datos[$i]['G2404_C55277'] = $key->G2404_C55277;

                $datos[$i]['G2404_C47290'] = $key->G2404_C47290;

                $datos[$i]['G2404_C47291'] = $key->G2404_C47291;

                $datos[$i]['G2404_C47292'] = explode(' ', $key->G2404_C47292)[0];
  
                $hora = '';
                if(!is_null($key->G2404_C47293)){
                    $hora = explode(' ', $key->G2404_C47293)[1];
                }

                $datos[$i]['G2404_C47293'] = $hora;

                $datos[$i]['G2404_C47294'] = $key->G2404_C47294;

                $datos[$i]['G2404_C47295'] = $key->G2404_C47295;

                $datos[$i]['G2404_C47296'] = $key->G2404_C47296;

                $datos[$i]['G2404_C47297'] = $key->G2404_C47297;

                $datos[$i]['G2404_C47298'] = $key->G2404_C47298;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2404";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2404_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2404_ConsInte__b as id,  G2404_C47302 as camp1 , G2404_C55277 as camp2 
                     FROM ".$BaseDatos.".G2404  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2404_ConsInte__b as id,  G2404_C47302 as camp1 , G2404_C55277 as camp2  
                    FROM ".$BaseDatos.".G2404  JOIN ".$BaseDatos.".G2404_M".$_POST['muestra']." ON G2404_ConsInte__b = G2404_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2404_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2404_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2404_C47302 LIKE '%".$B."%' OR G2404_C55277 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2404_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2404");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2404_ConsInte__b, G2404_FechaInsercion , G2404_Usuario ,  G2404_CodigoMiembro  , G2404_PoblacionOrigen , G2404_EstadoDiligenciamiento ,  G2404_IdLlamada , G2404_C47302 as principal ,G2404_C47301,G2404_C47302,G2404_C47303,G2404_C47304,G2404_C47305, a.LISOPC_Nombre____b as G2404_C47306, b.LISOPC_Nombre____b as G2404_C47307,G2404_C47308,G2404_C55276,G2404_C55277, c.LISOPC_Nombre____b as G2404_C47290, d.LISOPC_Nombre____b as G2404_C47291,G2404_C47292,G2404_C47293,G2404_C47294,G2404_C47295,G2404_C47296,G2404_C47297,G2404_C47298 FROM '.$BaseDatos.'.G2404 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2404_C47306 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2404_C47307 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2404_C47290 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2404_C47291';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2404_C47293)){
                    $hora_a = explode(' ', $fila->G2404_C47293)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2404_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2404_ConsInte__b , ($fila->G2404_C47301) , ($fila->G2404_C47302) , ($fila->G2404_C47303) , ($fila->G2404_C47304) , ($fila->G2404_C47305) , ($fila->G2404_C47306) , ($fila->G2404_C47307) , ($fila->G2404_C47308) , ($fila->G2404_C55276) , ($fila->G2404_C55277) , ($fila->G2404_C47290) , ($fila->G2404_C47291) , explode(' ', $fila->G2404_C47292)[0] , $hora_a , ($fila->G2404_C47294) , ($fila->G2404_C47295) , ($fila->G2404_C47296) , ($fila->G2404_C47297) , ($fila->G2404_C47298) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2404 WHERE G2404_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2404";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2404_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2404_ConsInte__b as id,  G2404_C47302 as camp1 , G2404_C55277 as camp2  FROM '.$BaseDatos.'.G2404 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2404_ConsInte__b as id,  G2404_C47302 as camp1 , G2404_C55277 as camp2  
                    FROM ".$BaseDatos.".G2404  JOIN ".$BaseDatos.".G2404_M".$_POST['muestra']." ON G2404_ConsInte__b = G2404_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2404_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2404_C47302 LIKE "%'.$B.'%" OR G2404_C55277 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2404_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2404 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2404(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2404_C47301"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47301 = '".$_POST["G2404_C47301"]."'";
                $LsqlI .= $separador."G2404_C47301";
                $LsqlV .= $separador."'".$_POST["G2404_C47301"]."'";
                $validar = 1;
            }
             
  
            $G2404_C47302 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2404_C47302"])){
                if($_POST["G2404_C47302"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2404_C47302 = $_POST["G2404_C47302"];
                    $LsqlU .= $separador." G2404_C47302 = ".$G2404_C47302."";
                    $LsqlI .= $separador." G2404_C47302";
                    $LsqlV .= $separador.$G2404_C47302;
                    $validar = 1;
                }
            }
  
            $G2404_C47303 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2404_C47303"])){
                if($_POST["G2404_C47303"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2404_C47303 = $_POST["G2404_C47303"];
                    $LsqlU .= $separador." G2404_C47303 = ".$G2404_C47303."";
                    $LsqlI .= $separador." G2404_C47303";
                    $LsqlV .= $separador.$G2404_C47303;
                    $validar = 1;
                }
            }
  
            $G2404_C47304 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2404_C47304"])){
                if($_POST["G2404_C47304"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2404_C47304 = $_POST["G2404_C47304"];
                    $LsqlU .= $separador." G2404_C47304 = ".$G2404_C47304."";
                    $LsqlI .= $separador." G2404_C47304";
                    $LsqlV .= $separador.$G2404_C47304;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2404_C47305"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47305 = '".$_POST["G2404_C47305"]."'";
                $LsqlI .= $separador."G2404_C47305";
                $LsqlV .= $separador."'".$_POST["G2404_C47305"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C47306"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47306 = '".$_POST["G2404_C47306"]."'";
                $LsqlI .= $separador."G2404_C47306";
                $LsqlV .= $separador."'".$_POST["G2404_C47306"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C47307"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47307 = '".$_POST["G2404_C47307"]."'";
                $LsqlI .= $separador."G2404_C47307";
                $LsqlV .= $separador."'".$_POST["G2404_C47307"]."'";
                $validar = 1;
            }
             
  
            $G2404_C47308 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2404_C47308"])){
                if($_POST["G2404_C47308"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2404_C47308 = $_POST["G2404_C47308"];
                    $LsqlU .= $separador." G2404_C47308 = ".$G2404_C47308."";
                    $LsqlI .= $separador." G2404_C47308";
                    $LsqlV .= $separador.$G2404_C47308;
                    $validar = 1;
                }
            }
  
            $G2404_C55276 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2404_C55276"])){
                if($_POST["G2404_C55276"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2404_C55276 = $_POST["G2404_C55276"];
                    $LsqlU .= $separador." G2404_C55276 = ".$G2404_C55276."";
                    $LsqlI .= $separador." G2404_C55276";
                    $LsqlV .= $separador.$G2404_C55276;
                    $validar = 1;
                }
            }
  
            $G2404_C55277 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2404_C55277"])){
                if($_POST["G2404_C55277"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2404_C55277 = $_POST["G2404_C55277"];
                    $LsqlU .= $separador." G2404_C55277 = ".$G2404_C55277."";
                    $LsqlI .= $separador." G2404_C55277";
                    $LsqlV .= $separador.$G2404_C55277;
                    $validar = 1;
                }
            }
 
            $G2404_C47290 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2404_C47290 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2404_C47290 = ".$G2404_C47290;
                    $LsqlI .= $separador." G2404_C47290";
                    $LsqlV .= $separador.$G2404_C47290;
                    $validar = 1;

                    
                }
            }
 
            $G2404_C47291 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2404_C47291 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2404_C47291 = ".$G2404_C47291;
                    $LsqlI .= $separador." G2404_C47291";
                    $LsqlV .= $separador.$G2404_C47291;
                    $validar = 1;
                }
            }
 
            $G2404_C47292 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2404_C47292 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2404_C47292 = ".$G2404_C47292;
                    $LsqlI .= $separador." G2404_C47292";
                    $LsqlV .= $separador.$G2404_C47292;
                    $validar = 1;
                }
            }
 
            $G2404_C47293 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2404_C47293 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2404_C47293 = ".$G2404_C47293;
                    $LsqlI .= $separador." G2404_C47293";
                    $LsqlV .= $separador.$G2404_C47293;
                    $validar = 1;
                }
            }
 
            $G2404_C47294 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2404_C47294 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2404_C47294 = ".$G2404_C47294;
                    $LsqlI .= $separador." G2404_C47294";
                    $LsqlV .= $separador.$G2404_C47294;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2404_C47295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47295 = '".$_POST["G2404_C47295"]."'";
                $LsqlI .= $separador."G2404_C47295";
                $LsqlV .= $separador."'".$_POST["G2404_C47295"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C47296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47296 = '".$_POST["G2404_C47296"]."'";
                $LsqlI .= $separador."G2404_C47296";
                $LsqlV .= $separador."'".$_POST["G2404_C47296"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C47297"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47297 = '".$_POST["G2404_C47297"]."'";
                $LsqlI .= $separador."G2404_C47297";
                $LsqlV .= $separador."'".$_POST["G2404_C47297"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C47298"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47298 = '".$_POST["G2404_C47298"]."'";
                $LsqlI .= $separador."G2404_C47298";
                $LsqlV .= $separador."'".$_POST["G2404_C47298"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C47299"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C47299 = '".$_POST["G2404_C47299"]."'";
                $LsqlI .= $separador."G2404_C47299";
                $LsqlV .= $separador."'".$_POST["G2404_C47299"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2404_C55939"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_C55939 = '".$_POST["G2404_C55939"]."'";
                $LsqlI .= $separador."G2404_C55939";
                $LsqlV .= $separador."'".$_POST["G2404_C55939"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2404_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2404_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2404_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2404_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2404_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2404_Usuario , G2404_FechaInsercion, G2404_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2404_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2404 WHERE G2404_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
