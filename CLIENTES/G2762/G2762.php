
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2762/G2762_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2762_ConsInte__b as id, G2762_C54566 as camp1 , G2762_C54567 as camp2 FROM ".$BaseDatos.".G2762  WHERE G2762_Usuario = ".$idUsuario." ORDER BY G2762_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2762_ConsInte__b as id, G2762_C54566 as camp1 , G2762_C54567 as camp2 FROM ".$BaseDatos.".G2762  ORDER BY G2762_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2762_ConsInte__b as id, G2762_C54566 as camp1 , G2762_C54567 as camp2 FROM ".$BaseDatos.".G2762 JOIN ".$BaseDatos.".G2762_M".$resultEstpas->muestr." ON G2762_ConsInte__b = G2762_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2762_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2762_ConsInte__b as id, G2762_C54566 as camp1 , G2762_C54567 as camp2 FROM ".$BaseDatos.".G2762 JOIN ".$BaseDatos.".G2762_M".$resultEstpas->muestr." ON G2762_ConsInte__b = G2762_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2762_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2762_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2762_ConsInte__b as id, G2762_C54566 as camp1 , G2762_C54567 as camp2 FROM ".$BaseDatos.".G2762  ORDER BY G2762_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="8282" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54566" id="LblG2762_C54566">CEDULA TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54566" value="<?php if (isset($_GET['G2762_C54566'])) {
                            echo $_GET['G2762_C54566'];
                        } ?>"  name="G2762_C54566"  placeholder="CEDULA TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54567" id="LblG2762_C54567">NOMBRE DEL TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54567" value="<?php if (isset($_GET['G2762_C54567'])) {
                            echo $_GET['G2762_C54567'];
                        } ?>"  name="G2762_C54567"  placeholder="NOMBRE DEL TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54568" id="LblG2762_C54568">DIRECCION DE RESIDENCIA</label><input type="text" class="form-control input-sm" id="G2762_C54568" value="<?php if (isset($_GET['G2762_C54568'])) {
                            echo $_GET['G2762_C54568'];
                        } ?>"  name="G2762_C54568"  placeholder="DIRECCION DE RESIDENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54569" id="LblG2762_C54569">BARRIO DE RESIDENCIA</label><input type="text" class="form-control input-sm" id="G2762_C54569" value="<?php if (isset($_GET['G2762_C54569'])) {
                            echo $_GET['G2762_C54569'];
                        } ?>"  name="G2762_C54569"  placeholder="BARRIO DE RESIDENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54570" id="LblG2762_C54570">CIUDAD DE RESIDENCIA</label><input type="text" class="form-control input-sm" id="G2762_C54570" value="<?php if (isset($_GET['G2762_C54570'])) {
                            echo $_GET['G2762_C54570'];
                        } ?>"  name="G2762_C54570"  placeholder="CIUDAD DE RESIDENCIA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54571" id="LblG2762_C54571">DIRECCION LABORAL</label><input type="text" class="form-control input-sm" id="G2762_C54571" value="<?php if (isset($_GET['G2762_C54571'])) {
                            echo $_GET['G2762_C54571'];
                        } ?>"  name="G2762_C54571"  placeholder="DIRECCION LABORAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54572" id="LblG2762_C54572">BARRIO LABORAL</label><input type="text" class="form-control input-sm" id="G2762_C54572" value="<?php if (isset($_GET['G2762_C54572'])) {
                            echo $_GET['G2762_C54572'];
                        } ?>"  name="G2762_C54572"  placeholder="BARRIO LABORAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54573" id="LblG2762_C54573">CIUDAD LABORAL</label><input type="text" class="form-control input-sm" id="G2762_C54573" value="<?php if (isset($_GET['G2762_C54573'])) {
                            echo $_GET['G2762_C54573'];
                        } ?>"  name="G2762_C54573"  placeholder="CIUDAD LABORAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54574" id="LblG2762_C54574">DIRECCION COMERCIAL</label><input type="text" class="form-control input-sm" id="G2762_C54574" value="<?php if (isset($_GET['G2762_C54574'])) {
                            echo $_GET['G2762_C54574'];
                        } ?>"  name="G2762_C54574"  placeholder="DIRECCION COMERCIAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54575" id="LblG2762_C54575">BARRIO COMERCIAL</label><input type="text" class="form-control input-sm" id="G2762_C54575" value="<?php if (isset($_GET['G2762_C54575'])) {
                            echo $_GET['G2762_C54575'];
                        } ?>"  name="G2762_C54575"  placeholder="BARRIO COMERCIAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54576" id="LblG2762_C54576">CIUDAD COMERCIAL</label><input type="text" class="form-control input-sm" id="G2762_C54576" value="<?php if (isset($_GET['G2762_C54576'])) {
                            echo $_GET['G2762_C54576'];
                        } ?>"  name="G2762_C54576"  placeholder="CIUDAD COMERCIAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54577" id="LblG2762_C54577">TELEFONO 1 DEL TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54577" value="<?php if (isset($_GET['G2762_C54577'])) {
                            echo $_GET['G2762_C54577'];
                        } ?>"  name="G2762_C54577"  placeholder="TELEFONO 1 DEL TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54578" id="LblG2762_C54578">TELEFONO 2 DEL TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54578" value="<?php if (isset($_GET['G2762_C54578'])) {
                            echo $_GET['G2762_C54578'];
                        } ?>"  name="G2762_C54578"  placeholder="TELEFONO 2 DEL TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54579" id="LblG2762_C54579">TELEFONO 3 DEL TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54579" value="<?php if (isset($_GET['G2762_C54579'])) {
                            echo $_GET['G2762_C54579'];
                        } ?>"  name="G2762_C54579"  placeholder="TELEFONO 3 DEL TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54580" id="LblG2762_C54580">TELEFONO 4 DEL TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54580" value="<?php if (isset($_GET['G2762_C54580'])) {
                            echo $_GET['G2762_C54580'];
                        } ?>"  name="G2762_C54580"  placeholder="TELEFONO 4 DEL TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54581" id="LblG2762_C54581">TELEFONO 5 DEL TITULAR</label><input type="text" class="form-control input-sm" id="G2762_C54581" value="<?php if (isset($_GET['G2762_C54581'])) {
                            echo $_GET['G2762_C54581'];
                        } ?>"  name="G2762_C54581"  placeholder="TELEFONO 5 DEL TITULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54582" id="LblG2762_C54582">ASESOR</label><input type="text" class="form-control input-sm" id="G2762_C54582" value="<?php if (isset($_GET['G2762_C54582'])) {
                            echo $_GET['G2762_C54582'];
                        } ?>"  name="G2762_C54582"  placeholder="ASESOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C55963" id="LblG2762_C55963">CARTERA</label><input type="text" class="form-control input-sm" id="G2762_C55963" value="<?php if (isset($_GET['G2762_C55963'])) {
                            echo $_GET['G2762_C55963'];
                        } ?>"  name="G2762_C55963"  placeholder="CARTERA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="8283" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54563" id="LblG2762_C54563">ORIGEN_DY_WF</label><input type="text" class="form-control input-sm" id="G2762_C54563" value="<?php if (isset($_GET['G2762_C54563'])) {
                            echo $_GET['G2762_C54563'];
                        } ?>" readonly name="G2762_C54563"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54564" id="LblG2762_C54564">OPTIN_DY_WF</label><input type="text" class="form-control input-sm" id="G2762_C54564" value="<?php if (isset($_GET['G2762_C54564'])) {
                            echo $_GET['G2762_C54564'];
                        } ?>" readonly name="G2762_C54564"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2762_C54565" id="LblG2762_C54565">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2762_C54565" id="G2762_C54565">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3270 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="8284" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8284c">
                RESULTADO DE GESTIÓN Y PERFIL DEUDOR
            </a>
        </h4>
        
    </div>
    <div id="s_8284c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2762_C54583" id="LblG2762_C54583">TIPO DE CONTANTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2762_C54583" id="G2762_C54583">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3272 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2762_C54584" id="LblG2762_C54584">POSIBILIDAD DE RECAUDO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2762_C54584" id="G2762_C54584">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3273 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2762_C54585" id="LblG2762_C54585">PERFIL DEUDOR </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2762_C54585" id="G2762_C54585">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3274 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8285" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8285c">
                ACTUALIZACION DE DATOS
            </a>
        </h4>
        
    </div>
    <div id="s_8285c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54586" id="LblG2762_C54586">VALOR TOTAL ACUERDO</label><input type="text" class="form-control input-sm" id="G2762_C54586" value="<?php if (isset($_GET['G2762_C54586'])) {
                            echo $_GET['G2762_C54586'];
                        } ?>"  name="G2762_C54586"  placeholder="VALOR TOTAL ACUERDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54587" id="LblG2762_C54587">NUMERO DE CUOTAS</label><input type="text" class="form-control input-sm" id="G2762_C54587" value="<?php if (isset($_GET['G2762_C54587'])) {
                            echo $_GET['G2762_C54587'];
                        } ?>"  name="G2762_C54587"  placeholder="NUMERO DE CUOTAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54588" id="LblG2762_C54588">VALOR CUOTA</label><input type="text" class="form-control input-sm" id="G2762_C54588" value="<?php if (isset($_GET['G2762_C54588'])) {
                            echo $_GET['G2762_C54588'];
                        } ?>"  name="G2762_C54588"  placeholder="VALOR CUOTA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54589" id="LblG2762_C54589">FECHA DE PAGO</label><input type="text" class="form-control input-sm" id="G2762_C54589" value="<?php if (isset($_GET['G2762_C54589'])) {
                            echo $_GET['G2762_C54589'];
                        } ?>"  name="G2762_C54589"  placeholder="FECHA DE PAGO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54590" id="LblG2762_C54590">TELEFONO- WHATSAPP</label><input type="text" class="form-control input-sm" id="G2762_C54590" value="<?php if (isset($_GET['G2762_C54590'])) {
                            echo $_GET['G2762_C54590'];
                        } ?>"  name="G2762_C54590"  placeholder="TELEFONO- WHATSAPP"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54591" id="LblG2762_C54591">CORREO ELECTRONICO</label><input type="text" class="form-control input-sm" id="G2762_C54591" value="<?php if (isset($_GET['G2762_C54591'])) {
                            echo $_GET['G2762_C54591'];
                        } ?>"  name="G2762_C54591"  placeholder="CORREO ELECTRONICO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54592" id="LblG2762_C54592">TELEFONO REFERENCIA PERSONAL</label><input type="text" class="form-control input-sm" id="G2762_C54592" value="<?php if (isset($_GET['G2762_C54592'])) {
                            echo $_GET['G2762_C54592'];
                        } ?>"  name="G2762_C54592"  placeholder="TELEFONO REFERENCIA PERSONAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54593" id="LblG2762_C54593">TELEFONO REFERENCIA FAMILIAR</label><input type="text" class="form-control input-sm" id="G2762_C54593" value="<?php if (isset($_GET['G2762_C54593'])) {
                            echo $_GET['G2762_C54593'];
                        } ?>"  name="G2762_C54593"  placeholder="TELEFONO REFERENCIA FAMILIAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54594" id="LblG2762_C54594">DIRECCION TRABAJO</label><input type="text" class="form-control input-sm" id="G2762_C54594" value="<?php if (isset($_GET['G2762_C54594'])) {
                            echo $_GET['G2762_C54594'];
                        } ?>"  name="G2762_C54594"  placeholder="DIRECCION TRABAJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2762_C54595" id="LblG2762_C54595">TELEFONO TRABAJO </label><input type="text" class="form-control input-sm" id="G2762_C54595" value="<?php if (isset($_GET['G2762_C54595'])) {
                            echo $_GET['G2762_C54595'];
                        } ?>"  name="G2762_C54595"  placeholder="TELEFONO TRABAJO "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">OPERACIONES</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear OPERACIONES" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2762/G2762_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2762_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2762_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2762_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G2762_C54566").val(item.G2762_C54566); 
                $("#G2762_C54567").val(item.G2762_C54567); 
                $("#G2762_C54568").val(item.G2762_C54568); 
                $("#G2762_C54569").val(item.G2762_C54569); 
                $("#G2762_C54570").val(item.G2762_C54570); 
                $("#G2762_C54571").val(item.G2762_C54571); 
                $("#G2762_C54572").val(item.G2762_C54572); 
                $("#G2762_C54573").val(item.G2762_C54573); 
                $("#G2762_C54574").val(item.G2762_C54574); 
                $("#G2762_C54575").val(item.G2762_C54575); 
                $("#G2762_C54576").val(item.G2762_C54576); 
                $("#G2762_C54577").val(item.G2762_C54577); 
                $("#G2762_C54578").val(item.G2762_C54578); 
                $("#G2762_C54579").val(item.G2762_C54579); 
                $("#G2762_C54580").val(item.G2762_C54580); 
                $("#G2762_C54581").val(item.G2762_C54581); 
                $("#G2762_C54582").val(item.G2762_C54582); 
                $("#G2762_C55963").val(item.G2762_C55963); 
                $("#G2762_C54563").val(item.G2762_C54563); 
                $("#G2762_C54564").val(item.G2762_C54564); 
                $("#G2762_C54565").val(item.G2762_C54565).trigger("change");  
                $("#G2762_C54583").val(item.G2762_C54583).trigger("change");  
                $("#G2762_C54584").val(item.G2762_C54584).trigger("change");  
                $("#G2762_C54585").val(item.G2762_C54585).trigger("change");  
                $("#G2762_C54586").val(item.G2762_C54586); 
                $("#G2762_C54587").val(item.G2762_C54587); 
                $("#G2762_C54588").val(item.G2762_C54588); 
                $("#G2762_C54589").val(item.G2762_C54589); 
                $("#G2762_C54590").val(item.G2762_C54590); 
                $("#G2762_C54591").val(item.G2762_C54591); 
                $("#G2762_C54592").val(item.G2762_C54592); 
                $("#G2762_C54593").val(item.G2762_C54593); 
                $("#G2762_C54594").val(item.G2762_C54594); 
                $("#G2762_C54595").val(item.G2762_C54595);
                
                cargarHijos_0(
        $("#G2762_C54566").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G2762_C54566").val());
            var id_0 = $("#G2762_C54566").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G2762_C54566").val());
            var id_0 = $("#G2762_C54566").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G2762_C54566").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2769&view=si&formaDetalle=si&formularioPadre=2762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=54843<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2769&view=si&formaDetalle=si&formularioPadre=2762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=54843&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2769&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=2762&pincheCampo=54843&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G2762_C54565").select2();

    $("#G2762_C54583").select2();

    $("#G2762_C54584").select2();

    $("#G2762_C54585").select2();
        //datepickers
        

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G2762_C54565").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CONTANTO 

    $("#G2762_C54583").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para POSIBILIDAD DE RECAUDO 

    $("#G2762_C54584").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PERFIL DEUDOR  

    $("#G2762_C54585").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2762_C54566").val(item.G2762_C54566);
 
                                                $("#G2762_C54567").val(item.G2762_C54567);
 
                                                $("#G2762_C54568").val(item.G2762_C54568);
 
                                                $("#G2762_C54569").val(item.G2762_C54569);
 
                                                $("#G2762_C54570").val(item.G2762_C54570);
 
                                                $("#G2762_C54571").val(item.G2762_C54571);
 
                                                $("#G2762_C54572").val(item.G2762_C54572);
 
                                                $("#G2762_C54573").val(item.G2762_C54573);
 
                                                $("#G2762_C54574").val(item.G2762_C54574);
 
                                                $("#G2762_C54575").val(item.G2762_C54575);
 
                                                $("#G2762_C54576").val(item.G2762_C54576);
 
                                                $("#G2762_C54577").val(item.G2762_C54577);
 
                                                $("#G2762_C54578").val(item.G2762_C54578);
 
                                                $("#G2762_C54579").val(item.G2762_C54579);
 
                                                $("#G2762_C54580").val(item.G2762_C54580);
 
                                                $("#G2762_C54581").val(item.G2762_C54581);
 
                                                $("#G2762_C54582").val(item.G2762_C54582);
 
                                                $("#G2762_C55963").val(item.G2762_C55963);
 
                                                $("#G2762_C54563").val(item.G2762_C54563);
 
                                                $("#G2762_C54564").val(item.G2762_C54564);
 
                    $("#G2762_C54565").val(item.G2762_C54565).trigger("change"); 
 
                    $("#G2762_C54583").val(item.G2762_C54583).trigger("change"); 
 
                    $("#G2762_C54584").val(item.G2762_C54584).trigger("change"); 
 
                    $("#G2762_C54585").val(item.G2762_C54585).trigger("change"); 
 
                                                $("#G2762_C54586").val(item.G2762_C54586);
 
                                                $("#G2762_C54587").val(item.G2762_C54587);
 
                                                $("#G2762_C54588").val(item.G2762_C54588);
 
                                                $("#G2762_C54589").val(item.G2762_C54589);
 
                                                $("#G2762_C54590").val(item.G2762_C54590);
 
                                                $("#G2762_C54591").val(item.G2762_C54591);
 
                                                $("#G2762_C54592").val(item.G2762_C54592);
 
                                                $("#G2762_C54593").val(item.G2762_C54593);
 
                                                $("#G2762_C54594").val(item.G2762_C54594);
 
                                                $("#G2762_C54595").val(item.G2762_C54595);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CEDULA TITULAR','NOMBRE DEL TITULAR','DIRECCION DE RESIDENCIA','BARRIO DE RESIDENCIA','CIUDAD DE RESIDENCIA','DIRECCION LABORAL','BARRIO LABORAL','CIUDAD LABORAL','DIRECCION COMERCIAL','BARRIO COMERCIAL','CIUDAD COMERCIAL','TELEFONO 1 DEL TITULAR','TELEFONO 2 DEL TITULAR','TELEFONO 3 DEL TITULAR','TELEFONO 4 DEL TITULAR','TELEFONO 5 DEL TITULAR','ASESOR','CARTERA','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','TIPO DE CONTANTO','POSIBILIDAD DE RECAUDO','PERFIL DEUDOR ','VALOR TOTAL ACUERDO','NUMERO DE CUOTAS','VALOR CUOTA','FECHA DE PAGO','TELEFONO- WHATSAPP','CORREO ELECTRONICO','TELEFONO REFERENCIA PERSONAL','TELEFONO REFERENCIA FAMILIAR','DIRECCION TRABAJO','TELEFONO TRABAJO '],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2762_C54566', 
                        index: 'G2762_C54566', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54567', 
                        index: 'G2762_C54567', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54568', 
                        index: 'G2762_C54568', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54569', 
                        index: 'G2762_C54569', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54570', 
                        index: 'G2762_C54570', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54571', 
                        index: 'G2762_C54571', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54572', 
                        index: 'G2762_C54572', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54573', 
                        index: 'G2762_C54573', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54574', 
                        index: 'G2762_C54574', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54575', 
                        index: 'G2762_C54575', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54576', 
                        index: 'G2762_C54576', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54577', 
                        index: 'G2762_C54577', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54578', 
                        index: 'G2762_C54578', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54579', 
                        index: 'G2762_C54579', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54580', 
                        index: 'G2762_C54580', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54581', 
                        index: 'G2762_C54581', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54582', 
                        index: 'G2762_C54582', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C55963', 
                        index: 'G2762_C55963', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54563', 
                        index: 'G2762_C54563', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54564', 
                        index: 'G2762_C54564', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54565', 
                        index:'G2762_C54565', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3270&campo=G2762_C54565'
                        }
                    }

                    ,
                    { 
                        name:'G2762_C54583', 
                        index:'G2762_C54583', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3272&campo=G2762_C54583'
                        }
                    }

                    ,
                    { 
                        name:'G2762_C54584', 
                        index:'G2762_C54584', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3273&campo=G2762_C54584'
                        }
                    }

                    ,
                    { 
                        name:'G2762_C54585', 
                        index:'G2762_C54585', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3274&campo=G2762_C54585'
                        }
                    }

                    ,
                    { 
                        name:'G2762_C54586', 
                        index: 'G2762_C54586', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54587', 
                        index: 'G2762_C54587', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54588', 
                        index: 'G2762_C54588', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54589', 
                        index: 'G2762_C54589', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54590', 
                        index: 'G2762_C54590', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54591', 
                        index: 'G2762_C54591', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54592', 
                        index: 'G2762_C54592', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54593', 
                        index: 'G2762_C54593', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54594', 
                        index: 'G2762_C54594', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2762_C54595', 
                        index: 'G2762_C54595', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2762_C54566',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
                ,subGrid: true,
                subGridRowExpanded: function(subgrid_id, row_id) { 
                    // we pass two parameters 
                    // subgrid_id is a id of the div tag created whitin a table data 
                    // the id of this elemenet is a combination of the "sg_" + id of the row 
                    // the row_id is the id of the row 
                    // If we wan to pass additinal parameters to the url we can use 
                    // a method getRowData(row_id) - which returns associative array in type name-value 
                    // here we can easy construct the flowing 
                    $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','DIAS MORA','CEDULA DEL TITULAR','NUMERO DE OPERACION','SUCURSAL','NOMBRE SUCURSAL','CUENTA','FECHA','SALDO','ESTADO','SALDO INTERES','SALDO COMISION','SALDO IVA','SALDO MORA','OTROS CONCEPTOS','SALDO TOTAL','SALDO SEGURO', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G2769_C54842', 
                                index: 'G2769_C54842', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54843', 
                                index: 'G2769_C54843', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54844', 
                                index: 'G2769_C54844', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54845', 
                                index: 'G2769_C54845', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54846', 
                                index: 'G2769_C54846', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54847', 
                                index: 'G2769_C54847', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54848', 
                                index: 'G2769_C54848', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54849', 
                                index: 'G2769_C54849', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54850', 
                                index: 'G2769_C54850', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54851', 
                                index: 'G2769_C54851', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54852', 
                                index: 'G2769_C54852', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54853', 
                                index: 'G2769_C54853', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54854', 
                                index: 'G2769_C54854', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54855', 
                                index: 'G2769_C54855', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54856', 
                                index: 'G2769_C54856', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2769_C54857', 
                                index: 'G2769_C54857', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G2762_C54566").val(item.G2762_C54566);

                        $("#G2762_C54567").val(item.G2762_C54567);

                        $("#G2762_C54568").val(item.G2762_C54568);

                        $("#G2762_C54569").val(item.G2762_C54569);

                        $("#G2762_C54570").val(item.G2762_C54570);

                        $("#G2762_C54571").val(item.G2762_C54571);

                        $("#G2762_C54572").val(item.G2762_C54572);

                        $("#G2762_C54573").val(item.G2762_C54573);

                        $("#G2762_C54574").val(item.G2762_C54574);

                        $("#G2762_C54575").val(item.G2762_C54575);

                        $("#G2762_C54576").val(item.G2762_C54576);

                        $("#G2762_C54577").val(item.G2762_C54577);

                        $("#G2762_C54578").val(item.G2762_C54578);

                        $("#G2762_C54579").val(item.G2762_C54579);

                        $("#G2762_C54580").val(item.G2762_C54580);

                        $("#G2762_C54581").val(item.G2762_C54581);

                        $("#G2762_C54582").val(item.G2762_C54582);

                        $("#G2762_C55963").val(item.G2762_C55963);

                        $("#G2762_C54563").val(item.G2762_C54563);

                        $("#G2762_C54564").val(item.G2762_C54564);
 
                    $("#G2762_C54565").val(item.G2762_C54565).trigger("change"); 
 
                    $("#G2762_C54583").val(item.G2762_C54583).trigger("change"); 
 
                    $("#G2762_C54584").val(item.G2762_C54584).trigger("change"); 
 
                    $("#G2762_C54585").val(item.G2762_C54585).trigger("change"); 

                        $("#G2762_C54586").val(item.G2762_C54586);

                        $("#G2762_C54587").val(item.G2762_C54587);

                        $("#G2762_C54588").val(item.G2762_C54588);

                        $("#G2762_C54589").val(item.G2762_C54589);

                        $("#G2762_C54590").val(item.G2762_C54590);

                        $("#G2762_C54591").val(item.G2762_C54591);

                        $("#G2762_C54592").val(item.G2762_C54592);

                        $("#G2762_C54593").val(item.G2762_C54593);

                        $("#G2762_C54594").val(item.G2762_C54594);

                        $("#G2762_C54595").val(item.G2762_C54595);
                        
            cargarHijos_0(
        $("#G2762_C54566").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','DIAS MORA','CEDULA DEL TITULAR','NUMERO DE OPERACION','SUCURSAL','NOMBRE SUCURSAL','CUENTA','FECHA','SALDO','ESTADO','SALDO INTERES','SALDO COMISION','SALDO IVA','SALDO MORA','OTROS CONCEPTOS','SALDO TOTAL','SALDO SEGURO', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2769_C54842', 
                        index: 'G2769_C54842', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54843', 
                        index: 'G2769_C54843', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54844', 
                        index: 'G2769_C54844', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54845', 
                        index: 'G2769_C54845', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54846', 
                        index: 'G2769_C54846', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54847', 
                        index: 'G2769_C54847', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54848', 
                        index: 'G2769_C54848', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54849', 
                        index: 'G2769_C54849', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54850', 
                        index: 'G2769_C54850', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54851', 
                        index: 'G2769_C54851', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54852', 
                        index: 'G2769_C54852', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54853', 
                        index: 'G2769_C54853', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54854', 
                        index: 'G2769_C54854', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54855', 
                        index: 'G2769_C54855', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54856', 
                        index: 'G2769_C54856', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2769_C54857', 
                        index: 'G2769_C54857', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G2769_C54842',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'OPERACIONES',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2769&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=54843&formularioPadre=2762<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G2762_C54566").val());
            var id_0 = $("#G2762_C54566").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G2762_C54566").val());
            var id_0 = $("#G2762_C54566").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
