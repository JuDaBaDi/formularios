<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		if($_GET['action'] == "GET_DATOS"){
//          *NBG*2020-05*enviar a la busqueda manual el valor del id en la muestra*  
            if(isset($_GET["id_campana_crm"]) && $_GET["id_campana_crm"] != 0){
                $querymuestra="select CAMPAN_ConsInte__GUION__Pob_b,CAMPAN_ConsInte__MUESTR_b from {$BaseDatos_systema}.CAMPAN where CAMPAN_ConsInte__b={$_GET["id_campana_crm"]}";
                $querymuestra=$mysqli->query($querymuestra);
                if($querymuestra && $querymuestra->num_rows > 0){
                    $datoCampan=$querymuestra->fetch_array();
                    $str_Pobla_Campan = "G{$datoCampan["CAMPAN_ConsInte__GUION__Pob_b"]}";
                    $intMuestra="{$str_Pobla_Campan}_M{$datoCampan["CAMPAN_ConsInte__MUESTR_b"]}";
                    
                }        
            }
            $str_Lsql = " SELECT *,{$intMuestra}_CoInMiPo__b as id_muestra FROM {$BaseDatos}.{$str_Pobla_Campan} LEFT JOIN {$BaseDatos}.{$intMuestra} ON {$str_Pobla_Campan}_ConsInte__b={$intMuestra}_CoInMiPo__b";
                $Where = " WHERE {$str_Pobla_Campan}_ConsInte__b > 0 AND ";
			$usados = 0;
			if(!is_null($_POST['G2762_C54566']) && $_POST['G2762_C54566'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54566 LIKE '%". $_POST['G2762_C54566'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2762_C54567']) && $_POST['G2762_C54567'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54567 LIKE '%". $_POST['G2762_C54567'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2762_C54577']) && $_POST['G2762_C54577'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54577 LIKE '%". $_POST['G2762_C54577'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2762_C54578']) && $_POST['G2762_C54578'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54578 LIKE '%". $_POST['G2762_C54578'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2762_C54579']) && $_POST['G2762_C54579'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54579 LIKE '%". $_POST['G2762_C54579'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2762_C54580']) && $_POST['G2762_C54580'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54580 LIKE '%". $_POST['G2762_C54580'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2762_C54581']) && $_POST['G2762_C54581'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2762_C54581 LIKE '%". $_POST['G2762_C54581'] ."%' ";
				$usados = 1;
			}
            if($usados==0){
                    $Where = "";
                }
                $str_Lsql .= $Where;
                
            //echo $str_Lsql;
			$resultado = $mysqli->query($str_Lsql);
			$arrayDatos = array();
			while ($key = $resultado->fetch_assoc()) {
				$arrayDatos[] = $key;
			}

			$newJson = array();
			$newJson[0]['cantidad_registros'] = $resultado->num_rows;
			$newJson[0]['registros'] = $arrayDatos;
            if($usados == 0 && $resultado->num_rows == 0){
                $newJson[0]['mensaje'] = "No se encontraron registros para este tipo de busqueda";
            }else{
                if($usados == 1 && $resultado->num_rows == 0){
                    $strSQLValidaReg="SELECT * FROM {$BaseDatos}.{$str_Pobla_Campan}{$Where}";
                       $strSQLValidaReg=$mysqli->query($strSQLValidaReg);
                    if($strSQLValidaReg ->num_rows > 0){
                        $newJson[0]['mensaje'] = "No tienes permiso para acceder a este registro";
                    }
                }
            }

			echo json_encode($newJson);
		}
	}
?>
