<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2762_ConsInte__b, G2762_FechaInsercion , G2762_Usuario ,  G2762_CodigoMiembro  , G2762_PoblacionOrigen , G2762_EstadoDiligenciamiento ,  G2762_IdLlamada , G2762_C54566 as principal ,G2762_C54566,G2762_C54567,G2762_C54568,G2762_C54569,G2762_C54570,G2762_C54571,G2762_C54572,G2762_C54573,G2762_C54574,G2762_C54575,G2762_C54576,G2762_C54577,G2762_C54578,G2762_C54579,G2762_C54580,G2762_C54581,G2762_C54582,G2762_C55963,G2762_C54563,G2762_C54564,G2762_C54565,G2762_C54583,G2762_C54584,G2762_C54585,G2762_C54586,G2762_C54587,G2762_C54588,G2762_C54589,G2762_C54590,G2762_C54591,G2762_C54592,G2762_C54593,G2762_C54594,G2762_C54595 FROM '.$BaseDatos.'.G2762 WHERE G2762_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2762_C54566'] = $key->G2762_C54566;

                $datos[$i]['G2762_C54567'] = $key->G2762_C54567;

                $datos[$i]['G2762_C54568'] = $key->G2762_C54568;

                $datos[$i]['G2762_C54569'] = $key->G2762_C54569;

                $datos[$i]['G2762_C54570'] = $key->G2762_C54570;

                $datos[$i]['G2762_C54571'] = $key->G2762_C54571;

                $datos[$i]['G2762_C54572'] = $key->G2762_C54572;

                $datos[$i]['G2762_C54573'] = $key->G2762_C54573;

                $datos[$i]['G2762_C54574'] = $key->G2762_C54574;

                $datos[$i]['G2762_C54575'] = $key->G2762_C54575;

                $datos[$i]['G2762_C54576'] = $key->G2762_C54576;

                $datos[$i]['G2762_C54577'] = $key->G2762_C54577;

                $datos[$i]['G2762_C54578'] = $key->G2762_C54578;

                $datos[$i]['G2762_C54579'] = $key->G2762_C54579;

                $datos[$i]['G2762_C54580'] = $key->G2762_C54580;

                $datos[$i]['G2762_C54581'] = $key->G2762_C54581;

                $datos[$i]['G2762_C54582'] = $key->G2762_C54582;

                $datos[$i]['G2762_C55963'] = $key->G2762_C55963;

                $datos[$i]['G2762_C54563'] = $key->G2762_C54563;

                $datos[$i]['G2762_C54564'] = $key->G2762_C54564;

                $datos[$i]['G2762_C54565'] = $key->G2762_C54565;

                $datos[$i]['G2762_C54583'] = $key->G2762_C54583;

                $datos[$i]['G2762_C54584'] = $key->G2762_C54584;

                $datos[$i]['G2762_C54585'] = $key->G2762_C54585;

                $datos[$i]['G2762_C54586'] = $key->G2762_C54586;

                $datos[$i]['G2762_C54587'] = $key->G2762_C54587;

                $datos[$i]['G2762_C54588'] = $key->G2762_C54588;

                $datos[$i]['G2762_C54589'] = $key->G2762_C54589;

                $datos[$i]['G2762_C54590'] = $key->G2762_C54590;

                $datos[$i]['G2762_C54591'] = $key->G2762_C54591;

                $datos[$i]['G2762_C54592'] = $key->G2762_C54592;

                $datos[$i]['G2762_C54593'] = $key->G2762_C54593;

                $datos[$i]['G2762_C54594'] = $key->G2762_C54594;

                $datos[$i]['G2762_C54595'] = $key->G2762_C54595;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2762";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2762_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2762_ConsInte__b as id,  G2762_C54566 as camp1 , G2762_C54567 as camp2 
                     FROM ".$BaseDatos.".G2762  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2762_ConsInte__b as id,  G2762_C54566 as camp1 , G2762_C54567 as camp2  
                    FROM ".$BaseDatos.".G2762  JOIN ".$BaseDatos.".G2762_M".$_POST['muestra']." ON G2762_ConsInte__b = G2762_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2762_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2762_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2762_C54566 LIKE '%".$B."%' OR G2762_C54567 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2762_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2762");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2762_ConsInte__b, G2762_FechaInsercion , G2762_Usuario ,  G2762_CodigoMiembro  , G2762_PoblacionOrigen , G2762_EstadoDiligenciamiento ,  G2762_IdLlamada , G2762_C54566 as principal ,G2762_C54566,G2762_C54567,G2762_C54568,G2762_C54569,G2762_C54570,G2762_C54571,G2762_C54572,G2762_C54573,G2762_C54574,G2762_C54575,G2762_C54576,G2762_C54577,G2762_C54578,G2762_C54579,G2762_C54580,G2762_C54581,G2762_C54582,G2762_C55963,G2762_C54563,G2762_C54564, a.LISOPC_Nombre____b as G2762_C54565, b.LISOPC_Nombre____b as G2762_C54583, c.LISOPC_Nombre____b as G2762_C54584, d.LISOPC_Nombre____b as G2762_C54585,G2762_C54586,G2762_C54587,G2762_C54588,G2762_C54589,G2762_C54590,G2762_C54591,G2762_C54592,G2762_C54593,G2762_C54594,G2762_C54595 FROM '.$BaseDatos.'.G2762 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2762_C54565 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2762_C54583 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2762_C54584 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2762_C54585';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2762_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2762_ConsInte__b , ($fila->G2762_C54566) , ($fila->G2762_C54567) , ($fila->G2762_C54568) , ($fila->G2762_C54569) , ($fila->G2762_C54570) , ($fila->G2762_C54571) , ($fila->G2762_C54572) , ($fila->G2762_C54573) , ($fila->G2762_C54574) , ($fila->G2762_C54575) , ($fila->G2762_C54576) , ($fila->G2762_C54577) , ($fila->G2762_C54578) , ($fila->G2762_C54579) , ($fila->G2762_C54580) , ($fila->G2762_C54581) , ($fila->G2762_C54582) , ($fila->G2762_C55963) , ($fila->G2762_C54563) , ($fila->G2762_C54564) , ($fila->G2762_C54565) , ($fila->G2762_C54583) , ($fila->G2762_C54584) , ($fila->G2762_C54585) , ($fila->G2762_C54586) , ($fila->G2762_C54587) , ($fila->G2762_C54588) , ($fila->G2762_C54589) , ($fila->G2762_C54590) , ($fila->G2762_C54591) , ($fila->G2762_C54592) , ($fila->G2762_C54593) , ($fila->G2762_C54594) , ($fila->G2762_C54595) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2762 WHERE G2762_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2762";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2762_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2762_ConsInte__b as id,  G2762_C54566 as camp1 , G2762_C54567 as camp2  FROM '.$BaseDatos.'.G2762 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2762_ConsInte__b as id,  G2762_C54566 as camp1 , G2762_C54567 as camp2  
                    FROM ".$BaseDatos.".G2762  JOIN ".$BaseDatos.".G2762_M".$_POST['muestra']." ON G2762_ConsInte__b = G2762_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2762_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2762_C54566 LIKE "%'.$B.'%" OR G2762_C54567 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2762_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2762 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2762(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2762_C54566"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54566 = '".$_POST["G2762_C54566"]."'";
                $LsqlI .= $separador."G2762_C54566";
                $LsqlV .= $separador."'".$_POST["G2762_C54566"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54567"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54567 = '".$_POST["G2762_C54567"]."'";
                $LsqlI .= $separador."G2762_C54567";
                $LsqlV .= $separador."'".$_POST["G2762_C54567"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54568"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54568 = '".$_POST["G2762_C54568"]."'";
                $LsqlI .= $separador."G2762_C54568";
                $LsqlV .= $separador."'".$_POST["G2762_C54568"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54569"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54569 = '".$_POST["G2762_C54569"]."'";
                $LsqlI .= $separador."G2762_C54569";
                $LsqlV .= $separador."'".$_POST["G2762_C54569"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54570"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54570 = '".$_POST["G2762_C54570"]."'";
                $LsqlI .= $separador."G2762_C54570";
                $LsqlV .= $separador."'".$_POST["G2762_C54570"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54571"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54571 = '".$_POST["G2762_C54571"]."'";
                $LsqlI .= $separador."G2762_C54571";
                $LsqlV .= $separador."'".$_POST["G2762_C54571"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54572"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54572 = '".$_POST["G2762_C54572"]."'";
                $LsqlI .= $separador."G2762_C54572";
                $LsqlV .= $separador."'".$_POST["G2762_C54572"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54573"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54573 = '".$_POST["G2762_C54573"]."'";
                $LsqlI .= $separador."G2762_C54573";
                $LsqlV .= $separador."'".$_POST["G2762_C54573"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54574"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54574 = '".$_POST["G2762_C54574"]."'";
                $LsqlI .= $separador."G2762_C54574";
                $LsqlV .= $separador."'".$_POST["G2762_C54574"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54575"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54575 = '".$_POST["G2762_C54575"]."'";
                $LsqlI .= $separador."G2762_C54575";
                $LsqlV .= $separador."'".$_POST["G2762_C54575"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54576"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54576 = '".$_POST["G2762_C54576"]."'";
                $LsqlI .= $separador."G2762_C54576";
                $LsqlV .= $separador."'".$_POST["G2762_C54576"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54577"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54577 = '".$_POST["G2762_C54577"]."'";
                $LsqlI .= $separador."G2762_C54577";
                $LsqlV .= $separador."'".$_POST["G2762_C54577"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54578"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54578 = '".$_POST["G2762_C54578"]."'";
                $LsqlI .= $separador."G2762_C54578";
                $LsqlV .= $separador."'".$_POST["G2762_C54578"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54579"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54579 = '".$_POST["G2762_C54579"]."'";
                $LsqlI .= $separador."G2762_C54579";
                $LsqlV .= $separador."'".$_POST["G2762_C54579"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54580"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54580 = '".$_POST["G2762_C54580"]."'";
                $LsqlI .= $separador."G2762_C54580";
                $LsqlV .= $separador."'".$_POST["G2762_C54580"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54581"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54581 = '".$_POST["G2762_C54581"]."'";
                $LsqlI .= $separador."G2762_C54581";
                $LsqlV .= $separador."'".$_POST["G2762_C54581"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54582"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54582 = '".$_POST["G2762_C54582"]."'";
                $LsqlI .= $separador."G2762_C54582";
                $LsqlV .= $separador."'".$_POST["G2762_C54582"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C55963"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C55963 = '".$_POST["G2762_C55963"]."'";
                $LsqlI .= $separador."G2762_C55963";
                $LsqlV .= $separador."'".$_POST["G2762_C55963"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54563"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54563 = '".$_POST["G2762_C54563"]."'";
                $LsqlI .= $separador."G2762_C54563";
                $LsqlV .= $separador."'".$_POST["G2762_C54563"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54564"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54564 = '".$_POST["G2762_C54564"]."'";
                $LsqlI .= $separador."G2762_C54564";
                $LsqlV .= $separador."'".$_POST["G2762_C54564"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54565"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54565 = '".$_POST["G2762_C54565"]."'";
                $LsqlI .= $separador."G2762_C54565";
                $LsqlV .= $separador."'".$_POST["G2762_C54565"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54583"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54583 = '".$_POST["G2762_C54583"]."'";
                $LsqlI .= $separador."G2762_C54583";
                $LsqlV .= $separador."'".$_POST["G2762_C54583"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54584"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54584 = '".$_POST["G2762_C54584"]."'";
                $LsqlI .= $separador."G2762_C54584";
                $LsqlV .= $separador."'".$_POST["G2762_C54584"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54585"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54585 = '".$_POST["G2762_C54585"]."'";
                $LsqlI .= $separador."G2762_C54585";
                $LsqlV .= $separador."'".$_POST["G2762_C54585"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54586"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54586 = '".$_POST["G2762_C54586"]."'";
                $LsqlI .= $separador."G2762_C54586";
                $LsqlV .= $separador."'".$_POST["G2762_C54586"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54587"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54587 = '".$_POST["G2762_C54587"]."'";
                $LsqlI .= $separador."G2762_C54587";
                $LsqlV .= $separador."'".$_POST["G2762_C54587"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54588"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54588 = '".$_POST["G2762_C54588"]."'";
                $LsqlI .= $separador."G2762_C54588";
                $LsqlV .= $separador."'".$_POST["G2762_C54588"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54589"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54589 = '".$_POST["G2762_C54589"]."'";
                $LsqlI .= $separador."G2762_C54589";
                $LsqlV .= $separador."'".$_POST["G2762_C54589"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54590 = '".$_POST["G2762_C54590"]."'";
                $LsqlI .= $separador."G2762_C54590";
                $LsqlV .= $separador."'".$_POST["G2762_C54590"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54591"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54591 = '".$_POST["G2762_C54591"]."'";
                $LsqlI .= $separador."G2762_C54591";
                $LsqlV .= $separador."'".$_POST["G2762_C54591"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54592"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54592 = '".$_POST["G2762_C54592"]."'";
                $LsqlI .= $separador."G2762_C54592";
                $LsqlV .= $separador."'".$_POST["G2762_C54592"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54593"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54593 = '".$_POST["G2762_C54593"]."'";
                $LsqlI .= $separador."G2762_C54593";
                $LsqlV .= $separador."'".$_POST["G2762_C54593"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54594"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54594 = '".$_POST["G2762_C54594"]."'";
                $LsqlI .= $separador."G2762_C54594";
                $LsqlV .= $separador."'".$_POST["G2762_C54594"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2762_C54595"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_C54595 = '".$_POST["G2762_C54595"]."'";
                $LsqlI .= $separador."G2762_C54595";
                $LsqlV .= $separador."'".$_POST["G2762_C54595"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2762_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2762_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2762_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2762_Usuario , G2762_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2762_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2762 WHERE G2762_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2762 SET G2762_UltiGest__b =-14, G2762_GesMasImp_b =-14, G2762_TipoReintentoUG_b =0, G2762_TipoReintentoGMI_b =0, G2762_EstadoUG_b =-14, G2762_EstadoGMI_b =-14, G2762_CantidadIntentos =0, G2762_CantidadIntentosGMI_b =0 WHERE G2762_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2769_ConsInte__b, G2769_C54842, G2769_C54843, G2769_C54844, G2769_C54845, G2769_C54846, G2769_C54847, G2769_C54848, G2769_C54849, G2769_C54850, G2769_C54851, G2769_C54852, G2769_C54853, G2769_C54854, G2769_C54855, G2769_C54856, G2769_C54857 FROM ".$BaseDatos.".G2769  ";

        $SQL .= " WHERE G2769_C54843 = '".$numero."'"; 

        $SQL .= " ORDER BY G2769_C54842";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2769_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2769_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2769_C54842)."</cell>";

                echo "<cell>". ($fila->G2769_C54843)."</cell>";

                echo "<cell>". ($fila->G2769_C54844)."</cell>";

                echo "<cell>". ($fila->G2769_C54845)."</cell>";

                echo "<cell>". ($fila->G2769_C54846)."</cell>";

                echo "<cell>". ($fila->G2769_C54847)."</cell>";

                echo "<cell>". ($fila->G2769_C54848)."</cell>";

                echo "<cell>". ($fila->G2769_C54849)."</cell>";

                echo "<cell>". ($fila->G2769_C54850)."</cell>";

                echo "<cell>". ($fila->G2769_C54851)."</cell>";

                echo "<cell>". ($fila->G2769_C54852)."</cell>";

                echo "<cell>". ($fila->G2769_C54853)."</cell>";

                echo "<cell>". ($fila->G2769_C54854)."</cell>";

                echo "<cell>". ($fila->G2769_C54855)."</cell>";

                echo "<cell>". ($fila->G2769_C54856)."</cell>";

                echo "<cell>". ($fila->G2769_C54857)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2769 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2769(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2769_C54842"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54842 = '".$_POST["G2769_C54842"]."'";
                    $LsqlI .= $separador."G2769_C54842";
                    $LsqlV .= $separador."'".$_POST["G2769_C54842"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54844"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54844 = '".$_POST["G2769_C54844"]."'";
                    $LsqlI .= $separador."G2769_C54844";
                    $LsqlV .= $separador."'".$_POST["G2769_C54844"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54845"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54845 = '".$_POST["G2769_C54845"]."'";
                    $LsqlI .= $separador."G2769_C54845";
                    $LsqlV .= $separador."'".$_POST["G2769_C54845"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54846"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54846 = '".$_POST["G2769_C54846"]."'";
                    $LsqlI .= $separador."G2769_C54846";
                    $LsqlV .= $separador."'".$_POST["G2769_C54846"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54847"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54847 = '".$_POST["G2769_C54847"]."'";
                    $LsqlI .= $separador."G2769_C54847";
                    $LsqlV .= $separador."'".$_POST["G2769_C54847"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54848"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54848 = '".$_POST["G2769_C54848"]."'";
                    $LsqlI .= $separador."G2769_C54848";
                    $LsqlV .= $separador."'".$_POST["G2769_C54848"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54849"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54849 = '".$_POST["G2769_C54849"]."'";
                    $LsqlI .= $separador."G2769_C54849";
                    $LsqlV .= $separador."'".$_POST["G2769_C54849"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54850"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54850 = '".$_POST["G2769_C54850"]."'";
                    $LsqlI .= $separador."G2769_C54850";
                    $LsqlV .= $separador."'".$_POST["G2769_C54850"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54851"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54851 = '".$_POST["G2769_C54851"]."'";
                    $LsqlI .= $separador."G2769_C54851";
                    $LsqlV .= $separador."'".$_POST["G2769_C54851"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54852"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54852 = '".$_POST["G2769_C54852"]."'";
                    $LsqlI .= $separador."G2769_C54852";
                    $LsqlV .= $separador."'".$_POST["G2769_C54852"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54853"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54853 = '".$_POST["G2769_C54853"]."'";
                    $LsqlI .= $separador."G2769_C54853";
                    $LsqlV .= $separador."'".$_POST["G2769_C54853"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54854"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54854 = '".$_POST["G2769_C54854"]."'";
                    $LsqlI .= $separador."G2769_C54854";
                    $LsqlV .= $separador."'".$_POST["G2769_C54854"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54855"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54855 = '".$_POST["G2769_C54855"]."'";
                    $LsqlI .= $separador."G2769_C54855";
                    $LsqlV .= $separador."'".$_POST["G2769_C54855"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54856"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54856 = '".$_POST["G2769_C54856"]."'";
                    $LsqlI .= $separador."G2769_C54856";
                    $LsqlV .= $separador."'".$_POST["G2769_C54856"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2769_C54857"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2769_C54857 = '".$_POST["G2769_C54857"]."'";
                    $LsqlI .= $separador."G2769_C54857";
                    $LsqlV .= $separador."'".$_POST["G2769_C54857"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2769_C54843 = $numero;
                    $LsqlU .= ", G2769_C54843 = ".$G2769_C54843."";
                    $LsqlI .= ", G2769_C54843";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2769_Usuario ,  G2769_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2769_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2769 WHERE  G2769_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
