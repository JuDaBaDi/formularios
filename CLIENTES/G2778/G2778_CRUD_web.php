<script>
    document.cookie = "same-site-cookie=foo; SameSite=Lax"; 
    document.cookie = "cross-site-cookie=bar; SameSite=None; Secure";
</script>
            
<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    define("RECAPTCHA_V3_SECRET_KEY", "6Lcc1dYUAAAAAHgqTohTDsl2g-0V5-egYLC4atVb");
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   
    
        //Inserciones o actualizaciones
        if(isset($_POST["oper"])){
            $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G2778 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G2778( G2778_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G2778_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2778_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G2778_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2778 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    $str_LsqlU .= $separador."G2778_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G2778_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2778 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    $str_LsqlU .= $separador."G2778_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G2778_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;

                $strParamPaso_t ='';

                if (isset($_POST["pasoId"])) {
                    
                    $strParamPaso_t = '&paso='.$_POST["pasoId"];

                    DispararProceso($_POST["pasoId"]);
                
                }

                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=Mjc3OA==&result=1'.$strParamPaso_t);

            } else {
                echo "Error Haciendo el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
