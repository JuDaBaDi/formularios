
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2778/G2778_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2778_ConsInte__b as id, G2778_C54969 as camp1 , G2778_C54970 as camp2 FROM ".$BaseDatos.".G2778  WHERE G2778_Usuario = ".$idUsuario." ORDER BY G2778_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2778_ConsInte__b as id, G2778_C54969 as camp1 , G2778_C54970 as camp2 FROM ".$BaseDatos.".G2778  ORDER BY G2778_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2778_ConsInte__b as id, G2778_C54969 as camp1 , G2778_C54970 as camp2 FROM ".$BaseDatos.".G2778 JOIN ".$BaseDatos.".G2778_M".$resultEstpas->muestr." ON G2778_ConsInte__b = G2778_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2778_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2778_ConsInte__b as id, G2778_C54969 as camp1 , G2778_C54970 as camp2 FROM ".$BaseDatos.".G2778 JOIN ".$BaseDatos.".G2778_M".$resultEstpas->muestr." ON G2778_ConsInte__b = G2778_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2778_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2778_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2778_ConsInte__b as id, G2778_C54969 as camp1 , G2778_C54970 as camp2 FROM ".$BaseDatos.".G2778  ORDER BY G2778_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="8340" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54969" id="LblG2778_C54969">CEDULA</label><input type="text" class="form-control input-sm" id="G2778_C54969" value="<?php if (isset($_GET['G2778_C54969'])) {
                            echo $_GET['G2778_C54969'];
                        } ?>"  name="G2778_C54969"  placeholder="CEDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54970" id="LblG2778_C54970">GIRADOR</label><input type="text" class="form-control input-sm" id="G2778_C54970" value="<?php if (isset($_GET['G2778_C54970'])) {
                            echo $_GET['G2778_C54970'];
                        } ?>"  name="G2778_C54970"  placeholder="GIRADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54971" id="LblG2778_C54971">TELEFONO1</label><input type="text" class="form-control input-sm" id="G2778_C54971" value="<?php if (isset($_GET['G2778_C54971'])) {
                            echo $_GET['G2778_C54971'];
                        } ?>"  name="G2778_C54971"  placeholder="TELEFONO1"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54972" id="LblG2778_C54972">TELEFONO2</label><input type="text" class="form-control input-sm" id="G2778_C54972" value="<?php if (isset($_GET['G2778_C54972'])) {
                            echo $_GET['G2778_C54972'];
                        } ?>"  name="G2778_C54972"  placeholder="TELEFONO2"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54973" id="LblG2778_C54973">TELEFONO3</label><input type="text" class="form-control input-sm" id="G2778_C54973" value="<?php if (isset($_GET['G2778_C54973'])) {
                            echo $_GET['G2778_C54973'];
                        } ?>"  name="G2778_C54973"  placeholder="TELEFONO3"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54974" id="LblG2778_C54974">TELEFONO4</label><input type="text" class="form-control input-sm" id="G2778_C54974" value="<?php if (isset($_GET['G2778_C54974'])) {
                            echo $_GET['G2778_C54974'];
                        } ?>"  name="G2778_C54974"  placeholder="TELEFONO4"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54975" id="LblG2778_C54975">TELEFONO5</label><input type="text" class="form-control input-sm" id="G2778_C54975" value="<?php if (isset($_GET['G2778_C54975'])) {
                            echo $_GET['G2778_C54975'];
                        } ?>"  name="G2778_C54975"  placeholder="TELEFONO5"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54976" id="LblG2778_C54976">FECHA MEMO</label><input type="text" class="form-control input-sm" id="G2778_C54976" value="<?php if (isset($_GET['G2778_C54976'])) {
                            echo $_GET['G2778_C54976'];
                        } ?>"  name="G2778_C54976"  placeholder="FECHA MEMO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54977" id="LblG2778_C54977">SALDO</label><input type="text" class="form-control input-sm" id="G2778_C54977" value="<?php if (isset($_GET['G2778_C54977'])) {
                            echo $_GET['G2778_C54977'];
                        } ?>"  name="G2778_C54977"  placeholder="SALDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54978" id="LblG2778_C54978">INTERESES</label><input type="text" class="form-control input-sm" id="G2778_C54978" value="<?php if (isset($_GET['G2778_C54978'])) {
                            echo $_GET['G2778_C54978'];
                        } ?>"  name="G2778_C54978"  placeholder="INTERESES"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54979" id="LblG2778_C54979">HONORARIOS</label><input type="text" class="form-control input-sm" id="G2778_C54979" value="<?php if (isset($_GET['G2778_C54979'])) {
                            echo $_GET['G2778_C54979'];
                        } ?>"  name="G2778_C54979"  placeholder="HONORARIOS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54980" id="LblG2778_C54980">HOGCA</label><input type="text" class="form-control input-sm" id="G2778_C54980" value="<?php if (isset($_GET['G2778_C54980'])) {
                            echo $_GET['G2778_C54980'];
                        } ?>"  name="G2778_C54980"  placeholder="HOGCA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54981" id="LblG2778_C54981">SANCION</label><input type="text" class="form-control input-sm" id="G2778_C54981" value="<?php if (isset($_GET['G2778_C54981'])) {
                            echo $_GET['G2778_C54981'];
                        } ?>"  name="G2778_C54981"  placeholder="SANCION"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54982" id="LblG2778_C54982">TOTAL A PAGAR</label><input type="text" class="form-control input-sm" id="G2778_C54982" value="<?php if (isset($_GET['G2778_C54982'])) {
                            echo $_GET['G2778_C54982'];
                        } ?>"  name="G2778_C54982"  placeholder="TOTAL A PAGAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54983" id="LblG2778_C54983">MOT_COBRADOR</label><input type="text" class="form-control input-sm" id="G2778_C54983" value="<?php if (isset($_GET['G2778_C54983'])) {
                            echo $_GET['G2778_C54983'];
                        } ?>"  name="G2778_C54983"  placeholder="MOT_COBRADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54984" id="LblG2778_C54984">NOMBRE AFILIADO</label><input type="text" class="form-control input-sm" id="G2778_C54984" value="<?php if (isset($_GET['G2778_C54984'])) {
                            echo $_GET['G2778_C54984'];
                        } ?>"  name="G2778_C54984"  placeholder="NOMBRE AFILIADO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54985" id="LblG2778_C54985">DIRECCION</label><input type="text" class="form-control input-sm" id="G2778_C54985" value="<?php if (isset($_GET['G2778_C54985'])) {
                            echo $_GET['G2778_C54985'];
                        } ?>"  name="G2778_C54985"  placeholder="DIRECCION"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54986" id="LblG2778_C54986">CIUDAD</label><input type="text" class="form-control input-sm" id="G2778_C54986" value="<?php if (isset($_GET['G2778_C54986'])) {
                            echo $_GET['G2778_C54986'];
                        } ?>"  name="G2778_C54986"  placeholder="CIUDAD"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54987" id="LblG2778_C54987">AGENTE</label><input type="text" class="form-control input-sm" id="G2778_C54987" value="<?php if (isset($_GET['G2778_C54987'])) {
                            echo $_GET['G2778_C54987'];
                        } ?>"  name="G2778_C54987"  placeholder="AGENTE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54988" id="LblG2778_C54988">ASESOR</label><input type="text" class="form-control input-sm" id="G2778_C54988" value="<?php if (isset($_GET['G2778_C54988'])) {
                            echo $_GET['G2778_C54988'];
                        } ?>"  name="G2778_C54988"  placeholder="ASESOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C56638" id="LblG2778_C56638">ESTADO(cargue)</label><input type="text" class="form-control input-sm" id="G2778_C56638" value="<?php if (isset($_GET['G2778_C56638'])) {
                            echo $_GET['G2778_C56638'];
                        } ?>"  name="G2778_C56638"  placeholder="ESTADO(cargue)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


</div>

<div  id="8341" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54966" id="LblG2778_C54966">ORIGEN_DY_WF</label><input type="text" class="form-control input-sm" id="G2778_C54966" value="<?php if (isset($_GET['G2778_C54966'])) {
                            echo $_GET['G2778_C54966'];
                        } ?>" readonly name="G2778_C54966"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54967" id="LblG2778_C54967">OPTIN_DY_WF</label><input type="text" class="form-control input-sm" id="G2778_C54967" value="<?php if (isset($_GET['G2778_C54967'])) {
                            echo $_GET['G2778_C54967'];
                        } ?>" readonly name="G2778_C54967"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2778_C54968" id="LblG2778_C54968">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2778_C54968" id="G2778_C54968">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3297 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="8342" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8342c">
                PERFIL DEUDOR 
            </a>
        </h4>
        
    </div>
    <div id="s_8342c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2778_C54989" id="LblG2778_C54989">ESTADO ACTUAL </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2778_C54989" id="G2778_C54989">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2575 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54990" id="LblG2778_C54990">LUGAR DE TRABAJO</label><input type="text" class="form-control input-sm" id="G2778_C54990" value="<?php if (isset($_GET['G2778_C54990'])) {
                            echo $_GET['G2778_C54990'];
                        } ?>"  name="G2778_C54990"  placeholder="LUGAR DE TRABAJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54991" id="LblG2778_C54991">CARGO O ACTIVIDAD ECONOMICA</label><input type="text" class="form-control input-sm" id="G2778_C54991" value="<?php if (isset($_GET['G2778_C54991'])) {
                            echo $_GET['G2778_C54991'];
                        } ?>"  name="G2778_C54991"  placeholder="CARGO O ACTIVIDAD ECONOMICA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54992" id="LblG2778_C54992">NOMBRE DE LA EMPRESA</label><input type="text" class="form-control input-sm" id="G2778_C54992" value="<?php if (isset($_GET['G2778_C54992'])) {
                            echo $_GET['G2778_C54992'];
                        } ?>"  name="G2778_C54992"  placeholder="NOMBRE DE LA EMPRESA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54993" id="LblG2778_C54993">DIRECCION ACTUALIZADA</label><input type="text" class="form-control input-sm" id="G2778_C54993" value="<?php if (isset($_GET['G2778_C54993'])) {
                            echo $_GET['G2778_C54993'];
                        } ?>"  name="G2778_C54993"  placeholder="DIRECCION ACTUALIZADA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54994" id="LblG2778_C54994">TELEFONO WHATSAPP </label><input type="text" class="form-control input-sm" id="G2778_C54994" value="<?php if (isset($_GET['G2778_C54994'])) {
                            echo $_GET['G2778_C54994'];
                        } ?>"  name="G2778_C54994"  placeholder="TELEFONO WHATSAPP "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54995" id="LblG2778_C54995">CORREO ELECTRONICO</label><input type="text" class="form-control input-sm" id="G2778_C54995" value="<?php if (isset($_GET['G2778_C54995'])) {
                            echo $_GET['G2778_C54995'];
                        } ?>"  name="G2778_C54995"  placeholder="CORREO ELECTRONICO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54996" id="LblG2778_C54996">REFERENCIA PERSONAL</label><input type="text" class="form-control input-sm" id="G2778_C54996" value="<?php if (isset($_GET['G2778_C54996'])) {
                            echo $_GET['G2778_C54996'];
                        } ?>"  name="G2778_C54996"  placeholder="REFERENCIA PERSONAL"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54997" id="LblG2778_C54997">REFERENCIA FAMILIAR</label><input type="text" class="form-control input-sm" id="G2778_C54997" value="<?php if (isset($_GET['G2778_C54997'])) {
                            echo $_GET['G2778_C54997'];
                        } ?>"  name="G2778_C54997"  placeholder="REFERENCIA FAMILIAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54998" id="LblG2778_C54998">TELEFONO REFERENCIA PERSONAL </label><input type="text" class="form-control input-sm" id="G2778_C54998" value="<?php if (isset($_GET['G2778_C54998'])) {
                            echo $_GET['G2778_C54998'];
                        } ?>"  name="G2778_C54998"  placeholder="TELEFONO REFERENCIA PERSONAL "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C54999" id="LblG2778_C54999">TELEFONO REFERENCIA FAMILIAR</label><input type="text" class="form-control input-sm" id="G2778_C54999" value="<?php if (isset($_GET['G2778_C54999'])) {
                            echo $_GET['G2778_C54999'];
                        } ?>"  name="G2778_C54999"  placeholder="TELEFONO REFERENCIA FAMILIAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8343" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8343c">
                CONTACTO
            </a>
        </h4>
        
    </div>
    <div id="s_8343c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2778_C55000" id="LblG2778_C55000">TIPO DE CONTACTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2778_C55000" id="G2778_C55000">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2902 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2778_C55001" id="LblG2778_C55001">RESULTADO CONTACTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2778_C55001" id="G2778_C55001">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2590 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2778_C55002" id="LblG2778_C55002">TIPO DE NEGOCIACION</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2778_C55002" id="G2778_C55002">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3115 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C55003" id="LblG2778_C55003">VALOR ACUERDO</label><input type="text" class="form-control input-sm" id="G2778_C55003" value="<?php if (isset($_GET['G2778_C55003'])) {
                            echo $_GET['G2778_C55003'];
                        } ?>"  name="G2778_C55003"  placeholder="VALOR ACUERDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2778_C55004" id="LblG2778_C55004">FECHA DE PAGO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2778_C55004'])) {
                            echo $_GET['G2778_C55004'];
                        } ?>"  name="G2778_C55004" id="G2778_C55004" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2778_C55005" id="LblG2778_C55005">FECHA PRÓXIMO PAGO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2778_C55005'])) {
                            echo $_GET['G2778_C55005'];
                        } ?>"  name="G2778_C55005" id="G2778_C55005" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2778_C55006" id="LblG2778_C55006">NUMERO DE CUOTAS</label><input type="text" class="form-control input-sm" id="G2778_C55006" value="<?php if (isset($_GET['G2778_C55006'])) {
                            echo $_GET['G2778_C55006'];
                        } ?>"  name="G2778_C55006"  placeholder="NUMERO DE CUOTAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2778/G2778_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2778_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2778_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2778_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2778_C54969").val(item.G2778_C54969); 
                $("#G2778_C54970").val(item.G2778_C54970); 
                $("#G2778_C54971").val(item.G2778_C54971); 
                $("#G2778_C54972").val(item.G2778_C54972); 
                $("#G2778_C54973").val(item.G2778_C54973); 
                $("#G2778_C54974").val(item.G2778_C54974); 
                $("#G2778_C54975").val(item.G2778_C54975); 
                $("#G2778_C54976").val(item.G2778_C54976); 
                $("#G2778_C54977").val(item.G2778_C54977); 
                $("#G2778_C54978").val(item.G2778_C54978); 
                $("#G2778_C54979").val(item.G2778_C54979); 
                $("#G2778_C54980").val(item.G2778_C54980); 
                $("#G2778_C54981").val(item.G2778_C54981); 
                $("#G2778_C54982").val(item.G2778_C54982); 
                $("#G2778_C54983").val(item.G2778_C54983); 
                $("#G2778_C54984").val(item.G2778_C54984); 
                $("#G2778_C54985").val(item.G2778_C54985); 
                $("#G2778_C54986").val(item.G2778_C54986); 
                $("#G2778_C54987").val(item.G2778_C54987); 
                $("#G2778_C54988").val(item.G2778_C54988); 
                $("#G2778_C56638").val(item.G2778_C56638); 
                $("#G2778_C54966").val(item.G2778_C54966); 
                $("#G2778_C54967").val(item.G2778_C54967); 
                $("#G2778_C54968").val(item.G2778_C54968).trigger("change");  
                $("#G2778_C54989").val(item.G2778_C54989).trigger("change");  
                $("#G2778_C54990").val(item.G2778_C54990); 
                $("#G2778_C54991").val(item.G2778_C54991); 
                $("#G2778_C54992").val(item.G2778_C54992); 
                $("#G2778_C54993").val(item.G2778_C54993); 
                $("#G2778_C54994").val(item.G2778_C54994); 
                $("#G2778_C54995").val(item.G2778_C54995); 
                $("#G2778_C54996").val(item.G2778_C54996); 
                $("#G2778_C54997").val(item.G2778_C54997); 
                $("#G2778_C54998").val(item.G2778_C54998); 
                $("#G2778_C54999").val(item.G2778_C54999); 
                $("#G2778_C55000").val(item.G2778_C55000).trigger("change");  
                $("#G2778_C55001").val(item.G2778_C55001).trigger("change");  
                $("#G2778_C55002").val(item.G2778_C55002).trigger("change");  
                $("#G2778_C55003").val(item.G2778_C55003); 
                $("#G2778_C55004").val(item.G2778_C55004); 
                $("#G2778_C55005").val(item.G2778_C55005); 
                $("#G2778_C55006").val(item.G2778_C55006);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2778_C54968").select2();

    $("#G2778_C54989").select2();

    $("#G2778_C55000").select2();

    $("#G2778_C55001").select2();

    $("#G2778_C55002").select2();
        //datepickers
        

        $("#G2778_C55004").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2778_C55005").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G2778_C54968").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO ACTUAL  

    $("#G2778_C54989").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CONTACTO 

    $("#G2778_C55000").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para RESULTADO CONTACTO 

    $("#G2778_C55001").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE NEGOCIACION 

    $("#G2778_C55002").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2778_C54969").val(item.G2778_C54969);
 
                                                $("#G2778_C54970").val(item.G2778_C54970);
 
                                                $("#G2778_C54971").val(item.G2778_C54971);
 
                                                $("#G2778_C54972").val(item.G2778_C54972);
 
                                                $("#G2778_C54973").val(item.G2778_C54973);
 
                                                $("#G2778_C54974").val(item.G2778_C54974);
 
                                                $("#G2778_C54975").val(item.G2778_C54975);
 
                                                $("#G2778_C54976").val(item.G2778_C54976);
 
                                                $("#G2778_C54977").val(item.G2778_C54977);
 
                                                $("#G2778_C54978").val(item.G2778_C54978);
 
                                                $("#G2778_C54979").val(item.G2778_C54979);
 
                                                $("#G2778_C54980").val(item.G2778_C54980);
 
                                                $("#G2778_C54981").val(item.G2778_C54981);
 
                                                $("#G2778_C54982").val(item.G2778_C54982);
 
                                                $("#G2778_C54983").val(item.G2778_C54983);
 
                                                $("#G2778_C54984").val(item.G2778_C54984);
 
                                                $("#G2778_C54985").val(item.G2778_C54985);
 
                                                $("#G2778_C54986").val(item.G2778_C54986);
 
                                                $("#G2778_C54987").val(item.G2778_C54987);
 
                                                $("#G2778_C54988").val(item.G2778_C54988);
 
                                                $("#G2778_C56638").val(item.G2778_C56638);
 
                                                $("#G2778_C54966").val(item.G2778_C54966);
 
                                                $("#G2778_C54967").val(item.G2778_C54967);
 
                    $("#G2778_C54968").val(item.G2778_C54968).trigger("change"); 
 
                    $("#G2778_C54989").val(item.G2778_C54989).trigger("change"); 
 
                                                $("#G2778_C54990").val(item.G2778_C54990);
 
                                                $("#G2778_C54991").val(item.G2778_C54991);
 
                                                $("#G2778_C54992").val(item.G2778_C54992);
 
                                                $("#G2778_C54993").val(item.G2778_C54993);
 
                                                $("#G2778_C54994").val(item.G2778_C54994);
 
                                                $("#G2778_C54995").val(item.G2778_C54995);
 
                                                $("#G2778_C54996").val(item.G2778_C54996);
 
                                                $("#G2778_C54997").val(item.G2778_C54997);
 
                                                $("#G2778_C54998").val(item.G2778_C54998);
 
                                                $("#G2778_C54999").val(item.G2778_C54999);
 
                    $("#G2778_C55000").val(item.G2778_C55000).trigger("change"); 
 
                    $("#G2778_C55001").val(item.G2778_C55001).trigger("change"); 
 
                    $("#G2778_C55002").val(item.G2778_C55002).trigger("change"); 
 
                                                $("#G2778_C55003").val(item.G2778_C55003);
 
                                                $("#G2778_C55004").val(item.G2778_C55004);
 
                                                $("#G2778_C55005").val(item.G2778_C55005);
 
                                                $("#G2778_C55006").val(item.G2778_C55006);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CEDULA','GIRADOR','TELEFONO1','TELEFONO2','TELEFONO3','TELEFONO4','TELEFONO5','FECHA MEMO','SALDO','INTERESES','HONORARIOS','HOGCA','SANCION','TOTAL A PAGAR','MOT_COBRADOR','NOMBRE AFILIADO','DIRECCION','CIUDAD','AGENTE','ASESOR','ESTADO(cargue)','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','ESTADO ACTUAL ','LUGAR DE TRABAJO','CARGO O ACTIVIDAD ECONOMICA','NOMBRE DE LA EMPRESA','DIRECCION ACTUALIZADA','TELEFONO WHATSAPP ','CORREO ELECTRONICO','REFERENCIA PERSONAL','REFERENCIA FAMILIAR','TELEFONO REFERENCIA PERSONAL ','TELEFONO REFERENCIA FAMILIAR','TIPO DE CONTACTO','RESULTADO CONTACTO','TIPO DE NEGOCIACION','VALOR ACUERDO','FECHA DE PAGO','FECHA PRÓXIMO PAGO','NUMERO DE CUOTAS'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2778_C54969', 
                        index: 'G2778_C54969', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54970', 
                        index: 'G2778_C54970', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54971', 
                        index: 'G2778_C54971', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54972', 
                        index: 'G2778_C54972', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54973', 
                        index: 'G2778_C54973', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54974', 
                        index: 'G2778_C54974', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54975', 
                        index: 'G2778_C54975', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54976', 
                        index: 'G2778_C54976', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54977', 
                        index: 'G2778_C54977', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54978', 
                        index: 'G2778_C54978', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54979', 
                        index: 'G2778_C54979', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54980', 
                        index: 'G2778_C54980', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54981', 
                        index: 'G2778_C54981', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54982', 
                        index: 'G2778_C54982', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54983', 
                        index: 'G2778_C54983', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54984', 
                        index: 'G2778_C54984', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54985', 
                        index: 'G2778_C54985', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54986', 
                        index: 'G2778_C54986', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54987', 
                        index: 'G2778_C54987', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54988', 
                        index: 'G2778_C54988', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C56638', 
                        index: 'G2778_C56638', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54966', 
                        index: 'G2778_C54966', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54967', 
                        index: 'G2778_C54967', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54968', 
                        index:'G2778_C54968', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3297&campo=G2778_C54968'
                        }
                    }

                    ,
                    { 
                        name:'G2778_C54989', 
                        index:'G2778_C54989', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2575&campo=G2778_C54989'
                        }
                    }

                    ,
                    { 
                        name:'G2778_C54990', 
                        index: 'G2778_C54990', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54991', 
                        index: 'G2778_C54991', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54992', 
                        index: 'G2778_C54992', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54993', 
                        index: 'G2778_C54993', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54994', 
                        index: 'G2778_C54994', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54995', 
                        index: 'G2778_C54995', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54996', 
                        index: 'G2778_C54996', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54997', 
                        index: 'G2778_C54997', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54998', 
                        index: 'G2778_C54998', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C54999', 
                        index: 'G2778_C54999', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2778_C55000', 
                        index:'G2778_C55000', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2902&campo=G2778_C55000'
                        }
                    }

                    ,
                    { 
                        name:'G2778_C55001', 
                        index:'G2778_C55001', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2590&campo=G2778_C55001'
                        }
                    }

                    ,
                    { 
                        name:'G2778_C55002', 
                        index:'G2778_C55002', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3115&campo=G2778_C55002'
                        }
                    }

                    ,
                    { 
                        name:'G2778_C55003', 
                        index: 'G2778_C55003', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2778_C55004', 
                        index:'G2778_C55004', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2778_C55005', 
                        index:'G2778_C55005', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2778_C55006', 
                        index: 'G2778_C55006', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2778_C54969',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2778_C54969").val(item.G2778_C54969);

                        $("#G2778_C54970").val(item.G2778_C54970);

                        $("#G2778_C54971").val(item.G2778_C54971);

                        $("#G2778_C54972").val(item.G2778_C54972);

                        $("#G2778_C54973").val(item.G2778_C54973);

                        $("#G2778_C54974").val(item.G2778_C54974);

                        $("#G2778_C54975").val(item.G2778_C54975);

                        $("#G2778_C54976").val(item.G2778_C54976);

                        $("#G2778_C54977").val(item.G2778_C54977);

                        $("#G2778_C54978").val(item.G2778_C54978);

                        $("#G2778_C54979").val(item.G2778_C54979);

                        $("#G2778_C54980").val(item.G2778_C54980);

                        $("#G2778_C54981").val(item.G2778_C54981);

                        $("#G2778_C54982").val(item.G2778_C54982);

                        $("#G2778_C54983").val(item.G2778_C54983);

                        $("#G2778_C54984").val(item.G2778_C54984);

                        $("#G2778_C54985").val(item.G2778_C54985);

                        $("#G2778_C54986").val(item.G2778_C54986);

                        $("#G2778_C54987").val(item.G2778_C54987);

                        $("#G2778_C54988").val(item.G2778_C54988);

                        $("#G2778_C56638").val(item.G2778_C56638);

                        $("#G2778_C54966").val(item.G2778_C54966);

                        $("#G2778_C54967").val(item.G2778_C54967);
 
                    $("#G2778_C54968").val(item.G2778_C54968).trigger("change"); 
 
                    $("#G2778_C54989").val(item.G2778_C54989).trigger("change"); 

                        $("#G2778_C54990").val(item.G2778_C54990);

                        $("#G2778_C54991").val(item.G2778_C54991);

                        $("#G2778_C54992").val(item.G2778_C54992);

                        $("#G2778_C54993").val(item.G2778_C54993);

                        $("#G2778_C54994").val(item.G2778_C54994);

                        $("#G2778_C54995").val(item.G2778_C54995);

                        $("#G2778_C54996").val(item.G2778_C54996);

                        $("#G2778_C54997").val(item.G2778_C54997);

                        $("#G2778_C54998").val(item.G2778_C54998);

                        $("#G2778_C54999").val(item.G2778_C54999);
 
                    $("#G2778_C55000").val(item.G2778_C55000).trigger("change"); 
 
                    $("#G2778_C55001").val(item.G2778_C55001).trigger("change"); 
 
                    $("#G2778_C55002").val(item.G2778_C55002).trigger("change"); 

                        $("#G2778_C55003").val(item.G2778_C55003);

                        $("#G2778_C55004").val(item.G2778_C55004);

                        $("#G2778_C55005").val(item.G2778_C55005);

                        $("#G2778_C55006").val(item.G2778_C55006);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
