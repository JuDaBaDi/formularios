<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2778_ConsInte__b, G2778_FechaInsercion , G2778_Usuario ,  G2778_CodigoMiembro  , G2778_PoblacionOrigen , G2778_EstadoDiligenciamiento ,  G2778_IdLlamada , G2778_C54969 as principal ,G2778_C54969,G2778_C54970,G2778_C54971,G2778_C54972,G2778_C54973,G2778_C54974,G2778_C54975,G2778_C54976,G2778_C54977,G2778_C54978,G2778_C54979,G2778_C54980,G2778_C54981,G2778_C54982,G2778_C54983,G2778_C54984,G2778_C54985,G2778_C54986,G2778_C54987,G2778_C54988,G2778_C56638,G2778_C54966,G2778_C54967,G2778_C54968,G2778_C54989,G2778_C54990,G2778_C54991,G2778_C54992,G2778_C54993,G2778_C54994,G2778_C54995,G2778_C54996,G2778_C54997,G2778_C54998,G2778_C54999,G2778_C55000,G2778_C55001,G2778_C55002,G2778_C55003,G2778_C55004,G2778_C55005,G2778_C55006 FROM '.$BaseDatos.'.G2778 WHERE G2778_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2778_C54969'] = $key->G2778_C54969;

                $datos[$i]['G2778_C54970'] = $key->G2778_C54970;

                $datos[$i]['G2778_C54971'] = $key->G2778_C54971;

                $datos[$i]['G2778_C54972'] = $key->G2778_C54972;

                $datos[$i]['G2778_C54973'] = $key->G2778_C54973;

                $datos[$i]['G2778_C54974'] = $key->G2778_C54974;

                $datos[$i]['G2778_C54975'] = $key->G2778_C54975;

                $datos[$i]['G2778_C54976'] = $key->G2778_C54976;

                $datos[$i]['G2778_C54977'] = $key->G2778_C54977;

                $datos[$i]['G2778_C54978'] = $key->G2778_C54978;

                $datos[$i]['G2778_C54979'] = $key->G2778_C54979;

                $datos[$i]['G2778_C54980'] = $key->G2778_C54980;

                $datos[$i]['G2778_C54981'] = $key->G2778_C54981;

                $datos[$i]['G2778_C54982'] = $key->G2778_C54982;

                $datos[$i]['G2778_C54983'] = $key->G2778_C54983;

                $datos[$i]['G2778_C54984'] = $key->G2778_C54984;

                $datos[$i]['G2778_C54985'] = $key->G2778_C54985;

                $datos[$i]['G2778_C54986'] = $key->G2778_C54986;

                $datos[$i]['G2778_C54987'] = $key->G2778_C54987;

                $datos[$i]['G2778_C54988'] = $key->G2778_C54988;

                $datos[$i]['G2778_C56638'] = $key->G2778_C56638;

                $datos[$i]['G2778_C54966'] = $key->G2778_C54966;

                $datos[$i]['G2778_C54967'] = $key->G2778_C54967;

                $datos[$i]['G2778_C54968'] = $key->G2778_C54968;

                $datos[$i]['G2778_C54989'] = $key->G2778_C54989;

                $datos[$i]['G2778_C54990'] = $key->G2778_C54990;

                $datos[$i]['G2778_C54991'] = $key->G2778_C54991;

                $datos[$i]['G2778_C54992'] = $key->G2778_C54992;

                $datos[$i]['G2778_C54993'] = $key->G2778_C54993;

                $datos[$i]['G2778_C54994'] = $key->G2778_C54994;

                $datos[$i]['G2778_C54995'] = $key->G2778_C54995;

                $datos[$i]['G2778_C54996'] = $key->G2778_C54996;

                $datos[$i]['G2778_C54997'] = $key->G2778_C54997;

                $datos[$i]['G2778_C54998'] = $key->G2778_C54998;

                $datos[$i]['G2778_C54999'] = $key->G2778_C54999;

                $datos[$i]['G2778_C55000'] = $key->G2778_C55000;

                $datos[$i]['G2778_C55001'] = $key->G2778_C55001;

                $datos[$i]['G2778_C55002'] = $key->G2778_C55002;

                $datos[$i]['G2778_C55003'] = $key->G2778_C55003;

                $datos[$i]['G2778_C55004'] = explode(' ', $key->G2778_C55004)[0];

                $datos[$i]['G2778_C55005'] = explode(' ', $key->G2778_C55005)[0];

                $datos[$i]['G2778_C55006'] = $key->G2778_C55006;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2778";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2778_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2778_ConsInte__b as id,  G2778_C54969 as camp1 , G2778_C54970 as camp2 
                     FROM ".$BaseDatos.".G2778  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2778_ConsInte__b as id,  G2778_C54969 as camp1 , G2778_C54970 as camp2  
                    FROM ".$BaseDatos.".G2778  JOIN ".$BaseDatos.".G2778_M".$_POST['muestra']." ON G2778_ConsInte__b = G2778_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2778_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2778_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2778_C54969 LIKE '%".$B."%' OR G2778_C54970 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2778_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2778");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2778_ConsInte__b, G2778_FechaInsercion , G2778_Usuario ,  G2778_CodigoMiembro  , G2778_PoblacionOrigen , G2778_EstadoDiligenciamiento ,  G2778_IdLlamada , G2778_C54969 as principal ,G2778_C54969,G2778_C54970,G2778_C54971,G2778_C54972,G2778_C54973,G2778_C54974,G2778_C54975,G2778_C54976,G2778_C54977,G2778_C54978,G2778_C54979,G2778_C54980,G2778_C54981,G2778_C54982,G2778_C54983,G2778_C54984,G2778_C54985,G2778_C54986,G2778_C54987,G2778_C54988,G2778_C56638,G2778_C54966,G2778_C54967, a.LISOPC_Nombre____b as G2778_C54968, b.LISOPC_Nombre____b as G2778_C54989,G2778_C54990,G2778_C54991,G2778_C54992,G2778_C54993,G2778_C54994,G2778_C54995,G2778_C54996,G2778_C54997,G2778_C54998,G2778_C54999, c.LISOPC_Nombre____b as G2778_C55000, d.LISOPC_Nombre____b as G2778_C55001, e.LISOPC_Nombre____b as G2778_C55002,G2778_C55003,G2778_C55004,G2778_C55005,G2778_C55006 FROM '.$BaseDatos.'.G2778 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2778_C54968 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2778_C54989 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2778_C55000 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2778_C55001 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2778_C55002';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2778_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2778_ConsInte__b , ($fila->G2778_C54969) , ($fila->G2778_C54970) , ($fila->G2778_C54971) , ($fila->G2778_C54972) , ($fila->G2778_C54973) , ($fila->G2778_C54974) , ($fila->G2778_C54975) , ($fila->G2778_C54976) , ($fila->G2778_C54977) , ($fila->G2778_C54978) , ($fila->G2778_C54979) , ($fila->G2778_C54980) , ($fila->G2778_C54981) , ($fila->G2778_C54982) , ($fila->G2778_C54983) , ($fila->G2778_C54984) , ($fila->G2778_C54985) , ($fila->G2778_C54986) , ($fila->G2778_C54987) , ($fila->G2778_C54988) , ($fila->G2778_C56638) , ($fila->G2778_C54966) , ($fila->G2778_C54967) , ($fila->G2778_C54968) , ($fila->G2778_C54989) , ($fila->G2778_C54990) , ($fila->G2778_C54991) , ($fila->G2778_C54992) , ($fila->G2778_C54993) , ($fila->G2778_C54994) , ($fila->G2778_C54995) , ($fila->G2778_C54996) , ($fila->G2778_C54997) , ($fila->G2778_C54998) , ($fila->G2778_C54999) , ($fila->G2778_C55000) , ($fila->G2778_C55001) , ($fila->G2778_C55002) , ($fila->G2778_C55003) , explode(' ', $fila->G2778_C55004)[0] , explode(' ', $fila->G2778_C55005)[0] , ($fila->G2778_C55006) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2778 WHERE G2778_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2778";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2778_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2778_ConsInte__b as id,  G2778_C54969 as camp1 , G2778_C54970 as camp2  FROM '.$BaseDatos.'.G2778 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2778_ConsInte__b as id,  G2778_C54969 as camp1 , G2778_C54970 as camp2  
                    FROM ".$BaseDatos.".G2778  JOIN ".$BaseDatos.".G2778_M".$_POST['muestra']." ON G2778_ConsInte__b = G2778_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2778_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2778_C54969 LIKE "%'.$B.'%" OR G2778_C54970 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2778_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2778 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2778(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2778_C54969"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54969 = '".$_POST["G2778_C54969"]."'";
                $LsqlI .= $separador."G2778_C54969";
                $LsqlV .= $separador."'".$_POST["G2778_C54969"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54970"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54970 = '".$_POST["G2778_C54970"]."'";
                $LsqlI .= $separador."G2778_C54970";
                $LsqlV .= $separador."'".$_POST["G2778_C54970"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54971"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54971 = '".$_POST["G2778_C54971"]."'";
                $LsqlI .= $separador."G2778_C54971";
                $LsqlV .= $separador."'".$_POST["G2778_C54971"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54972"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54972 = '".$_POST["G2778_C54972"]."'";
                $LsqlI .= $separador."G2778_C54972";
                $LsqlV .= $separador."'".$_POST["G2778_C54972"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54973"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54973 = '".$_POST["G2778_C54973"]."'";
                $LsqlI .= $separador."G2778_C54973";
                $LsqlV .= $separador."'".$_POST["G2778_C54973"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54974"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54974 = '".$_POST["G2778_C54974"]."'";
                $LsqlI .= $separador."G2778_C54974";
                $LsqlV .= $separador."'".$_POST["G2778_C54974"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54975"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54975 = '".$_POST["G2778_C54975"]."'";
                $LsqlI .= $separador."G2778_C54975";
                $LsqlV .= $separador."'".$_POST["G2778_C54975"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54976"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54976 = '".$_POST["G2778_C54976"]."'";
                $LsqlI .= $separador."G2778_C54976";
                $LsqlV .= $separador."'".$_POST["G2778_C54976"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54977"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54977 = '".$_POST["G2778_C54977"]."'";
                $LsqlI .= $separador."G2778_C54977";
                $LsqlV .= $separador."'".$_POST["G2778_C54977"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54978"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54978 = '".$_POST["G2778_C54978"]."'";
                $LsqlI .= $separador."G2778_C54978";
                $LsqlV .= $separador."'".$_POST["G2778_C54978"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54979"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54979 = '".$_POST["G2778_C54979"]."'";
                $LsqlI .= $separador."G2778_C54979";
                $LsqlV .= $separador."'".$_POST["G2778_C54979"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54980"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54980 = '".$_POST["G2778_C54980"]."'";
                $LsqlI .= $separador."G2778_C54980";
                $LsqlV .= $separador."'".$_POST["G2778_C54980"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54981"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54981 = '".$_POST["G2778_C54981"]."'";
                $LsqlI .= $separador."G2778_C54981";
                $LsqlV .= $separador."'".$_POST["G2778_C54981"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54982"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54982 = '".$_POST["G2778_C54982"]."'";
                $LsqlI .= $separador."G2778_C54982";
                $LsqlV .= $separador."'".$_POST["G2778_C54982"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54983"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54983 = '".$_POST["G2778_C54983"]."'";
                $LsqlI .= $separador."G2778_C54983";
                $LsqlV .= $separador."'".$_POST["G2778_C54983"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54984"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54984 = '".$_POST["G2778_C54984"]."'";
                $LsqlI .= $separador."G2778_C54984";
                $LsqlV .= $separador."'".$_POST["G2778_C54984"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54985"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54985 = '".$_POST["G2778_C54985"]."'";
                $LsqlI .= $separador."G2778_C54985";
                $LsqlV .= $separador."'".$_POST["G2778_C54985"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54986"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54986 = '".$_POST["G2778_C54986"]."'";
                $LsqlI .= $separador."G2778_C54986";
                $LsqlV .= $separador."'".$_POST["G2778_C54986"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54987"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54987 = '".$_POST["G2778_C54987"]."'";
                $LsqlI .= $separador."G2778_C54987";
                $LsqlV .= $separador."'".$_POST["G2778_C54987"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54988"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54988 = '".$_POST["G2778_C54988"]."'";
                $LsqlI .= $separador."G2778_C54988";
                $LsqlV .= $separador."'".$_POST["G2778_C54988"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C56638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C56638 = '".$_POST["G2778_C56638"]."'";
                $LsqlI .= $separador."G2778_C56638";
                $LsqlV .= $separador."'".$_POST["G2778_C56638"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54966"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54966 = '".$_POST["G2778_C54966"]."'";
                $LsqlI .= $separador."G2778_C54966";
                $LsqlV .= $separador."'".$_POST["G2778_C54966"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54967"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54967 = '".$_POST["G2778_C54967"]."'";
                $LsqlI .= $separador."G2778_C54967";
                $LsqlV .= $separador."'".$_POST["G2778_C54967"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54968"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54968 = '".$_POST["G2778_C54968"]."'";
                $LsqlI .= $separador."G2778_C54968";
                $LsqlV .= $separador."'".$_POST["G2778_C54968"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54989"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54989 = '".$_POST["G2778_C54989"]."'";
                $LsqlI .= $separador."G2778_C54989";
                $LsqlV .= $separador."'".$_POST["G2778_C54989"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54990"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54990 = '".$_POST["G2778_C54990"]."'";
                $LsqlI .= $separador."G2778_C54990";
                $LsqlV .= $separador."'".$_POST["G2778_C54990"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54991"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54991 = '".$_POST["G2778_C54991"]."'";
                $LsqlI .= $separador."G2778_C54991";
                $LsqlV .= $separador."'".$_POST["G2778_C54991"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54992"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54992 = '".$_POST["G2778_C54992"]."'";
                $LsqlI .= $separador."G2778_C54992";
                $LsqlV .= $separador."'".$_POST["G2778_C54992"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54993"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54993 = '".$_POST["G2778_C54993"]."'";
                $LsqlI .= $separador."G2778_C54993";
                $LsqlV .= $separador."'".$_POST["G2778_C54993"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54994"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54994 = '".$_POST["G2778_C54994"]."'";
                $LsqlI .= $separador."G2778_C54994";
                $LsqlV .= $separador."'".$_POST["G2778_C54994"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54995"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54995 = '".$_POST["G2778_C54995"]."'";
                $LsqlI .= $separador."G2778_C54995";
                $LsqlV .= $separador."'".$_POST["G2778_C54995"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54996"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54996 = '".$_POST["G2778_C54996"]."'";
                $LsqlI .= $separador."G2778_C54996";
                $LsqlV .= $separador."'".$_POST["G2778_C54996"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54997"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54997 = '".$_POST["G2778_C54997"]."'";
                $LsqlI .= $separador."G2778_C54997";
                $LsqlV .= $separador."'".$_POST["G2778_C54997"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54998"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54998 = '".$_POST["G2778_C54998"]."'";
                $LsqlI .= $separador."G2778_C54998";
                $LsqlV .= $separador."'".$_POST["G2778_C54998"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C54999"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C54999 = '".$_POST["G2778_C54999"]."'";
                $LsqlI .= $separador."G2778_C54999";
                $LsqlV .= $separador."'".$_POST["G2778_C54999"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C55000"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C55000 = '".$_POST["G2778_C55000"]."'";
                $LsqlI .= $separador."G2778_C55000";
                $LsqlV .= $separador."'".$_POST["G2778_C55000"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C55001"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C55001 = '".$_POST["G2778_C55001"]."'";
                $LsqlI .= $separador."G2778_C55001";
                $LsqlV .= $separador."'".$_POST["G2778_C55001"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C55002"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C55002 = '".$_POST["G2778_C55002"]."'";
                $LsqlI .= $separador."G2778_C55002";
                $LsqlV .= $separador."'".$_POST["G2778_C55002"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2778_C55003"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C55003 = '".$_POST["G2778_C55003"]."'";
                $LsqlI .= $separador."G2778_C55003";
                $LsqlV .= $separador."'".$_POST["G2778_C55003"]."'";
                $validar = 1;
            }
             
 
            $G2778_C55004 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2778_C55004"])){    
                if($_POST["G2778_C55004"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2778_C55004"]);
                    if(count($tieneHora) > 1){
                        $G2778_C55004 = "'".$_POST["G2778_C55004"]."'";
                    }else{
                        $G2778_C55004 = "'".str_replace(' ', '',$_POST["G2778_C55004"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2778_C55004 = ".$G2778_C55004;
                    $LsqlI .= $separador." G2778_C55004";
                    $LsqlV .= $separador.$G2778_C55004;
                    $validar = 1;
                }
            }
 
            $G2778_C55005 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2778_C55005"])){    
                if($_POST["G2778_C55005"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2778_C55005"]);
                    if(count($tieneHora) > 1){
                        $G2778_C55005 = "'".$_POST["G2778_C55005"]."'";
                    }else{
                        $G2778_C55005 = "'".str_replace(' ', '',$_POST["G2778_C55005"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2778_C55005 = ".$G2778_C55005;
                    $LsqlI .= $separador." G2778_C55005";
                    $LsqlV .= $separador.$G2778_C55005;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2778_C55006"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_C55006 = '".$_POST["G2778_C55006"]."'";
                $LsqlI .= $separador."G2778_C55006";
                $LsqlV .= $separador."'".$_POST["G2778_C55006"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2778_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2778_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2778_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2778_Usuario , G2778_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2778_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2778 WHERE G2778_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2778 SET G2778_UltiGest__b =-14, G2778_GesMasImp_b =-14, G2778_TipoReintentoUG_b =0, G2778_TipoReintentoGMI_b =0, G2778_EstadoUG_b =-14, G2778_EstadoGMI_b =-14, G2778_CantidadIntentos =0, G2778_CantidadIntentosGMI_b =0 WHERE G2778_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
