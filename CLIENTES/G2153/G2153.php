
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2153/G2153_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2153_ConsInte__b as id, G2153_C41323 as camp1 , G2153_C41324 as camp2 FROM ".$BaseDatos.".G2153  WHERE G2153_Usuario = ".$idUsuario." ORDER BY G2153_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2153_ConsInte__b as id, G2153_C41323 as camp1 , G2153_C41324 as camp2 FROM ".$BaseDatos.".G2153  ORDER BY G2153_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2153_ConsInte__b as id, G2153_C41323 as camp1 , G2153_C41324 as camp2 FROM ".$BaseDatos.".G2153 JOIN ".$BaseDatos.".G2153_M".$resultEstpas->muestr." ON G2153_ConsInte__b = G2153_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2153_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2153_ConsInte__b as id, G2153_C41323 as camp1 , G2153_C41324 as camp2 FROM ".$BaseDatos.".G2153 JOIN ".$BaseDatos.".G2153_M".$resultEstpas->muestr." ON G2153_ConsInte__b = G2153_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2153_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2153_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2153_ConsInte__b as id, G2153_C41323 as camp1 , G2153_C41324 as camp2 FROM ".$BaseDatos.".G2153  ORDER BY G2153_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){


    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="5959" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5959c">
                CONVERSACION
            </a>
        </h4>
        
    </div>
    <div id="s_5959c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41323" id="LblG2153_C41323">NOMBRE</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41323" value="<?php if (isset($_GET['G2153_C41323'])) {
                            echo $_GET['G2153_C41323'];
                        } ?>"  name="G2153_C41323"  placeholder="NOMBRE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41324" id="LblG2153_C41324">CEDULA</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41324" value="<?php if (isset($_GET['G2153_C41324'])) {
                            echo $_GET['G2153_C41324'];
                        } ?>"  name="G2153_C41324"  placeholder="CEDULA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41325" id="LblG2153_C41325">CELULAR</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41325" value="<?php if (isset($_GET['G2153_C41325'])) {
                            echo $_GET['G2153_C41325'];
                        } ?>"  name="G2153_C41325"  placeholder="CELULAR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41326" id="LblG2153_C41326">CIUDAD 1</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41326" id="G2153_C41326">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2291 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41327" id="LblG2153_C41327">FIJO</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41327" value="<?php if (isset($_GET['G2153_C41327'])) {
                            echo $_GET['G2153_C41327'];
                        } ?>"  name="G2153_C41327"  placeholder="FIJO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41328" id="LblG2153_C41328">DIRECCION</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41328" value="<?php if (isset($_GET['G2153_C41328'])) {
                            echo $_GET['G2153_C41328'];
                        } ?>"  name="G2153_C41328"  placeholder="DIRECCION">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41329" id="LblG2153_C41329">TIPO DE CLIENTE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41329" id="G2153_C41329">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2270 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C52332" id="LblG2153_C52332">Numero Grabacion</label>
                        <input type="text" class="form-control input-sm" id="G2153_C52332" value="<?php if (isset($_GET['G2153_C52332'])) {
                            echo $_GET['G2153_C52332'];
                        } ?>"  name="G2153_C52332"  placeholder="Numero Grabacion">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C52829" id="LblG2153_C52829">Fecha de Creación</label>
                        <input readonly type="text" class="form-control input-sm Fecha" value="<?php echo date("Y-m-d H:i:s"); ?>"  name="G2153_C52829" id="G2153_C52829" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5956" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5956c">
                PERSONA JURIDICA
            </a>
        </h4>
        
    </div>
    <div id="s_5956c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41330" id="LblG2153_C41330">TITULAR LINEA (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41330" value="<?php if (isset($_GET['G2153_C41330'])) {
                            echo $_GET['G2153_C41330'];
                        } ?>"  name="G2153_C41330"  placeholder="TITULAR LINEA (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41331" id="LblG2153_C41331">CEDULA TITULAR (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41331" value="<?php if (isset($_GET['G2153_C41331'])) {
                            echo $_GET['G2153_C41331'];
                        } ?>"  name="G2153_C41331"  placeholder="CEDULA TITULAR (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C41332" id="LblG2153_C41332">FECHA DE EXPEDICION (JURIDICO)</label>
                        <input autocomplete="off" type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2153_C41332'])) {
                            echo $_GET['G2153_C41332'];
                        } ?>"  name="G2153_C41332" id="G2153_C41332" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C41333" id="LblG2153_C41333">FECHA DE NACIMIENTO (JURIDICO)</label>
                        <input autocomplete="off" autocomplete="off" type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2153_C41333'])) {
                            echo $_GET['G2153_C41333'];
                        } ?>"  name="G2153_C41333" id="G2153_C41333" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41334" id="LblG2153_C41334">CORREO (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41334" value="<?php if (isset($_GET['G2153_C41334'])) {
                            echo $_GET['G2153_C41334'];
                        } ?>"  name="G2153_C41334"  placeholder="CORREO (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41335" id="LblG2153_C41335">SCORE (JURIDICO)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41335" id="G2153_C41335">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2275 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="5958" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41319" id="LblG2153_C41319">Agente</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41319" value="<?php echo getNombreUser($token);?>" readonly name="G2153_C41319"  placeholder="Agente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41320" id="LblG2153_C41320">Fecha</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41320" value="<?php echo date('Y-m-d');?>" readonly name="G2153_C41320"  placeholder="Fecha">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41321" id="LblG2153_C41321">Hora</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41321" value="<?php echo date('H:i:s');?>" readonly name="G2153_C41321"  placeholder="Hora">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41322" id="LblG2153_C41322">Campaña</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41322" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G2153_C41322"  placeholder="Campaña">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="5960" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5960c">
                PERSONA NATURAL
            </a>
        </h4>
        
    </div>
    <div id="s_5960c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41336" id="LblG2153_C41336">TITULAR LINEA (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41336" value="<?php if (isset($_GET['G2153_C41336'])) {
                            echo $_GET['G2153_C41336'];
                        } ?>"  name="G2153_C41336"  placeholder="TITULAR LINEA (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41337" id="LblG2153_C41337">CEDULA TITULAR (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41337" value="<?php if (isset($_GET['G2153_C41337'])) {
                            echo $_GET['G2153_C41337'];
                        } ?>"  name="G2153_C41337"  placeholder="CEDULA TITULAR (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C41338" id="LblG2153_C41338">FECHA DE EXPEDICION (NATURAL)</label>
                        <input autocomplete="off" type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2153_C41338'])) {
                            echo $_GET['G2153_C41338'];
                        } ?>"  name="G2153_C41338" id="G2153_C41338" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C41339" id="LblG2153_C41339">FECHA DE NACIMIENTO (NATURAL)</label>
                        <input autocomplete="off" type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2153_C41339'])) {
                            echo $_GET['G2153_C41339'];
                        } ?>"  name="G2153_C41339" id="G2153_C41339" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41340" id="LblG2153_C41340">CORREO (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41340" value="<?php if (isset($_GET['G2153_C41340'])) {
                            echo $_GET['G2153_C41340'];
                        } ?>"  name="G2153_C41340"  placeholder="CORREO (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41341" id="LblG2153_C41341">SCORE (NATURAL)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41341" id="G2153_C41341">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2275 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="5968" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41386" id="LblG2153_C41386">CANTIDAD DE LINEAS</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41386" value="<?php if (isset($_GET['G2153_C41386'])) {
                            echo $_GET['G2153_C41386'];
                        } ?>"  name="G2153_C41386"  placeholder="CANTIDAD DE LINEAS">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C41387" id="LblG2153_C41387">FECHA DE CORTE</label>
                        <input autocomplete="off" type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2153_C41387'])) {
                            echo $_GET['G2153_C41387'];
                        } ?>"  name="G2153_C41387" id="G2153_C41387" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C44535" id="LblG2153_C44535">NUMERO A PORTAR</label>
                        <input type="text" class="form-control input-sm" id="G2153_C44535" value="<?php if (isset($_GET['G2153_C44535'])) {
                            echo $_GET['G2153_C44535'];
                        } ?>"  name="G2153_C44535"  placeholder="NUMERO A PORTAR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        
            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2153_C41388" id="LblG2153_C41388">FECHA DE ENTREGA</label>
                        <input autocomplete="off" type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2153_C41388'])) {
                            echo $_GET['G2153_C41388'];
                        } ?>"  name="G2153_C41388" id="G2153_C41388" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C49280" id="LblG2153_C49280">FRANJA HORARIA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C49280" id="G2153_C49280">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2824 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->



        </div> <!-- AQUIFINSALDO1 -->

        <div class="row">

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2153_C41378" id="LblG2153_C41378">OBSERVACIONES GESTION</label>
                        <textarea class="form-control input-sm" name="G2153_C41378" id="G2153_C41378"  value="<?php if (isset($_GET['G2153_C41378'])) {
                            echo $_GET['G2153_C41378'];
                        } ?>" placeholder="OBSERVACIONES GESTION"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

        </div>

</div>

<div  id="5957" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="5961" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5961c">
                LINEA 1
            </a>
        </h4>
        
    </div>
    <div id="s_5961c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41342" id="LblG2153_C41342">OPERADOR ACTUAL (LINEA 1)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41342" id="G2153_C41342">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41343" id="LblG2153_C41343">PLAN (LINEA 1)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41343" id="G2153_C41343">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 AND LISOPC_ConsInte__b >= 53397 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->   


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41344" id="LblG2153_C41344">NIP (LINEA 1)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41344" value="<?php if (isset($_GET['G2153_C41344'])) {
                            echo $_GET['G2153_C41344'];
                        } ?>"  name="G2153_C41344"  placeholder="NIP (LINEA 1)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41345" id="LblG2153_C41345">VALOR PLAN (LINEA 1)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41345" id="G2153_C41345">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 AND LISOPC_ConsInte__b > 30390 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div>
        
        <div class="row">
            <div class="col-md-3 col-xs-3">
                <div class="form-group">
                        <label for="G2153_C56126" id="LblG2153_C56126">CONTRA ENTREGA O POSFECHADO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C56126" id="G2153_C56126">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3380 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    

                            ?>
                        </select>                    
                </div>
            </div>
        </div>         


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5962" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5962c">
                LINEA 2
            </a>
        </h4>
        
    </div>
    <div id="s_5962c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41346" id="LblG2153_C41346">OPERADOR ACTUAL (LINEA 2)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41346" id="G2153_C41346">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41347" id="LblG2153_C41347">PLAN (LINEA 2)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41347" id="G2153_C41347">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41348" id="LblG2153_C41348">NIP (LINEA 2)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41348" value="<?php if (isset($_GET['G2153_C41348'])) {
                            echo $_GET['G2153_C41348'];
                        } ?>"  name="G2153_C41348"  placeholder="NIP (LINEA 2)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41349" id="LblG2153_C41349">VALOR DEL PLAN (LINEA 2)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41349" id="G2153_C41349">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5963" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5963c">
                LINEA 3
            </a>
        </h4>
        
    </div>
    <div id="s_5963c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41350" id="LblG2153_C41350">OPERADOR ACTUAL (LINEA 3)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41350" id="G2153_C41350">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41351" id="LblG2153_C41351">PLAN (LINEA 3)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41351" id="G2153_C41351">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41352" id="LblG2153_C41352">NIP (LINEA 3)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41352" value="<?php if (isset($_GET['G2153_C41352'])) {
                            echo $_GET['G2153_C41352'];
                        } ?>"  name="G2153_C41352"  placeholder="NIP (LINEA 3)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41353" id="LblG2153_C41353">VALOR DEL PLAN (LINEA 3)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41353" id="G2153_C41353">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5964" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5964c">
                LINEA 4
            </a>
        </h4>
        
    </div>
    <div id="s_5964c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41354" id="LblG2153_C41354">OPERADOR ACTUAL (LINEA 4)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41354" id="G2153_C41354">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41355" id="LblG2153_C41355">PLAN (LINEA 4)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41355" id="G2153_C41355">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41356" id="LblG2153_C41356">NIP (LINEA 4)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41356" value="<?php if (isset($_GET['G2153_C41356'])) {
                            echo $_GET['G2153_C41356'];
                        } ?>"  name="G2153_C41356"  placeholder="NIP (LINEA 4)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41357" id="LblG2153_C41357">VALOR DEL PLAN (LINEA 4)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41357" id="G2153_C41357">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5965" style="display:none;">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5965c">
                LINEA 5
            </a>
        </h4>
        
    </div>
    <div id="s_5965c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41358" id="LblG2153_C41358">OPERADOR ACTUAL (LINEA 5)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41358" id="G2153_C41358">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41359" id="LblG2153_C41359">PLAN (LINEA 5)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41359" id="G2153_C41359">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41360" id="LblG2153_C41360">NIP (LINEA 5)</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41360" value="<?php if (isset($_GET['G2153_C41360'])) {
                            echo $_GET['G2153_C41360'];
                        } ?>"  name="G2153_C41360"  placeholder="NIP (LINEA 5)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41361" id="LblG2153_C41361">VALOR DEL PLAN (LINEA 5)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41361" id="G2153_C41361">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5966" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5966c">
                DATOS DE DIRECCION
            </a>
        </h4>
        
    </div>
    <div id="s_5966c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41362" id="LblG2153_C41362">PERSONA DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41362" value="<?php if (isset($_GET['G2153_C41362'])) {
                            echo $_GET['G2153_C41362'];
                        } ?>"  name="G2153_C41362"  placeholder="PERSONA DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41363" id="LblG2153_C41363">NUMERO DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41363" value="<?php if (isset($_GET['G2153_C41363'])) {
                            echo $_GET['G2153_C41363'];
                        } ?>"  name="G2153_C41363"  placeholder="NUMERO DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2153_C41376" id="LblG2153_C41376">RECAUDO CLIENTE</label>
                        <input type="text" class="form-control input-sm" id="G2153_C41376" value="<?php if (isset($_GET['G2153_C41376'])) {
                            echo $_GET['G2153_C41376'];
                        } ?>"  name="G2153_C41376"  placeholder="RECAUDO CLIENTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41375" id="LblG2153_C41375">DEPARTAMENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41375" id="G2153_C41375">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2292 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41374" id="LblG2153_C41374">CIUDAD</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41374" id="G2153_C41374">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2291 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->



  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2153_C41377" id="LblG2153_C41377">OBSERVACION DIRECCION</label>
                        <textarea class="form-control input-sm" name="G2153_C41377" id="G2153_C41377"  value="<?php if (isset($_GET['G2153_C41377'])) {
                            echo $_GET['G2153_C41377'];
                        } ?>" placeholder="OBSERVACION DIRECCION"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2153_C41314">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2272;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2153_C41314">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2272;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2153_C41315">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2153_C41316" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2153_C41317" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2153_C41318" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2153/G2153_eventos.js"></script> 
<script type="text/javascript">
    $(function(){


    $("#G2153_C41314").change(function(){

        if ($(this).val() == 30303 || $(this).val() == 30304) {

            $("#G2153_C41388").addClass( "OBLIGATORIO" );
            $("#G2153_C49280").addClass( "OBLIGATORIO" );
                
        }else{

            $("#G2153_C41388").removeClass("OBLIGATORIO");
            $("#G2153_C49280").removeClass("OBLIGATORIO");

        }

    });
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2153_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2153_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2153_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>; 
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2153_C41330").val(item.G2153_C41330); 
                $("#G2153_C41331").val(item.G2153_C41331); 
                $("#G2153_C41332").val(item.G2153_C41332); 
                $("#G2153_C41333").val(item.G2153_C41333); 
                $("#G2153_C41334").val(item.G2153_C41334); 
                $("#G2153_C41335").val(item.G2153_C41335).trigger("change");  
                $("#G2153_C41314").val(item.G2153_C41314).trigger("change");  
                $("#G2153_C41315").val(item.G2153_C41315).trigger("change");  
                $("#G2153_C41316").val(item.G2153_C41316); 
                $("#G2153_C41317").val(item.G2153_C41317); 
                $("#G2153_C41318").val(item.G2153_C41318); 
                $("#G2153_C41319").val(item.G2153_C41319); 
                $("#G2153_C41320").val(item.G2153_C41320); 
                $("#G2153_C41321").val(item.G2153_C41321); 
                $("#G2153_C41322").val(item.G2153_C41322); 
                $("#G2153_C41323").val(item.G2153_C41323); 
                $("#G2153_C41324").val(item.G2153_C41324); 
                $("#G2153_C41325").val(item.G2153_C41325); 
                $("#G2153_C41326").val(item.G2153_C41326).trigger("change");  
                $("#G2153_C41327").val(item.G2153_C41327); 
                $("#G2153_C41328").val(item.G2153_C41328); 
                $("#G2153_C41329").val(item.G2153_C41329).trigger("change");  
                $("#G2153_C41336").val(item.G2153_C41336); 
                $("#G2153_C41337").val(item.G2153_C41337); 
                $("#G2153_C41338").val(item.G2153_C41338); 
                $("#G2153_C41339").val(item.G2153_C41339); 
                $("#G2153_C41340").val(item.G2153_C41340); 
                $("#G2153_C41341").val(item.G2153_C41341).trigger("change");  
                $("#G2153_C41342").val(item.G2153_C41342).trigger("change");  
                $("#G2153_C41343").val(item.G2153_C41343).trigger("change");  
                $("#G2153_C56126").val(item.G2153_C56126).trigger("change");  
                $("#G2153_C41344").val(item.G2153_C41344); 
                $("#G2153_C41345").val(item.G2153_C41345).trigger("change");  
                $("#G2153_C41346").val(item.G2153_C41346).trigger("change");  
                $("#G2153_C41347").val(item.G2153_C41347).trigger("change");  
                $("#G2153_C41348").val(item.G2153_C41348); 
                $("#G2153_C41349").val(item.G2153_C41349).trigger("change");  
                $("#G2153_C41350").val(item.G2153_C41350).trigger("change");  
                $("#G2153_C41351").val(item.G2153_C41351).trigger("change");  
                $("#G2153_C41352").val(item.G2153_C41352); 
                $("#G2153_C41353").val(item.G2153_C41353).trigger("change");  
                $("#G2153_C41354").val(item.G2153_C41354).trigger("change");  
                $("#G2153_C41355").val(item.G2153_C41355).trigger("change");  
                $("#G2153_C41356").val(item.G2153_C41356); 
                $("#G2153_C41357").val(item.G2153_C41357).trigger("change");  
                $("#G2153_C41358").val(item.G2153_C41358).trigger("change");  
                $("#G2153_C41359").val(item.G2153_C41359).trigger("change");  
                $("#G2153_C41360").val(item.G2153_C41360); 
                $("#G2153_C41361").val(item.G2153_C41361).trigger("change");  
                $("#G2153_C41362").val(item.G2153_C41362); 
                $("#G2153_C41363").val(item.G2153_C41363);   
                $("#G2153_C41375").val(item.G2153_C41375).trigger("change");  
                $("#G2153_C41374").attr("opt",item.G2153_C41374);  
                $("#G2153_C41376").val(item.G2153_C41376); 
                $("#G2153_C41377").val(item.G2153_C41377); 
                $("#G2153_C41386").val(item.G2153_C41386); 
                $("#G2153_C41387").val(item.G2153_C41387); 
                $("#G2153_C41388").val(item.G2153_C41388); 
                $("#G2153_C44535").val(item.G2153_C44535); 
                $("#G2153_C49280").val(item.G2153_C49280).trigger("change");  
                $("#G2153_C44534").val(item.G2153_C44534); 
                $("#G2153_C41378").val(item.G2153_C41378);
                $("#G2153_C52332").val(item.G2153_C52332);
                $("#G2153_C52829").val(item.G2153_C52829);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2153_C41326").select2();

    $("#G2153_C41329").select2();

    $("#G2153_C41335").select2();

    $("#G2153_C41341").select2();

    $("#G2153_C49280").select2();

    $("#G2153_C41342").select2();

    $("#G2153_C41343").select2();
    
    $("#G2153_C56126").select2();

    $("#G2153_C41345").select2();

    $("#G2153_C41346").select2();

    $("#G2153_C41347").select2();

    $("#G2153_C41349").select2();

    $("#G2153_C41350").select2();

    $("#G2153_C41351").select2();

    $("#G2153_C41353").select2();

    $("#G2153_C41354").select2();

    $("#G2153_C41355").select2();

    $("#G2153_C41357").select2();

    $("#G2153_C41358").select2();

    $("#G2153_C41359").select2();

    $("#G2153_C41361").select2();

    $("#G2153_C41375").select2();

    $("#G2153_C41374").select2();
        //datepickers
        

        $("#G2153_C41332").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2153_C41333").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2153_C41316").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2153_C41338").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2153_C41339").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2153_C41387").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2153_C41388").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2153_C41317").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        
    $("#G2153_C52332").numeric();

    //function para CIUDAD 1 

    $("#G2153_C41326").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CLIENTE 

    $("#G2153_C41329").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SCORE (JURIDICO) 

    $("#G2153_C41335").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SCORE (NATURAL) 

    $("#G2153_C41341").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para FRANJA HORARIA 

    $("#G2153_C49280").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 1) 

    $("#G2153_C41342").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 1) 

    $("#G2153_C41343").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });    
        
    //function para POSFECHADO 

    $("#G2153_C56126").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR PLAN (LINEA 1) 

    $("#G2153_C41345").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 2) 

    $("#G2153_C41346").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 2) 

    $("#G2153_C41347").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 2) 

    $("#G2153_C41349").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 3) 

    $("#G2153_C41350").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 3) 

    $("#G2153_C41351").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 3) 

    $("#G2153_C41353").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 4) 

    $("#G2153_C41354").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 4) 

    $("#G2153_C41355").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 4) 

    $("#G2153_C41357").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 5) 

    $("#G2153_C41358").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 5) 

    $("#G2153_C41359").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 5) 

    $("#G2153_C41361").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DEPARTAMENTO 

    $("#G2153_C41375").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2291' , idPadre : $(this).val() },
            success : function(data){
                var optG2153_C41374 = $("#G2153_C41374").attr("opt");
                $("#G2153_C41374").html(data);
                if (optG2153_C41374 != null) {
                    $("#G2153_C41374").val(optG2153_C41374).trigger("change");
                }
            }
        });
        
    });

    //function para CIUDAD 

    $("#G2153_C41374").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            var inthasError_t = 0;

            $(".OBLIGATORIO").each(function(){

                if (this.id == 'G2153_C49280') {
                    
                    if (($(this).val() == "0" || $(this).val() == -1 || $(this).val() == null) && $(this).prop("disabled") == false) {

                        inthasError_t = inthasError_t + 1;
                        alertify.error("Es necesario elegir la \"FRANJA HORARIA\"");
                        valido = 1;

                    }

                }else{

                    if ($(this).val()== "" && $(this).prop("disabled") == false) {

                        inthasError_t = inthasError_t + 2;
                        alertify.error("Es necesario elegir la \"FECHA DE ENTREGA\"");
                        valido = 1;

                    }else{

                        var dia = d.getDate();

                        if (dia < 10) {

                            dia = "0"+dia;

                        }

                        var m = d.getMonth()+1;

                        if (m < 10) {

                            m = "0"+m;

                        }

                        var intSecondsFechaActual_t = Date.parse(d.getFullYear()+"-"+m+"-"+dia);
                        var intSecondsFechaEntrega_t = Date.parse(this.value);
                        
                        if (Number(intSecondsFechaEntrega_t) < Number(intSecondsFechaActual_t)) {

                            inthasError_t = inthasError_t + 2;

                            alert("La \"FECHA DE ENTREGA\" debe ser igual o mayor a la FECHA ACTUAL.");

                        }

                    }

                }

            });

            switch (inthasError_t) {
                case 3:

                    $("#G2153_C41388").closest(".form-group").addClass("has-error");
                    $("#G2153_C49280").closest(".form-group").addClass("has-error");

                    break;
                case 2:

                    $("#G2153_C41388").closest(".form-group").addClass("has-error");

                    break;
                case 1:
                
                    $("#G2153_C49280").closest(".form-group").addClass("has-error");

                    break;
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2153_C41330").val(item.G2153_C41330);
 
                                                $("#G2153_C41331").val(item.G2153_C41331);
 
                                                $("#G2153_C41332").val(item.G2153_C41332);
 
                                                $("#G2153_C41333").val(item.G2153_C41333);
 
                                                $("#G2153_C41334").val(item.G2153_C41334);
 
                    $("#G2153_C41335").val(item.G2153_C41335).trigger("change"); 
 
                    $("#G2153_C41314").val(item.G2153_C41314).trigger("change"); 
 
                    $("#G2153_C41315").val(item.G2153_C41315).trigger("change"); 
 
                                                $("#G2153_C41316").val(item.G2153_C41316);
 
                                                $("#G2153_C41317").val(item.G2153_C41317);
 
                                                $("#G2153_C41318").val(item.G2153_C41318);
 
                                                $("#G2153_C41319").val(item.G2153_C41319);
 
                                                $("#G2153_C41320").val(item.G2153_C41320);
 
                                                $("#G2153_C41321").val(item.G2153_C41321);
 
                                                $("#G2153_C41322").val(item.G2153_C41322);
 
                                                $("#G2153_C41323").val(item.G2153_C41323);
 
                                                $("#G2153_C41324").val(item.G2153_C41324);
 
                                                $("#G2153_C41325").val(item.G2153_C41325);
 
                    $("#G2153_C41326").val(item.G2153_C41326).trigger("change"); 
 
                                                $("#G2153_C41327").val(item.G2153_C41327);
 
                                                $("#G2153_C41328").val(item.G2153_C41328);
 
                    $("#G2153_C41329").val(item.G2153_C41329).trigger("change"); 
 
                                                $("#G2153_C41336").val(item.G2153_C41336);
 
                                                $("#G2153_C41337").val(item.G2153_C41337);
 
                                                $("#G2153_C41338").val(item.G2153_C41338);
 
                                                $("#G2153_C41339").val(item.G2153_C41339);
 
                                                $("#G2153_C41340").val(item.G2153_C41340);
 
                    $("#G2153_C41341").val(item.G2153_C41341).trigger("change"); 
 
                    $("#G2153_C41342").val(item.G2153_C41342).trigger("change"); 
 
                    $("#G2153_C41343").val(item.G2153_C41343).trigger("change"); 
                    
                    $("#G2153_C56126").val(item.G2153_C56126).trigger("change"); 
 
                                                $("#G2153_C41344").val(item.G2153_C41344);
 
                    $("#G2153_C41345").val(item.G2153_C41345).trigger("change"); 
 
                    $("#G2153_C41346").val(item.G2153_C41346).trigger("change"); 
 
                    $("#G2153_C41347").val(item.G2153_C41347).trigger("change"); 
 
                                                $("#G2153_C41348").val(item.G2153_C41348);
 
                    $("#G2153_C41349").val(item.G2153_C41349).trigger("change"); 
 
                    $("#G2153_C41350").val(item.G2153_C41350).trigger("change"); 
 
                    $("#G2153_C41351").val(item.G2153_C41351).trigger("change"); 
 
                                                $("#G2153_C41352").val(item.G2153_C41352);
 
                    $("#G2153_C41353").val(item.G2153_C41353).trigger("change"); 
 
                    $("#G2153_C41354").val(item.G2153_C41354).trigger("change"); 
 
                    $("#G2153_C41355").val(item.G2153_C41355).trigger("change"); 
 
                                                $("#G2153_C41356").val(item.G2153_C41356);
 
                    $("#G2153_C41357").val(item.G2153_C41357).trigger("change"); 
 
                    $("#G2153_C41358").val(item.G2153_C41358).trigger("change"); 
 
                    $("#G2153_C41359").val(item.G2153_C41359).trigger("change"); 
 
                                                $("#G2153_C41360").val(item.G2153_C41360);
 
                    $("#G2153_C41361").val(item.G2153_C41361).trigger("change"); 
 
                                                $("#G2153_C41362").val(item.G2153_C41362);
 
                                                $("#G2153_C41363").val(item.G2153_C41363);
 
                    $("#G2153_C41375").val(item.G2153_C41375).trigger("change"); 
 
                    $("#G2153_C41374").attr("opt",item.G2153_C41374); 
 
                                                $("#G2153_C41376").val(item.G2153_C41376);
 
                                                $("#G2153_C41377").val(item.G2153_C41377);
 
                                                $("#G2153_C41386").val(item.G2153_C41386);
 
                                                $("#G2153_C41387").val(item.G2153_C41387);
 
                                                $("#G2153_C41388").val(item.G2153_C41388);
 
                                                $("#G2153_C44535").val(item.G2153_C44535);
 
                    $("#G2153_C49280").val(item.G2153_C49280).trigger("change"); 
 
                                                $("#G2153_C44534").val(item.G2153_C44534);
 
                                                $("#G2153_C41378").val(item.G2153_C41378);

                                                $("#G2153_C52332").val(item.G2153_C52332);
                                                $("#G2153_C52829").val(item.G2153_C52829);

                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','TITULAR LINEA (JURIDICO)','CEDULA TITULAR (JURIDICO)','FECHA DE EXPEDICION (JURIDICO)','FECHA DE NACIMIENTO (JURIDICO)','CORREO (JURIDICO)','SCORE (JURIDICO)','Agente','Fecha','Hora','Campaña','NOMBRE','CEDULA','CELULAR','CIUDAD 1','FIJO','DIRECCION','TIPO DE CLIENTE','TITULAR LINEA (NATURAL)','CEDULA TITULAR (NATURAL)','FECHA DE EXPEDICION (NATURAL)','FECHA DE NACIMIENTO (NATURAL)','CORREO (NATURAL)','SCORE (NATURAL)','OPERADOR ACTUAL (LINEA 1)','PLAN (LINEA 1)','NIP (LINEA 1)','VALOR PLAN (LINEA 1)','OPERADOR ACTUAL (LINEA 2)','PLAN (LINEA 2)','NIP (LINEA 2)','VALOR DEL PLAN (LINEA 2)','OPERADOR ACTUAL (LINEA 3)','PLAN (LINEA 3)','NIP (LINEA 3)','VALOR DEL PLAN (LINEA 3)','OPERADOR ACTUAL (LINEA 4)','PLAN (LINEA 4)','NIP (LINEA 4)','VALOR DEL PLAN (LINEA 4)','OPERADOR ACTUAL (LINEA 5)','PLAN (LINEA 5)','NIP (LINEA 5)','VALOR DEL PLAN (LINEA 5)','PERSONA DE CONTACTO','NUMERO DE CONTACTO','TIPO DE VIA 1','NUMERO DE VIA 1','LITERAL 1','TIPO DE VIA 2','NUMERO DE VIA 2','LITERAL 2','ORIENTACION','NUMERO DE VIA 3','ORIENTACION 2','DEPARTAMENTO','CIUDAD','RECAUDO CLIENTE','OBSERVACION DIRECCION','CANTIDAD DE LINEAS','FECHA DE CORTE','FECHA DE ENTREGA','NUMERO A PORTAR','FRANJA HORARIA','FRANJA HORARIA (se eliminara)','OBSERVACIONES GESTION'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2153_C41330', 
                        index: 'G2153_C41330', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41331', 
                        index: 'G2153_C41331', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2153_C41332', 
                        index:'G2153_C41332', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2153_C41333', 
                        index:'G2153_C41333', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41334', 
                        index: 'G2153_C41334', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41335', 
                        index:'G2153_C41335', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2275&campo=G2153_C41335'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41319', 
                        index: 'G2153_C41319', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41320', 
                        index: 'G2153_C41320', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41321', 
                        index: 'G2153_C41321', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41322', 
                        index: 'G2153_C41322', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41323', 
                        index: 'G2153_C41323', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41324', 
                        index: 'G2153_C41324', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41325', 
                        index: 'G2153_C41325', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41326', 
                        index:'G2153_C41326', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2291&campo=G2153_C41326'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41327', 
                        index: 'G2153_C41327', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41328', 
                        index: 'G2153_C41328', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41329', 
                        index:'G2153_C41329', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2270&campo=G2153_C41329'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41336', 
                        index: 'G2153_C41336', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41337', 
                        index: 'G2153_C41337', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2153_C41338', 
                        index:'G2153_C41338', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2153_C41339', 
                        index:'G2153_C41339', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41340', 
                        index: 'G2153_C41340', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41341', 
                        index:'G2153_C41341', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2275&campo=G2153_C41341'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41342', 
                        index:'G2153_C41342', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2276&campo=G2153_C41342'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41343', 
                        index:'G2153_C41343', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2277&campo=G2153_C41343'
                        }
                    }                    
                    ,
                    { 
                        name:'G2153_C56126',
                        index:'G2153_C56126', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3380&campo=G2153_C56126'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41344', 
                        index: 'G2153_C41344', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41345', 
                        index:'G2153_C41345', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2284&campo=G2153_C41345'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41346', 
                        index:'G2153_C41346', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2276&campo=G2153_C41346'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41347', 
                        index:'G2153_C41347', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2277&campo=G2153_C41347'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41348', 
                        index: 'G2153_C41348', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41349', 
                        index:'G2153_C41349', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2284&campo=G2153_C41349'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41350', 
                        index:'G2153_C41350', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2276&campo=G2153_C41350'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41351', 
                        index:'G2153_C41351', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2277&campo=G2153_C41351'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41352', 
                        index: 'G2153_C41352', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41353', 
                        index:'G2153_C41353', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2284&campo=G2153_C41353'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41354', 
                        index:'G2153_C41354', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2276&campo=G2153_C41354'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41355', 
                        index:'G2153_C41355', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2277&campo=G2153_C41355'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41356', 
                        index: 'G2153_C41356', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41357', 
                        index:'G2153_C41357', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2284&campo=G2153_C41357'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41358', 
                        index:'G2153_C41358', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2276&campo=G2153_C41358'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41359', 
                        index:'G2153_C41359', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2277&campo=G2153_C41359'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41360', 
                        index: 'G2153_C41360', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41361', 
                        index:'G2153_C41361', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2284&campo=G2153_C41361'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41362', 
                        index: 'G2153_C41362', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41363', 
                        index: 'G2153_C41363', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41364', 
                        index:'G2153_C41364', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2285&campo=G2153_C41364'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41365', 
                        index: 'G2153_C41365', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41366', 
                        index: 'G2153_C41366', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41367', 
                        index:'G2153_C41367', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2285&campo=G2153_C41367'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41368', 
                        index: 'G2153_C41368', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41369', 
                        index: 'G2153_C41369', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41370', 
                        index:'G2153_C41370', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2286&campo=G2153_C41370'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41371', 
                        index: 'G2153_C41371', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41372', 
                        index:'G2153_C41372', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2286&campo=G2153_C41372'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41375', 
                        index:'G2153_C41375', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2292&campo=G2153_C41375'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41374', 
                        index:'G2153_C41374', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2291&campo=G2153_C41374'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C41376', 
                        index: 'G2153_C41376', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41377', 
                        index:'G2153_C41377', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41386', 
                        index: 'G2153_C41386', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2153_C41387', 
                        index:'G2153_C41387', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2153_C41388', 
                        index:'G2153_C41388', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2153_C44535', 
                        index: 'G2153_C44535', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C49280', 
                        index:'G2153_C49280', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2824&campo=G2153_C49280'
                        }
                    }

                    ,
                    { 
                        name:'G2153_C44534', 
                        index: 'G2153_C44534', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2153_C41378', 
                        index:'G2153_C41378', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2153_C41323',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2153_C41330").val(item.G2153_C41330);

                        $("#G2153_C41331").val(item.G2153_C41331);

                        $("#G2153_C41332").val(item.G2153_C41332);

                        $("#G2153_C41333").val(item.G2153_C41333);

                        $("#G2153_C41334").val(item.G2153_C41334);
 
                    $("#G2153_C41335").val(item.G2153_C41335).trigger("change"); 
 
                    $("#G2153_C41314").val(item.G2153_C41314).trigger("change"); 
 
                    $("#G2153_C41315").val(item.G2153_C41315).trigger("change"); 

                        $("#G2153_C41316").val(item.G2153_C41316);

                        $("#G2153_C41317").val(item.G2153_C41317);

                        $("#G2153_C41318").val(item.G2153_C41318);

                        $("#G2153_C41319").val(item.G2153_C41319);

                        $("#G2153_C41320").val(item.G2153_C41320);

                        $("#G2153_C41321").val(item.G2153_C41321);

                        $("#G2153_C41322").val(item.G2153_C41322);

                        $("#G2153_C41323").val(item.G2153_C41323);

                        $("#G2153_C41324").val(item.G2153_C41324);

                        $("#G2153_C41325").val(item.G2153_C41325);
 
                    $("#G2153_C41326").val(item.G2153_C41326).trigger("change"); 

                        $("#G2153_C41327").val(item.G2153_C41327);

                        $("#G2153_C41328").val(item.G2153_C41328);
 
                    $("#G2153_C41329").val(item.G2153_C41329).trigger("change"); 

                        $("#G2153_C41336").val(item.G2153_C41336);

                        $("#G2153_C41337").val(item.G2153_C41337);

                        $("#G2153_C41338").val(item.G2153_C41338);

                        $("#G2153_C41339").val(item.G2153_C41339);

                        $("#G2153_C41340").val(item.G2153_C41340);
 
                    $("#G2153_C41341").val(item.G2153_C41341).trigger("change"); 
 
                    $("#G2153_C41342").val(item.G2153_C41342).trigger("change"); 
 
                    $("#G2153_C41343").val(item.G2153_C41343).trigger("change"); 
                    
                    $("#G2153_C56126").val(item.G2153_C56126).trigger("change"); 

                        $("#G2153_C41344").val(item.G2153_C41344);
 
                    $("#G2153_C41345").val(item.G2153_C41345).trigger("change"); 
 
                    $("#G2153_C41346").val(item.G2153_C41346).trigger("change"); 
 
                    $("#G2153_C41347").val(item.G2153_C41347).trigger("change"); 

                        $("#G2153_C41348").val(item.G2153_C41348);
 
                    $("#G2153_C41349").val(item.G2153_C41349).trigger("change"); 
 
                    $("#G2153_C41350").val(item.G2153_C41350).trigger("change"); 
 
                    $("#G2153_C41351").val(item.G2153_C41351).trigger("change"); 

                        $("#G2153_C41352").val(item.G2153_C41352);
 
                    $("#G2153_C41353").val(item.G2153_C41353).trigger("change"); 
 
                    $("#G2153_C41354").val(item.G2153_C41354).trigger("change"); 
 
                    $("#G2153_C41355").val(item.G2153_C41355).trigger("change"); 

                        $("#G2153_C41356").val(item.G2153_C41356);
 
                    $("#G2153_C41357").val(item.G2153_C41357).trigger("change"); 
 
                    $("#G2153_C41358").val(item.G2153_C41358).trigger("change"); 
 
                    $("#G2153_C41359").val(item.G2153_C41359).trigger("change"); 

                        $("#G2153_C41360").val(item.G2153_C41360);
 
                    $("#G2153_C41361").val(item.G2153_C41361).trigger("change"); 

                        $("#G2153_C41362").val(item.G2153_C41362);

                        $("#G2153_C41363").val(item.G2153_C41363); 
 
                    $("#G2153_C41375").val(item.G2153_C41375).trigger("change"); 
 
                    $("#G2153_C41374").attr("opt",item.G2153_C41374); 

                        $("#G2153_C41376").val(item.G2153_C41376);

                        $("#G2153_C41377").val(item.G2153_C41377);

                        $("#G2153_C41386").val(item.G2153_C41386);

                        $("#G2153_C41387").val(item.G2153_C41387);

                        $("#G2153_C41388").val(item.G2153_C41388);

                        $("#G2153_C44535").val(item.G2153_C44535);
 
                    $("#G2153_C49280").val(item.G2153_C49280).trigger("change"); 

                        $("#G2153_C44534").val(item.G2153_C44534);

                        $("#G2153_C41378").val(item.G2153_C41378);

                        $("#G2153_C52332").val(item.G2153_C52332);
                        $("#G2153_C52829").val(item.G2153_C52829);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
