<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3102_ConsInte__b, G3102_FechaInsercion , G3102_Usuario ,  G3102_CodigoMiembro  , G3102_PoblacionOrigen , G3102_EstadoDiligenciamiento ,  G3102_IdLlamada , G3102_C62063 as principal ,G3102_C62062,G3102_C62063,G3102_C62064,G3102_C62065,G3102_C62066,G3102_C62067,G3102_C62068,G3102_C62069,G3102_C62070,G3102_C62071,G3102_C62072,G3102_C62073,G3102_C62074,G3102_C62075,G3102_C62076,G3102_C62077,G3102_C62078,G3102_C62079,G3102_C62080,G3102_C62081,G3102_C62082,G3102_C62083,G3102_C62084,G3102_C62085,G3102_C62086,G3102_C62087,G3102_C62088,G3102_C62089,G3102_C62090,G3102_C62091,G3102_C62059,G3102_C62060,G3102_C62061,G3102_C62093,G3102_C62092,G3102_C62345,G3102_C62625 FROM '.$BaseDatos.'.G3102 WHERE G3102_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3102_C62062'] = $key->G3102_C62062;

                $datos[$i]['G3102_C62063'] = $key->G3102_C62063;

                $datos[$i]['G3102_C62064'] = $key->G3102_C62064;

                $datos[$i]['G3102_C62065'] = $key->G3102_C62065;

                $datos[$i]['G3102_C62066'] = $key->G3102_C62066;

                $datos[$i]['G3102_C62067'] = $key->G3102_C62067;

                $datos[$i]['G3102_C62068'] = $key->G3102_C62068;

                $datos[$i]['G3102_C62069'] = $key->G3102_C62069;

                $datos[$i]['G3102_C62070'] = $key->G3102_C62070;

                $datos[$i]['G3102_C62071'] = $key->G3102_C62071;

                $datos[$i]['G3102_C62072'] = $key->G3102_C62072;

                $datos[$i]['G3102_C62073'] = $key->G3102_C62073;

                $datos[$i]['G3102_C62074'] = $key->G3102_C62074;

                $datos[$i]['G3102_C62075'] = $key->G3102_C62075;

                $datos[$i]['G3102_C62076'] = $key->G3102_C62076;

                $datos[$i]['G3102_C62077'] = $key->G3102_C62077;

                $datos[$i]['G3102_C62078'] = $key->G3102_C62078;

                $datos[$i]['G3102_C62079'] = $key->G3102_C62079;

                $datos[$i]['G3102_C62080'] = $key->G3102_C62080;

                $datos[$i]['G3102_C62081'] = $key->G3102_C62081;

                $datos[$i]['G3102_C62082'] = $key->G3102_C62082;

                $datos[$i]['G3102_C62083'] = $key->G3102_C62083;

                $datos[$i]['G3102_C62084'] = $key->G3102_C62084;

                $datos[$i]['G3102_C62085'] = $key->G3102_C62085;

                $datos[$i]['G3102_C62086'] = $key->G3102_C62086;

                $datos[$i]['G3102_C62087'] = $key->G3102_C62087;

                $datos[$i]['G3102_C62088'] = $key->G3102_C62088;

                $datos[$i]['G3102_C62089'] = $key->G3102_C62089;

                $datos[$i]['G3102_C62090'] = $key->G3102_C62090;

                $datos[$i]['G3102_C62091'] = $key->G3102_C62091;

                $datos[$i]['G3102_C62059'] = $key->G3102_C62059;

                $datos[$i]['G3102_C62060'] = $key->G3102_C62060;

                $datos[$i]['G3102_C62061'] = $key->G3102_C62061;

                $datos[$i]['G3102_C62093'] = $key->G3102_C62093;

                $datos[$i]['G3102_C62092'] = $key->G3102_C62092;

                $datos[$i]['G3102_C62345'] = $key->G3102_C62345;

                $datos[$i]['G3102_C62625'] = $key->G3102_C62625;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3102";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3102_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3102_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G3102_C62065 as camp2 
                     FROM ".$BaseDatos.".G3102  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G3102_C62063 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3102_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G3102_C62065 as camp2  
                    FROM ".$BaseDatos.".G3102  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G3102_C62063 JOIN ".$BaseDatos.".G3102_M".$_POST['muestra']." ON G3102_ConsInte__b = G3102_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3102_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3102_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3102_C62063 LIKE '%".$B."%' OR G3102_C62065 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3102_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3102");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3102_ConsInte__b, G3102_FechaInsercion , G3102_Usuario ,  G3102_CodigoMiembro  , G3102_PoblacionOrigen , G3102_EstadoDiligenciamiento ,  G3102_IdLlamada , G3102_C62063 as principal ,G3102_C62062, a.LISOPC_Nombre____b as G3102_C62063,G3102_C62064,G3102_C62065,G3102_C62066,G3102_C62067,G3102_C62068,G3102_C62069,G3102_C62070,G3102_C62071,G3102_C62072,G3102_C62073,G3102_C62074,G3102_C62075,G3102_C62076,G3102_C62077,G3102_C62078,G3102_C62079,G3102_C62080,G3102_C62081,G3102_C62082,G3102_C62083,G3102_C62084,G3102_C62085,G3102_C62086,G3102_C62087,G3102_C62088,G3102_C62089,G3102_C62090,G3102_C62091,G3102_C62059,G3102_C62060, b.LISOPC_Nombre____b as G3102_C62061,G3102_C62093,G3102_C62092,G3102_C62345, c.LISOPC_Nombre____b as G3102_C62625 FROM '.$BaseDatos.'.G3102 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3102_C62063 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3102_C62061 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3102_C62625';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G3102_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3102_ConsInte__b , ($fila->G3102_C62062) , ($fila->G3102_C62063) , ($fila->G3102_C62064) , ($fila->G3102_C62065) , ($fila->G3102_C62066) , ($fila->G3102_C62067) , ($fila->G3102_C62068) , ($fila->G3102_C62069) , ($fila->G3102_C62070) , ($fila->G3102_C62071) , ($fila->G3102_C62072) , ($fila->G3102_C62073) , ($fila->G3102_C62074) , ($fila->G3102_C62075) , ($fila->G3102_C62076) , ($fila->G3102_C62077) , ($fila->G3102_C62078) , ($fila->G3102_C62079) , ($fila->G3102_C62080) , ($fila->G3102_C62081) , ($fila->G3102_C62082) , ($fila->G3102_C62083) , ($fila->G3102_C62084) , ($fila->G3102_C62085) , ($fila->G3102_C62086) , ($fila->G3102_C62087) , ($fila->G3102_C62088) , ($fila->G3102_C62089) , ($fila->G3102_C62090) , ($fila->G3102_C62091) , ($fila->G3102_C62059) , ($fila->G3102_C62060) , ($fila->G3102_C62061) , ($fila->G3102_C62093) , ($fila->G3102_C62092) , ($fila->G3102_C62345) , ($fila->G3102_C62625) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3102 WHERE G3102_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3102";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3102_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3102_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G3102_C62065 as camp2  FROM '.$BaseDatos.'.G3102 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3102_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G3102_C62065 as camp2  
                    FROM ".$BaseDatos.".G3102  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G3102_C62063 JOIN ".$BaseDatos.".G3102_M".$_POST['muestra']." ON G3102_ConsInte__b = G3102_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3102_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3102_C62063 LIKE "%'.$B.'%" OR G3102_C62065 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3102_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3102 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3102(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G3102_C62062"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62062 = '".$_POST["G3102_C62062"]."'";
                $LsqlI .= $separador."G3102_C62062";
                $LsqlV .= $separador."'".$_POST["G3102_C62062"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62063"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62063 = '".$_POST["G3102_C62063"]."'";
                $LsqlI .= $separador."G3102_C62063";
                $LsqlV .= $separador."'".$_POST["G3102_C62063"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62064"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62064 = '".$_POST["G3102_C62064"]."'";
                $LsqlI .= $separador."G3102_C62064";
                $LsqlV .= $separador."'".$_POST["G3102_C62064"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62065"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62065 = '".$_POST["G3102_C62065"]."'";
                $LsqlI .= $separador."G3102_C62065";
                $LsqlV .= $separador."'".$_POST["G3102_C62065"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62066"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62066 = '".$_POST["G3102_C62066"]."'";
                $LsqlI .= $separador."G3102_C62066";
                $LsqlV .= $separador."'".$_POST["G3102_C62066"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62067"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62067 = '".$_POST["G3102_C62067"]."'";
                $LsqlI .= $separador."G3102_C62067";
                $LsqlV .= $separador."'".$_POST["G3102_C62067"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62068"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62068 = '".$_POST["G3102_C62068"]."'";
                $LsqlI .= $separador."G3102_C62068";
                $LsqlV .= $separador."'".$_POST["G3102_C62068"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62069"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62069 = '".$_POST["G3102_C62069"]."'";
                $LsqlI .= $separador."G3102_C62069";
                $LsqlV .= $separador."'".$_POST["G3102_C62069"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62070"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62070 = '".$_POST["G3102_C62070"]."'";
                $LsqlI .= $separador."G3102_C62070";
                $LsqlV .= $separador."'".$_POST["G3102_C62070"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62071"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62071 = '".$_POST["G3102_C62071"]."'";
                $LsqlI .= $separador."G3102_C62071";
                $LsqlV .= $separador."'".$_POST["G3102_C62071"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62072"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62072 = '".$_POST["G3102_C62072"]."'";
                $LsqlI .= $separador."G3102_C62072";
                $LsqlV .= $separador."'".$_POST["G3102_C62072"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62073"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62073 = '".$_POST["G3102_C62073"]."'";
                $LsqlI .= $separador."G3102_C62073";
                $LsqlV .= $separador."'".$_POST["G3102_C62073"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62074"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62074 = '".$_POST["G3102_C62074"]."'";
                $LsqlI .= $separador."G3102_C62074";
                $LsqlV .= $separador."'".$_POST["G3102_C62074"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62075"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62075 = '".$_POST["G3102_C62075"]."'";
                $LsqlI .= $separador."G3102_C62075";
                $LsqlV .= $separador."'".$_POST["G3102_C62075"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62076"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62076 = '".$_POST["G3102_C62076"]."'";
                $LsqlI .= $separador."G3102_C62076";
                $LsqlV .= $separador."'".$_POST["G3102_C62076"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62077"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62077 = '".$_POST["G3102_C62077"]."'";
                $LsqlI .= $separador."G3102_C62077";
                $LsqlV .= $separador."'".$_POST["G3102_C62077"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62078"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62078 = '".$_POST["G3102_C62078"]."'";
                $LsqlI .= $separador."G3102_C62078";
                $LsqlV .= $separador."'".$_POST["G3102_C62078"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62079"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62079 = '".$_POST["G3102_C62079"]."'";
                $LsqlI .= $separador."G3102_C62079";
                $LsqlV .= $separador."'".$_POST["G3102_C62079"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62080"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62080 = '".$_POST["G3102_C62080"]."'";
                $LsqlI .= $separador."G3102_C62080";
                $LsqlV .= $separador."'".$_POST["G3102_C62080"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62081"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62081 = '".$_POST["G3102_C62081"]."'";
                $LsqlI .= $separador."G3102_C62081";
                $LsqlV .= $separador."'".$_POST["G3102_C62081"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62082"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62082 = '".$_POST["G3102_C62082"]."'";
                $LsqlI .= $separador."G3102_C62082";
                $LsqlV .= $separador."'".$_POST["G3102_C62082"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62083"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62083 = '".$_POST["G3102_C62083"]."'";
                $LsqlI .= $separador."G3102_C62083";
                $LsqlV .= $separador."'".$_POST["G3102_C62083"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62084"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62084 = '".$_POST["G3102_C62084"]."'";
                $LsqlI .= $separador."G3102_C62084";
                $LsqlV .= $separador."'".$_POST["G3102_C62084"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62085"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62085 = '".$_POST["G3102_C62085"]."'";
                $LsqlI .= $separador."G3102_C62085";
                $LsqlV .= $separador."'".$_POST["G3102_C62085"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62086"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62086 = '".$_POST["G3102_C62086"]."'";
                $LsqlI .= $separador."G3102_C62086";
                $LsqlV .= $separador."'".$_POST["G3102_C62086"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62087"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62087 = '".$_POST["G3102_C62087"]."'";
                $LsqlI .= $separador."G3102_C62087";
                $LsqlV .= $separador."'".$_POST["G3102_C62087"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62088"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62088 = '".$_POST["G3102_C62088"]."'";
                $LsqlI .= $separador."G3102_C62088";
                $LsqlV .= $separador."'".$_POST["G3102_C62088"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62089"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62089 = '".$_POST["G3102_C62089"]."'";
                $LsqlI .= $separador."G3102_C62089";
                $LsqlV .= $separador."'".$_POST["G3102_C62089"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62090"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62090 = '".$_POST["G3102_C62090"]."'";
                $LsqlI .= $separador."G3102_C62090";
                $LsqlV .= $separador."'".$_POST["G3102_C62090"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62091"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62091 = '".$_POST["G3102_C62091"]."'";
                $LsqlI .= $separador."G3102_C62091";
                $LsqlV .= $separador."'".$_POST["G3102_C62091"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62059"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62059 = '".$_POST["G3102_C62059"]."'";
                $LsqlI .= $separador."G3102_C62059";
                $LsqlV .= $separador."'".$_POST["G3102_C62059"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62060"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62060 = '".$_POST["G3102_C62060"]."'";
                $LsqlI .= $separador."G3102_C62060";
                $LsqlV .= $separador."'".$_POST["G3102_C62060"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62061"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62061 = '".$_POST["G3102_C62061"]."'";
                $LsqlI .= $separador."G3102_C62061";
                $LsqlV .= $separador."'".$_POST["G3102_C62061"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62093"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62093 = '".$_POST["G3102_C62093"]."'";
                $LsqlI .= $separador."G3102_C62093";
                $LsqlV .= $separador."'".$_POST["G3102_C62093"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62092"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62092 = '".$_POST["G3102_C62092"]."'";
                $LsqlI .= $separador."G3102_C62092";
                $LsqlV .= $separador."'".$_POST["G3102_C62092"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62345"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62345 = '".$_POST["G3102_C62345"]."'";
                $LsqlI .= $separador."G3102_C62345";
                $LsqlV .= $separador."'".$_POST["G3102_C62345"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3102_C62625"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_C62625 = '".$_POST["G3102_C62625"]."'";
                $LsqlI .= $separador."G3102_C62625";
                $LsqlV .= $separador."'".$_POST["G3102_C62625"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3102_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3102_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3102_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3102_Usuario , G3102_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3102_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3102 WHERE G3102_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3102 SET G3102_UltiGest__b =-14, G3102_GesMasImp_b =-14, G3102_TipoReintentoUG_b =0, G3102_TipoReintentoGMI_b =0, G3102_ClasificacionUG_b =3, G3102_ClasificacionGMI_b =3, G3102_EstadoUG_b =-14, G3102_EstadoGMI_b =-14, G3102_CantidadIntentos =0, G3102_CantidadIntentosGMI_b =0 WHERE G3102_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G3102_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G3102 WHERE G3102_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G3138_ConsInte__b, G3138_C62621, G3138_C62622, G3138_C62623 FROM ".$BaseDatos.".G3138  ";

        $SQL .= " WHERE G3138_C62621 = '".$numero."'"; 

        $SQL .= " ORDER BY G3138_C62621";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G3138_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G3138_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G3138_C62621)."</cell>";

                if($fila->G3138_C62622 != ''){
                    echo "<cell>". explode(' ', $fila->G3138_C62622)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell><![CDATA[". ($fila->G3138_C62623)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3138 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3138(";
            $LsqlV = " VALUES ("; 

                $G3138_C62622 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G3138_C62622"])){    
                    if($_POST["G3138_C62622"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G3138_C62622 = "'".str_replace(' ', '',$_POST["G3138_C62622"])." 00:00:00'";
                        $LsqlU .= $separador." G3138_C62622 = ".$G3138_C62622;
                        $LsqlI .= $separador." G3138_C62622";
                        $LsqlV .= $separador.$G3138_C62622;
                        $validar = 1;
                    }
                }
  

                if(isset($_POST["G3138_C62623"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G3138_C62623 = '".$_POST["G3138_C62623"]."'";
                    $LsqlI .= $separador."G3138_C62623";
                    $LsqlV .= $separador."'".$_POST["G3138_C62623"]."'";
                    $validar = 1;
                }
                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G3138_C62621 = $numero;
                    $LsqlU .= ", G3138_C62621 = ".$G3138_C62621."";
                    $LsqlI .= ", G3138_C62621";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G3138_Usuario ,  G3138_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3138_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G3138 WHERE  G3138_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
