<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      
        if (isset($_GET["cargarHistorico"])) {

            $intIdRegistro_t = $_GET["intIdRegistro_t"];

            $strSQLGestiones_t = "SELECT G2786_ConsInte__b AS id, dyalogo_general.fn_item_lisopc(G2786_C55220) AS gestion, G2786_C55224 AS comentario, (CASE WHEN numero_telefonico IS NULL THEN CONCAT('A1477',G2786_C55235) ELSE numero_telefonico END) AS telefono, G2786_IdLlamada AS id_llamada, G2786_C55226 AS fecha, dyalogo_general.fn_nombre_USUARI(G2786_Usuario) AS agente FROM ".$BaseDatos.".G2786 LEFT JOIN dyalogo_telefonia.dy_llamadas_espejo ON G2786_IdLlamada = unique_id WHERE G2786_CodigoMiembro = ".$intIdRegistro_t." ORDER BY G2786_ConsInte__b DESC";

            if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
                header("Content-type: application/xhtml+xml;charset=utf-8"); 
            } else { 
                header("Content-type: text/xml;charset=utf-8"); 
            } 

            $et = ">"; 
            echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
            echo "<rows>"; // be sure to put text data in CDATA
            $result = $mysqli->query($strSQLGestiones_t);
            while( $fila = $result->fetch_object() ) {
                echo "<row asin='".$fila->id."'>"; 
                echo "<cell>". ($fila->id)."</cell>"; 

                    $fila->comentario = str_replace("<", "-", $fila->comentario);
                    $fila->comentario = str_replace(">", "-", $fila->comentario);

                    echo "<cell>". ($fila->gestion)."</cell>";
                    echo "<cell>". ($fila->comentario)."</cell>";
                    echo "<cell>". ($fila->telefono)."</cell>";
                    echo "<cell>". ($fila->id_llamada)."</cell>";
                    echo "<cell>". ($fila->fecha)."</cell>";
                    echo "<cell>". ($fila->agente)."</cell>";

                echo "</row>"; 
            } 
            echo "</rows>"; 
        }

      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2785_ConsInte__b, G2785_FechaInsercion , G2785_Usuario ,  G2785_CodigoMiembro  , G2785_PoblacionOrigen , G2785_EstadoDiligenciamiento ,  G2785_IdLlamada , G2785_C55206 as principal ,G2785_C55205,G2785_C55206,G2785_C55207,G2785_C55208,G2785_C55209,G2785_C55210,G2785_C55211,G2785_C55212,G2785_C55213,G2785_C55214,G2785_C55215,G2785_C55216,G2785_C55217,G2785_C55218,G2785_C55219,G2785_C55202,G2785_C55203,G2785_C55204,G2785_C56374,G2785_C56375,G2785_C56376,G2785_C56377,G2785_C56378,G2785_C56379,G2785_C56380,G2785_C56381,G2785_C56382 FROM '.$BaseDatos.'.G2785 WHERE G2785_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2785_C55205'] = $key->G2785_C55205;

                $datos[$i]['G2785_C55206'] = $key->G2785_C55206;

                $datos[$i]['G2785_C55207'] = explode(' ', $key->G2785_C55207)[0];

                $datos[$i]['G2785_C55208'] = $key->G2785_C55208;

                $datos[$i]['G2785_C55209'] = $key->G2785_C55209;

                $datos[$i]['G2785_C55210'] = $key->G2785_C55210;

                $datos[$i]['G2785_C55211'] = $key->G2785_C55211;

                $datos[$i]['G2785_C55212'] = $key->G2785_C55212;

                $datos[$i]['G2785_C55213'] = $key->G2785_C55213;

                $datos[$i]['G2785_C55214'] = $key->G2785_C55214;

                $datos[$i]['G2785_C55215'] = $key->G2785_C55215;

                $datos[$i]['G2785_C55216'] = $key->G2785_C55216;

                $datos[$i]['G2785_C55217'] = $key->G2785_C55217;

                $datos[$i]['G2785_C55218'] = $key->G2785_C55218;

                $datos[$i]['G2785_C55219'] = $key->G2785_C55219;

                $datos[$i]['G2785_C55202'] = $key->G2785_C55202;

                $datos[$i]['G2785_C55203'] = $key->G2785_C55203;

                $datos[$i]['G2785_C55204'] = $key->G2785_C55204;

                $datos[$i]['G2785_C56374'] = $key->G2785_C56374;

                $datos[$i]['G2785_C56375'] = $key->G2785_C56375;

                $datos[$i]['G2785_C56376'] = $key->G2785_C56376;

                $datos[$i]['G2785_C56377'] = $key->G2785_C56377;

                $datos[$i]['G2785_C56378'] = $key->G2785_C56378;

                $datos[$i]['G2785_C56379'] = explode(' ', $key->G2785_C56379)[0];

                $datos[$i]['G2785_C56380'] = $key->G2785_C56380;

                $datos[$i]['G2785_C56381'] = $key->G2785_C56381;

                $datos[$i]['G2785_C56382'] = $key->G2785_C56382;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2785";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2785_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2785_ConsInte__b as id,  G2785_C55205 as camp2 , G2785_C55206 as camp1 
                     FROM ".$BaseDatos.".G2785  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2785_ConsInte__b as id,  G2785_C55205 as camp2 , G2785_C55206 as camp1  
                    FROM ".$BaseDatos.".G2785  JOIN ".$BaseDatos.".G2785_M".$_POST['muestra']." ON G2785_ConsInte__b = G2785_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2785_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2785_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2785_C55205 LIKE '%".$B."%' OR G2785_C55206 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2785_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2785");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2785_ConsInte__b, G2785_FechaInsercion , G2785_Usuario ,  G2785_CodigoMiembro  , G2785_PoblacionOrigen , G2785_EstadoDiligenciamiento ,  G2785_IdLlamada , G2785_C55206 as principal ,G2785_C55205,G2785_C55206,G2785_C55207,G2785_C55208,G2785_C55209,G2785_C55210,G2785_C55211,G2785_C55212,G2785_C55213,G2785_C55214,G2785_C55215,G2785_C55216,G2785_C55217,G2785_C55218,G2785_C55219,G2785_C55202,G2785_C55203, a.LISOPC_Nombre____b as G2785_C55204,G2785_C56374,G2785_C56375,G2785_C56376, b.LISOPC_Nombre____b as G2785_C56377,G2785_C56378,G2785_C56379, c.LISOPC_Nombre____b as G2785_C56380,G2785_C56381,G2785_C56382 FROM '.$BaseDatos.'.G2785 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2785_C55204 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2785_C56377 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2785_C56380';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2785_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2785_ConsInte__b , ($fila->G2785_C55205) , ($fila->G2785_C55206) , explode(' ', $fila->G2785_C55207)[0] , ($fila->G2785_C55208) , ($fila->G2785_C55209) , ($fila->G2785_C55210) , ($fila->G2785_C55211) , ($fila->G2785_C55212) , ($fila->G2785_C55213) , ($fila->G2785_C55214) , ($fila->G2785_C55215) , ($fila->G2785_C55216) , ($fila->G2785_C55217) , ($fila->G2785_C55218) , ($fila->G2785_C55219) , ($fila->G2785_C55202) , ($fila->G2785_C55203) , ($fila->G2785_C55204) , ($fila->G2785_C56374) , ($fila->G2785_C56375) , ($fila->G2785_C56376) , ($fila->G2785_C56377) , ($fila->G2785_C56378) , explode(' ', $fila->G2785_C56379)[0] , ($fila->G2785_C56380) , ($fila->G2785_C56381) , ($fila->G2785_C56382) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2785 WHERE G2785_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2785";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2785_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2785_ConsInte__b as id,  G2785_C55205 as camp2 , G2785_C55206 as camp1  FROM '.$BaseDatos.'.G2785 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2785_ConsInte__b as id,  G2785_C55205 as camp2 , G2785_C55206 as camp1  
                    FROM ".$BaseDatos.".G2785  JOIN ".$BaseDatos.".G2785_M".$_POST['muestra']." ON G2785_ConsInte__b = G2785_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2785_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2785_C55205 LIKE "%'.$B.'%" OR G2785_C55206 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2785_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2785 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2785(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2785_C55205"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55205 = '".$_POST["G2785_C55205"]."'";
                $LsqlI .= $separador."G2785_C55205";
                $LsqlV .= $separador."'".$_POST["G2785_C55205"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55206"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55206 = '".$_POST["G2785_C55206"]."'";
                $LsqlI .= $separador."G2785_C55206";
                $LsqlV .= $separador."'".$_POST["G2785_C55206"]."'";
                $validar = 1;
            }
             
 
            $G2785_C55207 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2785_C55207"])){    
                if($_POST["G2785_C55207"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2785_C55207"]);
                    if(count($tieneHora) > 1){
                        $G2785_C55207 = "'".$_POST["G2785_C55207"]."'";
                    }else{
                        $G2785_C55207 = "'".str_replace(' ', '',$_POST["G2785_C55207"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2785_C55207 = ".$G2785_C55207;
                    $LsqlI .= $separador." G2785_C55207";
                    $LsqlV .= $separador.$G2785_C55207;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2785_C55208"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55208 = '".$_POST["G2785_C55208"]."'";
                $LsqlI .= $separador."G2785_C55208";
                $LsqlV .= $separador."'".$_POST["G2785_C55208"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55209"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55209 = '".$_POST["G2785_C55209"]."'";
                $LsqlI .= $separador."G2785_C55209";
                $LsqlV .= $separador."'".$_POST["G2785_C55209"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55210"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55210 = '".$_POST["G2785_C55210"]."'";
                $LsqlI .= $separador."G2785_C55210";
                $LsqlV .= $separador."'".$_POST["G2785_C55210"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55211"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55211 = '".$_POST["G2785_C55211"]."'";
                $LsqlI .= $separador."G2785_C55211";
                $LsqlV .= $separador."'".$_POST["G2785_C55211"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55212"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55212 = '".$_POST["G2785_C55212"]."'";
                $LsqlI .= $separador."G2785_C55212";
                $LsqlV .= $separador."'".$_POST["G2785_C55212"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55213"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55213 = '".$_POST["G2785_C55213"]."'";
                $LsqlI .= $separador."G2785_C55213";
                $LsqlV .= $separador."'".$_POST["G2785_C55213"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55214"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55214 = '".$_POST["G2785_C55214"]."'";
                $LsqlI .= $separador."G2785_C55214";
                $LsqlV .= $separador."'".$_POST["G2785_C55214"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55215"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55215 = '".$_POST["G2785_C55215"]."'";
                $LsqlI .= $separador."G2785_C55215";
                $LsqlV .= $separador."'".$_POST["G2785_C55215"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55216"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55216 = '".$_POST["G2785_C55216"]."'";
                $LsqlI .= $separador."G2785_C55216";
                $LsqlV .= $separador."'".$_POST["G2785_C55216"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55217"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55217 = '".$_POST["G2785_C55217"]."'";
                $LsqlI .= $separador."G2785_C55217";
                $LsqlV .= $separador."'".$_POST["G2785_C55217"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55218"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55218 = '".$_POST["G2785_C55218"]."'";
                $LsqlI .= $separador."G2785_C55218";
                $LsqlV .= $separador."'".$_POST["G2785_C55218"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55219"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55219 = '".$_POST["G2785_C55219"]."'";
                $LsqlI .= $separador."G2785_C55219";
                $LsqlV .= $separador."'".$_POST["G2785_C55219"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55202"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55202 = '".$_POST["G2785_C55202"]."'";
                $LsqlI .= $separador."G2785_C55202";
                $LsqlV .= $separador."'".$_POST["G2785_C55202"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55203"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55203 = '".$_POST["G2785_C55203"]."'";
                $LsqlI .= $separador."G2785_C55203";
                $LsqlV .= $separador."'".$_POST["G2785_C55203"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C55204"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C55204 = '".$_POST["G2785_C55204"]."'";
                $LsqlI .= $separador."G2785_C55204";
                $LsqlV .= $separador."'".$_POST["G2785_C55204"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C56374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56374 = '".$_POST["G2785_C56374"]."'";
                $LsqlI .= $separador."G2785_C56374";
                $LsqlV .= $separador."'".$_POST["G2785_C56374"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C56375"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56375 = '".$_POST["G2785_C56375"]."'";
                $LsqlI .= $separador."G2785_C56375";
                $LsqlV .= $separador."'".$_POST["G2785_C56375"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C56376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56376 = '".$_POST["G2785_C56376"]."'";
                $LsqlI .= $separador."G2785_C56376";
                $LsqlV .= $separador."'".$_POST["G2785_C56376"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C56377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56377 = '".$_POST["G2785_C56377"]."'";
                $LsqlI .= $separador."G2785_C56377";
                $LsqlV .= $separador."'".$_POST["G2785_C56377"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2785_C56378"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56378 = '".$_POST["G2785_C56378"]."'";
                $LsqlI .= $separador."G2785_C56378";
                $LsqlV .= $separador."'".$_POST["G2785_C56378"]."'";
                $validar = 1;
            }
             
 
            $G2785_C56379 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2785_C56379"])){    
                if($_POST["G2785_C56379"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2785_C56379"]);
                    if(count($tieneHora) > 1){
                        $G2785_C56379 = "'".$_POST["G2785_C56379"]."'";
                    }else{
                        $G2785_C56379 = "'".str_replace(' ', '',$_POST["G2785_C56379"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2785_C56379 = ".$G2785_C56379;
                    $LsqlI .= $separador." G2785_C56379";
                    $LsqlV .= $separador.$G2785_C56379;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2785_C56380"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56380 = '".$_POST["G2785_C56380"]."'";
                $LsqlI .= $separador."G2785_C56380";
                $LsqlV .= $separador."'".$_POST["G2785_C56380"]."'";
                $validar = 1;
            }
             
  
            $G2785_C56381 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2785_C56381"])){
                if($_POST["G2785_C56381"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2785_C56381 = $_POST["G2785_C56381"];
                    $LsqlU .= $separador." G2785_C56381 = ".$G2785_C56381."";
                    $LsqlI .= $separador." G2785_C56381";
                    $LsqlV .= $separador.$G2785_C56381;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2785_C56382"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_C56382 = '".$_POST["G2785_C56382"]."'";
                $LsqlI .= $separador."G2785_C56382";
                $LsqlV .= $separador."'".$_POST["G2785_C56382"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2785_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2785_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2785_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2785_Usuario , G2785_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2785_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2785 WHERE G2785_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2785 SET G2785_UltiGest__b =-14, G2785_GesMasImp_b =-14, G2785_TipoReintentoUG_b =0, G2785_TipoReintentoGMI_b =0, G2785_EstadoUG_b =-14, G2785_EstadoGMI_b =-14, G2785_CantidadIntentos =0, G2785_CantidadIntentosGMI_b =0 WHERE G2785_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2810_ConsInte__b, G2810_C55730, G2810_C55731, G2810_C55732 FROM ".$BaseDatos.".G2810  ";

        $SQL .= " WHERE G2810_C55730 = '".$numero."'"; 

        $SQL .= " ORDER BY G2810_C55730";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2810_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2810_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2810_C55730)."</cell>";

                if($fila->G2810_C55731 != ''){
                    echo "<cell>". explode(' ', $fila->G2810_C55731)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell><![CDATA[". ($fila->G2810_C55732)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2810 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2810(";
            $LsqlV = " VALUES ("; 

                $G2810_C55731 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2810_C55731"])){    
                    if($_POST["G2810_C55731"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2810_C55731 = "'".str_replace(' ', '',$_POST["G2810_C55731"])." 00:00:00'";
                        $LsqlU .= $separador." G2810_C55731 = ".$G2810_C55731;
                        $LsqlI .= $separador." G2810_C55731";
                        $LsqlV .= $separador.$G2810_C55731;
                        $validar = 1;
                    }
                }
  

                if(isset($_POST["G2810_C55732"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2810_C55732 = '".$_POST["G2810_C55732"]."'";
                    $LsqlI .= $separador."G2810_C55732";
                    $LsqlV .= $separador."'".$_POST["G2810_C55732"]."'";
                    $validar = 1;
                }
                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2810_C55730 = $numero;
                    $LsqlU .= ", G2810_C55730 = ".$G2810_C55730."";
                    $LsqlI .= ", G2810_C55730";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2810_Usuario ,  G2810_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2810_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2810 WHERE  G2810_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
