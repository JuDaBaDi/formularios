<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55283")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55283");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2412_ConsInte__b, G2412_FechaInsercion , G2412_Usuario ,  G2412_CodigoMiembro  , G2412_PoblacionOrigen , G2412_EstadoDiligenciamiento ,  G2412_IdLlamada , G2412_C47422 as principal ,G2412_C47421,G2412_C47422,G2412_C47423,G2412_C47424,G2412_C47425,G2412_C47426,G2412_C47427,G2412_C47428,G2412_C55282,G2412_C55283,G2412_C47410,G2412_C47411,G2412_C47412,G2412_C47413,G2412_C47414,G2412_C47415,G2412_C47416,G2412_C47417,G2412_C47418 FROM '.$BaseDatos.'.G2412 WHERE G2412_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2412_C47421'] = $key->G2412_C47421;

                $datos[$i]['G2412_C47422'] = $key->G2412_C47422;

                $datos[$i]['G2412_C47423'] = $key->G2412_C47423;

                $datos[$i]['G2412_C47424'] = $key->G2412_C47424;

                $datos[$i]['G2412_C47425'] = $key->G2412_C47425;

                $datos[$i]['G2412_C47426'] = $key->G2412_C47426;

                $datos[$i]['G2412_C47427'] = $key->G2412_C47427;

                $datos[$i]['G2412_C47428'] = $key->G2412_C47428;

                $datos[$i]['G2412_C55282'] = $key->G2412_C55282;

                $datos[$i]['G2412_C55283'] = $key->G2412_C55283;

                $datos[$i]['G2412_C47410'] = $key->G2412_C47410;

                $datos[$i]['G2412_C47411'] = $key->G2412_C47411;

                $datos[$i]['G2412_C47412'] = explode(' ', $key->G2412_C47412)[0];
  
                $hora = '';
                if(!is_null($key->G2412_C47413)){
                    $hora = explode(' ', $key->G2412_C47413)[1];
                }

                $datos[$i]['G2412_C47413'] = $hora;

                $datos[$i]['G2412_C47414'] = $key->G2412_C47414;

                $datos[$i]['G2412_C47415'] = $key->G2412_C47415;

                $datos[$i]['G2412_C47416'] = $key->G2412_C47416;

                $datos[$i]['G2412_C47417'] = $key->G2412_C47417;

                $datos[$i]['G2412_C47418'] = $key->G2412_C47418;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2412";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2412_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2412_ConsInte__b as id,  G2412_C47422 as camp1 , G2412_C55283 as camp2 
                     FROM ".$BaseDatos.".G2412  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2412_ConsInte__b as id,  G2412_C47422 as camp1 , G2412_C55283 as camp2  
                    FROM ".$BaseDatos.".G2412  JOIN ".$BaseDatos.".G2412_M".$_POST['muestra']." ON G2412_ConsInte__b = G2412_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2412_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2412_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2412_C47422 LIKE '%".$B."%' OR G2412_C55283 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2412_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2412");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2412_ConsInte__b, G2412_FechaInsercion , G2412_Usuario ,  G2412_CodigoMiembro  , G2412_PoblacionOrigen , G2412_EstadoDiligenciamiento ,  G2412_IdLlamada , G2412_C47422 as principal ,G2412_C47421,G2412_C47422,G2412_C47423,G2412_C47424,G2412_C47425, a.LISOPC_Nombre____b as G2412_C47426, b.LISOPC_Nombre____b as G2412_C47427,G2412_C47428,G2412_C55282,G2412_C55283, c.LISOPC_Nombre____b as G2412_C47410, d.LISOPC_Nombre____b as G2412_C47411,G2412_C47412,G2412_C47413,G2412_C47414,G2412_C47415,G2412_C47416,G2412_C47417,G2412_C47418 FROM '.$BaseDatos.'.G2412 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2412_C47426 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2412_C47427 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2412_C47410 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2412_C47411';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2412_C47413)){
                    $hora_a = explode(' ', $fila->G2412_C47413)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2412_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2412_ConsInte__b , ($fila->G2412_C47421) , ($fila->G2412_C47422) , ($fila->G2412_C47423) , ($fila->G2412_C47424) , ($fila->G2412_C47425) , ($fila->G2412_C47426) , ($fila->G2412_C47427) , ($fila->G2412_C47428) , ($fila->G2412_C55282) , ($fila->G2412_C55283) , ($fila->G2412_C47410) , ($fila->G2412_C47411) , explode(' ', $fila->G2412_C47412)[0] , $hora_a , ($fila->G2412_C47414) , ($fila->G2412_C47415) , ($fila->G2412_C47416) , ($fila->G2412_C47417) , ($fila->G2412_C47418) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2412 WHERE G2412_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2412";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2412_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2412_ConsInte__b as id,  G2412_C47422 as camp1 , G2412_C55283 as camp2  FROM '.$BaseDatos.'.G2412 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2412_ConsInte__b as id,  G2412_C47422 as camp1 , G2412_C55283 as camp2  
                    FROM ".$BaseDatos.".G2412  JOIN ".$BaseDatos.".G2412_M".$_POST['muestra']." ON G2412_ConsInte__b = G2412_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2412_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2412_C47422 LIKE "%'.$B.'%" OR G2412_C55283 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2412_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2412 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2412(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2412_C47421"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47421 = '".$_POST["G2412_C47421"]."'";
                $LsqlI .= $separador."G2412_C47421";
                $LsqlV .= $separador."'".$_POST["G2412_C47421"]."'";
                $validar = 1;
            }
             
  
            $G2412_C47422 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2412_C47422"])){
                if($_POST["G2412_C47422"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2412_C47422 = $_POST["G2412_C47422"];
                    $LsqlU .= $separador." G2412_C47422 = ".$G2412_C47422."";
                    $LsqlI .= $separador." G2412_C47422";
                    $LsqlV .= $separador.$G2412_C47422;
                    $validar = 1;
                }
            }
  
            $G2412_C47423 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2412_C47423"])){
                if($_POST["G2412_C47423"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2412_C47423 = $_POST["G2412_C47423"];
                    $LsqlU .= $separador." G2412_C47423 = ".$G2412_C47423."";
                    $LsqlI .= $separador." G2412_C47423";
                    $LsqlV .= $separador.$G2412_C47423;
                    $validar = 1;
                }
            }
  
            $G2412_C47424 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2412_C47424"])){
                if($_POST["G2412_C47424"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2412_C47424 = $_POST["G2412_C47424"];
                    $LsqlU .= $separador." G2412_C47424 = ".$G2412_C47424."";
                    $LsqlI .= $separador." G2412_C47424";
                    $LsqlV .= $separador.$G2412_C47424;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2412_C47425"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47425 = '".$_POST["G2412_C47425"]."'";
                $LsqlI .= $separador."G2412_C47425";
                $LsqlV .= $separador."'".$_POST["G2412_C47425"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C47426"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47426 = '".$_POST["G2412_C47426"]."'";
                $LsqlI .= $separador."G2412_C47426";
                $LsqlV .= $separador."'".$_POST["G2412_C47426"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C47427"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47427 = '".$_POST["G2412_C47427"]."'";
                $LsqlI .= $separador."G2412_C47427";
                $LsqlV .= $separador."'".$_POST["G2412_C47427"]."'";
                $validar = 1;
            }
             
  
            $G2412_C47428 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2412_C47428"])){
                if($_POST["G2412_C47428"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2412_C47428 = $_POST["G2412_C47428"];
                    $LsqlU .= $separador." G2412_C47428 = ".$G2412_C47428."";
                    $LsqlI .= $separador." G2412_C47428";
                    $LsqlV .= $separador.$G2412_C47428;
                    $validar = 1;
                }
            }
  
            $G2412_C55282 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2412_C55282"])){
                if($_POST["G2412_C55282"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2412_C55282 = $_POST["G2412_C55282"];
                    $LsqlU .= $separador." G2412_C55282 = ".$G2412_C55282."";
                    $LsqlI .= $separador." G2412_C55282";
                    $LsqlV .= $separador.$G2412_C55282;
                    $validar = 1;
                }
            }
  
            $G2412_C55283 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2412_C55283"])){
                if($_POST["G2412_C55283"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2412_C55283 = $_POST["G2412_C55283"];
                    $LsqlU .= $separador." G2412_C55283 = ".$G2412_C55283."";
                    $LsqlI .= $separador." G2412_C55283";
                    $LsqlV .= $separador.$G2412_C55283;
                    $validar = 1;
                }
            }
 
            $G2412_C47410 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2412_C47410 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2412_C47410 = ".$G2412_C47410;
                    $LsqlI .= $separador." G2412_C47410";
                    $LsqlV .= $separador.$G2412_C47410;
                    $validar = 1;

                    
                }
            }
 
            $G2412_C47411 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2412_C47411 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2412_C47411 = ".$G2412_C47411;
                    $LsqlI .= $separador." G2412_C47411";
                    $LsqlV .= $separador.$G2412_C47411;
                    $validar = 1;
                }
            }
 
            $G2412_C47412 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2412_C47412 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2412_C47412 = ".$G2412_C47412;
                    $LsqlI .= $separador." G2412_C47412";
                    $LsqlV .= $separador.$G2412_C47412;
                    $validar = 1;
                }
            }
 
            $G2412_C47413 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2412_C47413 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2412_C47413 = ".$G2412_C47413;
                    $LsqlI .= $separador." G2412_C47413";
                    $LsqlV .= $separador.$G2412_C47413;
                    $validar = 1;
                }
            }
 
            $G2412_C47414 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2412_C47414 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2412_C47414 = ".$G2412_C47414;
                    $LsqlI .= $separador." G2412_C47414";
                    $LsqlV .= $separador.$G2412_C47414;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2412_C47415"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47415 = '".$_POST["G2412_C47415"]."'";
                $LsqlI .= $separador."G2412_C47415";
                $LsqlV .= $separador."'".$_POST["G2412_C47415"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C47416"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47416 = '".$_POST["G2412_C47416"]."'";
                $LsqlI .= $separador."G2412_C47416";
                $LsqlV .= $separador."'".$_POST["G2412_C47416"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C47417"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47417 = '".$_POST["G2412_C47417"]."'";
                $LsqlI .= $separador."G2412_C47417";
                $LsqlV .= $separador."'".$_POST["G2412_C47417"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C47418"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47418 = '".$_POST["G2412_C47418"]."'";
                $LsqlI .= $separador."G2412_C47418";
                $LsqlV .= $separador."'".$_POST["G2412_C47418"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C47419"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C47419 = '".$_POST["G2412_C47419"]."'";
                $LsqlI .= $separador."G2412_C47419";
                $LsqlV .= $separador."'".$_POST["G2412_C47419"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2412_C55942"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_C55942 = '".$_POST["G2412_C55942"]."'";
                $LsqlI .= $separador."G2412_C55942";
                $LsqlV .= $separador."'".$_POST["G2412_C55942"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2412_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2412_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2412_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2412_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2412_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2412_Usuario , G2412_FechaInsercion, G2412_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2412_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2412 WHERE G2412_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
