
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2199/G2199_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2199_ConsInte__b as id, G2199_C42989 as camp1 , G2199_C42990 as camp2 FROM ".$BaseDatos.".G2199  WHERE G2199_Usuario = ".$idUsuario." ORDER BY G2199_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2199_ConsInte__b as id, G2199_C42989 as camp1 , G2199_C42990 as camp2 FROM ".$BaseDatos.".G2199  ORDER BY G2199_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2199_ConsInte__b as id, G2199_C42989 as camp1 , G2199_C42990 as camp2 FROM ".$BaseDatos.".G2199 JOIN ".$BaseDatos.".G2199_M".$resultEstpas->muestr." ON G2199_ConsInte__b = G2199_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2199_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2199_ConsInte__b as id, G2199_C42989 as camp1 , G2199_C42990 as camp2 FROM ".$BaseDatos.".G2199 JOIN ".$BaseDatos.".G2199_M".$resultEstpas->muestr." ON G2199_ConsInte__b = G2199_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2199_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2199_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2199_ConsInte__b as id, G2199_C42989 as camp1 , G2199_C42990 as camp2 FROM ".$BaseDatos.".G2199  ORDER BY G2199_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){


    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="6197" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6197c">
                BIENVENIDA 
            </a>
        </h4>
        
    </div>
    <div id="s_6197c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días /tardes/noches, Tengo el gusto de hablar con Sr/Sra (NOMBRE DEL AFILIADO) Mucho gusto mi nombre es (NOMBRE DEL AGENTE), me estoy comunicando de la EPS CAPRESOCA.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">¿Como se encuentra el dia de hoy? (ESPERAR RESPUESTA DEL CLIENTE)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Le informamos el motivo de nuestra llamada, nos encontramos realizando una consulta nuestros afiliados, sobre la crisis sanitaria sobre el COVID-19</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6189" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6189c">
                DATOS PERSONALES
            </a>
        </h4>
        
    </div>
    <div id="s_6189c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43057" id="LblG2199_C43057">¿De donde se realiza la llamada?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43057" id="G2199_C43057">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2398 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42997" id="LblG2199_C42997">A qué EPS está Afiliado </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42997" id="G2199_C42997">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2388 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42998" id="LblG2199_C42998">Tipo Afiliado</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42998" id="G2199_C42998">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2381 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42989" id="LblG2199_C42989">Nombres </label>
                        <input type="text" class="form-control input-sm" id="G2199_C42989" value="<?php if (isset($_GET['G2199_C42989'])) {
                            echo $_GET['G2199_C42989'];
                        } ?>"  name="G2199_C42989"  placeholder="Nombres ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42990" id="LblG2199_C42990">Apellidos</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42990" value="<?php if (isset($_GET['G2199_C42990'])) {
                            echo $_GET['G2199_C42990'];
                        } ?>"  name="G2199_C42990"  placeholder="Apellidos">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42991" id="LblG2199_C42991">Acudiente</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42991" value="<?php if (isset($_GET['G2199_C42991'])) {
                            echo $_GET['G2199_C42991'];
                        } ?>"  name="G2199_C42991"  placeholder="Acudiente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42992" id="LblG2199_C42992">Edad</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42992" value="<?php if (isset($_GET['G2199_C42992'])) {
                            echo $_GET['G2199_C42992'];
                        } ?>"  name="G2199_C42992"  placeholder="Edad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42993" id="LblG2199_C42993">Sexo </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42993" id="G2199_C42993">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2377 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42994" id="LblG2199_C42994">Número de teléfono </label>
                        <input type="text" class="form-control input-sm" id="G2199_C42994" value="<?php if (isset($_GET['G2199_C42994'])) {
                            echo $_GET['G2199_C42994'];
                        } ?>"  name="G2199_C42994"  placeholder="Número de teléfono ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42995" id="LblG2199_C42995">Tipo Identificación</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42995" id="G2199_C42995">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2378 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2199_C42996" id="LblG2199_C42996">Número de Identificación</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2199_C42996'])) {
                            echo $_GET['G2199_C42996'];
                        } ?>"  name="G2199_C42996" id="G2199_C42996" placeholder="Número de Identificación">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42999" id="LblG2199_C42999">Dirección</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42999" value="<?php if (isset($_GET['G2199_C42999'])) {
                            echo $_GET['G2199_C42999'];
                        } ?>"  name="G2199_C42999"  placeholder="Dirección">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43000" id="LblG2199_C43000">Ciudad</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43000" id="G2199_C43000">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2382 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C43052" id="LblG2199_C43052">Correo electronico</label>
                        <input type="text" class="form-control input-sm" id="G2199_C43052" value="<?php if (isset($_GET['G2199_C43052'])) {
                            echo $_GET['G2199_C43052'];
                        } ?>"  name="G2199_C43052"  placeholder="Correo electronico">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2199_C43604" id="LblG2199_C43604">Numero celular actualizado del afiliado</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2199_C43604'])) {
                            echo $_GET['G2199_C43604'];
                        } ?>"  name="G2199_C43604" id="G2199_C43604" placeholder="Numero celular actualizado del afiliado">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="6191" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42983" id="LblG2199_C42983">Agente</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42983" value="<?php echo getNombreUser($token);?>" readonly name="G2199_C42983"  placeholder="Agente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42984" id="LblG2199_C42984">Fecha</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42984" value="<?php echo date('Y-m-d');?>" readonly name="G2199_C42984"  placeholder="Fecha">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42985" id="LblG2199_C42985">Hora</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42985" value="<?php echo date('H:i:s');?>" readonly name="G2199_C42985"  placeholder="Hora">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C42986" id="LblG2199_C42986">Campaña</label>
                        <input type="text" class="form-control input-sm" id="G2199_C42986" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G2199_C42986"  placeholder="Campaña">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="6193" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6193c">
                SINTOMAS Y CLASIFICACION
            </a>
        </h4>
        
    </div>
    <div id="s_6193c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Preguntar al paciente sobre los siguientes síntomas, es caso de estudio al presentar más de dos a la vez en los últimos 14 dias.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43026" id="LblG2199_C43026">Fiebre mayor o igual a 38 °C</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43026" id="G2199_C43026" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43027" id="LblG2199_C43027">Tos</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43027" id="G2199_C43027" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43028" id="LblG2199_C43028">Secreción Nasal</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43028" id="G2199_C43028" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43029" id="LblG2199_C43029">Malestar General</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43029" id="G2199_C43029" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43030" id="LblG2199_C43030">Dificultad respiratoria</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43030" id="G2199_C43030" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43031" id="LblG2199_C43031">Dolor de Garganta</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43031" id="G2199_C43031" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43032" id="LblG2199_C43032">Fatiga</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43032" id="G2199_C43032" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43033" id="LblG2199_C43033">Sensación de falta de aire</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43033" id="G2199_C43033" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43034" id="LblG2199_C43034">Odinofágia</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43034" id="G2199_C43034" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43035" id="LblG2199_C43035">Adinamia</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43035" id="G2199_C43035" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43036" id="LblG2199_C43036">Respiración más rápida de lo normal</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43036" id="G2199_C43036" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43037" id="LblG2199_C43037">El pecho le suena o le duele al respirar</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43037" id="G2199_C43037" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43038" id="LblG2199_C43038">Somnolencia o dificultad para despertar</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43038" id="G2199_C43038" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43039" id="LblG2199_C43039">Ataques o convulsiones</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43039" id="G2199_C43039" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43040" id="LblG2199_C43040">Decaimiento</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43040" id="G2199_C43040" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43041" id="LblG2199_C43041">Deterioro del estado general en forma rápida</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43041" id="G2199_C43041" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43042" id="LblG2199_C43042">Ninguna de las anteriores</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43042" id="G2199_C43042" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2199_C43003" id="LblG2199_C43003">Fecha en la que comenzaron los síntomas en caso de haberlos</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2199_C43003'])) {
                            echo $_GET['G2199_C43003'];
                        } ?>"  name="G2199_C43003" id="G2199_C43003" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43004" id="LblG2199_C43004">Clasifica este paciente como Caso de Estudio</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43004" id="G2199_C43004">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="6190" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="6192" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6192c">
                PREGUNTAS INICIALES
            </a>
        </h4>
        
    </div>
    <div id="s_6192c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42987" id="LblG2199_C42987">Antecedentes de Viaje: Ud realizó algún viaje desde o hacia las zonas donde se presenta el virus o ha tenido cercanía con personas contagiadas en los últimos 14 días?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42987" id="G2199_C42987">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42988" id="LblG2199_C42988">¿Es Trabajador de la salud u otro personal del ámbito hospitalario que haya tenido contacto estrecho* con caso confirmado para enfermedad por nuevo coronavirus (COVID-19).?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42988" id="G2199_C42988">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2199_C43055" id="LblG2199_C43055">Trabajador de la Salud. ¿Donde trabaja?</label>
                        <textarea class="form-control input-sm" name="G2199_C43055" id="G2199_C43055"  value="<?php if (isset($_GET['G2199_C43055'])) {
                            echo $_GET['G2199_C43055'];
                        } ?>" placeholder="Trabajador de la Salud. ¿Donde trabaja?"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43001" id="LblG2199_C43001">Ha tenido contacto estrecho* en los últimos 14 días con un caso confirmado con infección respiratoria aguda grave asociada al nuevo coronavirus 2019 (COVID-19)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43001" id="G2199_C43001">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6194" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6194c">
                PREGUNTAS CASO DE ESTUDIO
            </a>
        </h4>
        
    </div>
    <div id="s_6194c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43010" id="LblG2199_C43010">Nexos epidemiologicos.</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43010" id="G2199_C43010">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C43058" id="LblG2199_C43058">¿Quién o quienes?</label>
                        <input type="text" class="form-control input-sm" id="G2199_C43058" value="<?php if (isset($_GET['G2199_C43058'])) {
                            echo $_GET['G2199_C43058'];
                        } ?>"  name="G2199_C43058"  placeholder="¿Quién o quienes?">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43053" id="LblG2199_C43053">Consume o ha consumido Ibuprofeno?</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43053" id="G2199_C43053" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43054" id="LblG2199_C43054">Ninguno de las anteriores</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43054" id="G2199_C43054" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43011" id="LblG2199_C43011">Consume medicamentos antinflamatorios o acetaminofén.</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43011" id="G2199_C43011" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6195" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6195c">
                CORMOBILIDAD 
            </a>
        </h4>
        
    </div>
    <div id="s_6195c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43043" id="LblG2199_C43043">Enfermedad Cardiovascular</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43043" id="G2199_C43043" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43044" id="LblG2199_C43044">Enfermedad Respiratoria Crónica</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43044" id="G2199_C43044" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43045" id="LblG2199_C43045">Hipertensión</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43045" id="G2199_C43045" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43046" id="LblG2199_C43046">Cáncer</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43046" id="G2199_C43046" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43048" id="LblG2199_C43048">Diabetes</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43048" id="G2199_C43048" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43049" id="LblG2199_C43049">VIH</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43049" id="G2199_C43049" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43047" id="LblG2199_C43047">Uso de corticoides o inmunosupresores</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43047" id="G2199_C43047" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43050" id="LblG2199_C43050">EPOC</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43050" id="G2199_C43050" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G2199_C43051" id="LblG2199_C43051">Fumador</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G2199_C43051" id="G2199_C43051" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43015" id="LblG2199_C43015">Está actualmente hospitalizado por IRA IRAG IRAGi?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43015" id="G2199_C43015">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2199_C43016" id="LblG2199_C43016">En que lugar se encuentra hospitalizado?</label>
                        <input type="text" class="form-control input-sm" id="G2199_C43016" value="<?php if (isset($_GET['G2199_C43016'])) {
                            echo $_GET['G2199_C43016'];
                        } ?>"  name="G2199_C43016"  placeholder="En que lugar se encuentra hospitalizado?">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2199_C43017" id="LblG2199_C43017">Fecha de ingreso hospitalización</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2199_C43017'])) {
                            echo $_GET['G2199_C43017'];
                        } ?>"  name="G2199_C43017" id="G2199_C43017" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2199_C43018" id="LblG2199_C43018">Fecha en la cual se tomó la muestra</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2199_C43018'])) {
                            echo $_GET['G2199_C43018'];
                        } ?>"  name="G2199_C43018" id="G2199_C43018" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6196" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6196c">
                CONCLUSIONES DEL ENTREVISTADOR 
            </a>
        </h4>
        
    </div>
    <div id="s_6196c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43019" id="LblG2199_C43019">¿Ya le han tomado muestra para COVID-19?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43019" id="G2199_C43019">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43020" id="LblG2199_C43020">¿Muerte probable por COVID-19?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43020" id="G2199_C43020">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2199_C43021" id="LblG2199_C43021">Observaciones - Describa la Ruta tratamiento a seguir del paciente *</label>
                        <textarea class="form-control input-sm" name="G2199_C43021" id="G2199_C43021"  value="<?php if (isset($_GET['G2199_C43021'])) {
                            echo $_GET['G2199_C43021'];
                        } ?>" placeholder="Observaciones - Describa la Ruta tratamiento a seguir del paciente *"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43022" id="LblG2199_C43022">Clasificación del Paciente de acuerdo con LINEAMIENTOS PARA LA DETECCIÓN Y MANEJO DE CASOS DE COVID-19 POR LOS PRESTADORES DE SERVICIOS DE SALUD EN COLOMBIA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43022" id="G2199_C43022">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2399 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2199_C42978">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2397;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2199_C42978">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2397;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2199_C42979">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2199_C42980" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2199_C42981" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2199_C42982" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2199/G2199_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2199_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2199_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2199_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2199_C43057").val(item.G2199_C43057).trigger("change");  
                $("#G2199_C42997").val(item.G2199_C42997).trigger("change");  
                $("#G2199_C42998").val(item.G2199_C42998).trigger("change");  
                $("#G2199_C42989").val(item.G2199_C42989); 
                $("#G2199_C42990").val(item.G2199_C42990); 
                $("#G2199_C42991").val(item.G2199_C42991); 
                $("#G2199_C42992").val(item.G2199_C42992); 
                $("#G2199_C42993").val(item.G2199_C42993).trigger("change");  
                $("#G2199_C42994").val(item.G2199_C42994); 
                $("#G2199_C42995").val(item.G2199_C42995).trigger("change");  
                $("#G2199_C42996").val(item.G2199_C42996); 
                $("#G2199_C42999").val(item.G2199_C42999); 
                $("#G2199_C43000").val(item.G2199_C43000).trigger("change");  
                $("#G2199_C43052").val(item.G2199_C43052); 
                $("#G2199_C43604").val(item.G2199_C43604); 
                $("#G2199_C42978").val(item.G2199_C42978).trigger("change");  
                $("#G2199_C42979").val(item.G2199_C42979).trigger("change");  
                $("#G2199_C42980").val(item.G2199_C42980); 
                $("#G2199_C42981").val(item.G2199_C42981); 
                $("#G2199_C42982").val(item.G2199_C42982); 
                $("#G2199_C42983").val(item.G2199_C42983); 
                $("#G2199_C42984").val(item.G2199_C42984); 
                $("#G2199_C42985").val(item.G2199_C42985); 
                $("#G2199_C42986").val(item.G2199_C42986); 
                $("#G2199_C42987").val(item.G2199_C42987).trigger("change");  
                $("#G2199_C42988").val(item.G2199_C42988).trigger("change");  
                $("#G2199_C43055").val(item.G2199_C43055); 
                $("#G2199_C43001").val(item.G2199_C43001).trigger("change");    
                if(item.G2199_C43002 == 1){
                    $("#G2199_C43002").attr('checked', true);
                }    
                if(item.G2199_C43026 == 1){
                    $("#G2199_C43026").attr('checked', true);
                }    
                if(item.G2199_C43027 == 1){
                    $("#G2199_C43027").attr('checked', true);
                }    
                if(item.G2199_C43028 == 1){
                    $("#G2199_C43028").attr('checked', true);
                }    
                if(item.G2199_C43029 == 1){
                    $("#G2199_C43029").attr('checked', true);
                }    
                if(item.G2199_C43030 == 1){
                    $("#G2199_C43030").attr('checked', true);
                }    
                if(item.G2199_C43031 == 1){
                    $("#G2199_C43031").attr('checked', true);
                }    
                if(item.G2199_C43032 == 1){
                    $("#G2199_C43032").attr('checked', true);
                }    
                if(item.G2199_C43033 == 1){
                    $("#G2199_C43033").attr('checked', true);
                }    
                if(item.G2199_C43034 == 1){
                    $("#G2199_C43034").attr('checked', true);
                }    
                if(item.G2199_C43035 == 1){
                    $("#G2199_C43035").attr('checked', true);
                }    
                if(item.G2199_C43036 == 1){
                    $("#G2199_C43036").attr('checked', true);
                }    
                if(item.G2199_C43037 == 1){
                    $("#G2199_C43037").attr('checked', true);
                }    
                if(item.G2199_C43038 == 1){
                    $("#G2199_C43038").attr('checked', true);
                }    
                if(item.G2199_C43039 == 1){
                    $("#G2199_C43039").attr('checked', true);
                }    
                if(item.G2199_C43040 == 1){
                    $("#G2199_C43040").attr('checked', true);
                }    
                if(item.G2199_C43041 == 1){
                    $("#G2199_C43041").attr('checked', true);
                }    
                if(item.G2199_C43042 == 1){
                    $("#G2199_C43042").attr('checked', true);
                }  
                $("#G2199_C43003").val(item.G2199_C43003); 
                $("#G2199_C43004").val(item.G2199_C43004).trigger("change");  
                $("#G2199_C43010").val(item.G2199_C43010).trigger("change");  
                $("#G2199_C43058").val(item.G2199_C43058);   
                if(item.G2199_C43053 == 1){
                    $("#G2199_C43053").attr('checked', true);
                }    
                if(item.G2199_C43054 == 1){
                    $("#G2199_C43054").attr('checked', true);
                }    
                if(item.G2199_C43011 == 1){
                    $("#G2199_C43011").attr('checked', true);
                }    
                if(item.G2199_C43043 == 1){
                    $("#G2199_C43043").attr('checked', true);
                }    
                if(item.G2199_C43044 == 1){
                    $("#G2199_C43044").attr('checked', true);
                }    
                if(item.G2199_C43045 == 1){
                    $("#G2199_C43045").attr('checked', true);
                }    
                if(item.G2199_C43046 == 1){
                    $("#G2199_C43046").attr('checked', true);
                }    
                if(item.G2199_C43048 == 1){
                    $("#G2199_C43048").attr('checked', true);
                }    
                if(item.G2199_C43049 == 1){
                    $("#G2199_C43049").attr('checked', true);
                }    
                if(item.G2199_C43047 == 1){
                    $("#G2199_C43047").attr('checked', true);
                }    
                if(item.G2199_C43050 == 1){
                    $("#G2199_C43050").attr('checked', true);
                }    
                if(item.G2199_C43051 == 1){
                    $("#G2199_C43051").attr('checked', true);
                }  
                $("#G2199_C43015").val(item.G2199_C43015).trigger("change");  
                $("#G2199_C43016").val(item.G2199_C43016); 
                $("#G2199_C43017").val(item.G2199_C43017); 
                $("#G2199_C43018").val(item.G2199_C43018); 
                $("#G2199_C43019").val(item.G2199_C43019).trigger("change");  
                $("#G2199_C43020").val(item.G2199_C43020).trigger("change");  
                $("#G2199_C43021").val(item.G2199_C43021); 
                $("#G2199_C43022").val(item.G2199_C43022).trigger("change");    
                if(item.G2199_C43023 == 1){
                    $("#G2199_C43023").attr('checked', true);
                }    
                if(item.G2199_C43024 == 1){
                    $("#G2199_C43024").attr('checked', true);
                }    
                if(item.G2199_C43025 == 1){
                    $("#G2199_C43025").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2199_C43057").select2();

    $("#G2199_C42997").select2();

    $("#G2199_C42998").select2();

    $("#G2199_C42993").select2();

    $("#G2199_C42995").select2();

    $("#G2199_C43000").select2();

    $("#G2199_C43004").select2();

    $("#G2199_C42987").select2();

    $("#G2199_C42988").select2();

    $("#G2199_C43001").select2();

    $("#G2199_C43010").select2();

    $("#G2199_C43015").select2();

    $("#G2199_C43019").select2();

    $("#G2199_C43020").select2();

    $("#G2199_C43022").select2();
        //datepickers
        

        $("#G2199_C42980").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2199_C43003").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2199_C43017").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2199_C43018").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2199_C42981").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G2199_C42996").numeric();
                
        $("#G2199_C43604").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ¿De donde se realiza la llamada? 

    $("#G2199_C43057").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para A qué EPS está Afiliado  

    $("#G2199_C42997").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo Afiliado 

    $("#G2199_C42998").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Sexo  

    $("#G2199_C42993").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo Identificación 

    $("#G2199_C42995").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Ciudad 

    $("#G2199_C43000").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Clasifica este paciente como Caso de Estudio 

    $("#G2199_C43004").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Antecedentes de Viaje: Ud realizó algún viaje desde o hacia las zonas donde se presenta el virus o ha tenido cercanía con personas contagiadas en los últimos 14 días? 

    $("#G2199_C42987").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Es Trabajador de la salud u otro personal del ámbito hospitalario que haya tenido contacto estrecho* con caso confirmado para enfermedad por nuevo coronavirus (COVID-19).? 

    $("#G2199_C42988").change(function(){ 
        $("#G2199_C43055").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '31128'){
            $("#G2199_C43055").closest(".form-group").removeClass("has-error"); 
            $("#G2199_C43055").prop('disabled', true); 
          
        }
     
        //Esto es la parte de las listas dependientes
        

    });

    //function para Ha tenido contacto estrecho* en los últimos 14 días con un caso confirmado con infección respiratoria aguda grave asociada al nuevo coronavirus 2019 (COVID-19) 

    $("#G2199_C43001").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Nexos epidemiologicos. 

    $("#G2199_C43010").change(function(){ 
        $("#G2199_C43058").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '31128'){
            $("#G2199_C43058").closest(".form-group").removeClass("has-error"); 
            $("#G2199_C43058").prop('disabled', true); 
          
        }
     
        //Esto es la parte de las listas dependientes
        

    });

    //function para Está actualmente hospitalizado por IRA IRAG IRAGi? 

    $("#G2199_C43015").change(function(){ 
        $("#G2199_C43016").prop('disabled', false);
    
        $("#G2199_C43017").prop('disabled', false);
    
        $("#G2199_C43018").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '31128'){
            $("#G2199_C43016").closest(".form-group").removeClass("has-error"); 
            $("#G2199_C43016").prop('disabled', true); 
          
            $("#G2199_C43017").closest(".form-group").removeClass("has-error"); 
            $("#G2199_C43017").prop('disabled', true); 
          
            $("#G2199_C43018").closest(".form-group").removeClass("has-error"); 
            $("#G2199_C43018").prop('disabled', true); 
          
        }
     
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Ya le han tomado muestra para COVID-19? 

    $("#G2199_C43019").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Muerte probable por COVID-19? 

    $("#G2199_C43020").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Clasificación del Paciente de acuerdo con LINEAMIENTOS PARA LA DETECCIÓN Y MANEJO DE CASOS DE COVID-19 POR LOS PRESTADORES DE SERVICIOS DE SALUD EN COLOMBIA 

    $("#G2199_C43022").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                    $("#G2199_C43057").val(item.G2199_C43057).trigger("change"); 
 
                    $("#G2199_C42997").val(item.G2199_C42997).trigger("change"); 
 
                    $("#G2199_C42998").val(item.G2199_C42998).trigger("change"); 
 
                                                $("#G2199_C42989").val(item.G2199_C42989);
 
                                                $("#G2199_C42990").val(item.G2199_C42990);
 
                                                $("#G2199_C42991").val(item.G2199_C42991);
 
                                                $("#G2199_C42992").val(item.G2199_C42992);
 
                    $("#G2199_C42993").val(item.G2199_C42993).trigger("change"); 
 
                                                $("#G2199_C42994").val(item.G2199_C42994);
 
                    $("#G2199_C42995").val(item.G2199_C42995).trigger("change"); 
 
                                                $("#G2199_C42996").val(item.G2199_C42996);
 
                                                $("#G2199_C42999").val(item.G2199_C42999);
 
                    $("#G2199_C43000").val(item.G2199_C43000).trigger("change"); 
 
                                                $("#G2199_C43052").val(item.G2199_C43052);
 
                                                $("#G2199_C43604").val(item.G2199_C43604);
 
                    $("#G2199_C42978").val(item.G2199_C42978).trigger("change"); 
 
                    $("#G2199_C42979").val(item.G2199_C42979).trigger("change"); 
 
                                                $("#G2199_C42980").val(item.G2199_C42980);
 
                                                $("#G2199_C42981").val(item.G2199_C42981);
 
                                                $("#G2199_C42982").val(item.G2199_C42982);
 
                                                $("#G2199_C42983").val(item.G2199_C42983);
 
                                                $("#G2199_C42984").val(item.G2199_C42984);
 
                                                $("#G2199_C42985").val(item.G2199_C42985);
 
                                                $("#G2199_C42986").val(item.G2199_C42986);
 
                    $("#G2199_C42987").val(item.G2199_C42987).trigger("change"); 
 
                    $("#G2199_C42988").val(item.G2199_C42988).trigger("change"); 
 
                                                $("#G2199_C43055").val(item.G2199_C43055);
 
                    $("#G2199_C43001").val(item.G2199_C43001).trigger("change"); 
      
                                                if(item.G2199_C43002 == 1){
                                                   $("#G2199_C43002").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43026 == 1){
                                                   $("#G2199_C43026").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43027 == 1){
                                                   $("#G2199_C43027").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43028 == 1){
                                                   $("#G2199_C43028").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43029 == 1){
                                                   $("#G2199_C43029").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43030 == 1){
                                                   $("#G2199_C43030").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43031 == 1){
                                                   $("#G2199_C43031").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43032 == 1){
                                                   $("#G2199_C43032").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43033 == 1){
                                                   $("#G2199_C43033").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43034 == 1){
                                                   $("#G2199_C43034").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43035 == 1){
                                                   $("#G2199_C43035").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43036 == 1){
                                                   $("#G2199_C43036").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43037 == 1){
                                                   $("#G2199_C43037").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43038 == 1){
                                                   $("#G2199_C43038").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43039 == 1){
                                                   $("#G2199_C43039").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43040 == 1){
                                                   $("#G2199_C43040").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43041 == 1){
                                                   $("#G2199_C43041").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43042 == 1){
                                                   $("#G2199_C43042").attr('checked', true);
                                                } 
 
                                                $("#G2199_C43003").val(item.G2199_C43003);
 
                    $("#G2199_C43004").val(item.G2199_C43004).trigger("change"); 
 
                    $("#G2199_C43010").val(item.G2199_C43010).trigger("change"); 
 
                                                $("#G2199_C43058").val(item.G2199_C43058);
      
                                                if(item.G2199_C43053 == 1){
                                                   $("#G2199_C43053").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43054 == 1){
                                                   $("#G2199_C43054").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43011 == 1){
                                                   $("#G2199_C43011").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43043 == 1){
                                                   $("#G2199_C43043").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43044 == 1){
                                                   $("#G2199_C43044").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43045 == 1){
                                                   $("#G2199_C43045").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43046 == 1){
                                                   $("#G2199_C43046").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43048 == 1){
                                                   $("#G2199_C43048").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43049 == 1){
                                                   $("#G2199_C43049").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43047 == 1){
                                                   $("#G2199_C43047").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43050 == 1){
                                                   $("#G2199_C43050").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43051 == 1){
                                                   $("#G2199_C43051").attr('checked', true);
                                                } 
 
                    $("#G2199_C43015").val(item.G2199_C43015).trigger("change"); 
 
                                                $("#G2199_C43016").val(item.G2199_C43016);
 
                                                $("#G2199_C43017").val(item.G2199_C43017);
 
                                                $("#G2199_C43018").val(item.G2199_C43018);
 
                    $("#G2199_C43019").val(item.G2199_C43019).trigger("change"); 
 
                    $("#G2199_C43020").val(item.G2199_C43020).trigger("change"); 
 
                                                $("#G2199_C43021").val(item.G2199_C43021);
 
                    $("#G2199_C43022").val(item.G2199_C43022).trigger("change"); 
      
                                                if(item.G2199_C43023 == 1){
                                                   $("#G2199_C43023").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43024 == 1){
                                                   $("#G2199_C43024").attr('checked', true);
                                                } 
      
                                                if(item.G2199_C43025 == 1){
                                                   $("#G2199_C43025").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','¿De donde se realiza la llamada?','A qué EPS está Afiliado ','Tipo Afiliado','Nombres ','Apellidos','Acudiente','Edad','Sexo ','Número de teléfono ','Tipo Identificación','Número de Identificación','Dirección','Ciudad','Correo electronico','Numero celular actualizado del afiliado','Agente','Fecha','Hora','Campaña','Antecedentes de Viaje: Ud realizó algún viaje desde o hacia las zonas donde se presenta el virus o ha tenido cercanía con personas contagiadas en los últimos 14 días?','¿Es Trabajador de la salud u otro personal del ámbito hospitalario que haya tenido contacto estrecho* con caso confirmado para enfermedad por nuevo coronavirus (COVID-19).?','Trabajador de la Salud. ¿Donde trabaja?','Ha tenido contacto estrecho* en los últimos 14 días con un caso confirmado con infección respiratoria aguda grave asociada al nuevo coronavirus 2019 (COVID-19)','Fiebre mayor o igual a 38 °C','Tos','Secreción Nasal','Malestar General','Dificultad respiratoria','Dolor de Garganta','Fatiga','Sensación de falta de aire','Odinofágia','Adinamia','Respiración más rápida de lo normal','El pecho le suena o le duele al respirar','Somnolencia o dificultad para despertar','Ataques o convulsiones','Decaimiento','Deterioro del estado general en forma rápida','Ninguna de las anteriores','Fecha en la que comenzaron los síntomas en caso de haberlos','Clasifica este paciente como Caso de Estudio','Nexos epidemiologicos.','¿Quién o quienes?','Consume o ha consumido Ibuprofeno?','Ninguno de las anteriores','Consume medicamentos antinflamatorios o acetaminofén.','Enfermedad Cardiovascular','Enfermedad Respiratoria Crónica','Hipertensión','Cáncer','Diabetes','VIH','Uso de corticoides o inmunosupresores','EPOC','Fumador','Está actualmente hospitalizado por IRA IRAG IRAGi?','En que lugar se encuentra hospitalizado?','Fecha de ingreso hospitalización','Fecha en la cual se tomó la muestra','¿Ya le han tomado muestra para COVID-19?','¿Muerte probable por COVID-19?','Observaciones - Describa la Ruta tratamiento a seguir del paciente *','Clasificación del Paciente de acuerdo con LINEAMIENTOS PARA LA DETECCIÓN Y MANEJO DE CASOS DE COVID-19 POR LOS PRESTADORES DE SERVICIOS DE SALUD EN COLOMBIA'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2199_C43057', 
                        index:'G2199_C43057', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2398&campo=G2199_C43057'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C42997', 
                        index:'G2199_C42997', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2388&campo=G2199_C42997'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C42998', 
                        index:'G2199_C42998', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2381&campo=G2199_C42998'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C42989', 
                        index: 'G2199_C42989', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42990', 
                        index: 'G2199_C42990', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42991', 
                        index: 'G2199_C42991', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42992', 
                        index: 'G2199_C42992', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42993', 
                        index:'G2199_C42993', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2377&campo=G2199_C42993'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C42994', 
                        index: 'G2199_C42994', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42995', 
                        index:'G2199_C42995', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2378&campo=G2199_C42995'
                        }
                    }
 
                    ,
                    {  
                        name:'G2199_C42996', 
                        index:'G2199_C42996', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C42999', 
                        index: 'G2199_C42999', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C43000', 
                        index:'G2199_C43000', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2382&campo=G2199_C43000'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43052', 
                        index: 'G2199_C43052', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2199_C43604', 
                        index:'G2199_C43604', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C42983', 
                        index: 'G2199_C42983', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42984', 
                        index: 'G2199_C42984', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42985', 
                        index: 'G2199_C42985', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42986', 
                        index: 'G2199_C42986', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C42987', 
                        index:'G2199_C42987', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C42987'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C42988', 
                        index:'G2199_C42988', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C42988'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43055', 
                        index:'G2199_C43055', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C43001', 
                        index:'G2199_C43001', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C43001'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43026', 
                        index:'G2199_C43026', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43027', 
                        index:'G2199_C43027', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43028', 
                        index:'G2199_C43028', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43029', 
                        index:'G2199_C43029', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43030', 
                        index:'G2199_C43030', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43031', 
                        index:'G2199_C43031', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43032', 
                        index:'G2199_C43032', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43033', 
                        index:'G2199_C43033', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43034', 
                        index:'G2199_C43034', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43035', 
                        index:'G2199_C43035', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43036', 
                        index:'G2199_C43036', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43037', 
                        index:'G2199_C43037', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43038', 
                        index:'G2199_C43038', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43039', 
                        index:'G2199_C43039', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43040', 
                        index:'G2199_C43040', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43041', 
                        index:'G2199_C43041', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43042', 
                        index:'G2199_C43042', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    {  
                        name:'G2199_C43003', 
                        index:'G2199_C43003', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43004', 
                        index:'G2199_C43004', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C43004'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43010', 
                        index:'G2199_C43010', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C43010'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43058', 
                        index: 'G2199_C43058', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C43053', 
                        index:'G2199_C43053', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43054', 
                        index:'G2199_C43054', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43011', 
                        index:'G2199_C43011', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43043', 
                        index:'G2199_C43043', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43044', 
                        index:'G2199_C43044', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43045', 
                        index:'G2199_C43045', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43046', 
                        index:'G2199_C43046', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43048', 
                        index:'G2199_C43048', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43049', 
                        index:'G2199_C43049', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43047', 
                        index:'G2199_C43047', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43050', 
                        index:'G2199_C43050', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43051', 
                        index:'G2199_C43051', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G2199_C43015', 
                        index:'G2199_C43015', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C43015'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43016', 
                        index: 'G2199_C43016', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2199_C43017', 
                        index:'G2199_C43017', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2199_C43018', 
                        index:'G2199_C43018', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43019', 
                        index:'G2199_C43019', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C43019'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43020', 
                        index:'G2199_C43020', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2383&campo=G2199_C43020'
                        }
                    }

                    ,
                    { 
                        name:'G2199_C43021', 
                        index:'G2199_C43021', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2199_C43022', 
                        index:'G2199_C43022', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2399&campo=G2199_C43022'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2199_C42989',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        
 
                    $("#G2199_C43057").val(item.G2199_C43057).trigger("change"); 
 
                    $("#G2199_C42997").val(item.G2199_C42997).trigger("change"); 
 
                    $("#G2199_C42998").val(item.G2199_C42998).trigger("change"); 

                        $("#G2199_C42989").val(item.G2199_C42989);

                        $("#G2199_C42990").val(item.G2199_C42990);

                        $("#G2199_C42991").val(item.G2199_C42991);

                        $("#G2199_C42992").val(item.G2199_C42992);
 
                    $("#G2199_C42993").val(item.G2199_C42993).trigger("change"); 

                        $("#G2199_C42994").val(item.G2199_C42994);
 
                    $("#G2199_C42995").val(item.G2199_C42995).trigger("change"); 

                        $("#G2199_C42996").val(item.G2199_C42996);

                        $("#G2199_C42999").val(item.G2199_C42999);
 
                    $("#G2199_C43000").val(item.G2199_C43000).trigger("change"); 

                        $("#G2199_C43052").val(item.G2199_C43052);

                        $("#G2199_C43604").val(item.G2199_C43604);
 
                    $("#G2199_C42978").val(item.G2199_C42978).trigger("change"); 
 
                    $("#G2199_C42979").val(item.G2199_C42979).trigger("change"); 

                        $("#G2199_C42980").val(item.G2199_C42980);

                        $("#G2199_C42981").val(item.G2199_C42981);

                        $("#G2199_C42982").val(item.G2199_C42982);

                        $("#G2199_C42983").val(item.G2199_C42983);

                        $("#G2199_C42984").val(item.G2199_C42984);

                        $("#G2199_C42985").val(item.G2199_C42985);

                        $("#G2199_C42986").val(item.G2199_C42986);
 
                    $("#G2199_C42987").val(item.G2199_C42987).trigger("change"); 
 
                    $("#G2199_C42988").val(item.G2199_C42988).trigger("change"); 

                        $("#G2199_C43055").val(item.G2199_C43055);
 
                    $("#G2199_C43001").val(item.G2199_C43001).trigger("change"); 
    
                        if(item.G2199_C43002 == 1){
                           $("#G2199_C43002").attr('checked', true);
                        } 
    
                        if(item.G2199_C43026 == 1){
                           $("#G2199_C43026").attr('checked', true);
                        } 
    
                        if(item.G2199_C43027 == 1){
                           $("#G2199_C43027").attr('checked', true);
                        } 
    
                        if(item.G2199_C43028 == 1){
                           $("#G2199_C43028").attr('checked', true);
                        } 
    
                        if(item.G2199_C43029 == 1){
                           $("#G2199_C43029").attr('checked', true);
                        } 
    
                        if(item.G2199_C43030 == 1){
                           $("#G2199_C43030").attr('checked', true);
                        } 
    
                        if(item.G2199_C43031 == 1){
                           $("#G2199_C43031").attr('checked', true);
                        } 
    
                        if(item.G2199_C43032 == 1){
                           $("#G2199_C43032").attr('checked', true);
                        } 
    
                        if(item.G2199_C43033 == 1){
                           $("#G2199_C43033").attr('checked', true);
                        } 
    
                        if(item.G2199_C43034 == 1){
                           $("#G2199_C43034").attr('checked', true);
                        } 
    
                        if(item.G2199_C43035 == 1){
                           $("#G2199_C43035").attr('checked', true);
                        } 
    
                        if(item.G2199_C43036 == 1){
                           $("#G2199_C43036").attr('checked', true);
                        } 
    
                        if(item.G2199_C43037 == 1){
                           $("#G2199_C43037").attr('checked', true);
                        } 
    
                        if(item.G2199_C43038 == 1){
                           $("#G2199_C43038").attr('checked', true);
                        } 
    
                        if(item.G2199_C43039 == 1){
                           $("#G2199_C43039").attr('checked', true);
                        } 
    
                        if(item.G2199_C43040 == 1){
                           $("#G2199_C43040").attr('checked', true);
                        } 
    
                        if(item.G2199_C43041 == 1){
                           $("#G2199_C43041").attr('checked', true);
                        } 
    
                        if(item.G2199_C43042 == 1){
                           $("#G2199_C43042").attr('checked', true);
                        } 

                        $("#G2199_C43003").val(item.G2199_C43003);
 
                    $("#G2199_C43004").val(item.G2199_C43004).trigger("change"); 
 
                    $("#G2199_C43010").val(item.G2199_C43010).trigger("change"); 

                        $("#G2199_C43058").val(item.G2199_C43058);
    
                        if(item.G2199_C43053 == 1){
                           $("#G2199_C43053").attr('checked', true);
                        } 
    
                        if(item.G2199_C43054 == 1){
                           $("#G2199_C43054").attr('checked', true);
                        } 
    
                        if(item.G2199_C43011 == 1){
                           $("#G2199_C43011").attr('checked', true);
                        } 
    
                        if(item.G2199_C43043 == 1){
                           $("#G2199_C43043").attr('checked', true);
                        } 
    
                        if(item.G2199_C43044 == 1){
                           $("#G2199_C43044").attr('checked', true);
                        } 
    
                        if(item.G2199_C43045 == 1){
                           $("#G2199_C43045").attr('checked', true);
                        } 
    
                        if(item.G2199_C43046 == 1){
                           $("#G2199_C43046").attr('checked', true);
                        } 
    
                        if(item.G2199_C43048 == 1){
                           $("#G2199_C43048").attr('checked', true);
                        } 
    
                        if(item.G2199_C43049 == 1){
                           $("#G2199_C43049").attr('checked', true);
                        } 
    
                        if(item.G2199_C43047 == 1){
                           $("#G2199_C43047").attr('checked', true);
                        } 
    
                        if(item.G2199_C43050 == 1){
                           $("#G2199_C43050").attr('checked', true);
                        } 
    
                        if(item.G2199_C43051 == 1){
                           $("#G2199_C43051").attr('checked', true);
                        } 
 
                    $("#G2199_C43015").val(item.G2199_C43015).trigger("change"); 

                        $("#G2199_C43016").val(item.G2199_C43016);

                        $("#G2199_C43017").val(item.G2199_C43017);

                        $("#G2199_C43018").val(item.G2199_C43018);
 
                    $("#G2199_C43019").val(item.G2199_C43019).trigger("change"); 
 
                    $("#G2199_C43020").val(item.G2199_C43020).trigger("change"); 

                        $("#G2199_C43021").val(item.G2199_C43021);
 
                    $("#G2199_C43022").val(item.G2199_C43022).trigger("change"); 
    
                        if(item.G2199_C43023 == 1){
                           $("#G2199_C43023").attr('checked', true);
                        } 
    
                        if(item.G2199_C43024 == 1){
                           $("#G2199_C43024").attr('checked', true);
                        } 
    
                        if(item.G2199_C43025 == 1){
                           $("#G2199_C43025").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
