<?php 
    /*
        Document   : index
        Created on : 2020-06-05 10:39:30
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjE5OQ==  
    */
    $url_crud =  "formularios/G2199/G2199_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G2199/G2199_CRUD_web.php" method="post" id="formLogin">
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Buenos días /tardes/noches, Tengo el gusto de hablar con Sr/Sra (NOMBRE DEL AFILIADO) Mucho gusto mi nombre es (NOMBRE DEL AGENTE), me estoy comunicando de la EPS CAPRESOCA.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>¿Como se encuentra el dia de hoy? (ESPERAR RESPUESTA DEL CLIENTE)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Le informamos el motivo de nuestra llamada, nos encontramos realizando una consulta nuestros afiliados, sobre la crisis sanitaria sobre el COVID-19</h3>
                            <!-- FIN LIBRETO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43057" id="LblG2199_C43057">¿De donde se realiza la llamada?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43057" id="G2199_C43057">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2398 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42997" id="LblG2199_C42997">A qué EPS está Afiliado </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42997" id="G2199_C42997">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2388 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42998" id="LblG2199_C42998">Tipo Afiliado</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42998" id="G2199_C42998">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2381 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C42989" id="LblG2199_C42989">Nombres </label>
								<input type="text" class="form-control input-sm" id="G2199_C42989" value=""  name="G2199_C42989"  placeholder="Nombres ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C42990" id="LblG2199_C42990">Apellidos</label>
								<input type="text" class="form-control input-sm" id="G2199_C42990" value=""  name="G2199_C42990"  placeholder="Apellidos">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C42991" id="LblG2199_C42991">Acudiente</label>
								<input type="text" class="form-control input-sm" id="G2199_C42991" value=""  name="G2199_C42991"  placeholder="Acudiente">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C42992" id="LblG2199_C42992">Edad</label>
								<input type="text" class="form-control input-sm" id="G2199_C42992" value=""  name="G2199_C42992"  placeholder="Edad">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42993" id="LblG2199_C42993">Sexo </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42993" id="G2199_C42993">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2377 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C42994" id="LblG2199_C42994">Número de teléfono </label>
								<input type="text" class="form-control input-sm" id="G2199_C42994" value=""  name="G2199_C42994"  placeholder="Número de teléfono ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42995" id="LblG2199_C42995">Tipo Identificación</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42995" id="G2199_C42995">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2378 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2199_C42996" id="LblG2199_C42996">Número de Identificación</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G2199_C42996" id="G2199_C42996" placeholder="Número de Identificación">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C42999" id="LblG2199_C42999">Dirección</label>
								<input type="text" class="form-control input-sm" id="G2199_C42999" value=""  name="G2199_C42999"  placeholder="Dirección">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43000" id="LblG2199_C43000">Ciudad</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43000" id="G2199_C43000">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2382 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C43052" id="LblG2199_C43052">Correo electronico</label>
								<input type="text" class="form-control input-sm" id="G2199_C43052" value=""  name="G2199_C43052"  placeholder="Correo electronico">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2199_C43604" id="LblG2199_C43604">Numero celular actualizado del afiliado</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G2199_C43604" id="G2199_C43604" placeholder="Numero celular actualizado del afiliado">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Preguntar al paciente sobre los siguientes síntomas, es caso de estudio al presentar más de dos a la vez en los últimos 14 dias.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43026" id="LblG2199_C43026">Fiebre mayor o igual a 38 °C</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43026" id="G2199_C43026" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43027" id="LblG2199_C43027">Tos</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43027" id="G2199_C43027" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43028" id="LblG2199_C43028">Secreción Nasal</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43028" id="G2199_C43028" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43029" id="LblG2199_C43029">Malestar General</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43029" id="G2199_C43029" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43030" id="LblG2199_C43030">Dificultad respiratoria</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43030" id="G2199_C43030" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43031" id="LblG2199_C43031">Dolor de Garganta</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43031" id="G2199_C43031" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43032" id="LblG2199_C43032">Fatiga</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43032" id="G2199_C43032" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43033" id="LblG2199_C43033">Sensación de falta de aire</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43033" id="G2199_C43033" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43034" id="LblG2199_C43034">Odinofágia</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43034" id="G2199_C43034" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43035" id="LblG2199_C43035">Adinamia</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43035" id="G2199_C43035" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43036" id="LblG2199_C43036">Respiración más rápida de lo normal</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43036" id="G2199_C43036" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43037" id="LblG2199_C43037">El pecho le suena o le duele al respirar</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43037" id="G2199_C43037" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43038" id="LblG2199_C43038">Somnolencia o dificultad para despertar</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43038" id="G2199_C43038" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43039" id="LblG2199_C43039">Ataques o convulsiones</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43039" id="G2199_C43039" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43040" id="LblG2199_C43040">Decaimiento</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43040" id="G2199_C43040" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43041" id="LblG2199_C43041">Deterioro del estado general en forma rápida</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43041" id="G2199_C43041" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43042" id="LblG2199_C43042">Ninguna de las anteriores</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43042" id="G2199_C43042" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2199_C43003" id="LblG2199_C43003">Fecha en la que comenzaron los síntomas en caso de haberlos</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2199_C43003" id="G2199_C43003" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43004" id="LblG2199_C43004">Clasifica este paciente como Caso de Estudio</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43004" id="G2199_C43004">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42987" id="LblG2199_C42987">Antecedentes de Viaje: Ud realizó algún viaje desde o hacia las zonas donde se presenta el virus o ha tenido cercanía con personas contagiadas en los últimos 14 días?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42987" id="G2199_C42987">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C42988" id="LblG2199_C42988">¿Es Trabajador de la salud u otro personal del ámbito hospitalario que haya tenido contacto estrecho* con caso confirmado para enfermedad por nuevo coronavirus (COVID-19).?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C42988" id="G2199_C42988">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2199_C43055" id="LblG2199_C43055">Trabajador de la Salud. ¿Donde trabaja?</label>
                                <textarea class="form-control input-sm" name="G2199_C43055" id="G2199_C43055"  value="" placeholder="Trabajador de la Salud. ¿Donde trabaja?"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43001" id="LblG2199_C43001">Ha tenido contacto estrecho* en los últimos 14 días con un caso confirmado con infección respiratoria aguda grave asociada al nuevo coronavirus 2019 (COVID-19)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43001" id="G2199_C43001">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43010" id="LblG2199_C43010">Nexos epidemiologicos.</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43010" id="G2199_C43010">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C43058" id="LblG2199_C43058">¿Quién o quienes?</label>
								<input type="text" class="form-control input-sm" id="G2199_C43058" value=""  name="G2199_C43058"  placeholder="¿Quién o quienes?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43053" id="LblG2199_C43053">Consume o ha consumido Ibuprofeno?</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43053" id="G2199_C43053" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43054" id="LblG2199_C43054">Ninguno de las anteriores</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43054" id="G2199_C43054" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43011" id="LblG2199_C43011">Consume medicamentos antinflamatorios o acetaminofén.</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43011" id="G2199_C43011" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43043" id="LblG2199_C43043">Enfermedad Cardiovascular</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43043" id="G2199_C43043" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43044" id="LblG2199_C43044">Enfermedad Respiratoria Crónica</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43044" id="G2199_C43044" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43045" id="LblG2199_C43045">Hipertensión</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43045" id="G2199_C43045" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43046" id="LblG2199_C43046">Cáncer</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43046" id="G2199_C43046" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43048" id="LblG2199_C43048">Diabetes</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43048" id="G2199_C43048" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43049" id="LblG2199_C43049">VIH</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43049" id="G2199_C43049" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43047" id="LblG2199_C43047">Uso de corticoides o inmunosupresores</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43047" id="G2199_C43047" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43050" id="LblG2199_C43050">EPOC</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43050" id="G2199_C43050" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->
  
                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                            <div class="form-group">
                                <label for="G2199_C43051" id="LblG2199_C43051">Fumador</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="G2199_C43051" id="G2199_C43051" data-error="Before you wreck yourself"  > 
                                    </label>
                                </div>
                            </div>
                            <!-- FIN DEL CAMPO SI/NO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43015" id="LblG2199_C43015">Está actualmente hospitalizado por IRA IRAG IRAGi?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43015" id="G2199_C43015">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2199_C43016" id="LblG2199_C43016">En que lugar se encuentra hospitalizado?</label>
								<input type="text" class="form-control input-sm" id="G2199_C43016" value=""  name="G2199_C43016"  placeholder="En que lugar se encuentra hospitalizado?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2199_C43017" id="LblG2199_C43017">Fecha de ingreso hospitalización</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2199_C43017" id="G2199_C43017" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2199_C43018" id="LblG2199_C43018">Fecha en la cual se tomó la muestra</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2199_C43018" id="G2199_C43018" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43019" id="LblG2199_C43019">¿Ya le han tomado muestra para COVID-19?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43019" id="G2199_C43019">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43020" id="LblG2199_C43020">¿Muerte probable por COVID-19?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43020" id="G2199_C43020">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2383 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2199_C43021" id="LblG2199_C43021">Observaciones - Describa la Ruta tratamiento a seguir del paciente *</label>
                                <textarea class="form-control input-sm" name="G2199_C43021" id="G2199_C43021"  value="" placeholder="Observaciones - Describa la Ruta tratamiento a seguir del paciente *"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2199_C43022" id="LblG2199_C43022">Clasificación del Paciente de acuerdo con LINEAMIENTOS PARA LA DETECCIÓN Y MANEJO DE CASOS DE COVID-19 POR LOS PRESTADORES DE SERVICIOS DE SALUD EN COLOMBIA</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2199_C43022" id="G2199_C43022">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2399 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2199/G2199_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2199_C42980").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2199_C43003").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2199_C43017").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2199_C43018").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G2199_C42981").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                

            $("#G2199_C42996").numeric();
            
            $("#G2199_C43604").numeric();
            

                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para ¿De donde se realiza la llamada? 

    $("#G2199_C43057").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para A qué EPS está Afiliado  

    $("#G2199_C42997").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo Afiliado 

    $("#G2199_C42998").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Sexo  

    $("#G2199_C42993").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo Identificación 

    $("#G2199_C42995").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Ciudad 

    $("#G2199_C43000").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Clasifica este paciente como Caso de Estudio 

    $("#G2199_C43004").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Antecedentes de Viaje: Ud realizó algún viaje desde o hacia las zonas donde se presenta el virus o ha tenido cercanía con personas contagiadas en los últimos 14 días? 

    $("#G2199_C42987").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Es Trabajador de la salud u otro personal del ámbito hospitalario que haya tenido contacto estrecho* con caso confirmado para enfermedad por nuevo coronavirus (COVID-19).? 

    $("#G2199_C42988").change(function(){ 
            $("#G2199_C42978").prop('disabled', false);
        
            $("#G2199_C42979").prop('disabled', false);
        
            $("#G2199_C42980").prop('disabled', false);
        
            $("#G2199_C42981").prop('disabled', false);
        
            $("#G2199_C42982").prop('disabled', false);
        
            if($(this).val() == '31128'){
            $("#G2199_C43055").prop('disabled', true); 
          
            }
         
        //Esto es la parte de las listas dependientes
        

    });

    //function para Ha tenido contacto estrecho* en los últimos 14 días con un caso confirmado con infección respiratoria aguda grave asociada al nuevo coronavirus 2019 (COVID-19) 

    $("#G2199_C43001").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Nexos epidemiologicos. 

    $("#G2199_C43010").change(function(){ 
            $("#G2199_C42978").prop('disabled', false);
        
            $("#G2199_C42979").prop('disabled', false);
        
            $("#G2199_C42980").prop('disabled', false);
        
            $("#G2199_C42981").prop('disabled', false);
        
            $("#G2199_C42982").prop('disabled', false);
        
            if($(this).val() == '31128'){
            $("#G2199_C43058").prop('disabled', true); 
          
            }
         
        //Esto es la parte de las listas dependientes
        

    });

    //function para Está actualmente hospitalizado por IRA IRAG IRAGi? 

    $("#G2199_C43015").change(function(){ 
            $("#G2199_C42978").prop('disabled', false);
        
            $("#G2199_C42979").prop('disabled', false);
        
            $("#G2199_C42980").prop('disabled', false);
        
            $("#G2199_C42981").prop('disabled', false);
        
            $("#G2199_C42982").prop('disabled', false);
        
            if($(this).val() == '31128'){
            $("#G2199_C43016").prop('disabled', true); 
          
            $("#G2199_C43017").prop('disabled', true); 
          
            $("#G2199_C43018").prop('disabled', true); 
          
            }
         
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Ya le han tomado muestra para COVID-19? 

    $("#G2199_C43019").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Muerte probable por COVID-19? 

    $("#G2199_C43020").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Clasificación del Paciente de acuerdo con LINEAMIENTOS PARA LA DETECCIÓN Y MANEJO DE CASOS DE COVID-19 POR LOS PRESTADORES DE SERVICIOS DE SALUD EN COLOMBIA 

    $("#G2199_C43022").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
