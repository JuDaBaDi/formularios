<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2199_ConsInte__b, G2199_FechaInsercion , G2199_Usuario ,  G2199_CodigoMiembro  , G2199_PoblacionOrigen , G2199_EstadoDiligenciamiento ,  G2199_IdLlamada , G2199_C42989 as principal ,G2199_C43057,G2199_C42997,G2199_C42998,G2199_C42989,G2199_C42990,G2199_C42991,G2199_C42992,G2199_C42993,G2199_C42994,G2199_C42995,G2199_C42996,G2199_C42999,G2199_C43000,G2199_C43052,G2199_C43604,G2199_C42978,G2199_C42979,G2199_C42980,G2199_C42981,G2199_C42982,G2199_C42983,G2199_C42984,G2199_C42985,G2199_C42986,G2199_C42987,G2199_C42988,G2199_C43055,G2199_C43001,G2199_C43026,G2199_C43027,G2199_C43028,G2199_C43029,G2199_C43030,G2199_C43031,G2199_C43032,G2199_C43033,G2199_C43034,G2199_C43035,G2199_C43036,G2199_C43037,G2199_C43038,G2199_C43039,G2199_C43040,G2199_C43041,G2199_C43042,G2199_C43003,G2199_C43004,G2199_C43010,G2199_C43058,G2199_C43053,G2199_C43054,G2199_C43011,G2199_C43043,G2199_C43044,G2199_C43045,G2199_C43046,G2199_C43048,G2199_C43049,G2199_C43047,G2199_C43050,G2199_C43051,G2199_C43015,G2199_C43016,G2199_C43017,G2199_C43018,G2199_C43019,G2199_C43020,G2199_C43021,G2199_C43022 FROM '.$BaseDatos.'.G2199 WHERE G2199_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2199_C43057'] = $key->G2199_C43057;

                $datos[$i]['G2199_C42997'] = $key->G2199_C42997;

                $datos[$i]['G2199_C42998'] = $key->G2199_C42998;

                $datos[$i]['G2199_C42989'] = $key->G2199_C42989;

                $datos[$i]['G2199_C42990'] = $key->G2199_C42990;

                $datos[$i]['G2199_C42991'] = $key->G2199_C42991;

                $datos[$i]['G2199_C42992'] = $key->G2199_C42992;

                $datos[$i]['G2199_C42993'] = $key->G2199_C42993;

                $datos[$i]['G2199_C42994'] = $key->G2199_C42994;

                $datos[$i]['G2199_C42995'] = $key->G2199_C42995;

                $datos[$i]['G2199_C42996'] = $key->G2199_C42996;

                $datos[$i]['G2199_C42999'] = $key->G2199_C42999;

                $datos[$i]['G2199_C43000'] = $key->G2199_C43000;

                $datos[$i]['G2199_C43052'] = $key->G2199_C43052;

                $datos[$i]['G2199_C43604'] = $key->G2199_C43604;

                $datos[$i]['G2199_C42978'] = $key->G2199_C42978;

                $datos[$i]['G2199_C42979'] = $key->G2199_C42979;

                $datos[$i]['G2199_C42980'] = explode(' ', $key->G2199_C42980)[0];
  
                $hora = '';
                if(!is_null($key->G2199_C42981)){
                    $hora = explode(' ', $key->G2199_C42981)[1];
                }

                $datos[$i]['G2199_C42981'] = $hora;

                $datos[$i]['G2199_C42982'] = $key->G2199_C42982;

                $datos[$i]['G2199_C42983'] = $key->G2199_C42983;

                $datos[$i]['G2199_C42984'] = $key->G2199_C42984;

                $datos[$i]['G2199_C42985'] = $key->G2199_C42985;

                $datos[$i]['G2199_C42986'] = $key->G2199_C42986;

                $datos[$i]['G2199_C42987'] = $key->G2199_C42987;

                $datos[$i]['G2199_C42988'] = $key->G2199_C42988;

                $datos[$i]['G2199_C43055'] = $key->G2199_C43055;

                $datos[$i]['G2199_C43001'] = $key->G2199_C43001;

                $datos[$i]['G2199_C43026'] = $key->G2199_C43026;

                $datos[$i]['G2199_C43027'] = $key->G2199_C43027;

                $datos[$i]['G2199_C43028'] = $key->G2199_C43028;

                $datos[$i]['G2199_C43029'] = $key->G2199_C43029;

                $datos[$i]['G2199_C43030'] = $key->G2199_C43030;

                $datos[$i]['G2199_C43031'] = $key->G2199_C43031;

                $datos[$i]['G2199_C43032'] = $key->G2199_C43032;

                $datos[$i]['G2199_C43033'] = $key->G2199_C43033;

                $datos[$i]['G2199_C43034'] = $key->G2199_C43034;

                $datos[$i]['G2199_C43035'] = $key->G2199_C43035;

                $datos[$i]['G2199_C43036'] = $key->G2199_C43036;

                $datos[$i]['G2199_C43037'] = $key->G2199_C43037;

                $datos[$i]['G2199_C43038'] = $key->G2199_C43038;

                $datos[$i]['G2199_C43039'] = $key->G2199_C43039;

                $datos[$i]['G2199_C43040'] = $key->G2199_C43040;

                $datos[$i]['G2199_C43041'] = $key->G2199_C43041;

                $datos[$i]['G2199_C43042'] = $key->G2199_C43042;

                $datos[$i]['G2199_C43003'] = explode(' ', $key->G2199_C43003)[0];

                $datos[$i]['G2199_C43004'] = $key->G2199_C43004;

                $datos[$i]['G2199_C43010'] = $key->G2199_C43010;

                $datos[$i]['G2199_C43058'] = $key->G2199_C43058;

                $datos[$i]['G2199_C43053'] = $key->G2199_C43053;

                $datos[$i]['G2199_C43054'] = $key->G2199_C43054;

                $datos[$i]['G2199_C43011'] = $key->G2199_C43011;

                $datos[$i]['G2199_C43043'] = $key->G2199_C43043;

                $datos[$i]['G2199_C43044'] = $key->G2199_C43044;

                $datos[$i]['G2199_C43045'] = $key->G2199_C43045;

                $datos[$i]['G2199_C43046'] = $key->G2199_C43046;

                $datos[$i]['G2199_C43048'] = $key->G2199_C43048;

                $datos[$i]['G2199_C43049'] = $key->G2199_C43049;

                $datos[$i]['G2199_C43047'] = $key->G2199_C43047;

                $datos[$i]['G2199_C43050'] = $key->G2199_C43050;

                $datos[$i]['G2199_C43051'] = $key->G2199_C43051;

                $datos[$i]['G2199_C43015'] = $key->G2199_C43015;

                $datos[$i]['G2199_C43016'] = $key->G2199_C43016;

                $datos[$i]['G2199_C43017'] = explode(' ', $key->G2199_C43017)[0];

                $datos[$i]['G2199_C43018'] = explode(' ', $key->G2199_C43018)[0];

                $datos[$i]['G2199_C43019'] = $key->G2199_C43019;

                $datos[$i]['G2199_C43020'] = $key->G2199_C43020;

                $datos[$i]['G2199_C43021'] = $key->G2199_C43021;

                $datos[$i]['G2199_C43022'] = $key->G2199_C43022;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2199";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2199_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2199_ConsInte__b as id,  G2199_C42989 as camp1 , G2199_C42990 as camp2 
                     FROM ".$BaseDatos.".G2199  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2199_ConsInte__b as id,  G2199_C42989 as camp1 , G2199_C42990 as camp2  
                    FROM ".$BaseDatos.".G2199  JOIN ".$BaseDatos.".G2199_M".$_POST['muestra']." ON G2199_ConsInte__b = G2199_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2199_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2199_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2199_C42989 LIKE '%".$B."%' OR G2199_C42990 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2199_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2199");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2199_ConsInte__b, G2199_FechaInsercion , G2199_Usuario ,  G2199_CodigoMiembro  , G2199_PoblacionOrigen , G2199_EstadoDiligenciamiento ,  G2199_IdLlamada , G2199_C42989 as principal , a.LISOPC_Nombre____b as G2199_C43057, b.LISOPC_Nombre____b as G2199_C42997, c.LISOPC_Nombre____b as G2199_C42998,G2199_C42989,G2199_C42990,G2199_C42991,G2199_C42992, d.LISOPC_Nombre____b as G2199_C42993,G2199_C42994, e.LISOPC_Nombre____b as G2199_C42995,G2199_C42996,G2199_C42999, f.LISOPC_Nombre____b as G2199_C43000,G2199_C43052,G2199_C43604, g.LISOPC_Nombre____b as G2199_C42978, h.LISOPC_Nombre____b as G2199_C42979,G2199_C42980,G2199_C42981,G2199_C42982,G2199_C42983,G2199_C42984,G2199_C42985,G2199_C42986, i.LISOPC_Nombre____b as G2199_C42987, j.LISOPC_Nombre____b as G2199_C42988,G2199_C43055, k.LISOPC_Nombre____b as G2199_C43001,G2199_C43026,G2199_C43027,G2199_C43028,G2199_C43029,G2199_C43030,G2199_C43031,G2199_C43032,G2199_C43033,G2199_C43034,G2199_C43035,G2199_C43036,G2199_C43037,G2199_C43038,G2199_C43039,G2199_C43040,G2199_C43041,G2199_C43042,G2199_C43003, l.LISOPC_Nombre____b as G2199_C43004, m.LISOPC_Nombre____b as G2199_C43010,G2199_C43058,G2199_C43053,G2199_C43054,G2199_C43011,G2199_C43043,G2199_C43044,G2199_C43045,G2199_C43046,G2199_C43048,G2199_C43049,G2199_C43047,G2199_C43050,G2199_C43051, n.LISOPC_Nombre____b as G2199_C43015,G2199_C43016,G2199_C43017,G2199_C43018, o.LISOPC_Nombre____b as G2199_C43019, p.LISOPC_Nombre____b as G2199_C43020,G2199_C43021, q.LISOPC_Nombre____b as G2199_C43022 FROM '.$BaseDatos.'.G2199 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2199_C43057 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2199_C42997 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2199_C42998 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2199_C42993 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2199_C42995 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2199_C43000 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2199_C42978 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2199_C42979 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2199_C42987 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2199_C42988 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G2199_C43001 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G2199_C43004 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G2199_C43010 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G2199_C43015 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G2199_C43019 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G2199_C43020 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as q ON q.LISOPC_ConsInte__b =  G2199_C43022';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2199_C42981)){
                    $hora_a = explode(' ', $fila->G2199_C42981)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2199_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2199_ConsInte__b , ($fila->G2199_C43057) , ($fila->G2199_C42997) , ($fila->G2199_C42998) , ($fila->G2199_C42989) , ($fila->G2199_C42990) , ($fila->G2199_C42991) , ($fila->G2199_C42992) , ($fila->G2199_C42993) , ($fila->G2199_C42994) , ($fila->G2199_C42995) , ($fila->G2199_C42996) , ($fila->G2199_C42999) , ($fila->G2199_C43000) , ($fila->G2199_C43052) , ($fila->G2199_C43604) , ($fila->G2199_C42978) , ($fila->G2199_C42979) , explode(' ', $fila->G2199_C42980)[0] , $hora_a , ($fila->G2199_C42982) , ($fila->G2199_C42983) , ($fila->G2199_C42984) , ($fila->G2199_C42985) , ($fila->G2199_C42986) , ($fila->G2199_C42987) , ($fila->G2199_C42988) , ($fila->G2199_C43055) , ($fila->G2199_C43001) , ($fila->G2199_C43026) , ($fila->G2199_C43027) , ($fila->G2199_C43028) , ($fila->G2199_C43029) , ($fila->G2199_C43030) , ($fila->G2199_C43031) , ($fila->G2199_C43032) , ($fila->G2199_C43033) , ($fila->G2199_C43034) , ($fila->G2199_C43035) , ($fila->G2199_C43036) , ($fila->G2199_C43037) , ($fila->G2199_C43038) , ($fila->G2199_C43039) , ($fila->G2199_C43040) , ($fila->G2199_C43041) , ($fila->G2199_C43042) , explode(' ', $fila->G2199_C43003)[0] , ($fila->G2199_C43004) , ($fila->G2199_C43010) , ($fila->G2199_C43058) , ($fila->G2199_C43053) , ($fila->G2199_C43054) , ($fila->G2199_C43011) , ($fila->G2199_C43043) , ($fila->G2199_C43044) , ($fila->G2199_C43045) , ($fila->G2199_C43046) , ($fila->G2199_C43048) , ($fila->G2199_C43049) , ($fila->G2199_C43047) , ($fila->G2199_C43050) , ($fila->G2199_C43051) , ($fila->G2199_C43015) , ($fila->G2199_C43016) , explode(' ', $fila->G2199_C43017)[0] , explode(' ', $fila->G2199_C43018)[0] , ($fila->G2199_C43019) , ($fila->G2199_C43020) , ($fila->G2199_C43021) , ($fila->G2199_C43022) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2199 WHERE G2199_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2199";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2199_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2199_ConsInte__b as id,  G2199_C42989 as camp1 , G2199_C42990 as camp2  FROM '.$BaseDatos.'.G2199 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2199_ConsInte__b as id,  G2199_C42989 as camp1 , G2199_C42990 as camp2  
                    FROM ".$BaseDatos.".G2199  JOIN ".$BaseDatos.".G2199_M".$_POST['muestra']." ON G2199_ConsInte__b = G2199_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2199_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2199_C42989 LIKE "%'.$B.'%" OR G2199_C42990 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2199_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2199 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2199(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2199_C43057"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43057 = '".$_POST["G2199_C43057"]."'";
                $LsqlI .= $separador."G2199_C43057";
                $LsqlV .= $separador."'".$_POST["G2199_C43057"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42997"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42997 = '".$_POST["G2199_C42997"]."'";
                $LsqlI .= $separador."G2199_C42997";
                $LsqlV .= $separador."'".$_POST["G2199_C42997"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42998"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42998 = '".$_POST["G2199_C42998"]."'";
                $LsqlI .= $separador."G2199_C42998";
                $LsqlV .= $separador."'".$_POST["G2199_C42998"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42989"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42989 = '".$_POST["G2199_C42989"]."'";
                $LsqlI .= $separador."G2199_C42989";
                $LsqlV .= $separador."'".$_POST["G2199_C42989"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42990"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42990 = '".$_POST["G2199_C42990"]."'";
                $LsqlI .= $separador."G2199_C42990";
                $LsqlV .= $separador."'".$_POST["G2199_C42990"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42991"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42991 = '".$_POST["G2199_C42991"]."'";
                $LsqlI .= $separador."G2199_C42991";
                $LsqlV .= $separador."'".$_POST["G2199_C42991"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42992"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42992 = '".$_POST["G2199_C42992"]."'";
                $LsqlI .= $separador."G2199_C42992";
                $LsqlV .= $separador."'".$_POST["G2199_C42992"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42993"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42993 = '".$_POST["G2199_C42993"]."'";
                $LsqlI .= $separador."G2199_C42993";
                $LsqlV .= $separador."'".$_POST["G2199_C42993"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42994"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42994 = '".$_POST["G2199_C42994"]."'";
                $LsqlI .= $separador."G2199_C42994";
                $LsqlV .= $separador."'".$_POST["G2199_C42994"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42995"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42995 = '".$_POST["G2199_C42995"]."'";
                $LsqlI .= $separador."G2199_C42995";
                $LsqlV .= $separador."'".$_POST["G2199_C42995"]."'";
                $validar = 1;
            }
             
  
            $G2199_C42996 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2199_C42996"])){
                if($_POST["G2199_C42996"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2199_C42996 = $_POST["G2199_C42996"];
                    $LsqlU .= $separador." G2199_C42996 = ".$G2199_C42996."";
                    $LsqlI .= $separador." G2199_C42996";
                    $LsqlV .= $separador.$G2199_C42996;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2199_C42999"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42999 = '".$_POST["G2199_C42999"]."'";
                $LsqlI .= $separador."G2199_C42999";
                $LsqlV .= $separador."'".$_POST["G2199_C42999"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43000"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43000 = '".$_POST["G2199_C43000"]."'";
                $LsqlI .= $separador."G2199_C43000";
                $LsqlV .= $separador."'".$_POST["G2199_C43000"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43052"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43052 = '".$_POST["G2199_C43052"]."'";
                $LsqlI .= $separador."G2199_C43052";
                $LsqlV .= $separador."'".$_POST["G2199_C43052"]."'";
                $validar = 1;
            }
             
  
            $G2199_C43604 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2199_C43604"])){
                if($_POST["G2199_C43604"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2199_C43604 = $_POST["G2199_C43604"];
                    $LsqlU .= $separador." G2199_C43604 = ".$G2199_C43604."";
                    $LsqlI .= $separador." G2199_C43604";
                    $LsqlV .= $separador.$G2199_C43604;
                    $validar = 1;
                }
            }
 
            $G2199_C42978 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2199_C42978 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2199_C42978 = ".$G2199_C42978;
                    $LsqlI .= $separador." G2199_C42978";
                    $LsqlV .= $separador.$G2199_C42978;
                    $validar = 1;

                    
                }
            }
 
            $G2199_C42979 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2199_C42979 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2199_C42979 = ".$G2199_C42979;
                    $LsqlI .= $separador." G2199_C42979";
                    $LsqlV .= $separador.$G2199_C42979;
                    $validar = 1;
                }
            }
 
            $G2199_C42980 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2199_C42980 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2199_C42980 = ".$G2199_C42980;
                    $LsqlI .= $separador." G2199_C42980";
                    $LsqlV .= $separador.$G2199_C42980;
                    $validar = 1;
                }
            }
 
            $G2199_C42981 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2199_C42981 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2199_C42981 = ".$G2199_C42981;
                    $LsqlI .= $separador." G2199_C42981";
                    $LsqlV .= $separador.$G2199_C42981;
                    $validar = 1;
                }
            }
 
            $G2199_C42982 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2199_C42982 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2199_C42982 = ".$G2199_C42982;
                    $LsqlI .= $separador." G2199_C42982";
                    $LsqlV .= $separador.$G2199_C42982;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2199_C42983"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42983 = '".$_POST["G2199_C42983"]."'";
                $LsqlI .= $separador."G2199_C42983";
                $LsqlV .= $separador."'".$_POST["G2199_C42983"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42984"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42984 = '".$_POST["G2199_C42984"]."'";
                $LsqlI .= $separador."G2199_C42984";
                $LsqlV .= $separador."'".$_POST["G2199_C42984"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42985"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42985 = '".$_POST["G2199_C42985"]."'";
                $LsqlI .= $separador."G2199_C42985";
                $LsqlV .= $separador."'".$_POST["G2199_C42985"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42986"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42986 = '".$_POST["G2199_C42986"]."'";
                $LsqlI .= $separador."G2199_C42986";
                $LsqlV .= $separador."'".$_POST["G2199_C42986"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42987"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42987 = '".$_POST["G2199_C42987"]."'";
                $LsqlI .= $separador."G2199_C42987";
                $LsqlV .= $separador."'".$_POST["G2199_C42987"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C42988"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C42988 = '".$_POST["G2199_C42988"]."'";
                $LsqlI .= $separador."G2199_C42988";
                $LsqlV .= $separador."'".$_POST["G2199_C42988"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43055"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43055 = '".$_POST["G2199_C43055"]."'";
                $LsqlI .= $separador."G2199_C43055";
                $LsqlV .= $separador."'".$_POST["G2199_C43055"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43001"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43001 = '".$_POST["G2199_C43001"]."'";
                $LsqlI .= $separador."G2199_C43001";
                $LsqlV .= $separador."'".$_POST["G2199_C43001"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43002"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43002 = '".$_POST["G2199_C43002"]."'";
                $LsqlI .= $separador."G2199_C43002";
                $LsqlV .= $separador."'".$_POST["G2199_C43002"]."'";
                $validar = 1;
            }
             
  
            $G2199_C43026 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43026"])){
                if($_POST["G2199_C43026"] == 'Yes'){
                    $G2199_C43026 = 1;
                }else if($_POST["G2199_C43026"] == 'off'){
                    $G2199_C43026 = 0;
                }else if($_POST["G2199_C43026"] == 'on'){
                    $G2199_C43026 = 1;
                }else if($_POST["G2199_C43026"] == 'No'){
                    $G2199_C43026 = 1;
                }else{
                    $G2199_C43026 = $_POST["G2199_C43026"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43026 = ".$G2199_C43026."";
                $LsqlI .= $separador." G2199_C43026";
                $LsqlV .= $separador.$G2199_C43026;

                $validar = 1;
            }
  
            $G2199_C43027 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43027"])){
                if($_POST["G2199_C43027"] == 'Yes'){
                    $G2199_C43027 = 1;
                }else if($_POST["G2199_C43027"] == 'off'){
                    $G2199_C43027 = 0;
                }else if($_POST["G2199_C43027"] == 'on'){
                    $G2199_C43027 = 1;
                }else if($_POST["G2199_C43027"] == 'No'){
                    $G2199_C43027 = 1;
                }else{
                    $G2199_C43027 = $_POST["G2199_C43027"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43027 = ".$G2199_C43027."";
                $LsqlI .= $separador." G2199_C43027";
                $LsqlV .= $separador.$G2199_C43027;

                $validar = 1;
            }
  
            $G2199_C43028 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43028"])){
                if($_POST["G2199_C43028"] == 'Yes'){
                    $G2199_C43028 = 1;
                }else if($_POST["G2199_C43028"] == 'off'){
                    $G2199_C43028 = 0;
                }else if($_POST["G2199_C43028"] == 'on'){
                    $G2199_C43028 = 1;
                }else if($_POST["G2199_C43028"] == 'No'){
                    $G2199_C43028 = 1;
                }else{
                    $G2199_C43028 = $_POST["G2199_C43028"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43028 = ".$G2199_C43028."";
                $LsqlI .= $separador." G2199_C43028";
                $LsqlV .= $separador.$G2199_C43028;

                $validar = 1;
            }
  
            $G2199_C43029 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43029"])){
                if($_POST["G2199_C43029"] == 'Yes'){
                    $G2199_C43029 = 1;
                }else if($_POST["G2199_C43029"] == 'off'){
                    $G2199_C43029 = 0;
                }else if($_POST["G2199_C43029"] == 'on'){
                    $G2199_C43029 = 1;
                }else if($_POST["G2199_C43029"] == 'No'){
                    $G2199_C43029 = 1;
                }else{
                    $G2199_C43029 = $_POST["G2199_C43029"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43029 = ".$G2199_C43029."";
                $LsqlI .= $separador." G2199_C43029";
                $LsqlV .= $separador.$G2199_C43029;

                $validar = 1;
            }
  
            $G2199_C43030 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43030"])){
                if($_POST["G2199_C43030"] == 'Yes'){
                    $G2199_C43030 = 1;
                }else if($_POST["G2199_C43030"] == 'off'){
                    $G2199_C43030 = 0;
                }else if($_POST["G2199_C43030"] == 'on'){
                    $G2199_C43030 = 1;
                }else if($_POST["G2199_C43030"] == 'No'){
                    $G2199_C43030 = 1;
                }else{
                    $G2199_C43030 = $_POST["G2199_C43030"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43030 = ".$G2199_C43030."";
                $LsqlI .= $separador." G2199_C43030";
                $LsqlV .= $separador.$G2199_C43030;

                $validar = 1;
            }
  
            $G2199_C43031 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43031"])){
                if($_POST["G2199_C43031"] == 'Yes'){
                    $G2199_C43031 = 1;
                }else if($_POST["G2199_C43031"] == 'off'){
                    $G2199_C43031 = 0;
                }else if($_POST["G2199_C43031"] == 'on'){
                    $G2199_C43031 = 1;
                }else if($_POST["G2199_C43031"] == 'No'){
                    $G2199_C43031 = 1;
                }else{
                    $G2199_C43031 = $_POST["G2199_C43031"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43031 = ".$G2199_C43031."";
                $LsqlI .= $separador." G2199_C43031";
                $LsqlV .= $separador.$G2199_C43031;

                $validar = 1;
            }
  
            $G2199_C43032 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43032"])){
                if($_POST["G2199_C43032"] == 'Yes'){
                    $G2199_C43032 = 1;
                }else if($_POST["G2199_C43032"] == 'off'){
                    $G2199_C43032 = 0;
                }else if($_POST["G2199_C43032"] == 'on'){
                    $G2199_C43032 = 1;
                }else if($_POST["G2199_C43032"] == 'No'){
                    $G2199_C43032 = 1;
                }else{
                    $G2199_C43032 = $_POST["G2199_C43032"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43032 = ".$G2199_C43032."";
                $LsqlI .= $separador." G2199_C43032";
                $LsqlV .= $separador.$G2199_C43032;

                $validar = 1;
            }
  
            $G2199_C43033 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43033"])){
                if($_POST["G2199_C43033"] == 'Yes'){
                    $G2199_C43033 = 1;
                }else if($_POST["G2199_C43033"] == 'off'){
                    $G2199_C43033 = 0;
                }else if($_POST["G2199_C43033"] == 'on'){
                    $G2199_C43033 = 1;
                }else if($_POST["G2199_C43033"] == 'No'){
                    $G2199_C43033 = 1;
                }else{
                    $G2199_C43033 = $_POST["G2199_C43033"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43033 = ".$G2199_C43033."";
                $LsqlI .= $separador." G2199_C43033";
                $LsqlV .= $separador.$G2199_C43033;

                $validar = 1;
            }
  
            $G2199_C43034 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43034"])){
                if($_POST["G2199_C43034"] == 'Yes'){
                    $G2199_C43034 = 1;
                }else if($_POST["G2199_C43034"] == 'off'){
                    $G2199_C43034 = 0;
                }else if($_POST["G2199_C43034"] == 'on'){
                    $G2199_C43034 = 1;
                }else if($_POST["G2199_C43034"] == 'No'){
                    $G2199_C43034 = 1;
                }else{
                    $G2199_C43034 = $_POST["G2199_C43034"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43034 = ".$G2199_C43034."";
                $LsqlI .= $separador." G2199_C43034";
                $LsqlV .= $separador.$G2199_C43034;

                $validar = 1;
            }
  
            $G2199_C43035 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43035"])){
                if($_POST["G2199_C43035"] == 'Yes'){
                    $G2199_C43035 = 1;
                }else if($_POST["G2199_C43035"] == 'off'){
                    $G2199_C43035 = 0;
                }else if($_POST["G2199_C43035"] == 'on'){
                    $G2199_C43035 = 1;
                }else if($_POST["G2199_C43035"] == 'No'){
                    $G2199_C43035 = 1;
                }else{
                    $G2199_C43035 = $_POST["G2199_C43035"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43035 = ".$G2199_C43035."";
                $LsqlI .= $separador." G2199_C43035";
                $LsqlV .= $separador.$G2199_C43035;

                $validar = 1;
            }
  
            $G2199_C43036 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43036"])){
                if($_POST["G2199_C43036"] == 'Yes'){
                    $G2199_C43036 = 1;
                }else if($_POST["G2199_C43036"] == 'off'){
                    $G2199_C43036 = 0;
                }else if($_POST["G2199_C43036"] == 'on'){
                    $G2199_C43036 = 1;
                }else if($_POST["G2199_C43036"] == 'No'){
                    $G2199_C43036 = 1;
                }else{
                    $G2199_C43036 = $_POST["G2199_C43036"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43036 = ".$G2199_C43036."";
                $LsqlI .= $separador." G2199_C43036";
                $LsqlV .= $separador.$G2199_C43036;

                $validar = 1;
            }
  
            $G2199_C43037 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43037"])){
                if($_POST["G2199_C43037"] == 'Yes'){
                    $G2199_C43037 = 1;
                }else if($_POST["G2199_C43037"] == 'off'){
                    $G2199_C43037 = 0;
                }else if($_POST["G2199_C43037"] == 'on'){
                    $G2199_C43037 = 1;
                }else if($_POST["G2199_C43037"] == 'No'){
                    $G2199_C43037 = 1;
                }else{
                    $G2199_C43037 = $_POST["G2199_C43037"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43037 = ".$G2199_C43037."";
                $LsqlI .= $separador." G2199_C43037";
                $LsqlV .= $separador.$G2199_C43037;

                $validar = 1;
            }
  
            $G2199_C43038 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43038"])){
                if($_POST["G2199_C43038"] == 'Yes'){
                    $G2199_C43038 = 1;
                }else if($_POST["G2199_C43038"] == 'off'){
                    $G2199_C43038 = 0;
                }else if($_POST["G2199_C43038"] == 'on'){
                    $G2199_C43038 = 1;
                }else if($_POST["G2199_C43038"] == 'No'){
                    $G2199_C43038 = 1;
                }else{
                    $G2199_C43038 = $_POST["G2199_C43038"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43038 = ".$G2199_C43038."";
                $LsqlI .= $separador." G2199_C43038";
                $LsqlV .= $separador.$G2199_C43038;

                $validar = 1;
            }
  
            $G2199_C43039 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43039"])){
                if($_POST["G2199_C43039"] == 'Yes'){
                    $G2199_C43039 = 1;
                }else if($_POST["G2199_C43039"] == 'off'){
                    $G2199_C43039 = 0;
                }else if($_POST["G2199_C43039"] == 'on'){
                    $G2199_C43039 = 1;
                }else if($_POST["G2199_C43039"] == 'No'){
                    $G2199_C43039 = 1;
                }else{
                    $G2199_C43039 = $_POST["G2199_C43039"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43039 = ".$G2199_C43039."";
                $LsqlI .= $separador." G2199_C43039";
                $LsqlV .= $separador.$G2199_C43039;

                $validar = 1;
            }
  
            $G2199_C43040 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43040"])){
                if($_POST["G2199_C43040"] == 'Yes'){
                    $G2199_C43040 = 1;
                }else if($_POST["G2199_C43040"] == 'off'){
                    $G2199_C43040 = 0;
                }else if($_POST["G2199_C43040"] == 'on'){
                    $G2199_C43040 = 1;
                }else if($_POST["G2199_C43040"] == 'No'){
                    $G2199_C43040 = 1;
                }else{
                    $G2199_C43040 = $_POST["G2199_C43040"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43040 = ".$G2199_C43040."";
                $LsqlI .= $separador." G2199_C43040";
                $LsqlV .= $separador.$G2199_C43040;

                $validar = 1;
            }
  
            $G2199_C43041 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43041"])){
                if($_POST["G2199_C43041"] == 'Yes'){
                    $G2199_C43041 = 1;
                }else if($_POST["G2199_C43041"] == 'off'){
                    $G2199_C43041 = 0;
                }else if($_POST["G2199_C43041"] == 'on'){
                    $G2199_C43041 = 1;
                }else if($_POST["G2199_C43041"] == 'No'){
                    $G2199_C43041 = 1;
                }else{
                    $G2199_C43041 = $_POST["G2199_C43041"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43041 = ".$G2199_C43041."";
                $LsqlI .= $separador." G2199_C43041";
                $LsqlV .= $separador.$G2199_C43041;

                $validar = 1;
            }
  
            $G2199_C43042 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43042"])){
                if($_POST["G2199_C43042"] == 'Yes'){
                    $G2199_C43042 = 1;
                }else if($_POST["G2199_C43042"] == 'off'){
                    $G2199_C43042 = 0;
                }else if($_POST["G2199_C43042"] == 'on'){
                    $G2199_C43042 = 1;
                }else if($_POST["G2199_C43042"] == 'No'){
                    $G2199_C43042 = 1;
                }else{
                    $G2199_C43042 = $_POST["G2199_C43042"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43042 = ".$G2199_C43042."";
                $LsqlI .= $separador." G2199_C43042";
                $LsqlV .= $separador.$G2199_C43042;

                $validar = 1;
            }
 
            $G2199_C43003 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2199_C43003"])){    
                if($_POST["G2199_C43003"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2199_C43003"]);
                    if(count($tieneHora) > 1){
                        $G2199_C43003 = "'".$_POST["G2199_C43003"]."'";
                    }else{
                        $G2199_C43003 = "'".str_replace(' ', '',$_POST["G2199_C43003"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2199_C43003 = ".$G2199_C43003;
                    $LsqlI .= $separador." G2199_C43003";
                    $LsqlV .= $separador.$G2199_C43003;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2199_C43004"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43004 = '".$_POST["G2199_C43004"]."'";
                $LsqlI .= $separador."G2199_C43004";
                $LsqlV .= $separador."'".$_POST["G2199_C43004"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43010"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43010 = '".$_POST["G2199_C43010"]."'";
                $LsqlI .= $separador."G2199_C43010";
                $LsqlV .= $separador."'".$_POST["G2199_C43010"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43058"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43058 = '".$_POST["G2199_C43058"]."'";
                $LsqlI .= $separador."G2199_C43058";
                $LsqlV .= $separador."'".$_POST["G2199_C43058"]."'";
                $validar = 1;
            }
             
  
            $G2199_C43053 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43053"])){
                if($_POST["G2199_C43053"] == 'Yes'){
                    $G2199_C43053 = 1;
                }else if($_POST["G2199_C43053"] == 'off'){
                    $G2199_C43053 = 0;
                }else if($_POST["G2199_C43053"] == 'on'){
                    $G2199_C43053 = 1;
                }else if($_POST["G2199_C43053"] == 'No'){
                    $G2199_C43053 = 1;
                }else{
                    $G2199_C43053 = $_POST["G2199_C43053"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43053 = ".$G2199_C43053."";
                $LsqlI .= $separador." G2199_C43053";
                $LsqlV .= $separador.$G2199_C43053;

                $validar = 1;
            }
  
            $G2199_C43054 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43054"])){
                if($_POST["G2199_C43054"] == 'Yes'){
                    $G2199_C43054 = 1;
                }else if($_POST["G2199_C43054"] == 'off'){
                    $G2199_C43054 = 0;
                }else if($_POST["G2199_C43054"] == 'on'){
                    $G2199_C43054 = 1;
                }else if($_POST["G2199_C43054"] == 'No'){
                    $G2199_C43054 = 1;
                }else{
                    $G2199_C43054 = $_POST["G2199_C43054"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43054 = ".$G2199_C43054."";
                $LsqlI .= $separador." G2199_C43054";
                $LsqlV .= $separador.$G2199_C43054;

                $validar = 1;
            }
  
            $G2199_C43011 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43011"])){
                if($_POST["G2199_C43011"] == 'Yes'){
                    $G2199_C43011 = 1;
                }else if($_POST["G2199_C43011"] == 'off'){
                    $G2199_C43011 = 0;
                }else if($_POST["G2199_C43011"] == 'on'){
                    $G2199_C43011 = 1;
                }else if($_POST["G2199_C43011"] == 'No'){
                    $G2199_C43011 = 1;
                }else{
                    $G2199_C43011 = $_POST["G2199_C43011"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43011 = ".$G2199_C43011."";
                $LsqlI .= $separador." G2199_C43011";
                $LsqlV .= $separador.$G2199_C43011;

                $validar = 1;
            }
  
            $G2199_C43043 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43043"])){
                if($_POST["G2199_C43043"] == 'Yes'){
                    $G2199_C43043 = 1;
                }else if($_POST["G2199_C43043"] == 'off'){
                    $G2199_C43043 = 0;
                }else if($_POST["G2199_C43043"] == 'on'){
                    $G2199_C43043 = 1;
                }else if($_POST["G2199_C43043"] == 'No'){
                    $G2199_C43043 = 1;
                }else{
                    $G2199_C43043 = $_POST["G2199_C43043"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43043 = ".$G2199_C43043."";
                $LsqlI .= $separador." G2199_C43043";
                $LsqlV .= $separador.$G2199_C43043;

                $validar = 1;
            }
  
            $G2199_C43044 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43044"])){
                if($_POST["G2199_C43044"] == 'Yes'){
                    $G2199_C43044 = 1;
                }else if($_POST["G2199_C43044"] == 'off'){
                    $G2199_C43044 = 0;
                }else if($_POST["G2199_C43044"] == 'on'){
                    $G2199_C43044 = 1;
                }else if($_POST["G2199_C43044"] == 'No'){
                    $G2199_C43044 = 1;
                }else{
                    $G2199_C43044 = $_POST["G2199_C43044"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43044 = ".$G2199_C43044."";
                $LsqlI .= $separador." G2199_C43044";
                $LsqlV .= $separador.$G2199_C43044;

                $validar = 1;
            }
  
            $G2199_C43045 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43045"])){
                if($_POST["G2199_C43045"] == 'Yes'){
                    $G2199_C43045 = 1;
                }else if($_POST["G2199_C43045"] == 'off'){
                    $G2199_C43045 = 0;
                }else if($_POST["G2199_C43045"] == 'on'){
                    $G2199_C43045 = 1;
                }else if($_POST["G2199_C43045"] == 'No'){
                    $G2199_C43045 = 1;
                }else{
                    $G2199_C43045 = $_POST["G2199_C43045"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43045 = ".$G2199_C43045."";
                $LsqlI .= $separador." G2199_C43045";
                $LsqlV .= $separador.$G2199_C43045;

                $validar = 1;
            }
  
            $G2199_C43046 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43046"])){
                if($_POST["G2199_C43046"] == 'Yes'){
                    $G2199_C43046 = 1;
                }else if($_POST["G2199_C43046"] == 'off'){
                    $G2199_C43046 = 0;
                }else if($_POST["G2199_C43046"] == 'on'){
                    $G2199_C43046 = 1;
                }else if($_POST["G2199_C43046"] == 'No'){
                    $G2199_C43046 = 1;
                }else{
                    $G2199_C43046 = $_POST["G2199_C43046"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43046 = ".$G2199_C43046."";
                $LsqlI .= $separador." G2199_C43046";
                $LsqlV .= $separador.$G2199_C43046;

                $validar = 1;
            }
  
            $G2199_C43048 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43048"])){
                if($_POST["G2199_C43048"] == 'Yes'){
                    $G2199_C43048 = 1;
                }else if($_POST["G2199_C43048"] == 'off'){
                    $G2199_C43048 = 0;
                }else if($_POST["G2199_C43048"] == 'on'){
                    $G2199_C43048 = 1;
                }else if($_POST["G2199_C43048"] == 'No'){
                    $G2199_C43048 = 1;
                }else{
                    $G2199_C43048 = $_POST["G2199_C43048"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43048 = ".$G2199_C43048."";
                $LsqlI .= $separador." G2199_C43048";
                $LsqlV .= $separador.$G2199_C43048;

                $validar = 1;
            }
  
            $G2199_C43049 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43049"])){
                if($_POST["G2199_C43049"] == 'Yes'){
                    $G2199_C43049 = 1;
                }else if($_POST["G2199_C43049"] == 'off'){
                    $G2199_C43049 = 0;
                }else if($_POST["G2199_C43049"] == 'on'){
                    $G2199_C43049 = 1;
                }else if($_POST["G2199_C43049"] == 'No'){
                    $G2199_C43049 = 1;
                }else{
                    $G2199_C43049 = $_POST["G2199_C43049"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43049 = ".$G2199_C43049."";
                $LsqlI .= $separador." G2199_C43049";
                $LsqlV .= $separador.$G2199_C43049;

                $validar = 1;
            }
  
            $G2199_C43047 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43047"])){
                if($_POST["G2199_C43047"] == 'Yes'){
                    $G2199_C43047 = 1;
                }else if($_POST["G2199_C43047"] == 'off'){
                    $G2199_C43047 = 0;
                }else if($_POST["G2199_C43047"] == 'on'){
                    $G2199_C43047 = 1;
                }else if($_POST["G2199_C43047"] == 'No'){
                    $G2199_C43047 = 1;
                }else{
                    $G2199_C43047 = $_POST["G2199_C43047"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43047 = ".$G2199_C43047."";
                $LsqlI .= $separador." G2199_C43047";
                $LsqlV .= $separador.$G2199_C43047;

                $validar = 1;
            }
  
            $G2199_C43050 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43050"])){
                if($_POST["G2199_C43050"] == 'Yes'){
                    $G2199_C43050 = 1;
                }else if($_POST["G2199_C43050"] == 'off'){
                    $G2199_C43050 = 0;
                }else if($_POST["G2199_C43050"] == 'on'){
                    $G2199_C43050 = 1;
                }else if($_POST["G2199_C43050"] == 'No'){
                    $G2199_C43050 = 1;
                }else{
                    $G2199_C43050 = $_POST["G2199_C43050"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43050 = ".$G2199_C43050."";
                $LsqlI .= $separador." G2199_C43050";
                $LsqlV .= $separador.$G2199_C43050;

                $validar = 1;
            }
  
            $G2199_C43051 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G2199_C43051"])){
                if($_POST["G2199_C43051"] == 'Yes'){
                    $G2199_C43051 = 1;
                }else if($_POST["G2199_C43051"] == 'off'){
                    $G2199_C43051 = 0;
                }else if($_POST["G2199_C43051"] == 'on'){
                    $G2199_C43051 = 1;
                }else if($_POST["G2199_C43051"] == 'No'){
                    $G2199_C43051 = 1;
                }else{
                    $G2199_C43051 = $_POST["G2199_C43051"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G2199_C43051 = ".$G2199_C43051."";
                $LsqlI .= $separador." G2199_C43051";
                $LsqlV .= $separador.$G2199_C43051;

                $validar = 1;
            }
  
            if(isset($_POST["G2199_C43015"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43015 = '".$_POST["G2199_C43015"]."'";
                $LsqlI .= $separador."G2199_C43015";
                $LsqlV .= $separador."'".$_POST["G2199_C43015"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43016"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43016 = '".$_POST["G2199_C43016"]."'";
                $LsqlI .= $separador."G2199_C43016";
                $LsqlV .= $separador."'".$_POST["G2199_C43016"]."'";
                $validar = 1;
            }
             
 
            $G2199_C43017 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2199_C43017"])){    
                if($_POST["G2199_C43017"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2199_C43017"]);
                    if(count($tieneHora) > 1){
                        $G2199_C43017 = "'".$_POST["G2199_C43017"]."'";
                    }else{
                        $G2199_C43017 = "'".str_replace(' ', '',$_POST["G2199_C43017"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2199_C43017 = ".$G2199_C43017;
                    $LsqlI .= $separador." G2199_C43017";
                    $LsqlV .= $separador.$G2199_C43017;
                    $validar = 1;
                }
            }
 
            $G2199_C43018 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2199_C43018"])){    
                if($_POST["G2199_C43018"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2199_C43018"]);
                    if(count($tieneHora) > 1){
                        $G2199_C43018 = "'".$_POST["G2199_C43018"]."'";
                    }else{
                        $G2199_C43018 = "'".str_replace(' ', '',$_POST["G2199_C43018"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2199_C43018 = ".$G2199_C43018;
                    $LsqlI .= $separador." G2199_C43018";
                    $LsqlV .= $separador.$G2199_C43018;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2199_C43019"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43019 = '".$_POST["G2199_C43019"]."'";
                $LsqlI .= $separador."G2199_C43019";
                $LsqlV .= $separador."'".$_POST["G2199_C43019"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43020"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43020 = '".$_POST["G2199_C43020"]."'";
                $LsqlI .= $separador."G2199_C43020";
                $LsqlV .= $separador."'".$_POST["G2199_C43020"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43021"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43021 = '".$_POST["G2199_C43021"]."'";
                $LsqlI .= $separador."G2199_C43021";
                $LsqlV .= $separador."'".$_POST["G2199_C43021"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43022"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43022 = '".$_POST["G2199_C43022"]."'";
                $LsqlI .= $separador."G2199_C43022";
                $LsqlV .= $separador."'".$_POST["G2199_C43022"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43023"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43023 = '".$_POST["G2199_C43023"]."'";
                $LsqlI .= $separador."G2199_C43023";
                $LsqlV .= $separador."'".$_POST["G2199_C43023"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43024"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43024 = '".$_POST["G2199_C43024"]."'";
                $LsqlI .= $separador."G2199_C43024";
                $LsqlV .= $separador."'".$_POST["G2199_C43024"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2199_C43025"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_C43025 = '".$_POST["G2199_C43025"]."'";
                $LsqlI .= $separador."G2199_C43025";
                $LsqlV .= $separador."'".$_POST["G2199_C43025"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2199_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2199_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2199_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2199_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2199_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2199_Usuario , G2199_FechaInsercion, G2199_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2199_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2199 WHERE G2199_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
