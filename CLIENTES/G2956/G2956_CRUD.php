<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 59123")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 59123");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2956_ConsInte__b, G2956_FechaInsercion , G2956_Usuario ,  G2956_CodigoMiembro  , G2956_PoblacionOrigen , G2956_EstadoDiligenciamiento ,  G2956_IdLlamada , G2956_C59106 as principal ,G2956_C59110,G2956_C59111,G2956_C59112,G2956_C59113,G2956_C59114,G2956_C59115,G2956_C59096,G2956_C59097,G2956_C59098,G2956_C59099,G2956_C59100,G2956_C59101,G2956_C59102,G2956_C59103,G2956_C59104,G2956_C59106,G2956_C59116,G2956_C59123,G2956_C59530,G2956_C59125 FROM '.$BaseDatos.'.G2956 WHERE G2956_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2956_C59110'] = $key->G2956_C59110;

                $datos[$i]['G2956_C59111'] = $key->G2956_C59111;

                $datos[$i]['G2956_C59112'] = $key->G2956_C59112;

                $datos[$i]['G2956_C59113'] = $key->G2956_C59113;

                $datos[$i]['G2956_C59114'] = $key->G2956_C59114;

                $datos[$i]['G2956_C59115'] = $key->G2956_C59115;

                $datos[$i]['G2956_C59096'] = $key->G2956_C59096;

                $datos[$i]['G2956_C59097'] = $key->G2956_C59097;

                $datos[$i]['G2956_C59098'] = explode(' ', $key->G2956_C59098)[0];
  
                $hora = '';
                if(!is_null($key->G2956_C59099)){
                    $hora = explode(' ', $key->G2956_C59099)[1];
                }

                $datos[$i]['G2956_C59099'] = $hora;

                $datos[$i]['G2956_C59100'] = $key->G2956_C59100;

                $datos[$i]['G2956_C59101'] = $key->G2956_C59101;

                $datos[$i]['G2956_C59102'] = $key->G2956_C59102;

                $datos[$i]['G2956_C59103'] = $key->G2956_C59103;

                $datos[$i]['G2956_C59104'] = $key->G2956_C59104;

                $datos[$i]['G2956_C59106'] = $key->G2956_C59106;

                $datos[$i]['G2956_C59116'] = $key->G2956_C59116;

                $datos[$i]['G2956_C59123'] = $key->G2956_C59123;

                $datos[$i]['G2956_C59530'] = $key->G2956_C59530;

                $datos[$i]['G2956_C59125'] = $key->G2956_C59125;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2956";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2956_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2956_ConsInte__b as id,  G2956_C59106 as camp1 , G2956_C59116 as camp2 
                     FROM ".$BaseDatos.".G2956  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2956_ConsInte__b as id,  G2956_C59106 as camp1 , G2956_C59116 as camp2  
                    FROM ".$BaseDatos.".G2956  JOIN ".$BaseDatos.".G2956_M".$_POST['muestra']." ON G2956_ConsInte__b = G2956_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2956_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2956_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2956_C59106 LIKE '%".$B."%' OR G2956_C59116 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2956_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

            if(isset($_GET['MostrarCombo_Guion_2967_C59386'])){
                echo '<select class="form-control input-sm"  name="2967_C59386" id="2967_C59386">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2967_C59386'])){
                $Ysql = "SELECT G2951_ConsInte__b as id,  G2951_C59033 as text FROM ".$BaseDatos.".G2951 WHERE G2951_C59034 LIKE '%".$_POST['q']."%' OR G2951_C59033 = '".$_POST['q']."' ";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2967_C59386'])){
                $Lsql = "SELECT  G2951_ConsInte__b as id , G2951_C59034, G2951_C59035, G2951_C59528 FROM ".$BaseDatos.".G2951 WHERE G2951_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2967_C59386'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2967_C59382'] = $key->G2951_C59034;

                    $data[$i]['G2967_C59384'] = $key->G2951_C59035;

                    $data[$i]['G2967_C59529'] = $key->G2951_C59528;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2956");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2956_ConsInte__b, G2956_FechaInsercion , G2956_Usuario ,  G2956_CodigoMiembro  , G2956_PoblacionOrigen , G2956_EstadoDiligenciamiento ,  G2956_IdLlamada , G2956_C59106 as principal ,G2956_C59110,G2956_C59111,G2956_C59112,G2956_C59113,G2956_C59114,G2956_C59115, a.LISOPC_Nombre____b as G2956_C59096, b.LISOPC_Nombre____b as G2956_C59097,G2956_C59098,G2956_C59099,G2956_C59100,G2956_C59101,G2956_C59102,G2956_C59103,G2956_C59104,G2956_C59106,G2956_C59116,G2956_C59123,G2956_C59530,G2956_C59125 FROM '.$BaseDatos.'.G2956 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2956_C59096 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2956_C59097';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2956_C59099)){
                    $hora_a = explode(' ', $fila->G2956_C59099)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2956_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2956_ConsInte__b , ($fila->G2956_C59110) , ($fila->G2956_C59111) , ($fila->G2956_C59112) , ($fila->G2956_C59113) , ($fila->G2956_C59114) , ($fila->G2956_C59115) , ($fila->G2956_C59096) , ($fila->G2956_C59097) , explode(' ', $fila->G2956_C59098)[0] , $hora_a , ($fila->G2956_C59100) , ($fila->G2956_C59101) , ($fila->G2956_C59102) , ($fila->G2956_C59103) , ($fila->G2956_C59104) , ($fila->G2956_C59106) , ($fila->G2956_C59116) , ($fila->G2956_C59123) , ($fila->G2956_C59530) , ($fila->G2956_C59125) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2956 WHERE G2956_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2956";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2956_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2956_ConsInte__b as id,  G2956_C59106 as camp1 , G2956_C59116 as camp2  FROM '.$BaseDatos.'.G2956 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2956_ConsInte__b as id,  G2956_C59106 as camp1 , G2956_C59116 as camp2  
                    FROM ".$BaseDatos.".G2956  JOIN ".$BaseDatos.".G2956_M".$_POST['muestra']." ON G2956_ConsInte__b = G2956_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2956_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2956_C59106 LIKE "%'.$B.'%" OR G2956_C59116 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2956_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2956 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2956(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2956_C59110"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59110 = '".$_POST["G2956_C59110"]."'";
                $LsqlI .= $separador."G2956_C59110";
                $LsqlV .= $separador."'".$_POST["G2956_C59110"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59111"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59111 = '".$_POST["G2956_C59111"]."'";
                $LsqlI .= $separador."G2956_C59111";
                $LsqlV .= $separador."'".$_POST["G2956_C59111"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59112"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59112 = '".$_POST["G2956_C59112"]."'";
                $LsqlI .= $separador."G2956_C59112";
                $LsqlV .= $separador."'".$_POST["G2956_C59112"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59113 = '".$_POST["G2956_C59113"]."'";
                $LsqlI .= $separador."G2956_C59113";
                $LsqlV .= $separador."'".$_POST["G2956_C59113"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59114"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59114 = '".$_POST["G2956_C59114"]."'";
                $LsqlI .= $separador."G2956_C59114";
                $LsqlV .= $separador."'".$_POST["G2956_C59114"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59115"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59115 = '".$_POST["G2956_C59115"]."'";
                $LsqlI .= $separador."G2956_C59115";
                $LsqlV .= $separador."'".$_POST["G2956_C59115"]."'";
                $validar = 1;
            }
             
 
            $G2956_C59096 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2956_C59096 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2956_C59096 = ".$G2956_C59096;
                    $LsqlI .= $separador." G2956_C59096";
                    $LsqlV .= $separador.$G2956_C59096;
                    $validar = 1;

                    
                }
            }
 
            $G2956_C59097 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2956_C59097 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2956_C59097 = ".$G2956_C59097;
                    $LsqlI .= $separador." G2956_C59097";
                    $LsqlV .= $separador.$G2956_C59097;
                    $validar = 1;
                }
            }
 
            $G2956_C59098 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2956_C59098 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2956_C59098 = ".$G2956_C59098;
                    $LsqlI .= $separador." G2956_C59098";
                    $LsqlV .= $separador.$G2956_C59098;
                    $validar = 1;
                }
            }
 
            $G2956_C59099 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2956_C59099 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2956_C59099 = ".$G2956_C59099;
                    $LsqlI .= $separador." G2956_C59099";
                    $LsqlV .= $separador.$G2956_C59099;
                    $validar = 1;
                }
            }
 
            $G2956_C59100 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2956_C59100 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2956_C59100 = ".$G2956_C59100;
                    $LsqlI .= $separador." G2956_C59100";
                    $LsqlV .= $separador.$G2956_C59100;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2956_C59101"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59101 = '".$_POST["G2956_C59101"]."'";
                $LsqlI .= $separador."G2956_C59101";
                $LsqlV .= $separador."'".$_POST["G2956_C59101"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59102"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59102 = '".$_POST["G2956_C59102"]."'";
                $LsqlI .= $separador."G2956_C59102";
                $LsqlV .= $separador."'".$_POST["G2956_C59102"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59103"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59103 = '".$_POST["G2956_C59103"]."'";
                $LsqlI .= $separador."G2956_C59103";
                $LsqlV .= $separador."'".$_POST["G2956_C59103"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59104"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59104 = '".$_POST["G2956_C59104"]."'";
                $LsqlI .= $separador."G2956_C59104";
                $LsqlV .= $separador."'".$_POST["G2956_C59104"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59105"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59105 = '".$_POST["G2956_C59105"]."'";
                $LsqlI .= $separador."G2956_C59105";
                $LsqlV .= $separador."'".$_POST["G2956_C59105"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59106"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59106 = '".$_POST["G2956_C59106"]."'";
                $LsqlI .= $separador."G2956_C59106";
                $LsqlV .= $separador."'".$_POST["G2956_C59106"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59116"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59116 = '".$_POST["G2956_C59116"]."'";
                $LsqlI .= $separador."G2956_C59116";
                $LsqlV .= $separador."'".$_POST["G2956_C59116"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59117"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59117 = '".$_POST["G2956_C59117"]."'";
                $LsqlI .= $separador."G2956_C59117";
                $LsqlV .= $separador."'".$_POST["G2956_C59117"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59118"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59118 = '".$_POST["G2956_C59118"]."'";
                $LsqlI .= $separador."G2956_C59118";
                $LsqlV .= $separador."'".$_POST["G2956_C59118"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59119"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59119 = '".$_POST["G2956_C59119"]."'";
                $LsqlI .= $separador."G2956_C59119";
                $LsqlV .= $separador."'".$_POST["G2956_C59119"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59120"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59120 = '".$_POST["G2956_C59120"]."'";
                $LsqlI .= $separador."G2956_C59120";
                $LsqlV .= $separador."'".$_POST["G2956_C59120"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59121"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59121 = '".$_POST["G2956_C59121"]."'";
                $LsqlI .= $separador."G2956_C59121";
                $LsqlV .= $separador."'".$_POST["G2956_C59121"]."'";
                $validar = 1;
            }
             
  
            $G2956_C59123 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2956_C59123"])){
                if($_POST["G2956_C59123"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2956_C59123 = $_POST["G2956_C59123"];
                    $LsqlU .= $separador." G2956_C59123 = ".$G2956_C59123."";
                    $LsqlI .= $separador." G2956_C59123";
                    $LsqlV .= $separador.$G2956_C59123;
                    $validar = 1;
                }
            }
  
            $G2956_C59530 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2956_C59530"])){
                if($_POST["G2956_C59530"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2956_C59530 = $_POST["G2956_C59530"];
                    $LsqlU .= $separador." G2956_C59530 = ".$G2956_C59530."";
                    $LsqlI .= $separador." G2956_C59530";
                    $LsqlV .= $separador.$G2956_C59530;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2956_C59125"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59125 = '".$_POST["G2956_C59125"]."'";
                $LsqlI .= $separador."G2956_C59125";
                $LsqlV .= $separador."'".$_POST["G2956_C59125"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2956_C59126"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_C59126 = '".$_POST["G2956_C59126"]."'";
                $LsqlI .= $separador."G2956_C59126";
                $LsqlV .= $separador."'".$_POST["G2956_C59126"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2956_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2956_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2956_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2956_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2956_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2956_Usuario , G2956_FechaInsercion, G2956_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2956_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2956 WHERE G2956_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }

    if (isset($_GET["SumarPedidos"])) {

        $idPedido = $_POST["idPedido"];

        $strSum_t = "SELECT SUM(G2967_C59385) AS suma FROM ".$BaseDatos.".G2967 WHERE G2967_C59381 = ".$idPedido;
        $resSum_t = $mysqli->query($strSum_t);

        if ($resSum_t && $resSum_t->num_rows > 0) {
            echo $resSum_t->fetch_object()->suma;;
        }

    }

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2967_ConsInte__b, G2967_C59381, G2967_C59529, G2951_C59033 as G2967_C59386, G2967_C59382, G2967_C59383, G2967_C59385, G2967_C59384 FROM ".$BaseDatos.".G2967  LEFT JOIN ".$BaseDatos.".G2951 ON G2951_ConsInte__b  =  G2967_C59386 ";

        $SQL .= " WHERE G2967_C59381 = '".$numero."'"; 

        $SQL .= " ORDER BY G2967_C59381";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2967_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2967_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G2967_C59381."</cell>"; 

                echo "<cell>". $fila->G2967_C59529."</cell>"; 

                echo "<cell>". ($fila->G2967_C59386)."</cell>";

                echo "<cell>". ($fila->G2967_C59382)."</cell>";

                echo "<cell>". $fila->G2967_C59383."</cell>"; 

                echo "<cell>". $fila->G2967_C59385."</cell>"; 

                echo "<cell>". $fila->G2967_C59384."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2967 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2967(";
            $LsqlV = " VALUES ("; 
  
                $G2967_C59529 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59529"])){    
                    if($_POST["G2967_C59529"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59529 = $_POST["G2967_C59529"];
                        $LsqlU .= $separador." G2967_C59529 = '".$G2967_C59529."'";
                        $LsqlI .= $separador." G2967_C59529";
                        $LsqlV .= $separador."'".$G2967_C59529."'";
                        $validar = 1;
                    }
                }
  
                if(isset($_POST["G2967_C59386"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2967_C59386 = '".$_POST["G2967_C59386"]."'";
                    $LsqlI .= $separador."G2967_C59386";
                    $LsqlV .= $separador."'".$_POST["G2967_C59386"]."'";
                    $validar = 1;
                }
                
 

                if(isset($_POST["G2967_C59382"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2967_C59382 = '".$_POST["G2967_C59382"]."'";
                    $LsqlI .= $separador."G2967_C59382";
                    $LsqlV .= $separador."'".$_POST["G2967_C59382"]."'";
                    $validar = 1;
                }

                                                                               
 
                $G2967_C59383= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59383"])){    
                    if($_POST["G2967_C59383"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59383 = $_POST["G2967_C59383"];
                        $LsqlU .= $separador." G2967_C59383 = '".$G2967_C59383."'";
                        $LsqlI .= $separador." G2967_C59383";
                        $LsqlV .= $separador."'".$G2967_C59383."'";
                        $validar = 1;
                    }
                }
  
                $G2967_C59385 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59385"])){    
                    if($_POST["G2967_C59385"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59385 = $_POST["G2967_C59385"];
                        $LsqlU .= $separador." G2967_C59385 = '".$G2967_C59385."'";
                        $LsqlI .= $separador." G2967_C59385";
                        $LsqlV .= $separador."'".$G2967_C59385."'";
                        $validar = 1;
                    }
                }
  
                $G2967_C59384 = NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G2967_C59384"])){    
                    if($_POST["G2967_C59384"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2967_C59384 = $_POST["G2967_C59384"];
                        $LsqlU .= $separador." G2967_C59384 = '".$G2967_C59384."'";
                        $LsqlI .= $separador." G2967_C59384";
                        $LsqlV .= $separador."'".$G2967_C59384."'";
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2967_C59381 = $numero;
                    $LsqlU .= ", G2967_C59381 = ".$G2967_C59381."";
                    $LsqlI .= ", G2967_C59381";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2967_Usuario ,  G2967_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2967_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2967 WHERE  G2967_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
