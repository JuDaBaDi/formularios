<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2337_ConsInte__b, G2337_FechaInsercion , G2337_Usuario ,  G2337_CodigoMiembro  , G2337_PoblacionOrigen , G2337_EstadoDiligenciamiento ,  G2337_IdLlamada , G2337_C45688 as principal ,G2337_C45688,G2337_C45689,G2337_C45690,G2337_C45691,G2337_C45692,G2337_C45693,G2337_C45694,G2337_C45695,G2337_C45696,G2337_C45697,G2337_C45698,G2337_C45699,G2337_C45700,G2337_C45701,G2337_C45702,G2337_C45703,G2337_C45704,G2337_C45705,G2337_C45706,G2337_C45707,G2337_C45708,G2337_C45709,G2337_C45710,G2337_C45711,G2337_C52353,G2337_C56083,G2337_C45712,G2337_C45713,G2337_C45714,G2337_C51272,G2337_C51273,G2337_C51274,G2337_C51275,G2337_C51276,G2337_C51277,G2337_C51278,G2337_C51279,G2337_C51280,G2337_C51281,G2337_C51282,G2337_C51365,G2337_C55953,G2337_C55954 FROM '.$BaseDatos.'.G2337 WHERE G2337_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2337_C45688'] = $key->G2337_C45688;

                $datos[$i]['G2337_C45689'] = $key->G2337_C45689;

                $datos[$i]['G2337_C45690'] = $key->G2337_C45690;

                $datos[$i]['G2337_C45691'] = $key->G2337_C45691;

                $datos[$i]['G2337_C45692'] = $key->G2337_C45692;

                $datos[$i]['G2337_C45693'] = $key->G2337_C45693;

                $datos[$i]['G2337_C45694'] = $key->G2337_C45694;

                $datos[$i]['G2337_C45695'] = $key->G2337_C45695;

                $datos[$i]['G2337_C45696'] = $key->G2337_C45696;

                $datos[$i]['G2337_C45697'] = $key->G2337_C45697;

                $datos[$i]['G2337_C45698'] = $key->G2337_C45698;

                $datos[$i]['G2337_C45699'] = $key->G2337_C45699;

                $datos[$i]['G2337_C45700'] = $key->G2337_C45700;

                $datos[$i]['G2337_C45701'] = $key->G2337_C45701;

                $datos[$i]['G2337_C45702'] = $key->G2337_C45702;

                $datos[$i]['G2337_C45703'] = $key->G2337_C45703;

                $datos[$i]['G2337_C45704'] = $key->G2337_C45704;

                $datos[$i]['G2337_C45705'] = $key->G2337_C45705;

                $datos[$i]['G2337_C45706'] = $key->G2337_C45706;

                $datos[$i]['G2337_C45707'] = $key->G2337_C45707;

                $datos[$i]['G2337_C45708'] = $key->G2337_C45708;

                $datos[$i]['G2337_C45709'] = $key->G2337_C45709;

                $datos[$i]['G2337_C45710'] = $key->G2337_C45710;

                $datos[$i]['G2337_C45711'] = $key->G2337_C45711;

                $datos[$i]['G2337_C52353'] = $key->G2337_C52353;

                $datos[$i]['G2337_C56083'] = $key->G2337_C56083;

                $datos[$i]['G2337_C45712'] = $key->G2337_C45712;

                $datos[$i]['G2337_C45713'] = $key->G2337_C45713;

                $datos[$i]['G2337_C45714'] = $key->G2337_C45714;

                $datos[$i]['G2337_C51272'] = $key->G2337_C51272;

                $datos[$i]['G2337_C51273'] = $key->G2337_C51273;

                $datos[$i]['G2337_C51274'] = $key->G2337_C51274;

                $datos[$i]['G2337_C51275'] = $key->G2337_C51275;

                $datos[$i]['G2337_C51276'] = $key->G2337_C51276;

                $datos[$i]['G2337_C51277'] = $key->G2337_C51277;

                $datos[$i]['G2337_C51278'] = $key->G2337_C51278;

                $datos[$i]['G2337_C51279'] = $key->G2337_C51279;

                $datos[$i]['G2337_C51280'] = $key->G2337_C51280;

                $datos[$i]['G2337_C51281'] = $key->G2337_C51281;

                $datos[$i]['G2337_C51282'] = explode(' ', $key->G2337_C51282)[0];

                $datos[$i]['G2337_C51365'] = explode(' ', $key->G2337_C51365)[0];

                $datos[$i]['G2337_C55953'] = $key->G2337_C55953;

                $datos[$i]['G2337_C55954'] = $key->G2337_C55954;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2337";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2337_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2337_ConsInte__b as id,  G2337_C45688 as camp1 , G2337_C45689 as camp2 
                     FROM ".$BaseDatos.".G2337  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2337_ConsInte__b as id,  G2337_C45688 as camp1 , G2337_C45689 as camp2  
                    FROM ".$BaseDatos.".G2337  JOIN ".$BaseDatos.".G2337_M".$_POST['muestra']." ON G2337_ConsInte__b = G2337_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2337_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2337_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2337_C45688 LIKE '%".$B."%' OR G2337_C45689 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2337_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2337");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2337_ConsInte__b, G2337_FechaInsercion , G2337_Usuario ,  G2337_CodigoMiembro  , G2337_PoblacionOrigen , G2337_EstadoDiligenciamiento ,  G2337_IdLlamada , G2337_C45688 as principal ,G2337_C45688,G2337_C45689,G2337_C45690,G2337_C45691,G2337_C45692,G2337_C45693,G2337_C45694,G2337_C45695,G2337_C45696,G2337_C45697,G2337_C45698,G2337_C45699,G2337_C45700,G2337_C45701,G2337_C45702,G2337_C45703,G2337_C45704,G2337_C45705,G2337_C45706,G2337_C45707,G2337_C45708,G2337_C45709,G2337_C45710,G2337_C45711,G2337_C52353,G2337_C56083,G2337_C45712,G2337_C45713, a.LISOPC_Nombre____b as G2337_C45714, b.LISOPC_Nombre____b as G2337_C51272,G2337_C51273,G2337_C51274,G2337_C51275,G2337_C51276,G2337_C51277, c.LISOPC_Nombre____b as G2337_C51278, d.LISOPC_Nombre____b as G2337_C51279, e.LISOPC_Nombre____b as G2337_C51280,G2337_C51281,G2337_C51282,G2337_C51365,G2337_C55953,G2337_C55954 FROM '.$BaseDatos.'.G2337 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2337_C45714 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2337_C51272 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2337_C51278 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2337_C51279 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2337_C51280';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2337_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2337_ConsInte__b , ($fila->G2337_C45688) , ($fila->G2337_C45689) , ($fila->G2337_C45690) , ($fila->G2337_C45691) , ($fila->G2337_C45692) , ($fila->G2337_C45693) , ($fila->G2337_C45694) , ($fila->G2337_C45695) , ($fila->G2337_C45696) , ($fila->G2337_C45697) , ($fila->G2337_C45698) , ($fila->G2337_C45699) , ($fila->G2337_C45700) , ($fila->G2337_C45701) , ($fila->G2337_C45702) , ($fila->G2337_C45703) , ($fila->G2337_C45704) , ($fila->G2337_C45705) , ($fila->G2337_C45706) , ($fila->G2337_C45707) , ($fila->G2337_C45708) , ($fila->G2337_C45709) , ($fila->G2337_C45710) , ($fila->G2337_C45711) , ($fila->G2337_C52353) , ($fila->G2337_C56083) , ($fila->G2337_C45712) , ($fila->G2337_C45713) , ($fila->G2337_C45714) , ($fila->G2337_C51272) , ($fila->G2337_C51273) , ($fila->G2337_C51274) , ($fila->G2337_C51275) , ($fila->G2337_C51276) , ($fila->G2337_C51277) , ($fila->G2337_C51278) , ($fila->G2337_C51279) , ($fila->G2337_C51280) , ($fila->G2337_C51281) , explode(' ', $fila->G2337_C51282)[0] , explode(' ', $fila->G2337_C51365)[0] , ($fila->G2337_C55953) , ($fila->G2337_C55954) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2337 WHERE G2337_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2337";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2337_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2337_ConsInte__b as id,  G2337_C45688 as camp1 , G2337_C45689 as camp2  FROM '.$BaseDatos.'.G2337 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2337_ConsInte__b as id,  G2337_C45688 as camp1 , G2337_C45689 as camp2  
                    FROM ".$BaseDatos.".G2337  JOIN ".$BaseDatos.".G2337_M".$_POST['muestra']." ON G2337_ConsInte__b = G2337_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2337_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2337_C45688 LIKE "%'.$B.'%" OR G2337_C45689 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2337_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2337 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2337(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2337_C45688"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45688 = '".$_POST["G2337_C45688"]."'";
                $LsqlI .= $separador."G2337_C45688";
                $LsqlV .= $separador."'".$_POST["G2337_C45688"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45689"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45689 = '".$_POST["G2337_C45689"]."'";
                $LsqlI .= $separador."G2337_C45689";
                $LsqlV .= $separador."'".$_POST["G2337_C45689"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45690"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45690 = '".$_POST["G2337_C45690"]."'";
                $LsqlI .= $separador."G2337_C45690";
                $LsqlV .= $separador."'".$_POST["G2337_C45690"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45691"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45691 = '".$_POST["G2337_C45691"]."'";
                $LsqlI .= $separador."G2337_C45691";
                $LsqlV .= $separador."'".$_POST["G2337_C45691"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45692"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45692 = '".$_POST["G2337_C45692"]."'";
                $LsqlI .= $separador."G2337_C45692";
                $LsqlV .= $separador."'".$_POST["G2337_C45692"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45693"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45693 = '".$_POST["G2337_C45693"]."'";
                $LsqlI .= $separador."G2337_C45693";
                $LsqlV .= $separador."'".$_POST["G2337_C45693"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45694"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45694 = '".$_POST["G2337_C45694"]."'";
                $LsqlI .= $separador."G2337_C45694";
                $LsqlV .= $separador."'".$_POST["G2337_C45694"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45695"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45695 = '".$_POST["G2337_C45695"]."'";
                $LsqlI .= $separador."G2337_C45695";
                $LsqlV .= $separador."'".$_POST["G2337_C45695"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45696"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45696 = '".$_POST["G2337_C45696"]."'";
                $LsqlI .= $separador."G2337_C45696";
                $LsqlV .= $separador."'".$_POST["G2337_C45696"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45697"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45697 = '".$_POST["G2337_C45697"]."'";
                $LsqlI .= $separador."G2337_C45697";
                $LsqlV .= $separador."'".$_POST["G2337_C45697"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45698"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45698 = '".$_POST["G2337_C45698"]."'";
                $LsqlI .= $separador."G2337_C45698";
                $LsqlV .= $separador."'".$_POST["G2337_C45698"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45699"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45699 = '".$_POST["G2337_C45699"]."'";
                $LsqlI .= $separador."G2337_C45699";
                $LsqlV .= $separador."'".$_POST["G2337_C45699"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45700"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45700 = '".$_POST["G2337_C45700"]."'";
                $LsqlI .= $separador."G2337_C45700";
                $LsqlV .= $separador."'".$_POST["G2337_C45700"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45701"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45701 = '".$_POST["G2337_C45701"]."'";
                $LsqlI .= $separador."G2337_C45701";
                $LsqlV .= $separador."'".$_POST["G2337_C45701"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45702"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45702 = '".$_POST["G2337_C45702"]."'";
                $LsqlI .= $separador."G2337_C45702";
                $LsqlV .= $separador."'".$_POST["G2337_C45702"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45703"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45703 = '".$_POST["G2337_C45703"]."'";
                $LsqlI .= $separador."G2337_C45703";
                $LsqlV .= $separador."'".$_POST["G2337_C45703"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45704"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45704 = '".$_POST["G2337_C45704"]."'";
                $LsqlI .= $separador."G2337_C45704";
                $LsqlV .= $separador."'".$_POST["G2337_C45704"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45705"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45705 = '".$_POST["G2337_C45705"]."'";
                $LsqlI .= $separador."G2337_C45705";
                $LsqlV .= $separador."'".$_POST["G2337_C45705"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45706"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45706 = '".$_POST["G2337_C45706"]."'";
                $LsqlI .= $separador."G2337_C45706";
                $LsqlV .= $separador."'".$_POST["G2337_C45706"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45707"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45707 = '".$_POST["G2337_C45707"]."'";
                $LsqlI .= $separador."G2337_C45707";
                $LsqlV .= $separador."'".$_POST["G2337_C45707"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45708"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45708 = '".$_POST["G2337_C45708"]."'";
                $LsqlI .= $separador."G2337_C45708";
                $LsqlV .= $separador."'".$_POST["G2337_C45708"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45709"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45709 = '".$_POST["G2337_C45709"]."'";
                $LsqlI .= $separador."G2337_C45709";
                $LsqlV .= $separador."'".$_POST["G2337_C45709"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45710"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45710 = '".$_POST["G2337_C45710"]."'";
                $LsqlI .= $separador."G2337_C45710";
                $LsqlV .= $separador."'".$_POST["G2337_C45710"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45711"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45711 = '".$_POST["G2337_C45711"]."'";
                $LsqlI .= $separador."G2337_C45711";
                $LsqlV .= $separador."'".$_POST["G2337_C45711"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C52353"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C52353 = '".$_POST["G2337_C52353"]."'";
                $LsqlI .= $separador."G2337_C52353";
                $LsqlV .= $separador."'".$_POST["G2337_C52353"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C56083"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C56083 = '".$_POST["G2337_C56083"]."'";
                $LsqlI .= $separador."G2337_C56083";
                $LsqlV .= $separador."'".$_POST["G2337_C56083"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45712"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45712 = '".$_POST["G2337_C45712"]."'";
                $LsqlI .= $separador."G2337_C45712";
                $LsqlV .= $separador."'".$_POST["G2337_C45712"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45713"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45713 = '".$_POST["G2337_C45713"]."'";
                $LsqlI .= $separador."G2337_C45713";
                $LsqlV .= $separador."'".$_POST["G2337_C45713"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C45714"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C45714 = '".$_POST["G2337_C45714"]."'";
                $LsqlI .= $separador."G2337_C45714";
                $LsqlV .= $separador."'".$_POST["G2337_C45714"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51272"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51272 = '".$_POST["G2337_C51272"]."'";
                $LsqlI .= $separador."G2337_C51272";
                $LsqlV .= $separador."'".$_POST["G2337_C51272"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51273"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51273 = '".$_POST["G2337_C51273"]."'";
                $LsqlI .= $separador."G2337_C51273";
                $LsqlV .= $separador."'".$_POST["G2337_C51273"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51274"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51274 = '".$_POST["G2337_C51274"]."'";
                $LsqlI .= $separador."G2337_C51274";
                $LsqlV .= $separador."'".$_POST["G2337_C51274"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51275"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51275 = '".$_POST["G2337_C51275"]."'";
                $LsqlI .= $separador."G2337_C51275";
                $LsqlV .= $separador."'".$_POST["G2337_C51275"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51276"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51276 = '".$_POST["G2337_C51276"]."'";
                $LsqlI .= $separador."G2337_C51276";
                $LsqlV .= $separador."'".$_POST["G2337_C51276"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51277"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51277 = '".$_POST["G2337_C51277"]."'";
                $LsqlI .= $separador."G2337_C51277";
                $LsqlV .= $separador."'".$_POST["G2337_C51277"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51278"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51278 = '".$_POST["G2337_C51278"]."'";
                $LsqlI .= $separador."G2337_C51278";
                $LsqlV .= $separador."'".$_POST["G2337_C51278"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51279"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51279 = '".$_POST["G2337_C51279"]."'";
                $LsqlI .= $separador."G2337_C51279";
                $LsqlV .= $separador."'".$_POST["G2337_C51279"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51280"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51280 = '".$_POST["G2337_C51280"]."'";
                $LsqlI .= $separador."G2337_C51280";
                $LsqlV .= $separador."'".$_POST["G2337_C51280"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C51281"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C51281 = '".$_POST["G2337_C51281"]."'";
                $LsqlI .= $separador."G2337_C51281";
                $LsqlV .= $separador."'".$_POST["G2337_C51281"]."'";
                $validar = 1;
            }
             
 
            $G2337_C51282 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2337_C51282"])){    
                if($_POST["G2337_C51282"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2337_C51282"]);
                    if(count($tieneHora) > 1){
                        $G2337_C51282 = "'".$_POST["G2337_C51282"]."'";
                    }else{
                        $G2337_C51282 = "'".str_replace(' ', '',$_POST["G2337_C51282"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2337_C51282 = ".$G2337_C51282;
                    $LsqlI .= $separador." G2337_C51282";
                    $LsqlV .= $separador.$G2337_C51282;
                    $validar = 1;
                }
            }
 
            $G2337_C51365 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2337_C51365"])){    
                if($_POST["G2337_C51365"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2337_C51365"]);
                    if(count($tieneHora) > 1){
                        $G2337_C51365 = "'".$_POST["G2337_C51365"]."'";
                    }else{
                        $G2337_C51365 = "'".str_replace(' ', '',$_POST["G2337_C51365"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2337_C51365 = ".$G2337_C51365;
                    $LsqlI .= $separador." G2337_C51365";
                    $LsqlV .= $separador.$G2337_C51365;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2337_C55953"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C55953 = '".$_POST["G2337_C55953"]."'";
                $LsqlI .= $separador."G2337_C55953";
                $LsqlV .= $separador."'".$_POST["G2337_C55953"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2337_C55954"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_C55954 = '".$_POST["G2337_C55954"]."'";
                $LsqlI .= $separador."G2337_C55954";
                $LsqlV .= $separador."'".$_POST["G2337_C55954"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2337_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2337_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2337_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2337_Usuario , G2337_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2337_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2337 WHERE G2337_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2337 SET G2337_UltiGest__b =-14, G2337_GesMasImp_b =-14, G2337_TipoReintentoUG_b =0, G2337_TipoReintentoGMI_b =0, G2337_EstadoUG_b =-14, G2337_EstadoGMI_b =-14, G2337_CantidadIntentos =0, G2337_CantidadIntentosGMI_b =0 WHERE G2337_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
