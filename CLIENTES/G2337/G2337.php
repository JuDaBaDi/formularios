
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2337/G2337_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2337_ConsInte__b as id, G2337_C45688 as camp1 , G2337_C45689 as camp2 FROM ".$BaseDatos.".G2337  WHERE G2337_Usuario = ".$idUsuario." ORDER BY G2337_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2337_ConsInte__b as id, G2337_C45688 as camp1 , G2337_C45689 as camp2 FROM ".$BaseDatos.".G2337  ORDER BY G2337_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2337_ConsInte__b as id, G2337_C45688 as camp1 , G2337_C45689 as camp2 FROM ".$BaseDatos.".G2337 JOIN ".$BaseDatos.".G2337_M".$resultEstpas->muestr." ON G2337_ConsInte__b = G2337_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2337_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2337_ConsInte__b as id, G2337_C45688 as camp1 , G2337_C45689 as camp2 FROM ".$BaseDatos.".G2337 JOIN ".$BaseDatos.".G2337_M".$resultEstpas->muestr." ON G2337_ConsInte__b = G2337_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2337_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2337_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2337_ConsInte__b as id, G2337_C45688 as camp1 , G2337_C45689 as camp2 FROM ".$BaseDatos.".G2337  ORDER BY G2337_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="6698" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45688" id="LblG2337_C45688">CEDULA</label><input type="text" class="form-control input-sm" id="G2337_C45688" value="<?php if (isset($_GET['G2337_C45688'])) {
                            echo $_GET['G2337_C45688'];
                        } ?>"  name="G2337_C45688"  placeholder="CEDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45689" id="LblG2337_C45689">GIRADOR</label><input type="text" class="form-control input-sm" id="G2337_C45689" value="<?php if (isset($_GET['G2337_C45689'])) {
                            echo $_GET['G2337_C45689'];
                        } ?>"  name="G2337_C45689"  placeholder="GIRADOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45690" id="LblG2337_C45690">NUMERO DE CREDITO</label><input type="text" class="form-control input-sm" id="G2337_C45690" value="<?php if (isset($_GET['G2337_C45690'])) {
                            echo $_GET['G2337_C45690'];
                        } ?>"  name="G2337_C45690"  placeholder="NUMERO DE CREDITO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45691" id="LblG2337_C45691">RESPONSABLE</label><input type="text" class="form-control input-sm" id="G2337_C45691" value="<?php if (isset($_GET['G2337_C45691'])) {
                            echo $_GET['G2337_C45691'];
                        } ?>"  name="G2337_C45691"  placeholder="RESPONSABLE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45692" id="LblG2337_C45692">EDAD_MORA</label><input type="text" class="form-control input-sm" id="G2337_C45692" value="<?php if (isset($_GET['G2337_C45692'])) {
                            echo $_GET['G2337_C45692'];
                        } ?>"  name="G2337_C45692"  placeholder="EDAD_MORA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45693" id="LblG2337_C45693">MARCA_TARJETA</label><input type="text" class="form-control input-sm" id="G2337_C45693" value="<?php if (isset($_GET['G2337_C45693'])) {
                            echo $_GET['G2337_C45693'];
                        } ?>"  name="G2337_C45693"  placeholder="MARCA_TARJETA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45694" id="LblG2337_C45694">ETAPA</label><input type="text" class="form-control input-sm" id="G2337_C45694" value="<?php if (isset($_GET['G2337_C45694'])) {
                            echo $_GET['G2337_C45694'];
                        } ?>"  name="G2337_C45694"  placeholder="ETAPA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45695" id="LblG2337_C45695">TELEFONO_1</label><input type="text" class="form-control input-sm" id="G2337_C45695" value="<?php if (isset($_GET['G2337_C45695'])) {
                            echo $_GET['G2337_C45695'];
                        } ?>"  name="G2337_C45695"  placeholder="TELEFONO_1"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45696" id="LblG2337_C45696">TELEFONO_2</label><input type="text" class="form-control input-sm" id="G2337_C45696" value="<?php if (isset($_GET['G2337_C45696'])) {
                            echo $_GET['G2337_C45696'];
                        } ?>"  name="G2337_C45696"  placeholder="TELEFONO_2"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45697" id="LblG2337_C45697">TELEFONO_3</label><input type="text" class="form-control input-sm" id="G2337_C45697" value="<?php if (isset($_GET['G2337_C45697'])) {
                            echo $_GET['G2337_C45697'];
                        } ?>"  name="G2337_C45697"  placeholder="TELEFONO_3"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45698" id="LblG2337_C45698">TELEFONO_4</label><input type="text" class="form-control input-sm" id="G2337_C45698" value="<?php if (isset($_GET['G2337_C45698'])) {
                            echo $_GET['G2337_C45698'];
                        } ?>"  name="G2337_C45698"  placeholder="TELEFONO_4"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45699" id="LblG2337_C45699">TELEFONO_5</label><input type="text" class="form-control input-sm" id="G2337_C45699" value="<?php if (isset($_GET['G2337_C45699'])) {
                            echo $_GET['G2337_C45699'];
                        } ?>"  name="G2337_C45699"  placeholder="TELEFONO_5"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45700" id="LblG2337_C45700">FECHA_VENCIMIENTO</label><input type="text" class="form-control input-sm" id="G2337_C45700" value="<?php if (isset($_GET['G2337_C45700'])) {
                            echo $_GET['G2337_C45700'];
                        } ?>"  name="G2337_C45700"  placeholder="FECHA_VENCIMIENTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45701" id="LblG2337_C45701">FECHA_GENERACION</label><input type="text" class="form-control input-sm" id="G2337_C45701" value="<?php if (isset($_GET['G2337_C45701'])) {
                            echo $_GET['G2337_C45701'];
                        } ?>"  name="G2337_C45701"  placeholder="FECHA_GENERACION"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45702" id="LblG2337_C45702">SALDO_CAPITAL_NO_FACTURADO</label><input type="text" class="form-control input-sm" id="G2337_C45702" value="<?php if (isset($_GET['G2337_C45702'])) {
                            echo $_GET['G2337_C45702'];
                        } ?>"  name="G2337_C45702"  placeholder="SALDO_CAPITAL_NO_FACTURADO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45703" id="LblG2337_C45703">SALDO_CAPITAL_FACTURADO</label><input type="text" class="form-control input-sm" id="G2337_C45703" value="<?php if (isset($_GET['G2337_C45703'])) {
                            echo $_GET['G2337_C45703'];
                        } ?>"  name="G2337_C45703"  placeholder="SALDO_CAPITAL_FACTURADO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45704" id="LblG2337_C45704">CAPITAL MORA</label><input type="text" class="form-control input-sm" id="G2337_C45704" value="<?php if (isset($_GET['G2337_C45704'])) {
                            echo $_GET['G2337_C45704'];
                        } ?>"  name="G2337_C45704"  placeholder="CAPITAL MORA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45705" id="LblG2337_C45705">SALDO_INTERES_CORRIENTE</label><input type="text" class="form-control input-sm" id="G2337_C45705" value="<?php if (isset($_GET['G2337_C45705'])) {
                            echo $_GET['G2337_C45705'];
                        } ?>"  name="G2337_C45705"  placeholder="SALDO_INTERES_CORRIENTE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45706" id="LblG2337_C45706">SALDO_INTERES_MORA</label><input type="text" class="form-control input-sm" id="G2337_C45706" value="<?php if (isset($_GET['G2337_C45706'])) {
                            echo $_GET['G2337_C45706'];
                        } ?>"  name="G2337_C45706"  placeholder="SALDO_INTERES_MORA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45707" id="LblG2337_C45707">SALDO_INTERES_EXTRACONTABLE</label><input type="text" class="form-control input-sm" id="G2337_C45707" value="<?php if (isset($_GET['G2337_C45707'])) {
                            echo $_GET['G2337_C45707'];
                        } ?>"  name="G2337_C45707"  placeholder="SALDO_INTERES_EXTRACONTABLE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45708" id="LblG2337_C45708">GAC</label><input type="text" class="form-control input-sm" id="G2337_C45708" value="<?php if (isset($_GET['G2337_C45708'])) {
                            echo $_GET['G2337_C45708'];
                        } ?>"  name="G2337_C45708"  placeholder="GAC"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45709" id="LblG2337_C45709">PAGO TOTAL AL CORTE</label><input type="text" class="form-control input-sm" id="G2337_C45709" value="<?php if (isset($_GET['G2337_C45709'])) {
                            echo $_GET['G2337_C45709'];
                        } ?>"  name="G2337_C45709"  placeholder="PAGO TOTAL AL CORTE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45710" id="LblG2337_C45710">HONORARIOS 10%</label><input type="text" class="form-control input-sm" id="G2337_C45710" value="<?php if (isset($_GET['G2337_C45710'])) {
                            echo $_GET['G2337_C45710'];
                        } ?>"  name="G2337_C45710"  placeholder="HONORARIOS 10%"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45711" id="LblG2337_C45711">PAGO TOTAL AL CORTE CON HONORARIOS</label><input type="text" class="form-control input-sm" id="G2337_C45711" value="<?php if (isset($_GET['G2337_C45711'])) {
                            echo $_GET['G2337_C45711'];
                        } ?>"  name="G2337_C45711"  placeholder="PAGO TOTAL AL CORTE CON HONORARIOS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C52353" id="LblG2337_C52353">AGENTE</label><input type="text" class="form-control input-sm" id="G2337_C52353" value="<?php if (isset($_GET['G2337_C52353'])) {
                            echo $_GET['G2337_C52353'];
                        } ?>"  name="G2337_C52353"  placeholder="AGENTE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C56083" id="LblG2337_C56083">ASESOR</label><input type="text" class="form-control input-sm" id="G2337_C56083" value="<?php if (isset($_GET['G2337_C56083'])) {
                            echo $_GET['G2337_C56083'];
                        } ?>"  name="G2337_C56083"  placeholder="ASESOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="6699" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45712" id="LblG2337_C45712">ORIGEN_DY_WF</label><input type="text" class="form-control input-sm" id="G2337_C45712" value="<?php if (isset($_GET['G2337_C45712'])) {
                            echo $_GET['G2337_C45712'];
                        } ?>" readonly name="G2337_C45712"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C45713" id="LblG2337_C45713">OPTIN_DY_WF</label><input type="text" class="form-control input-sm" id="G2337_C45713" value="<?php if (isset($_GET['G2337_C45713'])) {
                            echo $_GET['G2337_C45713'];
                        } ?>" readonly name="G2337_C45713"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2337_C45714" id="LblG2337_C45714">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2337_C45714" id="G2337_C45714">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2586 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="7793" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7793c">
                PERFILACION DEUDOR
            </a>
        </h4>
        
    </div>
    <div id="s_7793c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2337_C51272" id="LblG2337_C51272">ESTADO ACTUAL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2337_C51272" id="G2337_C51272">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2575 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C51273" id="LblG2337_C51273">LUGAR DE TRABAJO</label><input type="text" class="form-control input-sm" id="G2337_C51273" value="<?php if (isset($_GET['G2337_C51273'])) {
                            echo $_GET['G2337_C51273'];
                        } ?>"  name="G2337_C51273"  placeholder="LUGAR DE TRABAJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C51274" id="LblG2337_C51274">CARGO O ACTIVIDAD ECONÓMICA</label><input type="text" class="form-control input-sm" id="G2337_C51274" value="<?php if (isset($_GET['G2337_C51274'])) {
                            echo $_GET['G2337_C51274'];
                        } ?>"  name="G2337_C51274"  placeholder="CARGO O ACTIVIDAD ECONÓMICA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C51275" id="LblG2337_C51275">NOMBRE DE LA EMPRESA</label><input type="text" class="form-control input-sm" id="G2337_C51275" value="<?php if (isset($_GET['G2337_C51275'])) {
                            echo $_GET['G2337_C51275'];
                        } ?>"  name="G2337_C51275"  placeholder="NOMBRE DE LA EMPRESA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C51276" id="LblG2337_C51276">DIRECCION ACTUALIZADA</label><input type="text" class="form-control input-sm" id="G2337_C51276" value="<?php if (isset($_GET['G2337_C51276'])) {
                            echo $_GET['G2337_C51276'];
                        } ?>"  name="G2337_C51276"  placeholder="DIRECCION ACTUALIZADA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C51277" id="LblG2337_C51277">TELEFONO WHATSAPP</label><input type="text" class="form-control input-sm" id="G2337_C51277" value="<?php if (isset($_GET['G2337_C51277'])) {
                            echo $_GET['G2337_C51277'];
                        } ?>"  name="G2337_C51277"  placeholder="TELEFONO WHATSAPP"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="7794" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7794c">
                CONTACTO
            </a>
        </h4>
        
    </div>
    <div id="s_7794c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2337_C51278" id="LblG2337_C51278">TIPO DE CONTACTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2337_C51278" id="G2337_C51278">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2902 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2337_C51279" id="LblG2337_C51279">RESULTADO CONTACTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2337_C51279" id="G2337_C51279">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2560 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2337_C51280" id="LblG2337_C51280">TIPO NEGOCIACIÓN </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2337_C51280" id="G2337_C51280">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3008 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C51281" id="LblG2337_C51281">VALOR ACUERDO</label><input type="text" class="form-control input-sm" id="G2337_C51281" value="<?php if (isset($_GET['G2337_C51281'])) {
                            echo $_GET['G2337_C51281'];
                        } ?>"  name="G2337_C51281"  placeholder="VALOR ACUERDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2337_C51282" id="LblG2337_C51282">FECHA DE PAGO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2337_C51282'])) {
                            echo $_GET['G2337_C51282'];
                        } ?>"  name="G2337_C51282" id="G2337_C51282" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2337_C51365" id="LblG2337_C51365">FECHA PRÓXIMO PAGO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2337_C51365'])) {
                            echo $_GET['G2337_C51365'];
                        } ?>"  name="G2337_C51365" id="G2337_C51365" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C55953" id="LblG2337_C55953">NUMERO CUOTAS</label><input type="text" class="form-control input-sm" id="G2337_C55953" value="<?php if (isset($_GET['G2337_C55953'])) {
                            echo $_GET['G2337_C55953'];
                        } ?>"  name="G2337_C55953"  placeholder="NUMERO CUOTAS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2337_C55954" id="LblG2337_C55954">FECHA DE PAGO</label><input type="text" class="form-control input-sm" id="G2337_C55954" value="<?php if (isset($_GET['G2337_C55954'])) {
                            echo $_GET['G2337_C55954'];
                        } ?>"  name="G2337_C55954"  placeholder="FECHA DE PAGO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2337/G2337_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2337_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2337_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2337_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2337_C45688").val(item.G2337_C45688); 
                $("#G2337_C45689").val(item.G2337_C45689); 
                $("#G2337_C45690").val(item.G2337_C45690); 
                $("#G2337_C45691").val(item.G2337_C45691); 
                $("#G2337_C45692").val(item.G2337_C45692); 
                $("#G2337_C45693").val(item.G2337_C45693); 
                $("#G2337_C45694").val(item.G2337_C45694); 
                $("#G2337_C45695").val(item.G2337_C45695); 
                $("#G2337_C45696").val(item.G2337_C45696); 
                $("#G2337_C45697").val(item.G2337_C45697); 
                $("#G2337_C45698").val(item.G2337_C45698); 
                $("#G2337_C45699").val(item.G2337_C45699); 
                $("#G2337_C45700").val(item.G2337_C45700); 
                $("#G2337_C45701").val(item.G2337_C45701); 
                $("#G2337_C45702").val(item.G2337_C45702); 
                $("#G2337_C45703").val(item.G2337_C45703); 
                $("#G2337_C45704").val(item.G2337_C45704); 
                $("#G2337_C45705").val(item.G2337_C45705); 
                $("#G2337_C45706").val(item.G2337_C45706); 
                $("#G2337_C45707").val(item.G2337_C45707); 
                $("#G2337_C45708").val(item.G2337_C45708); 
                $("#G2337_C45709").val(item.G2337_C45709); 
                $("#G2337_C45710").val(item.G2337_C45710); 
                $("#G2337_C45711").val(item.G2337_C45711); 
                $("#G2337_C52353").val(item.G2337_C52353); 
                $("#G2337_C56083").val(item.G2337_C56083); 
                $("#G2337_C45712").val(item.G2337_C45712); 
                $("#G2337_C45713").val(item.G2337_C45713); 
                $("#G2337_C45714").val(item.G2337_C45714).trigger("change");  
                $("#G2337_C51272").val(item.G2337_C51272).trigger("change");  
                $("#G2337_C51273").val(item.G2337_C51273); 
                $("#G2337_C51274").val(item.G2337_C51274); 
                $("#G2337_C51275").val(item.G2337_C51275); 
                $("#G2337_C51276").val(item.G2337_C51276); 
                $("#G2337_C51277").val(item.G2337_C51277); 
                $("#G2337_C51278").val(item.G2337_C51278).trigger("change");  
                $("#G2337_C51279").val(item.G2337_C51279).trigger("change");  
                $("#G2337_C51280").attr("opt",item.G2337_C51280);  
                $("#G2337_C51281").val(item.G2337_C51281); 
                $("#G2337_C51282").val(item.G2337_C51282); 
                $("#G2337_C51365").val(item.G2337_C51365); 
                $("#G2337_C55953").val(item.G2337_C55953); 
                $("#G2337_C55954").val(item.G2337_C55954);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2337_C45714").select2();

    $("#G2337_C51272").select2();

    $("#G2337_C51278").select2();

    $("#G2337_C51279").select2();

    $("#G2337_C51280").select2();
        //datepickers
        

        $("#G2337_C51282").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2337_C51365").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G2337_C45714").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO ACTUAL 

    $("#G2337_C51272").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CONTACTO 

    $("#G2337_C51278").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para RESULTADO CONTACTO 

    $("#G2337_C51279").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3008' , idPadre : $(this).val() },
            success : function(data){
                var optG2337_C51280 = $("#G2337_C51280").attr("opt");
                $("#G2337_C51280").html(data);
                if (optG2337_C51280 != null) {
                    $("#G2337_C51280").val(optG2337_C51280).trigger("change");
                }
            }
        });
        
    });

    //function para TIPO NEGOCIACIÓN  

    $("#G2337_C51280").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2337_C45688").val(item.G2337_C45688);
 
                                                $("#G2337_C45689").val(item.G2337_C45689);
 
                                                $("#G2337_C45690").val(item.G2337_C45690);
 
                                                $("#G2337_C45691").val(item.G2337_C45691);
 
                                                $("#G2337_C45692").val(item.G2337_C45692);
 
                                                $("#G2337_C45693").val(item.G2337_C45693);
 
                                                $("#G2337_C45694").val(item.G2337_C45694);
 
                                                $("#G2337_C45695").val(item.G2337_C45695);
 
                                                $("#G2337_C45696").val(item.G2337_C45696);
 
                                                $("#G2337_C45697").val(item.G2337_C45697);
 
                                                $("#G2337_C45698").val(item.G2337_C45698);
 
                                                $("#G2337_C45699").val(item.G2337_C45699);
 
                                                $("#G2337_C45700").val(item.G2337_C45700);
 
                                                $("#G2337_C45701").val(item.G2337_C45701);
 
                                                $("#G2337_C45702").val(item.G2337_C45702);
 
                                                $("#G2337_C45703").val(item.G2337_C45703);
 
                                                $("#G2337_C45704").val(item.G2337_C45704);
 
                                                $("#G2337_C45705").val(item.G2337_C45705);
 
                                                $("#G2337_C45706").val(item.G2337_C45706);
 
                                                $("#G2337_C45707").val(item.G2337_C45707);
 
                                                $("#G2337_C45708").val(item.G2337_C45708);
 
                                                $("#G2337_C45709").val(item.G2337_C45709);
 
                                                $("#G2337_C45710").val(item.G2337_C45710);
 
                                                $("#G2337_C45711").val(item.G2337_C45711);
 
                                                $("#G2337_C52353").val(item.G2337_C52353);
 
                                                $("#G2337_C56083").val(item.G2337_C56083);
 
                                                $("#G2337_C45712").val(item.G2337_C45712);
 
                                                $("#G2337_C45713").val(item.G2337_C45713);
 
                    $("#G2337_C45714").val(item.G2337_C45714).trigger("change"); 
 
                    $("#G2337_C51272").val(item.G2337_C51272).trigger("change"); 
 
                                                $("#G2337_C51273").val(item.G2337_C51273);
 
                                                $("#G2337_C51274").val(item.G2337_C51274);
 
                                                $("#G2337_C51275").val(item.G2337_C51275);
 
                                                $("#G2337_C51276").val(item.G2337_C51276);
 
                                                $("#G2337_C51277").val(item.G2337_C51277);
 
                    $("#G2337_C51278").val(item.G2337_C51278).trigger("change"); 
 
                    $("#G2337_C51279").val(item.G2337_C51279).trigger("change"); 
 
                    $("#G2337_C51280").attr("opt",item.G2337_C51280); 
 
                                                $("#G2337_C51281").val(item.G2337_C51281);
 
                                                $("#G2337_C51282").val(item.G2337_C51282);
 
                                                $("#G2337_C51365").val(item.G2337_C51365);
 
                                                $("#G2337_C55953").val(item.G2337_C55953);
 
                                                $("#G2337_C55954").val(item.G2337_C55954);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CEDULA','GIRADOR','NUMERO DE CREDITO','RESPONSABLE','EDAD_MORA','MARCA_TARJETA','ETAPA','TELEFONO_1','TELEFONO_2','TELEFONO_3','TELEFONO_4','TELEFONO_5','FECHA_VENCIMIENTO','FECHA_GENERACION','SALDO_CAPITAL_NO_FACTURADO','SALDO_CAPITAL_FACTURADO','CAPITAL MORA','SALDO_INTERES_CORRIENTE','SALDO_INTERES_MORA','SALDO_INTERES_EXTRACONTABLE','GAC','PAGO TOTAL AL CORTE','HONORARIOS 10%','PAGO TOTAL AL CORTE CON HONORARIOS','AGENTE','ASESOR','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','ESTADO ACTUAL','LUGAR DE TRABAJO','CARGO O ACTIVIDAD ECONÓMICA','NOMBRE DE LA EMPRESA','DIRECCION ACTUALIZADA','TELEFONO WHATSAPP','TIPO DE CONTACTO','RESULTADO CONTACTO','TIPO NEGOCIACIÓN ','VALOR ACUERDO','FECHA DE PAGO','FECHA PRÓXIMO PAGO','NUMERO CUOTAS','FECHA DE PAGO'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2337_C45688', 
                        index: 'G2337_C45688', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45689', 
                        index: 'G2337_C45689', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45690', 
                        index: 'G2337_C45690', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45691', 
                        index: 'G2337_C45691', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45692', 
                        index: 'G2337_C45692', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45693', 
                        index: 'G2337_C45693', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45694', 
                        index: 'G2337_C45694', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45695', 
                        index: 'G2337_C45695', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45696', 
                        index: 'G2337_C45696', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45697', 
                        index: 'G2337_C45697', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45698', 
                        index: 'G2337_C45698', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45699', 
                        index: 'G2337_C45699', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45700', 
                        index: 'G2337_C45700', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45701', 
                        index: 'G2337_C45701', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45702', 
                        index: 'G2337_C45702', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45703', 
                        index: 'G2337_C45703', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45704', 
                        index: 'G2337_C45704', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45705', 
                        index: 'G2337_C45705', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45706', 
                        index: 'G2337_C45706', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45707', 
                        index: 'G2337_C45707', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45708', 
                        index: 'G2337_C45708', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45709', 
                        index: 'G2337_C45709', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45710', 
                        index: 'G2337_C45710', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45711', 
                        index: 'G2337_C45711', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C52353', 
                        index: 'G2337_C52353', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C56083', 
                        index: 'G2337_C56083', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45712', 
                        index: 'G2337_C45712', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45713', 
                        index: 'G2337_C45713', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C45714', 
                        index:'G2337_C45714', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2586&campo=G2337_C45714'
                        }
                    }

                    ,
                    { 
                        name:'G2337_C51272', 
                        index:'G2337_C51272', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2575&campo=G2337_C51272'
                        }
                    }

                    ,
                    { 
                        name:'G2337_C51273', 
                        index: 'G2337_C51273', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C51274', 
                        index: 'G2337_C51274', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C51275', 
                        index: 'G2337_C51275', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C51276', 
                        index: 'G2337_C51276', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C51277', 
                        index: 'G2337_C51277', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C51278', 
                        index:'G2337_C51278', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2902&campo=G2337_C51278'
                        }
                    }

                    ,
                    { 
                        name:'G2337_C51279', 
                        index:'G2337_C51279', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2560&campo=G2337_C51279'
                        }
                    }

                    ,
                    { 
                        name:'G2337_C51280', 
                        index:'G2337_C51280', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3008&campo=G2337_C51280'
                        }
                    }

                    ,
                    { 
                        name:'G2337_C51281', 
                        index: 'G2337_C51281', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2337_C51282', 
                        index:'G2337_C51282', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2337_C51365', 
                        index:'G2337_C51365', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2337_C55953', 
                        index: 'G2337_C55953', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2337_C55954', 
                        index: 'G2337_C55954', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2337_C45688',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2337_C45688").val(item.G2337_C45688);

                        $("#G2337_C45689").val(item.G2337_C45689);

                        $("#G2337_C45690").val(item.G2337_C45690);

                        $("#G2337_C45691").val(item.G2337_C45691);

                        $("#G2337_C45692").val(item.G2337_C45692);

                        $("#G2337_C45693").val(item.G2337_C45693);

                        $("#G2337_C45694").val(item.G2337_C45694);

                        $("#G2337_C45695").val(item.G2337_C45695);

                        $("#G2337_C45696").val(item.G2337_C45696);

                        $("#G2337_C45697").val(item.G2337_C45697);

                        $("#G2337_C45698").val(item.G2337_C45698);

                        $("#G2337_C45699").val(item.G2337_C45699);

                        $("#G2337_C45700").val(item.G2337_C45700);

                        $("#G2337_C45701").val(item.G2337_C45701);

                        $("#G2337_C45702").val(item.G2337_C45702);

                        $("#G2337_C45703").val(item.G2337_C45703);

                        $("#G2337_C45704").val(item.G2337_C45704);

                        $("#G2337_C45705").val(item.G2337_C45705);

                        $("#G2337_C45706").val(item.G2337_C45706);

                        $("#G2337_C45707").val(item.G2337_C45707);

                        $("#G2337_C45708").val(item.G2337_C45708);

                        $("#G2337_C45709").val(item.G2337_C45709);

                        $("#G2337_C45710").val(item.G2337_C45710);

                        $("#G2337_C45711").val(item.G2337_C45711);

                        $("#G2337_C52353").val(item.G2337_C52353);

                        $("#G2337_C56083").val(item.G2337_C56083);

                        $("#G2337_C45712").val(item.G2337_C45712);

                        $("#G2337_C45713").val(item.G2337_C45713);
 
                    $("#G2337_C45714").val(item.G2337_C45714).trigger("change"); 
 
                    $("#G2337_C51272").val(item.G2337_C51272).trigger("change"); 

                        $("#G2337_C51273").val(item.G2337_C51273);

                        $("#G2337_C51274").val(item.G2337_C51274);

                        $("#G2337_C51275").val(item.G2337_C51275);

                        $("#G2337_C51276").val(item.G2337_C51276);

                        $("#G2337_C51277").val(item.G2337_C51277);
 
                    $("#G2337_C51278").val(item.G2337_C51278).trigger("change"); 
 
                    $("#G2337_C51279").val(item.G2337_C51279).trigger("change"); 
 
                    $("#G2337_C51280").attr("opt",item.G2337_C51280); 

                        $("#G2337_C51281").val(item.G2337_C51281);

                        $("#G2337_C51282").val(item.G2337_C51282);

                        $("#G2337_C51365").val(item.G2337_C51365);

                        $("#G2337_C55953").val(item.G2337_C55953);

                        $("#G2337_C55954").val(item.G2337_C55954);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
