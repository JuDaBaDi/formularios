
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2188/G2188_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2188_ConsInte__b as id, G2188_C42685 as camp1 , G2188_C42686 as camp2 FROM ".$BaseDatos.".G2188  WHERE G2188_Usuario = ".$idUsuario." ORDER BY G2188_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2188_ConsInte__b as id, G2188_C42685 as camp1 , G2188_C42686 as camp2 FROM ".$BaseDatos.".G2188  ORDER BY G2188_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2188_ConsInte__b as id, G2188_C42685 as camp1 , G2188_C42686 as camp2 FROM ".$BaseDatos.".G2188 JOIN ".$BaseDatos.".G2188_M".$resultEstpas->muestr." ON G2188_ConsInte__b = G2188_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2188_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2188_ConsInte__b as id, G2188_C42685 as camp1 , G2188_C42686 as camp2 FROM ".$BaseDatos.".G2188 JOIN ".$BaseDatos.".G2188_M".$resultEstpas->muestr." ON G2188_ConsInte__b = G2188_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2188_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2188_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2188_ConsInte__b as id, G2188_C42685 as camp1 , G2188_C42686 as camp2 FROM ".$BaseDatos.".G2188  ORDER BY G2188_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){


    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="6143" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6143c">
                GENERAL
            </a>
        </h4>
        
    </div>
    <div id="s_6143c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42685" id="LblG2188_C42685">NOMBRE</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42685" value="<?php if (isset($_GET['G2188_C42685'])) {
                            echo $_GET['G2188_C42685'];
                        } ?>"  name="G2188_C42685"  placeholder="NOMBRE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42686" id="LblG2188_C42686">CEDULA</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42686" value="<?php if (isset($_GET['G2188_C42686'])) {
                            echo $_GET['G2188_C42686'];
                        } ?>"  name="G2188_C42686"  placeholder="CEDULA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42687" id="LblG2188_C42687">CELULAR</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42687" value="<?php if (isset($_GET['G2188_C42687'])) {
                            echo $_GET['G2188_C42687'];
                        } ?>"  name="G2188_C42687"  placeholder="CELULAR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42688" id="LblG2188_C42688">CIUDAD 1 </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42688" id="G2188_C42688">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2339 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42689" id="LblG2188_C42689">FIJO</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42689" value="<?php if (isset($_GET['G2188_C42689'])) {
                            echo $_GET['G2188_C42689'];
                        } ?>"  name="G2188_C42689"  placeholder="FIJO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42690" id="LblG2188_C42690">DIRECCION</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42690" value="<?php if (isset($_GET['G2188_C42690'])) {
                            echo $_GET['G2188_C42690'];
                        } ?>"  name="G2188_C42690"  placeholder="DIRECCION">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42691" id="LblG2188_C42691">TIPO DE CLIENTE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42691" id="G2188_C42691">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2340 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="6145" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42679" id="LblG2188_C42679">Agente</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42679" value="<?php echo getNombreUser($token);?>" readonly name="G2188_C42679"  placeholder="Agente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42680" id="LblG2188_C42680">Fecha</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42680" value="<?php echo date('Y-m-d');?>" readonly name="G2188_C42680"  placeholder="Fecha">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42681" id="LblG2188_C42681">Hora</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42681" value="<?php echo date('H:i:s');?>" readonly name="G2188_C42681"  placeholder="Hora">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42682" id="LblG2188_C42682">Campaña</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42682" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G2188_C42682"  placeholder="Campaña">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="6144" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="6146" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6146c">
                PERSONA JURIDICA
            </a>
        </h4>
        
    </div>
    <div id="s_6146c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42683" id="LblG2188_C42683">TITULAR LINEA (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42683" value="<?php if (isset($_GET['G2188_C42683'])) {
                            echo $_GET['G2188_C42683'];
                        } ?>"  name="G2188_C42683"  placeholder="TITULAR LINEA (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42684" id="LblG2188_C42684">CEDULA TITULAR (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42684" value="<?php if (isset($_GET['G2188_C42684'])) {
                            echo $_GET['G2188_C42684'];
                        } ?>"  name="G2188_C42684"  placeholder="CEDULA TITULAR (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2188_C42692" id="LblG2188_C42692">FECHA DE EXPEDICION (JURIDICO)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2188_C42692'])) {
                            echo $_GET['G2188_C42692'];
                        } ?>"  name="G2188_C42692" id="G2188_C42692" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2188_C42693" id="LblG2188_C42693">FECHA DE NACIMIENTO (JURIDICO)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2188_C42693'])) {
                            echo $_GET['G2188_C42693'];
                        } ?>"  name="G2188_C42693" id="G2188_C42693" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42694" id="LblG2188_C42694">CORREO (JURIDICO)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42694" value="<?php if (isset($_GET['G2188_C42694'])) {
                            echo $_GET['G2188_C42694'];
                        } ?>"  name="G2188_C42694"  placeholder="CORREO (JURIDICO)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42695" id="LblG2188_C42695">SCORE (JURIDICO) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42695" id="G2188_C42695">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2341 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6147" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6147c">
                PERSONA NATURAL
            </a>
        </h4>
        
    </div>
    <div id="s_6147c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42696" id="LblG2188_C42696">TITULAR LINEA (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42696" value="<?php if (isset($_GET['G2188_C42696'])) {
                            echo $_GET['G2188_C42696'];
                        } ?>"  name="G2188_C42696"  placeholder="TITULAR LINEA (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42697" id="LblG2188_C42697">CEDULA TITULAR (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42697" value="<?php if (isset($_GET['G2188_C42697'])) {
                            echo $_GET['G2188_C42697'];
                        } ?>"  name="G2188_C42697"  placeholder="CEDULA TITULAR (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2188_C42698" id="LblG2188_C42698">FECHA DE EXPEDICION (NATURAL)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2188_C42698'])) {
                            echo $_GET['G2188_C42698'];
                        } ?>"  name="G2188_C42698" id="G2188_C42698" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2188_C42699" id="LblG2188_C42699">FECHA DE NACIMIENTO (NATURAL)</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2188_C42699'])) {
                            echo $_GET['G2188_C42699'];
                        } ?>"  name="G2188_C42699" id="G2188_C42699" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42700" id="LblG2188_C42700">CORREO (NATURAL)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42700" value="<?php if (isset($_GET['G2188_C42700'])) {
                            echo $_GET['G2188_C42700'];
                        } ?>"  name="G2188_C42700"  placeholder="CORREO (NATURAL)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42701" id="LblG2188_C42701">SCORE (NATURAL) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42701" id="G2188_C42701">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2341 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="6148" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42702" id="LblG2188_C42702">CANTIDAD DE LINEAS</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42702" value="<?php if (isset($_GET['G2188_C42702'])) {
                            echo $_GET['G2188_C42702'];
                        } ?>"  name="G2188_C42702"  placeholder="CANTIDAD DE LINEAS">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2188_C42703" id="LblG2188_C42703">FECHA DE CORTE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2188_C42703'])) {
                            echo $_GET['G2188_C42703'];
                        } ?>"  name="G2188_C42703" id="G2188_C42703" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2188_C42704" id="LblG2188_C42704">FECHA DE ENTREGA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2188_C42704'])) {
                            echo $_GET['G2188_C42704'];
                        } ?>"  name="G2188_C42704" id="G2188_C42704" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C45113" id="LblG2188_C45113">FRANJA HORARIA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C45113" id="G2188_C45113">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2552 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


</div>

<div  class="panel box box-primary" id="6149" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6149c">
                LINEA 1
            </a>
        </h4>
        
    </div>
    <div id="s_6149c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42705" id="LblG2188_C42705">OPERADOR ACTUAL (LINEA 1) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42705" id="G2188_C42705">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2342 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42706" id="LblG2188_C42706">PLAN (LINEA 1) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42706" id="G2188_C42706">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2343 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42707" id="LblG2188_C42707">NIP (LINEA 1)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42707" value="<?php if (isset($_GET['G2188_C42707'])) {
                            echo $_GET['G2188_C42707'];
                        } ?>"  name="G2188_C42707"  placeholder="NIP (LINEA 1)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42708" id="LblG2188_C42708">VALOR PLAN (LINEA 1) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42708" id="G2188_C42708">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2344 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6150" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6150c">
                LINEA 2
            </a>
        </h4>
        
    </div>
    <div id="s_6150c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42709" id="LblG2188_C42709">OPERADOR ACTUAL (LINEA 2) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42709" id="G2188_C42709">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2342 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42710" id="LblG2188_C42710">PLAN (LINEA 2) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42710" id="G2188_C42710">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2343 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42711" id="LblG2188_C42711">NIP (LINEA 2)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42711" value="<?php if (isset($_GET['G2188_C42711'])) {
                            echo $_GET['G2188_C42711'];
                        } ?>"  name="G2188_C42711"  placeholder="NIP (LINEA 2)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42712" id="LblG2188_C42712">VALOR DEL PLAN (LINEA 2) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42712" id="G2188_C42712">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2345 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6151" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6151c">
                LINEA 3
            </a>
        </h4>
        
    </div>
    <div id="s_6151c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42713" id="LblG2188_C42713">OPERADOR ACTUAL (LINEA 3) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42713" id="G2188_C42713">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2342 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42714" id="LblG2188_C42714">PLAN (LINEA 3) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42714" id="G2188_C42714">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2343 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42715" id="LblG2188_C42715">NIP (LINEA 3)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42715" value="<?php if (isset($_GET['G2188_C42715'])) {
                            echo $_GET['G2188_C42715'];
                        } ?>"  name="G2188_C42715"  placeholder="NIP (LINEA 3)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42716" id="LblG2188_C42716">VALOR DEL PLAN (LINEA 3) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42716" id="G2188_C42716">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2346 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6152" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6152c">
                LINEA 4
            </a>
        </h4>
        
    </div>
    <div id="s_6152c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42717" id="LblG2188_C42717">OPERADOR ACTUAL (LINEA 4) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42717" id="G2188_C42717">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2342 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42718" id="LblG2188_C42718">PLAN (LINEA 4) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42718" id="G2188_C42718">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2343 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42719" id="LblG2188_C42719">NIP (LINEA 4)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42719" value="<?php if (isset($_GET['G2188_C42719'])) {
                            echo $_GET['G2188_C42719'];
                        } ?>"  name="G2188_C42719"  placeholder="NIP (LINEA 4)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42720" id="LblG2188_C42720">VALOR DEL PLAN (LINEA 4) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42720" id="G2188_C42720">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2347 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6153" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6153c">
                LINEA 5
            </a>
        </h4>
        
    </div>
    <div id="s_6153c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42721" id="LblG2188_C42721">OPERADOR ACTUAL (LINEA 5) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42721" id="G2188_C42721">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2342 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42722" id="LblG2188_C42722">PLAN (LINEA 5) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42722" id="G2188_C42722">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2343 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42723" id="LblG2188_C42723">NIP (LINEA 5)</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42723" value="<?php if (isset($_GET['G2188_C42723'])) {
                            echo $_GET['G2188_C42723'];
                        } ?>"  name="G2188_C42723"  placeholder="NIP (LINEA 5)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-3 col-xs-3">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42724" id="LblG2188_C42724">VALOR DEL PLAN (LINEA 5) </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42724" id="G2188_C42724">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2348 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6154" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6154c">
                DATOS DE DIRECCION
            </a>
        </h4>
        
    </div>
    <div id="s_6154c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42725" id="LblG2188_C42725">PERSONA DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42725" value="<?php if (isset($_GET['G2188_C42725'])) {
                            echo $_GET['G2188_C42725'];
                        } ?>"  name="G2188_C42725"  placeholder="PERSONA DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42740" id="LblG2188_C42740">NUMERO DE CONTACTO</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42740" value="<?php if (isset($_GET['G2188_C42740'])) {
                            echo $_GET['G2188_C42740'];
                        } ?>"  name="G2188_C42740"  placeholder="NUMERO DE CONTACTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42726" id="LblG2188_C42726">TIPO DE VIA 1 </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42726" id="G2188_C42726">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2349 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42727" id="LblG2188_C42727">NUMERO DE VIA 1</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42727" value="<?php if (isset($_GET['G2188_C42727'])) {
                            echo $_GET['G2188_C42727'];
                        } ?>"  name="G2188_C42727"  placeholder="NUMERO DE VIA 1">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42728" id="LblG2188_C42728">LITERAL 1</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42728" value="<?php if (isset($_GET['G2188_C42728'])) {
                            echo $_GET['G2188_C42728'];
                        } ?>"  name="G2188_C42728"  placeholder="LITERAL 1">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42729" id="LblG2188_C42729">TIPO DE VIA 2 </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42729" id="G2188_C42729">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2349 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42730" id="LblG2188_C42730">NUMERO DE VIA 2</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42730" value="<?php if (isset($_GET['G2188_C42730'])) {
                            echo $_GET['G2188_C42730'];
                        } ?>"  name="G2188_C42730"  placeholder="NUMERO DE VIA 2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42731" id="LblG2188_C42731">LITERAL 2</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42731" value="<?php if (isset($_GET['G2188_C42731'])) {
                            echo $_GET['G2188_C42731'];
                        } ?>"  name="G2188_C42731"  placeholder="LITERAL 2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42732" id="LblG2188_C42732">ORIENTACION </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42732" id="G2188_C42732">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2350 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42733" id="LblG2188_C42733">NUMERO DE VIA 3</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42733" value="<?php if (isset($_GET['G2188_C42733'])) {
                            echo $_GET['G2188_C42733'];
                        } ?>"  name="G2188_C42733"  placeholder="NUMERO DE VIA 3">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42734" id="LblG2188_C42734">ORIENTACION 2 </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42734" id="G2188_C42734">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2350 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42735" id="LblG2188_C42735">FRANJA HORARIA</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42735" value="<?php if (isset($_GET['G2188_C42735'])) {
                            echo $_GET['G2188_C42735'];
                        } ?>"  name="G2188_C42735"  placeholder="FRANJA HORARIA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42736" id="LblG2188_C42736">CIUDAD</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42736" id="G2188_C42736">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2339 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42737" id="LblG2188_C42737">DEPARTAMENTO </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42737" id="G2188_C42737">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2351 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2188_C42738" id="LblG2188_C42738">RECAUDO CLIENTE</label>
                        <input type="text" class="form-control input-sm" id="G2188_C42738" value="<?php if (isset($_GET['G2188_C42738'])) {
                            echo $_GET['G2188_C42738'];
                        } ?>"  name="G2188_C42738"  placeholder="RECAUDO CLIENTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2188_C42739" id="LblG2188_C42739">OBSERVACION DIRECCION</label>
                        <textarea class="form-control input-sm" name="G2188_C42739" id="G2188_C42739"  value="<?php if (isset($_GET['G2188_C42739'])) {
                            echo $_GET['G2188_C42739'];
                        } ?>" placeholder="OBSERVACION DIRECCION"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="6155" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6155c">
                PREVALIDACION
            </a>
        </h4>
        
    </div>
    <div id="s_6155c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Señor |Nombre del cliente| solicito me confirme con un si o un no a las siguientes preguntas de validacion de acuerdo :</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42742" id="LblG2188_C42742">apartir de este momento usted a contratado un PLAN DE VOZ Y NAVEGACION con TIGO DE ACUERDO? </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42742" id="G2188_C42742">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2352 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42743" id="LblG2188_C42743">AUTORIZA A TIGO, A REALIZAR UNA PORTABILIDAD NUMERICA DE SU ACTUAL OPERADOR A TIGO? </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42743" id="G2188_C42743">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2352 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42744" id="LblG2188_C42744">ES CONSCIENTE QUE AL FINALIZAR SU PORTABILIDAD, CONTAMOS CON 24 HORAS HÁBILES PARA HACER ENTREGA DE SU SERVICIO? </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42744" id="G2188_C42744">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2352 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2188_C42745" id="LblG2188_C42745">(HABEAS DATA) AUTORIZACIÓN DEL TRATO DE LOS DATOS LEY 1581 DE 2012 Señor(a) XXX, para poderte brindar la asesoría requerimos tu autorización para registrar tus datos en nuestros sistemas de información y manejarlos de acuerdo a lo establecido en la ley de datos personales, así mismo, para intercambiar tus datos con otros proveedores de servicios de comunicaciones para efectos de prevención y control del fraude y el cumplimiento de normas que así lo exijan, de acuerdo a la ley 1581 del año 2012, ley de habeas data, protección de datos personales. ¿De igual manera me autoriza a consultar sus centrales de riesgo? </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2188_C42745" id="G2188_C42745">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2352 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Recuerde que de parte de nuestra mensajería, TIGO se acercara a la dirección acordada o al punto donde se encuentra y hará entrega de su CHIP O SIMCARD</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Recuerde que a la hora de hacer entrega de su plan debe tener a la mano el valor acordado QUE ES DE 15.000y una fotocopia de su RUT Y CEDULA ampliada al 150%</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">!!BIENVENIDO A LA FAMILIA TIGO Y GRACIAS POR ATENDER MI LLAMADA!!</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2188_C42674">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2358;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2188_C42674">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2358;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2188_C42675">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2188_C42676" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2188_C42677" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2188_C42678" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2188/G2188_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2188_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2188_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2188_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>; 
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2188_C42685").val(item.G2188_C42685); 
                $("#G2188_C42686").val(item.G2188_C42686); 
                $("#G2188_C42687").val(item.G2188_C42687); 
                $("#G2188_C42688").val(item.G2188_C42688).trigger("change");  
                $("#G2188_C42689").val(item.G2188_C42689); 
                $("#G2188_C42690").val(item.G2188_C42690); 
                $("#G2188_C42691").val(item.G2188_C42691).trigger("change");  
                $("#G2188_C42674").val(item.G2188_C42674).trigger("change");  
                $("#G2188_C42675").val(item.G2188_C42675).trigger("change");  
                $("#G2188_C42676").val(item.G2188_C42676); 
                $("#G2188_C42677").val(item.G2188_C42677); 
                $("#G2188_C42678").val(item.G2188_C42678); 
                $("#G2188_C42679").val(item.G2188_C42679); 
                $("#G2188_C42680").val(item.G2188_C42680); 
                $("#G2188_C42681").val(item.G2188_C42681); 
                $("#G2188_C42682").val(item.G2188_C42682); 
                $("#G2188_C42683").val(item.G2188_C42683); 
                $("#G2188_C42684").val(item.G2188_C42684); 
                $("#G2188_C42692").val(item.G2188_C42692); 
                $("#G2188_C42693").val(item.G2188_C42693); 
                $("#G2188_C42694").val(item.G2188_C42694); 
                $("#G2188_C42695").val(item.G2188_C42695).trigger("change");  
                $("#G2188_C42696").val(item.G2188_C42696); 
                $("#G2188_C42697").val(item.G2188_C42697); 
                $("#G2188_C42698").val(item.G2188_C42698); 
                $("#G2188_C42699").val(item.G2188_C42699); 
                $("#G2188_C42700").val(item.G2188_C42700); 
                $("#G2188_C42701").val(item.G2188_C42701).trigger("change");  
                $("#G2188_C42702").val(item.G2188_C42702); 
                $("#G2188_C42703").val(item.G2188_C42703); 
                $("#G2188_C42704").val(item.G2188_C42704); 
                $("#G2188_C45113").val(item.G2188_C45113).trigger("change");  
                $("#G2188_C42705").val(item.G2188_C42705).trigger("change");  
                $("#G2188_C42706").val(item.G2188_C42706).trigger("change");  
                $("#G2188_C42707").val(item.G2188_C42707); 
                $("#G2188_C42708").val(item.G2188_C42708).trigger("change");  
                $("#G2188_C42709").val(item.G2188_C42709).trigger("change");  
                $("#G2188_C42710").val(item.G2188_C42710).trigger("change");  
                $("#G2188_C42711").val(item.G2188_C42711); 
                $("#G2188_C42712").val(item.G2188_C42712).trigger("change");  
                $("#G2188_C42713").val(item.G2188_C42713).trigger("change");  
                $("#G2188_C42714").val(item.G2188_C42714).trigger("change");  
                $("#G2188_C42715").val(item.G2188_C42715); 
                $("#G2188_C42716").val(item.G2188_C42716).trigger("change");  
                $("#G2188_C42717").val(item.G2188_C42717).trigger("change");  
                $("#G2188_C42718").val(item.G2188_C42718).trigger("change");  
                $("#G2188_C42719").val(item.G2188_C42719); 
                $("#G2188_C42720").val(item.G2188_C42720).trigger("change");  
                $("#G2188_C42721").val(item.G2188_C42721).trigger("change");  
                $("#G2188_C42722").val(item.G2188_C42722).trigger("change");  
                $("#G2188_C42723").val(item.G2188_C42723); 
                $("#G2188_C42724").val(item.G2188_C42724).trigger("change");  
                $("#G2188_C42725").val(item.G2188_C42725); 
                $("#G2188_C42740").val(item.G2188_C42740); 
                $("#G2188_C42726").val(item.G2188_C42726).trigger("change");  
                $("#G2188_C42727").val(item.G2188_C42727); 
                $("#G2188_C42728").val(item.G2188_C42728); 
                $("#G2188_C42729").val(item.G2188_C42729).trigger("change");  
                $("#G2188_C42730").val(item.G2188_C42730); 
                $("#G2188_C42731").val(item.G2188_C42731); 
                $("#G2188_C42732").val(item.G2188_C42732).trigger("change");  
                $("#G2188_C42733").val(item.G2188_C42733); 
                $("#G2188_C42734").val(item.G2188_C42734).trigger("change"); 
                $("#G2188_C42736").val(item.G2188_C42736).trigger("change");  
                $("#G2188_C42737").val(item.G2188_C42737).trigger("change");  
                $("#G2188_C42738").val(item.G2188_C42738); 
                $("#G2188_C42739").val(item.G2188_C42739);   
                if(item.G2188_C42741 == 1){
                    $("#G2188_C42741").attr('checked', true);
                }  
                $("#G2188_C42742").val(item.G2188_C42742).trigger("change");  
                $("#G2188_C42743").val(item.G2188_C42743).trigger("change");  
                $("#G2188_C42744").val(item.G2188_C42744).trigger("change");  
                $("#G2188_C42745").val(item.G2188_C42745).trigger("change");    
                if(item.G2188_C42746 == 1){
                    $("#G2188_C42746").attr('checked', true);
                }    
                if(item.G2188_C42747 == 1){
                    $("#G2188_C42747").attr('checked', true);
                }    
                if(item.G2188_C42748 == 1){
                    $("#G2188_C42748").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2188_C42688").select2();

    $("#G2188_C42691").select2();

    $("#G2188_C42695").select2();

    $("#G2188_C42701").select2();

    $("#G2188_C45113").select2();

    $("#G2188_C42705").select2();

    $("#G2188_C42706").select2();

    $("#G2188_C42708").select2();

    $("#G2188_C42709").select2();

    $("#G2188_C42710").select2();

    $("#G2188_C42712").select2();

    $("#G2188_C42713").select2();

    $("#G2188_C42714").select2();

    $("#G2188_C42716").select2();

    $("#G2188_C42717").select2();

    $("#G2188_C42718").select2();

    $("#G2188_C42720").select2();

    $("#G2188_C42721").select2();

    $("#G2188_C42722").select2();

    $("#G2188_C42724").select2();

    $("#G2188_C42726").select2();

    $("#G2188_C42729").select2();

    $("#G2188_C42732").select2();

    $("#G2188_C42734").select2();

    $("#G2188_C42736").select2();

    $("#G2188_C42737").select2();

    $("#G2188_C42742").select2();

    $("#G2188_C42743").select2();

    $("#G2188_C42744").select2();

    $("#G2188_C42745").select2();
        //datepickers
        

        $("#G2188_C42676").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2188_C42692").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2188_C42693").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2188_C42698").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2188_C42699").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2188_C42703").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2188_C42704").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2188_C42677").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para CIUDAD 1  

    $("#G2188_C42688").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CLIENTE 

    $("#G2188_C42691").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SCORE (JURIDICO)  

    $("#G2188_C42695").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SCORE (NATURAL)  

    $("#G2188_C42701").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para FRANJA HORARIA 

    $("#G2188_C45113").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 1)  

    $("#G2188_C42705").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 1)  

    $("#G2188_C42706").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR PLAN (LINEA 1)  

    $("#G2188_C42708").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 2)  

    $("#G2188_C42709").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 2)  

    $("#G2188_C42710").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 2)  

    $("#G2188_C42712").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 3)  

    $("#G2188_C42713").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 3)  

    $("#G2188_C42714").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 3)  

    $("#G2188_C42716").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 4)  

    $("#G2188_C42717").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 4)  

    $("#G2188_C42718").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 4)  

    $("#G2188_C42720").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 5)  

    $("#G2188_C42721").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 5)  

    $("#G2188_C42722").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 5)  

    $("#G2188_C42724").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE VIA 1  

    $("#G2188_C42726").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE VIA 2  

    $("#G2188_C42729").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ORIENTACION  

    $("#G2188_C42732").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ORIENTACION 2  

    $("#G2188_C42734").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para CIUDAD 

    $("#G2188_C42736").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DEPARTAMENTO  

    $("#G2188_C42737").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para apartir de este momento usted a contratado un PLAN DE VOZ Y NAVEGACION con TIGO DE ACUERDO?  

    $("#G2188_C42742").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para AUTORIZA A TIGO, A REALIZAR UNA PORTABILIDAD NUMERICA DE SU ACTUAL OPERADOR A TIGO?  

    $("#G2188_C42743").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ES CONSCIENTE QUE AL FINALIZAR SU PORTABILIDAD, CONTAMOS CON 24 HORAS HÁBILES PARA HACER ENTREGA DE SU SERVICIO?  

    $("#G2188_C42744").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para (HABEAS DATA) AUTORIZACIÓN DEL TRATO DE LOS DATOS LEY 1581 DE 2012 Señor(a) XXX, para poderte brindar la asesoría requerimos tu autorización para registrar tus datos en nuestros sistemas de información y manejarlos de acuerdo a lo establecido en la ley de datos personales, así mismo, para intercambiar tus datos con otros proveedores de servicios de comunicaciones para efectos de prevención y control del fraude y el cumplimiento de normas que así lo exijan, de acuerdo a la ley 1581 del año 2012, ley de habeas data, protección de datos personales. ¿De igual manera me autoriza a consultar sus centrales de riesgo?  

    $("#G2188_C42745").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2188_C42685").val(item.G2188_C42685);
 
                                                $("#G2188_C42686").val(item.G2188_C42686);
 
                                                $("#G2188_C42687").val(item.G2188_C42687);
 
                    $("#G2188_C42688").val(item.G2188_C42688).trigger("change"); 
 
                                                $("#G2188_C42689").val(item.G2188_C42689);
 
                                                $("#G2188_C42690").val(item.G2188_C42690);
 
                    $("#G2188_C42691").val(item.G2188_C42691).trigger("change"); 
 
                    $("#G2188_C42674").val(item.G2188_C42674).trigger("change"); 
 
                    $("#G2188_C42675").val(item.G2188_C42675).trigger("change"); 
 
                                                $("#G2188_C42676").val(item.G2188_C42676);
 
                                                $("#G2188_C42677").val(item.G2188_C42677);
 
                                                $("#G2188_C42678").val(item.G2188_C42678);
 
                                                $("#G2188_C42679").val(item.G2188_C42679);
 
                                                $("#G2188_C42680").val(item.G2188_C42680);
 
                                                $("#G2188_C42681").val(item.G2188_C42681);
 
                                                $("#G2188_C42682").val(item.G2188_C42682);
 
                                                $("#G2188_C42683").val(item.G2188_C42683);
 
                                                $("#G2188_C42684").val(item.G2188_C42684);
 
                                                $("#G2188_C42692").val(item.G2188_C42692);
 
                                                $("#G2188_C42693").val(item.G2188_C42693);
 
                                                $("#G2188_C42694").val(item.G2188_C42694);
 
                    $("#G2188_C42695").val(item.G2188_C42695).trigger("change"); 
 
                                                $("#G2188_C42696").val(item.G2188_C42696);
 
                                                $("#G2188_C42697").val(item.G2188_C42697);
 
                                                $("#G2188_C42698").val(item.G2188_C42698);
 
                                                $("#G2188_C42699").val(item.G2188_C42699);
 
                                                $("#G2188_C42700").val(item.G2188_C42700);
 
                    $("#G2188_C42701").val(item.G2188_C42701).trigger("change"); 
 
                                                $("#G2188_C42702").val(item.G2188_C42702);
 
                                                $("#G2188_C42703").val(item.G2188_C42703);
 
                                                $("#G2188_C42704").val(item.G2188_C42704);
 
                    $("#G2188_C45113").val(item.G2188_C45113).trigger("change"); 
 
                    $("#G2188_C42705").val(item.G2188_C42705).trigger("change"); 
 
                    $("#G2188_C42706").val(item.G2188_C42706).trigger("change"); 
 
                                                $("#G2188_C42707").val(item.G2188_C42707);
 
                    $("#G2188_C42708").val(item.G2188_C42708).trigger("change"); 
 
                    $("#G2188_C42709").val(item.G2188_C42709).trigger("change"); 
 
                    $("#G2188_C42710").val(item.G2188_C42710).trigger("change"); 
 
                                                $("#G2188_C42711").val(item.G2188_C42711);
 
                    $("#G2188_C42712").val(item.G2188_C42712).trigger("change"); 
 
                    $("#G2188_C42713").val(item.G2188_C42713).trigger("change"); 
 
                    $("#G2188_C42714").val(item.G2188_C42714).trigger("change"); 
 
                                                $("#G2188_C42715").val(item.G2188_C42715);
 
                    $("#G2188_C42716").val(item.G2188_C42716).trigger("change"); 
 
                    $("#G2188_C42717").val(item.G2188_C42717).trigger("change"); 
 
                    $("#G2188_C42718").val(item.G2188_C42718).trigger("change"); 
 
                                                $("#G2188_C42719").val(item.G2188_C42719);
 
                    $("#G2188_C42720").val(item.G2188_C42720).trigger("change"); 
 
                    $("#G2188_C42721").val(item.G2188_C42721).trigger("change"); 
 
                    $("#G2188_C42722").val(item.G2188_C42722).trigger("change"); 
 
                                                $("#G2188_C42723").val(item.G2188_C42723);
 
                    $("#G2188_C42724").val(item.G2188_C42724).trigger("change"); 
 
                                                $("#G2188_C42725").val(item.G2188_C42725);
 
                                                $("#G2188_C42740").val(item.G2188_C42740);
 
                    $("#G2188_C42726").val(item.G2188_C42726).trigger("change"); 
 
                                                $("#G2188_C42727").val(item.G2188_C42727);
 
                                                $("#G2188_C42728").val(item.G2188_C42728);
 
                    $("#G2188_C42729").val(item.G2188_C42729).trigger("change"); 
 
                                                $("#G2188_C42730").val(item.G2188_C42730);
 
                                                $("#G2188_C42731").val(item.G2188_C42731);
 
                    $("#G2188_C42732").val(item.G2188_C42732).trigger("change"); 
 
                                                $("#G2188_C42733").val(item.G2188_C42733);
 
                    $("#G2188_C42734").val(item.G2188_C42734).trigger("change"); 
 
 
                    $("#G2188_C42736").val(item.G2188_C42736).trigger("change"); 
 
                    $("#G2188_C42737").val(item.G2188_C42737).trigger("change"); 
 
                                                $("#G2188_C42738").val(item.G2188_C42738);
 
                                                $("#G2188_C42739").val(item.G2188_C42739);
      
                                                if(item.G2188_C42741 == 1){
                                                   $("#G2188_C42741").attr('checked', true);
                                                } 
 
                    $("#G2188_C42742").val(item.G2188_C42742).trigger("change"); 
 
                    $("#G2188_C42743").val(item.G2188_C42743).trigger("change"); 
 
                    $("#G2188_C42744").val(item.G2188_C42744).trigger("change"); 
 
                    $("#G2188_C42745").val(item.G2188_C42745).trigger("change"); 
      
                                                if(item.G2188_C42746 == 1){
                                                   $("#G2188_C42746").attr('checked', true);
                                                } 
      
                                                if(item.G2188_C42747 == 1){
                                                   $("#G2188_C42747").attr('checked', true);
                                                } 
      
                                                if(item.G2188_C42748 == 1){
                                                   $("#G2188_C42748").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','NOMBRE','CEDULA','CELULAR','CIUDAD 1 ','FIJO','DIRECCION','TIPO DE CLIENTE','Agente','Fecha','Hora','Campaña','TITULAR LINEA (JURIDICO)','CEDULA TITULAR (JURIDICO)','FECHA DE EXPEDICION (JURIDICO)','FECHA DE NACIMIENTO (JURIDICO)','CORREO (JURIDICO)','SCORE (JURIDICO) ','TITULAR LINEA (NATURAL)','CEDULA TITULAR (NATURAL)','FECHA DE EXPEDICION (NATURAL)','FECHA DE NACIMIENTO (NATURAL)','CORREO (NATURAL)','SCORE (NATURAL) ','CANTIDAD DE LINEAS','FECHA DE CORTE','FECHA DE ENTREGA','FRANJA HORARIA','OPERADOR ACTUAL (LINEA 1) ','PLAN (LINEA 1) ','NIP (LINEA 1)','VALOR PLAN (LINEA 1) ','OPERADOR ACTUAL (LINEA 2) ','PLAN (LINEA 2) ','NIP (LINEA 2)','VALOR DEL PLAN (LINEA 2) ','OPERADOR ACTUAL (LINEA 3) ','PLAN (LINEA 3) ','NIP (LINEA 3)','VALOR DEL PLAN (LINEA 3) ','OPERADOR ACTUAL (LINEA 4) ','PLAN (LINEA 4) ','NIP (LINEA 4)','VALOR DEL PLAN (LINEA 4) ','OPERADOR ACTUAL (LINEA 5) ','PLAN (LINEA 5) ','NIP (LINEA 5)','VALOR DEL PLAN (LINEA 5) ','PERSONA DE CONTACTO','NUMERO DE CONTACTO','TIPO DE VIA 1 ','NUMERO DE VIA 1','LITERAL 1','TIPO DE VIA 2 ','NUMERO DE VIA 2','LITERAL 2','ORIENTACION ','NUMERO DE VIA 3','ORIENTACION 2 ','FRANJA HORARIA','CIUDAD','DEPARTAMENTO ','RECAUDO CLIENTE','OBSERVACION DIRECCION','apartir de este momento usted a contratado un PLAN DE VOZ Y NAVEGACION con TIGO DE ACUERDO? ','AUTORIZA A TIGO, A REALIZAR UNA PORTABILIDAD NUMERICA DE SU ACTUAL OPERADOR A TIGO? ','ES CONSCIENTE QUE AL FINALIZAR SU PORTABILIDAD, CONTAMOS CON 24 HORAS HÁBILES PARA HACER ENTREGA DE SU SERVICIO? ','(HABEAS DATA) AUTORIZACIÓN DEL TRATO DE LOS DATOS LEY 1581 DE 2012 Señor(a) XXX, para poderte brindar la asesoría requerimos tu autorización para registrar tus datos en nuestros sistemas de información y manejarlos de acuerdo a lo establecido en la ley de datos personales, así mismo, para intercambiar tus datos con otros proveedores de servicios de comunicaciones para efectos de prevención y control del fraude y el cumplimiento de normas que así lo exijan, de acuerdo a la ley 1581 del año 2012, ley de habeas data, protección de datos personales. ¿De igual manera me autoriza a consultar sus centrales de riesgo? '],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2188_C42685', 
                        index: 'G2188_C42685', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42686', 
                        index: 'G2188_C42686', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42687', 
                        index: 'G2188_C42687', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42688', 
                        index:'G2188_C42688', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2339&campo=G2188_C42688'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42689', 
                        index: 'G2188_C42689', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42690', 
                        index: 'G2188_C42690', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42691', 
                        index:'G2188_C42691', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2340&campo=G2188_C42691'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42679', 
                        index: 'G2188_C42679', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42680', 
                        index: 'G2188_C42680', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42681', 
                        index: 'G2188_C42681', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42682', 
                        index: 'G2188_C42682', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42683', 
                        index: 'G2188_C42683', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42684', 
                        index: 'G2188_C42684', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2188_C42692', 
                        index:'G2188_C42692', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2188_C42693', 
                        index:'G2188_C42693', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42694', 
                        index: 'G2188_C42694', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42695', 
                        index:'G2188_C42695', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2341&campo=G2188_C42695'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42696', 
                        index: 'G2188_C42696', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42697', 
                        index: 'G2188_C42697', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2188_C42698', 
                        index:'G2188_C42698', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2188_C42699', 
                        index:'G2188_C42699', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42700', 
                        index: 'G2188_C42700', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42701', 
                        index:'G2188_C42701', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2341&campo=G2188_C42701'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42702', 
                        index: 'G2188_C42702', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2188_C42703', 
                        index:'G2188_C42703', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2188_C42704', 
                        index:'G2188_C42704', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2188_C45113', 
                        index:'G2188_C45113', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2552&campo=G2188_C45113'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42705', 
                        index:'G2188_C42705', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2342&campo=G2188_C42705'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42706', 
                        index:'G2188_C42706', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2343&campo=G2188_C42706'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42707', 
                        index: 'G2188_C42707', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42708', 
                        index:'G2188_C42708', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2344&campo=G2188_C42708'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42709', 
                        index:'G2188_C42709', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2342&campo=G2188_C42709'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42710', 
                        index:'G2188_C42710', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2343&campo=G2188_C42710'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42711', 
                        index: 'G2188_C42711', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42712', 
                        index:'G2188_C42712', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2345&campo=G2188_C42712'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42713', 
                        index:'G2188_C42713', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2342&campo=G2188_C42713'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42714', 
                        index:'G2188_C42714', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2343&campo=G2188_C42714'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42715', 
                        index: 'G2188_C42715', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42716', 
                        index:'G2188_C42716', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2346&campo=G2188_C42716'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42717', 
                        index:'G2188_C42717', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2342&campo=G2188_C42717'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42718', 
                        index:'G2188_C42718', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2343&campo=G2188_C42718'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42719', 
                        index: 'G2188_C42719', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42720', 
                        index:'G2188_C42720', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2347&campo=G2188_C42720'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42721', 
                        index:'G2188_C42721', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2342&campo=G2188_C42721'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42722', 
                        index:'G2188_C42722', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2343&campo=G2188_C42722'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42723', 
                        index: 'G2188_C42723', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42724', 
                        index:'G2188_C42724', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2348&campo=G2188_C42724'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42725', 
                        index: 'G2188_C42725', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42740', 
                        index: 'G2188_C42740', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42726', 
                        index:'G2188_C42726', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2349&campo=G2188_C42726'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42727', 
                        index: 'G2188_C42727', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42728', 
                        index: 'G2188_C42728', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42729', 
                        index:'G2188_C42729', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2349&campo=G2188_C42729'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42730', 
                        index: 'G2188_C42730', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42731', 
                        index: 'G2188_C42731', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42732', 
                        index:'G2188_C42732', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2350&campo=G2188_C42732'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42733', 
                        index: 'G2188_C42733', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42734', 
                        index:'G2188_C42734', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2350&campo=G2188_C42734'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42735', 
                        index: 'G2188_C42735', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42736', 
                        index:'G2188_C42736', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2339&campo=G2188_C42736'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42737', 
                        index:'G2188_C42737', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2351&campo=G2188_C42737'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42738', 
                        index: 'G2188_C42738', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42739', 
                        index:'G2188_C42739', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2188_C42742', 
                        index:'G2188_C42742', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2352&campo=G2188_C42742'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42743', 
                        index:'G2188_C42743', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2352&campo=G2188_C42743'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42744', 
                        index:'G2188_C42744', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2352&campo=G2188_C42744'
                        }
                    }

                    ,
                    { 
                        name:'G2188_C42745', 
                        index:'G2188_C42745', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2352&campo=G2188_C42745'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2188_C42685',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2188_C42685").val(item.G2188_C42685);

                        $("#G2188_C42686").val(item.G2188_C42686);

                        $("#G2188_C42687").val(item.G2188_C42687);
 
                    $("#G2188_C42688").val(item.G2188_C42688).trigger("change"); 

                        $("#G2188_C42689").val(item.G2188_C42689);

                        $("#G2188_C42690").val(item.G2188_C42690);
 
                    $("#G2188_C42691").val(item.G2188_C42691).trigger("change"); 
 
                    $("#G2188_C42674").val(item.G2188_C42674).trigger("change"); 
 
                    $("#G2188_C42675").val(item.G2188_C42675).trigger("change"); 

                        $("#G2188_C42676").val(item.G2188_C42676);

                        $("#G2188_C42677").val(item.G2188_C42677);

                        $("#G2188_C42678").val(item.G2188_C42678);

                        $("#G2188_C42679").val(item.G2188_C42679);

                        $("#G2188_C42680").val(item.G2188_C42680);

                        $("#G2188_C42681").val(item.G2188_C42681);

                        $("#G2188_C42682").val(item.G2188_C42682);

                        $("#G2188_C42683").val(item.G2188_C42683);

                        $("#G2188_C42684").val(item.G2188_C42684);

                        $("#G2188_C42692").val(item.G2188_C42692);

                        $("#G2188_C42693").val(item.G2188_C42693);

                        $("#G2188_C42694").val(item.G2188_C42694);
 
                    $("#G2188_C42695").val(item.G2188_C42695).trigger("change"); 

                        $("#G2188_C42696").val(item.G2188_C42696);

                        $("#G2188_C42697").val(item.G2188_C42697);

                        $("#G2188_C42698").val(item.G2188_C42698);

                        $("#G2188_C42699").val(item.G2188_C42699);

                        $("#G2188_C42700").val(item.G2188_C42700);
 
                    $("#G2188_C42701").val(item.G2188_C42701).trigger("change"); 

                        $("#G2188_C42702").val(item.G2188_C42702);

                        $("#G2188_C42703").val(item.G2188_C42703);

                        $("#G2188_C42704").val(item.G2188_C42704);
 
                    $("#G2188_C45113").val(item.G2188_C45113).trigger("change"); 
 
                    $("#G2188_C42705").val(item.G2188_C42705).trigger("change"); 
 
                    $("#G2188_C42706").val(item.G2188_C42706).trigger("change"); 

                        $("#G2188_C42707").val(item.G2188_C42707);
 
                    $("#G2188_C42708").val(item.G2188_C42708).trigger("change"); 
 
                    $("#G2188_C42709").val(item.G2188_C42709).trigger("change"); 
 
                    $("#G2188_C42710").val(item.G2188_C42710).trigger("change"); 

                        $("#G2188_C42711").val(item.G2188_C42711);
 
                    $("#G2188_C42712").val(item.G2188_C42712).trigger("change"); 
 
                    $("#G2188_C42713").val(item.G2188_C42713).trigger("change"); 
 
                    $("#G2188_C42714").val(item.G2188_C42714).trigger("change"); 

                        $("#G2188_C42715").val(item.G2188_C42715);
 
                    $("#G2188_C42716").val(item.G2188_C42716).trigger("change"); 
 
                    $("#G2188_C42717").val(item.G2188_C42717).trigger("change"); 
 
                    $("#G2188_C42718").val(item.G2188_C42718).trigger("change"); 

                        $("#G2188_C42719").val(item.G2188_C42719);
 
                    $("#G2188_C42720").val(item.G2188_C42720).trigger("change"); 
 
                    $("#G2188_C42721").val(item.G2188_C42721).trigger("change"); 
 
                    $("#G2188_C42722").val(item.G2188_C42722).trigger("change"); 

                        $("#G2188_C42723").val(item.G2188_C42723);
 
                    $("#G2188_C42724").val(item.G2188_C42724).trigger("change"); 

                        $("#G2188_C42725").val(item.G2188_C42725);

                        $("#G2188_C42740").val(item.G2188_C42740);
 
                    $("#G2188_C42726").val(item.G2188_C42726).trigger("change"); 

                        $("#G2188_C42727").val(item.G2188_C42727);

                        $("#G2188_C42728").val(item.G2188_C42728);
 
                    $("#G2188_C42729").val(item.G2188_C42729).trigger("change"); 

                        $("#G2188_C42730").val(item.G2188_C42730);

                        $("#G2188_C42731").val(item.G2188_C42731);
 
                    $("#G2188_C42732").val(item.G2188_C42732).trigger("change"); 

                        $("#G2188_C42733").val(item.G2188_C42733);
 
                    $("#G2188_C42734").val(item.G2188_C42734).trigger("change"); 
 
                    $("#G2188_C42736").val(item.G2188_C42736).trigger("change"); 
 
                    $("#G2188_C42737").val(item.G2188_C42737).trigger("change"); 

                        $("#G2188_C42738").val(item.G2188_C42738);

                        $("#G2188_C42739").val(item.G2188_C42739);
    
                        if(item.G2188_C42741 == 1){
                           $("#G2188_C42741").attr('checked', true);
                        } 
 
                    $("#G2188_C42742").val(item.G2188_C42742).trigger("change"); 
 
                    $("#G2188_C42743").val(item.G2188_C42743).trigger("change"); 
 
                    $("#G2188_C42744").val(item.G2188_C42744).trigger("change"); 
 
                    $("#G2188_C42745").val(item.G2188_C42745).trigger("change"); 
    
                        if(item.G2188_C42746 == 1){
                           $("#G2188_C42746").attr('checked', true);
                        } 
    
                        if(item.G2188_C42747 == 1){
                           $("#G2188_C42747").attr('checked', true);
                        } 
    
                        if(item.G2188_C42748 == 1){
                           $("#G2188_C42748").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
