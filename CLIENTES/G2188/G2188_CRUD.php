<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2188_ConsInte__b, G2188_FechaInsercion , G2188_Usuario ,  G2188_CodigoMiembro  , G2188_PoblacionOrigen , G2188_EstadoDiligenciamiento ,  G2188_IdLlamada , G2188_C42685 as principal ,G2188_C42685,G2188_C42686,G2188_C42687,G2188_C42688,G2188_C42689,G2188_C42690,G2188_C42691,G2188_C42674,G2188_C42675,G2188_C42676,G2188_C42677,G2188_C42678,G2188_C42679,G2188_C42680,G2188_C42681,G2188_C42682,G2188_C42683,G2188_C42684,G2188_C42692,G2188_C42693,G2188_C42694,G2188_C42695,G2188_C42696,G2188_C42697,G2188_C42698,G2188_C42699,G2188_C42700,G2188_C42701,G2188_C42702,G2188_C42703,G2188_C42704,G2188_C45113,G2188_C42705,G2188_C42706,G2188_C42707,G2188_C42708,G2188_C42709,G2188_C42710,G2188_C42711,G2188_C42712,G2188_C42713,G2188_C42714,G2188_C42715,G2188_C42716,G2188_C42717,G2188_C42718,G2188_C42719,G2188_C42720,G2188_C42721,G2188_C42722,G2188_C42723,G2188_C42724,G2188_C42725,G2188_C42740,G2188_C42726,G2188_C42727,G2188_C42728,G2188_C42729,G2188_C42730,G2188_C42731,G2188_C42732,G2188_C42733,G2188_C42734,G2188_C42736,G2188_C42737,G2188_C42738,G2188_C42739,G2188_C42742,G2188_C42743,G2188_C42744,G2188_C42745 FROM '.$BaseDatos.'.G2188 WHERE G2188_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2188_C42685'] = $key->G2188_C42685;

                $datos[$i]['G2188_C42686'] = $key->G2188_C42686;

                $datos[$i]['G2188_C42687'] = $key->G2188_C42687;

                $datos[$i]['G2188_C42688'] = $key->G2188_C42688;

                $datos[$i]['G2188_C42689'] = $key->G2188_C42689;

                $datos[$i]['G2188_C42690'] = $key->G2188_C42690;

                $datos[$i]['G2188_C42691'] = $key->G2188_C42691;

                $datos[$i]['G2188_C42674'] = $key->G2188_C42674;

                $datos[$i]['G2188_C42675'] = $key->G2188_C42675;

                $datos[$i]['G2188_C42676'] = explode(' ', $key->G2188_C42676)[0];
  
                $hora = '';
                if(!is_null($key->G2188_C42677)){
                    $hora = explode(' ', $key->G2188_C42677)[1];
                }

                $datos[$i]['G2188_C42677'] = $hora;

                $datos[$i]['G2188_C42678'] = $key->G2188_C42678;

                $datos[$i]['G2188_C42679'] = $key->G2188_C42679;

                $datos[$i]['G2188_C42680'] = $key->G2188_C42680;

                $datos[$i]['G2188_C42681'] = $key->G2188_C42681;

                $datos[$i]['G2188_C42682'] = $key->G2188_C42682;

                $datos[$i]['G2188_C42683'] = $key->G2188_C42683;

                $datos[$i]['G2188_C42684'] = $key->G2188_C42684;

                $datos[$i]['G2188_C42692'] = explode(' ', $key->G2188_C42692)[0];

                $datos[$i]['G2188_C42693'] = explode(' ', $key->G2188_C42693)[0];

                $datos[$i]['G2188_C42694'] = $key->G2188_C42694;

                $datos[$i]['G2188_C42695'] = $key->G2188_C42695;

                $datos[$i]['G2188_C42696'] = $key->G2188_C42696;

                $datos[$i]['G2188_C42697'] = $key->G2188_C42697;

                $datos[$i]['G2188_C42698'] = explode(' ', $key->G2188_C42698)[0];

                $datos[$i]['G2188_C42699'] = explode(' ', $key->G2188_C42699)[0];

                $datos[$i]['G2188_C42700'] = $key->G2188_C42700;

                $datos[$i]['G2188_C42701'] = $key->G2188_C42701;

                $datos[$i]['G2188_C42702'] = $key->G2188_C42702;

                $datos[$i]['G2188_C42703'] = explode(' ', $key->G2188_C42703)[0];

                $datos[$i]['G2188_C42704'] = explode(' ', $key->G2188_C42704)[0];

                $datos[$i]['G2188_C45113'] = $key->G2188_C45113;

                $datos[$i]['G2188_C42705'] = $key->G2188_C42705;

                $datos[$i]['G2188_C42706'] = $key->G2188_C42706;

                $datos[$i]['G2188_C42707'] = $key->G2188_C42707;

                $datos[$i]['G2188_C42708'] = $key->G2188_C42708;

                $datos[$i]['G2188_C42709'] = $key->G2188_C42709;

                $datos[$i]['G2188_C42710'] = $key->G2188_C42710;

                $datos[$i]['G2188_C42711'] = $key->G2188_C42711;

                $datos[$i]['G2188_C42712'] = $key->G2188_C42712;

                $datos[$i]['G2188_C42713'] = $key->G2188_C42713;

                $datos[$i]['G2188_C42714'] = $key->G2188_C42714;

                $datos[$i]['G2188_C42715'] = $key->G2188_C42715;

                $datos[$i]['G2188_C42716'] = $key->G2188_C42716;

                $datos[$i]['G2188_C42717'] = $key->G2188_C42717;

                $datos[$i]['G2188_C42718'] = $key->G2188_C42718;

                $datos[$i]['G2188_C42719'] = $key->G2188_C42719;

                $datos[$i]['G2188_C42720'] = $key->G2188_C42720;

                $datos[$i]['G2188_C42721'] = $key->G2188_C42721;

                $datos[$i]['G2188_C42722'] = $key->G2188_C42722;

                $datos[$i]['G2188_C42723'] = $key->G2188_C42723;

                $datos[$i]['G2188_C42724'] = $key->G2188_C42724;

                $datos[$i]['G2188_C42725'] = $key->G2188_C42725;

                $datos[$i]['G2188_C42740'] = $key->G2188_C42740;

                $datos[$i]['G2188_C42726'] = $key->G2188_C42726;

                $datos[$i]['G2188_C42727'] = $key->G2188_C42727;

                $datos[$i]['G2188_C42728'] = $key->G2188_C42728;

                $datos[$i]['G2188_C42729'] = $key->G2188_C42729;

                $datos[$i]['G2188_C42730'] = $key->G2188_C42730;

                $datos[$i]['G2188_C42731'] = $key->G2188_C42731;

                $datos[$i]['G2188_C42732'] = $key->G2188_C42732;

                $datos[$i]['G2188_C42733'] = $key->G2188_C42733;

                $datos[$i]['G2188_C42734'] = $key->G2188_C42734;

                $datos[$i]['G2188_C42736'] = $key->G2188_C42736;

                $datos[$i]['G2188_C42737'] = $key->G2188_C42737;

                $datos[$i]['G2188_C42738'] = $key->G2188_C42738;

                $datos[$i]['G2188_C42739'] = $key->G2188_C42739;

                $datos[$i]['G2188_C42742'] = $key->G2188_C42742;

                $datos[$i]['G2188_C42743'] = $key->G2188_C42743;

                $datos[$i]['G2188_C42744'] = $key->G2188_C42744;

                $datos[$i]['G2188_C42745'] = $key->G2188_C42745;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2188";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            $objRegPro_t = $resRegPro_t->fetch_object();

            if ($objRegPro_t->reg != 0) {
                $strRegProp_t = " AND G2188_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2188_ConsInte__b as id,  G2188_C42685 as camp1 , G2188_C42686 as camp2 
                     FROM ".$BaseDatos.".G2188  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2188_ConsInte__b as id,  G2188_C42685 as camp1 , G2188_C42686 as camp2  
                    FROM ".$BaseDatos.".G2188  JOIN ".$BaseDatos.".G2188_M".$_POST['muestra']." ON G2188_ConsInte__b = G2188_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2188_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2188_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2188_C42685 LIKE '%".$B."%' OR G2188_C42686 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2188_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2188");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2188_ConsInte__b, G2188_FechaInsercion , G2188_Usuario ,  G2188_CodigoMiembro  , G2188_PoblacionOrigen , G2188_EstadoDiligenciamiento ,  G2188_IdLlamada , G2188_C42685 as principal ,G2188_C42685,G2188_C42686,G2188_C42687, a.LISOPC_Nombre____b as G2188_C42688,G2188_C42689,G2188_C42690, b.LISOPC_Nombre____b as G2188_C42691, c.LISOPC_Nombre____b as G2188_C42674, d.LISOPC_Nombre____b as G2188_C42675,G2188_C42676,G2188_C42677,G2188_C42678,G2188_C42679,G2188_C42680,G2188_C42681,G2188_C42682,G2188_C42683,G2188_C42684,G2188_C42692,G2188_C42693,G2188_C42694, e.LISOPC_Nombre____b as G2188_C42695,G2188_C42696,G2188_C42697,G2188_C42698,G2188_C42699,G2188_C42700, f.LISOPC_Nombre____b as G2188_C42701,G2188_C42702,G2188_C42703,G2188_C42704, g.LISOPC_Nombre____b as G2188_C45113, h.LISOPC_Nombre____b as G2188_C42705, i.LISOPC_Nombre____b as G2188_C42706,G2188_C42707, j.LISOPC_Nombre____b as G2188_C42708, k.LISOPC_Nombre____b as G2188_C42709, l.LISOPC_Nombre____b as G2188_C42710,G2188_C42711, m.LISOPC_Nombre____b as G2188_C42712, n.LISOPC_Nombre____b as G2188_C42713, o.LISOPC_Nombre____b as G2188_C42714,G2188_C42715, p.LISOPC_Nombre____b as G2188_C42716, q.LISOPC_Nombre____b as G2188_C42717, r.LISOPC_Nombre____b as G2188_C42718,G2188_C42719, s.LISOPC_Nombre____b as G2188_C42720, t.LISOPC_Nombre____b as G2188_C42721, u.LISOPC_Nombre____b as G2188_C42722,G2188_C42723, v.LISOPC_Nombre____b as G2188_C42724,G2188_C42725,G2188_C42740, w.LISOPC_Nombre____b as G2188_C42726,G2188_C42727,G2188_C42728, x.LISOPC_Nombre____b as G2188_C42729,G2188_C42730,G2188_C42731, y.LISOPC_Nombre____b as G2188_C42732,G2188_C42733, z.LISOPC_Nombre____b as G2188_C42734, ab.LISOPC_Nombre____b as G2188_C42736, ac.LISOPC_Nombre____b as G2188_C42737,G2188_C42738,G2188_C42739, ad.LISOPC_Nombre____b as G2188_C42742, ae.LISOPC_Nombre____b as G2188_C42743, af.LISOPC_Nombre____b as G2188_C42744, ag.LISOPC_Nombre____b as G2188_C42745 FROM '.$BaseDatos.'.G2188 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2188_C42688 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2188_C42691 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2188_C42674 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2188_C42675 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2188_C42695 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2188_C42701 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2188_C45113 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2188_C42705 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2188_C42706 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2188_C42708 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G2188_C42709 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G2188_C42710 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G2188_C42712 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G2188_C42713 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G2188_C42714 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G2188_C42716 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as q ON q.LISOPC_ConsInte__b =  G2188_C42717 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as r ON r.LISOPC_ConsInte__b =  G2188_C42718 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as s ON s.LISOPC_ConsInte__b =  G2188_C42720 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as t ON t.LISOPC_ConsInte__b =  G2188_C42721 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as u ON u.LISOPC_ConsInte__b =  G2188_C42722 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as v ON v.LISOPC_ConsInte__b =  G2188_C42724 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as w ON w.LISOPC_ConsInte__b =  G2188_C42726 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as x ON x.LISOPC_ConsInte__b =  G2188_C42729 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as y ON y.LISOPC_ConsInte__b =  G2188_C42732 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as z ON z.LISOPC_ConsInte__b =  G2188_C42734 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ab ON ab.LISOPC_ConsInte__b =  G2188_C42736 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ac ON ac.LISOPC_ConsInte__b =  G2188_C42737 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ad ON ad.LISOPC_ConsInte__b =  G2188_C42742 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ae ON ae.LISOPC_ConsInte__b =  G2188_C42743 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as af ON af.LISOPC_ConsInte__b =  G2188_C42744 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ag ON ag.LISOPC_ConsInte__b =  G2188_C42745';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2188_C42677)){
                    $hora_a = explode(' ', $fila->G2188_C42677)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2188_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2188_ConsInte__b , ($fila->G2188_C42685) , ($fila->G2188_C42686) , ($fila->G2188_C42687) , ($fila->G2188_C42688) , ($fila->G2188_C42689) , ($fila->G2188_C42690) , ($fila->G2188_C42691) , ($fila->G2188_C42674) , ($fila->G2188_C42675) , explode(' ', $fila->G2188_C42676)[0] , $hora_a , ($fila->G2188_C42678) , ($fila->G2188_C42679) , ($fila->G2188_C42680) , ($fila->G2188_C42681) , ($fila->G2188_C42682) , ($fila->G2188_C42683) , ($fila->G2188_C42684) , explode(' ', $fila->G2188_C42692)[0] , explode(' ', $fila->G2188_C42693)[0] , ($fila->G2188_C42694) , ($fila->G2188_C42695) , ($fila->G2188_C42696) , ($fila->G2188_C42697) , explode(' ', $fila->G2188_C42698)[0] , explode(' ', $fila->G2188_C42699)[0] , ($fila->G2188_C42700) , ($fila->G2188_C42701) , ($fila->G2188_C42702) , explode(' ', $fila->G2188_C42703)[0] , explode(' ', $fila->G2188_C42704)[0] , ($fila->G2188_C45113) , ($fila->G2188_C42705) , ($fila->G2188_C42706) , ($fila->G2188_C42707) , ($fila->G2188_C42708) , ($fila->G2188_C42709) , ($fila->G2188_C42710) , ($fila->G2188_C42711) , ($fila->G2188_C42712) , ($fila->G2188_C42713) , ($fila->G2188_C42714) , ($fila->G2188_C42715) , ($fila->G2188_C42716) , ($fila->G2188_C42717) , ($fila->G2188_C42718) , ($fila->G2188_C42719) , ($fila->G2188_C42720) , ($fila->G2188_C42721) , ($fila->G2188_C42722) , ($fila->G2188_C42723) , ($fila->G2188_C42724) , ($fila->G2188_C42725) , ($fila->G2188_C42740) , ($fila->G2188_C42726) , ($fila->G2188_C42727) , ($fila->G2188_C42728) , ($fila->G2188_C42729) , ($fila->G2188_C42730) , ($fila->G2188_C42731) , ($fila->G2188_C42732) , ($fila->G2188_C42733) , ($fila->G2188_C42734) , ($fila->G2188_C42736) , ($fila->G2188_C42737) , ($fila->G2188_C42738) , ($fila->G2188_C42739) , ($fila->G2188_C42742) , ($fila->G2188_C42743) , ($fila->G2188_C42744) , ($fila->G2188_C42745) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2188 WHERE G2188_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2188";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            $objRegPro_t = $resRegPro_t->fetch_object();

            if ($objRegPro_t->reg != 0) {
                $strRegProp_t = ' AND G2188_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $strRegProp_t = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2188_ConsInte__b as id,  G2188_C42685 as camp1 , G2188_C42686 as camp2  FROM '.$BaseDatos.'.G2188 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2188_ConsInte__b as id,  G2188_C42685 as camp1 , G2188_C42686 as camp2  
                    FROM ".$BaseDatos.".G2188  JOIN ".$BaseDatos.".G2188_M".$_POST['muestra']." ON G2188_ConsInte__b = G2188_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2188_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2188_C42685 LIKE "%'.$B.'%" OR G2188_C42686 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2188_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2188 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2188(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2188_C42685"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42685 = '".$_POST["G2188_C42685"]."'";
                $LsqlI .= $separador."G2188_C42685";
                $LsqlV .= $separador."'".$_POST["G2188_C42685"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42686"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42686 = '".$_POST["G2188_C42686"]."'";
                $LsqlI .= $separador."G2188_C42686";
                $LsqlV .= $separador."'".$_POST["G2188_C42686"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42687"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42687 = '".$_POST["G2188_C42687"]."'";
                $LsqlI .= $separador."G2188_C42687";
                $LsqlV .= $separador."'".$_POST["G2188_C42687"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42688"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42688 = '".$_POST["G2188_C42688"]."'";
                $LsqlI .= $separador."G2188_C42688";
                $LsqlV .= $separador."'".$_POST["G2188_C42688"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42689"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42689 = '".$_POST["G2188_C42689"]."'";
                $LsqlI .= $separador."G2188_C42689";
                $LsqlV .= $separador."'".$_POST["G2188_C42689"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42690"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42690 = '".$_POST["G2188_C42690"]."'";
                $LsqlI .= $separador."G2188_C42690";
                $LsqlV .= $separador."'".$_POST["G2188_C42690"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42691"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42691 = '".$_POST["G2188_C42691"]."'";
                $LsqlI .= $separador."G2188_C42691";
                $LsqlV .= $separador."'".$_POST["G2188_C42691"]."'";
                $validar = 1;
            }
             
 
            $G2188_C42674 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2188_C42674 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2188_C42674 = ".$G2188_C42674;
                    $LsqlI .= $separador." G2188_C42674";
                    $LsqlV .= $separador.$G2188_C42674;
                    $validar = 1;

                    
                }
            }
 
            $G2188_C42675 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2188_C42675 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2188_C42675 = ".$G2188_C42675;
                    $LsqlI .= $separador." G2188_C42675";
                    $LsqlV .= $separador.$G2188_C42675;
                    $validar = 1;
                }
            }
 
            $G2188_C42676 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2188_C42676 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2188_C42676 = ".$G2188_C42676;
                    $LsqlI .= $separador." G2188_C42676";
                    $LsqlV .= $separador.$G2188_C42676;
                    $validar = 1;
                }
            }
 
            $G2188_C42677 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2188_C42677 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2188_C42677 = ".$G2188_C42677;
                    $LsqlI .= $separador." G2188_C42677";
                    $LsqlV .= $separador.$G2188_C42677;
                    $validar = 1;
                }
            }
 
            $G2188_C42678 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2188_C42678 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2188_C42678 = ".$G2188_C42678;
                    $LsqlI .= $separador." G2188_C42678";
                    $LsqlV .= $separador.$G2188_C42678;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2188_C42679"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42679 = '".$_POST["G2188_C42679"]."'";
                $LsqlI .= $separador."G2188_C42679";
                $LsqlV .= $separador."'".$_POST["G2188_C42679"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42680"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42680 = '".$_POST["G2188_C42680"]."'";
                $LsqlI .= $separador."G2188_C42680";
                $LsqlV .= $separador."'".$_POST["G2188_C42680"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42681"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42681 = '".$_POST["G2188_C42681"]."'";
                $LsqlI .= $separador."G2188_C42681";
                $LsqlV .= $separador."'".$_POST["G2188_C42681"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42682"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42682 = '".$_POST["G2188_C42682"]."'";
                $LsqlI .= $separador."G2188_C42682";
                $LsqlV .= $separador."'".$_POST["G2188_C42682"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42683"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42683 = '".$_POST["G2188_C42683"]."'";
                $LsqlI .= $separador."G2188_C42683";
                $LsqlV .= $separador."'".$_POST["G2188_C42683"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42684"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42684 = '".$_POST["G2188_C42684"]."'";
                $LsqlI .= $separador."G2188_C42684";
                $LsqlV .= $separador."'".$_POST["G2188_C42684"]."'";
                $validar = 1;
            }
             
 
            $G2188_C42692 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2188_C42692"])){    
                if($_POST["G2188_C42692"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2188_C42692"]);
                    if(count($tieneHora) > 1){
                        $G2188_C42692 = "'".$_POST["G2188_C42692"]."'";
                    }else{
                        $G2188_C42692 = "'".str_replace(' ', '',$_POST["G2188_C42692"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2188_C42692 = ".$G2188_C42692;
                    $LsqlI .= $separador." G2188_C42692";
                    $LsqlV .= $separador.$G2188_C42692;
                    $validar = 1;
                }
            }
 
            $G2188_C42693 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2188_C42693"])){    
                if($_POST["G2188_C42693"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2188_C42693"]);
                    if(count($tieneHora) > 1){
                        $G2188_C42693 = "'".$_POST["G2188_C42693"]."'";
                    }else{
                        $G2188_C42693 = "'".str_replace(' ', '',$_POST["G2188_C42693"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2188_C42693 = ".$G2188_C42693;
                    $LsqlI .= $separador." G2188_C42693";
                    $LsqlV .= $separador.$G2188_C42693;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2188_C42694"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42694 = '".$_POST["G2188_C42694"]."'";
                $LsqlI .= $separador."G2188_C42694";
                $LsqlV .= $separador."'".$_POST["G2188_C42694"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42695"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42695 = '".$_POST["G2188_C42695"]."'";
                $LsqlI .= $separador."G2188_C42695";
                $LsqlV .= $separador."'".$_POST["G2188_C42695"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42696"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42696 = '".$_POST["G2188_C42696"]."'";
                $LsqlI .= $separador."G2188_C42696";
                $LsqlV .= $separador."'".$_POST["G2188_C42696"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42697"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42697 = '".$_POST["G2188_C42697"]."'";
                $LsqlI .= $separador."G2188_C42697";
                $LsqlV .= $separador."'".$_POST["G2188_C42697"]."'";
                $validar = 1;
            }
             
 
            $G2188_C42698 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2188_C42698"])){    
                if($_POST["G2188_C42698"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2188_C42698"]);
                    if(count($tieneHora) > 1){
                        $G2188_C42698 = "'".$_POST["G2188_C42698"]."'";
                    }else{
                        $G2188_C42698 = "'".str_replace(' ', '',$_POST["G2188_C42698"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2188_C42698 = ".$G2188_C42698;
                    $LsqlI .= $separador." G2188_C42698";
                    $LsqlV .= $separador.$G2188_C42698;
                    $validar = 1;
                }
            }
 
            $G2188_C42699 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2188_C42699"])){    
                if($_POST["G2188_C42699"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2188_C42699"]);
                    if(count($tieneHora) > 1){
                        $G2188_C42699 = "'".$_POST["G2188_C42699"]."'";
                    }else{
                        $G2188_C42699 = "'".str_replace(' ', '',$_POST["G2188_C42699"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2188_C42699 = ".$G2188_C42699;
                    $LsqlI .= $separador." G2188_C42699";
                    $LsqlV .= $separador.$G2188_C42699;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2188_C42700"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42700 = '".$_POST["G2188_C42700"]."'";
                $LsqlI .= $separador."G2188_C42700";
                $LsqlV .= $separador."'".$_POST["G2188_C42700"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42701"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42701 = '".$_POST["G2188_C42701"]."'";
                $LsqlI .= $separador."G2188_C42701";
                $LsqlV .= $separador."'".$_POST["G2188_C42701"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42702"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42702 = '".$_POST["G2188_C42702"]."'";
                $LsqlI .= $separador."G2188_C42702";
                $LsqlV .= $separador."'".$_POST["G2188_C42702"]."'";
                $validar = 1;
            }
             
 
            $G2188_C42703 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2188_C42703"])){    
                if($_POST["G2188_C42703"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2188_C42703"]);
                    if(count($tieneHora) > 1){
                        $G2188_C42703 = "'".$_POST["G2188_C42703"]."'";
                    }else{
                        $G2188_C42703 = "'".str_replace(' ', '',$_POST["G2188_C42703"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2188_C42703 = ".$G2188_C42703;
                    $LsqlI .= $separador." G2188_C42703";
                    $LsqlV .= $separador.$G2188_C42703;
                    $validar = 1;
                }
            }
 
            $G2188_C42704 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2188_C42704"])){    
                if($_POST["G2188_C42704"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2188_C42704"]);
                    if(count($tieneHora) > 1){
                        $G2188_C42704 = "'".$_POST["G2188_C42704"]."'";
                    }else{
                        $G2188_C42704 = "'".str_replace(' ', '',$_POST["G2188_C42704"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2188_C42704 = ".$G2188_C42704;
                    $LsqlI .= $separador." G2188_C42704";
                    $LsqlV .= $separador.$G2188_C42704;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2188_C45113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C45113 = '".$_POST["G2188_C45113"]."'";
                $LsqlI .= $separador."G2188_C45113";
                $LsqlV .= $separador."'".$_POST["G2188_C45113"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42705"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42705 = '".$_POST["G2188_C42705"]."'";
                $LsqlI .= $separador."G2188_C42705";
                $LsqlV .= $separador."'".$_POST["G2188_C42705"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42706"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42706 = '".$_POST["G2188_C42706"]."'";
                $LsqlI .= $separador."G2188_C42706";
                $LsqlV .= $separador."'".$_POST["G2188_C42706"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42707"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42707 = '".$_POST["G2188_C42707"]."'";
                $LsqlI .= $separador."G2188_C42707";
                $LsqlV .= $separador."'".$_POST["G2188_C42707"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42708"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42708 = '".$_POST["G2188_C42708"]."'";
                $LsqlI .= $separador."G2188_C42708";
                $LsqlV .= $separador."'".$_POST["G2188_C42708"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42709"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42709 = '".$_POST["G2188_C42709"]."'";
                $LsqlI .= $separador."G2188_C42709";
                $LsqlV .= $separador."'".$_POST["G2188_C42709"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42710"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42710 = '".$_POST["G2188_C42710"]."'";
                $LsqlI .= $separador."G2188_C42710";
                $LsqlV .= $separador."'".$_POST["G2188_C42710"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42711"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42711 = '".$_POST["G2188_C42711"]."'";
                $LsqlI .= $separador."G2188_C42711";
                $LsqlV .= $separador."'".$_POST["G2188_C42711"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42712"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42712 = '".$_POST["G2188_C42712"]."'";
                $LsqlI .= $separador."G2188_C42712";
                $LsqlV .= $separador."'".$_POST["G2188_C42712"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42713"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42713 = '".$_POST["G2188_C42713"]."'";
                $LsqlI .= $separador."G2188_C42713";
                $LsqlV .= $separador."'".$_POST["G2188_C42713"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42714"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42714 = '".$_POST["G2188_C42714"]."'";
                $LsqlI .= $separador."G2188_C42714";
                $LsqlV .= $separador."'".$_POST["G2188_C42714"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42715 = '".$_POST["G2188_C42715"]."'";
                $LsqlI .= $separador."G2188_C42715";
                $LsqlV .= $separador."'".$_POST["G2188_C42715"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42716"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42716 = '".$_POST["G2188_C42716"]."'";
                $LsqlI .= $separador."G2188_C42716";
                $LsqlV .= $separador."'".$_POST["G2188_C42716"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42717"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42717 = '".$_POST["G2188_C42717"]."'";
                $LsqlI .= $separador."G2188_C42717";
                $LsqlV .= $separador."'".$_POST["G2188_C42717"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42718"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42718 = '".$_POST["G2188_C42718"]."'";
                $LsqlI .= $separador."G2188_C42718";
                $LsqlV .= $separador."'".$_POST["G2188_C42718"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42719"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42719 = '".$_POST["G2188_C42719"]."'";
                $LsqlI .= $separador."G2188_C42719";
                $LsqlV .= $separador."'".$_POST["G2188_C42719"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42720"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42720 = '".$_POST["G2188_C42720"]."'";
                $LsqlI .= $separador."G2188_C42720";
                $LsqlV .= $separador."'".$_POST["G2188_C42720"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42721"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42721 = '".$_POST["G2188_C42721"]."'";
                $LsqlI .= $separador."G2188_C42721";
                $LsqlV .= $separador."'".$_POST["G2188_C42721"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42722"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42722 = '".$_POST["G2188_C42722"]."'";
                $LsqlI .= $separador."G2188_C42722";
                $LsqlV .= $separador."'".$_POST["G2188_C42722"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42723"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42723 = '".$_POST["G2188_C42723"]."'";
                $LsqlI .= $separador."G2188_C42723";
                $LsqlV .= $separador."'".$_POST["G2188_C42723"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42724"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42724 = '".$_POST["G2188_C42724"]."'";
                $LsqlI .= $separador."G2188_C42724";
                $LsqlV .= $separador."'".$_POST["G2188_C42724"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42725"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42725 = '".$_POST["G2188_C42725"]."'";
                $LsqlI .= $separador."G2188_C42725";
                $LsqlV .= $separador."'".$_POST["G2188_C42725"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42740"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42740 = '".$_POST["G2188_C42740"]."'";
                $LsqlI .= $separador."G2188_C42740";
                $LsqlV .= $separador."'".$_POST["G2188_C42740"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42726"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42726 = '".$_POST["G2188_C42726"]."'";
                $LsqlI .= $separador."G2188_C42726";
                $LsqlV .= $separador."'".$_POST["G2188_C42726"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42727"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42727 = '".$_POST["G2188_C42727"]."'";
                $LsqlI .= $separador."G2188_C42727";
                $LsqlV .= $separador."'".$_POST["G2188_C42727"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42728"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42728 = '".$_POST["G2188_C42728"]."'";
                $LsqlI .= $separador."G2188_C42728";
                $LsqlV .= $separador."'".$_POST["G2188_C42728"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42729"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42729 = '".$_POST["G2188_C42729"]."'";
                $LsqlI .= $separador."G2188_C42729";
                $LsqlV .= $separador."'".$_POST["G2188_C42729"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42730"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42730 = '".$_POST["G2188_C42730"]."'";
                $LsqlI .= $separador."G2188_C42730";
                $LsqlV .= $separador."'".$_POST["G2188_C42730"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42731"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42731 = '".$_POST["G2188_C42731"]."'";
                $LsqlI .= $separador."G2188_C42731";
                $LsqlV .= $separador."'".$_POST["G2188_C42731"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42732"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42732 = '".$_POST["G2188_C42732"]."'";
                $LsqlI .= $separador."G2188_C42732";
                $LsqlV .= $separador."'".$_POST["G2188_C42732"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42733"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42733 = '".$_POST["G2188_C42733"]."'";
                $LsqlI .= $separador."G2188_C42733";
                $LsqlV .= $separador."'".$_POST["G2188_C42733"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42734"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42734 = '".$_POST["G2188_C42734"]."'";
                $LsqlI .= $separador."G2188_C42734";
                $LsqlV .= $separador."'".$_POST["G2188_C42734"]."'";
                $validar = 1;
            }
             
             
  
            if(isset($_POST["G2188_C42736"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42736 = '".$_POST["G2188_C42736"]."'";
                $LsqlI .= $separador."G2188_C42736";
                $LsqlV .= $separador."'".$_POST["G2188_C42736"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42737"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42737 = '".$_POST["G2188_C42737"]."'";
                $LsqlI .= $separador."G2188_C42737";
                $LsqlV .= $separador."'".$_POST["G2188_C42737"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42738"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42738 = '".$_POST["G2188_C42738"]."'";
                $LsqlI .= $separador."G2188_C42738";
                $LsqlV .= $separador."'".$_POST["G2188_C42738"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42739"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42739 = '".$_POST["G2188_C42739"]."'";
                $LsqlI .= $separador."G2188_C42739";
                $LsqlV .= $separador."'".$_POST["G2188_C42739"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42741"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42741 = '".$_POST["G2188_C42741"]."'";
                $LsqlI .= $separador."G2188_C42741";
                $LsqlV .= $separador."'".$_POST["G2188_C42741"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42742"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42742 = '".$_POST["G2188_C42742"]."'";
                $LsqlI .= $separador."G2188_C42742";
                $LsqlV .= $separador."'".$_POST["G2188_C42742"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42743"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42743 = '".$_POST["G2188_C42743"]."'";
                $LsqlI .= $separador."G2188_C42743";
                $LsqlV .= $separador."'".$_POST["G2188_C42743"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42744"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42744 = '".$_POST["G2188_C42744"]."'";
                $LsqlI .= $separador."G2188_C42744";
                $LsqlV .= $separador."'".$_POST["G2188_C42744"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42745"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42745 = '".$_POST["G2188_C42745"]."'";
                $LsqlI .= $separador."G2188_C42745";
                $LsqlV .= $separador."'".$_POST["G2188_C42745"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42746"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42746 = '".$_POST["G2188_C42746"]."'";
                $LsqlI .= $separador."G2188_C42746";
                $LsqlV .= $separador."'".$_POST["G2188_C42746"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42747"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42747 = '".$_POST["G2188_C42747"]."'";
                $LsqlI .= $separador."G2188_C42747";
                $LsqlV .= $separador."'".$_POST["G2188_C42747"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2188_C42748"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_C42748 = '".$_POST["G2188_C42748"]."'";
                $LsqlI .= $separador."G2188_C42748";
                $LsqlV .= $separador."'".$_POST["G2188_C42748"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_TipNo_Efe_b, MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $reintento = $dataMonoEf->MONOEF_TipNo_Efe_b;
                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2188_C42675 = ".$reintento;
                    $LsqlI .= $separador."G2188_C42675";
                    $LsqlV .= $separador.$reintento;
                    $validar = 1;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2188_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2188_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2188_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2188_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2188_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2188_Usuario , G2188_FechaInsercion, G2188_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2188_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2188 WHERE G2188_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
