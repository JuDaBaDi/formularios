<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		if($_GET['action'] == "GET_DATOS"){
//          *NBG*2020-05*enviar a la busqueda manual el valor del id en la muestra*  
            if(isset($_GET["id_campana_crm"]) && $_GET["id_campana_crm"] != 0){
                $querymuestra="select CAMPAN_ConsInte__GUION__Pob_b,CAMPAN_ConsInte__MUESTR_b from {$BaseDatos_systema}.CAMPAN where CAMPAN_ConsInte__b={$_GET["id_campana_crm"]}";
                $querymuestra=$mysqli->query($querymuestra);
                if($querymuestra && $querymuestra->num_rows > 0){
                    $datoCampan=$querymuestra->fetch_array();
                    $str_Pobla_Campan = "G{$datoCampan["CAMPAN_ConsInte__GUION__Pob_b"]}";
                    $intMuestra="{$str_Pobla_Campan}_M{$datoCampan["CAMPAN_ConsInte__MUESTR_b"]}";
                    
                }        
            }
            $str_Lsql = " SELECT *,{$intMuestra}_CoInMiPo__b as id_muestra FROM {$BaseDatos}.{$str_Pobla_Campan} LEFT JOIN {$BaseDatos}.{$intMuestra} ON {$str_Pobla_Campan}_ConsInte__b={$intMuestra}_CoInMiPo__b";
                $Where = " WHERE ";
			$usados = 0;
			if(!is_null($_POST['G2826_C56148']) && $_POST['G2826_C56148'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56148 LIKE '%". $_POST['G2826_C56148'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56149']) && $_POST['G2826_C56149'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56149 LIKE '%". $_POST['G2826_C56149'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56530']) && $_POST['G2826_C56530'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56530 LIKE '%". $_POST['G2826_C56530'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56150']) && $_POST['G2826_C56150'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56150 LIKE '%". $_POST['G2826_C56150'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56151']) && $_POST['G2826_C56151'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56151 LIKE '%". $_POST['G2826_C56151'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56152']) && $_POST['G2826_C56152'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56152 LIKE '%". $_POST['G2826_C56152'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56153']) && $_POST['G2826_C56153'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56153 LIKE '%". $_POST['G2826_C56153'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56157']) && $_POST['G2826_C56157'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56157 LIKE '%". $_POST['G2826_C56157'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56158']) && $_POST['G2826_C56158'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56158 LIKE '%". $_POST['G2826_C56158'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56154']) && $_POST['G2826_C56154'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56154 LIKE '%". $_POST['G2826_C56154'] ."%' ";
				$usados = 1;
			}
			if(!is_null($_POST['G2826_C56155']) && $_POST['G2826_C56155'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2826_C56155 LIKE '%". $_POST['G2826_C56155'] ."%' ";
				$usados = 1;
			}
            if($usados==0){
                    $Where = "";
                }
                $str_Lsql .= $Where;
                
            //echo $str_Lsql;
			$resultado = $mysqli->query($str_Lsql);
			$arrayDatos = array();
			while ($key = $resultado->fetch_assoc()) {
				$arrayDatos[] = $key;
			}

			$newJson = array();
			$newJson[0]['cantidad_registros'] = $resultado->num_rows;
			$newJson[0]['registros'] = $arrayDatos;
            if($usados == 0 && $resultado->num_rows == 0){
                $newJson[0]['mensaje'] = "No se encontraron registros para este tipo de busqueda";
            }else{
                if($usados == 1 && $resultado->num_rows == 0){
                    $strSQLValidaReg="SELECT * FROM {$BaseDatos}.{$str_Pobla_Campan}{$Where}";
                       $strSQLValidaReg=$mysqli->query($strSQLValidaReg);
                    if($strSQLValidaReg ->num_rows > 0){
                        $newJson[0]['mensaje'] = "No tienes permiso para acceder a este registro";
                    }
                }
            }

			echo json_encode($newJson);
		}
	}
?>
