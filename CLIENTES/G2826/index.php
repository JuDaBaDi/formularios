<?php 
    /*
        Document   : index
        Created on : 2020-09-21 14:04:37
        Author     : Jose David y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjgyNg==  
    */
    session_start();
    $url_crud =  "formularios/G2826/G2826_CRUD_web.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        if(isset($_GET['v'])){
            
        }else{
            echo '<body class="hold-transition" >';
        }
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <?php if(!isset($_GET['aceptaTerminos'])){ ?>
                    <div class="login-logo hed">
                        <?php if(!isset($_GET['v'])){ ?>
                        <?php }else{ 
                                
                            }
                        ?>

                    </div><!-- /.login-logo -->
                    <?php if(isset($_GET['v'])){ 
                        
                    }else { ?>
                    <div class="login-box-body">
					<?php } ?>
                        <form action="formularios/G2826/G2826_CRUD_web.php" method="post" id="formLogin" enctype="multipart/form-data">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56150" id="LblG2826_C56150">APELLIDOS*</label>
								<input type="text" class="form-control input-sm" id="G2826_C56150" value=""  name="G2826_C56150"  placeholder="APELLIDOS" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56151" id="LblG2826_C56151">NOMBRES*</label>
								<input type="text" class="form-control input-sm" id="G2826_C56151" value=""  name="G2826_C56151"  placeholder="NOMBRES" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56153" id="LblG2826_C56153">CORREO*</label>
								<input type="text" class="form-control input-sm" id="G2826_C56153" value=""  name="G2826_C56153"  placeholder="CORREO" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56154" id="LblG2826_C56154">CELULAR*</label>
								<input type="text" class="form-control input-sm" id="G2826_C56154" value=""  name="G2826_C56154"  placeholder="CELULAR" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56155" id="LblG2826_C56155">MAESTRIA_DE_INTERES*</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56155" id="G2826_C56155" required>
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3390 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                           
                            <div class="form-group">
                                <label for="G2826_C56156" id="LblG2826_C56156">COMENTARIO*</label>
                                <textarea class="form-control input-sm" name="G2826_C56156" id="G2826_C56156"  value="" placeholder="COMENTARIO" required></textarea>
                            </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
                        <div class="secciones">   
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        1.Datos personales
                                    </a>
                                </h2>

                            </div>
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56532" id="LblG2826_C56532">NACIONALIDAD</label>
								<input type="text" class="form-control input-sm" id="G2826_C56532" value=""  name="G2826_C56532"  placeholder="NACIONALIDAD" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56534" id="LblG2826_C56534">FECHA DE NACIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56534" id="G2826_C56534" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56996" id="LblG2826_C56996">LUGAR DE NACIMIENTO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56996" id="G2826_C56996" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3404 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56997" id="LblG2826_C56997">TIPO DE SANGRE</label>
								<input type="text" class="form-control input-sm" id="G2826_C56997" value=""  name="G2826_C56997"  placeholder="TIPO DE SANGRE" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56533" id="LblG2826_C56533">SEXO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56533" id="G2826_C56533" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3405 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56535" id="LblG2826_C56535">ESTADO CIVIL</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56535" id="G2826_C56535" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3403 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56537" id="LblG2826_C56537">PROVINCIA/CANTON DOMICILIO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56537" id="G2826_C56537" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3407 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56538" id="LblG2826_C56538">DIRECCION DOMICILIO</label>
								<input type="text" class="form-control input-sm" id="G2826_C56538" value=""  name="G2826_C56538"  placeholder="DIRECCION DOMICILIO" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56539" id="LblG2826_C56539">PROVINCIA / CANTON TRABAJO</label>
								<input type="text" class="form-control input-sm" id="G2826_C56539" value=""  name="G2826_C56539"  placeholder="PROVINCIA / CANTON TRABAJO" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56540" id="LblG2826_C56540">DIRECCION TRABAJO</label>
								<input type="text" class="form-control input-sm" id="G2826_C56540" value=""  name="G2826_C56540"  placeholder="DIRECCION TRABAJO" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <!-- FIN DEL CAMPO TIPO MEMO -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        2.Datos de la familia
                                    </a>
                                </h2>

                            </div> 
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        a.Primer familiar
                                    </a>
                                </h5>                            
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56541" id="LblG2826_C56541">NOMBRES 1er Familiar</label>
								<input type="text" class="form-control input-sm" id="G2826_C56541" value=""  name="G2826_C56541"  placeholder="NOMBRES 1er Familiar" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56542" id="LblG2826_C56542">APELLIDOS 1er Familiar</label>
								<input type="text" class="form-control input-sm" id="G2826_C56542" value=""  name="G2826_C56542"  placeholder="APELLIDOS 1er Familiar" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56543" id="LblG2826_C56543">FECHA DE NACIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56543" id="G2826_C56543" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56544" id="LblG2826_C56544">RELACIÓN</label>
								<input type="text" class="form-control input-sm" id="G2826_C56544" value=""  name="G2826_C56544"  placeholder="RELACIÓN" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        b.Segundo familiar
                                    </a>
                                </h5>  
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56545" id="LblG2826_C56545">NOMBRES 2do Familiar</label>
								<input type="text" class="form-control input-sm" id="G2826_C56545" value=""  name="G2826_C56545"  placeholder="NOMBRES 2do Familiar" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56546" id="LblG2826_C56546">APELLIDOS 2do Familiar</label>
								<input type="text" class="form-control input-sm" id="G2826_C56546" value=""  name="G2826_C56546"  placeholder="APELLIDOS 2do Familiar" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56547" id="LblG2826_C56547">FECHA DE NACIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56547" id="G2826_C56547" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56548" id="LblG2826_C56548">RELACIÓN</label>
								<input type="text" class="form-control input-sm" id="G2826_C56548" value=""  name="G2826_C56548"  placeholder="RELACIÓN" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        c.tercer familiar
                                    </a>
                                </h5>  
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56549" id="LblG2826_C56549">NOMBRES 3er Familiar</label>
								<input type="text" class="form-control input-sm" id="G2826_C56549" value=""  name="G2826_C56549"  placeholder="NOMBRES 3er Familiar" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56550" id="LblG2826_C56550">APELLIDOS 3er Familiar</label>
								<input type="text" class="form-control input-sm" id="G2826_C56550" value=""  name="G2826_C56550"  placeholder="APELLIDOS 3er Familiar" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56551" id="LblG2826_C56551">FECHA DE NACIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56551" id="G2826_C56551" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56552" id="LblG2826_C56552">RELACIÓN</label>
								<input type="text" class="form-control input-sm" id="G2826_C56552" value=""  name="G2826_C56552"  placeholder="RELACIÓN" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        3.Estudios
                                    </a>
                                </h2>

                            </div> 
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        a.Estudios primarios
                                    </a>
                                </h5>  
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56553" id="LblG2826_C56553">INSTITUCIÓN Est. Primarios</label>
								<input type="text" class="form-control input-sm" id="G2826_C56553" value=""  name="G2826_C56553"  placeholder="INSTITUCIÓN Est. Primarios" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56554" id="LblG2826_C56554">PROVINCIA / CANTON</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56554" id="G2826_C56554" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3407 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56555" id="LblG2826_C56555">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56555" id="G2826_C56555" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56556" id="LblG2826_C56556">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56556" id="G2826_C56556" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
                            <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        b.Estudios secundarios
                                    </a>
                            </h5> 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56557" id="LblG2826_C56557">INSTITUCIÓN Est. Secundarios</label>
								<input type="text" class="form-control input-sm" id="G2826_C56557" value=""  name="G2826_C56557"  placeholder="INSTITUCIÓN Est. Secundarios" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56558" id="LblG2826_C56558">PROVINCIA / CANTON</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56558" id="G2826_C56558" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3407 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56559" id="LblG2826_C56559">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56559" id="G2826_C56559" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56560" id="LblG2826_C56560">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56560" id="G2826_C56560" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
                            <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        c.Estudios superiores
                                    </a>
                            </h5> 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56561" id="LblG2826_C56561">INSTITUCIÓN Est. Superiores</label>
								<input type="text" class="form-control input-sm" id="G2826_C56561" value=""  name="G2826_C56561"  placeholder="INSTITUCIÓN Est. Superiores" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56562" id="LblG2826_C56562">CARRERA</label>
								<input type="text" class="form-control input-sm" id="G2826_C56562" value=""  name="G2826_C56562"  placeholder="CARRERA" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56563" id="LblG2826_C56563">LUGAR</label>
								<input type="text" class="form-control input-sm" id="G2826_C56563" value=""  name="G2826_C56563"  placeholder="LUGAR" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56564" id="LblG2826_C56564">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56564" id="G2826_C56564" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56565" id="LblG2826_C56565">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56565" id="G2826_C56565" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
<!--
							<div class="form-group">
								<label for="G2826_C56566" id="LblG2826_C56566">INSTITUCIÓN Posgrado</label>
								<input type="text" class="form-control input-sm" id="G2826_C56566" value=""  name="G2826_C56566"  placeholder="INSTITUCIÓN Posgrado" >
							</div>
-->
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
<!--
							<div class="form-group">
								<label for="G2826_C56567" id="LblG2826_C56567">PROGRAMA</label>
								<input type="text" class="form-control input-sm" id="G2826_C56567" value=""  name="G2826_C56567"  placeholder="PROGRAMA" >
							</div>
-->
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
<!--
							<div class="form-group">
								<label for="G2826_C56568" id="LblG2826_C56568">LUGAR</label>
								<input type="text" class="form-control input-sm" id="G2826_C56568" value=""  name="G2826_C56568"  placeholder="LUGAR" >
							</div>
-->
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
<!--
                            <div class="form-group">
                                <label for="G2826_C56569" id="LblG2826_C56569">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56569" id="G2826_C56569" placeholder="YYYY-MM-DD" >
                            </div>
-->
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
<!--
                            <div class="form-group">
                                <label for="G2826_C56570" id="LblG2826_C56570">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56570" id="G2826_C56570" placeholder="YYYY-MM-DD" >
                            </div>
-->
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        4.Experiencia laboral
                                    </a>
                                </h2>

                            </div> 
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        a.Trabajo actual
                                    </a>
                                </h5>							
							<div class="form-group">
								<label for="G2826_C56571" id="LblG2826_C56571">EMPRESA / INSTITUCION Exp. Laboral Actual</label>
								<input type="text" class="form-control input-sm" id="G2826_C56571" value=""  name="G2826_C56571"  placeholder="EMPRESA / INSTITUCION Exp. Laboral Actual" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56572" id="LblG2826_C56572">CARGO</label>
								<input type="text" class="form-control input-sm" id="G2826_C56572" value=""  name="G2826_C56572"  placeholder="CARGO" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56573" id="LblG2826_C56573">FECHA DE INICIO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56573" id="G2826_C56573" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO DECIMAL -->
                            <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56574" id="LblG2826_C56574">SUELDO</label>
                                <input type="text" class="form-control input-sm Decimal" value=""  name="G2826_C56574" id="G2826_C56574" placeholder="SUELDO" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO DECIMAL -->
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        b.Trabajo anterior
                                    </a>
                                </h5> 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56575" id="LblG2826_C56575">EMPRESA / INSTITUCIÓN Exp. Laboral Anterior</label>
								<input type="text" class="form-control input-sm" id="G2826_C56575" value=""  name="G2826_C56575"  placeholder="EMPRESA / INSTITUCIÓN Exp. Laboral Anterior" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56576" id="LblG2826_C56576">CARGO</label>
								<input type="text" class="form-control input-sm" id="G2826_C56576" value=""  name="G2826_C56576"  placeholder="CARGO" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56577" id="LblG2826_C56577">FECHA DE INICIO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56577" id="G2826_C56577" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56578" id="LblG2826_C56578">FECHA FINAL</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56578" id="G2826_C56578" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO DECIMAL -->
                            <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56579" id="LblG2826_C56579">SUELDO</label>
                                <input type="text" class="form-control input-sm Decimal" value=""  name="G2826_C56579" id="G2826_C56579" placeholder="SUELDO" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO DECIMAL -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        5.Experiencia docente(de ser el caso)
                                    </a>
                                </h2>

                            </div> 
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        a.Institución actual
                                    </a>
                                </h5> 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56580" id="LblG2826_C56580">INSTITUCIÓN Exp. Docente Actual</label>
								<input type="text" class="form-control input-sm" id="G2826_C56580" value=""  name="G2826_C56580"  placeholder="INSTITUCIÓN Exp. Docente Actual" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2826_C56581" id="LblG2826_C56581">FUNCIONES</label>
                                <textarea class="form-control input-sm" name="G2826_C56581" id="G2826_C56581"  value="" placeholder="FUNCIONES" ></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56582" id="LblG2826_C56582">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56582" id="G2826_C56582" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56583" id="LblG2826_C56583">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56583" id="G2826_C56583" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        b.Institución anterior
                                    </a>
                                </h5> 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56584" id="LblG2826_C56584">INSTITUCIÓN Exp. Docente Anterior</label>
								<input type="text" class="form-control input-sm" id="G2826_C56584" value=""  name="G2826_C56584"  placeholder="INSTITUCIÓN Exp. Docente Anterior" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56585" id="LblG2826_C56585">FUNCIONES</label>
								<input type="text" class="form-control input-sm" id="G2826_C56585" value=""  name="G2826_C56585"  placeholder="FUNCIONES" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56586" id="LblG2826_C56586">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56586" id="G2826_C56586" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2826_C56587" id="LblG2826_C56587">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56587" id="G2826_C56587" placeholder="YYYY-MM-DD" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        6.Experiencia en investigación(de ser el caso)
                                    </a>
                                </h2>

                            </div> 
                                <h5 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        a.Institución actual
                                    </a>
                                </h5> 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C56588" id="LblG2826_C56588">INSTITUCIÓN Exp. Investigación</label>
								<input type="text" class="form-control input-sm" id="G2826_C56588" value=""  name="G2826_C56588"  placeholder="INSTITUCIÓN Exp. Investigación" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2826_C56589" id="LblG2826_C56589">INVESTIGACIONES REALIZADAS</label>
                                <textarea class="form-control input-sm" name="G2826_C56589" id="G2826_C56589"  value="" placeholder="INVESTIGACIONES REALIZADAS" ></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
  
                            <!-- CAMPO TIPO MEMO -->
<!--
                            <div class="form-group">
                                <label for="G2826_C56590" id="LblG2826_C56590">FUNCIONES</label>
                                <textarea class="form-control input-sm" name="G2826_C56590" id="G2826_C56590"  value="" placeholder="FUNCIONES" ></textarea>
                            </div>
-->
                            <!-- FIN DEL CAMPO TIPO MEMO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
<!--
                            <div class="form-group">
                                <label for="G2826_C56591" id="LblG2826_C56591">DESDE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56591" id="G2826_C56591" placeholder="YYYY-MM-DD" >
                            </div>
-->
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
<!--
                            <div class="form-group">
                                <label for="G2826_C56592" id="LblG2826_C56592">HASTA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2826_C56592" id="G2826_C56592" placeholder="YYYY-MM-DD" >
                            </div>
-->
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        7.Manejo de idiomas
                                    </a>
                                </h2>

                            </div>                    
                    <div class="form-group">
                        <label for="G2826_C56593" id="LblG2826_C56593">ESPAÑOL</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56593" id="G2826_C56593" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56594" id="LblG2826_C56594">INGLÉS</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56594" id="G2826_C56594" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56595" id="LblG2826_C56595">FRANCÉS</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56595" id="G2826_C56595" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56596" id="LblG2826_C56596">QUICHUA</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56596" id="G2826_C56596" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2826_C56597" id="LblG2826_C56597">Otros Idiomas, especifique el nombre y nivel de dominio</label>
                                <textarea class="form-control input-sm" name="G2826_C56597" id="G2826_C56597"  value="" placeholder="Otros Idiomas, especifique el nombre y nivel de dominio" ></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        8.Tecnologias
                                    </a>
                                </h2>

                            </div>
                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56598" id="LblG2826_C56598">COMPUTADORA PERSONAL</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56598" id="G2826_C56598" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56599" id="LblG2826_C56599">COMPUTADORA PORTÁTIL</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56599" id="G2826_C56599" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56600" id="LblG2826_C56600">TABLET</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56600" id="G2826_C56600" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56601" id="LblG2826_C56601">PDA</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56601" id="G2826_C56601" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56602" id="LblG2826_C56602">SMARTPHONE</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56602" id="G2826_C56602" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56603" id="LblG2826_C56603">ACCESO A INTERNET</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56603" id="G2826_C56603" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56604" id="LblG2826_C56604">DOMINIO DE APLICACIONES DE OFICINA</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56604" id="G2826_C56604" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        9.Otros datos
                                    </a>
                                </h2>

                            </div>  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2826_C56605" id="LblG2826_C56605">INFORMACIÓN ADICIONAL: Proporcione en este espacio información que no fué incluido anteriormente en el presente instrumento y que considere que deba ser considerado como becas, distinciones académicas, membresías institucionales, etc.</label>
                                <textarea class="form-control input-sm" name="G2826_C56605" id="G2826_C56605"  value="" placeholder="INFORMACIÓN ADICIONAL: Proporcione en este espacio información que no fué incluido anteriormente en el presente instrumento y que considere que deba ser considerado como becas, distinciones académicas, membresías institucionales, etc." ></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2826_C56606" id="LblG2826_C56606">INFORMACIÓN IMPORTANTE: En caso de tener alguna enfermedad que usted considere deba ser del conocimiento del Director del programa por favor escríbalo</label>
                                <textarea class="form-control input-sm" name="G2826_C56606" id="G2826_C56606"  value="" placeholder="INFORMACIÓN IMPORTANTE: En caso de tener alguna enfermedad que usted considere deba ser del conocimiento del Director del programa por favor escríbalo" ></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        10.datos para la factura
                                    </a>
                                </h2>

                            </div>                             
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C57272" id="LblG2826_C57272">TIPO DE ID</label>
								<input type="text" class="form-control input-sm" id="G2826_C57272" value=""  name="G2826_C57272"  placeholder="TIPO DE ID" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C57273" id="LblG2826_C57273">NUMERO DE ID</label>
								<input type="text" class="form-control input-sm" id="G2826_C57273" value=""  name="G2826_C57273"  placeholder="NUMERO DE ID" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C57274" id="LblG2826_C57274">NOMBRE</label>
								<input type="text" class="form-control input-sm" id="G2826_C57274" value=""  name="G2826_C57274"  placeholder="NOMBRE" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C57275" id="LblG2826_C57275">CORREO</label>
								<input type="text" class="form-control input-sm" id="G2826_C57275" value=""  name="G2826_C57275"  placeholder="CORREO" >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        11.Tipo de financiamiento
                                    </a>
                                </h2>

                            </div>                             
                    <div class="form-group">
                        <label for="G2826_C57276" id="LblG2826_C57276">TIPO DE FINANCIAMIENTO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C57276" id="G2826_C57276" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 0 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2826_C57277" id="LblG2826_C57277">SI ELIGIO "OTROS" ESPECIFIQUE</label>
								<input type="text" class="form-control input-sm" id="G2826_C57277" value=""  name="G2826_C57277"  placeholder="SI ELIGIO OTROS ESPECIFIQUE" >
							</div>                       
                            <div class="box-header with-border" style="border-bottom:2px solid #3c8dbc">
                                <h2 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                                        12.Documentos
                                    </a>
                                </h2>

                            </div>                               
                            <!-- CAMPO TIPO ADJUNTO -->
                            <div class="form-group">
                                <label for="FG2826_C56609" id="LblFG2826_C56609">Copia a color de la Cédula de Identidad</label>
                                <input type="file" class="adjuntos" id="FG2826_C56609" name="FG2826_C56609">
                                <p class="help-block">
                                    Peso maximo del archivo 9 MB
                                </p>
                            </div>
 
                            <!-- CAMPO TIPO ADJUNTO -->
                            <div class="form-group">
                                <label for="FG2826_C56607" id="LblFG2826_C56607">Título de tercer nivel de grado notarizado</label>
                                <input type="file" class="adjuntos" id="FG2826_C56607" name="FG2826_C56607">
                                <p class="help-block">
                                    Peso maximo del archivo 9 MB
                                </p>
                            </div>
 
                            <!-- CAMPO TIPO ADJUNTO -->
                            <div class="form-group">
                                <label for="FG2826_C56608" id="LblFG2826_C56608">Registro del título en el SENESCYT</label>
                                <input type="file" class="adjuntos" id="FG2826_C56608" name="FG2826_C56608">
                                <p class="help-block">
                                    Peso maximo del archivo 9 MB
                                </p>
                            </div>

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56614" id="LblG2826_C56614">UEES-OnLine se compromete a proteger y respetar su privacidad, y solo usaremos su información personal para administrar su cuenta y proporcionar los productos y servicios que nos solicite. Nos gustaría ponernos en contacto con usted con la finalidad de mostrarle nuestros productos y servicios, así como otros contenidos que puedan interesarle. Si acepta los términos y condiciones de la convocatoria para realizar estudios en Posgrado en la modalidad online de la UEES, seleccione SI en la casilla a continuación. Al seleccionar SI, acepta que UEES-OnLine almacene y procese la información personal suministrada para proporcionarle el contenido solicitado.</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56614" id="G2826_C56614" >
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                </div>    
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="pasoId" id="pasoId" value="<?php if (isset($_GET["paso"])) {
                                echo $_GET["paso"];
                            }else{ echo "0"; } ?>">
                            <input type="hidden" name="id" id="hidId" value='0'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="ORIGEN_DY_WF" id="ORIGEN_DY_WF" value='<?php if(isset($_GET['origen'])){ echo $_GET['origen']; }else{ echo "0"; }?>' >
                            
                            <?php if (isset($_GET['v'])){ ?>
                                    
                            <?php }else{ ?>
                                    <input type= "hidden" name="OPTIN_DY_WF" id="OPTIN_DY_WF" value="SIMPLE">
                            <?php }?>

                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                    <?php 
                        }else{ ?>
                            <?php if(isset($_GET['v'])){ 
                                
                            }else { ?>
                            <div class="login-box-body">
                                <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                            <?php } ?>
                    <?php 
                            if($_GET['aceptaTerminos'] == 'acepto'){
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);

                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2826 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){


                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2826 WHERE G2826_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                        $Lsql = "UPDATE ".$BaseDatos.".G2826 SET G2826_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'CONFIRMADO' WHERE G2826_ConsInte__b =".$ultimoResgistroInsertado;
                                        if ($mysqli->query($Lsql) === TRUE) {
                                            /* Acepto toca meterlo en la muestra  G626_M285*/

                                            
                $muestraCompleta = "G2826_M2826";

                
                $nuevafecha = "NULL";
                $fecha = date('Y-m-d');
                $nuevafecha = strtotime ( '+0 day' , strtotime ( $fecha ) ) ;
                $nuevafecha = date('Y-m-d',$nuevafecha);
                $hora = date('H:i:s');
                $nuevahora = strtotime ( '+6 hour' , strtotime ( $hora ) ) ;
                $nuevahora = date('H:s:i',$nuevahora);
                $nuevafecha = $nuevafecha.$nuevahora;

                /* primero buscamos el que tenga el menor numero de registros */
                
                $Xlsql = "SELECT ASITAR_ConsInte__USUARI_b, COUNT(".$muestraCompleta."_ConIntUsu_b) AS total FROM     ".$BaseDatos_systema.".ASITAR LEFT JOIN ".$BaseDatos.".".$muestraCompleta." ON ASITAR_ConsInte__USUARI_b = ".$muestraCompleta."_ConIntUsu_b WHERE ASITAR_ConsInte__CAMPAN_b = 1518 AND (".$muestraCompleta."_Estado____b <> 3 OR (".$muestraCompleta."_Estado____b IS NULL)) AND (ASITAR_Automaticos_b <> 0 OR (ASITAR_Automaticos_b IS NULL)) GROUP BY ASITAR_ConsInte__USUARI_b ORDER BY COUNT(".$muestraCompleta."_ConIntUsu_b) LIMIT 1;";
                $res = $mysqli->query($Xlsql);
                $datoLsql = $res->fetch_array();

                /* ahora insertamos los datos en la muestra */
                
                $insertarMuestraLsql = "INSERT INTO  ".$BaseDatos.".".$muestraCompleta." (".$muestraCompleta."_CoInMiPo__b ,  ".$muestraCompleta."_NumeInte__b, ".$muestraCompleta."_Estado____b , ".$muestraCompleta."_ConIntUsu_b, ".$muestraCompleta."_FecHorAge_b) VALUES (".$ultimoResgistroInsertado.", 0 , 2, ".$datoLsql['ASITAR_ConsInte__USUARI_b'].", '".$nuevafecha."');";
                $mysqli->query($insertarMuestraLsql);  


                                    

                                            echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";

                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";   
                                    }


                                }

                            }else{
                                /* NO CONFIRMAN */
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);
                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2826 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){
                                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2826 WHERE G2826_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $Lsql = "UPDATE ".$BaseDatos.".G2826 SET G2826_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'NO CONFIRMADO' WHERE G2826_ConsInte__b =".$ultimoResgistroInsertado;

                                        if ($mysqli->query($Lsql) === TRUE) {
                                            echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";
                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";            
                                    }

                                }
                            }

                                
                        } ?>


                </div><!-- /.login-box -->

            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2826/G2826_eventos.js"></script>
        <script type="text/javascript">

            $("#formLogin").submit(function(event) {
                
            });
            $(function(){
                $(".adjuntos").change(function(){
                    var imax = $(this).attr("valor");
                    var imagen = this.files[0];

                    // if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" && imagen["type"] != "application/pdf"){
                    //     swal({
                    //         title : "Error al subir el archivo",
                    //         text  : "El archivo debe estar en formato PNG , JPG, PDF",
                    //         type  : "error",
                    //         confirmButtonText : "Cerrar"
                    //     });
                    // }else 
                    if(imagen["size"] > 9000000 ) {
                        $(this).val("");
                        swal({
                            title : "Error al subir el archivo",
                            text  : "El archivo no debe pesar mas de 9 MB",
                            type  : "error",
                            confirmButtonText : "Cerrar"
                        });
                    }
                });
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2826_C56534").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56543").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56547").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56551").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56555").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56556").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56559").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56560").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56564").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56565").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56569").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56570").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56573").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56577").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56578").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56582").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56583").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56586").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56587").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56591").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2826_C56592").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               

//            $("#G2826_C56574").inputmask("numeric", {
//                radixPoint: ",",
//                groupSeparator: ".",
//                digits: 2,
//                autoGroup: true,
//                rightAlign: false,
//                oncleared: function () { self.Value(''); }
//            });
//            
//            $("#G2826_C56579").inputmask("numeric", {
//                radixPoint: ",",
//                groupSeparator: ".",
//                digits: 2,
//                autoGroup: true,
//                rightAlign: false,
//                oncleared: function () { self.Value(''); }
//            });
            

               /* Si tiene dependiendias */
               


    //function para MAESTRIA_DE_INTERES 

    $("#G2826_C56155").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para LUGAR DE NACIMIENTO 

    $("#G2826_C56996").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SEXO 

    $("#G2826_C56533").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO CIVIL 

    $("#G2826_C56535").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PROVINCIA/CANTON DOMICILIO 

    $("#G2826_C56537").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PROVINCIA / CANTON 

    $("#G2826_C56554").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PROVINCIA / CANTON 

    $("#G2826_C56558").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESPAÑOL 

    $("#G2826_C56593").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para INGLÉS 

    $("#G2826_C56594").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para FRANCÉS 

    $("#G2826_C56595").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para QUICHUA 

    $("#G2826_C56596").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para COMPUTADORA PERSONAL 

    $("#G2826_C56598").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para COMPUTADORA PORTÁTIL 

    $("#G2826_C56599").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TABLET 

    $("#G2826_C56600").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PDA 

    $("#G2826_C56601").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SMARTPHONE 

    $("#G2826_C56602").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ACCESO A INTERNET 

    $("#G2826_C56603").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DOMINIO DE APLICACIONES DE OFICINA 

    $("#G2826_C56604").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para UEES-OnLine se compromete a proteger y respetar su privacidad, y solo usaremos su información personal para administrar su cuenta y proporcionar los productos y servicios que nos solicite. Nos gustaría ponernos en contacto con usted con la finalidad de mostrarle nuestros productos y servicios, así como otros contenidos que puedan interesarle. Si acepta los términos y condiciones de la convocatoria para realizar estudios en Posgrado en la modalidad online de la UEES, seleccione SI en la casilla a continuación. Al seleccionar SI, acepta que UEES-OnLine almacene y procese la información personal suministrada para proporcionarle el contenido solicitado. 

    $("#G2826_C56614").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });


               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                            redireccionar();
                        
                            
                <?php   }elseif($_GET['result'] ==  '1062'){ ?>
                            swal({
                                title: "Error!",
                                text: 'Este correo ya se encuentra registrado',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error al guardar los daatos',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
//                        unset($_SESSION['result']);
                    }
                ?>
            });
        </script>

        <?php 
        require_once('formularios/G2826/G2826_extender_funcionalidad.php');
        ?>
        
