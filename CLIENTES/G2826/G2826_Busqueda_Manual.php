
<?php
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
	if(isset($_GET["accion_manual"])){
		$idMonoef=-19;
		$texto = "Busqueda No Aplica";
	}else{
		$idMonoef =-18;
		$texto = "No Suministra Información";
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<link rel="stylesheet" href="assets/plugins/WinPicker/dist/wickedpicker.min.css">
<script type="text/javascript" src="assets/plugins/WinPicker/dist/wickedpicker.min.js"></script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56148" id="LblG2826_C56148">NUMEROID</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56148" name="G2826_C56148"  placeholder="NUMEROID">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56149" id="LblG2826_C56149">CODIGO_ESTUDIANTE</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56149" name="G2826_C56149"  placeholder="CODIGO_ESTUDIANTE">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56530" id="LblG2826_C56530">NUMERO DE CEDULA</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56530" name="G2826_C56530"  placeholder="NUMERO DE CEDULA">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56150" id="LblG2826_C56150">APELLIDOS</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56150" name="G2826_C56150"  placeholder="APELLIDOS">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56151" id="LblG2826_C56151">NOMBRES</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56151" name="G2826_C56151"  placeholder="NOMBRES">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56152" id="LblG2826_C56152">CORREO_UEES</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56152" name="G2826_C56152"  placeholder="CORREO_UEES">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56153" id="LblG2826_C56153">CORREO</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56153" name="G2826_C56153"  placeholder="CORREO">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56157" id="LblG2826_C56157">ASESOR</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56157" name="G2826_C56157"  placeholder="ASESOR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56158" id="LblG2826_C56158">ETIQUETA</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56158" name="G2826_C56158"  placeholder="ETIQUETA">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56154" id="LblG2826_C56154">CELULAR</label>
					    <input type="text" class="form-control input-sm" id="G2826_C56154" name="G2826_C56154"  placeholder="CELULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->

				<!-- CAMPO DE TIPO LISTA -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2826_C56155" id="LblG2826_C56155">MAESTRIA_DE_INTERES</label>
					    <select class="form-control input-sm"  name="G2826_C56155" id="G2826_C56155">
					        <option value="0">Seleccione</option>
					        <?php
					            /*
					                SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
					            */
					            $str_Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3390";

					            $obj = $mysqli->query($str_Lsql);
					            while($obje = $obj->fetch_object()){
					                echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

					            }    
					            
					        ?>
					    </select>
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO LISTA -->
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button"><?php echo $texto; ?></button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i>Buscar en toda la base de datos</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>
<input type="hidden" id="id_gestion_cbx" value="<?php echo $_GET["id_gestion_cbx"];?>">
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>');
<?php 
    $id_campan='';
    if(isset($_GET['id_campana_crm']) && $_GET['id_campana_crm'] != '0'){
        $id_campan='&id_campana_crm='.$_GET['id_campana_crm'];
    } 
?>

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }


    // Listen to message from child window
    bindEvent(window, 'message', function (e) {
        if(Array.isArray(e.data)){
            console.log(e.data);
            var keys=Object.keys(e.data[0].camposivr);
            var key=0;
            $.each(e.data[0].camposivr, function(i, valor){
                if($("#"+keys[key]).length > 0){
                    $("#"+keys[key]).val(valor); 
                }
                key++;
            });
            buscarRegistros(e.data);
//          $("#btnBuscar").click();
        }
        
        if(typeof(e.data)== 'object'){
            switch (e.data["accion"]){
                case "llamadaDesdeG" :
                    parent.postMessage(e.data, '*');
                    break;
                case "cerrargestion":
                    var origen="formulario"
                    finalizarGestion(e.data[datos],origen);
                    break;
            }
        }
    });
    
	$(function(){
    
        //DATEPICKER
        
        
        //TIMEPICKER
        
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
        ObjDataGestion.usuario        = '<?php if(isset($_GET['usuario'])) { echo $_GET['usuario']; } else{ echo '0'; }?>';
            $("#btnBuscar").click(function(){
            var valido=0;
            
            
            if(valido==0){
                var datos = $("#formId").serialize();
                $.ajax({
                    url     	: 'formularios/G2826/G2826_Funciones_Busqueda_Manual.php?action=GET_DATOS<?=$id_campan?>&agente=<?=$_GET["usuario"]?>',
                    type		: 'post',
                    dataType	: 'json',
                    data		: datos,
                    success 	: function(datosq){
                        if(datosq[0].cantidad_registros > 1){
                            buscarRegistros(datosq);
                        }else if(datosq[0].cantidad_registros == 1){
                            $("#buscador").hide();
                            $("#botones").hide();
                            $("#resulados").hide();
                            var id = datosq[0].registros[0].G2826_ConsInte__b;
                            if(datosq[0].registros[0].id_muestra == null){
                                $.ajax({
                                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                    type		: 'post',
                                    data        : {id:id},
                                    success 	: function(data){
                                        <?php if(isset($_GET['token'])) { ?>
                                            guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                            $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>');
                                        <?php } ?>
                                    }
                                });
                            }else{
                                <?php if(isset($_GET['token'])) { ?>
                                    guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                    $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>');
                                <?php } ?>                        
                            }
                        }else{
                            adicionarRegistro(2,datosq[0].mensaje);
                        }
                    }
                });
            }
		});

		$("\#btnCancelar").click(function(){
            borrarStorage('<?php echo $_GET["id_gestion_cbx"]?>');
            $.ajax({
                url     	: 'formularios/generados/PHP_Cerrar_Cancelar.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; } ?>&token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php echo "&idMonoef=".$idMonoef;?><?php echo "&tiempoInicio=".date("Y-m-d H:i:s");?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>',
                type		: 'post',
                dataType	: 'json',
                success 	: function(data){
                    var origen="bqmanual";
                    finalizarGestion(data,origen);    
                }
            });    
		});
	});

    function buscarRegistros(datosq){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
        ObjDataGestion.usuario        = '<?php if(isset($_GET['usuario'])) { echo $_GET['usuario']; } else{ echo '0'; }?>';
        var valores = null;
        var tabla_a_mostrar = '<div class="box box-default">'+
        '<div class="box-header">'+
            '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>'+
        '</div>'+
        '<div class="box-body">'+
            '<table class="table table-hover table-bordered" style="width:100%;">';
        tabla_a_mostrar += '<thead>';
        tabla_a_mostrar += '<tr>';
        tabla_a_mostrar += ' <th>NUMEROID</th><th>CODIGO_ESTUDIANTE</th><th>NUMERO DE CEDULA</th><th>APELLIDOS</th><th>NOMBRES</th><th>CORREO_UEES</th><th>CORREO</th><th>ASESOR</th><th>ETIQUETA</th><th>CELULAR</th><th>MAESTRIA_DE_INTERES</th> ';
        tabla_a_mostrar += '</tr>';
        tabla_a_mostrar += '</thead>';
        tabla_a_mostrar += '<tbody>';
        $.each(datosq[0].registros, function(i, item) {
            tabla_a_mostrar += '<tr idMuestra="'+ item.id_muestra +'" ConsInte="'+ item.G2826_ConsInte__b +'" class="EditRegistro">';
            tabla_a_mostrar += '<td>'+ item.G2826_C56148 +'</td><td>'+ item.G2826_C56149 +'</td><td>'+ item.G2826_C56530 +'</td><td>'+ item.G2826_C56150 +'</td><td>'+ item.G2826_C56151 +'</td><td>'+ item.G2826_C56152 +'</td><td>'+ item.G2826_C56153 +'</td><td>'+ item.G2826_C56157 +'</td><td>'+ item.G2826_C56158 +'</td><td>'+ item.G2826_C56154 +'</td><td>'+ item.G2826_C56155 +'</td>';
            tabla_a_mostrar += '</tr>';
        });
        tabla_a_mostrar += '</tbody>';
        tabla_a_mostrar += '</table></div></div>';

        $("#resultadosBusqueda").html(tabla_a_mostrar);

        $(".EditRegistro").dblclick(function(){
            var id = $(this).attr("ConsInte");
            var muestra=$(this).attr("idMuestra");
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: 'Esta seguro de editar este registro?',
                type: "warning",
                confirmButtonText: "Editar registro",
                cancelButtonText : "No Editar registro",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        $("#buscador").hide();
                        $("#botones").hide();
                        $("#resulados").hide();
                        if(muestra == 'null'){
                            $.ajax({
                                url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                type		: 'post',
                                data        : {id:id},
                                success 	: function(data){
                                    <?php if(isset($_GET['token'])) { ?>
                                        guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>');
                                    <?php } ?>
                                }
                            });
                        }else{
                            <?php if(isset($_GET['token'])) { ?>
                                guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>');
                            <?php } ?>                        
                        }
                    }else{
                        $("#buscador").show();
                        $("#botones").show();
                        $("#resulados").show();
                    }
                });
            });    
    }
    
	function adicionarRegistro(tipo,mensaje){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
        ObjDataGestion.usuario        = '<?php if(isset($_GET['sentido'])) { echo $_GET['usuario']; } else{ echo '0'; }?>';
    
        if(mensaje == null ){
            if(tipo == "1"){
                mensaje = "Desea adicionar un registro";
            }
            if(tipo == "2"){
                mensaje = "No se encontraron datos, desea adicionar un registro";
            }
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: mensaje,
                type: "warning",
                 confirmButtonText: "Adicionar registro",
                cancelButtonText : "Hacer otra busqueda",
                showCancelButton : true,
                closeOnConfirm : true
            },
            function(isconfirm){
                $("#buscador").hide();
                $("#botones").hide();
                $("#resulados").hide();
                if(isconfirm){
                    //JDBD - obtenemos los valores de la busqueda manual para enviarselos al formulario
                    var G2828_C56194 = $("#G2826_C56148").val();
			var G2828_C56195 = $("#G2826_C56149").val();
			var G2828_C56197 = $("#G2826_C56150").val();
			var G2828_C56196 = $("#G2826_C56151").val();
			var G2828_C56198 = $("#G2826_C56152").val();
			var G2828_C56199 = $("#G2826_C56153").val();
			var G2828_C56203 = $("#G2826_C56157").val();
			var G2828_C56204 = $("#G2826_C56158").val();
			var G2828_C56200 = $("#G2826_C56154").val();
			var G2828_C56201 = $("#G2826_C56155").val();
                    $.ajax({
                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                    type		: 'post',
                    dataType	: 'json',
                    success 	: function(numeroIdnuevo){
                        <?php if(isset($_GET['token'])){ ?>
                        guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',numeroIdnuevo,ObjDataGestion);
                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>&nuevoregistro=true&G2828_C56194='+G2828_C56194+'&G2828_C56195='+G2828_C56195+'&G2828_C56197='+G2828_C56197+'&G2828_C56196='+G2828_C56196+'&G2828_C56198='+G2828_C56198+'&G2828_C56199='+G2828_C56199+'&G2828_C56203='+G2828_C56203+'&G2828_C56204='+G2828_C56204+'&G2828_C56200='+G2828_C56200+'&G2828_C56201='+G2828_C56201);
                        <?php } ?>
                    }
                });
                }else{
                    limpiar();
                }
            });
        }else{
            swal("", mensaje, "warning");
        }
	}
	function limpiar(){
		$("#buscador").show();
		$("#buscador :input").each(function(){
			$(this).val("");
		});
		$("#resultadosBusqueda").html("");
		$("#botones").show();
		$("#resulados").show();
	}
</script>
