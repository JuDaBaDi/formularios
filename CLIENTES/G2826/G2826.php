
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2826/G2826_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2826_ConsInte__b as id, G2826_C56150 as camp2 , G2826_C56151 as camp1 FROM ".$BaseDatos.".G2826  WHERE G2826_Usuario = ".$idUsuario." ORDER BY G2826_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2826_ConsInte__b as id, G2826_C56150 as camp2 , G2826_C56151 as camp1 FROM ".$BaseDatos.".G2826  ORDER BY G2826_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2826_ConsInte__b as id, G2826_C56150 as camp2 , G2826_C56151 as camp1 FROM ".$BaseDatos.".G2826 JOIN ".$BaseDatos.".G2826_M".$resultEstpas->muestr." ON G2826_ConsInte__b = G2826_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2826_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2826_ConsInte__b as id, G2826_C56150 as camp2 , G2826_C56151 as camp1 FROM ".$BaseDatos.".G2826 JOIN ".$BaseDatos.".G2826_M".$resultEstpas->muestr." ON G2826_ConsInte__b = G2826_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2826_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2826_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2826_ConsInte__b as id, G2826_C56150 as camp2 , G2826_C56151 as camp1 FROM ".$BaseDatos.".G2826  ORDER BY G2826_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="8513" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8513c">
                1. GENERAL
            </a>
        </h4>
        
    </div>
    <div id="s_8513c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56147" id="LblG2826_C56147">TIPOID</label><input type="text" class="form-control input-sm" id="G2826_C56147" value="<?php if (isset($_GET['G2826_C56147'])) {
                            echo $_GET['G2826_C56147'];
                        } ?>"  name="G2826_C56147"  placeholder="TIPOID"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56148" id="LblG2826_C56148">NUMEROID</label><input type="text" class="form-control input-sm" id="G2826_C56148" value="<?php if (isset($_GET['G2826_C56148'])) {
                            echo $_GET['G2826_C56148'];
                        } ?>"  name="G2826_C56148"  placeholder="NUMEROID"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56149" id="LblG2826_C56149">CODIGO_ESTUDIANTE</label><input type="text" class="form-control input-sm" id="G2826_C56149" value="<?php if (isset($_GET['G2826_C56149'])) {
                            echo $_GET['G2826_C56149'];
                        } ?>"  name="G2826_C56149"  placeholder="CODIGO_ESTUDIANTE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56530" id="LblG2826_C56530">NUMERO DE CEDULA</label><input type="text" class="form-control input-sm" id="G2826_C56530" value="<?php if (isset($_GET['G2826_C56530'])) {
                            echo $_GET['G2826_C56530'];
                        } ?>"  name="G2826_C56530"  placeholder="NUMERO DE CEDULA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56150" id="LblG2826_C56150">APELLIDOS</label><input type="text" class="form-control input-sm" id="G2826_C56150" value="<?php if (isset($_GET['G2826_C56150'])) {
                            echo $_GET['G2826_C56150'];
                        } ?>"  name="G2826_C56150"  placeholder="APELLIDOS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56531" id="LblG2826_C56531">SEGUNDO APELLIDO</label><input type="text" class="form-control input-sm" id="G2826_C56531" value="<?php if (isset($_GET['G2826_C56531'])) {
                            echo $_GET['G2826_C56531'];
                        } ?>"  name="G2826_C56531"  placeholder="SEGUNDO APELLIDO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56151" id="LblG2826_C56151">NOMBRES</label><input type="text" class="form-control input-sm" id="G2826_C56151" value="<?php if (isset($_GET['G2826_C56151'])) {
                            echo $_GET['G2826_C56151'];
                        } ?>"  name="G2826_C56151"  placeholder="NOMBRES"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56152" id="LblG2826_C56152">CORREO_UEES</label><input type="text" class="form-control input-sm" id="G2826_C56152" value="<?php if (isset($_GET['G2826_C56152'])) {
                            echo $_GET['G2826_C56152'];
                        } ?>"  name="G2826_C56152"  placeholder="CORREO_UEES"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56153" id="LblG2826_C56153">CORREO</label><input type="text" class="form-control input-sm" id="G2826_C56153" value="<?php if (isset($_GET['G2826_C56153'])) {
                            echo $_GET['G2826_C56153'];
                        } ?>"  name="G2826_C56153"  placeholder="CORREO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56157" id="LblG2826_C56157">ASESOR</label><input type="text" class="form-control input-sm" id="G2826_C56157" value="<?php if (isset($_GET['G2826_C56157'])) {
                            echo $_GET['G2826_C56157'];
                        } ?>"  name="G2826_C56157"  placeholder="ASESOR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56158" id="LblG2826_C56158">ETIQUETA</label><input type="text" class="form-control input-sm" id="G2826_C56158" value="<?php if (isset($_GET['G2826_C56158'])) {
                            echo $_GET['G2826_C56158'];
                        } ?>"  name="G2826_C56158"  placeholder="ETIQUETA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56536" id="LblG2826_C56536">TELEFONO FIJO</label><input type="text" class="form-control input-sm" id="G2826_C56536" value="<?php if (isset($_GET['G2826_C56536'])) {
                            echo $_GET['G2826_C56536'];
                        } ?>"  name="G2826_C56536"  placeholder="TELEFONO FIJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56154" id="LblG2826_C56154">CELULAR</label><input type="text" class="form-control input-sm" id="G2826_C56154" value="<?php if (isset($_GET['G2826_C56154'])) {
                            echo $_GET['G2826_C56154'];
                        } ?>"  name="G2826_C56154"  placeholder="CELULAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56155" id="LblG2826_C56155">MAESTRIA_DE_INTERES</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56155" id="G2826_C56155">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3390 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56532" id="LblG2826_C56532">NACIONALIDAD</label><input type="text" class="form-control input-sm" id="G2826_C56532" value="<?php if (isset($_GET['G2826_C56532'])) {
                            echo $_GET['G2826_C56532'];
                        } ?>"  name="G2826_C56532"  placeholder="NACIONALIDAD"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56534" id="LblG2826_C56534">FECHA DE NACIMIENTO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56534'])) {
                            echo $_GET['G2826_C56534'];
                        } ?>"  name="G2826_C56534" id="G2826_C56534" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56996" id="LblG2826_C56996">LUGAR DE NACIMIENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56996" id="G2826_C56996">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3404 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56997" id="LblG2826_C56997">TIPO DE SANGRE</label><input type="text" class="form-control input-sm" id="G2826_C56997" value="<?php if (isset($_GET['G2826_C56997'])) {
                            echo $_GET['G2826_C56997'];
                        } ?>"  name="G2826_C56997"  placeholder="TIPO DE SANGRE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56533" id="LblG2826_C56533">SEXO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56533" id="G2826_C56533">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3405 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56535" id="LblG2826_C56535">ESTADO CIVIL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56535" id="G2826_C56535">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3403 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56537" id="LblG2826_C56537">PROVINCIA/CANTON DOMICILIO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56537" id="G2826_C56537">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3407 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56538" id="LblG2826_C56538">DIRECCION DOMICILIO</label><input type="text" class="form-control input-sm" id="G2826_C56538" value="<?php if (isset($_GET['G2826_C56538'])) {
                            echo $_GET['G2826_C56538'];
                        } ?>"  name="G2826_C56538"  placeholder="DIRECCION DOMICILIO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56539" id="LblG2826_C56539">PROVINCIA / CANTON TRABAJO</label><input type="text" class="form-control input-sm" id="G2826_C56539" value="<?php if (isset($_GET['G2826_C56539'])) {
                            echo $_GET['G2826_C56539'];
                        } ?>"  name="G2826_C56539"  placeholder="PROVINCIA / CANTON TRABAJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56540" id="LblG2826_C56540">DIRECCION TRABAJO</label><input type="text" class="form-control input-sm" id="G2826_C56540" value="<?php if (isset($_GET['G2826_C56540'])) {
                            echo $_GET['G2826_C56540'];
                        } ?>"  name="G2826_C56540"  placeholder="DIRECCION TRABAJO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56156" id="LblG2826_C56156">COMENTARIO</label>
                        <textarea class="form-control input-sm" name="G2826_C56156" id="G2826_C56156"  value="<?php if (isset($_GET['G2826_C56156'])) {
                            echo $_GET['G2826_C56156'];
                        } ?>" placeholder="COMENTARIO"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="8514" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56145" id="LblG2826_C56145">ORIGEN_DY_WF</label><input type="text" class="form-control input-sm" id="G2826_C56145" value="<?php if (isset($_GET['G2826_C56145'])) {
                            echo $_GET['G2826_C56145'];
                        } ?>" readonly name="G2826_C56145"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56146" id="LblG2826_C56146">OPTIN_DY_WF</label><input type="text" class="form-control input-sm" id="G2826_C56146" value="<?php if (isset($_GET['G2826_C56146'])) {
                            echo $_GET['G2826_C56146'];
                        } ?>" readonly name="G2826_C56146"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="8563" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8563c">
                2. DATOS DE LA FAMILIA - PRIMER FAMILIAR
            </a>
        </h4>
        
    </div>
    <div id="s_8563c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56541" id="LblG2826_C56541">NOMBRES 1er Familiar</label><input type="text" class="form-control input-sm" id="G2826_C56541" value="<?php if (isset($_GET['G2826_C56541'])) {
                            echo $_GET['G2826_C56541'];
                        } ?>"  name="G2826_C56541"  placeholder="NOMBRES 1er Familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56542" id="LblG2826_C56542">APELLIDOS 1er Familiar</label><input type="text" class="form-control input-sm" id="G2826_C56542" value="<?php if (isset($_GET['G2826_C56542'])) {
                            echo $_GET['G2826_C56542'];
                        } ?>"  name="G2826_C56542"  placeholder="APELLIDOS 1er Familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56543" id="LblG2826_C56543">FECHA DE NACIMIENTO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56543'])) {
                            echo $_GET['G2826_C56543'];
                        } ?>"  name="G2826_C56543" id="G2826_C56543" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56544" id="LblG2826_C56544">RELACIÓN</label><input type="text" class="form-control input-sm" id="G2826_C56544" value="<?php if (isset($_GET['G2826_C56544'])) {
                            echo $_GET['G2826_C56544'];
                        } ?>"  name="G2826_C56544"  placeholder="RELACIÓN"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8564" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8564c">
                3. DATOS DE FAMILIA - SEGUNDO FAMILIAR
            </a>
        </h4>
        
    </div>
    <div id="s_8564c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56545" id="LblG2826_C56545">NOMBRES 2do Familiar</label><input type="text" class="form-control input-sm" id="G2826_C56545" value="<?php if (isset($_GET['G2826_C56545'])) {
                            echo $_GET['G2826_C56545'];
                        } ?>"  name="G2826_C56545"  placeholder="NOMBRES 2do Familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56546" id="LblG2826_C56546">APELLIDOS 2do Familiar</label><input type="text" class="form-control input-sm" id="G2826_C56546" value="<?php if (isset($_GET['G2826_C56546'])) {
                            echo $_GET['G2826_C56546'];
                        } ?>"  name="G2826_C56546"  placeholder="APELLIDOS 2do Familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56547" id="LblG2826_C56547">FECHA DE NACIMIENTO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56547'])) {
                            echo $_GET['G2826_C56547'];
                        } ?>"  name="G2826_C56547" id="G2826_C56547" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56548" id="LblG2826_C56548">RELACIÓN</label><input type="text" class="form-control input-sm" id="G2826_C56548" value="<?php if (isset($_GET['G2826_C56548'])) {
                            echo $_GET['G2826_C56548'];
                        } ?>"  name="G2826_C56548"  placeholder="RELACIÓN"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8565" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8565c">
                4. DATOS DE FAMILIA - TERCER FAMILIAR
            </a>
        </h4>
        
    </div>
    <div id="s_8565c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56549" id="LblG2826_C56549">NOMBRES 3er Familiar</label><input type="text" class="form-control input-sm" id="G2826_C56549" value="<?php if (isset($_GET['G2826_C56549'])) {
                            echo $_GET['G2826_C56549'];
                        } ?>"  name="G2826_C56549"  placeholder="NOMBRES 3er Familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56550" id="LblG2826_C56550">APELLIDOS 3er Familiar</label><input type="text" class="form-control input-sm" id="G2826_C56550" value="<?php if (isset($_GET['G2826_C56550'])) {
                            echo $_GET['G2826_C56550'];
                        } ?>"  name="G2826_C56550"  placeholder="APELLIDOS 3er Familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56551" id="LblG2826_C56551">FECHA DE NACIMIENTO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56551'])) {
                            echo $_GET['G2826_C56551'];
                        } ?>"  name="G2826_C56551" id="G2826_C56551" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56552" id="LblG2826_C56552">RELACIÓN</label><input type="text" class="form-control input-sm" id="G2826_C56552" value="<?php if (isset($_GET['G2826_C56552'])) {
                            echo $_GET['G2826_C56552'];
                        } ?>"  name="G2826_C56552"  placeholder="RELACIÓN"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8566" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8566c">
                5. ESTUDIOS PRIMARIOS
            </a>
        </h4>
        
    </div>
    <div id="s_8566c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56553" id="LblG2826_C56553">INSTITUCIÓN Est. Primarios</label><input type="text" class="form-control input-sm" id="G2826_C56553" value="<?php if (isset($_GET['G2826_C56553'])) {
                            echo $_GET['G2826_C56553'];
                        } ?>"  name="G2826_C56553"  placeholder="INSTITUCIÓN Est. Primarios"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56554" id="LblG2826_C56554">PROVINCIA / CANTON</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56554" id="G2826_C56554">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3407 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56555" id="LblG2826_C56555">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56555'])) {
                            echo $_GET['G2826_C56555'];
                        } ?>"  name="G2826_C56555" id="G2826_C56555" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56556" id="LblG2826_C56556">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56556'])) {
                            echo $_GET['G2826_C56556'];
                        } ?>"  name="G2826_C56556" id="G2826_C56556" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8567" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8567c">
                6. ESTUDIOS SECUNDARIOS
            </a>
        </h4>
        
    </div>
    <div id="s_8567c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56557" id="LblG2826_C56557">INSTITUCIÓN Est. Secundarios</label><input type="text" class="form-control input-sm" id="G2826_C56557" value="<?php if (isset($_GET['G2826_C56557'])) {
                            echo $_GET['G2826_C56557'];
                        } ?>"  name="G2826_C56557"  placeholder="INSTITUCIÓN Est. Secundarios"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56558" id="LblG2826_C56558">PROVINCIA / CANTON</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56558" id="G2826_C56558">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3407 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56559" id="LblG2826_C56559">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56559'])) {
                            echo $_GET['G2826_C56559'];
                        } ?>"  name="G2826_C56559" id="G2826_C56559" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56560" id="LblG2826_C56560">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56560'])) {
                            echo $_GET['G2826_C56560'];
                        } ?>"  name="G2826_C56560" id="G2826_C56560" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8568" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8568c">
                7. ESTUDIOS SUPERIORES
            </a>
        </h4>
        
    </div>
    <div id="s_8568c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56561" id="LblG2826_C56561">INSTITUCIÓN Est. Superiores</label><input type="text" class="form-control input-sm" id="G2826_C56561" value="<?php if (isset($_GET['G2826_C56561'])) {
                            echo $_GET['G2826_C56561'];
                        } ?>"  name="G2826_C56561"  placeholder="INSTITUCIÓN Est. Superiores"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56562" id="LblG2826_C56562">CARRERA</label><input type="text" class="form-control input-sm" id="G2826_C56562" value="<?php if (isset($_GET['G2826_C56562'])) {
                            echo $_GET['G2826_C56562'];
                        } ?>"  name="G2826_C56562"  placeholder="CARRERA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56563" id="LblG2826_C56563">LUGAR</label><input type="text" class="form-control input-sm" id="G2826_C56563" value="<?php if (isset($_GET['G2826_C56563'])) {
                            echo $_GET['G2826_C56563'];
                        } ?>"  name="G2826_C56563"  placeholder="LUGAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56564" id="LblG2826_C56564">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56564'])) {
                            echo $_GET['G2826_C56564'];
                        } ?>"  name="G2826_C56564" id="G2826_C56564" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56565" id="LblG2826_C56565">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56565'])) {
                            echo $_GET['G2826_C56565'];
                        } ?>"  name="G2826_C56565" id="G2826_C56565" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8569" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8569c">
                8. ESTUDIO POSGRADO
            </a>
        </h4>
        
    </div>
    <div id="s_8569c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56566" id="LblG2826_C56566">INSTITUCIÓN Posgrado</label><input type="text" class="form-control input-sm" id="G2826_C56566" value="<?php if (isset($_GET['G2826_C56566'])) {
                            echo $_GET['G2826_C56566'];
                        } ?>"  name="G2826_C56566"  placeholder="INSTITUCIÓN Posgrado"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56567" id="LblG2826_C56567">PROGRAMA</label><input type="text" class="form-control input-sm" id="G2826_C56567" value="<?php if (isset($_GET['G2826_C56567'])) {
                            echo $_GET['G2826_C56567'];
                        } ?>"  name="G2826_C56567"  placeholder="PROGRAMA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56568" id="LblG2826_C56568">LUGAR</label><input type="text" class="form-control input-sm" id="G2826_C56568" value="<?php if (isset($_GET['G2826_C56568'])) {
                            echo $_GET['G2826_C56568'];
                        } ?>"  name="G2826_C56568"  placeholder="LUGAR"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56569" id="LblG2826_C56569">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56569'])) {
                            echo $_GET['G2826_C56569'];
                        } ?>"  name="G2826_C56569" id="G2826_C56569" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56570" id="LblG2826_C56570">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56570'])) {
                            echo $_GET['G2826_C56570'];
                        } ?>"  name="G2826_C56570" id="G2826_C56570" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8570" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8570c">
                9. EXPERIENCIA LABORAL - ACTUAL
            </a>
        </h4>
        
    </div>
    <div id="s_8570c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56571" id="LblG2826_C56571">EMPRESA / INSTITUCION Exp. Laboral Actual</label><input type="text" class="form-control input-sm" id="G2826_C56571" value="<?php if (isset($_GET['G2826_C56571'])) {
                            echo $_GET['G2826_C56571'];
                        } ?>"  name="G2826_C56571"  placeholder="EMPRESA / INSTITUCION Exp. Laboral Actual"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56572" id="LblG2826_C56572">CARGO</label><input type="text" class="form-control input-sm" id="G2826_C56572" value="<?php if (isset($_GET['G2826_C56572'])) {
                            echo $_GET['G2826_C56572'];
                        } ?>"  name="G2826_C56572"  placeholder="CARGO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56573" id="LblG2826_C56573">FECHA DE INICIO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56573'])) {
                            echo $_GET['G2826_C56573'];
                        } ?>"  name="G2826_C56573" id="G2826_C56573" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO DECIMAL -->
                    <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56574" id="LblG2826_C56574">SUELDO</label>
                        <input type="text" class="form-control input-sm Decimal" value="<?php if (isset($_GET['G2826_C56574'])) {
                            echo $_GET['G2826_C56574'];
                        } ?>"  name="G2826_C56574" id="G2826_C56574" placeholder="SUELDO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8571" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8571c">
                10. EXPERIENCIA LABORAL - ANTERIOR
            </a>
        </h4>
        
    </div>
    <div id="s_8571c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56575" id="LblG2826_C56575">EMPRESA / INSTITUCIÓN Exp. Laboral Anterior</label><input type="text" class="form-control input-sm" id="G2826_C56575" value="<?php if (isset($_GET['G2826_C56575'])) {
                            echo $_GET['G2826_C56575'];
                        } ?>"  name="G2826_C56575"  placeholder="EMPRESA / INSTITUCIÓN Exp. Laboral Anterior"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56576" id="LblG2826_C56576">CARGO</label><input type="text" class="form-control input-sm" id="G2826_C56576" value="<?php if (isset($_GET['G2826_C56576'])) {
                            echo $_GET['G2826_C56576'];
                        } ?>"  name="G2826_C56576"  placeholder="CARGO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56577" id="LblG2826_C56577">FECHA DE INICIO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56577'])) {
                            echo $_GET['G2826_C56577'];
                        } ?>"  name="G2826_C56577" id="G2826_C56577" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56578" id="LblG2826_C56578">FECHA FINAL</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56578'])) {
                            echo $_GET['G2826_C56578'];
                        } ?>"  name="G2826_C56578" id="G2826_C56578" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO DECIMAL -->
                    <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56579" id="LblG2826_C56579">SUELDO</label>
                        <input type="text" class="form-control input-sm Decimal" value="<?php if (isset($_GET['G2826_C56579'])) {
                            echo $_GET['G2826_C56579'];
                        } ?>"  name="G2826_C56579" id="G2826_C56579" placeholder="SUELDO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8572" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8572c">
                11. EXPERIENCIA DOCENTE - ACTUAL
            </a>
        </h4>
        
    </div>
    <div id="s_8572c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56580" id="LblG2826_C56580">INSTITUCIÓN Exp. Docente Actual</label><input type="text" class="form-control input-sm" id="G2826_C56580" value="<?php if (isset($_GET['G2826_C56580'])) {
                            echo $_GET['G2826_C56580'];
                        } ?>"  name="G2826_C56580"  placeholder="INSTITUCIÓN Exp. Docente Actual"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56581" id="LblG2826_C56581">FUNCIONES</label>
                        <textarea class="form-control input-sm" name="G2826_C56581" id="G2826_C56581"  value="<?php if (isset($_GET['G2826_C56581'])) {
                            echo $_GET['G2826_C56581'];
                        } ?>" placeholder="FUNCIONES"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56582" id="LblG2826_C56582">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56582'])) {
                            echo $_GET['G2826_C56582'];
                        } ?>"  name="G2826_C56582" id="G2826_C56582" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56583" id="LblG2826_C56583">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56583'])) {
                            echo $_GET['G2826_C56583'];
                        } ?>"  name="G2826_C56583" id="G2826_C56583" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8573" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8573c">
                12. EXPERIENCIA DOCENTE - ANTERIOR
            </a>
        </h4>
        
    </div>
    <div id="s_8573c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56584" id="LblG2826_C56584">INSTITUCIÓN Exp. Docente Anterior</label><input type="text" class="form-control input-sm" id="G2826_C56584" value="<?php if (isset($_GET['G2826_C56584'])) {
                            echo $_GET['G2826_C56584'];
                        } ?>"  name="G2826_C56584"  placeholder="INSTITUCIÓN Exp. Docente Anterior"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56585" id="LblG2826_C56585">FUNCIONES</label><input type="text" class="form-control input-sm" id="G2826_C56585" value="<?php if (isset($_GET['G2826_C56585'])) {
                            echo $_GET['G2826_C56585'];
                        } ?>"  name="G2826_C56585"  placeholder="FUNCIONES"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56586" id="LblG2826_C56586">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56586'])) {
                            echo $_GET['G2826_C56586'];
                        } ?>"  name="G2826_C56586" id="G2826_C56586" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56587" id="LblG2826_C56587">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56587'])) {
                            echo $_GET['G2826_C56587'];
                        } ?>"  name="G2826_C56587" id="G2826_C56587" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8574" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8574c">
                13. EXPERIENCIA EN INVESTIGACIÓN
            </a>
        </h4>
        
    </div>
    <div id="s_8574c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C56588" id="LblG2826_C56588">INSTITUCIÓN Exp. Investigación</label><input type="text" class="form-control input-sm" id="G2826_C56588" value="<?php if (isset($_GET['G2826_C56588'])) {
                            echo $_GET['G2826_C56588'];
                        } ?>"  name="G2826_C56588"  placeholder="INSTITUCIÓN Exp. Investigación"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56589" id="LblG2826_C56589">INVESTIGACIONES REALIZADAS</label>
                        <textarea class="form-control input-sm" name="G2826_C56589" id="G2826_C56589"  value="<?php if (isset($_GET['G2826_C56589'])) {
                            echo $_GET['G2826_C56589'];
                        } ?>" placeholder="INVESTIGACIONES REALIZADAS"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56590" id="LblG2826_C56590">FUNCIONES</label>
                        <textarea class="form-control input-sm" name="G2826_C56590" id="G2826_C56590"  value="<?php if (isset($_GET['G2826_C56590'])) {
                            echo $_GET['G2826_C56590'];
                        } ?>" placeholder="FUNCIONES"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56591" id="LblG2826_C56591">DESDE</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56591'])) {
                            echo $_GET['G2826_C56591'];
                        } ?>"  name="G2826_C56591" id="G2826_C56591" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2826_C56592" id="LblG2826_C56592">HASTA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2826_C56592'])) {
                            echo $_GET['G2826_C56592'];
                        } ?>"  name="G2826_C56592" id="G2826_C56592" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8575" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8575c">
                14. MANEJO DE IDIOMAS
            </a>
        </h4>
        
    </div>
    <div id="s_8575c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56593" id="LblG2826_C56593">ESPAÑOL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56593" id="G2826_C56593">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56594" id="LblG2826_C56594">INGLÉS</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56594" id="G2826_C56594">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56595" id="LblG2826_C56595">FRANCÉS</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56595" id="G2826_C56595">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56596" id="LblG2826_C56596">QUICHUA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56596" id="G2826_C56596">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56597" id="LblG2826_C56597">Otros Idiomas, especifique el nombre y nivel de dominio</label>
                        <textarea class="form-control input-sm" name="G2826_C56597" id="G2826_C56597"  value="<?php if (isset($_GET['G2826_C56597'])) {
                            echo $_GET['G2826_C56597'];
                        } ?>" placeholder="Otros Idiomas, especifique el nombre y nivel de dominio"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8576" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8576c">
                15. TECNOLOGÍA
            </a>
        </h4>
        
    </div>
    <div id="s_8576c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56598" id="LblG2826_C56598">COMPUTADORA PERSONAL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56598" id="G2826_C56598">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56599" id="LblG2826_C56599">COMPUTADORA PORTÁTIL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56599" id="G2826_C56599">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56600" id="LblG2826_C56600">TABLET</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56600" id="G2826_C56600">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56601" id="LblG2826_C56601">PDA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56601" id="G2826_C56601">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56602" id="LblG2826_C56602">SMARTPHONE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56602" id="G2826_C56602">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56603" id="LblG2826_C56603">ACCESO A INTERNET</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56603" id="G2826_C56603">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56604" id="LblG2826_C56604">DOMINIO DE APLICACIONES DE OFICINA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56604" id="G2826_C56604">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3408 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8577" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8577c">
                16. OTROS DATOS
            </a>
        </h4>
        
    </div>
    <div id="s_8577c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56605" id="LblG2826_C56605">INFORMACIÓN ADICIONAL: Proporcione en este espacio información que no fué incluido anteriormente en el presente instrumento y que considere que deba ser considerado como becas, distinciones académicas, membresías institucionales, etc.</label>
                        <textarea class="form-control input-sm" name="G2826_C56605" id="G2826_C56605"  value="<?php if (isset($_GET['G2826_C56605'])) {
                            echo $_GET['G2826_C56605'];
                        } ?>" placeholder="INFORMACIÓN ADICIONAL: Proporcione en este espacio información que no fué incluido anteriormente en el presente instrumento y que considere que deba ser considerado como becas, distinciones académicas, membresías institucionales, etc."></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2826_C56606" id="LblG2826_C56606">INFORMACIÓN IMPORTANTE: En caso de tener alguna enfermedad que usted considere deba ser del conocimiento del Director del programa por favor escríbalo</label>
                        <textarea class="form-control input-sm" name="G2826_C56606" id="G2826_C56606"  value="<?php if (isset($_GET['G2826_C56606'])) {
                            echo $_GET['G2826_C56606'];
                        } ?>" placeholder="INFORMACIÓN IMPORTANTE: En caso de tener alguna enfermedad que usted considere deba ser del conocimiento del Director del programa por favor escríbalo"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8660" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8660c">
                DATOS PARA LA FACTURA
            </a>
        </h4>
        
    </div>
    <div id="s_8660c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C57272" id="LblG2826_C57272">TIPO DE ID</label><input type="text" class="form-control input-sm" id="G2826_C57272" value="<?php if (isset($_GET['G2826_C57272'])) {
                            echo $_GET['G2826_C57272'];
                        } ?>"  name="G2826_C57272"  placeholder="TIPO DE ID"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C57273" id="LblG2826_C57273">NUMERO DE ID</label><input type="text" class="form-control input-sm" id="G2826_C57273" value="<?php if (isset($_GET['G2826_C57273'])) {
                            echo $_GET['G2826_C57273'];
                        } ?>"  name="G2826_C57273"  placeholder="NUMERO DE ID"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C57274" id="LblG2826_C57274">NOMBRE</label><input type="text" class="form-control input-sm" id="G2826_C57274" value="<?php if (isset($_GET['G2826_C57274'])) {
                            echo $_GET['G2826_C57274'];
                        } ?>"  name="G2826_C57274"  placeholder="NOMBRE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C57275" id="LblG2826_C57275">CORREO</label><input type="text" class="form-control input-sm" id="G2826_C57275" value="<?php if (isset($_GET['G2826_C57275'])) {
                            echo $_GET['G2826_C57275'];
                        } ?>"  name="G2826_C57275"  placeholder="CORREO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8661" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8661c">
                FINANCIACIÓN
            </a>
        </h4>
        
    </div>
    <div id="s_8661c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C57276" id="LblG2826_C57276">TIPO DE FINANCIAMIENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C57276" id="G2826_C57276">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3445 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2826_C57277" id="LblG2826_C57277">SI ELIGIO "OTROS" EXPECIFIQUE</label><input type="text" class="form-control input-sm" id="G2826_C57277" value="<?php if (isset($_GET['G2826_C57277'])) {
                            echo $_GET['G2826_C57277'];
                        } ?>"  name="G2826_C57277"  placeholder="SI ELIGIO "OTROS" EXPECIFIQUE"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8578" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8578c">
                17. DOCUMENTOS
            </a>
        </h4>
        
    </div>
    <div id="s_8578c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56609" id="LblFG2826_C56609">Copia a color de la Cédula de Identidad</label>
                        <input type="file" class="adjuntos" id="FG2826_C56609" name="FG2826_C56609">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56609" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56609" name="G2826_C56609" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56607" id="LblFG2826_C56607">Título de tercer nivel de grado notarizado</label>
                        <input type="file" class="adjuntos" id="FG2826_C56607" name="FG2826_C56607">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56607" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56607" name="G2826_C56607" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56608" id="LblFG2826_C56608">Registro del título en el SENESCYT</label>
                        <input type="file" class="adjuntos" id="FG2826_C56608" name="FG2826_C56608">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56608" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56608" name="G2826_C56608" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56610" id="LblFG2826_C56610">Cartas de referencias personales o profesionales actualizadas</label>
                        <input type="file" class="adjuntos" id="FG2826_C56610" name="FG2826_C56610">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56610" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56610" name="G2826_C56610" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56611" id="LblFG2826_C56611">Hoja de vida actualizada</label>
                        <input type="file" class="adjuntos" id="FG2826_C56611" name="FG2826_C56611">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56611" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56611" name="G2826_C56611" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56612" id="LblFG2826_C56612">Respaldos que sustente la información detallada de la hoja de vida</label>
                        <input type="file" class="adjuntos" id="FG2826_C56612" name="FG2826_C56612">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56612" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56612" name="G2826_C56612" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2826_C56613" id="LblFG2826_C56613">Foto tamaño carnet</label>
                        <input type="file" class="adjuntos" id="FG2826_C56613" name="FG2826_C56613">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2826_C56613" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2826_C56613" name="G2826_C56613" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="8579" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_8579c">
                18. TÉRMINOS Y CONDICIONES
            </a>
        </h4>
        
    </div>
    <div id="s_8579c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2826_C56614" id="LblG2826_C56614">UEES-OnLine se compromete a proteger y respetar su privacidad, y solo usaremos su información personal para administrar su cuenta y proporcionar los productos y servicios que nos solicite. Nos gustaría ponernos en contacto con usted con la finalidad de mostrarle nuestros productos y servicios, así como otros contenidos que puedan interesarle. Si acepta los términos y condiciones de la convocatoria para realizar estudios en Posgrado en la modalidad online de la UEES, seleccione SI en la casilla a continuación. Al seleccionar SI, acepta que UEES-OnLine almacene y procese la información personal suministrada para proporcionarle el contenido solicitado.</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2826_C56614" id="G2826_C56614">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3409 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2826/G2826_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2826_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2826_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2826_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    $(".adjuntos").change(function(){
        var imax = $(this).attr("valor");
        var imagen = this.files[0];

        // if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" && imagen["type"] != "application/pdf"){
        //     $(".NuevaFoto").val(");
        //     swal({
        //         title : "Error al subir el archivo",
        //         text  : "El archivo debe estar en formato PNG , JPG, PDF",
        //         type  : "error",
        //         confirmButtonText : "Cerrar"
        //     });
        // }else 
        if(imagen["size"] > 9000000 ) {
            $(".NuevaFoto").val("");
            $(this).val("");
            swal({
                title : "Error al subir el archivo",
                text  : "El archivo no debe pesar mas de 9 MB",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }
    });
    
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2826_C56147").val(item.G2826_C56147); 
                $("#G2826_C56148").val(item.G2826_C56148); 
                $("#G2826_C56149").val(item.G2826_C56149); 
                $("#G2826_C56530").val(item.G2826_C56530); 
                $("#G2826_C56150").val(item.G2826_C56150); 
                $("#G2826_C56531").val(item.G2826_C56531); 
                $("#G2826_C56151").val(item.G2826_C56151); 
                $("#G2826_C56152").val(item.G2826_C56152); 
                $("#G2826_C56153").val(item.G2826_C56153); 
                $("#G2826_C56157").val(item.G2826_C56157); 
                $("#G2826_C56158").val(item.G2826_C56158); 
                $("#G2826_C56536").val(item.G2826_C56536); 
                $("#G2826_C56154").val(item.G2826_C56154); 
                $("#G2826_C56155").val(item.G2826_C56155).trigger("change");  
                $("#G2826_C56532").val(item.G2826_C56532); 
                $("#G2826_C56534").val(item.G2826_C56534); 
                $("#G2826_C56996").val(item.G2826_C56996).trigger("change");  
                $("#G2826_C56997").val(item.G2826_C56997); 
                $("#G2826_C56533").val(item.G2826_C56533).trigger("change");  
                $("#G2826_C56535").val(item.G2826_C56535).trigger("change");  
                $("#G2826_C56537").val(item.G2826_C56537).trigger("change");  
                $("#G2826_C56538").val(item.G2826_C56538); 
                $("#G2826_C56539").val(item.G2826_C56539); 
                $("#G2826_C56540").val(item.G2826_C56540); 
                $("#G2826_C56156").val(item.G2826_C56156); 
                $("#G2826_C56145").val(item.G2826_C56145); 
                $("#G2826_C56146").val(item.G2826_C56146); 
                $("#G2826_C56541").val(item.G2826_C56541); 
                $("#G2826_C56542").val(item.G2826_C56542); 
                $("#G2826_C56543").val(item.G2826_C56543); 
                $("#G2826_C56544").val(item.G2826_C56544); 
                $("#G2826_C56545").val(item.G2826_C56545); 
                $("#G2826_C56546").val(item.G2826_C56546); 
                $("#G2826_C56547").val(item.G2826_C56547); 
                $("#G2826_C56548").val(item.G2826_C56548); 
                $("#G2826_C56549").val(item.G2826_C56549); 
                $("#G2826_C56550").val(item.G2826_C56550); 
                $("#G2826_C56551").val(item.G2826_C56551); 
                $("#G2826_C56552").val(item.G2826_C56552); 
                $("#G2826_C56553").val(item.G2826_C56553); 
                $("#G2826_C56554").val(item.G2826_C56554).trigger("change");  
                $("#G2826_C56555").val(item.G2826_C56555); 
                $("#G2826_C56556").val(item.G2826_C56556); 
                $("#G2826_C56557").val(item.G2826_C56557); 
                $("#G2826_C56558").val(item.G2826_C56558).trigger("change");  
                $("#G2826_C56559").val(item.G2826_C56559); 
                $("#G2826_C56560").val(item.G2826_C56560); 
                $("#G2826_C56561").val(item.G2826_C56561); 
                $("#G2826_C56562").val(item.G2826_C56562); 
                $("#G2826_C56563").val(item.G2826_C56563); 
                $("#G2826_C56564").val(item.G2826_C56564); 
                $("#G2826_C56565").val(item.G2826_C56565); 
                $("#G2826_C56566").val(item.G2826_C56566); 
                $("#G2826_C56567").val(item.G2826_C56567); 
                $("#G2826_C56568").val(item.G2826_C56568); 
                $("#G2826_C56569").val(item.G2826_C56569); 
                $("#G2826_C56570").val(item.G2826_C56570); 
                $("#G2826_C56571").val(item.G2826_C56571); 
                $("#G2826_C56572").val(item.G2826_C56572); 
                $("#G2826_C56573").val(item.G2826_C56573); 
                $("#G2826_C56574").val(item.G2826_C56574); 
                $("#G2826_C56575").val(item.G2826_C56575); 
                $("#G2826_C56576").val(item.G2826_C56576); 
                $("#G2826_C56577").val(item.G2826_C56577); 
                $("#G2826_C56578").val(item.G2826_C56578); 
                $("#G2826_C56579").val(item.G2826_C56579); 
                $("#G2826_C56580").val(item.G2826_C56580); 
                $("#G2826_C56581").val(item.G2826_C56581); 
                $("#G2826_C56582").val(item.G2826_C56582); 
                $("#G2826_C56583").val(item.G2826_C56583); 
                $("#G2826_C56584").val(item.G2826_C56584); 
                $("#G2826_C56585").val(item.G2826_C56585); 
                $("#G2826_C56586").val(item.G2826_C56586); 
                $("#G2826_C56587").val(item.G2826_C56587); 
                $("#G2826_C56588").val(item.G2826_C56588); 
                $("#G2826_C56589").val(item.G2826_C56589); 
                $("#G2826_C56590").val(item.G2826_C56590); 
                $("#G2826_C56591").val(item.G2826_C56591); 
                $("#G2826_C56592").val(item.G2826_C56592); 
                $("#G2826_C56593").val(item.G2826_C56593).trigger("change");  
                $("#G2826_C56594").val(item.G2826_C56594).trigger("change");  
                $("#G2826_C56595").val(item.G2826_C56595).trigger("change");  
                $("#G2826_C56596").val(item.G2826_C56596).trigger("change");  
                $("#G2826_C56597").val(item.G2826_C56597); 
                $("#G2826_C56598").val(item.G2826_C56598).trigger("change");  
                $("#G2826_C56599").val(item.G2826_C56599).trigger("change");  
                $("#G2826_C56600").val(item.G2826_C56600).trigger("change");  
                $("#G2826_C56601").val(item.G2826_C56601).trigger("change");  
                $("#G2826_C56602").val(item.G2826_C56602).trigger("change");  
                $("#G2826_C56603").val(item.G2826_C56603).trigger("change");  
                $("#G2826_C56604").val(item.G2826_C56604).trigger("change");  
                $("#G2826_C56605").val(item.G2826_C56605); 
                $("#G2826_C56606").val(item.G2826_C56606); 
                if (item.G2826_C56609){
                    $("#G2826_C56609").val(item.G2826_C56609);
                }else{
                    $("#G2826_C56609").val("Sin Adjunto");
                } 
                if (item.G2826_C56607){
                    $("#G2826_C56607").val(item.G2826_C56607);
                }else{
                    $("#G2826_C56607").val("Sin Adjunto");
                } 
                if (item.G2826_C56608){
                    $("#G2826_C56608").val(item.G2826_C56608);
                }else{
                    $("#G2826_C56608").val("Sin Adjunto");
                } 
                if (item.G2826_C56610){
                    $("#G2826_C56610").val(item.G2826_C56610);
                }else{
                    $("#G2826_C56610").val("Sin Adjunto");
                } 
                if (item.G2826_C56611){
                    $("#G2826_C56611").val(item.G2826_C56611);
                }else{
                    $("#G2826_C56611").val("Sin Adjunto");
                } 
                if (item.G2826_C56612){
                    $("#G2826_C56612").val(item.G2826_C56612);
                }else{
                    $("#G2826_C56612").val("Sin Adjunto");
                } 
                if (item.G2826_C56613){
                    $("#G2826_C56613").val(item.G2826_C56613);
                }else{
                    $("#G2826_C56613").val("Sin Adjunto");
                } 
                $("#G2826_C56614").val(item.G2826_C56614).trigger("change");  
                $("#G2826_C57272").val(item.G2826_C57272); 
                $("#G2826_C57273").val(item.G2826_C57273); 
                $("#G2826_C57274").val(item.G2826_C57274); 
                $("#G2826_C57275").val(item.G2826_C57275); 
                $("#G2826_C57276").val(item.G2826_C57276).trigger("change");  
                $("#G2826_C57277").val(item.G2826_C57277);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2826_C56155").select2();

    $("#G2826_C56996").select2();

    $("#G2826_C56533").select2();

    $("#G2826_C56535").select2();

    $("#G2826_C56537").select2();

    $("#G2826_C56554").select2();

    $("#G2826_C56558").select2();

    $("#G2826_C56593").select2();

    $("#G2826_C56594").select2();

    $("#G2826_C56595").select2();

    $("#G2826_C56596").select2();

    $("#G2826_C56598").select2();

    $("#G2826_C56599").select2();

    $("#G2826_C56600").select2();

    $("#G2826_C56601").select2();

    $("#G2826_C56602").select2();

    $("#G2826_C56603").select2();

    $("#G2826_C56604").select2();

    $("#G2826_C57276").select2();

    $("#G2826_C56614").select2();
        //datepickers
        

        $("#G2826_C56534").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56543").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56547").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56551").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56555").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56556").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56559").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56560").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56564").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56565").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56569").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56570").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56573").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56577").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56578").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56582").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56583").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56586").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56587").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56591").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2826_C56592").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        

        $("#G2826_C56574").numeric({ decimal : ".",  negative : false, scale: 4 });
                
        $("#G2826_C56579").numeric({ decimal : ".",  negative : false, scale: 4 });
                

        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para MAESTRIA_DE_INTERES 

    $("#G2826_C56155").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para LUGAR DE NACIMIENTO 

    $("#G2826_C56996").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SEXO 

    $("#G2826_C56533").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO CIVIL 

    $("#G2826_C56535").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PROVINCIA/CANTON DOMICILIO 

    $("#G2826_C56537").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PROVINCIA / CANTON 

    $("#G2826_C56554").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PROVINCIA / CANTON 

    $("#G2826_C56558").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESPAÑOL 

    $("#G2826_C56593").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para INGLÉS 

    $("#G2826_C56594").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para FRANCÉS 

    $("#G2826_C56595").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para QUICHUA 

    $("#G2826_C56596").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para COMPUTADORA PERSONAL 

    $("#G2826_C56598").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para COMPUTADORA PORTÁTIL 

    $("#G2826_C56599").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TABLET 

    $("#G2826_C56600").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PDA 

    $("#G2826_C56601").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SMARTPHONE 

    $("#G2826_C56602").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ACCESO A INTERNET 

    $("#G2826_C56603").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DOMINIO DE APLICACIONES DE OFICINA 

    $("#G2826_C56604").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE FINANCIAMIENTO 

    $("#G2826_C57276").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para UEES-OnLine se compromete a proteger y respetar su privacidad, y solo usaremos su información personal para administrar su cuenta y proporcionar los productos y servicios que nos solicite. Nos gustaría ponernos en contacto con usted con la finalidad de mostrarle nuestros productos y servicios, así como otros contenidos que puedan interesarle. Si acepta los términos y condiciones de la convocatoria para realizar estudios en Posgrado en la modalidad online de la UEES, seleccione SI en la casilla a continuación. Al seleccionar SI, acepta que UEES-OnLine almacene y procese la información personal suministrada para proporcionarle el contenido solicitado. 

    $("#G2826_C56614").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G2826_C56150").val() == "") && $("#G2826_C56150").prop("disabled") == false){
                alertify.error('APELLIDOS debe ser diligenciado');
                $("#G2826_C56150").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2826_C56151").val() == "") && $("#G2826_C56151").prop("disabled") == false){
                alertify.error('NOMBRES debe ser diligenciado');
                $("#G2826_C56151").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2826_C56153").val() == "") && $("#G2826_C56153").prop("disabled") == false){
                alertify.error('CORREO debe ser diligenciado');
                $("#G2826_C56153").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2826_C56154").val() == "") && $("#G2826_C56154").prop("disabled") == false){
                alertify.error('CELULAR debe ser diligenciado');
                $("#G2826_C56154").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2826_C56155").val()==0 || $("#G2826_C56155").val() == null || $("#G2826_C56155").val() == -1) && $("#G2826_C56155").prop("disabled") == false){
                alertify.error('MAESTRIA_DE_INTERES debe ser diligenciado');
                $("#G2826_C56155").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G2826_C56156").val() == "") && $("#G2826_C56156").prop("disabled") == false){
                alertify.error('COMENTARIO debe ser diligenciado');
                $("#G2826_C56156").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2826_C56147").val(item.G2826_C56147);
 
                                                $("#G2826_C56148").val(item.G2826_C56148);
 
                                                $("#G2826_C56149").val(item.G2826_C56149);
 
                                                $("#G2826_C56530").val(item.G2826_C56530);
 
                                                $("#G2826_C56150").val(item.G2826_C56150);
 
                                                $("#G2826_C56531").val(item.G2826_C56531);
 
                                                $("#G2826_C56151").val(item.G2826_C56151);
 
                                                $("#G2826_C56152").val(item.G2826_C56152);
 
                                                $("#G2826_C56153").val(item.G2826_C56153);
 
                                                $("#G2826_C56157").val(item.G2826_C56157);
 
                                                $("#G2826_C56158").val(item.G2826_C56158);
 
                                                $("#G2826_C56536").val(item.G2826_C56536);
 
                                                $("#G2826_C56154").val(item.G2826_C56154);
 
                    $("#G2826_C56155").val(item.G2826_C56155).trigger("change"); 
 
                                                $("#G2826_C56532").val(item.G2826_C56532);
 
                                                $("#G2826_C56534").val(item.G2826_C56534);
 
                    $("#G2826_C56996").val(item.G2826_C56996).trigger("change"); 
 
                                                $("#G2826_C56997").val(item.G2826_C56997);
 
                    $("#G2826_C56533").val(item.G2826_C56533).trigger("change"); 
 
                    $("#G2826_C56535").val(item.G2826_C56535).trigger("change"); 
 
                    $("#G2826_C56537").val(item.G2826_C56537).trigger("change"); 
 
                                                $("#G2826_C56538").val(item.G2826_C56538);
 
                                                $("#G2826_C56539").val(item.G2826_C56539);
 
                                                $("#G2826_C56540").val(item.G2826_C56540);
 
                                                $("#G2826_C56156").val(item.G2826_C56156);
 
                                                $("#G2826_C56145").val(item.G2826_C56145);
 
                                                $("#G2826_C56146").val(item.G2826_C56146);
 
                                                $("#G2826_C56541").val(item.G2826_C56541);
 
                                                $("#G2826_C56542").val(item.G2826_C56542);
 
                                                $("#G2826_C56543").val(item.G2826_C56543);
 
                                                $("#G2826_C56544").val(item.G2826_C56544);
 
                                                $("#G2826_C56545").val(item.G2826_C56545);
 
                                                $("#G2826_C56546").val(item.G2826_C56546);
 
                                                $("#G2826_C56547").val(item.G2826_C56547);
 
                                                $("#G2826_C56548").val(item.G2826_C56548);
 
                                                $("#G2826_C56549").val(item.G2826_C56549);
 
                                                $("#G2826_C56550").val(item.G2826_C56550);
 
                                                $("#G2826_C56551").val(item.G2826_C56551);
 
                                                $("#G2826_C56552").val(item.G2826_C56552);
 
                                                $("#G2826_C56553").val(item.G2826_C56553);
 
                    $("#G2826_C56554").val(item.G2826_C56554).trigger("change"); 
 
                                                $("#G2826_C56555").val(item.G2826_C56555);
 
                                                $("#G2826_C56556").val(item.G2826_C56556);
 
                                                $("#G2826_C56557").val(item.G2826_C56557);
 
                    $("#G2826_C56558").val(item.G2826_C56558).trigger("change"); 
 
                                                $("#G2826_C56559").val(item.G2826_C56559);
 
                                                $("#G2826_C56560").val(item.G2826_C56560);
 
                                                $("#G2826_C56561").val(item.G2826_C56561);
 
                                                $("#G2826_C56562").val(item.G2826_C56562);
 
                                                $("#G2826_C56563").val(item.G2826_C56563);
 
                                                $("#G2826_C56564").val(item.G2826_C56564);
 
                                                $("#G2826_C56565").val(item.G2826_C56565);
 
                                                $("#G2826_C56566").val(item.G2826_C56566);
 
                                                $("#G2826_C56567").val(item.G2826_C56567);
 
                                                $("#G2826_C56568").val(item.G2826_C56568);
 
                                                $("#G2826_C56569").val(item.G2826_C56569);
 
                                                $("#G2826_C56570").val(item.G2826_C56570);
 
                                                $("#G2826_C56571").val(item.G2826_C56571);
 
                                                $("#G2826_C56572").val(item.G2826_C56572);
 
                                                $("#G2826_C56573").val(item.G2826_C56573);
 
                                                $("#G2826_C56574").val(item.G2826_C56574);
 
                                                $("#G2826_C56575").val(item.G2826_C56575);
 
                                                $("#G2826_C56576").val(item.G2826_C56576);
 
                                                $("#G2826_C56577").val(item.G2826_C56577);
 
                                                $("#G2826_C56578").val(item.G2826_C56578);
 
                                                $("#G2826_C56579").val(item.G2826_C56579);
 
                                                $("#G2826_C56580").val(item.G2826_C56580);
 
                                                $("#G2826_C56581").val(item.G2826_C56581);
 
                                                $("#G2826_C56582").val(item.G2826_C56582);
 
                                                $("#G2826_C56583").val(item.G2826_C56583);
 
                                                $("#G2826_C56584").val(item.G2826_C56584);
 
                                                $("#G2826_C56585").val(item.G2826_C56585);
 
                                                $("#G2826_C56586").val(item.G2826_C56586);
 
                                                $("#G2826_C56587").val(item.G2826_C56587);
 
                                                $("#G2826_C56588").val(item.G2826_C56588);
 
                                                $("#G2826_C56589").val(item.G2826_C56589);
 
                                                $("#G2826_C56590").val(item.G2826_C56590);
 
                                                $("#G2826_C56591").val(item.G2826_C56591);
 
                                                $("#G2826_C56592").val(item.G2826_C56592);
 
                    $("#G2826_C56593").val(item.G2826_C56593).trigger("change"); 
 
                    $("#G2826_C56594").val(item.G2826_C56594).trigger("change"); 
 
                    $("#G2826_C56595").val(item.G2826_C56595).trigger("change"); 
 
                    $("#G2826_C56596").val(item.G2826_C56596).trigger("change"); 
 
                                                $("#G2826_C56597").val(item.G2826_C56597);
 
                    $("#G2826_C56598").val(item.G2826_C56598).trigger("change"); 
 
                    $("#G2826_C56599").val(item.G2826_C56599).trigger("change"); 
 
                    $("#G2826_C56600").val(item.G2826_C56600).trigger("change"); 
 
                    $("#G2826_C56601").val(item.G2826_C56601).trigger("change"); 
 
                    $("#G2826_C56602").val(item.G2826_C56602).trigger("change"); 
 
                    $("#G2826_C56603").val(item.G2826_C56603).trigger("change"); 
 
                    $("#G2826_C56604").val(item.G2826_C56604).trigger("change"); 
 
                                                $("#G2826_C56605").val(item.G2826_C56605);
 
                                                $("#G2826_C56606").val(item.G2826_C56606);
 
                                                if (item.G2826_C56609) {
                                                    $("#G2826_C56609").val(item.G2826_C56609);
                                                }else{
                                                    $("#G2826_C56609").val("Sin Adjunto");
                                                }
 
                                                if (item.G2826_C56607) {
                                                    $("#G2826_C56607").val(item.G2826_C56607);
                                                }else{
                                                    $("#G2826_C56607").val("Sin Adjunto");
                                                }
 
                                                if (item.G2826_C56608) {
                                                    $("#G2826_C56608").val(item.G2826_C56608);
                                                }else{
                                                    $("#G2826_C56608").val("Sin Adjunto");
                                                }
 
                                                if (item.G2826_C56610) {
                                                    $("#G2826_C56610").val(item.G2826_C56610);
                                                }else{
                                                    $("#G2826_C56610").val("Sin Adjunto");
                                                }
 
                                                if (item.G2826_C56611) {
                                                    $("#G2826_C56611").val(item.G2826_C56611);
                                                }else{
                                                    $("#G2826_C56611").val("Sin Adjunto");
                                                }
 
                                                if (item.G2826_C56612) {
                                                    $("#G2826_C56612").val(item.G2826_C56612);
                                                }else{
                                                    $("#G2826_C56612").val("Sin Adjunto");
                                                }
 
                                                if (item.G2826_C56613) {
                                                    $("#G2826_C56613").val(item.G2826_C56613);
                                                }else{
                                                    $("#G2826_C56613").val("Sin Adjunto");
                                                }
 
                    $("#G2826_C56614").val(item.G2826_C56614).trigger("change"); 
 
                                                $("#G2826_C57272").val(item.G2826_C57272);
 
                                                $("#G2826_C57273").val(item.G2826_C57273);
 
                                                $("#G2826_C57274").val(item.G2826_C57274);
 
                                                $("#G2826_C57275").val(item.G2826_C57275);
 
                    $("#G2826_C57276").val(item.G2826_C57276).trigger("change"); 
 
                                                $("#G2826_C57277").val(item.G2826_C57277);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','TIPOID','NUMEROID','CODIGO_ESTUDIANTE','NUMERO DE CEDULA','APELLIDOS','SEGUNDO APELLIDO','NOMBRES','CORREO_UEES','CORREO','ASESOR','ETIQUETA','TELEFONO FIJO','CELULAR','MAESTRIA_DE_INTERES','NACIONALIDAD','FECHA DE NACIMIENTO','LUGAR DE NACIMIENTO','TIPO DE SANGRE','SEXO','ESTADO CIVIL','PROVINCIA/CANTON DOMICILIO','DIRECCION DOMICILIO','PROVINCIA / CANTON TRABAJO','DIRECCION TRABAJO','COMENTARIO','ORIGEN_DY_WF','OPTIN_DY_WF','NOMBRES 1er Familiar','APELLIDOS 1er Familiar','FECHA DE NACIMIENTO','RELACIÓN','NOMBRES 2do Familiar','APELLIDOS 2do Familiar','FECHA DE NACIMIENTO','RELACIÓN','NOMBRES 3er Familiar','APELLIDOS 3er Familiar','FECHA DE NACIMIENTO','RELACIÓN','INSTITUCIÓN Est. Primarios','PROVINCIA / CANTON','DESDE','HASTA','INSTITUCIÓN Est. Secundarios','PROVINCIA / CANTON','DESDE','HASTA','INSTITUCIÓN Est. Superiores','CARRERA','LUGAR','DESDE','HASTA','INSTITUCIÓN Posgrado','PROGRAMA','LUGAR','DESDE','HASTA','EMPRESA / INSTITUCION Exp. Laboral Actual','CARGO','FECHA DE INICIO','SUELDO','EMPRESA / INSTITUCIÓN Exp. Laboral Anterior','CARGO','FECHA DE INICIO','FECHA FINAL','SUELDO','INSTITUCIÓN Exp. Docente Actual','FUNCIONES','DESDE','HASTA','INSTITUCIÓN Exp. Docente Anterior','FUNCIONES','DESDE','HASTA','INSTITUCIÓN Exp. Investigación','INVESTIGACIONES REALIZADAS','FUNCIONES','DESDE','HASTA','ESPAÑOL','INGLÉS','FRANCÉS','QUICHUA','Otros Idiomas, especifique el nombre y nivel de dominio','COMPUTADORA PERSONAL','COMPUTADORA PORTÁTIL','TABLET','PDA','SMARTPHONE','ACCESO A INTERNET','DOMINIO DE APLICACIONES DE OFICINA','INFORMACIÓN ADICIONAL: Proporcione en este espacio información que no fué incluido anteriormente en el presente instrumento y que considere que deba ser considerado como becas, distinciones académicas, membresías institucionales, etc.','INFORMACIÓN IMPORTANTE: En caso de tener alguna enfermedad que usted considere deba ser del conocimiento del Director del programa por favor escríbalo','Copia a color de la Cédula de Identidad','Título de tercer nivel de grado notarizado','Registro del título en el SENESCYT','Cartas de referencias personales o profesionales actualizadas','Hoja de vida actualizada','Respaldos que sustente la información detallada de la hoja de vida','Foto tamaño carnet','UEES-OnLine se compromete a proteger y respetar su privacidad, y solo usaremos su información personal para administrar su cuenta y proporcionar los productos y servicios que nos solicite. Nos gustaría ponernos en contacto con usted con la finalidad de mostrarle nuestros productos y servicios, así como otros contenidos que puedan interesarle. Si acepta los términos y condiciones de la convocatoria para realizar estudios en Posgrado en la modalidad online de la UEES, seleccione SI en la casilla a continuación. Al seleccionar SI, acepta que UEES-OnLine almacene y procese la información personal suministrada para proporcionarle el contenido solicitado.','TIPO DE ID','NUMERO DE ID','NOMBRE','CORREO','TIPO DE FINANCIAMIENTO','SI ELIGIO "OTROS" EXPECIFIQUE'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2826_C56147', 
                        index: 'G2826_C56147', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56148', 
                        index: 'G2826_C56148', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56149', 
                        index: 'G2826_C56149', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56530', 
                        index: 'G2826_C56530', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56150', 
                        index: 'G2826_C56150', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56531', 
                        index: 'G2826_C56531', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56151', 
                        index: 'G2826_C56151', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56152', 
                        index: 'G2826_C56152', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56153', 
                        index: 'G2826_C56153', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56157', 
                        index: 'G2826_C56157', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56158', 
                        index: 'G2826_C56158', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56536', 
                        index: 'G2826_C56536', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56154', 
                        index: 'G2826_C56154', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56155', 
                        index:'G2826_C56155', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3390&campo=G2826_C56155'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56532', 
                        index: 'G2826_C56532', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56534', 
                        index:'G2826_C56534', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56996', 
                        index:'G2826_C56996', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3404&campo=G2826_C56996'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56997', 
                        index: 'G2826_C56997', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56533', 
                        index:'G2826_C56533', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3405&campo=G2826_C56533'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56535', 
                        index:'G2826_C56535', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3403&campo=G2826_C56535'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56537', 
                        index:'G2826_C56537', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3407&campo=G2826_C56537'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56538', 
                        index: 'G2826_C56538', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56539', 
                        index: 'G2826_C56539', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56540', 
                        index: 'G2826_C56540', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56156', 
                        index:'G2826_C56156', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56145', 
                        index: 'G2826_C56145', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56146', 
                        index: 'G2826_C56146', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56541', 
                        index: 'G2826_C56541', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56542', 
                        index: 'G2826_C56542', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56543', 
                        index:'G2826_C56543', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56544', 
                        index: 'G2826_C56544', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56545', 
                        index: 'G2826_C56545', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56546', 
                        index: 'G2826_C56546', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56547', 
                        index:'G2826_C56547', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56548', 
                        index: 'G2826_C56548', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56549', 
                        index: 'G2826_C56549', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56550', 
                        index: 'G2826_C56550', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56551', 
                        index:'G2826_C56551', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56552', 
                        index: 'G2826_C56552', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56553', 
                        index: 'G2826_C56553', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56554', 
                        index:'G2826_C56554', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3407&campo=G2826_C56554'
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56555', 
                        index:'G2826_C56555', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56556', 
                        index:'G2826_C56556', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56557', 
                        index: 'G2826_C56557', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56558', 
                        index:'G2826_C56558', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3407&campo=G2826_C56558'
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56559', 
                        index:'G2826_C56559', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56560', 
                        index:'G2826_C56560', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56561', 
                        index: 'G2826_C56561', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56562', 
                        index: 'G2826_C56562', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56563', 
                        index: 'G2826_C56563', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56564', 
                        index:'G2826_C56564', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56565', 
                        index:'G2826_C56565', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56566', 
                        index: 'G2826_C56566', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56567', 
                        index: 'G2826_C56567', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56568', 
                        index: 'G2826_C56568', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56569', 
                        index:'G2826_C56569', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56570', 
                        index:'G2826_C56570', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56571', 
                        index: 'G2826_C56571', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56572', 
                        index: 'G2826_C56572', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56573', 
                        index:'G2826_C56573', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56574', 
                        index:'G2826_C56574', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2826_C56575', 
                        index: 'G2826_C56575', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56576', 
                        index: 'G2826_C56576', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56577', 
                        index:'G2826_C56577', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56578', 
                        index:'G2826_C56578', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56579', 
                        index:'G2826_C56579', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2826_C56580', 
                        index: 'G2826_C56580', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56581', 
                        index:'G2826_C56581', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56582', 
                        index:'G2826_C56582', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56583', 
                        index:'G2826_C56583', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56584', 
                        index: 'G2826_C56584', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56585', 
                        index: 'G2826_C56585', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56586', 
                        index:'G2826_C56586', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56587', 
                        index:'G2826_C56587', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56588', 
                        index: 'G2826_C56588', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56589', 
                        index:'G2826_C56589', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56590', 
                        index:'G2826_C56590', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2826_C56591', 
                        index:'G2826_C56591', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2826_C56592', 
                        index:'G2826_C56592', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56593', 
                        index:'G2826_C56593', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3408&campo=G2826_C56593'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56594', 
                        index:'G2826_C56594', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3408&campo=G2826_C56594'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56595', 
                        index:'G2826_C56595', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3408&campo=G2826_C56595'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56596', 
                        index:'G2826_C56596', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3408&campo=G2826_C56596'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56597', 
                        index:'G2826_C56597', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56598', 
                        index:'G2826_C56598', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56598'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56599', 
                        index:'G2826_C56599', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56599'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56600', 
                        index:'G2826_C56600', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56600'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56601', 
                        index:'G2826_C56601', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56601'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56602', 
                        index:'G2826_C56602', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56602'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56603', 
                        index:'G2826_C56603', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56603'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56604', 
                        index:'G2826_C56604', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3408&campo=G2826_C56604'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C56605', 
                        index:'G2826_C56605', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56606', 
                        index:'G2826_C56606', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56609', 
                        index: 'G2826_C56609', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56607', 
                        index: 'G2826_C56607', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56608', 
                        index: 'G2826_C56608', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56610', 
                        index: 'G2826_C56610', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56611', 
                        index: 'G2826_C56611', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56612', 
                        index: 'G2826_C56612', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56613', 
                        index: 'G2826_C56613', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C56614', 
                        index:'G2826_C56614', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3409&campo=G2826_C56614'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C57272', 
                        index: 'G2826_C57272', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C57273', 
                        index: 'G2826_C57273', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C57274', 
                        index: 'G2826_C57274', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C57275', 
                        index: 'G2826_C57275', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2826_C57276', 
                        index:'G2826_C57276', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3445&campo=G2826_C57276'
                        }
                    }

                    ,
                    { 
                        name:'G2826_C57277', 
                        index: 'G2826_C57277', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2826_C56151',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2826_C56147").val(item.G2826_C56147);

                        $("#G2826_C56148").val(item.G2826_C56148);

                        $("#G2826_C56149").val(item.G2826_C56149);

                        $("#G2826_C56530").val(item.G2826_C56530);

                        $("#G2826_C56150").val(item.G2826_C56150);

                        $("#G2826_C56531").val(item.G2826_C56531);

                        $("#G2826_C56151").val(item.G2826_C56151);

                        $("#G2826_C56152").val(item.G2826_C56152);

                        $("#G2826_C56153").val(item.G2826_C56153);

                        $("#G2826_C56157").val(item.G2826_C56157);

                        $("#G2826_C56158").val(item.G2826_C56158);

                        $("#G2826_C56536").val(item.G2826_C56536);

                        $("#G2826_C56154").val(item.G2826_C56154);
 
                    $("#G2826_C56155").val(item.G2826_C56155).trigger("change"); 

                        $("#G2826_C56532").val(item.G2826_C56532);

                        $("#G2826_C56534").val(item.G2826_C56534);
 
                    $("#G2826_C56996").val(item.G2826_C56996).trigger("change"); 

                        $("#G2826_C56997").val(item.G2826_C56997);
 
                    $("#G2826_C56533").val(item.G2826_C56533).trigger("change"); 
 
                    $("#G2826_C56535").val(item.G2826_C56535).trigger("change"); 
 
                    $("#G2826_C56537").val(item.G2826_C56537).trigger("change"); 

                        $("#G2826_C56538").val(item.G2826_C56538);

                        $("#G2826_C56539").val(item.G2826_C56539);

                        $("#G2826_C56540").val(item.G2826_C56540);

                        $("#G2826_C56156").val(item.G2826_C56156);

                        $("#G2826_C56145").val(item.G2826_C56145);

                        $("#G2826_C56146").val(item.G2826_C56146);

                        $("#G2826_C56541").val(item.G2826_C56541);

                        $("#G2826_C56542").val(item.G2826_C56542);

                        $("#G2826_C56543").val(item.G2826_C56543);

                        $("#G2826_C56544").val(item.G2826_C56544);

                        $("#G2826_C56545").val(item.G2826_C56545);

                        $("#G2826_C56546").val(item.G2826_C56546);

                        $("#G2826_C56547").val(item.G2826_C56547);

                        $("#G2826_C56548").val(item.G2826_C56548);

                        $("#G2826_C56549").val(item.G2826_C56549);

                        $("#G2826_C56550").val(item.G2826_C56550);

                        $("#G2826_C56551").val(item.G2826_C56551);

                        $("#G2826_C56552").val(item.G2826_C56552);

                        $("#G2826_C56553").val(item.G2826_C56553);
 
                    $("#G2826_C56554").val(item.G2826_C56554).trigger("change"); 

                        $("#G2826_C56555").val(item.G2826_C56555);

                        $("#G2826_C56556").val(item.G2826_C56556);

                        $("#G2826_C56557").val(item.G2826_C56557);
 
                    $("#G2826_C56558").val(item.G2826_C56558).trigger("change"); 

                        $("#G2826_C56559").val(item.G2826_C56559);

                        $("#G2826_C56560").val(item.G2826_C56560);

                        $("#G2826_C56561").val(item.G2826_C56561);

                        $("#G2826_C56562").val(item.G2826_C56562);

                        $("#G2826_C56563").val(item.G2826_C56563);

                        $("#G2826_C56564").val(item.G2826_C56564);

                        $("#G2826_C56565").val(item.G2826_C56565);

                        $("#G2826_C56566").val(item.G2826_C56566);

                        $("#G2826_C56567").val(item.G2826_C56567);

                        $("#G2826_C56568").val(item.G2826_C56568);

                        $("#G2826_C56569").val(item.G2826_C56569);

                        $("#G2826_C56570").val(item.G2826_C56570);

                        $("#G2826_C56571").val(item.G2826_C56571);

                        $("#G2826_C56572").val(item.G2826_C56572);

                        $("#G2826_C56573").val(item.G2826_C56573);

                        $("#G2826_C56574").val(item.G2826_C56574);

                        $("#G2826_C56575").val(item.G2826_C56575);

                        $("#G2826_C56576").val(item.G2826_C56576);

                        $("#G2826_C56577").val(item.G2826_C56577);

                        $("#G2826_C56578").val(item.G2826_C56578);

                        $("#G2826_C56579").val(item.G2826_C56579);

                        $("#G2826_C56580").val(item.G2826_C56580);

                        $("#G2826_C56581").val(item.G2826_C56581);

                        $("#G2826_C56582").val(item.G2826_C56582);

                        $("#G2826_C56583").val(item.G2826_C56583);

                        $("#G2826_C56584").val(item.G2826_C56584);

                        $("#G2826_C56585").val(item.G2826_C56585);

                        $("#G2826_C56586").val(item.G2826_C56586);

                        $("#G2826_C56587").val(item.G2826_C56587);

                        $("#G2826_C56588").val(item.G2826_C56588);

                        $("#G2826_C56589").val(item.G2826_C56589);

                        $("#G2826_C56590").val(item.G2826_C56590);

                        $("#G2826_C56591").val(item.G2826_C56591);

                        $("#G2826_C56592").val(item.G2826_C56592);
 
                    $("#G2826_C56593").val(item.G2826_C56593).trigger("change"); 
 
                    $("#G2826_C56594").val(item.G2826_C56594).trigger("change"); 
 
                    $("#G2826_C56595").val(item.G2826_C56595).trigger("change"); 
 
                    $("#G2826_C56596").val(item.G2826_C56596).trigger("change"); 

                        $("#G2826_C56597").val(item.G2826_C56597);
 
                    $("#G2826_C56598").val(item.G2826_C56598).trigger("change"); 
 
                    $("#G2826_C56599").val(item.G2826_C56599).trigger("change"); 
 
                    $("#G2826_C56600").val(item.G2826_C56600).trigger("change"); 
 
                    $("#G2826_C56601").val(item.G2826_C56601).trigger("change"); 
 
                    $("#G2826_C56602").val(item.G2826_C56602).trigger("change"); 
 
                    $("#G2826_C56603").val(item.G2826_C56603).trigger("change"); 
 
                    $("#G2826_C56604").val(item.G2826_C56604).trigger("change"); 

                        $("#G2826_C56605").val(item.G2826_C56605);

                        $("#G2826_C56606").val(item.G2826_C56606);

                        if (item.G2826_C56609) {
                            $("#G2826_C56609").val(item.G2826_C56609);
                        }else{
                            $("#G2826_C56609").val("Sin Adjunto");
                        }

                        if (item.G2826_C56607) {
                            $("#G2826_C56607").val(item.G2826_C56607);
                        }else{
                            $("#G2826_C56607").val("Sin Adjunto");
                        }

                        if (item.G2826_C56608) {
                            $("#G2826_C56608").val(item.G2826_C56608);
                        }else{
                            $("#G2826_C56608").val("Sin Adjunto");
                        }

                        if (item.G2826_C56610) {
                            $("#G2826_C56610").val(item.G2826_C56610);
                        }else{
                            $("#G2826_C56610").val("Sin Adjunto");
                        }

                        if (item.G2826_C56611) {
                            $("#G2826_C56611").val(item.G2826_C56611);
                        }else{
                            $("#G2826_C56611").val("Sin Adjunto");
                        }

                        if (item.G2826_C56612) {
                            $("#G2826_C56612").val(item.G2826_C56612);
                        }else{
                            $("#G2826_C56612").val("Sin Adjunto");
                        }

                        if (item.G2826_C56613) {
                            $("#G2826_C56613").val(item.G2826_C56613);
                        }else{
                            $("#G2826_C56613").val("Sin Adjunto");
                        }
 
                    $("#G2826_C56614").val(item.G2826_C56614).trigger("change"); 

                        $("#G2826_C57272").val(item.G2826_C57272);

                        $("#G2826_C57273").val(item.G2826_C57273);

                        $("#G2826_C57274").val(item.G2826_C57274);

                        $("#G2826_C57275").val(item.G2826_C57275);
 
                    $("#G2826_C57276").val(item.G2826_C57276).trigger("change"); 

                        $("#G2826_C57277").val(item.G2826_C57277);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
