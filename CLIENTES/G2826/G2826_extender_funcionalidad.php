<?php

        include(__DIR__."/../../conexion.php");
        $url_crud =  "formularios/G2826/G2826_extender_funcionalidad.php";
        //Este archivo es para agregar funcionalidades al G, y que al momento de generar de nuevo no se pierdan

        //Cosas como nuevas consultas, nuevos Inserts, Envio de correos_, etc, en fin extender mas los formularios en PHP

    if(isset($_POST['celular'])){
        $sql="SELECT G2826_ConsInte__b as id FROM DYALOGOCRM_WEB.G2826 where G2826_C56154 ={$_POST['celular']} limit 1";
        $sql=$mysqli->query($sql);
        
        if($sql && $sql->num_rows ==1){
            $sql=$sql->fetch_object();
            $datos['id']=$sql->id;
        }else{
            $datos['id']='0';
        }
        echo json_encode($datos);
    }
?>
<?php if(isset($_GET['document'])) : ?>
<script>
    $('input[type=text],input[type=file],select,textarea').each(function(){
        if(this.id == 'G2826_C56150' || this.id == 'G2826_C56151' || this.id == 'G2826_C56153' || this.id == 'G2826_C56155' || this.id == 'G2826_C56156'){
            $(this).parent().remove();
        }else{
            if(this.id != 'G2826_C56154'){
                $(this).attr('disabled','disabled');
            }
        }
    });
    $('#G2826_C56154').change(function(){
        $.ajax({
            url:'<?=$url_crud?>',
            type:'post',
            datatype:'json',
            data:{celular:$(this).val()},
            success:function(data){
                data=JSON.parse(data);
                if(data.id == 0){
                    alert('No se encontraron datos');
                    $('input[type=text],input[type=file],select,textarea').each(function(){
                        if(this.id != 'G2826_C56154'){
                            $(this).attr('disabled','disabled');
                        } 
                    });
                    $("button[type=submit]").attr('disabled','disabled');    
                }else{
                    $('#hidId').val(data.id);
                    $('input[type=text],input[type=file],select,textarea').each(function(){
                       $(this).attr('disabled',false); 
                    });
                    $("button[type=submit]").attr('disabled',false);
                }
            }
        })
    });
    $("button[type=submit]").attr('disabled','disabled');
    $("#oper").val('edit');
    $(".login-logo").html('');
    $(".login-box-msg").html('');
    $("button").removeClass('btn-primary btn-flat');
    $(".btn").css('background','#810C33');
    $(".btn").css('color','white');
    $("button").parent().removeClass('col-xs-8');
    $("button").parent().addClass('col-xs-12');    
</script>    
<?php endif; ?>
<?php if(!isset($_GET['document']) && !isset($_POST['celular'])) : ?>
<script>    
    $('input[type=text],input[type=file],select,textarea').each(function(){
        if(this.id != 'G2826_C56150' && this.id != 'G2826_C56151' && this.id != 'G2826_C56153' && this.id != 'G2826_C56154' && this.id != 'G2826_C56155' && this.id != 'G2826_C56156'){
            $(this).parent().remove();
        }
    });
    $(".login-logo").html('');
    $(".login-box-msg").html('');
    $("button").removeClass('btn-primary btn-flat');
    $(".btn").css('background','#810C33');
    $(".btn").css('color','white');
    $("button").parent().removeClass('col-xs-8');
    $("button").parent().addClass('col-xs-12');
    $("#G2826_C56155").parent().css('display','none');
    $(".secciones").css('display','none');
    <?php if(isset($_GET['maestria'])) : ?>
        $('#G2826_C56155').val('<?=$_GET['maestria']?>');
    <?php endif; ?> 
    
    function redireccionar(){
        window.parent.postMessage(true, '*');
//        var strCarrera_t=$('#G2826_C56155').val();
//        switch(strCarrera_t){
//            case '40773':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/administracion-empresas/thankyou.php';
//                break;            
//            case '40648':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/administracion-publica/thankyou.php';
//                break;
//            case '40653':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/auditoria-integral/thankyou.php';
//                break;
//            case '40776':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/banca-mercados/thankyou.php';
//                break;
//            case '40774':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/comercio-electronico/thankyou.php';
//                break;
//            case '40771':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/contabilidad-finanzas/thankyou.php';
//                break;
//            case '40655':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/gestion-cadena/thankyou.php';
//                break;
//            case '40657':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/gestion-calidad/thankyou.php';
//                break;
//            case '40780':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/gestion-comunicacion/thankyou.php';
//                break;
//            case '40772':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/gestion-financiera/thankyou.php';
//                break;
//            case '40777':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/gestion-innovacion/thankyou.php';
//                break;
//            case '40775':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/gestion-talento/thankyou.php';
//                break;
//            case '40654':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/inteligencia-negocios/thankyou.php';
//                break;
//            case '40656':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/management-estrategico/thankyou.php';
//                break;
//            case '40782':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/marketing/thankyou.php';
//                break;
//            case '40769':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/marketing-digital/thankyou.php';
//                break;
//            case '40778':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/negocios-digitales/thankyou.php';
//                break;
//            case '40651':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/seguridad-ocupacional/thankyou.php';
//                break;
//            case '40779':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/seguros-gestion/thankyou.php';
//                break;
//            case '40770':
//                window.location.href='https://uees.edu.ec/postgrado/ESAI/sostenibilidad-empresarial/thankyou.php';
//                break;
//            case '40647':
//                window.location.href='https://www.uees.edu.ec/postgrado/derecho-2/thankyou.php';
//                break;
//            case '40645':
//                window.location.href='https://www.uees.edu.ec/postgrado/derecho-constitucional-2/thankyou.php';
//                break;
//            case '40644':
//                window.location.href='https://www.uees.edu.ec/postgrado/derecho-penal-2/thankyou.php';
//                break;
//            case '40643':
//                window.location.href='https://www.uees.edu.ec/postgrado/derecho-procesal-2/thankyou.php';
//                break;
//            case '40646':
//                window.location.href='https://www.uees.edu.ec/postgrado/criminalistica-2/thankyou.php';
//                break;
//            case '40650':
//                window.location.href='https://www.uees.edu.ec/postgrado/educativa-2/thankyou.php';
//                break;            
//            case '40649':
//                window.location.href='https://www.uees.edu.ec/postgrado/educacion-2/thankyou.php';
//                break;  
//        }
    }
</script>
<?php endif; ?>