
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G3198/G3198_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : $_SESSION["IDENTIFICACION"];
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G3198_ConsInte__b as id, G3198_C64154 as camp1 , G3198_C64154 as camp2 FROM ".$BaseDatos.".G3198  WHERE G3198_Usuario = ".$idUsuario." ORDER BY G3198_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G3198_ConsInte__b as id, G3198_C64154 as camp1 , G3198_C64154 as camp2 FROM ".$BaseDatos.".G3198  ORDER BY G3198_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G3198_ConsInte__b as id, G3198_C64154 as camp1 , G3198_C64154 as camp2 FROM ".$BaseDatos.".G3198 JOIN ".$BaseDatos.".G3198_M".$resultEstpas->muestr." ON G3198_ConsInte__b = G3198_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G3198_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G3198_ConsInte__b as id, G3198_C64154 as camp1 , G3198_C64154 as camp2 FROM ".$BaseDatos.".G3198 JOIN ".$BaseDatos.".G3198_M".$resultEstpas->muestr." ON G3198_ConsInte__b = G3198_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G3198_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G3198_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G3198_ConsInte__b as id, G3198_C64154 as camp1 , G3198_C64154 as camp2 FROM ".$BaseDatos.".G3198  ORDER BY G3198_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="9901" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3198_C64154" id="LblG3198_C64154">ID GESTION</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3198_C64154'])) {
                            echo $_GET['G3198_C64154'];
                        } ?>" readonly name="G3198_C64154" id="G3198_C64154" placeholder="ID GESTION"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra. (Nombre del paciente) Con que especialidad o especialista requiere su cita?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="9902" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9902c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9902c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3198_C64143" id="LblG3198_C64143">ORIGEN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3198_C64143" value="<?php if (isset($_GET['G3198_C64143'])) {
                            echo $_GET['G3198_C64143'];
                        } ?>" readonly name="G3198_C64143"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3198_C64144" id="LblG3198_C64144">OPTIN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3198_C64144" value="<?php if (isset($_GET['G3198_C64144'])) {
                            echo $_GET['G3198_C64144'];
                        } ?>" readonly name="G3198_C64144"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64145" id="LblG3198_C64145">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64145" id="G3198_C64145">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3941 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  id="9903" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el paciente solicita consulta por primera vez SE LE DEBEN NOMBRAR TODOS LOS ESPECIALISTAS, dándole el derecho al paciente de elegir, en los casos de Ortopedia, Cx general, Ginecologia se deben nombrar todos y recomendar el médico especializado según diagnostico.        </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64147" id="LblG3198_C64147">Especialidad</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64147" id="G3198_C64147">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3945 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64148" id="LblG3198_C64148">Especialista que requiere</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64148" id="G3198_C64148">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3946 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64149" id="LblG3198_C64149"> 1 vez /control</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64149" id="G3198_C64149">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3895 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64249" id="LblG3198_C64249">DISPONIBILIDAD DE AGENDAMIENTO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64249" id="G3198_C64249">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3892 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si no se tiene disponibilidad de agendamiento le informo al paicente lo siguiente       </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Vamos a tomar sus datos (Cedula- Nombre. Teléfono, Eps y especialidad - especialista que requiere, 1 vez/ control), para poder agendarla cuanto antes, Uno de mis compañeros le estara contactando para indicarle fecha y hora de la misma.        </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G3198_C64153" id="LblG3198_C64153">¿Señor(a), usted tiene a mano Autorización y Orden medica?   </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64153" id="G3198_C64153">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3942 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G3198_C64153" id="respuesta_LblG3198_C64153">Respuesta</label>
                        <textarea id="respuesta_G3198_C64153" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9904" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sería tan amable de confirmarme que dice la orden médica (El Ordenamiento debe ser el mismo que se esta autorizado).   </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr/Sra (Nombre del paciente) vamos a confirmar los datos que tiene su autorizacion.    </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3198_C64157" id="LblG3198_C64157">Fecha de autorización </label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G3198_C64157'])) {
                            echo $_GET['G3198_C64157'];
                        } ?>"  name="G3198_C64157" id="G3198_C64157" placeholder="YYYY-MM-DD" nombre="Fecha de autorización ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64158" id="LblG3198_C64158">Remitido A </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64158" id="G3198_C64158">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3955 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3198_C64159" id="LblG3198_C64159">Codigo de autorización </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3198_C64159" value="<?php if (isset($_GET['G3198_C64159'])) {
                            echo $_GET['G3198_C64159'];
                        } ?>"  name="G3198_C64159"  placeholder="Codigo de autorización "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G3198_C64160" id="LblG3198_C64160">Copago </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G3198_C64160" value="<?php if (isset($_GET['G3198_C64160'])) {
                            echo $_GET['G3198_C64160'];
                        } ?>"  name="G3198_C64160"  placeholder="Copago "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64161" id="LblG3198_C64161">Vigencia de autorización </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64161" id="G3198_C64161">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3894 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el paciente no tiene orden médica, se debe de confirmar con el área de Facturación si se puede autorizar dependiendo el procedimiento o consulta que solicite, esto en los casos de MP - PARTICULAR   </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9905" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">ESTABLECER EL RIESGO DE CAIDAS   </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">CONDICIONES DEL PACIENTE PARA ESTABLECER EL RIESGO DE CAIDAS Y EN CASO TAL DE TENERLO DEBE QUEDAR EN LA OBSERVACION DE DINAMICA       </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64165" id="LblG3198_C64165">A. El paciente tiene algún impedimento o dificultad para caminar?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64165" id="G3198_C64165">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si el paciente responde SI, continuamos con la segunda pregunta, de lo contrario pasamos a la pregunta número tres.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64167" id="LblG3198_C64167">B. El paciente tiene en este momento algún aparato o soporte ortopédico Como: ¿yeso, bastón, muletas, caminador o silla de ruedas?"       </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64167" id="G3198_C64167">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64168" id="LblG3198_C64168">C. ¿El paciente tiene discapacidad total o parcial de tipo visual o auditiva?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64168" id="G3198_C64168">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64169" id="LblG3198_C64169">D. ¿El paciente tiene alguna limitación o discapacidad neurológica? Como: retardo mental"</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64169" id="G3198_C64169">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9906" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">PROTOCOLO DE PREGUNTAS COVID 19   </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G3198_C64171" id="LblG3198_C64171">1. Cuantos años tiene?</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G3198_C64171'])) {
                            echo $_GET['G3198_C64171'];
                        } ?>"  name="G3198_C64171" id="G3198_C64171" placeholder="1. Cuantos años tiene?"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64172" id="LblG3198_C64172">2. ¿Ha viajado los últimos 30 días fuera del país?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64172" id="G3198_C64172">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64173" id="LblG3198_C64173">3. ¿Ha tenido contacto con personas diagnosticadas con COVID 19?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64173" id="G3198_C64173">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64174" id="LblG3198_C64174">4. ¿Ha tenido contacto con personas que viajaron al exterior los últimos 30 días?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64174" id="G3198_C64174">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64175" id="LblG3198_C64175">5. ¿Tiene o tuvo gripa durante las últimas tres semanas?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64175" id="G3198_C64175">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64176" id="LblG3198_C64176">6. Ha presentado alguno de esto síntomas: Tos, dolor de garganta, ¿fiebre?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64176" id="G3198_C64176">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Paciente que se le asigne fisioterapia aparte de estas preguntas le debemos preguntar lo siguiente:</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64178" id="LblG3198_C64178">1. Es persona diabética,</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64178" id="G3198_C64178">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64179" id="LblG3198_C64179">2. Sufre problemas cardio respiratorios,</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64179" id="G3198_C64179">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64180" id="LblG3198_C64180">3. Es hipertensa.</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64180" id="G3198_C64180">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="9907" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si es un procedimiento se le debe explicar y darle la opción al paciente de enviarle un correo electrónico con dicha preparación, EL ENVIO LO REALIZAN DESDE LOS CORREOS INSTITUCIONALES.  Y se le hace la claridad al paciente que para proximas solicitudes de citas se debe comunicar nuevamente a la linea.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">"Sr/Sra.(Nombre del paciente)  Le confirmo su cita/procedimiento, le queda programado para el día  (Fecha y hora), Le recordamos presentarse: • 30 minutos antes de la hora asignada (si es procedimiento el tiempo indicado)  • Traer orden y autorización (Si es MP solo trae la documentación que se requiera) • Si viene para consulta especializada, traer fotocopia de la última evolución de la historia clínica  • Debe traer exámenes y estudios solicitados por el especialista si los tiene • Recuerde traer su tapabocas ya sea desechable o de tele que le cubra nariz y boca. • Procurar asistir sin acompañante y en caso tal de requerirlo, debe ser solo una persona mayor de edad, recordar que el cuidado es de todos.  • En caso tal de no poder asistir llamar al 8849999 y confirmar la cancelación de la misma "</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G3198_C64183" id="LblG3198_C64183">Está satisfecho con la información brindada?   </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G3198_C64183" id="G3198_C64183">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3896 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G3198_C64184" id="LblG3198_C64184">Observaciones            </label>
                        <textarea class="form-control input-sm" name="G3198_C64184" id="G3198_C64184"  value="<?php if (isset($_GET['G3198_C64184'])) {
                            echo $_GET['G3198_C64184'];
                        } ?>" placeholder="Observaciones            "></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Gracias por llamar a SES HOSPITAL UNIVERSITARIO DE CALDAS, recuerde que lo atendió (Nombre completo del asesor).        </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G3198/G3198_eventos.js"></script>
<script type="text/javascript" src="formularios/G3198/G3198_extender_funcionalidad.php"></script><?php require_once "G3198_extender_funcionalidad.php";?><?php require_once "formularios/enviarSms_Mail.php";?>
<script type="text/javascript">

function ocultarBotones(){

    $("#add").hide();
    $("#delete").hide();
    $("#edit").hide();
    $("#Save").hide();
    $("#cancel").hide();

}

ocultarBotones();

function mostrarBotones(){

    $("#add").show();
    $("#delete").show();
    $("#edit").show();
    $("#Save").show();
    $("#cancel").show();

}

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {

        switch(e.data){

            case 'edit':
                    $("#edit").click();
                break;
            case 'delete':
                    $("#delete").click();
                break;
            case 'Save':
                    $("#Save").click();
                break;

        }
        
        
    });
    
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje;
        sendMessage('' + mensaje);
    });

    //JDBD - Funcion para descargar los adjuntos
    function bajarAdjunto(id){

        var strURL_t = $("#"+id).attr("adjunto");

        if (strURL_t != "") {

            location.href='<?=$url_crud;?>?adjunto='+strURL_t;
            
        }


    }
    
    

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php 
            if($_GET["yourfather"] != "NULL"){ 
                if($_GET["yourfather"] == "-1") {
                    if(isset($_GET["token"]) && isset($_GET["idFather"])){ ?>
                        $("#G3198_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$_GET["idFather"]?>");
                        $("#G3198_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET["idFather"]?>");
                        setTimeout(function(){
                            $("#G3198_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                    <?php }else{
                    $sqlMiembro=$mysqli->query("SELECT G{$_GET['formularioPadre']}_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G{$_GET['formularioPadre']} WHERE G{$_GET['formularioPadre']}_ConsInte__b={$_GET['idFather']}");
                    if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                        $sqlMiembro=$sqlMiembro->fetch_object();
                        $intMiembro=$sqlMiembro->miembro;
                    }
        ?>
                        $("#G3198_C<?=$_GET['pincheCampo'];?>").attr("opt","<?=$intMiembro?>");
                        $("#G3198_C<?=$_GET['pincheCampo'];?>").val("<?=$intMiembro?>");
                        setTimeout(function(){
                            $("#G3198_C<?=$_GET['pincheCampo'];?>").change();       
                        },1000);                        
                <?php } ?>
        <?php }else{ ?>
                $("#G3198_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php } ?>        
        <?php }else{ ?>
            if(document.getElementById("G3198_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G3198_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
        
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            $("#G3198_C64147").val("0").trigger("change");
            $("#G3198_C64148").val("0").trigger("change");
            $("#G3198_C64149").val("0").trigger("change");
            $("#G3198_C64249").val("0").trigger("change");
            $("#G3198_C64158").val("0").trigger("change");
            $("#G3198_C64161").val("0").trigger("change");
            $("#G3198_C64165").val("0").trigger("change");
            $("#G3198_C64167").val("0").trigger("change");
            $("#G3198_C64168").val("0").trigger("change");
            $("#G3198_C64169").val("0").trigger("change");
            $("#G3198_C64172").val("0").trigger("change");
            $("#G3198_C64173").val("0").trigger("change");
            $("#G3198_C64174").val("0").trigger("change");
            $("#G3198_C64175").val("0").trigger("change");
            $("#G3198_C64176").val("0").trigger("change");
            $("#G3198_C64178").val("0").trigger("change");
            $("#G3198_C64179").val("0").trigger("change");
            $("#G3198_C64180").val("0").trigger("change");
            $("#G3198_C64183").val("0").trigger("change");
            
            
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G3198_C64154").val(item.G3198_C64154);   
                if(item.G3198_C64146 == 1){
                    $("#G3198_C64146").attr('checked', true);
                }  
                $("#G3198_C64143").val(item.G3198_C64143); 
                $("#G3198_C64144").val(item.G3198_C64144); 
                $("#G3198_C64145").val(item.G3198_C64145).trigger("change");    
                if(item.G3198_C64150 == 1){
                    $("#G3198_C64150").attr('checked', true);
                }  
                $("#G3198_C64147").val(item.G3198_C64147).trigger("change");  
                $("#G3198_C64148").attr("opt",item.G3198_C64148);  
                $("#G3198_C64149").val(item.G3198_C64149).trigger("change");  
                $("#G3198_C64249").val(item.G3198_C64249).trigger("change");    
                if(item.G3198_C64151 == 1){
                    $("#G3198_C64151").attr('checked', true);
                }    
                if(item.G3198_C64152 == 1){
                    $("#G3198_C64152").attr('checked', true);
                }  
                $("#G3198_C64153").val(item.G3198_C64153).trigger("change");    
                if(item.G3198_C64155 == 1){
                    $("#G3198_C64155").attr('checked', true);
                }    
                if(item.G3198_C64156 == 1){
                    $("#G3198_C64156").attr('checked', true);
                }  
                $("#G3198_C64157").val(item.G3198_C64157); 
                $("#G3198_C64158").val(item.G3198_C64158).trigger("change");  
                $("#G3198_C64159").val(item.G3198_C64159); 
                $("#G3198_C64160").val(item.G3198_C64160); 
                $("#G3198_C64161").val(item.G3198_C64161).trigger("change");    
                if(item.G3198_C64162 == 1){
                    $("#G3198_C64162").attr('checked', true);
                }    
                if(item.G3198_C64163 == 1){
                    $("#G3198_C64163").attr('checked', true);
                }    
                if(item.G3198_C64164 == 1){
                    $("#G3198_C64164").attr('checked', true);
                }  
                $("#G3198_C64165").val(item.G3198_C64165).trigger("change");    
                if(item.G3198_C64166 == 1){
                    $("#G3198_C64166").attr('checked', true);
                }  
                $("#G3198_C64167").val(item.G3198_C64167).trigger("change");  
                $("#G3198_C64168").val(item.G3198_C64168).trigger("change");  
                $("#G3198_C64169").val(item.G3198_C64169).trigger("change");    
                if(item.G3198_C64170 == 1){
                    $("#G3198_C64170").attr('checked', true);
                }  
                $("#G3198_C64171").val(item.G3198_C64171); 
                $("#G3198_C64172").val(item.G3198_C64172).trigger("change");  
                $("#G3198_C64173").val(item.G3198_C64173).trigger("change");  
                $("#G3198_C64174").val(item.G3198_C64174).trigger("change");  
                $("#G3198_C64175").val(item.G3198_C64175).trigger("change");  
                $("#G3198_C64176").val(item.G3198_C64176).trigger("change");    
                if(item.G3198_C64177 == 1){
                    $("#G3198_C64177").attr('checked', true);
                }  
                $("#G3198_C64178").val(item.G3198_C64178).trigger("change");  
                $("#G3198_C64179").val(item.G3198_C64179).trigger("change");  
                $("#G3198_C64180").val(item.G3198_C64180).trigger("change");    
                if(item.G3198_C64181 == 1){
                    $("#G3198_C64181").attr('checked', true);
                }    
                if(item.G3198_C64182 == 1){
                    $("#G3198_C64182").attr('checked', true);
                }  
                $("#G3198_C64183").val(item.G3198_C64183).trigger("change");  
                $("#G3198_C64184").val(item.G3198_C64184);   
                if(item.G3198_C64185 == 1){
                    $("#G3198_C64185").attr('checked', true);
                } 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G3198_C64145").select2();

    $("#G3198_C64147").select2();

    $("#G3198_C64148").select2();

    $("#G3198_C64149").select2();

    $("#G3198_C64249").select2();

    $("#G3198_C64153").select2();

    $("#G3198_C64158").select2();

    $("#G3198_C64161").select2();

    $("#G3198_C64165").select2();

    $("#G3198_C64167").select2();

    $("#G3198_C64168").select2();

    $("#G3198_C64169").select2();

    $("#G3198_C64172").select2();

    $("#G3198_C64173").select2();

    $("#G3198_C64174").select2();

    $("#G3198_C64175").select2();

    $("#G3198_C64176").select2();

    $("#G3198_C64178").select2();

    $("#G3198_C64179").select2();

    $("#G3198_C64180").select2();

    $("#G3198_C64183").select2();
        //datepickers
        

        $("#G3198_C64157").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G3198_C64154").numeric();
                
        $("#G3198_C64171").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G3198_C64145").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Especialidad 

    $("#G3198_C64147").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3946' , idPadre : $(this).val() },
            success : function(data){
                var optG3198_C64148 = $("#G3198_C64148").attr("opt");
                $("#G3198_C64148").html(data);
                if (optG3198_C64148 != null) {
                    $("#G3198_C64148").val(optG3198_C64148).trigger("change");
                }
            }
        });
        
    });

    //function para Especialista que requiere 

    $("#G3198_C64148").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para  1 vez /control 

    $("#G3198_C64149").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DISPONIBILIDAD DE AGENDAMIENTO 

    $("#G3198_C64249").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Señor(a), usted tiene a mano Autorización y Orden medica?    

    $("#G3198_C64153").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G3198_C64153 option:selected").attr('respuesta');
        $("#respuesta_G3198_C64153").val(respuesta);
        

    });

    //function para Remitido A  

    $("#G3198_C64158").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Vigencia de autorización  

    $("#G3198_C64161").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para A. El paciente tiene algún impedimento o dificultad para caminar? 

    $("#G3198_C64165").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para B. El paciente tiene en este momento algún aparato o soporte ortopédico Como: ¿yeso, bastón, muletas, caminador o silla de ruedas?"        

    $("#G3198_C64167").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para C. ¿El paciente tiene discapacidad total o parcial de tipo visual o auditiva? 

    $("#G3198_C64168").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para D. ¿El paciente tiene alguna limitación o discapacidad neurológica? Como: retardo mental" 

    $("#G3198_C64169").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 2. ¿Ha viajado los últimos 30 días fuera del país? 

    $("#G3198_C64172").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 3. ¿Ha tenido contacto con personas diagnosticadas con COVID 19? 

    $("#G3198_C64173").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 4. ¿Ha tenido contacto con personas que viajaron al exterior los últimos 30 días? 

    $("#G3198_C64174").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 5. ¿Tiene o tuvo gripa durante las últimas tres semanas? 

    $("#G3198_C64175").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 6. Ha presentado alguno de esto síntomas: Tos, dolor de garganta, ¿fiebre? 

    $("#G3198_C64176").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 1. Es persona diabética, 

    $("#G3198_C64178").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 2. Sufre problemas cardio respiratorios, 

    $("#G3198_C64179").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 3. Es hipertensa. 

    $("#G3198_C64180").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Está satisfecho con la información brindada?    

    $("#G3198_C64183").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G3198_C64154").val(item.G3198_C64154);
      
                                                if(item.G3198_C64146 == 1){
                                                   $("#G3198_C64146").attr('checked', true);
                                                } 
 
                                                $("#G3198_C64143").val(item.G3198_C64143);
 
                                                $("#G3198_C64144").val(item.G3198_C64144);
 
                    $("#G3198_C64145").val(item.G3198_C64145).trigger("change"); 
      
                                                if(item.G3198_C64150 == 1){
                                                   $("#G3198_C64150").attr('checked', true);
                                                } 
 
                    $("#G3198_C64147").val(item.G3198_C64147).trigger("change"); 
 
                    $("#G3198_C64148").attr("opt",item.G3198_C64148); 
 
                    $("#G3198_C64149").val(item.G3198_C64149).trigger("change"); 
 
                    $("#G3198_C64249").val(item.G3198_C64249).trigger("change"); 
      
                                                if(item.G3198_C64151 == 1){
                                                   $("#G3198_C64151").attr('checked', true);
                                                } 
      
                                                if(item.G3198_C64152 == 1){
                                                   $("#G3198_C64152").attr('checked', true);
                                                } 
 
                    $("#G3198_C64153").val(item.G3198_C64153).trigger("change"); 
      
                                                if(item.G3198_C64155 == 1){
                                                   $("#G3198_C64155").attr('checked', true);
                                                } 
      
                                                if(item.G3198_C64156 == 1){
                                                   $("#G3198_C64156").attr('checked', true);
                                                } 
 
                                                $("#G3198_C64157").val(item.G3198_C64157);
 
                    $("#G3198_C64158").val(item.G3198_C64158).trigger("change"); 
 
                                                $("#G3198_C64159").val(item.G3198_C64159);
 
                                                $("#G3198_C64160").val(item.G3198_C64160);
 
                    $("#G3198_C64161").val(item.G3198_C64161).trigger("change"); 
      
                                                if(item.G3198_C64162 == 1){
                                                   $("#G3198_C64162").attr('checked', true);
                                                } 
      
                                                if(item.G3198_C64163 == 1){
                                                   $("#G3198_C64163").attr('checked', true);
                                                } 
      
                                                if(item.G3198_C64164 == 1){
                                                   $("#G3198_C64164").attr('checked', true);
                                                } 
 
                    $("#G3198_C64165").val(item.G3198_C64165).trigger("change"); 
      
                                                if(item.G3198_C64166 == 1){
                                                   $("#G3198_C64166").attr('checked', true);
                                                } 
 
                    $("#G3198_C64167").val(item.G3198_C64167).trigger("change"); 
 
                    $("#G3198_C64168").val(item.G3198_C64168).trigger("change"); 
 
                    $("#G3198_C64169").val(item.G3198_C64169).trigger("change"); 
      
                                                if(item.G3198_C64170 == 1){
                                                   $("#G3198_C64170").attr('checked', true);
                                                } 
 
                                                $("#G3198_C64171").val(item.G3198_C64171);
 
                    $("#G3198_C64172").val(item.G3198_C64172).trigger("change"); 
 
                    $("#G3198_C64173").val(item.G3198_C64173).trigger("change"); 
 
                    $("#G3198_C64174").val(item.G3198_C64174).trigger("change"); 
 
                    $("#G3198_C64175").val(item.G3198_C64175).trigger("change"); 
 
                    $("#G3198_C64176").val(item.G3198_C64176).trigger("change"); 
      
                                                if(item.G3198_C64177 == 1){
                                                   $("#G3198_C64177").attr('checked', true);
                                                } 
 
                    $("#G3198_C64178").val(item.G3198_C64178).trigger("change"); 
 
                    $("#G3198_C64179").val(item.G3198_C64179).trigger("change"); 
 
                    $("#G3198_C64180").val(item.G3198_C64180).trigger("change"); 
      
                                                if(item.G3198_C64181 == 1){
                                                   $("#G3198_C64181").attr('checked', true);
                                                } 
      
                                                if(item.G3198_C64182 == 1){
                                                   $("#G3198_C64182").attr('checked', true);
                                                } 
 
                    $("#G3198_C64183").val(item.G3198_C64183).trigger("change"); 
 
                                                $("#G3198_C64184").val(item.G3198_C64184);
      
                                                if(item.G3198_C64185 == 1){
                                                   $("#G3198_C64185").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }   
                sendMessage('Cierrame');     
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G3198_C64147").val()==0 || $("#G3198_C64147").val() == null || $("#G3198_C64147").val() == -1) && $("#G3198_C64147").prop("disabled") == false){
                alertify.error('Especialidad debe ser diligenciado');
                $("#G3198_C64147").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64148").val()==0 || $("#G3198_C64148").val() == null || $("#G3198_C64148").val() == -1) && $("#G3198_C64148").prop("disabled") == false){
                alertify.error('Especialista que requiere debe ser diligenciado');
                $("#G3198_C64148").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64149").val()==0 || $("#G3198_C64149").val() == null || $("#G3198_C64149").val() == -1) && $("#G3198_C64149").prop("disabled") == false){
                alertify.error(' 1 vez /control debe ser diligenciado');
                $("#G3198_C64149").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64249").val()==0 || $("#G3198_C64249").val() == null || $("#G3198_C64249").val() == -1) && $("#G3198_C64249").prop("disabled") == false){
                alertify.error('DISPONIBILIDAD DE AGENDAMIENTO debe ser diligenciado');
                $("#G3198_C64249").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64153").val()==0 || $("#G3198_C64153").val() == null || $("#G3198_C64153").val() == -1) && $("#G3198_C64153").prop("disabled") == false){
                alertify.error('¿Señor(a), usted tiene a mano Autorización y Orden medica?			 debe ser diligenciado');
                $("#G3198_C64153").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64157").val() == "") && $("#G3198_C64157").prop("disabled") == false){
                alertify.error('Fecha de autorización  debe ser diligenciado');
                $("#G3198_C64157").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64159").val() == "") && $("#G3198_C64159").prop("disabled") == false){
                alertify.error('Codigo de autorización  debe ser diligenciado');
                $("#G3198_C64159").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64160").val() == "") && $("#G3198_C64160").prop("disabled") == false){
                alertify.error('Copago  debe ser diligenciado');
                $("#G3198_C64160").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64161").val()==0 || $("#G3198_C64161").val() == null || $("#G3198_C64161").val() == -1) && $("#G3198_C64161").prop("disabled") == false){
                alertify.error('Vigencia de autorización  debe ser diligenciado');
                $("#G3198_C64161").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G3198_C64183").val()==0 || $("#G3198_C64183").val() == null || $("#G3198_C64183").val() == -1) && $("#G3198_C64183").prop("disabled") == false){
                alertify.error('Está satisfecho con la información brindada?			 debe ser diligenciado');
                $("#G3198_C64183").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            /*let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });

            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }*/

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>

         mostrarBotones();
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','ID GESTION','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','Especialidad','Especialista que requiere',' 1 vez /control','DISPONIBILIDAD DE AGENDAMIENTO','¿Señor(a), usted tiene a mano Autorización y Orden medica?			','Fecha de autorización ','Remitido A ','Codigo de autorización ','Copago ','Vigencia de autorización ','A. El paciente tiene algún impedimento o dificultad para caminar?','B. El paciente tiene en este momento algún aparato o soporte ortopédico Como: ¿yeso, bastón, muletas, caminador o silla de ruedas?"			 			','C. ¿El paciente tiene discapacidad total o parcial de tipo visual o auditiva?','D. ¿El paciente tiene alguna limitación o discapacidad neurológica? Como: retardo mental"','1. Cuantos años tiene?','2. ¿Ha viajado los últimos 30 días fuera del país?','3. ¿Ha tenido contacto con personas diagnosticadas con COVID 19?','4. ¿Ha tenido contacto con personas que viajaron al exterior los últimos 30 días?','5. ¿Tiene o tuvo gripa durante las últimas tres semanas?','6. Ha presentado alguno de esto síntomas: Tos, dolor de garganta, ¿fiebre?','1. Es persona diabética,','2. Sufre problemas cardio respiratorios,','3. Es hipertensa.','Está satisfecho con la información brindada?			','Observaciones 			 			 			'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                    ,
                    {  
                        name:'G3198_C64154', 
                        index:'G3198_C64154', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3198_C64143', 
                        index: 'G3198_C64143', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3198_C64144', 
                        index: 'G3198_C64144', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3198_C64145', 
                        index:'G3198_C64145', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3941&campo=G3198_C64145'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64147', 
                        index:'G3198_C64147', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3945&campo=G3198_C64147'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64148', 
                        index:'G3198_C64148', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3946&campo=G3198_C64148'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64149', 
                        index:'G3198_C64149', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3895&campo=G3198_C64149'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64249', 
                        index:'G3198_C64249', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3892&campo=G3198_C64249'
                        }
                    }

                    ,
                    {  
                        name:'G3198_C64157', 
                        index:'G3198_C64157', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64158', 
                        index:'G3198_C64158', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3955&campo=G3198_C64158'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64159', 
                        index: 'G3198_C64159', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3198_C64160', 
                        index: 'G3198_C64160', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G3198_C64161', 
                        index:'G3198_C64161', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3894&campo=G3198_C64161'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64165', 
                        index:'G3198_C64165', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64165'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64167', 
                        index:'G3198_C64167', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64167'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64168', 
                        index:'G3198_C64168', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64168'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64169', 
                        index:'G3198_C64169', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64169'
                        }
                    }
 
                    ,
                    {  
                        name:'G3198_C64171', 
                        index:'G3198_C64171', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G3198_C64172', 
                        index:'G3198_C64172', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64172'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64173', 
                        index:'G3198_C64173', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64173'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64174', 
                        index:'G3198_C64174', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64174'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64175', 
                        index:'G3198_C64175', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64175'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64176', 
                        index:'G3198_C64176', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64176'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64178', 
                        index:'G3198_C64178', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64178'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64179', 
                        index:'G3198_C64179', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64179'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64180', 
                        index:'G3198_C64180', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64180'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64183', 
                        index:'G3198_C64183', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3896&campo=G3198_C64183'
                        }
                    }

                    ,
                    { 
                        name:'G3198_C64184', 
                        index:'G3198_C64184', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G3198_C64154',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G3198_C64154").val(item.G3198_C64154);
    
                        if(item.G3198_C64146 == 1){
                           $("#G3198_C64146").attr('checked', true);
                        } 

                        $("#G3198_C64143").val(item.G3198_C64143);

                        $("#G3198_C64144").val(item.G3198_C64144);
 
                    $("#G3198_C64145").val(item.G3198_C64145).trigger("change"); 
    
                        if(item.G3198_C64150 == 1){
                           $("#G3198_C64150").attr('checked', true);
                        } 
 
                    $("#G3198_C64147").val(item.G3198_C64147).trigger("change"); 
 
                    $("#G3198_C64148").attr("opt",item.G3198_C64148); 
 
                    $("#G3198_C64149").val(item.G3198_C64149).trigger("change"); 
 
                    $("#G3198_C64249").val(item.G3198_C64249).trigger("change"); 
    
                        if(item.G3198_C64151 == 1){
                           $("#G3198_C64151").attr('checked', true);
                        } 
    
                        if(item.G3198_C64152 == 1){
                           $("#G3198_C64152").attr('checked', true);
                        } 
 
                    $("#G3198_C64153").val(item.G3198_C64153).trigger("change"); 
    
                        if(item.G3198_C64155 == 1){
                           $("#G3198_C64155").attr('checked', true);
                        } 
    
                        if(item.G3198_C64156 == 1){
                           $("#G3198_C64156").attr('checked', true);
                        } 

                        $("#G3198_C64157").val(item.G3198_C64157);
 
                    $("#G3198_C64158").val(item.G3198_C64158).trigger("change"); 

                        $("#G3198_C64159").val(item.G3198_C64159);

                        $("#G3198_C64160").val(item.G3198_C64160);
 
                    $("#G3198_C64161").val(item.G3198_C64161).trigger("change"); 
    
                        if(item.G3198_C64162 == 1){
                           $("#G3198_C64162").attr('checked', true);
                        } 
    
                        if(item.G3198_C64163 == 1){
                           $("#G3198_C64163").attr('checked', true);
                        } 
    
                        if(item.G3198_C64164 == 1){
                           $("#G3198_C64164").attr('checked', true);
                        } 
 
                    $("#G3198_C64165").val(item.G3198_C64165).trigger("change"); 
    
                        if(item.G3198_C64166 == 1){
                           $("#G3198_C64166").attr('checked', true);
                        } 
 
                    $("#G3198_C64167").val(item.G3198_C64167).trigger("change"); 
 
                    $("#G3198_C64168").val(item.G3198_C64168).trigger("change"); 
 
                    $("#G3198_C64169").val(item.G3198_C64169).trigger("change"); 
    
                        if(item.G3198_C64170 == 1){
                           $("#G3198_C64170").attr('checked', true);
                        } 

                        $("#G3198_C64171").val(item.G3198_C64171);
 
                    $("#G3198_C64172").val(item.G3198_C64172).trigger("change"); 
 
                    $("#G3198_C64173").val(item.G3198_C64173).trigger("change"); 
 
                    $("#G3198_C64174").val(item.G3198_C64174).trigger("change"); 
 
                    $("#G3198_C64175").val(item.G3198_C64175).trigger("change"); 
 
                    $("#G3198_C64176").val(item.G3198_C64176).trigger("change"); 
    
                        if(item.G3198_C64177 == 1){
                           $("#G3198_C64177").attr('checked', true);
                        } 
 
                    $("#G3198_C64178").val(item.G3198_C64178).trigger("change"); 
 
                    $("#G3198_C64179").val(item.G3198_C64179).trigger("change"); 
 
                    $("#G3198_C64180").val(item.G3198_C64180).trigger("change"); 
    
                        if(item.G3198_C64181 == 1){
                           $("#G3198_C64181").attr('checked', true);
                        } 
    
                        if(item.G3198_C64182 == 1){
                           $("#G3198_C64182").attr('checked', true);
                        } 
 
                    $("#G3198_C64183").val(item.G3198_C64183).trigger("change"); 

                        $("#G3198_C64184").val(item.G3198_C64184);
    
                        if(item.G3198_C64185 == 1){
                           $("#G3198_C64185").attr('checked', true);
                        } 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
