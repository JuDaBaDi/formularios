<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G3198_ConsInte__b, G3198_FechaInsercion , G3198_Usuario ,  G3198_CodigoMiembro  , G3198_PoblacionOrigen , G3198_EstadoDiligenciamiento ,  G3198_IdLlamada , G3198_C64154 as principal ,G3198_C64154,G3198_C64143,G3198_C64144,G3198_C64145,G3198_C64147,G3198_C64148,G3198_C64149,G3198_C64249,G3198_C64153,G3198_C64157,G3198_C64158,G3198_C64159,G3198_C64160,G3198_C64161,G3198_C64165,G3198_C64167,G3198_C64168,G3198_C64169,G3198_C64171,G3198_C64172,G3198_C64173,G3198_C64174,G3198_C64175,G3198_C64176,G3198_C64178,G3198_C64179,G3198_C64180,G3198_C64183,G3198_C64184 FROM '.$BaseDatos.'.G3198 WHERE G3198_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G3198_C64154'] = $key->G3198_C64154;

                $datos[$i]['G3198_C64143'] = $key->G3198_C64143;

                $datos[$i]['G3198_C64144'] = $key->G3198_C64144;

                $datos[$i]['G3198_C64145'] = $key->G3198_C64145;

                $datos[$i]['G3198_C64147'] = $key->G3198_C64147;

                $datos[$i]['G3198_C64148'] = $key->G3198_C64148;

                $datos[$i]['G3198_C64149'] = $key->G3198_C64149;

                $datos[$i]['G3198_C64249'] = $key->G3198_C64249;

                $datos[$i]['G3198_C64153'] = $key->G3198_C64153;

                $datos[$i]['G3198_C64157'] = explode(' ', $key->G3198_C64157)[0];

                $datos[$i]['G3198_C64158'] = $key->G3198_C64158;

                $datos[$i]['G3198_C64159'] = $key->G3198_C64159;

                $datos[$i]['G3198_C64160'] = $key->G3198_C64160;

                $datos[$i]['G3198_C64161'] = $key->G3198_C64161;

                $datos[$i]['G3198_C64165'] = $key->G3198_C64165;

                $datos[$i]['G3198_C64167'] = $key->G3198_C64167;

                $datos[$i]['G3198_C64168'] = $key->G3198_C64168;

                $datos[$i]['G3198_C64169'] = $key->G3198_C64169;

                $datos[$i]['G3198_C64171'] = $key->G3198_C64171;

                $datos[$i]['G3198_C64172'] = $key->G3198_C64172;

                $datos[$i]['G3198_C64173'] = $key->G3198_C64173;

                $datos[$i]['G3198_C64174'] = $key->G3198_C64174;

                $datos[$i]['G3198_C64175'] = $key->G3198_C64175;

                $datos[$i]['G3198_C64176'] = $key->G3198_C64176;

                $datos[$i]['G3198_C64178'] = $key->G3198_C64178;

                $datos[$i]['G3198_C64179'] = $key->G3198_C64179;

                $datos[$i]['G3198_C64180'] = $key->G3198_C64180;

                $datos[$i]['G3198_C64183'] = $key->G3198_C64183;

                $datos[$i]['G3198_C64184'] = $key->G3198_C64184;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3198";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G3198_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G3198_ConsInte__b as id,  G3198_C64154 as camp1 , G3198_C64154 as camp2 
                     FROM ".$BaseDatos.".G3198  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G3198_ConsInte__b as id,  G3198_C64154 as camp1 , G3198_C64154 as camp2  
                    FROM ".$BaseDatos.".G3198  JOIN ".$BaseDatos.".G3198_M".$_POST['muestra']." ON G3198_ConsInte__b = G3198_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G3198_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G3198_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G3198_C64154 LIKE '%".$B."%' OR G3198_C64154 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G3198_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G3198");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G3198_ConsInte__b, G3198_FechaInsercion , G3198_Usuario ,  G3198_CodigoMiembro  , G3198_PoblacionOrigen , G3198_EstadoDiligenciamiento ,  G3198_IdLlamada , G3198_C64154 as principal ,G3198_C64154,G3198_C64143,G3198_C64144, a.LISOPC_Nombre____b as G3198_C64145, b.LISOPC_Nombre____b as G3198_C64147, c.LISOPC_Nombre____b as G3198_C64148, d.LISOPC_Nombre____b as G3198_C64149, e.LISOPC_Nombre____b as G3198_C64249,G3198_C64153,G3198_C64157, f.LISOPC_Nombre____b as G3198_C64158,G3198_C64159,G3198_C64160, g.LISOPC_Nombre____b as G3198_C64161, h.LISOPC_Nombre____b as G3198_C64165, i.LISOPC_Nombre____b as G3198_C64167, j.LISOPC_Nombre____b as G3198_C64168, k.LISOPC_Nombre____b as G3198_C64169,G3198_C64171, l.LISOPC_Nombre____b as G3198_C64172, m.LISOPC_Nombre____b as G3198_C64173, n.LISOPC_Nombre____b as G3198_C64174, o.LISOPC_Nombre____b as G3198_C64175, p.LISOPC_Nombre____b as G3198_C64176, q.LISOPC_Nombre____b as G3198_C64178, r.LISOPC_Nombre____b as G3198_C64179, s.LISOPC_Nombre____b as G3198_C64180, t.LISOPC_Nombre____b as G3198_C64183,G3198_C64184 FROM '.$BaseDatos.'.G3198 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G3198_C64145 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G3198_C64147 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G3198_C64148 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G3198_C64149 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G3198_C64249 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G3198_C64158 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G3198_C64161 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G3198_C64165 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G3198_C64167 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G3198_C64168 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G3198_C64169 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G3198_C64172 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G3198_C64173 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G3198_C64174 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G3198_C64175 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G3198_C64176 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as q ON q.LISOPC_ConsInte__b =  G3198_C64178 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as r ON r.LISOPC_ConsInte__b =  G3198_C64179 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as s ON s.LISOPC_ConsInte__b =  G3198_C64180 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as t ON t.LISOPC_ConsInte__b =  G3198_C64183';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G3198_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G3198_ConsInte__b , ($fila->G3198_C64154) , ($fila->G3198_C64143) , ($fila->G3198_C64144) , ($fila->G3198_C64145) , ($fila->G3198_C64147) , ($fila->G3198_C64148) , ($fila->G3198_C64149) , ($fila->G3198_C64249) , ($fila->G3198_C64153) , explode(' ', $fila->G3198_C64157)[0] , ($fila->G3198_C64158) , ($fila->G3198_C64159) , ($fila->G3198_C64160) , ($fila->G3198_C64161) , ($fila->G3198_C64165) , ($fila->G3198_C64167) , ($fila->G3198_C64168) , ($fila->G3198_C64169) , ($fila->G3198_C64171) , ($fila->G3198_C64172) , ($fila->G3198_C64173) , ($fila->G3198_C64174) , ($fila->G3198_C64175) , ($fila->G3198_C64176) , ($fila->G3198_C64178) , ($fila->G3198_C64179) , ($fila->G3198_C64180) , ($fila->G3198_C64183) , ($fila->G3198_C64184) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G3198 WHERE G3198_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 3198";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G3198_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G3198_ConsInte__b as id,  G3198_C64154 as camp1 , G3198_C64154 as camp2  FROM '.$BaseDatos.'.G3198 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G3198_ConsInte__b as id,  G3198_C64154 as camp1 , G3198_C64154 as camp2  
                    FROM ".$BaseDatos.".G3198  JOIN ".$BaseDatos.".G3198_M".$_POST['muestra']." ON G3198_ConsInte__b = G3198_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G3198_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G3198_C64154 LIKE "%'.$B.'%" OR G3198_C64154 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G3198_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G3198 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G3198(";
            $LsqlV = " VALUES ("; 
  
            $G3198_C64154 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3198_C64154"])){
                if($_POST["G3198_C64154"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3198_C64154 = $_POST["G3198_C64154"];
                    $LsqlU .= $separador." G3198_C64154 = ".$G3198_C64154."";
                    $LsqlI .= $separador." G3198_C64154";
                    $LsqlV .= $separador.$G3198_C64154;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3198_C64146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64146 = '".$_POST["G3198_C64146"]."'";
                $LsqlI .= $separador."G3198_C64146";
                $LsqlV .= $separador."'".$_POST["G3198_C64146"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64143"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64143 = '".$_POST["G3198_C64143"]."'";
                $LsqlI .= $separador."G3198_C64143";
                $LsqlV .= $separador."'".$_POST["G3198_C64143"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64144 = '".$_POST["G3198_C64144"]."'";
                $LsqlI .= $separador."G3198_C64144";
                $LsqlV .= $separador."'".$_POST["G3198_C64144"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64145"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64145 = '".$_POST["G3198_C64145"]."'";
                $LsqlI .= $separador."G3198_C64145";
                $LsqlV .= $separador."'".$_POST["G3198_C64145"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64150"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64150 = '".$_POST["G3198_C64150"]."'";
                $LsqlI .= $separador."G3198_C64150";
                $LsqlV .= $separador."'".$_POST["G3198_C64150"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64147"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64147 = '".$_POST["G3198_C64147"]."'";
                $LsqlI .= $separador."G3198_C64147";
                $LsqlV .= $separador."'".$_POST["G3198_C64147"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64148"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64148 = '".$_POST["G3198_C64148"]."'";
                $LsqlI .= $separador."G3198_C64148";
                $LsqlV .= $separador."'".$_POST["G3198_C64148"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64149"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64149 = '".$_POST["G3198_C64149"]."'";
                $LsqlI .= $separador."G3198_C64149";
                $LsqlV .= $separador."'".$_POST["G3198_C64149"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64249"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64249 = '".$_POST["G3198_C64249"]."'";
                $LsqlI .= $separador."G3198_C64249";
                $LsqlV .= $separador."'".$_POST["G3198_C64249"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64151"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64151 = '".$_POST["G3198_C64151"]."'";
                $LsqlI .= $separador."G3198_C64151";
                $LsqlV .= $separador."'".$_POST["G3198_C64151"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64152"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64152 = '".$_POST["G3198_C64152"]."'";
                $LsqlI .= $separador."G3198_C64152";
                $LsqlV .= $separador."'".$_POST["G3198_C64152"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64153 = '".$_POST["G3198_C64153"]."'";
                $LsqlI .= $separador."G3198_C64153";
                $LsqlV .= $separador."'".$_POST["G3198_C64153"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64155"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64155 = '".$_POST["G3198_C64155"]."'";
                $LsqlI .= $separador."G3198_C64155";
                $LsqlV .= $separador."'".$_POST["G3198_C64155"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64156"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64156 = '".$_POST["G3198_C64156"]."'";
                $LsqlI .= $separador."G3198_C64156";
                $LsqlV .= $separador."'".$_POST["G3198_C64156"]."'";
                $validar = 1;
            }
             
 
            $G3198_C64157 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G3198_C64157"])){    
                if($_POST["G3198_C64157"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G3198_C64157"]);
                    if(count($tieneHora) > 1){
                        $G3198_C64157 = "'".$_POST["G3198_C64157"]."'";
                    }else{
                        $G3198_C64157 = "'".str_replace(' ', '',$_POST["G3198_C64157"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G3198_C64157 = ".$G3198_C64157;
                    $LsqlI .= $separador." G3198_C64157";
                    $LsqlV .= $separador.$G3198_C64157;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3198_C64158"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64158 = '".$_POST["G3198_C64158"]."'";
                $LsqlI .= $separador."G3198_C64158";
                $LsqlV .= $separador."'".$_POST["G3198_C64158"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64159 = '".$_POST["G3198_C64159"]."'";
                $LsqlI .= $separador."G3198_C64159";
                $LsqlV .= $separador."'".$_POST["G3198_C64159"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64160"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64160 = '".$_POST["G3198_C64160"]."'";
                $LsqlI .= $separador."G3198_C64160";
                $LsqlV .= $separador."'".$_POST["G3198_C64160"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64161"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64161 = '".$_POST["G3198_C64161"]."'";
                $LsqlI .= $separador."G3198_C64161";
                $LsqlV .= $separador."'".$_POST["G3198_C64161"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64162"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64162 = '".$_POST["G3198_C64162"]."'";
                $LsqlI .= $separador."G3198_C64162";
                $LsqlV .= $separador."'".$_POST["G3198_C64162"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64163"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64163 = '".$_POST["G3198_C64163"]."'";
                $LsqlI .= $separador."G3198_C64163";
                $LsqlV .= $separador."'".$_POST["G3198_C64163"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64164"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64164 = '".$_POST["G3198_C64164"]."'";
                $LsqlI .= $separador."G3198_C64164";
                $LsqlV .= $separador."'".$_POST["G3198_C64164"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64165"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64165 = '".$_POST["G3198_C64165"]."'";
                $LsqlI .= $separador."G3198_C64165";
                $LsqlV .= $separador."'".$_POST["G3198_C64165"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64166"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64166 = '".$_POST["G3198_C64166"]."'";
                $LsqlI .= $separador."G3198_C64166";
                $LsqlV .= $separador."'".$_POST["G3198_C64166"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64167"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64167 = '".$_POST["G3198_C64167"]."'";
                $LsqlI .= $separador."G3198_C64167";
                $LsqlV .= $separador."'".$_POST["G3198_C64167"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64168"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64168 = '".$_POST["G3198_C64168"]."'";
                $LsqlI .= $separador."G3198_C64168";
                $LsqlV .= $separador."'".$_POST["G3198_C64168"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64169"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64169 = '".$_POST["G3198_C64169"]."'";
                $LsqlI .= $separador."G3198_C64169";
                $LsqlV .= $separador."'".$_POST["G3198_C64169"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64170"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64170 = '".$_POST["G3198_C64170"]."'";
                $LsqlI .= $separador."G3198_C64170";
                $LsqlV .= $separador."'".$_POST["G3198_C64170"]."'";
                $validar = 1;
            }
             
  
            $G3198_C64171 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G3198_C64171"])){
                if($_POST["G3198_C64171"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G3198_C64171 = $_POST["G3198_C64171"];
                    $LsqlU .= $separador." G3198_C64171 = ".$G3198_C64171."";
                    $LsqlI .= $separador." G3198_C64171";
                    $LsqlV .= $separador.$G3198_C64171;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G3198_C64172"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64172 = '".$_POST["G3198_C64172"]."'";
                $LsqlI .= $separador."G3198_C64172";
                $LsqlV .= $separador."'".$_POST["G3198_C64172"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64173"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64173 = '".$_POST["G3198_C64173"]."'";
                $LsqlI .= $separador."G3198_C64173";
                $LsqlV .= $separador."'".$_POST["G3198_C64173"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64174"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64174 = '".$_POST["G3198_C64174"]."'";
                $LsqlI .= $separador."G3198_C64174";
                $LsqlV .= $separador."'".$_POST["G3198_C64174"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64175"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64175 = '".$_POST["G3198_C64175"]."'";
                $LsqlI .= $separador."G3198_C64175";
                $LsqlV .= $separador."'".$_POST["G3198_C64175"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64176"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64176 = '".$_POST["G3198_C64176"]."'";
                $LsqlI .= $separador."G3198_C64176";
                $LsqlV .= $separador."'".$_POST["G3198_C64176"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64177"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64177 = '".$_POST["G3198_C64177"]."'";
                $LsqlI .= $separador."G3198_C64177";
                $LsqlV .= $separador."'".$_POST["G3198_C64177"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64178"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64178 = '".$_POST["G3198_C64178"]."'";
                $LsqlI .= $separador."G3198_C64178";
                $LsqlV .= $separador."'".$_POST["G3198_C64178"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64179"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64179 = '".$_POST["G3198_C64179"]."'";
                $LsqlI .= $separador."G3198_C64179";
                $LsqlV .= $separador."'".$_POST["G3198_C64179"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64180"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64180 = '".$_POST["G3198_C64180"]."'";
                $LsqlI .= $separador."G3198_C64180";
                $LsqlV .= $separador."'".$_POST["G3198_C64180"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64181"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64181 = '".$_POST["G3198_C64181"]."'";
                $LsqlI .= $separador."G3198_C64181";
                $LsqlV .= $separador."'".$_POST["G3198_C64181"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64182"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64182 = '".$_POST["G3198_C64182"]."'";
                $LsqlI .= $separador."G3198_C64182";
                $LsqlV .= $separador."'".$_POST["G3198_C64182"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64183"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64183 = '".$_POST["G3198_C64183"]."'";
                $LsqlI .= $separador."G3198_C64183";
                $LsqlV .= $separador."'".$_POST["G3198_C64183"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64184"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64184 = '".$_POST["G3198_C64184"]."'";
                $LsqlI .= $separador."G3198_C64184";
                $LsqlV .= $separador."'".$_POST["G3198_C64184"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G3198_C64185"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_C64185 = '".$_POST["G3198_C64185"]."'";
                $LsqlI .= $separador."G3198_C64185";
                $LsqlV .= $separador."'".$_POST["G3198_C64185"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G3198_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G3198_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G3198_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G3198_Usuario , G3198_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G3198_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G3198 WHERE G3198_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G3198 SET G3198_UltiGest__b =-14, G3198_GesMasImp_b =-14, G3198_TipoReintentoUG_b =0, G3198_TipoReintentoGMI_b =0, G3198_ClasificacionUG_b =3, G3198_ClasificacionGMI_b =3, G3198_EstadoUG_b =-14, G3198_EstadoGMI_b =-14, G3198_CantidadIntentos =0, G3198_CantidadIntentosGMI_b =0 WHERE G3198_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
