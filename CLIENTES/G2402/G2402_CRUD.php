<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 55273")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 55273");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2402_ConsInte__b, G2402_FechaInsercion , G2402_Usuario ,  G2402_CodigoMiembro  , G2402_PoblacionOrigen , G2402_EstadoDiligenciamiento ,  G2402_IdLlamada , G2402_C47272 as principal ,G2402_C47271,G2402_C47272,G2402_C47273,G2402_C47274,G2402_C47275,G2402_C47276,G2402_C57809,G2402_C57810,G2402_C57811,G2402_C55273,G2402_C47260,G2402_C47261,G2402_C47262,G2402_C47263,G2402_C47264,G2402_C47265,G2402_C47266,G2402_C47267,G2402_C47268 FROM '.$BaseDatos.'.G2402 WHERE G2402_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2402_C47271'] = $key->G2402_C47271;

                $datos[$i]['G2402_C47272'] = $key->G2402_C47272;

                $datos[$i]['G2402_C47273'] = $key->G2402_C47273;

                $datos[$i]['G2402_C47274'] = $key->G2402_C47274;

                $datos[$i]['G2402_C47275'] = $key->G2402_C47275;

                $datos[$i]['G2402_C47276'] = $key->G2402_C47276;

                $datos[$i]['G2402_C57809'] = $key->G2402_C57809;

                $datos[$i]['G2402_C57810'] = $key->G2402_C57810;

                $datos[$i]['G2402_C57811'] = $key->G2402_C57811;

                $datos[$i]['G2402_C55273'] = $key->G2402_C55273;

                $datos[$i]['G2402_C47260'] = $key->G2402_C47260;

                $datos[$i]['G2402_C47261'] = $key->G2402_C47261;

                $datos[$i]['G2402_C47262'] = explode(' ', $key->G2402_C47262)[0];
  
                $hora = '';
                if(!is_null($key->G2402_C47263)){
                    $hora = explode(' ', $key->G2402_C47263)[1];
                }

                $datos[$i]['G2402_C47263'] = $hora;

                $datos[$i]['G2402_C47264'] = $key->G2402_C47264;

                $datos[$i]['G2402_C47265'] = $key->G2402_C47265;

                $datos[$i]['G2402_C47266'] = $key->G2402_C47266;

                $datos[$i]['G2402_C47267'] = $key->G2402_C47267;

                $datos[$i]['G2402_C47268'] = $key->G2402_C47268;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2402";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2402_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2402_ConsInte__b as id,  G2402_C47272 as camp1 , G2402_C55273 as camp2 
                     FROM ".$BaseDatos.".G2402  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2402_ConsInte__b as id,  G2402_C47272 as camp1 , G2402_C55273 as camp2  
                    FROM ".$BaseDatos.".G2402  JOIN ".$BaseDatos.".G2402_M".$_POST['muestra']." ON G2402_ConsInte__b = G2402_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2402_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2402_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2402_C47272 LIKE '%".$B."%' OR G2402_C55273 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2402_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2402");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2402_ConsInte__b, G2402_FechaInsercion , G2402_Usuario ,  G2402_CodigoMiembro  , G2402_PoblacionOrigen , G2402_EstadoDiligenciamiento ,  G2402_IdLlamada , G2402_C47272 as principal ,G2402_C47271,G2402_C47272,G2402_C47273,G2402_C47274,G2402_C47275, a.LISOPC_Nombre____b as G2402_C47276, b.LISOPC_Nombre____b as G2402_C57809,G2402_C57810,G2402_C57811,G2402_C55273, c.LISOPC_Nombre____b as G2402_C47260, d.LISOPC_Nombre____b as G2402_C47261,G2402_C47262,G2402_C47263,G2402_C47264,G2402_C47265,G2402_C47266,G2402_C47267,G2402_C47268 FROM '.$BaseDatos.'.G2402 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2402_C47276 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2402_C57809 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2402_C47260 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2402_C47261';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2402_C47263)){
                    $hora_a = explode(' ', $fila->G2402_C47263)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2402_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2402_ConsInte__b , ($fila->G2402_C47271) , ($fila->G2402_C47272) , ($fila->G2402_C47273) , ($fila->G2402_C47274) , ($fila->G2402_C47275) , ($fila->G2402_C47276) , ($fila->G2402_C57809) , ($fila->G2402_C57810) , ($fila->G2402_C57811) , ($fila->G2402_C55273) , ($fila->G2402_C47260) , ($fila->G2402_C47261) , explode(' ', $fila->G2402_C47262)[0] , $hora_a , ($fila->G2402_C47264) , ($fila->G2402_C47265) , ($fila->G2402_C47266) , ($fila->G2402_C47267) , ($fila->G2402_C47268) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2402 WHERE G2402_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2402";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2402_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2402_ConsInte__b as id,  G2402_C47272 as camp1 , G2402_C55273 as camp2  FROM '.$BaseDatos.'.G2402 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2402_ConsInte__b as id,  G2402_C47272 as camp1 , G2402_C55273 as camp2  
                    FROM ".$BaseDatos.".G2402  JOIN ".$BaseDatos.".G2402_M".$_POST['muestra']." ON G2402_ConsInte__b = G2402_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2402_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2402_C47272 LIKE "%'.$B.'%" OR G2402_C55273 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2402_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2402 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2402(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2402_C47271"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47271 = '".$_POST["G2402_C47271"]."'";
                $LsqlI .= $separador."G2402_C47271";
                $LsqlV .= $separador."'".$_POST["G2402_C47271"]."'";
                $validar = 1;
            }
             
  
            $G2402_C47272 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2402_C47272"])){
                if($_POST["G2402_C47272"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2402_C47272 = $_POST["G2402_C47272"];
                    $LsqlU .= $separador." G2402_C47272 = ".$G2402_C47272."";
                    $LsqlI .= $separador." G2402_C47272";
                    $LsqlV .= $separador.$G2402_C47272;
                    $validar = 1;
                }
            }
  
            $G2402_C47273 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2402_C47273"])){
                if($_POST["G2402_C47273"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2402_C47273 = $_POST["G2402_C47273"];
                    $LsqlU .= $separador." G2402_C47273 = ".$G2402_C47273."";
                    $LsqlI .= $separador." G2402_C47273";
                    $LsqlV .= $separador.$G2402_C47273;
                    $validar = 1;
                }
            }
  
            $G2402_C47274 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2402_C47274"])){
                if($_POST["G2402_C47274"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2402_C47274 = $_POST["G2402_C47274"];
                    $LsqlU .= $separador." G2402_C47274 = ".$G2402_C47274."";
                    $LsqlI .= $separador." G2402_C47274";
                    $LsqlV .= $separador.$G2402_C47274;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2402_C47275"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47275 = '".$_POST["G2402_C47275"]."'";
                $LsqlI .= $separador."G2402_C47275";
                $LsqlV .= $separador."'".$_POST["G2402_C47275"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C47276"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47276 = '".$_POST["G2402_C47276"]."'";
                $LsqlI .= $separador."G2402_C47276";
                $LsqlV .= $separador."'".$_POST["G2402_C47276"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C57809"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C57809 = '".$_POST["G2402_C57809"]."'";
                $LsqlI .= $separador."G2402_C57809";
                $LsqlV .= $separador."'".$_POST["G2402_C57809"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C57810"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C57810 = '".$_POST["G2402_C57810"]."'";
                $LsqlI .= $separador."G2402_C57810";
                $LsqlV .= $separador."'".$_POST["G2402_C57810"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C57811"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C57811 = '".$_POST["G2402_C57811"]."'";
                $LsqlI .= $separador."G2402_C57811";
                $LsqlV .= $separador."'".$_POST["G2402_C57811"]."'";
                $validar = 1;
            }
             
  
            $G2402_C55273 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2402_C55273"])){
                if($_POST["G2402_C55273"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2402_C55273 = $_POST["G2402_C55273"];
                    $LsqlU .= $separador." G2402_C55273 = ".$G2402_C55273."";
                    $LsqlI .= $separador." G2402_C55273";
                    $LsqlV .= $separador.$G2402_C55273;
                    $validar = 1;
                }
            }
 
            $G2402_C47260 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2402_C47260 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2402_C47260 = ".$G2402_C47260;
                    $LsqlI .= $separador." G2402_C47260";
                    $LsqlV .= $separador.$G2402_C47260;
                    $validar = 1;

                    
                }
            }
 
            $G2402_C47261 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2402_C47261 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2402_C47261 = ".$G2402_C47261;
                    $LsqlI .= $separador." G2402_C47261";
                    $LsqlV .= $separador.$G2402_C47261;
                    $validar = 1;
                }
            }
 
            $G2402_C47262 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2402_C47262 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2402_C47262 = ".$G2402_C47262;
                    $LsqlI .= $separador." G2402_C47262";
                    $LsqlV .= $separador.$G2402_C47262;
                    $validar = 1;
                }
            }
 
            $G2402_C47263 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2402_C47263 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2402_C47263 = ".$G2402_C47263;
                    $LsqlI .= $separador." G2402_C47263";
                    $LsqlV .= $separador.$G2402_C47263;
                    $validar = 1;
                }
            }
 
            $G2402_C47264 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2402_C47264 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2402_C47264 = ".$G2402_C47264;
                    $LsqlI .= $separador." G2402_C47264";
                    $LsqlV .= $separador.$G2402_C47264;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2402_C47265"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47265 = '".$_POST["G2402_C47265"]."'";
                $LsqlI .= $separador."G2402_C47265";
                $LsqlV .= $separador."'".$_POST["G2402_C47265"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C47266"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47266 = '".$_POST["G2402_C47266"]."'";
                $LsqlI .= $separador."G2402_C47266";
                $LsqlV .= $separador."'".$_POST["G2402_C47266"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C47267"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47267 = '".$_POST["G2402_C47267"]."'";
                $LsqlI .= $separador."G2402_C47267";
                $LsqlV .= $separador."'".$_POST["G2402_C47267"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C47268"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47268 = '".$_POST["G2402_C47268"]."'";
                $LsqlI .= $separador."G2402_C47268";
                $LsqlV .= $separador."'".$_POST["G2402_C47268"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C47269"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C47269 = '".$_POST["G2402_C47269"]."'";
                $LsqlI .= $separador."G2402_C47269";
                $LsqlV .= $separador."'".$_POST["G2402_C47269"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2402_C55937"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_C55937 = '".$_POST["G2402_C55937"]."'";
                $LsqlI .= $separador."G2402_C55937";
                $LsqlV .= $separador."'".$_POST["G2402_C55937"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2402_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2402_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2402_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2402_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2402_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2402_Usuario , G2402_FechaInsercion, G2402_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2402_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2402 WHERE G2402_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        // echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
