<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2264_ConsInte__b, G2264_FechaInsercion , G2264_Usuario ,  G2264_CodigoMiembro  , G2264_PoblacionOrigen , G2264_EstadoDiligenciamiento ,  G2264_IdLlamada , G2264_C44234 as principal ,G2264_C44231,G2264_C44232,G2264_C44233,G2264_C44234,G2264_C44235,G2264_C44236,G2264_C44237,G2264_C44238,G2264_C44239,G2264_C44240,G2264_C44241,G2264_C44242,G2264_C44243,G2264_C44244,G2264_C44245,G2264_C44228,G2264_C44229,G2264_C44230,G2264_C44246,G2264_C44247,G2264_C44248,G2264_C44249,G2264_C44250,G2264_C44251,G2264_C44252,G2264_C44253,G2264_C44254,G2264_C44255,G2264_C44256,G2264_C44257,G2264_C44258,G2264_C44259 FROM '.$BaseDatos.'.G2264 WHERE G2264_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2264_C44231'] = $key->G2264_C44231;

                $datos[$i]['G2264_C44232'] = $key->G2264_C44232;

                $datos[$i]['G2264_C44233'] = $key->G2264_C44233;

                $datos[$i]['G2264_C44234'] = $key->G2264_C44234;

                $datos[$i]['G2264_C44235'] = $key->G2264_C44235;

                $datos[$i]['G2264_C44236'] = $key->G2264_C44236;

                $datos[$i]['G2264_C44237'] = $key->G2264_C44237;

                $datos[$i]['G2264_C44238'] = $key->G2264_C44238;

                $datos[$i]['G2264_C44239'] = $key->G2264_C44239;

                $datos[$i]['G2264_C44240'] = $key->G2264_C44240;

                $datos[$i]['G2264_C44241'] = $key->G2264_C44241;

                $datos[$i]['G2264_C44242'] = $key->G2264_C44242;

                $datos[$i]['G2264_C44243'] = $key->G2264_C44243;

                $datos[$i]['G2264_C44244'] = $key->G2264_C44244;

                $datos[$i]['G2264_C44245'] = $key->G2264_C44245;

                $datos[$i]['G2264_C44228'] = $key->G2264_C44228;

                $datos[$i]['G2264_C44229'] = $key->G2264_C44229;

                $datos[$i]['G2264_C44230'] = $key->G2264_C44230;

                $datos[$i]['G2264_C44246'] = $key->G2264_C44246;

                $datos[$i]['G2264_C44247'] = $key->G2264_C44247;

                $datos[$i]['G2264_C44248'] = $key->G2264_C44248;

                $datos[$i]['G2264_C44249'] = explode(' ', $key->G2264_C44249)[0];

                $datos[$i]['G2264_C44250'] = $key->G2264_C44250;

                $datos[$i]['G2264_C44251'] = $key->G2264_C44251;

                $datos[$i]['G2264_C44252'] = $key->G2264_C44252;

                $datos[$i]['G2264_C44253'] = $key->G2264_C44253;

                $datos[$i]['G2264_C44254'] = $key->G2264_C44254;

                $datos[$i]['G2264_C44255'] = $key->G2264_C44255;

                $datos[$i]['G2264_C44256'] = explode(' ', $key->G2264_C44256)[0];
  
                $hora = '';
                if(!is_null($key->G2264_C44257)){
                    $hora = explode(' ', $key->G2264_C44257)[1];
                }

                $datos[$i]['G2264_C44257'] = $hora;

                $datos[$i]['G2264_C44258'] = $key->G2264_C44258;

                $datos[$i]['G2264_C44259'] = $key->G2264_C44259;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2264";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = " AND G2264_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $regProp = "";
            }

            //JDBD valores filtros.
            $B = $_POST["B"];
            $A = $_POST["A"];
            $T = $_POST["T"];
            $F = $_POST["F"];
            $E = $_POST["E"];


            $Lsql = "SELECT G2264_ConsInte__b as id,  G2264_C44231 as camp2 , G2264_C44234 as camp1 
                     FROM ".$BaseDatos.".G2264  WHERE TRUE ".$regProp;

            if ($B != "" && $B != NULL) {
                $Lsql .= " AND (G2264_C44231 LIKE '%".$B."%' OR G2264_C44234 LIKE '%".$B."%') ";
            }
            if ($A != 0 && $A != -1 && $A != NULL) {
                $Lsql .= " AND G2264_Usuario = ".$A." ";
            }
            if ($T != 0 && $T != -1 && $T != NULL) {
                $Lsql .= " AND G2264_C = ".$T." ";
            }
            if ($F != "" && $F != NULL) {
                $Lsql .= " AND DATE_FORMAT(G2264_FechaInsercion,'%Y-%m-%d') = '".$F."' ";
            }
            if ($E != 0 && $E != -1 && $E != NULL) {
                if ($E == -203) {
                    $Lsql .= " AND G2264_C = -203 OR G2264_C = '' OR G2264_C IS NULL ";
                }else{
                    $Lsql .= " AND G2264_C = ".$E." "; 
                }
            }


            $Lsql .= " ORDER BY G2264_ConsInte__b DESC LIMIT 0, 50 "; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2264");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2264_ConsInte__b, G2264_FechaInsercion , G2264_Usuario ,  G2264_CodigoMiembro  , G2264_PoblacionOrigen , G2264_EstadoDiligenciamiento ,  G2264_IdLlamada , G2264_C44234 as principal ,G2264_C44231, a.LISOPC_Nombre____b as G2264_C44232,G2264_C44233,G2264_C44234,G2264_C44235,G2264_C44236,G2264_C44237,G2264_C44238, b.LISOPC_Nombre____b as G2264_C44239, c.LISOPC_Nombre____b as G2264_C44240,G2264_C44241, d.LISOPC_Nombre____b as G2264_C44242, e.LISOPC_Nombre____b as G2264_C44243,G2264_C44244,G2264_C44245,G2264_C44228,G2264_C44229, f.LISOPC_Nombre____b as G2264_C44230,G2264_C44246,G2264_C44247,G2264_C44248,G2264_C44249, g.LISOPC_Nombre____b as G2264_C44250, h.LISOPC_Nombre____b as G2264_C44251, i.LISOPC_Nombre____b as G2264_C44252,G2264_C44253,G2264_C44254,G2264_C44255,G2264_C44256,G2264_C44257,G2264_C44258,G2264_C44259 FROM '.$BaseDatos.'.G2264 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2264_C44232 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2264_C44239 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2264_C44240 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2264_C44242 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2264_C44243 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2264_C44230 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2264_C44250 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2264_C44251 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2264_C44252';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2264_C44257)){
                    $hora_a = explode(' ', $fila->G2264_C44257)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2264_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2264_ConsInte__b , ($fila->G2264_C44231) , ($fila->G2264_C44232) , ($fila->G2264_C44233) , ($fila->G2264_C44234) , ($fila->G2264_C44235) , ($fila->G2264_C44236) , ($fila->G2264_C44237) , ($fila->G2264_C44238) , ($fila->G2264_C44239) , ($fila->G2264_C44240) , ($fila->G2264_C44241) , ($fila->G2264_C44242) , ($fila->G2264_C44243) , ($fila->G2264_C44244) , ($fila->G2264_C44245) , ($fila->G2264_C44228) , ($fila->G2264_C44229) , ($fila->G2264_C44230) , ($fila->G2264_C44246) , ($fila->G2264_C44247) , ($fila->G2264_C44248) , explode(' ', $fila->G2264_C44249)[0] , ($fila->G2264_C44250) , ($fila->G2264_C44251) , ($fila->G2264_C44252) , ($fila->G2264_C44253) , ($fila->G2264_C44254) , ($fila->G2264_C44255) , explode(' ', $fila->G2264_C44256)[0] , $hora_a , ($fila->G2264_C44258) , ($fila->G2264_C44259) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2264 WHERE G2264_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2264";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = ' AND G2264_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $regProp = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $A = 0;
            $T = 0;
            $F = "";
            $B = "";
            $E = 0;

            if (isset($_POST["A"])) {
                $A = $_POST["A"];
            }
            if (isset($_POST["T"])) {
                $T = $_POST["T"];
            }
            if (isset($_POST["F"])) {
                $F = $_POST["F"];
            }
            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            if (isset($_POST["E"])) {
                $E = $_POST["E"];
            }

            $Zsql = 'SELECT  G2264_ConsInte__b as id,  G2264_C44231 as camp2 , G2264_C44234 as camp1  FROM '.$BaseDatos.'.G2264 WHERE TRUE'.$regProp; 

            if ($A != 0) {
                $Zsql .= ' AND G2264_Usuario = '.$A.' ';
            }

            if ($T != 0) {
                $Zsql .= ' AND G2264_C = '.$T.' ';
            }

            if ($F != "") {
                $Zsql .= ' AND DATE_FORMAT(G2264_FechaInsercion,"%Y-%m-%d") = "'.$F.'" ';
            }

            if ($B != "") {
                $Zsql .= ' AND (G2264_C44231 LIKE "%'.$B.'%" OR G2264_C44234 LIKE "%'.$B.'%") ';
            }

            if ($E != 0) {
                if ($E == -203) {
                    $Zsql .= ' AND G2264_C = -203 OR G2264_C = "" OR G2264_C IS NULL ';
                }else{
                    $Zsql .= ' AND G2264_C = '.$E.'  ';
                }
            }

            $Zsql .= ' ORDER BY G2264_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2264 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2264(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2264_C44231"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44231 = '".$_POST["G2264_C44231"]."'";
                $LsqlI .= $separador."G2264_C44231";
                $LsqlV .= $separador."'".$_POST["G2264_C44231"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44232"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44232 = '".$_POST["G2264_C44232"]."'";
                $LsqlI .= $separador."G2264_C44232";
                $LsqlV .= $separador."'".$_POST["G2264_C44232"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44233"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44233 = '".$_POST["G2264_C44233"]."'";
                $LsqlI .= $separador."G2264_C44233";
                $LsqlV .= $separador."'".$_POST["G2264_C44233"]."'";
                $validar = 1;
            }
             
  
            $G2264_C44234 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2264_C44234"])){
                if($_POST["G2264_C44234"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2264_C44234 = $_POST["G2264_C44234"];
                    $LsqlU .= $separador." G2264_C44234 = ".$G2264_C44234."";
                    $LsqlI .= $separador." G2264_C44234";
                    $LsqlV .= $separador.$G2264_C44234;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2264_C44235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44235 = '".$_POST["G2264_C44235"]."'";
                $LsqlI .= $separador."G2264_C44235";
                $LsqlV .= $separador."'".$_POST["G2264_C44235"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44236"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44236 = '".$_POST["G2264_C44236"]."'";
                $LsqlI .= $separador."G2264_C44236";
                $LsqlV .= $separador."'".$_POST["G2264_C44236"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44237"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44237 = '".$_POST["G2264_C44237"]."'";
                $LsqlI .= $separador."G2264_C44237";
                $LsqlV .= $separador."'".$_POST["G2264_C44237"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44238"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44238 = '".$_POST["G2264_C44238"]."'";
                $LsqlI .= $separador."G2264_C44238";
                $LsqlV .= $separador."'".$_POST["G2264_C44238"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44239"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44239 = '".$_POST["G2264_C44239"]."'";
                $LsqlI .= $separador."G2264_C44239";
                $LsqlV .= $separador."'".$_POST["G2264_C44239"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44240"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44240 = '".$_POST["G2264_C44240"]."'";
                $LsqlI .= $separador."G2264_C44240";
                $LsqlV .= $separador."'".$_POST["G2264_C44240"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44241"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44241 = '".$_POST["G2264_C44241"]."'";
                $LsqlI .= $separador."G2264_C44241";
                $LsqlV .= $separador."'".$_POST["G2264_C44241"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44242"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44242 = '".$_POST["G2264_C44242"]."'";
                $LsqlI .= $separador."G2264_C44242";
                $LsqlV .= $separador."'".$_POST["G2264_C44242"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44243"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44243 = '".$_POST["G2264_C44243"]."'";
                $LsqlI .= $separador."G2264_C44243";
                $LsqlV .= $separador."'".$_POST["G2264_C44243"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44244 = '".$_POST["G2264_C44244"]."'";
                $LsqlI .= $separador."G2264_C44244";
                $LsqlV .= $separador."'".$_POST["G2264_C44244"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44245"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44245 = '".$_POST["G2264_C44245"]."'";
                $LsqlI .= $separador."G2264_C44245";
                $LsqlV .= $separador."'".$_POST["G2264_C44245"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44228"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44228 = '".$_POST["G2264_C44228"]."'";
                $LsqlI .= $separador."G2264_C44228";
                $LsqlV .= $separador."'".$_POST["G2264_C44228"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44229"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44229 = '".$_POST["G2264_C44229"]."'";
                $LsqlI .= $separador."G2264_C44229";
                $LsqlV .= $separador."'".$_POST["G2264_C44229"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44230"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44230 = '".$_POST["G2264_C44230"]."'";
                $LsqlI .= $separador."G2264_C44230";
                $LsqlV .= $separador."'".$_POST["G2264_C44230"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44246"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44246 = '".$_POST["G2264_C44246"]."'";
                $LsqlI .= $separador."G2264_C44246";
                $LsqlV .= $separador."'".$_POST["G2264_C44246"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44247"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44247 = '".$_POST["G2264_C44247"]."'";
                $LsqlI .= $separador."G2264_C44247";
                $LsqlV .= $separador."'".$_POST["G2264_C44247"]."'";
                $validar = 1;
            }
             
  
            $G2264_C44248 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2264_C44248"])){
                if($_POST["G2264_C44248"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2264_C44248 = $_POST["G2264_C44248"];
                    $LsqlU .= $separador." G2264_C44248 = '".$G2264_C44248."'";
                    $LsqlI .= $separador." G2264_C44248";
                    $LsqlV .= $separador."'".$G2264_C44248."'";
                    $validar = 1;
                }
            }
 
            $G2264_C44249 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2264_C44249"])){    
                if($_POST["G2264_C44249"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2264_C44249"]);
                    if(count($tieneHora) > 1){
                        $G2264_C44249 = "'".$_POST["G2264_C44249"]."'";
                    }else{
                        $G2264_C44249 = "'".str_replace(' ', '',$_POST["G2264_C44249"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2264_C44249 = ".$G2264_C44249;
                    $LsqlI .= $separador." G2264_C44249";
                    $LsqlV .= $separador.$G2264_C44249;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2264_C44250"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44250 = '".$_POST["G2264_C44250"]."'";
                $LsqlI .= $separador."G2264_C44250";
                $LsqlV .= $separador."'".$_POST["G2264_C44250"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44251"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44251 = '".$_POST["G2264_C44251"]."'";
                $LsqlI .= $separador."G2264_C44251";
                $LsqlV .= $separador."'".$_POST["G2264_C44251"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44252"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44252 = '".$_POST["G2264_C44252"]."'";
                $LsqlI .= $separador."G2264_C44252";
                $LsqlV .= $separador."'".$_POST["G2264_C44252"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44253"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44253 = '".$_POST["G2264_C44253"]."'";
                $LsqlI .= $separador."G2264_C44253";
                $LsqlV .= $separador."'".$_POST["G2264_C44253"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44254"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44254 = '".$_POST["G2264_C44254"]."'";
                $LsqlI .= $separador."G2264_C44254";
                $LsqlV .= $separador."'".$_POST["G2264_C44254"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44255"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44255 = '".$_POST["G2264_C44255"]."'";
                $LsqlI .= $separador."G2264_C44255";
                $LsqlV .= $separador."'".$_POST["G2264_C44255"]."'";
                $validar = 1;
            }
             
 
            $G2264_C44256 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2264_C44256"])){    
                if($_POST["G2264_C44256"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2264_C44256"]);
                    if(count($tieneHora) > 1){
                        $G2264_C44256 = "'".$_POST["G2264_C44256"]."'";
                    }else{
                        $G2264_C44256 = "'".str_replace(' ', '',$_POST["G2264_C44256"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2264_C44256 = ".$G2264_C44256;
                    $LsqlI .= $separador." G2264_C44256";
                    $LsqlV .= $separador.$G2264_C44256;
                    $validar = 1;
                }
            }
  
            $G2264_C44257 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G2264_C44257"])){   
                if($_POST["G2264_C44257"] != '' && $_POST["G2264_C44257"] != 'undefined' && $_POST["G2264_C44257"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2264_C44257 = "'".$fecha." ".str_replace(' ', '',$_POST["G2264_C44257"])."'";
                    $LsqlU .= $separador." G2264_C44257 = ".$G2264_C44257."";
                    $LsqlI .= $separador." G2264_C44257";
                    $LsqlV .= $separador.$G2264_C44257;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2264_C44258"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44258 = '".$_POST["G2264_C44258"]."'";
                $LsqlI .= $separador."G2264_C44258";
                $LsqlV .= $separador."'".$_POST["G2264_C44258"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2264_C44259"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_C44259 = '".$_POST["G2264_C44259"]."'";
                $LsqlI .= $separador."G2264_C44259";
                $LsqlV .= $separador."'".$_POST["G2264_C44259"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2264_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2264_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2264_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2264_Usuario , G2264_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2264_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2264 WHERE G2264_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2264 SET G2264_UltiGest__b =-14, G2264_GesMasImp_b =-14, G2264_TipoReintentoUG_b =0, G2264_TipoReintentoGMI_b =0, G2264_EstadoUG_b =-14, G2264_EstadoGMI_b =-14, G2264_CantidadIntentos =0, G2264_CantidadIntentosGMI_b =0 WHERE G2264_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
