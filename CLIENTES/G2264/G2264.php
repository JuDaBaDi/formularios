
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2264/G2264_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2264_ConsInte__b as id, G2264_C44231 as camp2 , G2264_C44234 as camp1 FROM ".$BaseDatos.".G2264  WHERE G2264_Usuario = ".$idUsuario." ORDER BY G2264_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2264_ConsInte__b as id, G2264_C44231 as camp2 , G2264_C44234 as camp1 FROM ".$BaseDatos.".G2264  ORDER BY G2264_ConsInte__b DESC LIMIT 0, 50";
        }

        if(isset($_GET['tareaB'])){

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareaB'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2264_ConsInte__b as id, G2264_C44231 as camp2 , G2264_C44234 as camp1 FROM ".$BaseDatos.".G2264 JOIN ".$BaseDatos.".G2264_M".$resultEstpas->muestr." ON G2264_ConsInte__b = G2264_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2264_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2264_ConsInte__b as id, G2264_C44231 as camp2 , G2264_C44234 as camp1 FROM ".$BaseDatos.".G2264 JOIN ".$BaseDatos.".G2264_M".$resultEstpas->muestr." ON G2264_ConsInte__b = G2264_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2264_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2264_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2264_ConsInte__b as id, G2264_C44231 as camp2 , G2264_C44234 as camp1 FROM ".$BaseDatos.".G2264  ORDER BY G2264_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="6438" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44231" id="LblG2264_C44231">CÓDIGO USUARIO</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44231" value="<?php if (isset($_GET['G2264_C44231'])) {
                            echo $_GET['G2264_C44231'];
                        } ?>"  name="G2264_C44231"  placeholder="CÓDIGO USUARIO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44232" id="LblG2264_C44232">CLIENTE CENTRODIESEL</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44232" id="G2264_C44232">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2324 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G2264_C44233" id="LblG2264_C44233">COMENTARIOS</label>
                        <textarea class="form-control input-sm" name="G2264_C44233" id="G2264_C44233"  value="<?php if (isset($_GET['G2264_C44233'])) {
                            echo $_GET['G2264_C44233'];
                        } ?>" placeholder="COMENTARIOS"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2264_C44234" id="LblG2264_C44234">NIT</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2264_C44234'])) {
                            echo $_GET['G2264_C44234'];
                        } ?>"  name="G2264_C44234" id="G2264_C44234" placeholder="NIT">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44235" id="LblG2264_C44235">NOMBRE</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44235" value="<?php if (isset($_GET['G2264_C44235'])) {
                            echo $_GET['G2264_C44235'];
                        } ?>"  name="G2264_C44235"  placeholder="NOMBRE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44236" id="LblG2264_C44236">TELEFONO 1</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44236" value="<?php if (isset($_GET['G2264_C44236'])) {
                            echo $_GET['G2264_C44236'];
                        } ?>"  name="G2264_C44236"  placeholder="TELEFONO 1">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44237" id="LblG2264_C44237">TELEFONO 2</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44237" value="<?php if (isset($_GET['G2264_C44237'])) {
                            echo $_GET['G2264_C44237'];
                        } ?>"  name="G2264_C44237"  placeholder="TELEFONO 2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44238" id="LblG2264_C44238">TELEFONO 3</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44238" value="<?php if (isset($_GET['G2264_C44238'])) {
                            echo $_GET['G2264_C44238'];
                        } ?>"  name="G2264_C44238"  placeholder="TELEFONO 3">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44239" id="LblG2264_C44239">DUEÑO DE VARIOS VH</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44239" id="G2264_C44239">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2323 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44240" id="LblG2264_C44240">ACTUALIZA DATOS</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44240" id="G2264_C44240">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2323 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44241" id="LblG2264_C44241">HABEAS DATA</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44241" value="<?php if (isset($_GET['G2264_C44241'])) {
                            echo $_GET['G2264_C44241'];
                        } ?>"  name="G2264_C44241"  placeholder="HABEAS DATA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44242" id="LblG2264_C44242">PLAN DE COMPRA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44242" id="G2264_C44242">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2323 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44243" id="LblG2264_C44243">SOLICITA COTIZACIÓN</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44243" id="G2264_C44243">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2325 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44244" id="LblG2264_C44244">QUEJA PQRS</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44244" value="<?php if (isset($_GET['G2264_C44244'])) {
                            echo $_GET['G2264_C44244'];
                        } ?>"  name="G2264_C44244"  placeholder="QUEJA PQRS">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44245" id="LblG2264_C44245">BARRIO</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44245" value="<?php if (isset($_GET['G2264_C44245'])) {
                            echo $_GET['G2264_C44245'];
                        } ?>"  name="G2264_C44245"  placeholder="BARRIO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


</div>

<div  id="6439" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44228" id="LblG2264_C44228">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44228" value="<?php if (isset($_GET['G2264_C44228'])) {
                            echo $_GET['G2264_C44228'];
                        } ?>" readonly name="G2264_C44228"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44229" id="LblG2264_C44229">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44229" value="<?php if (isset($_GET['G2264_C44229'])) {
                            echo $_GET['G2264_C44229'];
                        } ?>" readonly name="G2264_C44229"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44230" id="LblG2264_C44230">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44230" id="G2264_C44230">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2499 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="6440" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_6440c">
                VEHÍCULO
            </a>
        </h4>
        
    </div>
    <div id="s_6440c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44246" id="LblG2264_C44246">PLACA</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44246" value="<?php if (isset($_GET['G2264_C44246'])) {
                            echo $_GET['G2264_C44246'];
                        } ?>"  name="G2264_C44246"  placeholder="PLACA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44247" id="LblG2264_C44247">LINEA</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44247" value="<?php if (isset($_GET['G2264_C44247'])) {
                            echo $_GET['G2264_C44247'];
                        } ?>"  name="G2264_C44247"  placeholder="LINEA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2264_C44248" id="LblG2264_C44248">PERIODICIDAD</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2264_C44248'])) {
                            echo $_GET['G2264_C44248'];
                        } ?>"  name="G2264_C44248" id="G2264_C44248" placeholder="PERIODICIDAD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2264_C44249" id="LblG2264_C44249">FECHA ULTIMO INGRESO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2264_C44249'])) {
                            echo $_GET['G2264_C44249'];
                        } ?>"  name="G2264_C44249" id="G2264_C44249" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44250" id="LblG2264_C44250">TIPO DE CARROCERÍA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44250" id="G2264_C44250">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2327 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44251" id="LblG2264_C44251">VEHÍCULOS EN OPERACIÓN</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44251" id="G2264_C44251">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2323 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2264_C44252" id="LblG2264_C44252">SECTOR DE TRABAJO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2264_C44252" id="G2264_C44252">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2328 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44253" id="LblG2264_C44253">NOMBRE DEL CONDUCTOR</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44253" value="<?php if (isset($_GET['G2264_C44253'])) {
                            echo $_GET['G2264_C44253'];
                        } ?>"  name="G2264_C44253"  placeholder="NOMBRE DEL CONDUCTOR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44254" id="LblG2264_C44254">TELÉFONO DEL CONDUCTOR</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44254" value="<?php if (isset($_GET['G2264_C44254'])) {
                            echo $_GET['G2264_C44254'];
                        } ?>"  name="G2264_C44254"  placeholder="TELÉFONO DEL CONDUCTOR">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44255" id="LblG2264_C44255">UBICACIÓN DEL VH</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44255" value="<?php if (isset($_GET['G2264_C44255'])) {
                            echo $_GET['G2264_C44255'];
                        } ?>"  name="G2264_C44255"  placeholder="UBICACIÓN DEL VH">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2264_C44256" id="LblG2264_C44256">FECHA CITA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2264_C44256'])) {
                            echo $_GET['G2264_C44256'];
                        } ?>"  name="G2264_C44256" id="G2264_C44256" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G2264_C44257" id="LblG2264_C44257">HORA CITA</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora"  name="G2264_C44257" id="G2264_C44257" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G2264_C44257">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44258" id="LblG2264_C44258">ULTIMO KILOMETRAJE</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44258" value="<?php if (isset($_GET['G2264_C44258'])) {
                            echo $_GET['G2264_C44258'];
                        } ?>"  name="G2264_C44258"  placeholder="ULTIMO KILOMETRAJE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2264_C44259" id="LblG2264_C44259">LINEA VEHICULO</label>
                        <input type="text" class="form-control input-sm" id="G2264_C44259" value="<?php if (isset($_GET['G2264_C44259'])) {
                            echo $_GET['G2264_C44259'];
                        } ?>"  name="G2264_C44259"  placeholder="LINEA VEHICULO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2264/G2264_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2264_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2264_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2264_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>; 
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2264_C44231").val(item.G2264_C44231); 
                $("#G2264_C44232").val(item.G2264_C44232).trigger("change");  
                $("#G2264_C44233").val(item.G2264_C44233); 
                $("#G2264_C44234").val(item.G2264_C44234); 
                $("#G2264_C44235").val(item.G2264_C44235); 
                $("#G2264_C44236").val(item.G2264_C44236); 
                $("#G2264_C44237").val(item.G2264_C44237); 
                $("#G2264_C44238").val(item.G2264_C44238); 
                $("#G2264_C44239").val(item.G2264_C44239).trigger("change");  
                $("#G2264_C44240").val(item.G2264_C44240).trigger("change");  
                $("#G2264_C44241").val(item.G2264_C44241); 
                $("#G2264_C44242").val(item.G2264_C44242).trigger("change");  
                $("#G2264_C44243").val(item.G2264_C44243).trigger("change");  
                $("#G2264_C44244").val(item.G2264_C44244); 
                $("#G2264_C44245").val(item.G2264_C44245); 
                $("#G2264_C44228").val(item.G2264_C44228); 
                $("#G2264_C44229").val(item.G2264_C44229); 
                $("#G2264_C44230").val(item.G2264_C44230).trigger("change");  
                $("#G2264_C44246").val(item.G2264_C44246); 
                $("#G2264_C44247").val(item.G2264_C44247); 
                $("#G2264_C44248").val(item.G2264_C44248); 
                $("#G2264_C44249").val(item.G2264_C44249); 
                $("#G2264_C44250").val(item.G2264_C44250).trigger("change");  
                $("#G2264_C44251").val(item.G2264_C44251).trigger("change");  
                $("#G2264_C44252").val(item.G2264_C44252).trigger("change");  
                $("#G2264_C44253").val(item.G2264_C44253); 
                $("#G2264_C44254").val(item.G2264_C44254); 
                $("#G2264_C44255").val(item.G2264_C44255); 
                $("#G2264_C44256").val(item.G2264_C44256); 
                $("#G2264_C44257").val(item.G2264_C44257); 
                $("#G2264_C44258").val(item.G2264_C44258); 
                $("#G2264_C44259").val(item.G2264_C44259);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2264_C44232").select2();

    $("#G2264_C44239").select2();

    $("#G2264_C44240").select2();

    $("#G2264_C44242").select2();

    $("#G2264_C44243").select2();

    $("#G2264_C44230").select2();

    $("#G2264_C44250").select2();

    $("#G2264_C44251").select2();

    $("#G2264_C44252").select2();
        //datepickers
        

        $("#G2264_C44249").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2264_C44256").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'HORA CITA', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2264_C44257").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G2264_C44234").numeric();
                
        $("#G2264_C44248").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para CLIENTE CENTRODIESEL 

    $("#G2264_C44232").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DUEÑO DE VARIOS VH 

    $("#G2264_C44239").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ACTUALIZA DATOS 

    $("#G2264_C44240").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN DE COMPRA 

    $("#G2264_C44242").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SOLICITA COTIZACIÓN 

    $("#G2264_C44243").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G2264_C44230").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CARROCERÍA 

    $("#G2264_C44250").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VEHÍCULOS EN OPERACIÓN 

    $("#G2264_C44251").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SECTOR DE TRABAJO 

    $("#G2264_C44252").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2264_C44231").val(item.G2264_C44231);
 
                    $("#G2264_C44232").val(item.G2264_C44232).trigger("change"); 
 
                                                $("#G2264_C44233").val(item.G2264_C44233);
 
                                                $("#G2264_C44234").val(item.G2264_C44234);
 
                                                $("#G2264_C44235").val(item.G2264_C44235);
 
                                                $("#G2264_C44236").val(item.G2264_C44236);
 
                                                $("#G2264_C44237").val(item.G2264_C44237);
 
                                                $("#G2264_C44238").val(item.G2264_C44238);
 
                    $("#G2264_C44239").val(item.G2264_C44239).trigger("change"); 
 
                    $("#G2264_C44240").val(item.G2264_C44240).trigger("change"); 
 
                                                $("#G2264_C44241").val(item.G2264_C44241);
 
                    $("#G2264_C44242").val(item.G2264_C44242).trigger("change"); 
 
                    $("#G2264_C44243").val(item.G2264_C44243).trigger("change"); 
 
                                                $("#G2264_C44244").val(item.G2264_C44244);
 
                                                $("#G2264_C44245").val(item.G2264_C44245);
 
                                                $("#G2264_C44228").val(item.G2264_C44228);
 
                                                $("#G2264_C44229").val(item.G2264_C44229);
 
                    $("#G2264_C44230").val(item.G2264_C44230).trigger("change"); 
 
                                                $("#G2264_C44246").val(item.G2264_C44246);
 
                                                $("#G2264_C44247").val(item.G2264_C44247);
 
                                                $("#G2264_C44248").val(item.G2264_C44248);
 
                                                $("#G2264_C44249").val(item.G2264_C44249);
 
                    $("#G2264_C44250").val(item.G2264_C44250).trigger("change"); 
 
                    $("#G2264_C44251").val(item.G2264_C44251).trigger("change"); 
 
                    $("#G2264_C44252").val(item.G2264_C44252).trigger("change"); 
 
                                                $("#G2264_C44253").val(item.G2264_C44253);
 
                                                $("#G2264_C44254").val(item.G2264_C44254);
 
                                                $("#G2264_C44255").val(item.G2264_C44255);
 
                                                $("#G2264_C44256").val(item.G2264_C44256);
 
                                                $("#G2264_C44257").val(item.G2264_C44257);
 
                                                $("#G2264_C44258").val(item.G2264_C44258);
 
                                                $("#G2264_C44259").val(item.G2264_C44259);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CÓDIGO USUARIO','CLIENTE CENTRODIESEL','COMENTARIOS','NIT','NOMBRE','TELEFONO 1','TELEFONO 2','TELEFONO 3','DUEÑO DE VARIOS VH','ACTUALIZA DATOS','HABEAS DATA','PLAN DE COMPRA','SOLICITA COTIZACIÓN','QUEJA PQRS','BARRIO','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','PLACA','LINEA','PERIODICIDAD','FECHA ULTIMO INGRESO','TIPO DE CARROCERÍA','VEHÍCULOS EN OPERACIÓN','SECTOR DE TRABAJO','NOMBRE DEL CONDUCTOR','TELÉFONO DEL CONDUCTOR','UBICACIÓN DEL VH','FECHA CITA','HORA CITA','ULTIMO KILOMETRAJE','LINEA VEHICULO'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2264_C44231', 
                        index: 'G2264_C44231', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44232', 
                        index:'G2264_C44232', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2324&campo=G2264_C44232'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44233', 
                        index:'G2264_C44233', 
                        width:150, 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2264_C44234', 
                        index:'G2264_C44234', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2264_C44235', 
                        index: 'G2264_C44235', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44236', 
                        index: 'G2264_C44236', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44237', 
                        index: 'G2264_C44237', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44238', 
                        index: 'G2264_C44238', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44239', 
                        index:'G2264_C44239', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2323&campo=G2264_C44239'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44240', 
                        index:'G2264_C44240', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2323&campo=G2264_C44240'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44241', 
                        index: 'G2264_C44241', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44242', 
                        index:'G2264_C44242', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2323&campo=G2264_C44242'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44243', 
                        index:'G2264_C44243', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2325&campo=G2264_C44243'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44244', 
                        index: 'G2264_C44244', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44245', 
                        index: 'G2264_C44245', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44228', 
                        index: 'G2264_C44228', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44229', 
                        index: 'G2264_C44229', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44230', 
                        index:'G2264_C44230', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2499&campo=G2264_C44230'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44246', 
                        index: 'G2264_C44246', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44247', 
                        index: 'G2264_C44247', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2264_C44248', 
                        index:'G2264_C44248', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    {  
                        name:'G2264_C44249', 
                        index:'G2264_C44249', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44250', 
                        index:'G2264_C44250', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2327&campo=G2264_C44250'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44251', 
                        index:'G2264_C44251', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2323&campo=G2264_C44251'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44252', 
                        index:'G2264_C44252', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2328&campo=G2264_C44252'
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44253', 
                        index: 'G2264_C44253', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44254', 
                        index: 'G2264_C44254', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44255', 
                        index: 'G2264_C44255', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2264_C44256', 
                        index:'G2264_C44256', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2264_C44257', 
                        index:'G2264_C44257', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA CITA', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2264_C44258', 
                        index: 'G2264_C44258', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2264_C44259', 
                        index: 'G2264_C44259', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2264_C44234',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2264_C44231").val(item.G2264_C44231);
 
                    $("#G2264_C44232").val(item.G2264_C44232).trigger("change"); 

                        $("#G2264_C44233").val(item.G2264_C44233);

                        $("#G2264_C44234").val(item.G2264_C44234);

                        $("#G2264_C44235").val(item.G2264_C44235);

                        $("#G2264_C44236").val(item.G2264_C44236);

                        $("#G2264_C44237").val(item.G2264_C44237);

                        $("#G2264_C44238").val(item.G2264_C44238);
 
                    $("#G2264_C44239").val(item.G2264_C44239).trigger("change"); 
 
                    $("#G2264_C44240").val(item.G2264_C44240).trigger("change"); 

                        $("#G2264_C44241").val(item.G2264_C44241);
 
                    $("#G2264_C44242").val(item.G2264_C44242).trigger("change"); 
 
                    $("#G2264_C44243").val(item.G2264_C44243).trigger("change"); 

                        $("#G2264_C44244").val(item.G2264_C44244);

                        $("#G2264_C44245").val(item.G2264_C44245);

                        $("#G2264_C44228").val(item.G2264_C44228);

                        $("#G2264_C44229").val(item.G2264_C44229);
 
                    $("#G2264_C44230").val(item.G2264_C44230).trigger("change"); 

                        $("#G2264_C44246").val(item.G2264_C44246);

                        $("#G2264_C44247").val(item.G2264_C44247);

                        $("#G2264_C44248").val(item.G2264_C44248);

                        $("#G2264_C44249").val(item.G2264_C44249);
 
                    $("#G2264_C44250").val(item.G2264_C44250).trigger("change"); 
 
                    $("#G2264_C44251").val(item.G2264_C44251).trigger("change"); 
 
                    $("#G2264_C44252").val(item.G2264_C44252).trigger("change"); 

                        $("#G2264_C44253").val(item.G2264_C44253);

                        $("#G2264_C44254").val(item.G2264_C44254);

                        $("#G2264_C44255").val(item.G2264_C44255);

                        $("#G2264_C44256").val(item.G2264_C44256);

                        $("#G2264_C44257").val(item.G2264_C44257);

                        $("#G2264_C44258").val(item.G2264_C44258);

                        $("#G2264_C44259").val(item.G2264_C44259);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
