
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2961/G2961_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : $_SESSION["IDENTIFICACION"];
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2961_ConsInte__b as id, G2961_C59154 as camp1 , G2961_C59157 as camp2 FROM ".$BaseDatos.".G2961  WHERE G2961_Usuario = ".$idUsuario." ORDER BY G2961_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2961_ConsInte__b as id, G2961_C59154 as camp1 , G2961_C59157 as camp2 FROM ".$BaseDatos.".G2961  ORDER BY G2961_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2961_ConsInte__b as id, G2961_C59154 as camp1 , G2961_C59157 as camp2 FROM ".$BaseDatos.".G2961 JOIN ".$BaseDatos.".G2961_M".$resultEstpas->muestr." ON G2961_ConsInte__b = G2961_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2961_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2961_ConsInte__b as id, G2961_C59154 as camp1 , G2961_C59157 as camp2 FROM ".$BaseDatos.".G2961 JOIN ".$BaseDatos.".G2961_M".$resultEstpas->muestr." ON G2961_ConsInte__b = G2961_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2961_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2961_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G2961_ConsInte__b as id, G2961_C59154 as camp1 , G2961_C59157 as camp2 FROM ".$BaseDatos.".G2961  ORDER BY G2961_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="9026" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9026c">
                GENERAL
            </a>
        </h4>
        
    </div>
    <div id="s_9026c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59154" id="LblG2961_C59154">Fecha pre-inscripción:</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59154'])) {
                            echo $_GET['G2961_C59154'];
                        } ?>"  name="G2961_C59154" id="G2961_C59154" placeholder="YYYY-MM-DD" nombre="Fecha pre-inscripción:">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59155" id="LblG2961_C59155">No. pre-inscripción:</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59155'])) {
                            echo $_GET['G2961_C59155'];
                        } ?>"  name="G2961_C59155" id="G2961_C59155" placeholder="No. pre-inscripción:"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C59156" id="LblG2961_C59156">Sede</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C59156" id="G2961_C59156">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3586 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59157" id="LblG2961_C59157">Tipo de pre-inscripción:</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59157" value="<?php if (isset($_GET['G2961_C59157'])) {
                            echo $_GET['G2961_C59157'];
                        } ?>"  name="G2961_C59157"  placeholder="Tipo de pre-inscripción:"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G2961_C59159" id="LblG2961_C59159"> Programa académico</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G2961_C59159" id="G2961_C59159">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9027" style='display:none;'>
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9027c">
                CONTROL
            </a>
        </h4>
        
    </div>
    <div id="s_9027c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59151" id="LblG2961_C59151">ORIGEN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59151" value="<?php if (isset($_GET['G2961_C59151'])) {
                            echo $_GET['G2961_C59151'];
                        } ?>" readonly name="G2961_C59151"  placeholder="ORIGEN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59152" id="LblG2961_C59152">OPTIN_DY_WF</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59152" value="<?php if (isset($_GET['G2961_C59152'])) {
                            echo $_GET['G2961_C59152'];
                        } ?>" readonly name="G2961_C59152"  placeholder="OPTIN_DY_WF"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C59153" id="LblG2961_C59153">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C59153" id="G2961_C59153">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3584 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9098" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9098c">
                DATOS PERSONALES
            </a>
        </h4>
        
    </div>
    <div id="s_9098c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59582" id="LblG2961_C59582"> Tipo de documento:</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59582" value="<?php if (isset($_GET['G2961_C59582'])) {
                            echo $_GET['G2961_C59582'];
                        } ?>"  name="G2961_C59582"  placeholder=" Tipo de documento:"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59583" id="LblG2961_C59583">Documento</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59583" value="<?php if (isset($_GET['G2961_C59583'])) {
                            echo $_GET['G2961_C59583'];
                        } ?>"  name="G2961_C59583"  placeholder="Documento"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59584" id="LblG2961_C59584">Genero</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59584" value="<?php if (isset($_GET['G2961_C59584'])) {
                            echo $_GET['G2961_C59584'];
                        } ?>"  name="G2961_C59584"  placeholder="Genero"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59585" id="LblG2961_C59585">Fecha Nacimiento</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59585'])) {
                            echo $_GET['G2961_C59585'];
                        } ?>"  name="G2961_C59585" id="G2961_C59585" placeholder="YYYY-MM-DD" nombre="Fecha Nacimiento">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59586" id="LblG2961_C59586">Primer Apellido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59586" value="<?php if (isset($_GET['G2961_C59586'])) {
                            echo $_GET['G2961_C59586'];
                        } ?>"  name="G2961_C59586"  placeholder="Primer Apellido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59587" id="LblG2961_C59587">Segundo  Apellido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59587" value="<?php if (isset($_GET['G2961_C59587'])) {
                            echo $_GET['G2961_C59587'];
                        } ?>"  name="G2961_C59587"  placeholder="Segundo  Apellido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59588" id="LblG2961_C59588">Primer Nombre</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59588" value="<?php if (isset($_GET['G2961_C59588'])) {
                            echo $_GET['G2961_C59588'];
                        } ?>"  name="G2961_C59588"  placeholder="Primer Nombre"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59589" id="LblG2961_C59589">Segundo Nombre</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59589" value="<?php if (isset($_GET['G2961_C59589'])) {
                            echo $_GET['G2961_C59589'];
                        } ?>"  name="G2961_C59589"  placeholder="Segundo Nombre"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59590" id="LblG2961_C59590">Lugar de Nacimiento</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59590" value="<?php if (isset($_GET['G2961_C59590'])) {
                            echo $_GET['G2961_C59590'];
                        } ?>"  name="G2961_C59590"  placeholder="Lugar de Nacimiento"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59591" id="LblG2961_C59591">Telefono Movil</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59591'])) {
                            echo $_GET['G2961_C59591'];
                        } ?>"  name="G2961_C59591" id="G2961_C59591" placeholder="Telefono Movil"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59592" id="LblG2961_C59592">Telefono de residencia</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59592'])) {
                            echo $_GET['G2961_C59592'];
                        } ?>"  name="G2961_C59592" id="G2961_C59592" placeholder="Telefono de residencia"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59593" id="LblG2961_C59593">Estrato</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59593'])) {
                            echo $_GET['G2961_C59593'];
                        } ?>"  name="G2961_C59593" id="G2961_C59593" placeholder="Estrato"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59594" id="LblG2961_C59594">Lugar de Residencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59594" value="<?php if (isset($_GET['G2961_C59594'])) {
                            echo $_GET['G2961_C59594'];
                        } ?>"  name="G2961_C59594"  placeholder="Lugar de Residencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59595" id="LblG2961_C59595">Direccion de residencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59595" value="<?php if (isset($_GET['G2961_C59595'])) {
                            echo $_GET['G2961_C59595'];
                        } ?>"  name="G2961_C59595"  placeholder="Direccion de residencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59596" id="LblG2961_C59596">Barrio de residencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59596" value="<?php if (isset($_GET['G2961_C59596'])) {
                            echo $_GET['G2961_C59596'];
                        } ?>"  name="G2961_C59596"  placeholder="Barrio de residencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59597" id="LblG2961_C59597">Email del contacto</label><input type="email" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59597" value="<?php if (isset($_GET['G2961_C59597'])) {
                            echo $_GET['G2961_C59597'];
                        } ?>"  name="G2961_C59597"  placeholder="Email del contacto"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59598" id="LblG2961_C59598">Correo Electronico</label><input type="email" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59598" value="<?php if (isset($_GET['G2961_C59598'])) {
                            echo $_GET['G2961_C59598'];
                        } ?>"  name="G2961_C59598"  placeholder="Correo Electronico"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59599" id="LblG2961_C59599">Confirmar correo Electronico</label><input type="email" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59599" value="<?php if (isset($_GET['G2961_C59599'])) {
                            echo $_GET['G2961_C59599'];
                        } ?>"  name="G2961_C59599"  placeholder="Confirmar correo Electronico"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9099" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9099c">
                INFORMACION COMPLEMENTARIA
            </a>
        </h4>
        
    </div>
    <div id="s_9099c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C59634" id="LblG2961_C59634">¿Por cuál medio de comunicación  se enteró de la universidad?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C59634" id="G2961_C59634">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3621 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59605" id="LblG2961_C59605">Período de Admisión</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59605" value="<?php if (isset($_GET['G2961_C59605'])) {
                            echo $_GET['G2961_C59605'];
                        } ?>"  name="G2961_C59605"  placeholder="Período de Admisión"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C59606" id="LblG2961_C59606">Sede</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C59606" id="G2961_C59606">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3586 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59607" id="LblG2961_C59607">Nivel</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59607" value="<?php if (isset($_GET['G2961_C59607'])) {
                            echo $_GET['G2961_C59607'];
                        } ?>"  name="G2961_C59607"  placeholder="Nivel"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59608" id="LblG2961_C59608">Jornada</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59608" value="<?php if (isset($_GET['G2961_C59608'])) {
                            echo $_GET['G2961_C59608'];
                        } ?>"  name="G2961_C59608"  placeholder="Jornada"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C59609" id="LblG2961_C59609">Programa Académico</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C59609" id="G2961_C59609">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59610" id="LblG2961_C59610">Identificación</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59610'])) {
                            echo $_GET['G2961_C59610'];
                        } ?>"  name="G2961_C59610" id="G2961_C59610" placeholder="Identificación"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59611" id="LblG2961_C59611">Nombre Aspirante</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59611" value="<?php if (isset($_GET['G2961_C59611'])) {
                            echo $_GET['G2961_C59611'];
                        } ?>"  name="G2961_C59611"  placeholder="Nombre Aspirante"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59612" id="LblG2961_C59612">Dirección de Residencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59612" value="<?php if (isset($_GET['G2961_C59612'])) {
                            echo $_GET['G2961_C59612'];
                        } ?>"  name="G2961_C59612"  placeholder="Dirección de Residencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59613" id="LblG2961_C59613">Teléfono</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59613" value="<?php if (isset($_GET['G2961_C59613'])) {
                            echo $_GET['G2961_C59613'];
                        } ?>"  name="G2961_C59613"  placeholder="Teléfono"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59614" id="LblG2961_C59614">Lugar de Residencia</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59614" value="<?php if (isset($_GET['G2961_C59614'])) {
                            echo $_GET['G2961_C59614'];
                        } ?>"  name="G2961_C59614"  placeholder="Lugar de Residencia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59615" id="LblG2961_C59615">Correo Electrónico</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59615" value="<?php if (isset($_GET['G2961_C59615'])) {
                            echo $_GET['G2961_C59615'];
                        } ?>"  name="G2961_C59615"  placeholder="Correo Electrónico"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59616" id="LblG2961_C59616">Telefono Movil</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59616" value="<?php if (isset($_GET['G2961_C59616'])) {
                            echo $_GET['G2961_C59616'];
                        } ?>"  name="G2961_C59616"  placeholder="Telefono Movil"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59617" id="LblG2961_C59617">Fecha Preinscrip.</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59617'])) {
                            echo $_GET['G2961_C59617'];
                        } ?>"  name="G2961_C59617" id="G2961_C59617" placeholder="YYYY-MM-DD" nombre="Fecha Preinscrip.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59618" id="LblG2961_C59618">Preins.Aprob.</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59618" value="<?php if (isset($_GET['G2961_C59618'])) {
                            echo $_GET['G2961_C59618'];
                        } ?>"  name="G2961_C59618"  placeholder="Preins.Aprob."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59619" id="LblG2961_C59619">Fecha Envio E-mail</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59619'])) {
                            echo $_GET['G2961_C59619'];
                        } ?>"  name="G2961_C59619" id="G2961_C59619" placeholder="YYYY-MM-DD" nombre="Fecha Envio E-mail">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59620" id="LblG2961_C59620">Fecha de pago</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59620'])) {
                            echo $_GET['G2961_C59620'];
                        } ?>"  name="G2961_C59620" id="G2961_C59620" placeholder="YYYY-MM-DD" nombre="Fecha de pago">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59621" id="LblG2961_C59621">Inscrito</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59621" value="<?php if (isset($_GET['G2961_C59621'])) {
                            echo $_GET['G2961_C59621'];
                        } ?>"  name="G2961_C59621"  placeholder="Inscrito"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59622" id="LblG2961_C59622">Fecha Inscripcion</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59622'])) {
                            echo $_GET['G2961_C59622'];
                        } ?>"  name="G2961_C59622" id="G2961_C59622" placeholder="YYYY-MM-DD" nombre="Fecha Inscripcion">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59623" id="LblG2961_C59623">Tiene Entrevista</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59623" value="<?php if (isset($_GET['G2961_C59623'])) {
                            echo $_GET['G2961_C59623'];
                        } ?>"  name="G2961_C59623"  placeholder="Tiene Entrevista"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59624" id="LblG2961_C59624">Fecha Entrevista</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59624'])) {
                            echo $_GET['G2961_C59624'];
                        } ?>"  name="G2961_C59624" id="G2961_C59624" placeholder="YYYY-MM-DD" nombre="Fecha Entrevista">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59625" id="LblG2961_C59625">Admitido</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59625" value="<?php if (isset($_GET['G2961_C59625'])) {
                            echo $_GET['G2961_C59625'];
                        } ?>"  name="G2961_C59625"  placeholder="Admitido"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59626" id="LblG2961_C59626">Codigo de Estudiante</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59626'])) {
                            echo $_GET['G2961_C59626'];
                        } ?>"  name="G2961_C59626" id="G2961_C59626" placeholder="Codigo de Estudiante"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59627" id="LblG2961_C59627">Fecha de Admitido</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59627'])) {
                            echo $_GET['G2961_C59627'];
                        } ?>"  name="G2961_C59627" id="G2961_C59627" placeholder="YYYY-MM-DD" nombre="Fecha de Admitido">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59628" id="LblG2961_C59628">Matricula Financiera</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59628" value="<?php if (isset($_GET['G2961_C59628'])) {
                            echo $_GET['G2961_C59628'];
                        } ?>"  name="G2961_C59628"  placeholder="Matricula Financiera"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59629" id="LblG2961_C59629">Fecha Matricula Financiera</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59629'])) {
                            echo $_GET['G2961_C59629'];
                        } ?>"  name="G2961_C59629" id="G2961_C59629" placeholder="YYYY-MM-DD" nombre="Fecha Matricula Financiera">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59630" id="LblG2961_C59630">Matricula Academica</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59630" value="<?php if (isset($_GET['G2961_C59630'])) {
                            echo $_GET['G2961_C59630'];
                        } ?>"  name="G2961_C59630"  placeholder="Matricula Academica"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59631" id="LblG2961_C59631">Fecha de matricula Academica</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G2961_C59631'])) {
                            echo $_GET['G2961_C59631'];
                        } ?>"  name="G2961_C59631" id="G2961_C59631" placeholder="YYYY-MM-DD" nombre="Fecha de matricula Academica">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G2961_C59632" id="LblG2961_C59632">Cantidad Matriculadas</label><input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G2961_C59632'])) {
                            echo $_GET['G2961_C59632'];
                        } ?>"  name="G2961_C59632" id="G2961_C59632" placeholder="Cantidad Matriculadas"></div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59633" id="LblG2961_C59633">Localidad</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59633" value="<?php if (isset($_GET['G2961_C59633'])) {
                            echo $_GET['G2961_C59633'];
                        } ?>"  name="G2961_C59633"  placeholder="Localidad"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9139" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9139c">
                ENCUESTA
            </a>
        </h4>
        
    </div>
    <div id="s_9139c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59800" id="LblG2961_C59800">de 1 a 5 como califica la atención recibida </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59800" value="<?php if (isset($_GET['G2961_C59800'])) {
                            echo $_GET['G2961_C59800'];
                        } ?>"  name="G2961_C59800"  placeholder="de 1 a 5 como califica la atención recibida "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59801" id="LblG2961_C59801">de 1 a 5 como califica la información recibida</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59801" value="<?php if (isset($_GET['G2961_C59801'])) {
                            echo $_GET['G2961_C59801'];
                        } ?>"  name="G2961_C59801"  placeholder="de 1 a 5 como califica la información recibida"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C59802" id="LblG2961_C59802">le gustaría que le contactemos en referencia a la gestión realizada 1 si 2 no </label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C59802" value="<?php if (isset($_GET['G2961_C59802'])) {
                            echo $_GET['G2961_C59802'];
                        } ?>"  name="G2961_C59802"  placeholder="le gustaría que le contactemos en referencia a la gestión realizada 1 si 2 no "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9344" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9344c">
                PREGUNTAS
            </a>
        </h4>
        
    </div>
    <div id="s_9344c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C60753" id="LblG2961_C60753">Pregunta de cierre: Nombre ¿Para Iniciar el camino a tu meta deseas que realicemos el proceso de inscripción puedo ayudarte y prometo que será breve.</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C60753" id="G2961_C60753">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3589 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C60754" id="LblG2961_C60754">*"¿Autorizas de manera expresa y consciente a la Universitaria Agustiniana para que de conformidad con la Ley 1581 de 2012 y sus decretos reglamentarios trate, almacene y transmita tus datos personales acá consignados?."</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C60754" id="G2961_C60754">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3589 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9340" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9340c">
                ACCIONES AUN NO DECIDE
            </a>
        </h4>
        
    </div>
    <div id="s_9340c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C60751" id="LblG2961_C60751">Posgrado o Pregrado</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C60751" id="G2961_C60751">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3625 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C61107" id="LblG2961_C61107">Envió de mensajes aun no decide #1</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C61107" id="G2961_C61107">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C61109" id="LblG2961_C61109">Envió de mensajes aun no decide #2</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C61109" id="G2961_C61109">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C61110" id="LblG2961_C61110">Envió de mensajes aun no decide #3</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C61110" id="G2961_C61110">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C61111" id="LblG2961_C61111">Envió de mensajes aun no decide #4</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C61111" id="G2961_C61111">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C61112" id="LblG2961_C61112">Envió de mensajes aun no decide #5</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C61112" id="G2961_C61112">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2961_C61113" id="LblG2961_C61113">Envió de mensajes aun no decide #6</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2961_C61113" id="G2961_C61113">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3585 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    if($obje->OPCION_ConsInte__b ==0){
                            echo "<option selected='true' value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }else{
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }
                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9383" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9383c">
                ACCIONES PRE-INSCRITO
            </a>
        </h4>
        
    </div>
    <div id="s_9383c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2961_C61411" id="LblG2961_C61411">Enviado a prescripción</label><input type="text" maxlength="253" onKeyDown="longitud(this.id)" onKeyUp="longitud(this.id)" onfocus="longitud(this.id)" onblur="longitud(this.id,true)" class="form-control input-sm" id="G2961_C61411" value="<?php if (isset($_GET['G2961_C61411'])) {
                            echo $_GET['G2961_C61411'];
                        } ?>" readonly name="G2961_C61411"  placeholder="Enviado a prescripción"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>

<div  class="panel box box-primary" id="9384" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_9384c">
                ACCIONES INSCRITO
            </a>
        </h4>
        
    </div>
    <div id="s_9384c" class="panel-collapse collapse in">
        <div class="box-body">

                </div>
            </div> <!-- AQUIFINSECCION -->
        </div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2961/G2961_eventos.js"></script>
<script type="text/javascript" src="formularios/G2961/G2961_extender_funcionalidad.php"></script><?php require_once "G2961_extender_funcionalidad.php";?><?php require_once "formularios/enviarSms_Mail.php";?>
<script type="text/javascript">
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }
    
    //escuchar mensajes de  otro formulario
    bindEvent(window, 'message', function (e) {
        console.log(e.data);
        
        
    });
    
    //enviar mensajes al formulario padre
    var sendMessage = function (msg) {
        window.parent.postMessage(msg, '*');
    };    
    var messageButton = document.getElementById('Save');    
    bindEvent(messageButton, 'click', function (e) {
        var mensaje;
        sendMessage('' + mensaje);
    });

    //JDBD - Funcion para descargar los adjuntos
    function bajarAdjunto(id){

        var strURL_t = $("#"+id).attr("adjunto");

        if (strURL_t != "") {

            location.href='<?=$url_crud;?>?adjunto='+strURL_t;
            
        }


    }
    
    

    $(function(){
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
            
            $("#G2961_C59156").val("0").trigger("change");
            $("#G2961_C59634").val("0").trigger("change");
            $("#G2961_C59606").val("0").trigger("change");
            $("#G2961_C59609").val("0").trigger("change");
            $("#G2961_C60753").val("0").trigger("change");
            $("#G2961_C60754").val("0").trigger("change");
            $("#G2961_C60751").val("0").trigger("change");
            $("#G2961_C61107").val("0").trigger("change");
            $("#G2961_C61109").val("0").trigger("change");
            $("#G2961_C61110").val("0").trigger("change");
            $("#G2961_C61111").val("0").trigger("change");
            $("#G2961_C61112").val("0").trigger("change");
            $("#G2961_C61113").val("0").trigger("change");
            
            
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2961_C59154").val(item.G2961_C59154); 
                $("#G2961_C59155").val(item.G2961_C59155); 
                $("#G2961_C59156").val(item.G2961_C59156).trigger("change");  
                $("#G2961_C59157").val(item.G2961_C59157); 
                $("#G2961_C59159").attr("opt",item.G2961_C59159);
                $("#G2961_C59159").val(item.G2961_C59159).trigger("change"); 
                $("#G2961_C59151").val(item.G2961_C59151); 
                $("#G2961_C59152").val(item.G2961_C59152); 
                $("#G2961_C59153").val(item.G2961_C59153).trigger("change");  
                $("#G2961_C59582").val(item.G2961_C59582); 
                $("#G2961_C59583").val(item.G2961_C59583); 
                $("#G2961_C59584").val(item.G2961_C59584); 
                $("#G2961_C59585").val(item.G2961_C59585); 
                $("#G2961_C59586").val(item.G2961_C59586); 
                $("#G2961_C59587").val(item.G2961_C59587); 
                $("#G2961_C59588").val(item.G2961_C59588); 
                $("#G2961_C59589").val(item.G2961_C59589); 
                $("#G2961_C59590").val(item.G2961_C59590); 
                $("#G2961_C59591").val(item.G2961_C59591); 
                $("#G2961_C59592").val(item.G2961_C59592); 
                $("#G2961_C59593").val(item.G2961_C59593); 
                $("#G2961_C59594").val(item.G2961_C59594); 
                $("#G2961_C59595").val(item.G2961_C59595); 
                $("#G2961_C59596").val(item.G2961_C59596); 
                $("#G2961_C59597").val(item.G2961_C59597); 
                $("#G2961_C59598").val(item.G2961_C59598); 
                $("#G2961_C59599").val(item.G2961_C59599); 
                $("#G2961_C59634").val(item.G2961_C59634).trigger("change");  
                $("#G2961_C59605").val(item.G2961_C59605); 
                $("#G2961_C59606").val(item.G2961_C59606).trigger("change");  
                $("#G2961_C59607").val(item.G2961_C59607); 
                $("#G2961_C59608").val(item.G2961_C59608); 
                $("#G2961_C59609").val(item.G2961_C59609).trigger("change");  
                $("#G2961_C59610").val(item.G2961_C59610); 
                $("#G2961_C59611").val(item.G2961_C59611); 
                $("#G2961_C59612").val(item.G2961_C59612); 
                $("#G2961_C59613").val(item.G2961_C59613); 
                $("#G2961_C59614").val(item.G2961_C59614); 
                $("#G2961_C59615").val(item.G2961_C59615); 
                $("#G2961_C59616").val(item.G2961_C59616); 
                $("#G2961_C59617").val(item.G2961_C59617); 
                $("#G2961_C59618").val(item.G2961_C59618); 
                $("#G2961_C59619").val(item.G2961_C59619); 
                $("#G2961_C59620").val(item.G2961_C59620); 
                $("#G2961_C59621").val(item.G2961_C59621); 
                $("#G2961_C59622").val(item.G2961_C59622); 
                $("#G2961_C59623").val(item.G2961_C59623); 
                $("#G2961_C59624").val(item.G2961_C59624); 
                $("#G2961_C59625").val(item.G2961_C59625); 
                $("#G2961_C59626").val(item.G2961_C59626); 
                $("#G2961_C59627").val(item.G2961_C59627); 
                $("#G2961_C59628").val(item.G2961_C59628); 
                $("#G2961_C59629").val(item.G2961_C59629); 
                $("#G2961_C59630").val(item.G2961_C59630); 
                $("#G2961_C59631").val(item.G2961_C59631); 
                $("#G2961_C59632").val(item.G2961_C59632); 
                $("#G2961_C59633").val(item.G2961_C59633); 
                $("#G2961_C59800").val(item.G2961_C59800); 
                $("#G2961_C59801").val(item.G2961_C59801); 
                $("#G2961_C59802").val(item.G2961_C59802); 
                $("#G2961_C60751").val(item.G2961_C60751).trigger("change");  
                $("#G2961_C61107").attr("opt",item.G2961_C61107);  
                $("#G2961_C61109").attr("opt",item.G2961_C61109);  
                $("#G2961_C61110").attr("opt",item.G2961_C61110);  
                $("#G2961_C61111").attr("opt",item.G2961_C61111);  
                $("#G2961_C61112").attr("opt",item.G2961_C61112);  
                $("#G2961_C61113").attr("opt",item.G2961_C61113);  
                $("#G2961_C60753").val(item.G2961_C60753).trigger("change");  
                $("#G2961_C60754").val(item.G2961_C60754).trigger("change");  
                $("#G2961_C61411").val(item.G2961_C61411);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2961_C59156").select2();
                            $("#G2961_C59159").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2961_C59159=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        try{
                                            try{
                                                after_select_G2961_C59159(data,document.getElementsByClassName('select2-search__field')[0].value);
                                            }catch{
                                                console.log('error');
                                            }
                                            return {
                                                results: $.map(data, function(obj) {
                                                    return {id: obj.id,text: obj.text};
                                                })
                                            };
                                        }catch{
                                            console.log('error');
                                        }
                                    },
                                    cache: true
                                }
                            });

                            $("#G2961_C59159").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G2961_C59159 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G2961_C59159").html('<option value="'+data.G2970_ConsInte__b+'" >'+data.G2970_C59413+'</option>');
                                        
                                    }
                                });
                            });

    $("#G2961_C59153").select2();

    $("#G2961_C59634").select2();

    $("#G2961_C59606").select2();

    $("#G2961_C59609").select2();

    $("#G2961_C60753").select2();

    $("#G2961_C60754").select2();

    $("#G2961_C60751").select2();

    $("#G2961_C61107").select2();

    $("#G2961_C61109").select2();

    $("#G2961_C61110").select2();

    $("#G2961_C61111").select2();

    $("#G2961_C61112").select2();

    $("#G2961_C61113").select2();
        //datepickers
        

        $("#G2961_C59154").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59585").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59617").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59619").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59620").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59622").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59624").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59627").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59629").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G2961_C59631").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        

        $("#G2961_C59155").numeric();
                
        $("#G2961_C59591").numeric();
                
        $("#G2961_C59592").numeric();
                
        $("#G2961_C59593").numeric();
                
        $("#G2961_C59610").numeric();
                
        $("#G2961_C59626").numeric();
                
        $("#G2961_C59632").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Sede 

    $("#G2961_C59156").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G2961_C59153").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Por cuál medio de comunicación  se enteró de la universidad? 

    $("#G2961_C59634").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Sede 

    $("#G2961_C59606").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Programa Académico 

    $("#G2961_C59609").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Pregunta de cierre: Nombre ¿Para Iniciar el camino a tu meta deseas que realicemos el proceso de inscripción puedo ayudarte y prometo que será breve. 

    $("#G2961_C60753").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para *"¿Autorizas de manera expresa y consciente a la Universitaria Agustiniana para que de conformidad con la Ley 1581 de 2012 y sus decretos reglamentarios trate, almacene y transmita tus datos personales acá consignados?." 

    $("#G2961_C60754").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Posgrado o Pregrado 

    $("#G2961_C60751").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3585' , idPadre : $(this).val() },
            success : function(data){
                var optG2961_C61107 = $("#G2961_C61107").attr("opt");
                $("#G2961_C61107").html(data);
                if (optG2961_C61107 != null) {
                    $("#G2961_C61107").val(optG2961_C61107).trigger("change");
                }
            }
        });
        
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3585' , idPadre : $(this).val() },
            success : function(data){
                var optG2961_C61109 = $("#G2961_C61109").attr("opt");
                $("#G2961_C61109").html(data);
                if (optG2961_C61109 != null) {
                    $("#G2961_C61109").val(optG2961_C61109).trigger("change");
                }
            }
        });
        
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3585' , idPadre : $(this).val() },
            success : function(data){
                var optG2961_C61110 = $("#G2961_C61110").attr("opt");
                $("#G2961_C61110").html(data);
                if (optG2961_C61110 != null) {
                    $("#G2961_C61110").val(optG2961_C61110).trigger("change");
                }
            }
        });
        
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3585' , idPadre : $(this).val() },
            success : function(data){
                var optG2961_C61111 = $("#G2961_C61111").attr("opt");
                $("#G2961_C61111").html(data);
                if (optG2961_C61111 != null) {
                    $("#G2961_C61111").val(optG2961_C61111).trigger("change");
                }
            }
        });
        
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3585' , idPadre : $(this).val() },
            success : function(data){
                var optG2961_C61112 = $("#G2961_C61112").attr("opt");
                $("#G2961_C61112").html(data);
                if (optG2961_C61112 != null) {
                    $("#G2961_C61112").val(optG2961_C61112).trigger("change");
                }
            }
        });
        
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '3585' , idPadre : $(this).val() },
            success : function(data){
                var optG2961_C61113 = $("#G2961_C61113").attr("opt");
                $("#G2961_C61113").html(data);
                if (optG2961_C61113 != null) {
                    $("#G2961_C61113").val(optG2961_C61113).trigger("change");
                }
            }
        });
        
    });

    //function para Envió de mensajes aun no decide #1 

    $("#G2961_C61107").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Envió de mensajes aun no decide #2 

    $("#G2961_C61109").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Envió de mensajes aun no decide #3 

    $("#G2961_C61110").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Envió de mensajes aun no decide #4 

    $("#G2961_C61111").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Envió de mensajes aun no decide #5 

    $("#G2961_C61112").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Envió de mensajes aun no decide #6 

    $("#G2961_C61113").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        function cierroGestion(){
                var bol_respuesta = before_save();
                if(bol_respuesta){            
                    $("#Save").attr("disabled",true);
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    if($("#calidad").val() =="0"){
                                    
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data;
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2961_C59154").val(item.G2961_C59154);
 
                                                $("#G2961_C59155").val(item.G2961_C59155);
 
                    $("#G2961_C59156").val(item.G2961_C59156).trigger("change"); 
 
                                                $("#G2961_C59157").val(item.G2961_C59157);
 
                    $("#G2961_C59159").attr("opt",item.G2961_C59159);
                    $("#G2961_C59159").val(item.G2961_C59159).trigger("change");
 
                                                $("#G2961_C59151").val(item.G2961_C59151);
 
                                                $("#G2961_C59152").val(item.G2961_C59152);
 
                    $("#G2961_C59153").val(item.G2961_C59153).trigger("change"); 
 
                                                $("#G2961_C59582").val(item.G2961_C59582);
 
                                                $("#G2961_C59583").val(item.G2961_C59583);
 
                                                $("#G2961_C59584").val(item.G2961_C59584);
 
                                                $("#G2961_C59585").val(item.G2961_C59585);
 
                                                $("#G2961_C59586").val(item.G2961_C59586);
 
                                                $("#G2961_C59587").val(item.G2961_C59587);
 
                                                $("#G2961_C59588").val(item.G2961_C59588);
 
                                                $("#G2961_C59589").val(item.G2961_C59589);
 
                                                $("#G2961_C59590").val(item.G2961_C59590);
 
                                                $("#G2961_C59591").val(item.G2961_C59591);
 
                                                $("#G2961_C59592").val(item.G2961_C59592);
 
                                                $("#G2961_C59593").val(item.G2961_C59593);
 
                                                $("#G2961_C59594").val(item.G2961_C59594);
 
                                                $("#G2961_C59595").val(item.G2961_C59595);
 
                                                $("#G2961_C59596").val(item.G2961_C59596);
 
                                                $("#G2961_C59597").val(item.G2961_C59597);
 
                                                $("#G2961_C59598").val(item.G2961_C59598);
 
                                                $("#G2961_C59599").val(item.G2961_C59599);
 
                    $("#G2961_C59634").val(item.G2961_C59634).trigger("change"); 
 
                                                $("#G2961_C59605").val(item.G2961_C59605);
 
                    $("#G2961_C59606").val(item.G2961_C59606).trigger("change"); 
 
                                                $("#G2961_C59607").val(item.G2961_C59607);
 
                                                $("#G2961_C59608").val(item.G2961_C59608);
 
                    $("#G2961_C59609").val(item.G2961_C59609).trigger("change"); 
 
                                                $("#G2961_C59610").val(item.G2961_C59610);
 
                                                $("#G2961_C59611").val(item.G2961_C59611);
 
                                                $("#G2961_C59612").val(item.G2961_C59612);
 
                                                $("#G2961_C59613").val(item.G2961_C59613);
 
                                                $("#G2961_C59614").val(item.G2961_C59614);
 
                                                $("#G2961_C59615").val(item.G2961_C59615);
 
                                                $("#G2961_C59616").val(item.G2961_C59616);
 
                                                $("#G2961_C59617").val(item.G2961_C59617);
 
                                                $("#G2961_C59618").val(item.G2961_C59618);
 
                                                $("#G2961_C59619").val(item.G2961_C59619);
 
                                                $("#G2961_C59620").val(item.G2961_C59620);
 
                                                $("#G2961_C59621").val(item.G2961_C59621);
 
                                                $("#G2961_C59622").val(item.G2961_C59622);
 
                                                $("#G2961_C59623").val(item.G2961_C59623);
 
                                                $("#G2961_C59624").val(item.G2961_C59624);
 
                                                $("#G2961_C59625").val(item.G2961_C59625);
 
                                                $("#G2961_C59626").val(item.G2961_C59626);
 
                                                $("#G2961_C59627").val(item.G2961_C59627);
 
                                                $("#G2961_C59628").val(item.G2961_C59628);
 
                                                $("#G2961_C59629").val(item.G2961_C59629);
 
                                                $("#G2961_C59630").val(item.G2961_C59630);
 
                                                $("#G2961_C59631").val(item.G2961_C59631);
 
                                                $("#G2961_C59632").val(item.G2961_C59632);
 
                                                $("#G2961_C59633").val(item.G2961_C59633);
 
                                                $("#G2961_C59800").val(item.G2961_C59800);
 
                                                $("#G2961_C59801").val(item.G2961_C59801);
 
                                                $("#G2961_C59802").val(item.G2961_C59802);
 
                    $("#G2961_C60751").val(item.G2961_C60751).trigger("change"); 
 
                    $("#G2961_C61107").attr("opt",item.G2961_C61107); 
 
                    $("#G2961_C61109").attr("opt",item.G2961_C61109); 
 
                    $("#G2961_C61110").attr("opt",item.G2961_C61110); 
 
                    $("#G2961_C61111").attr("opt",item.G2961_C61111); 
 
                    $("#G2961_C61112").attr("opt",item.G2961_C61112); 
 
                    $("#G2961_C61113").attr("opt",item.G2961_C61113); 
 
                    $("#G2961_C60753").val(item.G2961_C60753).trigger("change"); 
 
                    $("#G2961_C60754").val(item.G2961_C60754).trigger("change"); 
 
                                                $("#G2961_C61411").val(item.G2961_C61411);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  
                                    }else{
                                        $("#calidad").val("0");
                                    }
                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }        
        }
        
        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }
            
            /*let booValido=false;
            let showModal=false;
            let strPatrones="";
            let strEjemplo="";
            $('.error-phone').remove();
            $.each($('.telefono').prev(), function(b,key){
                if(this.value !="" && this.value !=0){
                    let strTelefono=this.value;
                    $.each(arr['patron_regexp'], function(i, item){
                        let regex=arr['patron_regexp'][i];
                        let delComillas=/'/g;
                        regex=regex.replace(delComillas,"");
                        let patron= new RegExp(regex);
                        if(patron.test(strTelefono)){
                            booValido=true;
                        }
                        strPatrones+=arr['patron'][i]+'  ';
                        strEjemplo+=arr['patron_ejemplo'][i]+'  ';
                    });
                    if(!booValido){
                        valido=1;
                        showModal=true;
                        $(this).closest(".form-group").append("<span class='error-phone' style='color:red;cursor:pointer' data-toggle='popover' data-trigger='hover' data-content='El número de teléfono digitado no es valido con estos formatos <br> "+strPatrones+" <br> Ejemplo: <br>"+strEjemplo+"'>Este número de teléfono no es valido <i style='color:red;' class='fa fa-question-circle'></i></span>");
                        $(this).closest(".form-group").addClass("has-error");
                        $('.error-phone').css("margin-top:7px");
                        $(this).focus();
                        $('[data-toggle="popover"]').popover({
                            html : true,
                            placement: "right"
                        });
                    }
                }
                
            });

            if(showModal){
            swal({
                html : true,
                title: "Número de télefono no valido",
                text: 'El registro que está guardando, no tiene ningún teléfono con un formato válido según lo definido.',
                type: "warning",
                confirmButtonText: "dejar los teléfonos así y guardar",
                cancelButtonText : "Modificar el/los télofonos",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        cierroGestion();
                    }else{
                        valido==1
                    }
                });                
            }*/

            if(valido == '0'){
                cierroGestion();
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Fecha pre-inscripción:','No. pre-inscripción:','Sede','Tipo de pre-inscripción:',' Programa académico','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY',' Tipo de documento:','Documento','Genero','Fecha Nacimiento','Primer Apellido','Segundo  Apellido','Primer Nombre','Segundo Nombre','Lugar de Nacimiento','Telefono Movil','Telefono de residencia','Estrato','Lugar de Residencia','Direccion de residencia','Barrio de residencia','Email del contacto','Correo Electronico','Confirmar correo Electronico','¿Por cuál medio de comunicación  se enteró de la universidad?','Período de Admisión','Sede','Nivel','Jornada','Programa Académico','Identificación','Nombre Aspirante','Dirección de Residencia','Teléfono','Lugar de Residencia','Correo Electrónico','Telefono Movil','Fecha Preinscrip.','Preins.Aprob.','Fecha Envio E-mail','Fecha de pago','Inscrito','Fecha Inscripcion','Tiene Entrevista','Fecha Entrevista','Admitido','Codigo de Estudiante','Fecha de Admitido','Matricula Financiera','Fecha Matricula Financiera','Matricula Academica','Fecha de matricula Academica','Cantidad Matriculadas','Localidad','de 1 a 5 como califica la atención recibida ','de 1 a 5 como califica la información recibida','le gustaría que le contactemos en referencia a la gestión realizada 1 si 2 no ','Posgrado o Pregrado','Envió de mensajes aun no decide #1','Envió de mensajes aun no decide #2','Envió de mensajes aun no decide #3','Envió de mensajes aun no decide #4','Envió de mensajes aun no decide #5','Envió de mensajes aun no decide #6','Pregunta de cierre: Nombre ¿Para Iniciar el camino a tu meta deseas que realicemos el proceso de inscripción puedo ayudarte y prometo que será breve.','*"¿Autorizas de manera expresa y consciente a la Universitaria Agustiniana para que de conformidad con la Ley 1581 de 2012 y sus decretos reglamentarios trate, almacene y transmita tus datos personales acá consignados?."','Enviado a prescripción'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {  
                        name:'G2961_C59154', 
                        index:'G2961_C59154', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G2961_C59155', 
                        index:'G2961_C59155', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2961_C59156', 
                        index:'G2961_C59156', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3586&campo=G2961_C59156'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59157', 
                        index: 'G2961_C59157', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59159', 
                        index:'G2961_C59159', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G2961_C59159=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59151', 
                        index: 'G2961_C59151', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59152', 
                        index: 'G2961_C59152', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59153', 
                        index:'G2961_C59153', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3584&campo=G2961_C59153'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59582', 
                        index: 'G2961_C59582', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59583', 
                        index: 'G2961_C59583', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59584', 
                        index: 'G2961_C59584', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59585', 
                        index:'G2961_C59585', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59586', 
                        index: 'G2961_C59586', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59587', 
                        index: 'G2961_C59587', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59588', 
                        index: 'G2961_C59588', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59589', 
                        index: 'G2961_C59589', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59590', 
                        index: 'G2961_C59590', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2961_C59591', 
                        index:'G2961_C59591', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G2961_C59592', 
                        index:'G2961_C59592', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G2961_C59593', 
                        index:'G2961_C59593', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2961_C59594', 
                        index: 'G2961_C59594', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59595', 
                        index: 'G2961_C59595', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59596', 
                        index: 'G2961_C59596', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59634', 
                        index:'G2961_C59634', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3621&campo=G2961_C59634'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59605', 
                        index: 'G2961_C59605', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59606', 
                        index:'G2961_C59606', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3586&campo=G2961_C59606'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59607', 
                        index: 'G2961_C59607', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59608', 
                        index: 'G2961_C59608', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59609', 
                        index:'G2961_C59609', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C59609'
                        }
                    }
 
                    ,
                    {  
                        name:'G2961_C59610', 
                        index:'G2961_C59610', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2961_C59611', 
                        index: 'G2961_C59611', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59612', 
                        index: 'G2961_C59612', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59613', 
                        index: 'G2961_C59613', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59614', 
                        index: 'G2961_C59614', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59615', 
                        index: 'G2961_C59615', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59616', 
                        index: 'G2961_C59616', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59617', 
                        index:'G2961_C59617', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59618', 
                        index: 'G2961_C59618', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59619', 
                        index:'G2961_C59619', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G2961_C59620', 
                        index:'G2961_C59620', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59621', 
                        index: 'G2961_C59621', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59622', 
                        index:'G2961_C59622', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59623', 
                        index: 'G2961_C59623', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59624', 
                        index:'G2961_C59624', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59625', 
                        index: 'G2961_C59625', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G2961_C59626', 
                        index:'G2961_C59626', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    {  
                        name:'G2961_C59627', 
                        index:'G2961_C59627', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59628', 
                        index: 'G2961_C59628', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59629', 
                        index:'G2961_C59629', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2961_C59630', 
                        index: 'G2961_C59630', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G2961_C59631', 
                        index:'G2961_C59631', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }
 
                    ,
                    {  
                        name:'G2961_C59632', 
                        index:'G2961_C59632', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G2961_C59633', 
                        index: 'G2961_C59633', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59800', 
                        index: 'G2961_C59800', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59801', 
                        index: 'G2961_C59801', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C59802', 
                        index: 'G2961_C59802', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2961_C60751', 
                        index:'G2961_C60751', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3625&campo=G2961_C60751'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61107', 
                        index:'G2961_C61107', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C61107'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61109', 
                        index:'G2961_C61109', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C61109'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61110', 
                        index:'G2961_C61110', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C61110'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61111', 
                        index:'G2961_C61111', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C61111'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61112', 
                        index:'G2961_C61112', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C61112'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61113', 
                        index:'G2961_C61113', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3585&campo=G2961_C61113'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C60753', 
                        index:'G2961_C60753', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3589&campo=G2961_C60753'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C60754', 
                        index:'G2961_C60754', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=3589&campo=G2961_C60754'
                        }
                    }

                    ,
                    { 
                        name:'G2961_C61411', 
                        index: 'G2961_C61411', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2961_C59154',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                    $("#G2961_C59154").val(item.G2961_C59154);

                    $("#G2961_C59155").val(item.G2961_C59155);
 
                    $("#G2961_C59156").val(item.G2961_C59156).trigger("change"); 

                    $("#G2961_C59157").val(item.G2961_C59157);
 
                    $("#G2961_C59159").attr("opt",item.G2961_C59159);
                    $("#G2961_C59159").val(item.G2961_C59159).trigger("change");

                    $("#G2961_C59151").val(item.G2961_C59151);

                    $("#G2961_C59152").val(item.G2961_C59152);
 
                    $("#G2961_C59153").val(item.G2961_C59153).trigger("change"); 

                    $("#G2961_C59582").val(item.G2961_C59582);

                    $("#G2961_C59583").val(item.G2961_C59583);

                    $("#G2961_C59584").val(item.G2961_C59584);

                    $("#G2961_C59585").val(item.G2961_C59585);

                    $("#G2961_C59586").val(item.G2961_C59586);

                    $("#G2961_C59587").val(item.G2961_C59587);

                    $("#G2961_C59588").val(item.G2961_C59588);

                    $("#G2961_C59589").val(item.G2961_C59589);

                    $("#G2961_C59590").val(item.G2961_C59590);

                    $("#G2961_C59591").val(item.G2961_C59591);

                    $("#G2961_C59592").val(item.G2961_C59592);

                    $("#G2961_C59593").val(item.G2961_C59593);

                    $("#G2961_C59594").val(item.G2961_C59594);

                    $("#G2961_C59595").val(item.G2961_C59595);

                    $("#G2961_C59596").val(item.G2961_C59596);

                    $("#G2961_C59597").val(item.G2961_C59597);

                    $("#G2961_C59598").val(item.G2961_C59598);

                    $("#G2961_C59599").val(item.G2961_C59599);
 
                    $("#G2961_C59634").val(item.G2961_C59634).trigger("change"); 

                    $("#G2961_C59605").val(item.G2961_C59605);
 
                    $("#G2961_C59606").val(item.G2961_C59606).trigger("change"); 

                    $("#G2961_C59607").val(item.G2961_C59607);

                    $("#G2961_C59608").val(item.G2961_C59608);
 
                    $("#G2961_C59609").val(item.G2961_C59609).trigger("change"); 

                    $("#G2961_C59610").val(item.G2961_C59610);

                    $("#G2961_C59611").val(item.G2961_C59611);

                    $("#G2961_C59612").val(item.G2961_C59612);

                    $("#G2961_C59613").val(item.G2961_C59613);

                    $("#G2961_C59614").val(item.G2961_C59614);

                    $("#G2961_C59615").val(item.G2961_C59615);

                    $("#G2961_C59616").val(item.G2961_C59616);

                    $("#G2961_C59617").val(item.G2961_C59617);

                    $("#G2961_C59618").val(item.G2961_C59618);

                    $("#G2961_C59619").val(item.G2961_C59619);

                    $("#G2961_C59620").val(item.G2961_C59620);

                    $("#G2961_C59621").val(item.G2961_C59621);

                    $("#G2961_C59622").val(item.G2961_C59622);

                    $("#G2961_C59623").val(item.G2961_C59623);

                    $("#G2961_C59624").val(item.G2961_C59624);

                    $("#G2961_C59625").val(item.G2961_C59625);

                    $("#G2961_C59626").val(item.G2961_C59626);

                    $("#G2961_C59627").val(item.G2961_C59627);

                    $("#G2961_C59628").val(item.G2961_C59628);

                    $("#G2961_C59629").val(item.G2961_C59629);

                    $("#G2961_C59630").val(item.G2961_C59630);

                    $("#G2961_C59631").val(item.G2961_C59631);

                    $("#G2961_C59632").val(item.G2961_C59632);

                    $("#G2961_C59633").val(item.G2961_C59633);

                    $("#G2961_C59800").val(item.G2961_C59800);

                    $("#G2961_C59801").val(item.G2961_C59801);

                    $("#G2961_C59802").val(item.G2961_C59802);
 
                    $("#G2961_C60751").val(item.G2961_C60751).trigger("change"); 
 
                    $("#G2961_C61107").attr("opt",item.G2961_C61107); 
 
                    $("#G2961_C61109").attr("opt",item.G2961_C61109); 
 
                    $("#G2961_C61110").attr("opt",item.G2961_C61110); 
 
                    $("#G2961_C61111").attr("opt",item.G2961_C61111); 
 
                    $("#G2961_C61112").attr("opt",item.G2961_C61112); 
 
                    $("#G2961_C61113").attr("opt",item.G2961_C61113); 
 
                    $("#G2961_C60753").val(item.G2961_C60753).trigger("change"); 
 
                    $("#G2961_C60754").val(item.G2961_C60754).trigger("change"); 

                    $("#G2961_C61411").val(item.G2961_C61411);
                        
                    $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
            
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    }
    
    function CalcularFormula(){
        
    }

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
