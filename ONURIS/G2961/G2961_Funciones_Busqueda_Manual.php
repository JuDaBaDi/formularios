<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		if($_GET['action'] == "GET_DATOS"){

			//JDBD - Busqueda manual.          
            if(isset($_GET["id_campana_crm"]) && $_GET["id_campana_crm"] != 0){

                $querymuestra="SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b FROM ".$BaseDatos_systema.".CAMPAN where CAMPAN_ConsInte__b=".$_GET["id_campana_crm"];

                $querymuestra=$mysqli->query($querymuestra);

                if($querymuestra && $querymuestra->num_rows > 0){
                    $datoCampan=$querymuestra->fetch_array();

                    $str_Pobla_Campan = "G".$datoCampan["CAMPAN_ConsInte__GUION__Pob_b"];
                    $intMuestra= $str_Pobla_Campan."_M".$datoCampan["CAMPAN_ConsInte__MUESTR_b"];
                    
                }        
            }
            $str_Lsql = " SELECT *, ".$intMuestra."_CoInMiPo__b AS id_muestra FROM ".$BaseDatos.".".$str_Pobla_Campan." LEFT JOIN ".$BaseDatos.".".$intMuestra." ON ".$str_Pobla_Campan."_ConsInte__b = ".$intMuestra."_CoInMiPo__b";
                $Where = " WHERE ".$str_Pobla_Campan."_ConsInte__b > 0 AND ";
			$usados = 0;
			if(isset($_POST['G2961_C59583']) && !is_null($_POST['G2961_C59583']) && $_POST['G2961_C59583'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2961_C59583 LIKE '%". $_POST['G2961_C59583'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G2961_C59586']) && !is_null($_POST['G2961_C59586']) && $_POST['G2961_C59586'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2961_C59586 LIKE '%". $_POST['G2961_C59586'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G2961_C59587']) && !is_null($_POST['G2961_C59587']) && $_POST['G2961_C59587'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2961_C59587 LIKE '%". $_POST['G2961_C59587'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G2961_C59588']) && !is_null($_POST['G2961_C59588']) && $_POST['G2961_C59588'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2961_C59588 LIKE '%". $_POST['G2961_C59588'] ."%' ";
				$usados = 1;
			}
			if(isset($_POST['G2961_C59589']) && !is_null($_POST['G2961_C59589']) && $_POST['G2961_C59589'] != ''){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." G2961_C59589 LIKE '%". $_POST['G2961_C59589'] ."%' ";
				$usados = 1;
			}
            if($usados==0){
                    $Where = "";
                }
                $str_Lsql .= $Where;
                
            //echo $str_Lsql;
			$resultado = $mysqli->query($str_Lsql);
			$arrayDatos = array();
			while ($key = $resultado->fetch_assoc()) {
				$arrayDatos[] = $key;
			}

			$newJson = array();
			$newJson[0]['cantidad_registros'] = $resultado->num_rows;
			$newJson[0]['registros'] = $arrayDatos;
            if($usados == 0 && $resultado->num_rows == 0){
                $newJson[0]['mensaje'] = "No se encontraron registros para este tipo de busqueda";
            }else{
                if($usados == 1 && $resultado->num_rows == 0){
                    $strSQLValidaReg="SELECT * FROM {$BaseDatos}.{$str_Pobla_Campan}{$Where}";
                       $strSQLValidaReg=$mysqli->query($strSQLValidaReg);
                    if($strSQLValidaReg ->num_rows > 0){
                        $newJson[0]['mensaje'] = "No tienes permiso para acceder a este registro";
                    }
                }
            }

			echo json_encode($newJson);
		}
	}
?>
