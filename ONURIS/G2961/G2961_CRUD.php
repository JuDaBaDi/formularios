<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2961_ConsInte__b, G2961_FechaInsercion , G2961_Usuario ,  G2961_CodigoMiembro  , G2961_PoblacionOrigen , G2961_EstadoDiligenciamiento ,  G2961_IdLlamada , G2961_C59154 as principal ,G2961_C59154,G2961_C59155,G2961_C59156,G2961_C59157,G2961_C59159,G2961_C59151,G2961_C59152,G2961_C59153,G2961_C59582,G2961_C59583,G2961_C59584,G2961_C59585,G2961_C59586,G2961_C59587,G2961_C59588,G2961_C59589,G2961_C59590,G2961_C59591,G2961_C59592,G2961_C59593,G2961_C59594,G2961_C59595,G2961_C59596,G2961_C59597,G2961_C59598,G2961_C59599,G2961_C59634,G2961_C59605,G2961_C59606,G2961_C59607,G2961_C59608,G2961_C59609,G2961_C59610,G2961_C59611,G2961_C59612,G2961_C59613,G2961_C59614,G2961_C59615,G2961_C59616,G2961_C59617,G2961_C59618,G2961_C59619,G2961_C59620,G2961_C59621,G2961_C59622,G2961_C59623,G2961_C59624,G2961_C59625,G2961_C59626,G2961_C59627,G2961_C59628,G2961_C59629,G2961_C59630,G2961_C59631,G2961_C59632,G2961_C59633,G2961_C59800,G2961_C59801,G2961_C59802,G2961_C60751,G2961_C61107,G2961_C61109,G2961_C61110,G2961_C61111,G2961_C61112,G2961_C61113,G2961_C60753,G2961_C60754,G2961_C61411 FROM '.$BaseDatos.'.G2961 WHERE G2961_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2961_C59154'] = explode(' ', $key->G2961_C59154)[0];

                $datos[$i]['G2961_C59155'] = $key->G2961_C59155;

                $datos[$i]['G2961_C59156'] = $key->G2961_C59156;

                $datos[$i]['G2961_C59157'] = $key->G2961_C59157;

                $datos[$i]['G2961_C59159'] = $key->G2961_C59159;

                $datos[$i]['G2961_C59151'] = $key->G2961_C59151;

                $datos[$i]['G2961_C59152'] = $key->G2961_C59152;

                $datos[$i]['G2961_C59153'] = $key->G2961_C59153;

                $datos[$i]['G2961_C59582'] = $key->G2961_C59582;

                $datos[$i]['G2961_C59583'] = $key->G2961_C59583;

                $datos[$i]['G2961_C59584'] = $key->G2961_C59584;

                $datos[$i]['G2961_C59585'] = explode(' ', $key->G2961_C59585)[0];

                $datos[$i]['G2961_C59586'] = $key->G2961_C59586;

                $datos[$i]['G2961_C59587'] = $key->G2961_C59587;

                $datos[$i]['G2961_C59588'] = $key->G2961_C59588;

                $datos[$i]['G2961_C59589'] = $key->G2961_C59589;

                $datos[$i]['G2961_C59590'] = $key->G2961_C59590;

                $datos[$i]['G2961_C59591'] = $key->G2961_C59591;

                $datos[$i]['G2961_C59592'] = $key->G2961_C59592;

                $datos[$i]['G2961_C59593'] = $key->G2961_C59593;

                $datos[$i]['G2961_C59594'] = $key->G2961_C59594;

                $datos[$i]['G2961_C59595'] = $key->G2961_C59595;

                $datos[$i]['G2961_C59596'] = $key->G2961_C59596;

                $datos[$i]['G2961_C59597'] = $key->G2961_C59597;

                $datos[$i]['G2961_C59598'] = $key->G2961_C59598;

                $datos[$i]['G2961_C59599'] = $key->G2961_C59599;

                $datos[$i]['G2961_C59634'] = $key->G2961_C59634;

                $datos[$i]['G2961_C59605'] = $key->G2961_C59605;

                $datos[$i]['G2961_C59606'] = $key->G2961_C59606;

                $datos[$i]['G2961_C59607'] = $key->G2961_C59607;

                $datos[$i]['G2961_C59608'] = $key->G2961_C59608;

                $datos[$i]['G2961_C59609'] = $key->G2961_C59609;

                $datos[$i]['G2961_C59610'] = $key->G2961_C59610;

                $datos[$i]['G2961_C59611'] = $key->G2961_C59611;

                $datos[$i]['G2961_C59612'] = $key->G2961_C59612;

                $datos[$i]['G2961_C59613'] = $key->G2961_C59613;

                $datos[$i]['G2961_C59614'] = $key->G2961_C59614;

                $datos[$i]['G2961_C59615'] = $key->G2961_C59615;

                $datos[$i]['G2961_C59616'] = $key->G2961_C59616;

                $datos[$i]['G2961_C59617'] = explode(' ', $key->G2961_C59617)[0];

                $datos[$i]['G2961_C59618'] = $key->G2961_C59618;

                $datos[$i]['G2961_C59619'] = explode(' ', $key->G2961_C59619)[0];

                $datos[$i]['G2961_C59620'] = explode(' ', $key->G2961_C59620)[0];

                $datos[$i]['G2961_C59621'] = $key->G2961_C59621;

                $datos[$i]['G2961_C59622'] = explode(' ', $key->G2961_C59622)[0];

                $datos[$i]['G2961_C59623'] = $key->G2961_C59623;

                $datos[$i]['G2961_C59624'] = explode(' ', $key->G2961_C59624)[0];

                $datos[$i]['G2961_C59625'] = $key->G2961_C59625;

                $datos[$i]['G2961_C59626'] = $key->G2961_C59626;

                $datos[$i]['G2961_C59627'] = explode(' ', $key->G2961_C59627)[0];

                $datos[$i]['G2961_C59628'] = $key->G2961_C59628;

                $datos[$i]['G2961_C59629'] = explode(' ', $key->G2961_C59629)[0];

                $datos[$i]['G2961_C59630'] = $key->G2961_C59630;

                $datos[$i]['G2961_C59631'] = explode(' ', $key->G2961_C59631)[0];

                $datos[$i]['G2961_C59632'] = $key->G2961_C59632;

                $datos[$i]['G2961_C59633'] = $key->G2961_C59633;

                $datos[$i]['G2961_C59800'] = $key->G2961_C59800;

                $datos[$i]['G2961_C59801'] = $key->G2961_C59801;

                $datos[$i]['G2961_C59802'] = $key->G2961_C59802;

                $datos[$i]['G2961_C60751'] = $key->G2961_C60751;

                $datos[$i]['G2961_C61107'] = $key->G2961_C61107;

                $datos[$i]['G2961_C61109'] = $key->G2961_C61109;

                $datos[$i]['G2961_C61110'] = $key->G2961_C61110;

                $datos[$i]['G2961_C61111'] = $key->G2961_C61111;

                $datos[$i]['G2961_C61112'] = $key->G2961_C61112;

                $datos[$i]['G2961_C61113'] = $key->G2961_C61113;

                $datos[$i]['G2961_C60753'] = $key->G2961_C60753;

                $datos[$i]['G2961_C60754'] = $key->G2961_C60754;

                $datos[$i]['G2961_C61411'] = $key->G2961_C61411;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2961";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2961_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2961_ConsInte__b as id,  G2961_C59154 as camp1 , G2961_C59157 as camp2 
                     FROM ".$BaseDatos.".G2961  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2961_ConsInte__b as id,  G2961_C59154 as camp1 , G2961_C59157 as camp2  
                    FROM ".$BaseDatos.".G2961  JOIN ".$BaseDatos.".G2961_M".$_POST['muestra']." ON G2961_ConsInte__b = G2961_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2961_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2961_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2961_C59154 LIKE '%".$B."%' OR G2961_C59157 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2961_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2961_C59159'])){
                                $Ysql = "SELECT G2970_ConsInte__b as id,  as text FROM ".$BaseDatos.".G2970 WHERE  LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2961_C59159"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2970 WHERE G2970_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2961_C59159"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2961");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2961_ConsInte__b, G2961_FechaInsercion , G2961_Usuario ,  G2961_CodigoMiembro  , G2961_PoblacionOrigen , G2961_EstadoDiligenciamiento ,  G2961_IdLlamada , G2961_C59154 as principal ,G2961_C59154,G2961_C59155, a.LISOPC_Nombre____b as G2961_C59156,G2961_C59157,G2961_C59151,G2961_C59152, b.LISOPC_Nombre____b as G2961_C59153,G2961_C59582,G2961_C59583,G2961_C59584,G2961_C59585,G2961_C59586,G2961_C59587,G2961_C59588,G2961_C59589,G2961_C59590,G2961_C59591,G2961_C59592,G2961_C59593,G2961_C59594,G2961_C59595,G2961_C59596,G2961_C59597,G2961_C59598,G2961_C59599, c.LISOPC_Nombre____b as G2961_C59634,G2961_C59605, d.LISOPC_Nombre____b as G2961_C59606,G2961_C59607,G2961_C59608, e.LISOPC_Nombre____b as G2961_C59609,G2961_C59610,G2961_C59611,G2961_C59612,G2961_C59613,G2961_C59614,G2961_C59615,G2961_C59616,G2961_C59617,G2961_C59618,G2961_C59619,G2961_C59620,G2961_C59621,G2961_C59622,G2961_C59623,G2961_C59624,G2961_C59625,G2961_C59626,G2961_C59627,G2961_C59628,G2961_C59629,G2961_C59630,G2961_C59631,G2961_C59632,G2961_C59633,G2961_C59800,G2961_C59801,G2961_C59802, f.LISOPC_Nombre____b as G2961_C60751, g.LISOPC_Nombre____b as G2961_C61107, h.LISOPC_Nombre____b as G2961_C61109, i.LISOPC_Nombre____b as G2961_C61110, j.LISOPC_Nombre____b as G2961_C61111, k.LISOPC_Nombre____b as G2961_C61112, l.LISOPC_Nombre____b as G2961_C61113, m.LISOPC_Nombre____b as G2961_C60753, n.LISOPC_Nombre____b as G2961_C60754,G2961_C61411 FROM '.$BaseDatos.'.G2961 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2961_C59156 LEFT JOIN '.$BaseDatos.'.G2970 ON G2970_ConsInte__b  =  G2961_C59159 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2961_C59153 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2961_C59634 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2961_C59606 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2961_C59609 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2961_C60751 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2961_C61107 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2961_C61109 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2961_C61110 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2961_C61111 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G2961_C61112 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G2961_C61113 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G2961_C60753 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G2961_C60754';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2961_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2961_ConsInte__b , explode(' ', $fila->G2961_C59154)[0] , ($fila->G2961_C59155) , ($fila->G2961_C59156) , ($fila->G2961_C59157) , ($fila->G2961_C59151) , ($fila->G2961_C59152) , ($fila->G2961_C59153) , ($fila->G2961_C59582) , ($fila->G2961_C59583) , ($fila->G2961_C59584) , explode(' ', $fila->G2961_C59585)[0] , ($fila->G2961_C59586) , ($fila->G2961_C59587) , ($fila->G2961_C59588) , ($fila->G2961_C59589) , ($fila->G2961_C59590) , ($fila->G2961_C59591) , ($fila->G2961_C59592) , ($fila->G2961_C59593) , ($fila->G2961_C59594) , ($fila->G2961_C59595) , ($fila->G2961_C59596) , ($fila->G2961_C59597) , ($fila->G2961_C59598) , ($fila->G2961_C59599) , ($fila->G2961_C59634) , ($fila->G2961_C59605) , ($fila->G2961_C59606) , ($fila->G2961_C59607) , ($fila->G2961_C59608) , ($fila->G2961_C59609) , ($fila->G2961_C59610) , ($fila->G2961_C59611) , ($fila->G2961_C59612) , ($fila->G2961_C59613) , ($fila->G2961_C59614) , ($fila->G2961_C59615) , ($fila->G2961_C59616) , explode(' ', $fila->G2961_C59617)[0] , ($fila->G2961_C59618) , explode(' ', $fila->G2961_C59619)[0] , explode(' ', $fila->G2961_C59620)[0] , ($fila->G2961_C59621) , explode(' ', $fila->G2961_C59622)[0] , ($fila->G2961_C59623) , explode(' ', $fila->G2961_C59624)[0] , ($fila->G2961_C59625) , ($fila->G2961_C59626) , explode(' ', $fila->G2961_C59627)[0] , ($fila->G2961_C59628) , explode(' ', $fila->G2961_C59629)[0] , ($fila->G2961_C59630) , explode(' ', $fila->G2961_C59631)[0] , ($fila->G2961_C59632) , ($fila->G2961_C59633) , ($fila->G2961_C59800) , ($fila->G2961_C59801) , ($fila->G2961_C59802) , ($fila->G2961_C60751) , ($fila->G2961_C61107) , ($fila->G2961_C61109) , ($fila->G2961_C61110) , ($fila->G2961_C61111) , ($fila->G2961_C61112) , ($fila->G2961_C61113) , ($fila->G2961_C60753) , ($fila->G2961_C60754) , ($fila->G2961_C61411) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2961 WHERE G2961_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2961";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2961_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2961_ConsInte__b as id,  G2961_C59154 as camp1 , G2961_C59157 as camp2  FROM '.$BaseDatos.'.G2961 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2961_ConsInte__b as id,  G2961_C59154 as camp1 , G2961_C59157 as camp2  
                    FROM ".$BaseDatos.".G2961  JOIN ".$BaseDatos.".G2961_M".$_POST['muestra']." ON G2961_ConsInte__b = G2961_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2961_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2961_C59154 LIKE "%'.$B.'%" OR G2961_C59157 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2961_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2961 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2961(";
            $LsqlV = " VALUES ("; 
 
            $G2961_C59154 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59154"])){    
                if($_POST["G2961_C59154"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59154"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59154 = "'".$_POST["G2961_C59154"]."'";
                    }else{
                        $G2961_C59154 = "'".str_replace(' ', '',$_POST["G2961_C59154"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59154 = ".$G2961_C59154;
                    $LsqlI .= $separador." G2961_C59154";
                    $LsqlV .= $separador.$G2961_C59154;
                    $validar = 1;
                }
            }
  
            $G2961_C59155 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59155"])){
                if($_POST["G2961_C59155"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59155 = $_POST["G2961_C59155"];
                    $LsqlU .= $separador." G2961_C59155 = ".$G2961_C59155."";
                    $LsqlI .= $separador." G2961_C59155";
                    $LsqlV .= $separador.$G2961_C59155;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59156"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59156 = '".$_POST["G2961_C59156"]."'";
                $LsqlI .= $separador."G2961_C59156";
                $LsqlV .= $separador."'".$_POST["G2961_C59156"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59157"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59157 = '".$_POST["G2961_C59157"]."'";
                $LsqlI .= $separador."G2961_C59157";
                $LsqlV .= $separador."'".$_POST["G2961_C59157"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59159 = '".$_POST["G2961_C59159"]."'";
                $LsqlI .= $separador."G2961_C59159";
                $LsqlV .= $separador."'".$_POST["G2961_C59159"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59151"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59151 = '".$_POST["G2961_C59151"]."'";
                $LsqlI .= $separador."G2961_C59151";
                $LsqlV .= $separador."'".$_POST["G2961_C59151"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59152"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59152 = '".$_POST["G2961_C59152"]."'";
                $LsqlI .= $separador."G2961_C59152";
                $LsqlV .= $separador."'".$_POST["G2961_C59152"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59153 = '".$_POST["G2961_C59153"]."'";
                $LsqlI .= $separador."G2961_C59153";
                $LsqlV .= $separador."'".$_POST["G2961_C59153"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59582"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59582 = '".$_POST["G2961_C59582"]."'";
                $LsqlI .= $separador."G2961_C59582";
                $LsqlV .= $separador."'".$_POST["G2961_C59582"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59583"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59583 = '".$_POST["G2961_C59583"]."'";
                $LsqlI .= $separador."G2961_C59583";
                $LsqlV .= $separador."'".$_POST["G2961_C59583"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59584"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59584 = '".$_POST["G2961_C59584"]."'";
                $LsqlI .= $separador."G2961_C59584";
                $LsqlV .= $separador."'".$_POST["G2961_C59584"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59585 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59585"])){    
                if($_POST["G2961_C59585"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59585"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59585 = "'".$_POST["G2961_C59585"]."'";
                    }else{
                        $G2961_C59585 = "'".str_replace(' ', '',$_POST["G2961_C59585"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59585 = ".$G2961_C59585;
                    $LsqlI .= $separador." G2961_C59585";
                    $LsqlV .= $separador.$G2961_C59585;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59586"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59586 = '".$_POST["G2961_C59586"]."'";
                $LsqlI .= $separador."G2961_C59586";
                $LsqlV .= $separador."'".$_POST["G2961_C59586"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59587"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59587 = '".$_POST["G2961_C59587"]."'";
                $LsqlI .= $separador."G2961_C59587";
                $LsqlV .= $separador."'".$_POST["G2961_C59587"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59588"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59588 = '".$_POST["G2961_C59588"]."'";
                $LsqlI .= $separador."G2961_C59588";
                $LsqlV .= $separador."'".$_POST["G2961_C59588"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59589"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59589 = '".$_POST["G2961_C59589"]."'";
                $LsqlI .= $separador."G2961_C59589";
                $LsqlV .= $separador."'".$_POST["G2961_C59589"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59590 = '".$_POST["G2961_C59590"]."'";
                $LsqlI .= $separador."G2961_C59590";
                $LsqlV .= $separador."'".$_POST["G2961_C59590"]."'";
                $validar = 1;
            }
             
  
            $G2961_C59591 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59591"])){
                if($_POST["G2961_C59591"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59591 = $_POST["G2961_C59591"];
                    $LsqlU .= $separador." G2961_C59591 = ".$G2961_C59591."";
                    $LsqlI .= $separador." G2961_C59591";
                    $LsqlV .= $separador.$G2961_C59591;
                    $validar = 1;
                }
            }
  
            $G2961_C59592 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59592"])){
                if($_POST["G2961_C59592"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59592 = $_POST["G2961_C59592"];
                    $LsqlU .= $separador." G2961_C59592 = ".$G2961_C59592."";
                    $LsqlI .= $separador." G2961_C59592";
                    $LsqlV .= $separador.$G2961_C59592;
                    $validar = 1;
                }
            }
  
            $G2961_C59593 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59593"])){
                if($_POST["G2961_C59593"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59593 = $_POST["G2961_C59593"];
                    $LsqlU .= $separador." G2961_C59593 = ".$G2961_C59593."";
                    $LsqlI .= $separador." G2961_C59593";
                    $LsqlV .= $separador.$G2961_C59593;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59594"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59594 = '".$_POST["G2961_C59594"]."'";
                $LsqlI .= $separador."G2961_C59594";
                $LsqlV .= $separador."'".$_POST["G2961_C59594"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59595"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59595 = '".$_POST["G2961_C59595"]."'";
                $LsqlI .= $separador."G2961_C59595";
                $LsqlV .= $separador."'".$_POST["G2961_C59595"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59596"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59596 = '".$_POST["G2961_C59596"]."'";
                $LsqlI .= $separador."G2961_C59596";
                $LsqlV .= $separador."'".$_POST["G2961_C59596"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59597"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59597 = '".$_POST["G2961_C59597"]."'";
                $LsqlI .= $separador."G2961_C59597";
                $LsqlV .= $separador."'".$_POST["G2961_C59597"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59598"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59598 = '".$_POST["G2961_C59598"]."'";
                $LsqlI .= $separador."G2961_C59598";
                $LsqlV .= $separador."'".$_POST["G2961_C59598"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59599"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59599 = '".$_POST["G2961_C59599"]."'";
                $LsqlI .= $separador."G2961_C59599";
                $LsqlV .= $separador."'".$_POST["G2961_C59599"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59634"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59634 = '".$_POST["G2961_C59634"]."'";
                $LsqlI .= $separador."G2961_C59634";
                $LsqlV .= $separador."'".$_POST["G2961_C59634"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59605"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59605 = '".$_POST["G2961_C59605"]."'";
                $LsqlI .= $separador."G2961_C59605";
                $LsqlV .= $separador."'".$_POST["G2961_C59605"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59606"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59606 = '".$_POST["G2961_C59606"]."'";
                $LsqlI .= $separador."G2961_C59606";
                $LsqlV .= $separador."'".$_POST["G2961_C59606"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59607"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59607 = '".$_POST["G2961_C59607"]."'";
                $LsqlI .= $separador."G2961_C59607";
                $LsqlV .= $separador."'".$_POST["G2961_C59607"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59608"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59608 = '".$_POST["G2961_C59608"]."'";
                $LsqlI .= $separador."G2961_C59608";
                $LsqlV .= $separador."'".$_POST["G2961_C59608"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59609"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59609 = '".$_POST["G2961_C59609"]."'";
                $LsqlI .= $separador."G2961_C59609";
                $LsqlV .= $separador."'".$_POST["G2961_C59609"]."'";
                $validar = 1;
            }
             
  
            $G2961_C59610 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59610"])){
                if($_POST["G2961_C59610"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59610 = $_POST["G2961_C59610"];
                    $LsqlU .= $separador." G2961_C59610 = ".$G2961_C59610."";
                    $LsqlI .= $separador." G2961_C59610";
                    $LsqlV .= $separador.$G2961_C59610;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59611"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59611 = '".$_POST["G2961_C59611"]."'";
                $LsqlI .= $separador."G2961_C59611";
                $LsqlV .= $separador."'".$_POST["G2961_C59611"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59612"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59612 = '".$_POST["G2961_C59612"]."'";
                $LsqlI .= $separador."G2961_C59612";
                $LsqlV .= $separador."'".$_POST["G2961_C59612"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59613"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59613 = '".$_POST["G2961_C59613"]."'";
                $LsqlI .= $separador."G2961_C59613";
                $LsqlV .= $separador."'".$_POST["G2961_C59613"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59614"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59614 = '".$_POST["G2961_C59614"]."'";
                $LsqlI .= $separador."G2961_C59614";
                $LsqlV .= $separador."'".$_POST["G2961_C59614"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59615"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59615 = '".$_POST["G2961_C59615"]."'";
                $LsqlI .= $separador."G2961_C59615";
                $LsqlV .= $separador."'".$_POST["G2961_C59615"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59616"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59616 = '".$_POST["G2961_C59616"]."'";
                $LsqlI .= $separador."G2961_C59616";
                $LsqlV .= $separador."'".$_POST["G2961_C59616"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59617 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59617"])){    
                if($_POST["G2961_C59617"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59617"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59617 = "'".$_POST["G2961_C59617"]."'";
                    }else{
                        $G2961_C59617 = "'".str_replace(' ', '',$_POST["G2961_C59617"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59617 = ".$G2961_C59617;
                    $LsqlI .= $separador." G2961_C59617";
                    $LsqlV .= $separador.$G2961_C59617;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59618"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59618 = '".$_POST["G2961_C59618"]."'";
                $LsqlI .= $separador."G2961_C59618";
                $LsqlV .= $separador."'".$_POST["G2961_C59618"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59619 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59619"])){    
                if($_POST["G2961_C59619"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59619"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59619 = "'".$_POST["G2961_C59619"]."'";
                    }else{
                        $G2961_C59619 = "'".str_replace(' ', '',$_POST["G2961_C59619"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59619 = ".$G2961_C59619;
                    $LsqlI .= $separador." G2961_C59619";
                    $LsqlV .= $separador.$G2961_C59619;
                    $validar = 1;
                }
            }
 
            $G2961_C59620 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59620"])){    
                if($_POST["G2961_C59620"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59620"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59620 = "'".$_POST["G2961_C59620"]."'";
                    }else{
                        $G2961_C59620 = "'".str_replace(' ', '',$_POST["G2961_C59620"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59620 = ".$G2961_C59620;
                    $LsqlI .= $separador." G2961_C59620";
                    $LsqlV .= $separador.$G2961_C59620;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59621"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59621 = '".$_POST["G2961_C59621"]."'";
                $LsqlI .= $separador."G2961_C59621";
                $LsqlV .= $separador."'".$_POST["G2961_C59621"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59622 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59622"])){    
                if($_POST["G2961_C59622"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59622"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59622 = "'".$_POST["G2961_C59622"]."'";
                    }else{
                        $G2961_C59622 = "'".str_replace(' ', '',$_POST["G2961_C59622"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59622 = ".$G2961_C59622;
                    $LsqlI .= $separador." G2961_C59622";
                    $LsqlV .= $separador.$G2961_C59622;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59623"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59623 = '".$_POST["G2961_C59623"]."'";
                $LsqlI .= $separador."G2961_C59623";
                $LsqlV .= $separador."'".$_POST["G2961_C59623"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59624 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59624"])){    
                if($_POST["G2961_C59624"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59624"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59624 = "'".$_POST["G2961_C59624"]."'";
                    }else{
                        $G2961_C59624 = "'".str_replace(' ', '',$_POST["G2961_C59624"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59624 = ".$G2961_C59624;
                    $LsqlI .= $separador." G2961_C59624";
                    $LsqlV .= $separador.$G2961_C59624;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59625"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59625 = '".$_POST["G2961_C59625"]."'";
                $LsqlI .= $separador."G2961_C59625";
                $LsqlV .= $separador."'".$_POST["G2961_C59625"]."'";
                $validar = 1;
            }
             
  
            $G2961_C59626 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59626"])){
                if($_POST["G2961_C59626"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59626 = $_POST["G2961_C59626"];
                    $LsqlU .= $separador." G2961_C59626 = ".$G2961_C59626."";
                    $LsqlI .= $separador." G2961_C59626";
                    $LsqlV .= $separador.$G2961_C59626;
                    $validar = 1;
                }
            }
 
            $G2961_C59627 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59627"])){    
                if($_POST["G2961_C59627"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59627"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59627 = "'".$_POST["G2961_C59627"]."'";
                    }else{
                        $G2961_C59627 = "'".str_replace(' ', '',$_POST["G2961_C59627"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59627 = ".$G2961_C59627;
                    $LsqlI .= $separador." G2961_C59627";
                    $LsqlV .= $separador.$G2961_C59627;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59628"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59628 = '".$_POST["G2961_C59628"]."'";
                $LsqlI .= $separador."G2961_C59628";
                $LsqlV .= $separador."'".$_POST["G2961_C59628"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59629 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59629"])){    
                if($_POST["G2961_C59629"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59629"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59629 = "'".$_POST["G2961_C59629"]."'";
                    }else{
                        $G2961_C59629 = "'".str_replace(' ', '',$_POST["G2961_C59629"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59629 = ".$G2961_C59629;
                    $LsqlI .= $separador." G2961_C59629";
                    $LsqlV .= $separador.$G2961_C59629;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59630"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59630 = '".$_POST["G2961_C59630"]."'";
                $LsqlI .= $separador."G2961_C59630";
                $LsqlV .= $separador."'".$_POST["G2961_C59630"]."'";
                $validar = 1;
            }
             
 
            $G2961_C59631 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2961_C59631"])){    
                if($_POST["G2961_C59631"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2961_C59631"]);
                    if(count($tieneHora) > 1){
                        $G2961_C59631 = "'".$_POST["G2961_C59631"]."'";
                    }else{
                        $G2961_C59631 = "'".str_replace(' ', '',$_POST["G2961_C59631"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2961_C59631 = ".$G2961_C59631;
                    $LsqlI .= $separador." G2961_C59631";
                    $LsqlV .= $separador.$G2961_C59631;
                    $validar = 1;
                }
            }
  
            $G2961_C59632 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2961_C59632"])){
                if($_POST["G2961_C59632"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2961_C59632 = $_POST["G2961_C59632"];
                    $LsqlU .= $separador." G2961_C59632 = ".$G2961_C59632."";
                    $LsqlI .= $separador." G2961_C59632";
                    $LsqlV .= $separador.$G2961_C59632;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2961_C59633"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59633 = '".$_POST["G2961_C59633"]."'";
                $LsqlI .= $separador."G2961_C59633";
                $LsqlV .= $separador."'".$_POST["G2961_C59633"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59800"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59800 = '".$_POST["G2961_C59800"]."'";
                $LsqlI .= $separador."G2961_C59800";
                $LsqlV .= $separador."'".$_POST["G2961_C59800"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59801"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59801 = '".$_POST["G2961_C59801"]."'";
                $LsqlI .= $separador."G2961_C59801";
                $LsqlV .= $separador."'".$_POST["G2961_C59801"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C59802"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C59802 = '".$_POST["G2961_C59802"]."'";
                $LsqlI .= $separador."G2961_C59802";
                $LsqlV .= $separador."'".$_POST["G2961_C59802"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C60751"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C60751 = '".$_POST["G2961_C60751"]."'";
                $LsqlI .= $separador."G2961_C60751";
                $LsqlV .= $separador."'".$_POST["G2961_C60751"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61107"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61107 = '".$_POST["G2961_C61107"]."'";
                $LsqlI .= $separador."G2961_C61107";
                $LsqlV .= $separador."'".$_POST["G2961_C61107"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61109"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61109 = '".$_POST["G2961_C61109"]."'";
                $LsqlI .= $separador."G2961_C61109";
                $LsqlV .= $separador."'".$_POST["G2961_C61109"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61110"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61110 = '".$_POST["G2961_C61110"]."'";
                $LsqlI .= $separador."G2961_C61110";
                $LsqlV .= $separador."'".$_POST["G2961_C61110"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61111"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61111 = '".$_POST["G2961_C61111"]."'";
                $LsqlI .= $separador."G2961_C61111";
                $LsqlV .= $separador."'".$_POST["G2961_C61111"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61112"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61112 = '".$_POST["G2961_C61112"]."'";
                $LsqlI .= $separador."G2961_C61112";
                $LsqlV .= $separador."'".$_POST["G2961_C61112"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61113 = '".$_POST["G2961_C61113"]."'";
                $LsqlI .= $separador."G2961_C61113";
                $LsqlV .= $separador."'".$_POST["G2961_C61113"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C60753"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C60753 = '".$_POST["G2961_C60753"]."'";
                $LsqlI .= $separador."G2961_C60753";
                $LsqlV .= $separador."'".$_POST["G2961_C60753"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C60754"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C60754 = '".$_POST["G2961_C60754"]."'";
                $LsqlI .= $separador."G2961_C60754";
                $LsqlV .= $separador."'".$_POST["G2961_C60754"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2961_C61411"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_C61411 = '".$_POST["G2961_C61411"]."'";
                $LsqlI .= $separador."G2961_C61411";
                $LsqlV .= $separador."'".$_POST["G2961_C61411"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2961_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2961_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2961_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2961_Usuario , G2961_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2961_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2961 WHERE G2961_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2961 SET G2961_UltiGest__b =-14, G2961_GesMasImp_b =-14, G2961_TipoReintentoUG_b =0, G2961_TipoReintentoGMI_b =0, G2961_ClasificacionUG_b =3, G2961_ClasificacionGMI_b =3, G2961_EstadoUG_b =-14, G2961_EstadoGMI_b =-14, G2961_CantidadIntentos =0, G2961_CantidadIntentosGMI_b =0 WHERE G2961_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
