<?php 
    /*
        Document   : index
        Created on : 2021-07-26 20:02:13
        Author     : Jose David y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = Mjk2MQ==  
    */
    $url_crud =  "formularios/G2961/G2961_CRUD_web.php";
    include_once(__DIR__."/../../funciones.php");

    $arrPatron = json_decode(ObtenerPatron(4));    
    $patron = "";
    $patronSimple = "";

    if(!is_null($arrPatron)){

        if($arrPatron->patron_regexp){
            foreach($arrPatron->patron_regexp as $val){
                if(!is_null($val) || $val != ""){
                    $val = str_replace("'", "", $val);
                    if($patron == ""){
                        $patron .= $val;
                    }else{
                        $patron .= "|".$val;
                    }
                }
            }
        }

        if($arrPatron->patron){
            foreach ($arrPatron->patron_ejemplo as $val) {
                if(!is_null($val) || $val != ""){
                    if($patronSimple == ""){
                        $patronSimple .= $val;
                    }else{
                        $patronSimple .= " o ".$val;
                    }
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://www.google.com/recaptcha/api.js?render=6Lcc1dYUAAAAAIEO_HV7QhIhz36kA_7EfngIkhwG"></script>
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
        <!-- aqui va el css personalizado -->
        
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }

            form#formLogin h3{
                font-size: 14px;
            }
        </style>
    </head>
    <?php  
        if(isset($_GET['v'])){
            
        }else{
            echo '<body class="hold-transition" >';
        }
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <?php if(!isset($_GET['aceptaTerminos'])){ ?>
                    <div class="login-logo hed">
                        <?php if(!isset($_GET['v'])){ ?>
                            
                        <?php }else{ 
                                
                            }
                        ?>

                    </div><!-- /.login-logo -->
                    <?php if(isset($_GET['v'])){ 
                        
                    }else { ?>
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
					<?php } ?>
                        <form action="formularios/G2961/G2961_CRUD_web.php" method="post" id="formLogin" enctype="multipart/form-data">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2961_C59588" id="LblG2961_C59588">Primer Nombre</label>
								<input type="text" class="form-control input-sm " id="G2961_C59588" value=""  name="G2961_C59588"  placeholder="Primer Nombre"  >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2961_C59589" id="LblG2961_C59589">Segundo Nombre</label>
								<input type="text" class="form-control input-sm " id="G2961_C59589" value=""  name="G2961_C59589"  placeholder="Segundo Nombre"  >
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2961_C59591" id="LblG2961_C59591">Telefono Movil</label>
                                <input type="text" class="form-control input-sm Numerico " value=""  name="G2961_C59591" id="G2961_C59591" placeholder="Telefono Movil"  >
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
 
                            <!-- CAMPO TIPO EMAIL -->
                            <div class="form-group">
                                <label for="G2961_C59598" id="LblG2961_C59598">Correo Electronico</label>
                                <input type="email" class="form-control input-sm" id="G2961_C59598" value=""  name="G2961_C59598"  placeholder="Correo Electronico" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="pasoId" id="pasoId" value="<?php if (isset($_GET["paso"])) {
                                echo $_GET["paso"];
                            }else{ echo "0"; } ?>">
                            <input type="hidden" name="id" id="hidId" value='0'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="ORIGEN_DY_WF" id="ORIGEN_DY_WF" value='<?php if(isset($_GET['origen'])){ echo $_GET['origen']; }else{ echo "webForm"; }?>' >
                            
                            <?php if (isset($_GET['v'])){ ?>
                                    
                            <?php }else{ ?>
                                    <input type= "hidden" name="OPTIN_DY_WF" id="OPTIN_DY_WF" value="SIMPLE">
                            <?php }?>

                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                    <?php 
                        }else{ ?>
                            <?php if(isset($_GET['v'])){ 
                                
                            }else { ?>
                            <div class="login-box-body">
                                <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                            <?php } ?>
                    <?php 
                            if($_GET['aceptaTerminos'] == 'acepto'){
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);

                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2961 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){


                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2961 WHERE G2961_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                        $Lsql = "UPDATE ".$BaseDatos.".G2961 SET G2961_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'CONFIRMADO' WHERE G2961_ConsInte__b =".$ultimoResgistroInsertado;
                                        if ($mysqli->query($Lsql) === TRUE) {
                                            /* Acepto toca meterlo en la muestra  G626_M285*/

                                            

                                            echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";

                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";   
                                    }


                                }

                            }else{
                                /* NO CONFIRMAN */
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);
                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2961 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){
                                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2961 WHERE G2961_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $Lsql = "UPDATE ".$BaseDatos.".G2961 SET G2961_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'NO CONFIRMADO' WHERE G2961_ConsInte__b =".$ultimoResgistroInsertado;

                                        if ($mysqli->query($Lsql) === TRUE) {
                                            echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";
                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";            
                                    }

                                }
                            }

                                
                        } ?>


                </div><!-- /.login-box -->

            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2961/G2961_eventos.js"></script>
        <script type="text/javascript">

            $("#formLogin").submit(function(event) {
                // $('.error-pattern').remove();

                // let patrones = <?php if($patron != ''){echo $patron;}else{echo "0";} ?>;

                // if(patrones != "0"){

                //     let expreg = /patrones/;
                //     let valido = true;
                    
                //     $('.telefono').each(function(){
                //         if(!expreg.test($(this).val())){
                //             $(this).after('<span class="error-pattern" style="color: red;">Por favor envia un formato valido <?php echo $patronSimple ?></span>');
                //             valido = false;
                //         }
                //     });
                    
                //     if(valido){ return; }
                    
                //     event.preventDefault();
                // }

                event.preventDefault(); 
            grecaptcha.ready(function() {            
                grecaptcha.execute('6Lcc1dYUAAAAAIEO_HV7QhIhz36kA_7EfngIkhwG', {action: 'envio_captcha'}).then(function(token) {
                    $('#formLogin').prepend('<input type="hidden" name="tokens" value='+ token + '>');
                    $('#formLogin').prepend('<input type="hidden" name="action" value="envio_captcha">');
                    $('#formLogin').unbind('submit').submit();
                });
            });
            });
            $(function(){
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

                //Timepickers
                


                //Validaciones numeros Enteros
                

            $("#G2961_C59591").numeric();
            

                //Validaciones numeros Decimales
               


               /* Si tiene dependiendias */
               



               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){
                            <?php 
                            if(isset($_GET['v'])) {
                                
                            } 
                            ?>
                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>

        <?php 
        if(isset($_GET['v'])) {
            
        } 
        ?>
        
