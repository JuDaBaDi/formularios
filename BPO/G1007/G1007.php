
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1007/G1007_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1007_ConsInte__b as id, G1007_C14027 as camp1 , G1007_C14026 as camp2 FROM ".$BaseDatos.".G1007  WHERE G1007_Usuario = ".$idUsuario." ORDER BY G1007_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1007_ConsInte__b as id, G1007_C14027 as camp1 , G1007_C14026 as camp2 FROM ".$BaseDatos.".G1007  ORDER BY G1007_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1007_ConsInte__b as id, G1007_C14027 as camp1 , G1007_C14026 as camp2 FROM ".$BaseDatos.".G1007  ORDER BY G1007_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div id="1638" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1007_C14020" id="LblG1007_C14020">FECHA CREACION</label>
			            <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1007_C14020'])) {
	                		echo $_GET['G1007_C14020'];
	                	} ?>"  name="G1007_C14020" id="G1007_C14020" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14021" id="LblG1007_C14021">CODIGO AVISO</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14021" value="<?php if (isset($_GET['G1007_C14021'])) {
	                		echo $_GET['G1007_C14021'];
	                	} ?>"  name="G1007_C14021"  placeholder="CODIGO AVISO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14027" id="LblG1007_C14027">CLIENTE</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14027" value="<?php if (isset($_GET['G1007_C14027'])) {
	                		echo $_GET['G1007_C14027'];
	                	} ?>"  name="G1007_C14027"  placeholder="CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14026" id="LblG1007_C14026">CODIGO CLIENTE</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14026" value="<?php if (isset($_GET['G1007_C14026'])) {
	                		echo $_GET['G1007_C14026'];
	                	} ?>"  name="G1007_C14026"  placeholder="CODIGO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C27096" id="LblG1007_C27096">CON PRODUCTO</label>
			            <input type="text" class="form-control input-sm" id="G1007_C27096" value="<?php if (isset($_GET['G1007_C27096'])) {
	                		echo $_GET['G1007_C27096'];
	                	} ?>"  name="G1007_C27096"  placeholder="CON PRODUCTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C27097" id="LblG1007_C27097">DESCRIPCION</label>
			            <input type="text" class="form-control input-sm" id="G1007_C27097" value="<?php if (isset($_GET['G1007_C27097'])) {
	                		echo $_GET['G1007_C27097'];
	                	} ?>"  name="G1007_C27097"  placeholder="DESCRIPCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C27098" id="LblG1007_C27098">CON FOTO</label>
			            <input type="text" class="form-control input-sm" id="G1007_C27098" value="<?php if (isset($_GET['G1007_C27098'])) {
	                		echo $_GET['G1007_C27098'];
	                	} ?>"  name="G1007_C27098"  placeholder="CON FOTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14028" id="LblG1007_C14028">TELEFONO 1</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14028" value="<?php if (isset($_GET['G1007_C14028'])) {
	                		echo $_GET['G1007_C14028'];
	                	} ?>"  name="G1007_C14028"  placeholder="TELEFONO 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14029" id="LblG1007_C14029">TELEFONO 2</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14029" value="<?php if (isset($_GET['G1007_C14029'])) {
	                		echo $_GET['G1007_C14029'];
	                	} ?>"  name="G1007_C14029"  placeholder="TELEFONO 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14024" id="LblG1007_C14024">TIPO OFERTA</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14024" value="<?php if (isset($_GET['G1007_C14024'])) {
	                		echo $_GET['G1007_C14024'];
	                	} ?>"  name="G1007_C14024"  placeholder="TIPO OFERTA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14023" id="LblG1007_C14023">TIPO INMUEBLE</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14023" value="<?php if (isset($_GET['G1007_C14023'])) {
	                		echo $_GET['G1007_C14023'];
	                	} ?>"  name="G1007_C14023"  placeholder="TIPO INMUEBLE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14025" id="LblG1007_C14025">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14025" value="<?php if (isset($_GET['G1007_C14025'])) {
	                		echo $_GET['G1007_C14025'];
	                	} ?>"  name="G1007_C14025"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14030" id="LblG1007_C14030">EMAIL</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14030" value="<?php if (isset($_GET['G1007_C14030'])) {
	                		echo $_GET['G1007_C14030'];
	                	} ?>"  name="G1007_C14030"  placeholder="EMAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14031" id="LblG1007_C14031">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14031" value="<?php if (isset($_GET['G1007_C14031'])) {
	                		echo $_GET['G1007_C14031'];
	                	} ?>"  name="G1007_C14031"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C27099" id="LblG1007_C27099">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1007_C27099" value="<?php if (isset($_GET['G1007_C27099'])) {
	                		echo $_GET['G1007_C27099'];
	                	} ?>"  name="G1007_C27099"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14022" id="LblG1007_C14022">CONCEPTO(QUITAR)</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14022" value="<?php if (isset($_GET['G1007_C14022'])) {
	                		echo $_GET['G1007_C14022'];
	                	} ?>"  name="G1007_C14022"  placeholder="CONCEPTO(QUITAR)">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1007_C14032" id="LblG1007_C14032">OBSERVACIONES CONTACTO EFECTIVO(QUITAR)</label>
			            <textarea class="form-control input-sm" name="G1007_C14032" id="G1007_C14032"  value="<?php if (isset($_GET['G1007_C14032'])) {
	                		echo $_GET['G1007_C14032'];
	                	} ?>" placeholder="OBSERVACIONES CONTACTO EFECTIVO(QUITAR)"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>


        </div>


</div>

<div id="1639" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14033" id="LblG1007_C14033">ORIGEN_DY_WF</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14033" value="<?php if (isset($_GET['G1007_C14033'])) {
	                		echo $_GET['G1007_C14033'];
	                	} ?>" readonly name="G1007_C14033"  placeholder="ORIGEN_DY_WF">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1007_C14034" id="LblG1007_C14034">OPTIN_DY_WF</label>
			            <input type="text" class="form-control input-sm" id="G1007_C14034" value="<?php if (isset($_GET['G1007_C14034'])) {
	                		echo $_GET['G1007_C14034'];
	                	} ?>" readonly name="G1007_C14034"  placeholder="OPTIN_DY_WF">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_1916">
                TIPIFICACIONES
            </a>
        </h4>
    </div>
    <div id="s_1916" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1007_C15540" id="LblG1007_C15540">GESTION</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1007_C15540" id="G1007_C15540">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 747 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1007_C15541" id="LblG1007_C15541">TIPIFICACION 1</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1007_C15541" id="G1007_C15541">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 748 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1007_C15542" id="LblG1007_C15542">TIPIFICACION 2</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1007_C15542" id="G1007_C15542">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 743 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1007/G1007_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
	//JDBD - Esta seccion es solo para la interaccion con el formulario Padre
	/////////////////////////////////////////////////////////////////////////
	<?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
		<?php if($_GET["yourfather"] != "NULL"){ ?>
			$("#G1007_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
		<?php }else{ ?>
			if(document.getElementById("G1007_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
				$.ajax({
					url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
					type     : 'POST',
					data     : { q : <?php echo $_GET["idFather"]; ?> },
					success  : function(data){
						$("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
					}
				});
			}else{
				$("#G1007_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
			}
		<?php } ?>
	<?php } ?>
	/////////////////////////////////////////////////////////////////////////
	<?php if (!isset($_GET["view"])) {?>
		$("#add").click(function(){
							
		});
	<?php } ?>;	
	var meses = new Array(12);
	meses[0] = "01";
	meses[1] = "02";
	meses[2] = "03";
	meses[3] = "04";
	meses[4] = "05";
	meses[5] = "06";
	meses[6] = "07";
	meses[7] = "08";
	meses[8] = "09";
	meses[9] = "10";
	meses[10] = "11";
	meses[11] = "12";

	var d = new Date();
	var h = d.getHours();
	var horas = (h < 10) ? '0' + h : h;
	var dia = d.getDate();
	var dias = (dia < 10) ? '0' + dia : dia;
	var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
	$("#FechaInicio").val(fechaInicial);
            

	//Esta es por si lo llaman en modo formulario de edicion LigthBox
	<?php if(isset($_GET['registroId'])){ ?>
	$.ajax({
		url      : '<?=$url_crud;?>',
		type     : 'POST',
		data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
		dataType : 'json',
		success  : function(data){
			//recorrer datos y enviarlos al formulario
			$.each(data, function(i, item) {
                     
				$("#G1007_C14020").val(item.G1007_C14020); 
				$("#G1007_C14021").val(item.G1007_C14021); 
				$("#G1007_C14027").val(item.G1007_C14027); 
				$("#G1007_C14026").val(item.G1007_C14026); 
				$("#G1007_C27096").val(item.G1007_C27096); 
				$("#G1007_C27097").val(item.G1007_C27097); 
				$("#G1007_C27098").val(item.G1007_C27098); 
				$("#G1007_C14028").val(item.G1007_C14028); 
				$("#G1007_C14029").val(item.G1007_C14029); 
				$("#G1007_C14024").val(item.G1007_C14024); 
				$("#G1007_C14023").val(item.G1007_C14023); 
				$("#G1007_C14025").val(item.G1007_C14025); 
				$("#G1007_C14030").val(item.G1007_C14030); 
				$("#G1007_C14031").val(item.G1007_C14031); 
				$("#G1007_C27099").val(item.G1007_C27099); 
				$("#G1007_C14022").val(item.G1007_C14022); 
				$("#G1007_C14032").val(item.G1007_C14032); 
				$("#G1007_C14033").val(item.G1007_C14033); 
				$("#G1007_C14034").val(item.G1007_C14034); 
				$("#G1007_C15540").val(item.G1007_C15540).trigger("change");  
				$("#G1007_C15541").val(item.G1007_C15541).trigger("change");  
				$("#G1007_C15542").val(item.G1007_C15542).trigger("change"); 
				
				$("#h3mio").html(item.principal);

			});

			//Deshabilitar los campos 3

			//Habilitar todos los campos para edicion
			$('#FormularioDatos :input').each(function(){
				$(this).attr('disabled', true);
			});              

			//Habilidar los botones de operacion, add, editar, eliminar
			$("#add").attr('disabled', false);
			$("#edit").attr('disabled', false);
			$("#delete").attr('disabled', false);

			//Desahabiliatra los botones de salvar y seleccionar_registro
			$("#cancel").attr('disabled', true);
			$("#Save").attr('disabled', true);
		} 
	});

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1007_C15540").select2();

    $("#G1007_C15541").select2();

    $("#G1007_C15542").select2();
        //datepickers
        

        $("#G1007_C14020").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para GESTION 

    $("#G1007_C15540").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPIFICACION 1 

    $("#G1007_C15541").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPIFICACION 2 

    $("#G1007_C15542").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

    		if($("#G1007_C15540").prop("selectedIndex")==0 || $("#G1007_C15540").prop("selectedIndex") == -1){
    			alertify.error('GESTION debe ser diligenciado');
    			$("#G1007_C15540").closest(".form-group").addClass("has-error");
    			valido = 1;
    		}

    		if($("#G1007_C15541").prop("selectedIndex")==0 || $("#G1007_C15541").prop("selectedIndex") == -1){
    			alertify.error('TIPIFICACION 1 debe ser diligenciado');
    			$("#G1007_C15541").closest(".form-group").addClass("has-error");
    			valido = 1;
    		}
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        	var ID = <?=$_GET['registroId'];?>
			                        <?php }else{ ?>	
			                        	var ID = data
			                        <?php } ?>	
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : ID },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    
 
		                                    	$("#G1007_C14020").val(item.G1007_C14020);
 
		                                    	$("#G1007_C14021").val(item.G1007_C14021);
 
		                                    	$("#G1007_C14027").val(item.G1007_C14027);
 
		                                    	$("#G1007_C14026").val(item.G1007_C14026);
 
		                                    	$("#G1007_C27096").val(item.G1007_C27096);
 
		                                    	$("#G1007_C27097").val(item.G1007_C27097);
 
		                                    	$("#G1007_C27098").val(item.G1007_C27098);
 
		                                    	$("#G1007_C14028").val(item.G1007_C14028);
 
		                                    	$("#G1007_C14029").val(item.G1007_C14029);
 
		                                    	$("#G1007_C14024").val(item.G1007_C14024);
 
		                                    	$("#G1007_C14023").val(item.G1007_C14023);
 
		                                    	$("#G1007_C14025").val(item.G1007_C14025);
 
		                                    	$("#G1007_C14030").val(item.G1007_C14030);
 
		                                    	$("#G1007_C14031").val(item.G1007_C14031);
 
		                                    	$("#G1007_C27099").val(item.G1007_C27099);
 
		                                    	$("#G1007_C14022").val(item.G1007_C14022);
 
		                                    	$("#G1007_C14032").val(item.G1007_C14032);
 
		                                    	$("#G1007_C14033").val(item.G1007_C14033);
 
		                                    	$("#G1007_C14034").val(item.G1007_C14034);
 
                    $("#G1007_C15540").val(item.G1007_C15540).trigger("change"); 
 
                    $("#G1007_C15541").val(item.G1007_C15541).trigger("change"); 
 
                    $("#G1007_C15542").val(item.G1007_C15542).trigger("change"); 
		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos 2

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(ID);  

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			console.log(xt);
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
		                        	
				
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','FECHA CREACION','CODIGO AVISO','CLIENTE','CODIGO CLIENTE','CON PRODUCTO','DESCRIPCION','CON FOTO','TELEFONO 1','TELEFONO 2','TIPO OFERTA','TIPO INMUEBLE','CIUDAD','EMAIL','DEPARTAMENTO','CLASIFICACION','CONCEPTO(QUITAR)','OBSERVACIONES CONTACTO EFECTIVO(QUITAR)','ORIGEN_DY_WF','OPTIN_DY_WF','GESTION','TIPIFICACION 1','TIPIFICACION 2'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                {  
	                    name:'G1007_C14020', 
	                    index:'G1007_C14020', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1007_C14021', 
	                    index: 'G1007_C14021', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14027', 
	                    index: 'G1007_C14027', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14026', 
	                    index: 'G1007_C14026', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C27096', 
	                    index: 'G1007_C27096', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C27097', 
	                    index: 'G1007_C27097', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C27098', 
	                    index: 'G1007_C27098', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14028', 
	                    index: 'G1007_C14028', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14029', 
	                    index: 'G1007_C14029', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14024', 
	                    index: 'G1007_C14024', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14023', 
	                    index: 'G1007_C14023', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14025', 
	                    index: 'G1007_C14025', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14030', 
	                    index: 'G1007_C14030', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14031', 
	                    index: 'G1007_C14031', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C27099', 
	                    index: 'G1007_C27099', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14022', 
	                    index: 'G1007_C14022', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14032', 
	                    index:'G1007_C14032', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14033', 
	                    index: 'G1007_C14033', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C14034', 
	                    index: 'G1007_C14034', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1007_C15540', 
	                    index:'G1007_C15540', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1007_C15540'
	                    }
	                }

	                ,
	                { 
	                    name:'G1007_C15541', 
	                    index:'G1007_C15541', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1007_C15541'
	                    }
	                }

	                ,
	                { 
	                    name:'G1007_C15542', 
	                    index:'G1007_C15542', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1007_C15542'
	                    }
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1007_C14027',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1007_C14020").val(item.G1007_C14020);

                        $("#G1007_C14021").val(item.G1007_C14021);

                        $("#G1007_C14027").val(item.G1007_C14027);

                        $("#G1007_C14026").val(item.G1007_C14026);

                        $("#G1007_C27096").val(item.G1007_C27096);

                        $("#G1007_C27097").val(item.G1007_C27097);

                        $("#G1007_C27098").val(item.G1007_C27098);

                        $("#G1007_C14028").val(item.G1007_C14028);

                        $("#G1007_C14029").val(item.G1007_C14029);

                        $("#G1007_C14024").val(item.G1007_C14024);

                        $("#G1007_C14023").val(item.G1007_C14023);

                        $("#G1007_C14025").val(item.G1007_C14025);

                        $("#G1007_C14030").val(item.G1007_C14030);

                        $("#G1007_C14031").val(item.G1007_C14031);

                        $("#G1007_C27099").val(item.G1007_C27099);

                        $("#G1007_C14022").val(item.G1007_C14022);

                        $("#G1007_C14032").val(item.G1007_C14032);

                        $("#G1007_C14033").val(item.G1007_C14033);

                        $("#G1007_C14034").val(item.G1007_C14034);
 
                    $("#G1007_C15540").val(item.G1007_C15540).trigger("change"); 
 
                    $("#G1007_C15541").val(item.G1007_C15541).trigger("change"); 
 
                    $("#G1007_C15542").val(item.G1007_C15542).trigger("change"); 
			            
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
