<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   

    //Inserciones o actualizaciones
    if(isset($_POST["oper"])){
        $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G1059 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1059( G1059_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
 
        $G1059_C15258 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1059_C15258"])){    
            if($_POST["G1059_C15258"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1059_C15258 = "'".str_replace(' ', '',$_POST["G1059_C15258"])." 00:00:00'";
                $str_LsqlU .= $separador." G1059_C15258 = ".$G1059_C15258;
                $str_LsqlI .= $separador." G1059_C15258";
                $str_LsqlV .= $separador.$G1059_C15258;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1059_C15259"]) && $_POST["G1059_C15259"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15259 = '".$_POST["G1059_C15259"]."'";
            $str_LsqlI .= $separador."G1059_C15259";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15259"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15217"]) && $_POST["G1059_C15217"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15217 = '".$_POST["G1059_C15217"]."'";
            $str_LsqlI .= $separador."G1059_C15217";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15217"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15216"]) && $_POST["G1059_C15216"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15216 = '".$_POST["G1059_C15216"]."'";
            $str_LsqlI .= $separador."G1059_C15216";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15216"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15226"]) && $_POST["G1059_C15226"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15226 = '".$_POST["G1059_C15226"]."'";
            $str_LsqlI .= $separador."G1059_C15226";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15226"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15228"]) && $_POST["G1059_C15228"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15228 = '".$_POST["G1059_C15228"]."'";
            $str_LsqlI .= $separador."G1059_C15228";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15228"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15227"]) && $_POST["G1059_C15227"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15227 = '".$_POST["G1059_C15227"]."'";
            $str_LsqlI .= $separador."G1059_C15227";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15227"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15218"]) && $_POST["G1059_C15218"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15218 = '".$_POST["G1059_C15218"]."'";
            $str_LsqlI .= $separador."G1059_C15218";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15218"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15219"]) && $_POST["G1059_C15219"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15219 = '".$_POST["G1059_C15219"]."'";
            $str_LsqlI .= $separador."G1059_C15219";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15219"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15221"]) && $_POST["G1059_C15221"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15221 = '".$_POST["G1059_C15221"]."'";
            $str_LsqlI .= $separador."G1059_C15221";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15221"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15222"]) && $_POST["G1059_C15222"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15222 = '".$_POST["G1059_C15222"]."'";
            $str_LsqlI .= $separador."G1059_C15222";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15222"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15223"]) && $_POST["G1059_C15223"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15223 = '".$_POST["G1059_C15223"]."'";
            $str_LsqlI .= $separador."G1059_C15223";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15223"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15220"]) && $_POST["G1059_C15220"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15220 = '".$_POST["G1059_C15220"]."'";
            $str_LsqlI .= $separador."G1059_C15220";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15220"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15224"]) && $_POST["G1059_C15224"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15224 = '".$_POST["G1059_C15224"]."'";
            $str_LsqlI .= $separador."G1059_C15224";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15224"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15225"]) && $_POST["G1059_C15225"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15225 = '".$_POST["G1059_C15225"]."'";
            $str_LsqlI .= $separador."G1059_C15225";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15225"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15232"]) && $_POST["G1059_C15232"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15232 = '".$_POST["G1059_C15232"]."'";
            $str_LsqlI .= $separador."G1059_C15232";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15232"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15374"]) && $_POST["G1059_C15374"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15374 = '".$_POST["G1059_C15374"]."'";
            $str_LsqlI .= $separador."G1059_C15374";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15374"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15375"]) && $_POST["G1059_C15375"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15375 = '".$_POST["G1059_C15375"]."'";
            $str_LsqlI .= $separador."G1059_C15375";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15375"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C15376"]) && $_POST["G1059_C15376"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C15376 = '".$_POST["G1059_C15376"]."'";
            $str_LsqlI .= $separador."G1059_C15376";
            $str_LsqlV .= $separador."'".$_POST["G1059_C15376"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1059_C21548"]) && $_POST["G1059_C21548"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_C21548 = '".$_POST["G1059_C21548"]."'";
            $str_LsqlI .= $separador."G1059_C21548";
            $str_LsqlV .= $separador."'".$_POST["G1059_C21548"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1059_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1059_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1059_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1059 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    $str_LsqlU .= $separador."G1059_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G1059_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1059 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    $str_LsqlU .= $separador."G1059_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G1059_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;

                


                if(isset($_POST['v'])){
                    
                
                }else{
                    header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTA1OQ==&result=1');
                }
            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
