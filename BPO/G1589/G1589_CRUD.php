<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1589_ConsInte__b, G1589_FechaInsercion , G1589_Usuario ,  G1589_CodigoMiembro  , G1589_PoblacionOrigen , G1589_EstadoDiligenciamiento ,  G1589_IdLlamada , G1589_C38314 as principal ,G1589_C28489,G1589_C28490,G1589_C28491,G1589_C28492,G1589_C28493,G1589_C28494,G1589_C28495,G1589_C28496,G1589_C28497,G1589_C38295,G1589_C38297,G1589_C38298,G1589_C38299,G1589_C38300,G1589_C38301,G1589_C38302,G1589_C38303,G1589_C38304,G1589_C38305,G1589_C38306,G1589_C38308,G1589_C38307,G1589_C38309,G1589_C38310,G1589_C38311,G1589_C38313,G1589_C38525,G1589_C38526,G1589_C38527,G1589_C38314,G1589_C38315,G1589_C38316,G1589_C38317,G1589_C38318,G1589_C38319,G1589_C38320,G1589_C38321,G1589_C38322,G1589_C38323,G1589_C38324,G1589_C38325,G1589_C38326,G1589_C38327,G1589_C38328,G1589_C38329,G1589_C38330,G1589_C38331,G1589_C38332,G1589_C38333,G1589_C38334,G1589_C38335,G1589_C38336,G1589_C38337 FROM '.$BaseDatos.'.G1589 WHERE G1589_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1589_C28489'] = $key->G1589_C28489;

                $datos[$i]['G1589_C28490'] = $key->G1589_C28490;

                $datos[$i]['G1589_C28491'] = explode(' ', $key->G1589_C28491)[0];
  
                $hora = '';
                if(!is_null($key->G1589_C28492)){
                    $hora = explode(' ', $key->G1589_C28492)[1];
                }

                $datos[$i]['G1589_C28492'] = $hora;

                $datos[$i]['G1589_C28493'] = $key->G1589_C28493;

                $datos[$i]['G1589_C28494'] = $key->G1589_C28494;

                $datos[$i]['G1589_C28495'] = $key->G1589_C28495;

                $datos[$i]['G1589_C28496'] = $key->G1589_C28496;

                $datos[$i]['G1589_C28497'] = $key->G1589_C28497;

                $datos[$i]['G1589_C38295'] = $key->G1589_C38295;

                $datos[$i]['G1589_C38297'] = $key->G1589_C38297;

                $datos[$i]['G1589_C38298'] = $key->G1589_C38298;

                $datos[$i]['G1589_C38299'] = $key->G1589_C38299;

                $datos[$i]['G1589_C38300'] = $key->G1589_C38300;

                $datos[$i]['G1589_C38301'] = $key->G1589_C38301;

                $datos[$i]['G1589_C38302'] = $key->G1589_C38302;

                $datos[$i]['G1589_C38303'] = $key->G1589_C38303;

                $datos[$i]['G1589_C38304'] = $key->G1589_C38304;

                $datos[$i]['G1589_C38305'] = $key->G1589_C38305;

                $datos[$i]['G1589_C38306'] = $key->G1589_C38306;

                $datos[$i]['G1589_C38308'] = $key->G1589_C38308;

                $datos[$i]['G1589_C38307'] = $key->G1589_C38307;

                $datos[$i]['G1589_C38309'] = $key->G1589_C38309;

                $datos[$i]['G1589_C38310'] = $key->G1589_C38310;

                $datos[$i]['G1589_C38311'] = $key->G1589_C38311;

                $datos[$i]['G1589_C38313'] = $key->G1589_C38313;

                $datos[$i]['G1589_C38525'] = $key->G1589_C38525;

                $datos[$i]['G1589_C38526'] = $key->G1589_C38526;

                $datos[$i]['G1589_C38527'] = $key->G1589_C38527;

                $datos[$i]['G1589_C38314'] = $key->G1589_C38314;

                $datos[$i]['G1589_C38315'] = $key->G1589_C38315;

                $datos[$i]['G1589_C38316'] = $key->G1589_C38316;

                $datos[$i]['G1589_C38317'] = $key->G1589_C38317;

                $datos[$i]['G1589_C38318'] = explode(' ', $key->G1589_C38318)[0];

                $datos[$i]['G1589_C38319'] = $key->G1589_C38319;

                $datos[$i]['G1589_C38320'] = $key->G1589_C38320;

                $datos[$i]['G1589_C38321'] = $key->G1589_C38321;

                $datos[$i]['G1589_C38322'] = $key->G1589_C38322;

                $datos[$i]['G1589_C38323'] = $key->G1589_C38323;

                $datos[$i]['G1589_C38324'] = $key->G1589_C38324;

                $datos[$i]['G1589_C38325'] = $key->G1589_C38325;

                $datos[$i]['G1589_C38326'] = $key->G1589_C38326;

                $datos[$i]['G1589_C38327'] = $key->G1589_C38327;

                $datos[$i]['G1589_C38328'] = $key->G1589_C38328;

                $datos[$i]['G1589_C38329'] = $key->G1589_C38329;

                $datos[$i]['G1589_C38330'] = $key->G1589_C38330;

                $datos[$i]['G1589_C38331'] = $key->G1589_C38331;

                $datos[$i]['G1589_C38332'] = $key->G1589_C38332;

                $datos[$i]['G1589_C38333'] = $key->G1589_C38333;

                $datos[$i]['G1589_C38334'] = $key->G1589_C38334;

                $datos[$i]['G1589_C38335'] = $key->G1589_C38335;

                $datos[$i]['G1589_C38336'] = $key->G1589_C38336;

                $datos[$i]['G1589_C38337'] = $key->G1589_C38337;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1589";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1589_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1589_ConsInte__b as id,  G1589_C38314 as camp1 , G1589_C38315 as camp2 
                     FROM ".$BaseDatos.".G1589  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1589_ConsInte__b as id,  G1589_C38314 as camp1 , G1589_C38315 as camp2  
                    FROM ".$BaseDatos.".G1589  JOIN ".$BaseDatos.".G1589_M".$_POST['muestra']." ON G1589_ConsInte__b = G1589_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1589_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1589_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1589_C38314 LIKE '%".$B."%' OR G1589_C38315 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1589_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G1589_C38305'])){
                                $Ysql = "SELECT G2043_ConsInte__b as id, G2043_C37408 as text FROM ".$BaseDatos.".G2043 WHERE G2043_C37408 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1589_C38305"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2043 WHERE G2043_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1589_C38305"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G1589_C38321'])){
                                $Ysql = "SELECT G2005_ConsInte__b as id, G2005_C36697 as text FROM ".$BaseDatos.".G2005 WHERE G2005_C36697 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1589_C38321"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2005 WHERE G2005_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1589_C38321"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G1589_C38325'])){
                                $Ysql = "SELECT G2043_ConsInte__b as id, G2043_C37408 as text FROM ".$BaseDatos.".G2043 WHERE G2043_C37408 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1589_C38325"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2043 WHERE G2043_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1589_C38325"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1589");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1589_ConsInte__b, G1589_FechaInsercion , G1589_Usuario ,  G1589_CodigoMiembro  , G1589_PoblacionOrigen , G1589_EstadoDiligenciamiento ,  G1589_IdLlamada , G1589_C38314 as principal , a.LISOPC_Nombre____b as G1589_C28489, b.LISOPC_Nombre____b as G1589_C28490,G1589_C28491,G1589_C28492,G1589_C28493,G1589_C28494,G1589_C28495,G1589_C28496,G1589_C28497,G1589_C38295,G1589_C38297,G1589_C38298,G1589_C38299,G1589_C38300,G1589_C38301,G1589_C38302,G1589_C38303, c.LISOPC_Nombre____b as G1589_C38304, G2043_C37408,G1589_C38306, d.LISOPC_Nombre____b as G1589_C38308, e.LISOPC_Nombre____b as G1589_C38307, f.LISOPC_Nombre____b as G1589_C38309,G1589_C38310,G1589_C38311, g.LISOPC_Nombre____b as G1589_C38313, h.LISOPC_Nombre____b as G1589_C38525, i.LISOPC_Nombre____b as G1589_C38526, j.LISOPC_Nombre____b as G1589_C38527,G1589_C38314,G1589_C38315,G1589_C38316, k.LISOPC_Nombre____b as G1589_C38317,G1589_C38318,G1589_C38319, l.LISOPC_Nombre____b as G1589_C38320, G2005_C36697,G1589_C38322, m.LISOPC_Nombre____b as G1589_C38323,G1589_C38324, G2043_C37408,G1589_C38326,G1589_C38327,G1589_C38328,G1589_C38329,G1589_C38330,G1589_C38331,G1589_C38332, n.LISOPC_Nombre____b as G1589_C38333,G1589_C38334, o.LISOPC_Nombre____b as G1589_C38335,G1589_C38336,G1589_C38337 FROM '.$BaseDatos.'.G1589 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1589_C28489 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1589_C28490 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1589_C38304 LEFT JOIN '.$BaseDatos.'.G2043 ON G2043_ConsInte__b  =  G1589_C38305 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1589_C38308 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1589_C38307 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1589_C38309 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1589_C38313 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1589_C38525 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1589_C38526 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1589_C38527 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1589_C38317 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G1589_C38320 LEFT JOIN '.$BaseDatos.'.G2005 ON G2005_ConsInte__b  =  G1589_C38321 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G1589_C38323 LEFT JOIN '.$BaseDatos.'.G2043 ON G2043_ConsInte__b  =  G1589_C38325 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G1589_C38333 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G1589_C38335';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1589 WHERE G1589_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1589";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1589_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1589_ConsInte__b as id,  G1589_C38314 as camp1 , G1589_C38315 as camp2  FROM '.$BaseDatos.'.G1589 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1589_ConsInte__b as id,  G1589_C38314 as camp1 , G1589_C38315 as camp2  
                    FROM ".$BaseDatos.".G1589  JOIN ".$BaseDatos.".G1589_M".$_POST['muestra']." ON G1589_ConsInte__b = G1589_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1589_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1589_C38314 LIKE "%'.$B.'%" OR G1589_C38315 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1589_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1589 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1589(";
            $LsqlV = " VALUES ("; 
 
            $G1589_C28489 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1589_C28489 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1589_C28489 = ".$G1589_C28489;
                    $LsqlI .= $separador." G1589_C28489";
                    $LsqlV .= $separador.$G1589_C28489;
                    $validar = 1;

                    
                }
            }
 
            $G1589_C28490 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1589_C28490 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1589_C28490 = ".$G1589_C28490;
                    $LsqlI .= $separador." G1589_C28490";
                    $LsqlV .= $separador.$G1589_C28490;
                    $validar = 1;
                }
            }
 
            $G1589_C28491 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1589_C28491 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1589_C28491 = ".$G1589_C28491;
                    $LsqlI .= $separador." G1589_C28491";
                    $LsqlV .= $separador.$G1589_C28491;
                    $validar = 1;
                }
            }
 
            $G1589_C28492 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1589_C28492 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1589_C28492 = ".$G1589_C28492;
                    $LsqlI .= $separador." G1589_C28492";
                    $LsqlV .= $separador.$G1589_C28492;
                    $validar = 1;
                }
            }
 
            $G1589_C28493 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1589_C28493 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1589_C28493 = ".$G1589_C28493;
                    $LsqlI .= $separador." G1589_C28493";
                    $LsqlV .= $separador.$G1589_C28493;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1589_C28494"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C28494 = '".$_POST["G1589_C28494"]."'";
                $LsqlI .= $separador."G1589_C28494";
                $LsqlV .= $separador."'".$_POST["G1589_C28494"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C28495"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C28495 = '".$_POST["G1589_C28495"]."'";
                $LsqlI .= $separador."G1589_C28495";
                $LsqlV .= $separador."'".$_POST["G1589_C28495"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C28496"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C28496 = '".$_POST["G1589_C28496"]."'";
                $LsqlI .= $separador."G1589_C28496";
                $LsqlV .= $separador."'".$_POST["G1589_C28496"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C28497"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C28497 = '".$_POST["G1589_C28497"]."'";
                $LsqlI .= $separador."G1589_C28497";
                $LsqlV .= $separador."'".$_POST["G1589_C28497"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38295 = '".$_POST["G1589_C38295"]."'";
                $LsqlI .= $separador."G1589_C38295";
                $LsqlV .= $separador."'".$_POST["G1589_C38295"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38296 = '".$_POST["G1589_C38296"]."'";
                $LsqlI .= $separador."G1589_C38296";
                $LsqlV .= $separador."'".$_POST["G1589_C38296"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38297"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38297 = '".$_POST["G1589_C38297"]."'";
                $LsqlI .= $separador."G1589_C38297";
                $LsqlV .= $separador."'".$_POST["G1589_C38297"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38298"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38298 = '".$_POST["G1589_C38298"]."'";
                $LsqlI .= $separador."G1589_C38298";
                $LsqlV .= $separador."'".$_POST["G1589_C38298"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38299"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38299 = '".$_POST["G1589_C38299"]."'";
                $LsqlI .= $separador."G1589_C38299";
                $LsqlV .= $separador."'".$_POST["G1589_C38299"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38300"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38300 = '".$_POST["G1589_C38300"]."'";
                $LsqlI .= $separador."G1589_C38300";
                $LsqlV .= $separador."'".$_POST["G1589_C38300"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38301"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38301 = '".$_POST["G1589_C38301"]."'";
                $LsqlI .= $separador."G1589_C38301";
                $LsqlV .= $separador."'".$_POST["G1589_C38301"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38302"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38302 = '".$_POST["G1589_C38302"]."'";
                $LsqlI .= $separador."G1589_C38302";
                $LsqlV .= $separador."'".$_POST["G1589_C38302"]."'";
                $validar = 1;
            }
             
  
            $G1589_C38303 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1589_C38303"])){
                if($_POST["G1589_C38303"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1589_C38303 = $_POST["G1589_C38303"];
                    $LsqlU .= $separador." G1589_C38303 = ".$G1589_C38303."";
                    $LsqlI .= $separador." G1589_C38303";
                    $LsqlV .= $separador.$G1589_C38303;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1589_C38304"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38304 = '".$_POST["G1589_C38304"]."'";
                $LsqlI .= $separador."G1589_C38304";
                $LsqlV .= $separador."'".$_POST["G1589_C38304"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38305"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38305 = '".$_POST["G1589_C38305"]."'";
                $LsqlI .= $separador."G1589_C38305";
                $LsqlV .= $separador."'".$_POST["G1589_C38305"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38306"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38306 = '".$_POST["G1589_C38306"]."'";
                $LsqlI .= $separador."G1589_C38306";
                $LsqlV .= $separador."'".$_POST["G1589_C38306"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38308"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38308 = '".$_POST["G1589_C38308"]."'";
                $LsqlI .= $separador."G1589_C38308";
                $LsqlV .= $separador."'".$_POST["G1589_C38308"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38307"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38307 = '".$_POST["G1589_C38307"]."'";
                $LsqlI .= $separador."G1589_C38307";
                $LsqlV .= $separador."'".$_POST["G1589_C38307"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38309"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38309 = '".$_POST["G1589_C38309"]."'";
                $LsqlI .= $separador."G1589_C38309";
                $LsqlV .= $separador."'".$_POST["G1589_C38309"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38310"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38310 = '".$_POST["G1589_C38310"]."'";
                $LsqlI .= $separador."G1589_C38310";
                $LsqlV .= $separador."'".$_POST["G1589_C38310"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38311"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38311 = '".$_POST["G1589_C38311"]."'";
                $LsqlI .= $separador."G1589_C38311";
                $LsqlV .= $separador."'".$_POST["G1589_C38311"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38313 = '".$_POST["G1589_C38313"]."'";
                $LsqlI .= $separador."G1589_C38313";
                $LsqlV .= $separador."'".$_POST["G1589_C38313"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38525"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38525 = '".$_POST["G1589_C38525"]."'";
                $LsqlI .= $separador."G1589_C38525";
                $LsqlV .= $separador."'".$_POST["G1589_C38525"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38526"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38526 = '".$_POST["G1589_C38526"]."'";
                $LsqlI .= $separador."G1589_C38526";
                $LsqlV .= $separador."'".$_POST["G1589_C38526"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38527"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38527 = '".$_POST["G1589_C38527"]."'";
                $LsqlI .= $separador."G1589_C38527";
                $LsqlV .= $separador."'".$_POST["G1589_C38527"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38314 = '".$_POST["G1589_C38314"]."'";
                $LsqlI .= $separador."G1589_C38314";
                $LsqlV .= $separador."'".$_POST["G1589_C38314"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38315 = '".$_POST["G1589_C38315"]."'";
                $LsqlI .= $separador."G1589_C38315";
                $LsqlV .= $separador."'".$_POST["G1589_C38315"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38316 = '".$_POST["G1589_C38316"]."'";
                $LsqlI .= $separador."G1589_C38316";
                $LsqlV .= $separador."'".$_POST["G1589_C38316"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38317 = '".$_POST["G1589_C38317"]."'";
                $LsqlI .= $separador."G1589_C38317";
                $LsqlV .= $separador."'".$_POST["G1589_C38317"]."'";
                $validar = 1;
            }
             
 
            $G1589_C38318 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1589_C38318"])){    
                if($_POST["G1589_C38318"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1589_C38318"]);
                    if(count($tieneHora) > 1){
                        $G1589_C38318 = "'".$_POST["G1589_C38318"]."'";
                    }else{
                        $G1589_C38318 = "'".str_replace(' ', '',$_POST["G1589_C38318"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1589_C38318 = ".$G1589_C38318;
                    $LsqlI .= $separador." G1589_C38318";
                    $LsqlV .= $separador.$G1589_C38318;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1589_C38319"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38319 = '".$_POST["G1589_C38319"]."'";
                $LsqlI .= $separador."G1589_C38319";
                $LsqlV .= $separador."'".$_POST["G1589_C38319"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38320"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38320 = '".$_POST["G1589_C38320"]."'";
                $LsqlI .= $separador."G1589_C38320";
                $LsqlV .= $separador."'".$_POST["G1589_C38320"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38321"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38321 = '".$_POST["G1589_C38321"]."'";
                $LsqlI .= $separador."G1589_C38321";
                $LsqlV .= $separador."'".$_POST["G1589_C38321"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38322 = '".$_POST["G1589_C38322"]."'";
                $LsqlI .= $separador."G1589_C38322";
                $LsqlV .= $separador."'".$_POST["G1589_C38322"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38323"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38323 = '".$_POST["G1589_C38323"]."'";
                $LsqlI .= $separador."G1589_C38323";
                $LsqlV .= $separador."'".$_POST["G1589_C38323"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38324"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38324 = '".$_POST["G1589_C38324"]."'";
                $LsqlI .= $separador."G1589_C38324";
                $LsqlV .= $separador."'".$_POST["G1589_C38324"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38325"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38325 = '".$_POST["G1589_C38325"]."'";
                $LsqlI .= $separador."G1589_C38325";
                $LsqlV .= $separador."'".$_POST["G1589_C38325"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38326"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38326 = '".$_POST["G1589_C38326"]."'";
                $LsqlI .= $separador."G1589_C38326";
                $LsqlV .= $separador."'".$_POST["G1589_C38326"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38327"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38327 = '".$_POST["G1589_C38327"]."'";
                $LsqlI .= $separador."G1589_C38327";
                $LsqlV .= $separador."'".$_POST["G1589_C38327"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38328"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38328 = '".$_POST["G1589_C38328"]."'";
                $LsqlI .= $separador."G1589_C38328";
                $LsqlV .= $separador."'".$_POST["G1589_C38328"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38329"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38329 = '".$_POST["G1589_C38329"]."'";
                $LsqlI .= $separador."G1589_C38329";
                $LsqlV .= $separador."'".$_POST["G1589_C38329"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38330"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38330 = '".$_POST["G1589_C38330"]."'";
                $LsqlI .= $separador."G1589_C38330";
                $LsqlV .= $separador."'".$_POST["G1589_C38330"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38331"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38331 = '".$_POST["G1589_C38331"]."'";
                $LsqlI .= $separador."G1589_C38331";
                $LsqlV .= $separador."'".$_POST["G1589_C38331"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38332"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38332 = '".$_POST["G1589_C38332"]."'";
                $LsqlI .= $separador."G1589_C38332";
                $LsqlV .= $separador."'".$_POST["G1589_C38332"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38333"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38333 = '".$_POST["G1589_C38333"]."'";
                $LsqlI .= $separador."G1589_C38333";
                $LsqlV .= $separador."'".$_POST["G1589_C38333"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38334"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38334 = '".$_POST["G1589_C38334"]."'";
                $LsqlI .= $separador."G1589_C38334";
                $LsqlV .= $separador."'".$_POST["G1589_C38334"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38335"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38335 = '".$_POST["G1589_C38335"]."'";
                $LsqlI .= $separador."G1589_C38335";
                $LsqlV .= $separador."'".$_POST["G1589_C38335"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38336"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38336 = '".$_POST["G1589_C38336"]."'";
                $LsqlI .= $separador."G1589_C38336";
                $LsqlV .= $separador."'".$_POST["G1589_C38336"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38337"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38337 = '".$_POST["G1589_C38337"]."'";
                $LsqlI .= $separador."G1589_C38337";
                $LsqlV .= $separador."'".$_POST["G1589_C38337"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38338"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38338 = '".$_POST["G1589_C38338"]."'";
                $LsqlI .= $separador."G1589_C38338";
                $LsqlV .= $separador."'".$_POST["G1589_C38338"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1589_C38339"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_C38339 = '".$_POST["G1589_C38339"]."'";
                $LsqlI .= $separador."G1589_C38339";
                $LsqlV .= $separador."'".$_POST["G1589_C38339"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1589_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1589_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1589_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1589_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1589_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1589_Usuario , G1589_FechaInsercion, G1589_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1589_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1589 WHERE G1589_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1651_ConsInte__b, G1651_C29727, G1651_C29724, G1651_C29725,G1651_C29728,G1651_C29726,G1651_C29731,G1651_C29732 FROM ".$BaseDatos.".G1651  ";

        $SQL .= " WHERE G1651_C29727 = '".$numero."'"; 

        $SQL .= " ORDER BY G1651_C29723";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1651_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1651_ConsInte__b)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29727)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29724)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29725)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29728)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29726)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29731)."</cell>"; 
            echo "<cell>". ($fila->G1651_C29732)."</cell>"; 
            
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1716_ConsInte__b, G1716_C31050, G1716_C31051, G1716_C31052, G1716_C31053, G1716_C31054, G1716_C31055, G1716_C38340, G1716_C38341 FROM ".$BaseDatos.".G1716  ";

        $SQL .= " WHERE G1716_C31050 = '".$numero."'"; 

        $SQL .= " ORDER BY G1716_C31050";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1716_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1716_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G1716_C31050."</cell>"; 

                echo "<cell>". ($fila->G1716_C31051)."</cell>";

                echo "<cell>". ($fila->G1716_C31052)."</cell>";

                echo "<cell>". ($fila->G1716_C31053)."</cell>";

                echo "<cell>". ($fila->G1716_C31054)."</cell>";

                if($fila->G1716_C31055 != ''){
                    echo "<cell>". explode(' ', $fila->G1716_C31055)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G1716_C38340)."</cell>";

                echo "<cell>". ($fila->G1716_C38341)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_2"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1716_ConsInte__b, G1716_C31050, G1716_C31051, G1716_C31052, G1716_C31053, G1716_C31054, G1716_C31055, G1716_C38340, G1716_C38341 FROM ".$BaseDatos.".G1716  ";

        $SQL .= " WHERE G1716_C31050 = '".$numero."'"; 

        $SQL .= " ORDER BY G1716_C31050";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1716_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1716_ConsInte__b)."</cell>"; 
            

                echo "<cell>". $fila->G1716_C31050."</cell>"; 

                echo "<cell>". ($fila->G1716_C31051)."</cell>";

                echo "<cell>". ($fila->G1716_C31052)."</cell>";

                echo "<cell>". ($fila->G1716_C31053)."</cell>";

                echo "<cell>". ($fila->G1716_C31054)."</cell>";

                if($fila->G1716_C31055 != ''){
                    echo "<cell>". explode(' ', $fila->G1716_C31055)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G1716_C38340)."</cell>";

                echo "<cell>". ($fila->G1716_C38341)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1651 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1651(";
            $LsqlV = " VALUES ("; 

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1651_C29727 = $numero;
                    $LsqlU .= ", G1651_C29727 = ".$G1651_C29727."";
                    $LsqlI .= ", G1651_C29727";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1651_Usuario ,  G1651_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1651_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1651 WHERE  G1651_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1716 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1716(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G1716_C31051"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31051 = '".$_POST["G1716_C31051"]."'";
                    $LsqlI .= $separador."G1716_C31051";
                    $LsqlV .= $separador."'".$_POST["G1716_C31051"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C31052"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31052 = '".$_POST["G1716_C31052"]."'";
                    $LsqlI .= $separador."G1716_C31052";
                    $LsqlV .= $separador."'".$_POST["G1716_C31052"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C31053"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31053 = '".$_POST["G1716_C31053"]."'";
                    $LsqlI .= $separador."G1716_C31053";
                    $LsqlV .= $separador."'".$_POST["G1716_C31053"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C31054"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31054 = '".$_POST["G1716_C31054"]."'";
                    $LsqlI .= $separador."G1716_C31054";
                    $LsqlV .= $separador."'".$_POST["G1716_C31054"]."'";
                    $validar = 1;
                }

                                                                               

                $G1716_C31055 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G1716_C31055"])){    
                    if($_POST["G1716_C31055"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1716_C31055 = "'".str_replace(' ', '',$_POST["G1716_C31055"])." 00:00:00'";
                        $LsqlU .= $separador." G1716_C31055 = ".$G1716_C31055;
                        $LsqlI .= $separador." G1716_C31055";
                        $LsqlV .= $separador.$G1716_C31055;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G1716_C38340"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C38340 = '".$_POST["G1716_C38340"]."'";
                    $LsqlI .= $separador."G1716_C38340";
                    $LsqlV .= $separador."'".$_POST["G1716_C38340"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C38341"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C38341 = '".$_POST["G1716_C38341"]."'";
                    $LsqlI .= $separador."G1716_C38341";
                    $LsqlV .= $separador."'".$_POST["G1716_C38341"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1716_C31050 = $numero;
                    $LsqlU .= ", G1716_C31050 = ".$G1716_C31050."";
                    $LsqlI .= ", G1716_C31050";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1716_Usuario ,  G1716_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1716_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1716 WHERE  G1716_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_2"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1716 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1716(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G1716_C31051"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31051 = '".$_POST["G1716_C31051"]."'";
                    $LsqlI .= $separador."G1716_C31051";
                    $LsqlV .= $separador."'".$_POST["G1716_C31051"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C31052"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31052 = '".$_POST["G1716_C31052"]."'";
                    $LsqlI .= $separador."G1716_C31052";
                    $LsqlV .= $separador."'".$_POST["G1716_C31052"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C31053"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31053 = '".$_POST["G1716_C31053"]."'";
                    $LsqlI .= $separador."G1716_C31053";
                    $LsqlV .= $separador."'".$_POST["G1716_C31053"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C31054"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C31054 = '".$_POST["G1716_C31054"]."'";
                    $LsqlI .= $separador."G1716_C31054";
                    $LsqlV .= $separador."'".$_POST["G1716_C31054"]."'";
                    $validar = 1;
                }

                                                                               

                $G1716_C31055 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G1716_C31055"])){    
                    if($_POST["G1716_C31055"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1716_C31055 = "'".str_replace(' ', '',$_POST["G1716_C31055"])." 00:00:00'";
                        $LsqlU .= $separador." G1716_C31055 = ".$G1716_C31055;
                        $LsqlI .= $separador." G1716_C31055";
                        $LsqlV .= $separador.$G1716_C31055;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G1716_C38340"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C38340 = '".$_POST["G1716_C38340"]."'";
                    $LsqlI .= $separador."G1716_C38340";
                    $LsqlV .= $separador."'".$_POST["G1716_C38340"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1716_C38341"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1716_C38341 = '".$_POST["G1716_C38341"]."'";
                    $LsqlI .= $separador."G1716_C38341";
                    $LsqlV .= $separador."'".$_POST["G1716_C38341"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1716_C31050 = $numero;
                    $LsqlU .= ", G1716_C31050 = ".$G1716_C31050."";
                    $LsqlI .= ", G1716_C31050";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1716_Usuario ,  G1716_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1716_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1716 WHERE  G1716_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
