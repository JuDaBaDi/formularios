<?php 
    /*
        Document   : index
        Created on : 2020-10-23 16:10:16
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MTU4OQ==  
    */
    $url_crud =  "formularios/G1589/G1589_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G1589/G1589_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38295" id="LblG1589_C38295">CODINTERNO</label>
								<input type="text" class="form-control input-sm" id="G1589_C38295" value=""  name="G1589_C38295"  placeholder="CODINTERNO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Buen día, mi nombre es _________, soy ejecutivo de actualización y vinculación de audiencia de la revista LA BARRA. ¿Como está usted el día de hoy?</h3>
                            <!-- FIN LIBRETO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38297" id="LblG1589_C38297">Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía </label>
								<input type="text" class="form-control input-sm" id="G1589_C38297" value=""  name="G1589_C38297"  placeholder="Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38298" id="LblG1589_C38298">no tendría ningún costo. ¿Hablo con el Sr./Sra.?</label>
								<input type="text" class="form-control input-sm" id="G1589_C38298" value=""  name="G1589_C38298"  placeholder="no tendría ningún costo. ¿Hablo con el Sr./Sra.?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1589_C38299" id="LblG1589_C38299">Si si habla con el Sr./Sra / si no habla con el Sr./Sra</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38299" id="G1589_C38299">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2125 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1589_C38299" id="respuesta_LblG1589_C38299">Respuesta</label>
                        <textarea id="respuesta_G1589_C38299" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1589_C38300" id="LblG1589_C38300">Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo?	</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38300" id="G1589_C38300">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1527 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1589_C38300" id="respuesta_LblG1589_C38300">Respuesta</label>
                        <textarea id="respuesta_G1589_C38300" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1589_C38301" id="LblG1589_C38301">1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38301" id="G1589_C38301">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2127 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1589_C38301" id="respuesta_LblG1589_C38301">Respuesta</label>
                        <textarea id="respuesta_G1589_C38301" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38302" id="LblG1589_C38302">NOMBRE COMERCIAL DE LA EMPRESA (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38302" value=""  name="G1589_C38302"  placeholder="NOMBRE COMERCIAL DE LA EMPRESA (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1589_C38303" id="LblG1589_C38303">NIT</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G1589_C38303" id="G1589_C38303" placeholder="NIT">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38304" id="LblG1589_C38304">NÚMERO DE EMPLEADOS</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38304" id="G1589_C38304">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1544 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <?php 
                            $str_Lsql = "SELECT  G2043_ConsInte__b as id , G2043_C37408, G2043_C37407 FROM ".$BaseDatos_systema.".G2043";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1589_C38305" id="LblG1589_C38305">D.E CIUDAD (*)</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1589_C38305" id="G1589_C38305">
                                    <option>MUNICIPIO | DEPARTAMENTO</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0|G1589_C38306'>".($obj->G2043_C37408)." | ".($obj->G2043_C37407)."</option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38306" id="LblG1589_C38306">D.E DEPARTAMENTO (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38306" value=""  name="G1589_C38306"  placeholder="D.E DEPARTAMENTO (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38308" id="LblG1589_C38308">ACTIVIDAD(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38308" id="G1589_C38308">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2233 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38307" id="LblG1589_C38307">PERFIL(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38307" id="G1589_C38307">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2232 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38309" id="LblG1589_C38309">¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38309" id="G1589_C38309">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2234 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38310" id="LblG1589_C38310">CODIGO DE EMPRESA</label>
								<input type="text" class="form-control input-sm" id="G1589_C38310" value=""  name="G1589_C38310"  placeholder="CODIGO DE EMPRESA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38311" id="LblG1589_C38311">PAGINA_WEB</label>
								<input type="text" class="form-control input-sm" id="G1589_C38311" value=""  name="G1589_C38311"  placeholder="PAGINA_WEB">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38313" id="LblG1589_C38313">¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38313" id="G1589_C38313">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2235 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38525" id="LblG1589_C38525">¿como se clasifica el hotel respecto al numero de estrellas?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38525" id="G1589_C38525">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2253 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38526" id="LblG1589_C38526">¿cuantas habitaciones tiene el hotel?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38526" id="G1589_C38526">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2254 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38527" id="LblG1589_C38527">¿el hotel tiene operacion nacional o internacional?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38527" id="G1589_C38527">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2255 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38314" id="LblG1589_C38314">NOMBRES (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38314" value=""  name="G1589_C38314"  placeholder="NOMBRES (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38315" id="LblG1589_C38315">APELLIDOS (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38315" value=""  name="G1589_C38315"  placeholder="APELLIDOS (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38316" id="LblG1589_C38316">DOCUMENTO</label>
								<input type="text" class="form-control input-sm" id="G1589_C38316" value=""  name="G1589_C38316"  placeholder="DOCUMENTO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38317" id="LblG1589_C38317">SEXO(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38317" id="G1589_C38317">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2112 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1589_C38318" id="LblG1589_C38318">FECHA DE NACIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G1589_C38318" id="G1589_C38318" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38320" id="LblG1589_C38320">¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38320" id="G1589_C38320">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1527 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <?php 
                            $str_Lsql = "SELECT  G2005_ConsInte__b as id , G2005_C36697, G2005_C36696 FROM ".$BaseDatos_systema.".G2005";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1589_C38321" id="LblG1589_C38321">CARGO(*)</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1589_C38321" id="G1589_C38321">
                                    <option>CARGO | GRUPOCARGO</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0|G1589_C38322'>".($obj->G2005_C36697)." | ".($obj->G2005_C36696)."</option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38322" id="LblG1589_C38322">GRUPO CARGO</label>
								<input type="text" class="form-control input-sm" id="G1589_C38322" value=""  name="G1589_C38322"  placeholder="GRUPO CARGO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38323" id="LblG1589_C38323">TIPO SUSCRIPCIÓN(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38323" id="G1589_C38323">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2115 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38324" id="LblG1589_C38324">D.P PAÍS (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38324" value=""  name="G1589_C38324"  placeholder="D.P PAÍS (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <?php 
                            $str_Lsql = "SELECT  G2043_ConsInte__b as id , G2043_C37408, G2043_C37407 FROM ".$BaseDatos_systema.".G2043";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1589_C38325" id="LblG1589_C38325">D.P CIUDAD (*)</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1589_C38325" id="G1589_C38325">
                                    <option>MUNICIPIO | DEPARTAMENTO</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0|G1589_C38326'>".($obj->G2043_C37408)." | ".($obj->G2043_C37407)."</option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38326" id="LblG1589_C38326">D.P DEPARTAMENTO (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38326" value=""  name="G1589_C38326"  placeholder="D.P DEPARTAMENTO (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38327" id="LblG1589_C38327">D.P BARRIO</label>
								<input type="text" class="form-control input-sm" id="G1589_C38327" value=""  name="G1589_C38327"  placeholder="D.P BARRIO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38328" id="LblG1589_C38328">DIRECCIÓN (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38328" value=""  name="G1589_C38328"  placeholder="DIRECCIÓN (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38329" id="LblG1589_C38329">DIRECCIÓN 2</label>
								<input type="text" class="form-control input-sm" id="G1589_C38329" value=""  name="G1589_C38329"  placeholder="DIRECCIÓN 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38330" id="LblG1589_C38330">TELEFONO 1 (*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38330" value=""  name="G1589_C38330"  placeholder="TELEFONO 1 (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38331" id="LblG1589_C38331">TELEFONO 2</label>
								<input type="text" class="form-control input-sm" id="G1589_C38331" value=""  name="G1589_C38331"  placeholder="TELEFONO 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1589_C38332" id="LblG1589_C38332">CELULAR(*)</label>
								<input type="text" class="form-control input-sm" id="G1589_C38332" value=""  name="G1589_C38332"  placeholder="CELULAR(*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38333" id="LblG1589_C38333">¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38333" id="G1589_C38333">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1527 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G1589_C38334" id="LblG1589_C38334">OBSERVACIONES (CALIFICACIÓN)(*)</label>
                                <textarea class="form-control input-sm" name="G1589_C38334" id="G1589_C38334"  value="" placeholder="OBSERVACIONES (CALIFICACIÓN)(*)"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1589_C38335" id="LblG1589_C38335">TIPO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38335" id="G1589_C38335">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2129 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1589_C38336" id="LblG1589_C38336">OBJECION	</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38336" id="G1589_C38336">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2130 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1589_C38336" id="respuesta_LblG1589_C38336">Respuesta</label>
                        <textarea id="respuesta_G1589_C38336" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1589_C38337" id="LblG1589_C38337">PREGUNTA FRECUENTES</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1589_C38337" id="G1589_C38337">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2131 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1589_C38337" id="respuesta_LblG1589_C38337">Respuesta</label>
                        <textarea id="respuesta_G1589_C38337" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Recuerde que esta información será almacenada en nuestra base de datos; además podrá consultar nuestro aviso de privacidad y política de datos personales, en nuestra página web. WWW.AXIOMAB2B.COM</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr./Sra.__________, muchas gracias por atender mi llamada, le recuerdo que hablo con _____ de la revista LA BARRA que tenga un feliz día/tarde.</h3>
                            <!-- FIN LIBRETO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G1589/G1589_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


        $("#G1589_C38305").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                         
                                '<div class="col-md-6">' + r[0] + '</div>' + 
                                '<div class="col-md-6">' + r[1] + '</div>' +
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1589_C38305").change(function(){
            var valores = $("#G1589_C38305 option:selected").text();
            var campos = $("#G1589_C38305 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

        $("#G1589_C38321").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                         
                                '<div class="col-md-6">' + r[0] + '</div>' + 
                                '<div class="col-md-6">' + r[1] + '</div>' +
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1589_C38321").change(function(){
            var valores = $("#G1589_C38321 option:selected").text();
            var campos = $("#G1589_C38321 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

        $("#G1589_C38325").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                         
                                '<div class="col-md-6">' + r[0] + '</div>' + 
                                '<div class="col-md-6">' + r[1] + '</div>' +
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1589_C38325").change(function(){
            var valores = $("#G1589_C38325 option:selected").text();
            var campos = $("#G1589_C38325 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

                //datepickers
                

            $("#G1589_C28491").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1589_C38318").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G1589_C28492").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                

            $("#G1589_C38303").numeric();
            

                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para Si si habla con el Sr./Sra / si no habla con el Sr./Sra 

    $("#G1589_C38299").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo?	 

    $("#G1589_C38300").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo? 

    $("#G1589_C38301").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para NÚMERO DE EMPLEADOS 

    $("#G1589_C38304").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ACTIVIDAD(*) 

    $("#G1589_C38308").change(function(){ 
            $("#G1589_C28489").prop('disabled', false);
        
            $("#G1589_C28490").prop('disabled', false);
        
            $("#G1589_C28491").prop('disabled', false);
        
            $("#G1589_C28492").prop('disabled', false);
        
            $("#G1589_C28493").prop('disabled', false);
        
            if($(this).val() == '172188'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172189'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172190'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172191'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172192'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172193'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172199'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172200'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172201'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172202'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172203'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172204'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172205'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172206'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172207'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172208'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172209'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172210'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172211'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172212'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172213'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172214'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
        
            if($(this).val() == '172215'){
            $("#G1589_C38525").prop('disabled', true); 
          
            $("#G1589_C38526").prop('disabled', true); 
          
            $("#G1589_C38527").prop('disabled', true); 
          
            }
         
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2232' , idPadre : $(this).val() },
            success : function(data){
                $("#G1589_C38307").html(data);
            }
        });
        
    });

    //function para PERFIL(*) 

    $("#G1589_C38307").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA? 

    $("#G1589_C38309").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO? 

    $("#G1589_C38313").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿como se clasifica el hotel respecto al numero de estrellas? 

    $("#G1589_C38525").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿cuantas habitaciones tiene el hotel? 

    $("#G1589_C38526").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿el hotel tiene operacion nacional o internacional? 

    $("#G1589_C38527").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SEXO(*) 

    $("#G1589_C38317").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido? 

    $("#G1589_C38320").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO SUSCRIPCIÓN(*) 

    $("#G1589_C38323").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria? 

    $("#G1589_C38333").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO 

    $("#G1589_C38335").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OBJECION	 

    $("#G1589_C38336").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PREGUNTA FRECUENTES 

    $("#G1589_C38337").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
