<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1135 WHERE G1135_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }


      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1135_ConsInte__b, G1135_FechaInsercion , USUARI_Nombre____b ,G1135_CodigoMiembro,G1135_PoblacionOrigen , G1135_EstadoDiligenciamiento ,  G1135_IdLlamada , G1135_C17483 as principal ,G1135_C20667,G1135_CodigoMiembro,G1135_C17483,G1135_C17484,G1135_C17485,G1135_C20668,G1135_C20669,G1135_C17486,G1135_C17487,G1135_C17488,G1135_C17489,G1135_C18901,G1135_C17490,G1135_C17491,G1135_C17492,G1135_C17493,G1135_C17494,G1135_C18902,G1135_C18903,G1135_C18904,G1135_C20954,G1135_C18905,G1135_C22656,G1135_C17495,G1135_C17496,G1135_C17497,G1135_C17498,G1135_C17499,G1135_C17500,G1135_C17501,G1135_C17502,G1135_C17503,G1135_C17504,G1135_C17505,G1135_C21447,G1135_C21448,G1135_C18645,G1135_C17507,G1135_C17508,G1135_C17509,G1135_C18646,G1135_C18647,G1135_C18648,G1135_C18649,G1135_C18650,G1135_C20955,G1135_C18651,G1135_C17512,G1135_C18906,G1135_C18907,G1135_C18908,G1135_C18909,G1135_C18910,G1135_C18911 FROM '.$BaseDatos.'.G1135 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1135_Usuario WHERE G1135_ConsInte__b ='.$_POST['id'];

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1135_C20667'] = $key->G1135_C20667;

                $datos[$i]['G1135_CodigoMiembro'] = $key->G1135_CodigoMiembro;

                $datos[$i]['G1135_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1135_C17483'] = $key->G1135_C17483;

                $datos[$i]['G1135_C17484'] = $key->G1135_C17484;

                $datos[$i]['G1135_C17485'] = $key->G1135_C17485;

                $datos[$i]['G1135_C20668'] = $key->G1135_C20668;

                $datos[$i]['G1135_C20669'] = $key->G1135_C20669;

                $datos[$i]['G1135_C17486'] = $key->G1135_C17486;

                $datos[$i]['G1135_C17487'] = $key->G1135_C17487;

                $datos[$i]['G1135_C17488'] = $key->G1135_C17488;

                $datos[$i]['G1135_C17489'] = $key->G1135_C17489;

                $datos[$i]['G1135_C18901'] = $key->G1135_C18901;

                $datos[$i]['G1135_C17490'] = $key->G1135_C17490;

                $datos[$i]['G1135_C17491'] = $key->G1135_C17491;

                $datos[$i]['G1135_C17492'] = $key->G1135_C17492;

                $datos[$i]['G1135_C17493'] = $key->G1135_C17493;

                $datos[$i]['G1135_C17494'] = $key->G1135_C17494;

                $datos[$i]['G1135_C18902'] = $key->G1135_C18902;

                $datos[$i]['G1135_C18903'] = $key->G1135_C18903;

                $datos[$i]['G1135_C18904'] = explode(' ', $key->G1135_C18904)[0];
  
                $hora = '';
                if(!is_null($key->G1135_C20954)){
                    $hora = explode(' ', $key->G1135_C20954)[1];
                }

                $datos[$i]['G1135_C18905'] = explode(' ', $key->G1135_C18905)[0];

                $datos[$i]['G1135_C22656'] = $key->G1135_C22656;

                $datos[$i]['G1135_C17495'] = $key->G1135_C17495;

                $datos[$i]['G1135_C17496'] = $key->G1135_C17496;

                $datos[$i]['G1135_C17497'] = $key->G1135_C17497;

                $datos[$i]['G1135_C17498'] = $key->G1135_C17498;

                $datos[$i]['G1135_C17499'] = explode(' ', $key->G1135_C17499)[0];
  
                $hora = '';
                if(!is_null($key->G1135_C17500)){
                    $hora = explode(' ', $key->G1135_C17500)[1];
                }

                $datos[$i]['G1135_C17501'] = $key->G1135_C17501;

                $datos[$i]['G1135_C17502'] = $key->G1135_C17502;

                $datos[$i]['G1135_C17503'] = $key->G1135_C17503;

                $datos[$i]['G1135_C17504'] = $key->G1135_C17504;

                $datos[$i]['G1135_C17505'] = $key->G1135_C17505;

                $datos[$i]['G1135_C21447'] = $key->G1135_C21447;

                $datos[$i]['G1135_C21448'] = $key->G1135_C21448;

                $datos[$i]['G1135_C18645'] = $key->G1135_C18645;

                $datos[$i]['G1135_C17507'] = explode(' ', $key->G1135_C17507)[0];
  
                $hora = '';
                if(!is_null($key->G1135_C17508)){
                    $hora = explode(' ', $key->G1135_C17508)[1];
                }

                $datos[$i]['G1135_C17509'] = $key->G1135_C17509;

                $datos[$i]['G1135_C18646'] = $key->G1135_C18646;

                $datos[$i]['G1135_C18647'] = $key->G1135_C18647;

                $datos[$i]['G1135_C18648'] = $key->G1135_C18648;

                $datos[$i]['G1135_C18649'] = $key->G1135_C18649;

                $datos[$i]['G1135_C18650'] = explode(' ', $key->G1135_C18650)[0];
  
                $hora = '';
                if(!is_null($key->G1135_C20955)){
                    $hora = explode(' ', $key->G1135_C20955)[1];
                }

                $datos[$i]['G1135_C18651'] = explode(' ', $key->G1135_C18651)[0];

                $datos[$i]['G1135_C17512'] = $key->G1135_C17512;

                $datos[$i]['G1135_C18906'] = $key->G1135_C18906;

                $datos[$i]['G1135_C18907'] = $key->G1135_C18907;

                $datos[$i]['G1135_C18908'] = $key->G1135_C18908;

                $datos[$i]['G1135_C18909'] = $key->G1135_C18909;

                $datos[$i]['G1135_C18910'] = $key->G1135_C18910;

                $datos[$i]['G1135_C18911'] = $key->G1135_C18911;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1135_ConsInte__b as id,  G1135_C17483 as camp1 , G1135_C17485 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1135 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1135_C17496  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1135_C17483 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1135_C17485 like '%".$_POST['Busqueda']."%' ) ";
            }
            
            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1135_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }

			if(!is_null($_POST['G1135_C20667']) && $_POST['G1135_C20667'] != ''){
				$Lsql .= " AND G1135_C20667 LIKE '%". $_POST['G1135_C20667'] ."%' ";
			}
			if(!is_null($_POST['G1135_C17483']) && $_POST['G1135_C17483'] != ''){
				$Lsql .= " AND G1135_C17483 LIKE '%". $_POST['G1135_C17483'] ."%' ";
			}
			if(!is_null($_POST['G1135_C17484']) && $_POST['G1135_C17484'] != ''){
				$Lsql .= " AND G1135_C17484 LIKE '%". $_POST['G1135_C17484'] ."%' ";
			}
			if(!is_null($_POST['G1135_C17486']) && $_POST['G1135_C17486'] != ''){
				$Lsql .= " AND G1135_C17486 LIKE '%". $_POST['G1135_C17486'] ."%' ";
			}

            $Lsql .= " ORDER BY FIELD(G1135_C17496,14272,14273,14274,14275,14276), G1135_FechaInsercion DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }else if($key->estado == 'Sin gestión'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = '4';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b DESC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1135_C18908'])){
            echo '<select class="form-control input-sm"  name="G1135_C18908" id="G1135_C18908">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1135_C18908'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1135_C18908'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1135_C18908'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1135");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1135_ConsInte__b, G1135_FechaInsercion , G1135_Usuario ,  G1135_CodigoMiembro  , G1135_PoblacionOrigen , G1135_EstadoDiligenciamiento ,  G1135_IdLlamada , G1135_C17483 as principal ,G1135_C20667,G1135_C17483,G1135_C17484,G1135_C17485,G1135_C20668,G1135_C20669,G1135_C17486,G1135_C17487,G1135_C17488,G1135_C17489,G1135_C18901,G1135_C17490,G1135_C17491,G1135_C17492,G1135_C17493,G1135_C17494,G1135_C18902, a.LISOPC_Nombre____b as G1135_C18903,G1135_C18904,G1135_C20954,G1135_C18905,G1135_C22656, b.LISOPC_Nombre____b as G1135_C17495, c.LISOPC_Nombre____b as G1135_C17496,G1135_C17497,G1135_C17498,G1135_C17499,G1135_C17500,G1135_C17501,G1135_C17502,G1135_C17503,G1135_C17504,G1135_C17505,G1135_C21447,G1135_C21448, d.LISOPC_Nombre____b as G1135_C18645,G1135_C17507,G1135_C17508,G1135_C17509, e.LISOPC_Nombre____b as G1135_C18646, f.LISOPC_Nombre____b as G1135_C18647, g.LISOPC_Nombre____b as G1135_C18648, h.LISOPC_Nombre____b as G1135_C18649,G1135_C18650,G1135_C20955,G1135_C18651,G1135_C17512, i.LISOPC_Nombre____b as G1135_C18906, j.LISOPC_Nombre____b as G1135_C18907,G1135_C18909, k.LISOPC_Nombre____b as G1135_C18910,G1135_C18911 FROM '.$BaseDatos.'.G1135 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1135_C18903 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1135_C17495 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1135_C17496 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1135_C18645 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1135_C18646 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1135_C18647 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1135_C18648 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1135_C18649 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1135_C18906 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1135_C18907 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1135_C18908 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1135_C18910';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1135_C17500)){
                    $hora_a = explode(' ', $fila->G1135_C17500)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1135_C17508)){
                    $hora_b = explode(' ', $fila->G1135_C17508)[1];
                }

                $hora_c = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1135_C20955)){
                    $hora_c = explode(' ', $fila->G1135_C20955)[1];
                }

                $hora_d = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1135_C20954)){
                    $hora_d = explode(' ', $fila->G1135_C20954)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1135_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1135_ConsInte__b , ($fila->G1135_C20667) , ($fila->G1135_C17483) , ($fila->G1135_C17484) , ($fila->G1135_C17485) , ($fila->G1135_C20668) , ($fila->G1135_C20669) , ($fila->G1135_C17486) , ($fila->G1135_C17487) , ($fila->G1135_C17488) , ($fila->G1135_C17489) , ($fila->G1135_C18901) , ($fila->G1135_C17490) , ($fila->G1135_C17491) , ($fila->G1135_C17492) , ($fila->G1135_C17493) , ($fila->G1135_C17494) , ($fila->G1135_C18902) , ($fila->G1135_C18903) , explode(' ', $fila->G1135_C18904)[0] , $hora_a , explode(' ', $fila->G1135_C18905)[0] , ($fila->G1135_C22656) , ($fila->G1135_C17495) , ($fila->G1135_C17496) , ($fila->G1135_C17497) , ($fila->G1135_C17498) , explode(' ', $fila->G1135_C17499)[0] , $hora_b , ($fila->G1135_C17501) , ($fila->G1135_C17502) , ($fila->G1135_C17503) , ($fila->G1135_C17504) , ($fila->G1135_C17505) , ($fila->G1135_C21447) , ($fila->G1135_C21448) , ($fila->G1135_C18645) , explode(' ', $fila->G1135_C17507)[0] , $hora_c , ($fila->G1135_C17509) , ($fila->G1135_C18646) , ($fila->G1135_C18647) , ($fila->G1135_C18648) , ($fila->G1135_C18649) , explode(' ', $fila->G1135_C18650)[0] , $hora_d , explode(' ', $fila->G1135_C18651)[0] , ($fila->G1135_C17512) , ($fila->G1135_C18906) , ($fila->G1135_C18907) , ($fila->G1135_C18909) , ($fila->G1135_C18910) , ($fila->G1135_C18911) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1135 WHERE G1135_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1135_ConsInte__b as id,  G1135_C17483 as camp1 , G1135_C17485 as camp2, G1135_C17496 as estado  FROM '.$BaseDatos.'.G1135 ORDER BY FIELD(G1135_C17496,14272,14273,14274,14275,14276), G1135_FechaInsercion DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){

                $color = '';
                $strIconoBackOffice = '';
                if($obj->estado == '14272' || $obj->estado == '14274'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14275'){
                    $color = 'color:green;';
                    $strIconoBackOffice = 'Cerrada';
                }else if($obj->estado == '14276'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }else if($obj->estado == '14273'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }

                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1135 SET "; 
  
            if(isset($_POST["G1135_C20667"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C20667 = '".$_POST["G1135_C20667"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17483"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17483 = '".$_POST["G1135_C17483"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17484"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17484 = '".$_POST["G1135_C17484"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17485"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17485 = '".$_POST["G1135_C17485"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C20668"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C20668 = '".$_POST["G1135_C20668"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C20669"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C20669 = '".$_POST["G1135_C20669"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17486"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17486 = '".$_POST["G1135_C17486"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17487"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17487 = '".$_POST["G1135_C17487"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17488"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17488 = '".$_POST["G1135_C17488"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17489"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17489 = '".$_POST["G1135_C17489"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18901"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18901 = '".$_POST["G1135_C18901"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17490"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17490 = '".$_POST["G1135_C17490"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17491"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17491 = '".$_POST["G1135_C17491"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17492"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17492 = '".$_POST["G1135_C17492"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17493"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17493 = '".$_POST["G1135_C17493"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17494"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17494 = '".$_POST["G1135_C17494"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18902"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18902 = '".$_POST["G1135_C18902"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18903"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18903 = '".$_POST["G1135_C18903"]."'";
                $validar = 1;
            }
             
 
            $G1135_C18904 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18904"])){    
                if($_POST["G1135_C18904"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1135_C18904"]);
                    if(count($tieneHora) > 1){
                    	$G1135_C18904 = "'".$_POST["G1135_C18904"]."'";
                    }else{
                    	$G1135_C18904 = "'".str_replace(' ', '',$_POST["G1135_C18904"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1135_C18904 = ".$G1135_C18904;
                    $validar = 1;
                }
            }
  
            $G1135_C20954 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1135_C20954"])){   
                if($_POST["G1135_C20954"] != '' && $_POST["G1135_C20954"] != 'undefined' && $_POST["G1135_C20954"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C20954 = "'".$fecha." ".str_replace(' ', '',$_POST["G1135_C20954"])."'";
                    $LsqlU .= $separador." G1135_C20954 = ".$G1135_C20954."";
                    $validar = 1;
                }
            }
 
            $G1135_C18905 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18905"])){    
                if($_POST["G1135_C18905"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1135_C18905"]);
                    if(count($tieneHora) > 1){
                    	$G1135_C18905 = "'".$_POST["G1135_C18905"]."'";
                    }else{
                    	$G1135_C18905 = "'".str_replace(' ', '',$_POST["G1135_C18905"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1135_C18905 = ".$G1135_C18905;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1135_C22656"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C22656 = '".$_POST["G1135_C22656"]."'";
                $validar = 1;
            }
             
 
            $G1135_C17495 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1135_C17495 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1135_C17495 = ".$G1135_C17495;
                    $validar = 1;

                    
                }
            }
 
            $G1135_C17496 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1135_C17496 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1135_C17496 = ".$G1135_C17496;
                    $validar = 1;
                }
            }
  
            $G1135_C17497 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C17497"])){
                if($_POST["G1135_C17497"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17497 = $_POST["G1135_C17497"];
                    $LsqlU .= $separador." G1135_C17497 = ".$G1135_C17497."";
                    $validar = 1;
                }
            }
  
            $G1135_C17498 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C17498"])){
                if($_POST["G1135_C17498"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17498 = $_POST["G1135_C17498"];
                    $LsqlU .= $separador." G1135_C17498 = ".$G1135_C17498."";
                    $validar = 1;
                }
            }
 
            $G1135_C17501 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1135_C17501 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1135_C17501 = ".$G1135_C17501;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1135_C17502"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17502 = '".$_POST["G1135_C17502"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17503"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17503 = '".$_POST["G1135_C17503"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17504"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17504 = '".$_POST["G1135_C17504"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17505"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17505 = '".$_POST["G1135_C17505"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C21447"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C21447 = '".$_POST["G1135_C21447"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C21448"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C21448 = '".$_POST["G1135_C21448"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18645 = '".$_POST["G1135_C18645"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C17506"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17506 = '".$_POST["G1135_C17506"]."'";
                $validar = 1;
            }
             
 
            $G1135_C17507 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C17507"])){    
                if($_POST["G1135_C17507"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1135_C17507"]);
                    if(count($tieneHora) > 1){
                    	$G1135_C17507 = "'".$_POST["G1135_C17507"]."'";
                    }else{
                    	$G1135_C17507 = "'".str_replace(' ', '',$_POST["G1135_C17507"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1135_C17507 = ".$G1135_C17507;
                    $validar = 1;
                }
            }
  
            $G1135_C17508 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1135_C17508"])){   
            //     if($_POST["G1135_C17508"] != '' && $_POST["G1135_C17508"] != 'undefined' && $_POST["G1135_C17508"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1135_C17508 = "'".$fecha." ".str_replace(' ', '',$_POST["G1135_C17508"])."'";
            //         $LsqlU .= $separador." G1135_C17508 = ".$G1135_C17508."";
            //         $validar = 1;
            //     }
            // }
  
            if(isset($_POST["G1135_C17509"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17509 = '".$_POST["G1135_C17509"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18646 = '".$_POST["G1135_C18646"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18647 = '".$_POST["G1135_C18647"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18648"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18648 = '".$_POST["G1135_C18648"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18649 = '".$_POST["G1135_C18649"]."'";
                $validar = 1;
            }
             
 
            $G1135_C18650 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18650"])){    
                if($_POST["G1135_C18650"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18650 = "'".explode(" ", trim($_POST["G1135_C18650"]))[0]." 00:00:00'";

                    $LsqlU .= $separador." G1135_C18650 = ".$G1135_C18650;
                    $validar = 1;
                }
            }
  
            // $G1135_C20955 = NULL;
            // este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1135_C20955"])){   
            //     if($_POST["G1135_C20955"] != '' && $_POST["G1135_C20955"] != 'undefined' && $_POST["G1135_C20955"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1135_C20955 = "'".$fecha." ".str_replace(' ', '',$_POST["G1135_C20955"])."'";
            //         $LsqlU .= $separador." G1135_C20955 = ".$G1135_C20955."";
            //         $validar = 1;
            //     }
            // }
 
            // $G1135_C18651 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no
            // if(isset($_POST["G1135_C18651"])){    
            //     if($_POST["G1135_C18651"] != ''){
            //         $separador = "";
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $tieneHora = explode(' ' , $_POST["G1135_C18651"]);
            //         if(count($tieneHora) > 1){
            //         	$G1135_C18651 = "'".$_POST["G1135_C18651"]."'";
            //         }else{
            //         	$G1135_C18651 = "'".str_replace(' ', '',$_POST["G1135_C18651"])." 00:00:00'";
            //         }


            //         $LsqlU .= $separador." G1135_C18651 = ".$G1135_C18651;
            //         $validar = 1;
            //     }
            // }
  
            if(isset($_POST["G1135_C17512"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17512 = '".$_POST["G1135_C17512"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18906"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18906 = '".$_POST["G1135_C18906"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18907"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18907 = '".$_POST["G1135_C18907"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1135_C18908"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18908 = '".$_POST["G1135_C18908"]."'";
                $validar = 1;
            }
             
  
            $G1135_C18909 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C18909"])){
                if($_POST["G1135_C18909"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18909 = $_POST["G1135_C18909"];
                    $LsqlU .= $separador." G1135_C18909 = ".$G1135_C18909."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1135_C18910"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18910 = '".$_POST["G1135_C18910"]."'";
                $validar = 1;
            }
             
  
            $G1135_C18911 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C18911"])){
                if($_POST["G1135_C18911"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18911 = $_POST["G1135_C18911"];
                    $LsqlU .= $separador." G1135_C18911 = ".$G1135_C18911."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
            
                    if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1135_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }

			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1135_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            // echo $Lsql;
            // die();

            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                    $error = $mysqli->error;
                    $consultaT=$Lsql;
                    $actual = date('Y-m-d H:i:s');
                    $consulta = "INSERT INTO ".$BaseDatos.".log_yorman_temporal (sqlGenerado,comentario,fecha) VALUES (\"".$consultaT."\",'No se pudo actualizar la tarea','".$actual."');";
                    $insercion = $mysqli->query($consulta);
                    $id = $mysqli->insert_id;

                    $consulta = "UPDATE ".$BaseDatos.".log_yorman_temporal SET errorGenerado = \"".$error."\" WHERE id = ".$id; 
                    $actualizado = $mysqli->query($consulta);
                }
            }
        }
    }
  

  
?>
