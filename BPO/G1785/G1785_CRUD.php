<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1785_ConsInte__b, G1785_FechaInsercion , G1785_Usuario ,  G1785_CodigoMiembro  , G1785_PoblacionOrigen , G1785_EstadoDiligenciamiento ,  G1785_IdLlamada , G1785_C33016 as principal ,G1785_C33018,G1785_C33033,G1785_C33020,G1785_C33007,G1785_C33008,G1785_C33009,G1785_C33010,G1785_C33011,G1785_C33012,G1785_C33013,G1785_C33014,G1785_C33015,G1785_C33023,G1785_C33024,G1785_C33026,G1785_C33030,G1785_C33016,G1785_C33017,G1785_C33027,G1785_C33031,G1785_C33025,G1785_C33032,G1785_C33034,G1785_C33035 FROM '.$BaseDatos.'.G1785 WHERE G1785_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1785_C33018'] = $key->G1785_C33018;

                $datos[$i]['G1785_C33033'] = $key->G1785_C33033;

                $datos[$i]['G1785_C33020'] = $key->G1785_C33020;

                $datos[$i]['G1785_C33007'] = $key->G1785_C33007;

                $datos[$i]['G1785_C33008'] = $key->G1785_C33008;

                $datos[$i]['G1785_C33009'] = explode(' ', $key->G1785_C33009)[0];
  
                $hora = '';
                if(!is_null($key->G1785_C33010)){
                    $hora = explode(' ', $key->G1785_C33010)[1];
                }

                $datos[$i]['G1785_C33010'] = $hora;

                $datos[$i]['G1785_C33011'] = $key->G1785_C33011;

                $datos[$i]['G1785_C33012'] = $key->G1785_C33012;

                $datos[$i]['G1785_C33013'] = $key->G1785_C33013;

                $datos[$i]['G1785_C33014'] = $key->G1785_C33014;

                $datos[$i]['G1785_C33015'] = $key->G1785_C33015;

                $datos[$i]['G1785_C33023'] = $key->G1785_C33023;

                $datos[$i]['G1785_C33024'] = $key->G1785_C33024;

                $datos[$i]['G1785_C33026'] = $key->G1785_C33026;

                $datos[$i]['G1785_C33030'] = $key->G1785_C33030;

                $datos[$i]['G1785_C33016'] = $key->G1785_C33016;

                $datos[$i]['G1785_C33017'] = $key->G1785_C33017;

                $datos[$i]['G1785_C33027'] = $key->G1785_C33027;

                $datos[$i]['G1785_C33031'] = $key->G1785_C33031;

                $datos[$i]['G1785_C33025'] = $key->G1785_C33025;

                $datos[$i]['G1785_C33032'] = $key->G1785_C33032;

                $datos[$i]['G1785_C33034'] = $key->G1785_C33034;

                $datos[$i]['G1785_C33035'] = $key->G1785_C33035;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1785";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = " AND G1785_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $regProp = "";
            }

            //JDBD valores filtros.
            $B = $_POST["B"];
            $A = $_POST["A"];
            $T = $_POST["T"];
            $F = $_POST["F"];
            $E = $_POST["E"];


            $Lsql = "SELECT G1785_ConsInte__b as id,  G1785_C33023 as camp2 , G1785_C33016 as camp1 
                     FROM ".$BaseDatos.".G1785  WHERE TRUE ".$regProp;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1785_ConsInte__b as id,  G1785_C33023 as camp2 , G1785_C33016 as camp1  
                    FROM ".$BaseDatos.".G1785  JOIN ".$BaseDatos.".G1785_M".$_POST['muestra']." ON G1785_ConsInte__b = G1785_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1785_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if ($B != "" && $B != NULL) {
                $Lsql .= " AND (G1785_C33023 LIKE '%".$B."%' OR G1785_C33016 LIKE '%".$B."%') ";
            }
            if ($A != 0 && $A != -1 && $A != NULL) {
                $Lsql .= " AND G1785_Usuario = ".$A." ";
            }
            if ($T != 0 && $T != -1 && $T != NULL) {
                $Lsql .= " AND G1785_C33007 = ".$T." ";
            }
            if ($F != "" && $F != NULL) {
                $Lsql .= " AND DATE_FORMAT(G1785_FechaInsercion,'%Y-%m-%d') = '".$F."' ";
            }
            if ($E != 0 && $E != -1 && $E != NULL) {
                if ($E == -203) {
                    $Lsql .= " AND G1785_C = -203 OR G1785_C = '' OR G1785_C IS NULL ";
                }else{
                    $Lsql .= " AND G1785_C = ".$E." "; 
                }
            }


            $Lsql .= " ORDER BY G1785_ConsInte__b DESC LIMIT 0, 50 "; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1785");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1785_ConsInte__b, G1785_FechaInsercion , G1785_Usuario ,  G1785_CodigoMiembro  , G1785_PoblacionOrigen , G1785_EstadoDiligenciamiento ,  G1785_IdLlamada , G1785_C33016 as principal ,G1785_C33018,G1785_C33033,G1785_C33020, a.LISOPC_Nombre____b as G1785_C33007, b.LISOPC_Nombre____b as G1785_C33008,G1785_C33009,G1785_C33010,G1785_C33011,G1785_C33012,G1785_C33013,G1785_C33014,G1785_C33015,G1785_C33023,G1785_C33024,G1785_C33026,G1785_C33030,G1785_C33016,G1785_C33017,G1785_C33027,G1785_C33031,G1785_C33025,G1785_C33032,G1785_C33034,G1785_C33035 FROM '.$BaseDatos.'.G1785 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1785_C33007 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1785_C33008';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1785_C33010)){
                    $hora_a = explode(' ', $fila->G1785_C33010)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1785_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1785_ConsInte__b , ($fila->G1785_C33018) , ($fila->G1785_C33033) , ($fila->G1785_C33020) , ($fila->G1785_C33007) , ($fila->G1785_C33008) , explode(' ', $fila->G1785_C33009)[0] , $hora_a , ($fila->G1785_C33011) , ($fila->G1785_C33012) , ($fila->G1785_C33013) , ($fila->G1785_C33014) , ($fila->G1785_C33015) , ($fila->G1785_C33023) , ($fila->G1785_C33024) , ($fila->G1785_C33026) , ($fila->G1785_C33030) , ($fila->G1785_C33016) , ($fila->G1785_C33017) , ($fila->G1785_C33027) , ($fila->G1785_C33031) , ($fila->G1785_C33025) , ($fila->G1785_C33032) , ($fila->G1785_C33034) , ($fila->G1785_C33035) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1785 WHERE G1785_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1785";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = ' AND G1785_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $regProp = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $A = 0;
            $T = 0;
            $F = "";
            $B = "";
            $E = 0;

            if (isset($_POST["A"])) {
                $A = $_POST["A"];
            }
            if (isset($_POST["T"])) {
                $T = $_POST["T"];
            }
            if (isset($_POST["F"])) {
                $F = $_POST["F"];
            }
            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            if (isset($_POST["E"])) {
                $E = $_POST["E"];
            }

            $Zsql = 'SELECT  G1785_ConsInte__b as id,  G1785_C33023 as camp2 , G1785_C33016 as camp1  FROM '.$BaseDatos.'.G1785 WHERE TRUE'.$regProp;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1785_ConsInte__b as id,  G1785_C33023 as camp2 , G1785_C33016 as camp1  
                    FROM ".$BaseDatos.".G1785  JOIN ".$BaseDatos.".G1785_M".$_POST['muestra']." ON G1785_ConsInte__b = G1785_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1785_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if ($A != 0) {
                $Zsql .= ' AND G1785_Usuario = '.$A.' ';
            }

            if ($T != 0) {
                $Zsql .= ' AND G1785_C33007 = '.$T.' ';
            }

            if ($F != "") {
                $Zsql .= ' AND DATE_FORMAT(G1785_FechaInsercion,"%Y-%m-%d") = "'.$F.'" ';
            }

            if ($B != "") {
                $Zsql .= ' AND (G1785_C33023 LIKE "%'.$B.'%" OR G1785_C33016 LIKE "%'.$B.'%") ';
            }

            if ($E != 0) {
                if ($E == -203) {
                    $Zsql .= ' AND G1785_C = -203 OR G1785_C = "" OR G1785_C IS NULL ';
                }else{
                    $Zsql .= ' AND G1785_C = '.$E.'  ';
                }
            }

            $Zsql .= ' ORDER BY G1785_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1785 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1785(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1785_C33018"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33018 = '".$_POST["G1785_C33018"]."'";
                $LsqlI .= $separador."G1785_C33018";
                $LsqlV .= $separador."'".$_POST["G1785_C33018"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33033"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33033 = '".$_POST["G1785_C33033"]."'";
                $LsqlI .= $separador."G1785_C33033";
                $LsqlV .= $separador."'".$_POST["G1785_C33033"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33019"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33019 = '".$_POST["G1785_C33019"]."'";
                $LsqlI .= $separador."G1785_C33019";
                $LsqlV .= $separador."'".$_POST["G1785_C33019"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33020"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33020 = '".$_POST["G1785_C33020"]."'";
                $LsqlI .= $separador."G1785_C33020";
                $LsqlV .= $separador."'".$_POST["G1785_C33020"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33021"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33021 = '".$_POST["G1785_C33021"]."'";
                $LsqlI .= $separador."G1785_C33021";
                $LsqlV .= $separador."'".$_POST["G1785_C33021"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33022"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33022 = '".$_POST["G1785_C33022"]."'";
                $LsqlI .= $separador."G1785_C33022";
                $LsqlV .= $separador."'".$_POST["G1785_C33022"]."'";
                $validar = 1;
            }
             
 
            $G1785_C33007 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1785_C33007 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1785_C33007 = ".$G1785_C33007;
                    $LsqlI .= $separador." G1785_C33007";
                    $LsqlV .= $separador.$G1785_C33007;
                    $validar = 1;

                    
                }
            }
 
            $G1785_C33008 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1785_C33008 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1785_C33008 = ".$G1785_C33008;
                    $LsqlI .= $separador." G1785_C33008";
                    $LsqlV .= $separador.$G1785_C33008;
                    $validar = 1;
                }
            }
 
            $G1785_C33009 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1785_C33009 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1785_C33009 = ".$G1785_C33009;
                    $LsqlI .= $separador." G1785_C33009";
                    $LsqlV .= $separador.$G1785_C33009;
                    $validar = 1;
                }
            }
 
            $G1785_C33010 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1785_C33010 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1785_C33010 = ".$G1785_C33010;
                    $LsqlI .= $separador." G1785_C33010";
                    $LsqlV .= $separador.$G1785_C33010;
                    $validar = 1;
                }
            }
 
            $G1785_C33011 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1785_C33011 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1785_C33011 = ".$G1785_C33011;
                    $LsqlI .= $separador." G1785_C33011";
                    $LsqlV .= $separador.$G1785_C33011;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1785_C33012"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33012 = '".$_POST["G1785_C33012"]."'";
                $LsqlI .= $separador."G1785_C33012";
                $LsqlV .= $separador."'".$_POST["G1785_C33012"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33013"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33013 = '".$_POST["G1785_C33013"]."'";
                $LsqlI .= $separador."G1785_C33013";
                $LsqlV .= $separador."'".$_POST["G1785_C33013"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33014"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33014 = '".$_POST["G1785_C33014"]."'";
                $LsqlI .= $separador."G1785_C33014";
                $LsqlV .= $separador."'".$_POST["G1785_C33014"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33015"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33015 = '".$_POST["G1785_C33015"]."'";
                $LsqlI .= $separador."G1785_C33015";
                $LsqlV .= $separador."'".$_POST["G1785_C33015"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33023"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33023 = '".$_POST["G1785_C33023"]."'";
                $LsqlI .= $separador."G1785_C33023";
                $LsqlV .= $separador."'".$_POST["G1785_C33023"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33024"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33024 = '".$_POST["G1785_C33024"]."'";
                $LsqlI .= $separador."G1785_C33024";
                $LsqlV .= $separador."'".$_POST["G1785_C33024"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33026"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33026 = '".$_POST["G1785_C33026"]."'";
                $LsqlI .= $separador."G1785_C33026";
                $LsqlV .= $separador."'".$_POST["G1785_C33026"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33030"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33030 = '".$_POST["G1785_C33030"]."'";
                $LsqlI .= $separador."G1785_C33030";
                $LsqlV .= $separador."'".$_POST["G1785_C33030"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33016"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33016 = '".$_POST["G1785_C33016"]."'";
                $LsqlI .= $separador."G1785_C33016";
                $LsqlV .= $separador."'".$_POST["G1785_C33016"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33017"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33017 = '".$_POST["G1785_C33017"]."'";
                $LsqlI .= $separador."G1785_C33017";
                $LsqlV .= $separador."'".$_POST["G1785_C33017"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33027"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33027 = '".$_POST["G1785_C33027"]."'";
                $LsqlI .= $separador."G1785_C33027";
                $LsqlV .= $separador."'".$_POST["G1785_C33027"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33031"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33031 = '".$_POST["G1785_C33031"]."'";
                $LsqlI .= $separador."G1785_C33031";
                $LsqlV .= $separador."'".$_POST["G1785_C33031"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33025"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33025 = '".$_POST["G1785_C33025"]."'";
                $LsqlI .= $separador."G1785_C33025";
                $LsqlV .= $separador."'".$_POST["G1785_C33025"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33032"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33032 = '".$_POST["G1785_C33032"]."'";
                $LsqlI .= $separador."G1785_C33032";
                $LsqlV .= $separador."'".$_POST["G1785_C33032"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33034"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33034 = '".$_POST["G1785_C33034"]."'";
                $LsqlI .= $separador."G1785_C33034";
                $LsqlV .= $separador."'".$_POST["G1785_C33034"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33035"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33035 = '".$_POST["G1785_C33035"]."'";
                $LsqlI .= $separador."G1785_C33035";
                $LsqlV .= $separador."'".$_POST["G1785_C33035"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33028"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33028 = '".$_POST["G1785_C33028"]."'";
                $LsqlI .= $separador."G1785_C33028";
                $LsqlV .= $separador."'".$_POST["G1785_C33028"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33029"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33029 = '".$_POST["G1785_C33029"]."'";
                $LsqlI .= $separador."G1785_C33029";
                $LsqlV .= $separador."'".$_POST["G1785_C33029"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33036"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33036 = '".$_POST["G1785_C33036"]."'";
                $LsqlI .= $separador."G1785_C33036";
                $LsqlV .= $separador."'".$_POST["G1785_C33036"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33037"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33037 = '".$_POST["G1785_C33037"]."'";
                $LsqlI .= $separador."G1785_C33037";
                $LsqlV .= $separador."'".$_POST["G1785_C33037"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1785_C33038"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_C33038 = '".$_POST["G1785_C33038"]."'";
                $LsqlI .= $separador."G1785_C33038";
                $LsqlV .= $separador."'".$_POST["G1785_C33038"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_TipNo_Efe_b, MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $reintento = $dataMonoEf->MONOEF_TipNo_Efe_b;
                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1785_C33008 = ".$reintento;
                    $LsqlI .= $separador."G1785_C33008";
                    $LsqlV .= $separador.$reintento;
                    $validar = 1;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1785_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1785_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1785_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1785_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1785_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1785_Usuario , G1785_FechaInsercion, G1785_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1785_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1785 WHERE G1785_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;

                        if (isset($_POST["TipNoEF"]) && ($_POST["TipNoEF"] == 2 || $_POST["TipNoEF"] == "2")) {{

                            $strSQLInsert_t = "INSERT INTO ".$BaseDatos.".G1785_M1318 (G1785_M1318_CoInMiPo__b,G1785_M1318_Activo____b,G1785_M1318_Estado____b,G1785_M1318_TipoReintentoGMI_b,G1785_M1318_NumeInte__b,G1785_M1318_CantidadIntentosGMI_b,G1785_M1318_FecHorAge_b,G1785_M1318_Comentari_b) VALUES (".$_GET['CodigoMiembro'].",-1,2,0,0,0,'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."','".$_POST["textAreaComentarios"]."')";
                            
                            $mysqli->query($strSQLInsert_t);

                        }

                        echo $UltimoID;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
