
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1042/G1042_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1042_ConsInte__b as id, G1042_C14728 as camp1 , G1042_C14808 as camp2 FROM ".$BaseDatos.".G1042  WHERE G1042_Usuario = ".$idUsuario." ORDER BY G1042_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1042_ConsInte__b as id, G1042_C14728 as camp1 , G1042_C14808 as camp2 FROM ".$BaseDatos.".G1042  ORDER BY G1042_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1042_ConsInte__b as id, G1042_C14728 as camp1 , G1042_C14808 as camp2 FROM ".$BaseDatos.".G1042 JOIN ".$BaseDatos.".G1042_M".$resultEstpas->muestr." ON G1042_ConsInte__b = G1042_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1042_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1042_ConsInte__b as id, G1042_C14728 as camp1 , G1042_C14808 as camp2 FROM ".$BaseDatos.".G1042 JOIN ".$BaseDatos.".G1042_M".$resultEstpas->muestr." ON G1042_ConsInte__b = G1042_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1042_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1042_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1042_ConsInte__b as id, G1042_C14728 as camp1 , G1042_C14808 as camp2 FROM ".$BaseDatos.".G1042  ORDER BY G1042_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){


    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary"  id="1795">
    <div class="box-header with-border">
        <h4 class="box-title">
            CONVERSACION CON EL PACIENTE
        </h4>
        
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días/tardes/noches esta comunicado con CONVATEC MEDICAL CARE Clinica de Heridas y Ostomias le habla  (operador) con quien tengo el gusto de hablar?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Mucho gusto señor (a) en que le puedo colaborar?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1042_C29142" id="LblG1042_C29142">Me podría confirmar, ¿usted es el paciente?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1042_C29142" id="G1042_C29142">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 723 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C29143" id="LblG1042_C29143">Me confirma su nombre</label><input type="text" class="form-control input-sm" id="G1042_C29143" value="<?php if (isset($_GET['G1042_C29143'])) {
                            echo $_GET['G1042_C29143'];
                        } ?>"  name="G1042_C29143"  placeholder="Me confirma su nombre"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1042_C29144" id="LblG1042_C29144">Me confirma el parentesco que tiene con el paciente</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1042_C29144" id="G1042_C29144">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1141 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1042_C29150" id="LblG1042_C29150">Preguntas y Respuestas</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1042_C29150" id="G1042_C29150">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1588 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1042_C29150" id="respuesta_LblG1042_C29150">Respuesta</label>
                        <textarea id="respuesta_G1042_C29150" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


    </div>
</div>

<div  id="1791" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14728" id="LblG1042_C14728">Me podría informar nombre  del paciente</label><input type="text" class="form-control input-sm" id="G1042_C14728" value="<?php if (isset($_GET['G1042_C14728'])) {
                            echo $_GET['G1042_C14728'];
                        } ?>"  name="G1042_C14728"  placeholder="Me podría informar nombre  del paciente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C29145" id="LblG1042_C29145">Me podría informar apellido del paciente</label><input type="text" class="form-control input-sm" id="G1042_C29145" value="<?php if (isset($_GET['G1042_C29145'])) {
                            echo $_GET['G1042_C29145'];
                        } ?>"  name="G1042_C29145"  placeholder="Me podría informar apellido del paciente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14808" id="LblG1042_C14808">Me podría informar número de documento del paciente</label><input type="text" class="form-control input-sm" id="G1042_C14808" value="<?php if (isset($_GET['G1042_C14808'])) {
                            echo $_GET['G1042_C14808'];
                        } ?>"  name="G1042_C14808"  placeholder="Me podría informar número de documento del paciente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14732" id="LblG1042_C14732">Me podría informar número de celular  del paciente</label><input type="text" class="form-control input-sm" id="G1042_C14732" value="<?php if (isset($_GET['G1042_C14732'])) {
                            echo $_GET['G1042_C14732'];
                        } ?>"  name="G1042_C14732"  placeholder="Me podría informar número de celular  del paciente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14731" id="LblG1042_C14731">Me podría informar número de teléfono fijo  del paciente</label><input type="text" class="form-control input-sm" id="G1042_C14731" value="<?php if (isset($_GET['G1042_C14731'])) {
                            echo $_GET['G1042_C14731'];
                        } ?>"  name="G1042_C14731"  placeholder="Me podría informar número de teléfono fijo  del paciente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1042_C29146" id="LblG1042_C29146">Me podría informar la EPS que lo remite</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1042_C29146" id="G1042_C29146">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1333 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Por favor tome nota de la siguiente información, su cita quedo programada para:</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C23293" id="LblG1042_C23293">El día de su agenda es</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1042_C23293'])) {
                            echo $_GET['G1042_C23293'];
                        } ?>"  name="G1042_C23293" id="G1042_C23293" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G1042_C23294" id="LblG1042_C23294">A la hora</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora"  name="G1042_C23294" id="G1042_C23294" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G1042_C23294">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Recuerde llegar entre 15 o 20 minutos antes de la cita, llevar orden firmada por el profesional junto con la autorización, originales y vigentes, se permite el ingreso de un acompañante siempre y cuando sea mayor de edad y no se permite el ingreso de menores de edad a la clínica, le confirmo la dirección calle 82 # 18-27/31, a dos cuadras de la estación de héroes o junto a la universidad san martín.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Señor (a) recuerde que habló con (nombre del asesor) de CONVATEC MEDICAL CARE Clinica de heridas y Ostomias. Feliz día/tarde.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="1793" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14742" id="LblG1042_C14742">Agente</label><input type="text" class="form-control input-sm" id="G1042_C14742" value="<?php echo getNombreUser($token);?>" readonly name="G1042_C14742"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14743" id="LblG1042_C14743">Fecha</label><input type="text" class="form-control input-sm" id="G1042_C14743" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G1042_C14743"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14744" id="LblG1042_C14744">Hora</label><input type="text" class="form-control input-sm" id="G1042_C14744" value="<?php echo date('H:i:s');?>" readonly name="G1042_C14744"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1042_C14745" id="LblG1042_C14745">Campaña</label><input type="text" class="form-control input-sm" id="G1042_C14745" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1042_C14745"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div hidden id="s_1794" >
<h3 class="box box-title">
        <a style="float: right;" class="btn btn-success pull-right FinalizarCalificacion" role="button" >Finalizar Calificacion&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i>
        </a></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">ETIQUETA TELEFONICA</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32124" id="LblG1042_C32124">Utiliza términos de cortesía como por favor, gracias, señor, señora</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32124'])) {
                            echo $_GET['G1042_C32124'];
                        } ?>"  name="G1042_C32124" id="G1042_C32124" placeholder="Utiliza términos de cortesía como por favor, gracias, señor, señora">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32128" id="LblG1042_C32128">Utiliza frases para clarificar lo manifestado por el cliente - Parafraseo</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32128'])) {
                            echo $_GET['G1042_C32128'];
                        } ?>"  name="G1042_C32128" id="G1042_C32128" placeholder="Utiliza frases para clarificar lo manifestado por el cliente - Parafraseo">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32130" id="LblG1042_C32130">Proporciona la información de acuerdo con el guión</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32130'])) {
                            echo $_GET['G1042_C32130'];
                        } ?>"  name="G1042_C32130" id="G1042_C32130" placeholder="Proporciona la información de acuerdo con el guión">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32131" id="LblG1042_C32131">Indica al cliente pasos a seguir</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32131'])) {
                            echo $_GET['G1042_C32131'];
                        } ?>"  name="G1042_C32131" id="G1042_C32131" placeholder="Indica al cliente pasos a seguir">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">MANEJO DE SITUACIONES DIFICILES</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32134" id="LblG1042_C32134">Evita interrumpir al cliente y escucha activamente.</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32134'])) {
                            echo $_GET['G1042_C32134'];
                        } ?>"  name="G1042_C32134" id="G1042_C32134" placeholder="Evita interrumpir al cliente y escucha activamente.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32135" id="LblG1042_C32135">Ofrece disculpas por lo sucedido</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32135'])) {
                            echo $_GET['G1042_C32135'];
                        } ?>"  name="G1042_C32135" id="G1042_C32135" placeholder="Ofrece disculpas por lo sucedido">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32136" id="LblG1042_C32136">Mantiene la llamada hasta el final</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32136'])) {
                            echo $_GET['G1042_C32136'];
                        } ?>"  name="G1042_C32136" id="G1042_C32136" placeholder="Mantiene la llamada hasta el final">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32137" id="LblG1042_C32137">Evita acoso o agresión verbal al cliente - ( Incluye Palabras inadecuadas o aumentar el volumen de la voz)</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32137'])) {
                            echo $_GET['G1042_C32137'];
                        } ?>"  name="G1042_C32137" id="G1042_C32137" placeholder="Evita acoso o agresión verbal al cliente - ( Incluye Palabras inadecuadas o aumentar el volumen de la voz)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">REGISTRO DE LA INFORMACIÓN</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32140" id="LblG1042_C32140">Tipifica bajo el concepto adecuado la gestión</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32140'])) {
                            echo $_GET['G1042_C32140'];
                        } ?>"  name="G1042_C32140" id="G1042_C32140" placeholder="Tipifica bajo el concepto adecuado la gestión">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">SALUDO</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32141" id="LblG1042_C32141">Evita errores e inconsistencias en la información registrada</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32141'])) {
                            echo $_GET['G1042_C32141'];
                        } ?>"  name="G1042_C32141" id="G1042_C32141" placeholder="Evita errores e inconsistencias en la información registrada">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32118" id="LblG1042_C32118">Utiliza el saludo de acuerdo con lo establecido en el guión </label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32118'])) {
                            echo $_GET['G1042_C32118'];
                        } ?>"  name="G1042_C32118" id="G1042_C32118" placeholder="Utiliza el saludo de acuerdo con lo establecido en el guión ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32142" id="LblG1042_C32142">Valida y registra en línea la información del cliente</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32142'])) {
                            echo $_GET['G1042_C32142'];
                        } ?>"  name="G1042_C32142" id="G1042_C32142" placeholder="Valida y registra en línea la información del cliente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32120" id="LblG1042_C32120">Hace identificación del cliente y confirmación de sus datos</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32120'])) {
                            echo $_GET['G1042_C32120'];
                        } ?>"  name="G1042_C32120" id="G1042_C32120" placeholder="Hace identificación del cliente y confirmación de sus datos">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">CIERRE</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32145" id="LblG1042_C32145">Invita al cliente a realizar encuesta de satisfacción</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32145'])) {
                            echo $_GET['G1042_C32145'];
                        } ?>"  name="G1042_C32145" id="G1042_C32145" placeholder="Invita al cliente a realizar encuesta de satisfacción">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32119" id="LblG1042_C32119">Realiza confirmación de habeas data</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32119'])) {
                            echo $_GET['G1042_C32119'];
                        } ?>"  name="G1042_C32119" id="G1042_C32119" placeholder="Realiza confirmación de habeas data">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32123" id="LblG1042_C32123">Evita utilizar muletillas, tecnisismos, diminutivos y dubitativas</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32123'])) {
                            echo $_GET['G1042_C32123'];
                        } ?>"  name="G1042_C32123" id="G1042_C32123" placeholder="Evita utilizar muletillas, tecnisismos, diminutivos y dubitativas">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32146" id="LblG1042_C32146">Se despide de acuerdo con el guión </label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32146'])) {
                            echo $_GET['G1042_C32146'];
                        } ?>"  name="G1042_C32146" id="G1042_C32146" placeholder="Se despide de acuerdo con el guión ">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32125" id="LblG1042_C32125">Utiliza protocolo de llamada en espera - Utiliza Hold</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32125'])) {
                            echo $_GET['G1042_C32125'];
                        } ?>"  name="G1042_C32125" id="G1042_C32125" placeholder="Utiliza protocolo de llamada en espera - Utiliza Hold">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">DESARROLLO DE LA LLAMADA</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">NOTA FINAL</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32129" id="LblG1042_C32129">Pregunta adecuadamente antes de dar una respuesta</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1042_C32129'])) {
                            echo $_GET['G1042_C32129'];
                        } ?>"  name="G1042_C32129" id="G1042_C32129" placeholder="Pregunta adecuadamente antes de dar una respuesta">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1042_C32114" id="LblG1042_C32114">ESTADO_CALIDAD_Q_DY</label>
                        <select disabled class="form-control input-sm select2"  style="width: 100%;" name="G1042_C32114" id="G1042_C32114">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = -3 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO DECIMAL -->
                    <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1042_C32113" id="LblG1042_C32113">CALIFICACION_Q_DY</label>
                        <input type="text" class="form-control input-sm Decimal" value="<?php if (isset($_GET['G1042_C32113'])) {
                            echo $_GET['G1042_C32113'];
                        } ?>"  name="G1042_C32113" id="G1042_C32113" placeholder="CALIFICACION_Q_DY">
                    </div>
                    <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1042_C32115" id="LblG1042_C32115">COMENTARIO_CALIDAD_Q_DY</label>
                        <textarea class="form-control input-sm" name="G1042_C32115" id="G1042_C32115"  value="<?php if (isset($_GET['G1042_C32115'])) {
                            echo $_GET['G1042_C32115'];
                        } ?>" placeholder="COMENTARIO_CALIDAD_Q_DY"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1042_C32116" id="LblG1042_C32116">COMENTARIO_AGENTE_Q_DY</label>
                        <textarea class="form-control input-sm" name="G1042_C32116" id="G1042_C32116" readonly value="<?php if (isset($_GET['G1042_C32116'])) {
                            echo $_GET['G1042_C32116'];
                        } ?>" placeholder="COMENTARIO_AGENTE_Q_DY"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
            <div class="col-md-12 col-xs-12">       
                <audio id="Abtns_1794" style="width: 100%" controls>
                    <source id="btns_1794" src="" type="audio/x-wav">
                    Your browser does not support the audio tag.
                </audio>
            </div>
        </div>

</div>

<div  id="1792" >
<h3 class="box box-title"></h3>

</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1042_C14737">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 713;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1042_C14737">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 713;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1042_C14738">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1042_C14739" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1042_C14740" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1042_C14741" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G1042/G1042_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
                //JDBD abrimos el modal de correos.
                $(".FinalizarCalificacion").click(function(){
                    $("#enviarCalificacion").modal("show");
                });

                $("#sendEmails").click(function(){
                    $("#loading").attr("hidden",false);
                    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
                    var cajaCorreos = $("#cajaCorreos").val();

                    if (cajaCorreos == null || cajaCorreos == "") {
                        cajaCorreos = "";
                    }else{
                        cajaCorreos = cajaCorreos.replace(/ /g, "");
                        cajaCorreos = cajaCorreos.replace(/,,,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,,/g, ",");
                        cajaCorreos = cajaCorreos.replace(/,,/g, ",");

                        if (cajaCorreos[0] == ",") {
                            cajaCorreos = cajaCorreos.substring(1);
                        }

                        if (cajaCorreos[cajaCorreos.length-1] == ",") {
                            cajaCorreos = cajaCorreos.substring(0,cajaCorreos.length-1);
                        }

                        var porciones = cajaCorreos.split(",");

                        for (var i = 0; i < porciones.length; i++) {
                            if (!emailRegex.test(porciones[i])) {
                                porciones.splice(i, 1);
                            }
                        }

                        cajaCorreos = porciones.join(",");
                    }

                    var formData = new FormData($("#FormularioDatos")[0]);
                    formData.append("IdGestion",$("#IdGestion").val());
                    formData.append("IdGuion",<?=$_GET["formulario"];?>);
                    formData.append("IdCal",<?=$_SESSION["IDENTIFICACION"];?>);
                    formData.append("Correos",cajaCorreos);

                    $.ajax({
                        url: "<?=$url_crud;?>?EnviarCalificacion=si",  
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data){
                            alertify.success("Calificacion enviada.");
                        },
                        error : function(){
                            alertify.error("No se pudo enviar la calificacion.");   
                        },
                        complete : function(){
                            $("#loading").attr("hidden",true);
                            $("#CerrarCalificacion").click();
                        }

                    }); 
                    
                });
                
                $("#s_1794").attr("hidden", false);
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1042_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1042_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1042_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G1042_C14728").val(item.G1042_C14728); 
                $("#G1042_C29145").val(item.G1042_C29145); 
                $("#G1042_C14808").val(item.G1042_C14808); 
                $("#G1042_C14732").val(item.G1042_C14732); 
                $("#G1042_C14731").val(item.G1042_C14731); 
                $("#G1042_C29146").val(item.G1042_C29146).trigger("change");    
                if(item.G1042_C23297 == 1){
                    $("#G1042_C23297").attr('checked', true);
                }    
                if(item.G1042_C23300 == 1){
                    $("#G1042_C23300").attr('checked', true);
                }  
                $("#G1042_C23293").val(item.G1042_C23293); 
                $("#G1042_C23294").val(item.G1042_C23294);   
                if(item.G1042_C23298 == 1){
                    $("#G1042_C23298").attr('checked', true);
                }    
                if(item.G1042_C23299 == 1){
                    $("#G1042_C23299").attr('checked', true);
                }  
                $("#G1042_C14737").val(item.G1042_C14737).trigger("change");  
                $("#G1042_C14738").val(item.G1042_C14738).trigger("change");  
                $("#G1042_C14739").val(item.G1042_C14739); 
                $("#G1042_C14740").val(item.G1042_C14740); 
                $("#G1042_C14741").val(item.G1042_C14741); 
                $("#G1042_C14742").val(item.G1042_C14742); 
                $("#G1042_C14743").val(item.G1042_C14743); 
                $("#G1042_C14744").val(item.G1042_C14744); 
                $("#G1042_C14745").val(item.G1042_C14745);   
                if(item.G1042_C32122 == 1){
                    $("#G1042_C32122").attr('checked', true);
                }  
                $("#G1042_C32124").val(item.G1042_C32124); 
                $("#G1042_C32128").val(item.G1042_C32128); 
                $("#G1042_C32130").val(item.G1042_C32130); 
                $("#G1042_C32131").val(item.G1042_C32131);   
                if(item.G1042_C32133 == 1){
                    $("#G1042_C32133").attr('checked', true);
                }  
                $("#G1042_C32134").val(item.G1042_C32134); 
                $("#G1042_C32135").val(item.G1042_C32135); 
                $("#G1042_C32136").val(item.G1042_C32136); 
                $("#G1042_C32137").val(item.G1042_C32137);   
                if(item.G1042_C32139 == 1){
                    $("#G1042_C32139").attr('checked', true);
                }  
                $("#G1042_C32140").val(item.G1042_C32140);   
                if(item.G1042_C32117 == 1){
                    $("#G1042_C32117").attr('checked', true);
                }  
                $("#G1042_C32141").val(item.G1042_C32141); 
                $("#G1042_C32118").val(item.G1042_C32118); 
                $("#G1042_C32142").val(item.G1042_C32142); 
                $("#G1042_C32120").val(item.G1042_C32120);   
                if(item.G1042_C32144 == 1){
                    $("#G1042_C32144").attr('checked', true);
                }  
                $("#G1042_C32145").val(item.G1042_C32145); 
                $("#G1042_C32119").val(item.G1042_C32119); 
                $("#G1042_C32123").val(item.G1042_C32123); 
                $("#G1042_C32146").val(item.G1042_C32146); 
                $("#G1042_C32125").val(item.G1042_C32125);   
                if(item.G1042_C32127 == 1){
                    $("#G1042_C32127").attr('checked', true);
                }    
                if(item.G1042_C32148 == 1){
                    $("#G1042_C32148").attr('checked', true);
                }  
                $("#G1042_C32129").val(item.G1042_C32129); 
                $("#G1042_C32114").val(item.G1042_C32114).trigger("change");  
                $("#G1042_C32113").val(item.G1042_C32113); 
                $("#G1042_C32115").val(item.G1042_C32115); 
                $("#G1042_C32116").val(item.G1042_C32116);   
                if(item.G1042_C14746 == 1){
                    $("#G1042_C14746").attr('checked', true);
                }    
                if(item.G1042_C14747 == 1){
                    $("#G1042_C14747").attr('checked', true);
                }    
                if(item.G1042_C14806 == 1){
                    $("#G1042_C14806").attr('checked', true);
                }    
                if(item.G1042_C23296 == 1){
                    $("#G1042_C23296").attr('checked', true);
                }  
                $("#G1042_C29142").val(item.G1042_C29142).trigger("change");  
                $("#G1042_C29143").val(item.G1042_C29143); 
                $("#G1042_C29144").val(item.G1042_C29144).trigger("change");  
                $("#G1042_C29150").val(item.G1042_C29150).trigger("change"); 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1042_C29142").select2();

    $("#G1042_C29144").select2();

    $("#G1042_C29150").select2();

    $("#G1042_C29146").select2();

    $("#G1042_C32114").select2();
        //datepickers
        

        $("#G1042_C23293").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1042_C14739").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'A la hora', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1042_C23294").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1042_C14740").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G1042_C32124").numeric();
                
        $("#G1042_C32128").numeric();
                
        $("#G1042_C32130").numeric();
                
        $("#G1042_C32131").numeric();
                
        $("#G1042_C32134").numeric();
                
        $("#G1042_C32135").numeric();
                
        $("#G1042_C32136").numeric();
                
        $("#G1042_C32137").numeric();
                
        $("#G1042_C32140").numeric();
                
        $("#G1042_C32141").numeric();
                
        $("#G1042_C32118").numeric();
                
        $("#G1042_C32142").numeric();
                
        $("#G1042_C32120").numeric();
                
        $("#G1042_C32145").numeric();
                
        $("#G1042_C32119").numeric();
                
        $("#G1042_C32123").numeric();
                
        $("#G1042_C32146").numeric();
                
        $("#G1042_C32125").numeric();
                
        $("#G1042_C32129").numeric();
                

        //Validaciones numeros Decimales
        

        $("#G1042_C32113").numeric({ decimal : ".",  negative : false, scale: 4 });
                

        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Me podría confirmar, ¿usted es el paciente? 

    $("#G1042_C29142").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Me confirma el parentesco que tiene con el paciente 

    $("#G1042_C29144").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Preguntas y Respuestas 

    $("#G1042_C29150").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1042_C29150 option:selected").attr('respuesta');
        $("#respuesta_G1042_C29150").val(respuesta);
        

    });

    //function para Me podría informar la EPS que lo remite 

    $("#G1042_C29146").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_CALIDAD_Q_DY 

    $("#G1042_C32114").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            

            if(($("#G1042_C14808").val() == "") && $("#G1042_C14808").prop("disabled") == false){
                alertify.error('Me podría informar número de documento del paciente debe ser diligenciado');
                $("#G1042_C14808").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G1042_C14732").val() == "") && $("#G1042_C14732").prop("disabled") == false){
                alertify.error('Me podría informar número de celular  del paciente debe ser diligenciado');
                $("#G1042_C14732").closest(".form-group").addClass("has-error");
                valido = 1;
            }

            if(($("#G1042_C14731").val() == "") && $("#G1042_C14731").prop("disabled") == false){
                alertify.error('Me podría informar número de teléfono fijo  del paciente debe ser diligenciado');
                $("#G1042_C14731").closest(".form-group").addClass("has-error");
                valido = 1;
            }
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1042_C14728").val(item.G1042_C14728);
 
                                                $("#G1042_C29145").val(item.G1042_C29145);
 
                                                $("#G1042_C14808").val(item.G1042_C14808);
 
                                                $("#G1042_C14732").val(item.G1042_C14732);
 
                                                $("#G1042_C14731").val(item.G1042_C14731);
 
                    $("#G1042_C29146").val(item.G1042_C29146).trigger("change"); 
      
                                                if(item.G1042_C23297 == 1){
                                                   $("#G1042_C23297").attr('checked', true);
                                                } 
      
                                                if(item.G1042_C23300 == 1){
                                                   $("#G1042_C23300").attr('checked', true);
                                                } 
 
                                                $("#G1042_C23293").val(item.G1042_C23293);
 
                                                $("#G1042_C23294").val(item.G1042_C23294);
      
                                                if(item.G1042_C23298 == 1){
                                                   $("#G1042_C23298").attr('checked', true);
                                                } 
      
                                                if(item.G1042_C23299 == 1){
                                                   $("#G1042_C23299").attr('checked', true);
                                                } 
 
                    $("#G1042_C14737").val(item.G1042_C14737).trigger("change"); 
 
                    $("#G1042_C14738").val(item.G1042_C14738).trigger("change"); 
 
                                                $("#G1042_C14739").val(item.G1042_C14739);
 
                                                $("#G1042_C14740").val(item.G1042_C14740);
 
                                                $("#G1042_C14741").val(item.G1042_C14741);
 
                                                $("#G1042_C14742").val(item.G1042_C14742);
 
                                                $("#G1042_C14743").val(item.G1042_C14743);
 
                                                $("#G1042_C14744").val(item.G1042_C14744);
 
                                                $("#G1042_C14745").val(item.G1042_C14745);
      
                                                if(item.G1042_C32122 == 1){
                                                   $("#G1042_C32122").attr('checked', true);
                                                } 
 
                                                $("#G1042_C32124").val(item.G1042_C32124);
 
                                                $("#G1042_C32128").val(item.G1042_C32128);
 
                                                $("#G1042_C32130").val(item.G1042_C32130);
 
                                                $("#G1042_C32131").val(item.G1042_C32131);
      
                                                if(item.G1042_C32133 == 1){
                                                   $("#G1042_C32133").attr('checked', true);
                                                } 
 
                                                $("#G1042_C32134").val(item.G1042_C32134);
 
                                                $("#G1042_C32135").val(item.G1042_C32135);
 
                                                $("#G1042_C32136").val(item.G1042_C32136);
 
                                                $("#G1042_C32137").val(item.G1042_C32137);
      
                                                if(item.G1042_C32139 == 1){
                                                   $("#G1042_C32139").attr('checked', true);
                                                } 
 
                                                $("#G1042_C32140").val(item.G1042_C32140);
      
                                                if(item.G1042_C32117 == 1){
                                                   $("#G1042_C32117").attr('checked', true);
                                                } 
 
                                                $("#G1042_C32141").val(item.G1042_C32141);
 
                                                $("#G1042_C32118").val(item.G1042_C32118);
 
                                                $("#G1042_C32142").val(item.G1042_C32142);
 
                                                $("#G1042_C32120").val(item.G1042_C32120);
      
                                                if(item.G1042_C32144 == 1){
                                                   $("#G1042_C32144").attr('checked', true);
                                                } 
 
                                                $("#G1042_C32145").val(item.G1042_C32145);
 
                                                $("#G1042_C32119").val(item.G1042_C32119);
 
                                                $("#G1042_C32123").val(item.G1042_C32123);
 
                                                $("#G1042_C32146").val(item.G1042_C32146);
 
                                                $("#G1042_C32125").val(item.G1042_C32125);
      
                                                if(item.G1042_C32127 == 1){
                                                   $("#G1042_C32127").attr('checked', true);
                                                } 
      
                                                if(item.G1042_C32148 == 1){
                                                   $("#G1042_C32148").attr('checked', true);
                                                } 
 
                                                $("#G1042_C32129").val(item.G1042_C32129);
 
                    $("#G1042_C32114").val(item.G1042_C32114).trigger("change"); 
 
                                                $("#G1042_C32113").val(item.G1042_C32113);
 
                                                $("#G1042_C32115").val(item.G1042_C32115);
 
                                                $("#G1042_C32116").val(item.G1042_C32116);
      
                                                if(item.G1042_C14746 == 1){
                                                   $("#G1042_C14746").attr('checked', true);
                                                } 
      
                                                if(item.G1042_C14747 == 1){
                                                   $("#G1042_C14747").attr('checked', true);
                                                } 
      
                                                if(item.G1042_C14806 == 1){
                                                   $("#G1042_C14806").attr('checked', true);
                                                } 
      
                                                if(item.G1042_C23296 == 1){
                                                   $("#G1042_C23296").attr('checked', true);
                                                } 
 
                    $("#G1042_C29142").val(item.G1042_C29142).trigger("change"); 
 
                                                $("#G1042_C29143").val(item.G1042_C29143);
 
                    $("#G1042_C29144").val(item.G1042_C29144).trigger("change"); 
 
                    $("#G1042_C29150").val(item.G1042_C29150).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Me podría informar nombre  del paciente','Me podría informar apellido del paciente','Me podría informar número de documento del paciente','Me podría informar número de celular  del paciente','Me podría informar número de teléfono fijo  del paciente','Me podría informar la EPS que lo remite','El día de su agenda es','A la hora','Agente','Fecha','Hora','Campaña','Utiliza términos de cortesía como por favor, gracias, señor, señora','Utiliza frases para clarificar lo manifestado por el cliente - Parafraseo','Proporciona la información de acuerdo con el guión','Indica al cliente pasos a seguir','Evita interrumpir al cliente y escucha activamente.','Ofrece disculpas por lo sucedido','Mantiene la llamada hasta el final','Evita acoso o agresión verbal al cliente - ( Incluye Palabras inadecuadas o aumentar el volumen de la voz)','Tipifica bajo el concepto adecuado la gestión','Evita errores e inconsistencias en la información registrada','Utiliza el saludo de acuerdo con lo establecido en el guión ','Valida y registra en línea la información del cliente','Hace identificación del cliente y confirmación de sus datos','Invita al cliente a realizar encuesta de satisfacción','Realiza confirmación de habeas data','Evita utilizar muletillas, tecnisismos, diminutivos y dubitativas','Se despide de acuerdo con el guión ','Utiliza protocolo de llamada en espera - Utiliza Hold','Pregunta adecuadamente antes de dar una respuesta','ESTADO_CALIDAD_Q_DY','CALIFICACION_Q_DY','COMENTARIO_CALIDAD_Q_DY','COMENTARIO_AGENTE_Q_DY','Me podría confirmar, ¿usted es el paciente?','Me confirma su nombre','Me confirma el parentesco que tiene con el paciente','Preguntas y Respuestas'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1042_C14728', 
                        index: 'G1042_C14728', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C29145', 
                        index: 'G1042_C29145', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C14808', 
                        index: 'G1042_C14808', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C14732', 
                        index: 'G1042_C14732', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C14731', 
                        index: 'G1042_C14731', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C29146', 
                        index:'G1042_C29146', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1333&campo=G1042_C29146'
                        }
                    }

                    ,
                    {  
                        name:'G1042_C23293', 
                        index:'G1042_C23293', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G1042_C23294', 
                        index:'G1042_C23294', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'A la hora', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1042_C14742', 
                        index: 'G1042_C14742', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C14743', 
                        index: 'G1042_C14743', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C14744', 
                        index: 'G1042_C14744', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C14745', 
                        index: 'G1042_C14745', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G1042_C32124', 
                        index:'G1042_C32124', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32128', 
                        index:'G1042_C32128', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32130', 
                        index:'G1042_C32130', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32131', 
                        index:'G1042_C32131', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32134', 
                        index:'G1042_C32134', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32135', 
                        index:'G1042_C32135', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32136', 
                        index:'G1042_C32136', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32137', 
                        index:'G1042_C32137', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32140', 
                        index:'G1042_C32140', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32141', 
                        index:'G1042_C32141', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32118', 
                        index:'G1042_C32118', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32142', 
                        index:'G1042_C32142', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32120', 
                        index:'G1042_C32120', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32145', 
                        index:'G1042_C32145', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32119', 
                        index:'G1042_C32119', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32123', 
                        index:'G1042_C32123', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32146', 
                        index:'G1042_C32146', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32125', 
                        index:'G1042_C32125', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1042_C32129', 
                        index:'G1042_C32129', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G1042_C32114', 
                        index:'G1042_C32114', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=-3&campo=G1042_C32114'
                        }
                    }

                    ,
                    {  
                        name:'G1042_C32113', 
                        index:'G1042_C32113', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G1042_C32115', 
                        index:'G1042_C32115', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C32116', 
                        index:'G1042_C32116', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C29142', 
                        index:'G1042_C29142', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=723&campo=G1042_C29142'
                        }
                    }

                    ,
                    { 
                        name:'G1042_C29143', 
                        index: 'G1042_C29143', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1042_C29144', 
                        index:'G1042_C29144', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1141&campo=G1042_C29144'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1042_C14728',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1042_C14728").val(item.G1042_C14728);

                        $("#G1042_C29145").val(item.G1042_C29145);

                        $("#G1042_C14808").val(item.G1042_C14808);

                        $("#G1042_C14732").val(item.G1042_C14732);

                        $("#G1042_C14731").val(item.G1042_C14731);
 
                    $("#G1042_C29146").val(item.G1042_C29146).trigger("change"); 
    
                        if(item.G1042_C23297 == 1){
                           $("#G1042_C23297").attr('checked', true);
                        } 
    
                        if(item.G1042_C23300 == 1){
                           $("#G1042_C23300").attr('checked', true);
                        } 

                        $("#G1042_C23293").val(item.G1042_C23293);

                        $("#G1042_C23294").val(item.G1042_C23294);
    
                        if(item.G1042_C23298 == 1){
                           $("#G1042_C23298").attr('checked', true);
                        } 
    
                        if(item.G1042_C23299 == 1){
                           $("#G1042_C23299").attr('checked', true);
                        } 
 
                    $("#G1042_C14737").val(item.G1042_C14737).trigger("change"); 
 
                    $("#G1042_C14738").val(item.G1042_C14738).trigger("change"); 

                        $("#G1042_C14739").val(item.G1042_C14739);

                        $("#G1042_C14740").val(item.G1042_C14740);

                        $("#G1042_C14741").val(item.G1042_C14741);

                        $("#G1042_C14742").val(item.G1042_C14742);

                        $("#G1042_C14743").val(item.G1042_C14743);

                        $("#G1042_C14744").val(item.G1042_C14744);

                        $("#G1042_C14745").val(item.G1042_C14745);
    
                        if(item.G1042_C32122 == 1){
                           $("#G1042_C32122").attr('checked', true);
                        } 

                        $("#G1042_C32124").val(item.G1042_C32124);

                        $("#G1042_C32128").val(item.G1042_C32128);

                        $("#G1042_C32130").val(item.G1042_C32130);

                        $("#G1042_C32131").val(item.G1042_C32131);
    
                        if(item.G1042_C32133 == 1){
                           $("#G1042_C32133").attr('checked', true);
                        } 

                        $("#G1042_C32134").val(item.G1042_C32134);

                        $("#G1042_C32135").val(item.G1042_C32135);

                        $("#G1042_C32136").val(item.G1042_C32136);

                        $("#G1042_C32137").val(item.G1042_C32137);
    
                        if(item.G1042_C32139 == 1){
                           $("#G1042_C32139").attr('checked', true);
                        } 

                        $("#G1042_C32140").val(item.G1042_C32140);
    
                        if(item.G1042_C32117 == 1){
                           $("#G1042_C32117").attr('checked', true);
                        } 

                        $("#G1042_C32141").val(item.G1042_C32141);

                        $("#G1042_C32118").val(item.G1042_C32118);

                        $("#G1042_C32142").val(item.G1042_C32142);

                        $("#G1042_C32120").val(item.G1042_C32120);
    
                        if(item.G1042_C32144 == 1){
                           $("#G1042_C32144").attr('checked', true);
                        } 

                        $("#G1042_C32145").val(item.G1042_C32145);

                        $("#G1042_C32119").val(item.G1042_C32119);

                        $("#G1042_C32123").val(item.G1042_C32123);

                        $("#G1042_C32146").val(item.G1042_C32146);

                        $("#G1042_C32125").val(item.G1042_C32125);
    
                        if(item.G1042_C32127 == 1){
                           $("#G1042_C32127").attr('checked', true);
                        } 
    
                        if(item.G1042_C32148 == 1){
                           $("#G1042_C32148").attr('checked', true);
                        } 

                        $("#G1042_C32129").val(item.G1042_C32129);
 
                    $("#G1042_C32114").val(item.G1042_C32114).trigger("change"); 

                        $("#G1042_C32113").val(item.G1042_C32113);

                        $("#G1042_C32115").val(item.G1042_C32115);

                        $("#G1042_C32116").val(item.G1042_C32116);
    
                        if(item.G1042_C14746 == 1){
                           $("#G1042_C14746").attr('checked', true);
                        } 
    
                        if(item.G1042_C14747 == 1){
                           $("#G1042_C14747").attr('checked', true);
                        } 
    
                        if(item.G1042_C14806 == 1){
                           $("#G1042_C14806").attr('checked', true);
                        } 
    
                        if(item.G1042_C23296 == 1){
                           $("#G1042_C23296").attr('checked', true);
                        } 
 
                    $("#G1042_C29142").val(item.G1042_C29142).trigger("change"); 

                        $("#G1042_C29143").val(item.G1042_C29143);
 
                    $("#G1042_C29144").val(item.G1042_C29144).trigger("change"); 
 
                    $("#G1042_C29150").val(item.G1042_C29150).trigger("change"); 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                    $.ajax({// JDBD - Obtener el link de la llamada y reproducir
                    url      : '<?=$url_crud;?>?llenarBtnLlamada=si',
                    type     : 'POST',
                    data     : {idReg : id},
                    success  : function(data){
                        var audio = $("#Abtns_1794");
                        $("#btns_1794").attr("src",data+"&streaming=true").appendTo(audio);
                        audio.load();
                    }});
                
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
