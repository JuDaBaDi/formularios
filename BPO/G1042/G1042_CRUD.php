<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1042;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1042
                  SET G1042_C32114 = -201
                  WHERE G1042_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1042 
                    WHERE G1042_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1042_C32113"]) || $gestion["G1042_C32113"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1042_C32113"];
        }

        if (is_null($gestion["G1042_C32115"]) || $gestion["G1042_C32115"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1042_C32115"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1042_FechaInsercion"]."',".$gestion["G1042_Usuario"].",'".$gestion["G1042_C".$P["P"]]."','".$gestion["G1042_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "bpo.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>Añadir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1042 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1042_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1042_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1042_LinkContenido as url FROM ".$BaseDatos.".G1042 WHERE G1042_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1042_ConsInte__b, G1042_FechaInsercion , G1042_Usuario ,  G1042_CodigoMiembro  , G1042_PoblacionOrigen , G1042_EstadoDiligenciamiento ,  G1042_IdLlamada , G1042_C14728 as principal ,G1042_C14728,G1042_C29145,G1042_C14808,G1042_C14732,G1042_C14731,G1042_C29146,G1042_C23293,G1042_C23294,G1042_C14737,G1042_C14738,G1042_C14739,G1042_C14740,G1042_C14741,G1042_C14742,G1042_C14743,G1042_C14744,G1042_C14745,G1042_C32124,G1042_C32128,G1042_C32130,G1042_C32131,G1042_C32134,G1042_C32135,G1042_C32136,G1042_C32137,G1042_C32140,G1042_C32141,G1042_C32118,G1042_C32142,G1042_C32120,G1042_C32145,G1042_C32119,G1042_C32123,G1042_C32146,G1042_C32125,G1042_C32129,G1042_C32114,G1042_C32113,G1042_C32115,G1042_C32116,G1042_C29142,G1042_C29143,G1042_C29144,G1042_C29150 FROM '.$BaseDatos.'.G1042 WHERE G1042_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1042_C14728'] = $key->G1042_C14728;

                $datos[$i]['G1042_C29145'] = $key->G1042_C29145;

                $datos[$i]['G1042_C14808'] = $key->G1042_C14808;

                $datos[$i]['G1042_C14732'] = $key->G1042_C14732;

                $datos[$i]['G1042_C14731'] = $key->G1042_C14731;

                $datos[$i]['G1042_C29146'] = $key->G1042_C29146;

                $datos[$i]['G1042_C23293'] = explode(' ', $key->G1042_C23293)[0];
  
                $hora = '';
                if(!is_null($key->G1042_C23294)){
                    $hora = explode(' ', $key->G1042_C23294)[1];
                }

                $datos[$i]['G1042_C23294'] = $hora;

                $datos[$i]['G1042_C14737'] = $key->G1042_C14737;

                $datos[$i]['G1042_C14738'] = $key->G1042_C14738;

                $datos[$i]['G1042_C14739'] = explode(' ', $key->G1042_C14739)[0];
  
                $hora = '';
                if(!is_null($key->G1042_C14740)){
                    $hora = explode(' ', $key->G1042_C14740)[1];
                }

                $datos[$i]['G1042_C14740'] = $hora;

                $datos[$i]['G1042_C14741'] = $key->G1042_C14741;

                $datos[$i]['G1042_C14742'] = $key->G1042_C14742;

                $datos[$i]['G1042_C14743'] = $key->G1042_C14743;

                $datos[$i]['G1042_C14744'] = $key->G1042_C14744;

                $datos[$i]['G1042_C14745'] = $key->G1042_C14745;

                $datos[$i]['G1042_C32124'] = $key->G1042_C32124;

                $datos[$i]['G1042_C32128'] = $key->G1042_C32128;

                $datos[$i]['G1042_C32130'] = $key->G1042_C32130;

                $datos[$i]['G1042_C32131'] = $key->G1042_C32131;

                $datos[$i]['G1042_C32134'] = $key->G1042_C32134;

                $datos[$i]['G1042_C32135'] = $key->G1042_C32135;

                $datos[$i]['G1042_C32136'] = $key->G1042_C32136;

                $datos[$i]['G1042_C32137'] = $key->G1042_C32137;

                $datos[$i]['G1042_C32140'] = $key->G1042_C32140;

                $datos[$i]['G1042_C32141'] = $key->G1042_C32141;

                $datos[$i]['G1042_C32118'] = $key->G1042_C32118;

                $datos[$i]['G1042_C32142'] = $key->G1042_C32142;

                $datos[$i]['G1042_C32120'] = $key->G1042_C32120;

                $datos[$i]['G1042_C32145'] = $key->G1042_C32145;

                $datos[$i]['G1042_C32119'] = $key->G1042_C32119;

                $datos[$i]['G1042_C32123'] = $key->G1042_C32123;

                $datos[$i]['G1042_C32146'] = $key->G1042_C32146;

                $datos[$i]['G1042_C32125'] = $key->G1042_C32125;

                $datos[$i]['G1042_C32129'] = $key->G1042_C32129;

                $datos[$i]['G1042_C32114'] = $key->G1042_C32114;

                $datos[$i]['G1042_C32113'] = $key->G1042_C32113;

                $datos[$i]['G1042_C32115'] = $key->G1042_C32115;

                $datos[$i]['G1042_C32116'] = $key->G1042_C32116;

                $datos[$i]['G1042_C29142'] = $key->G1042_C29142;

                $datos[$i]['G1042_C29143'] = $key->G1042_C29143;

                $datos[$i]['G1042_C29144'] = $key->G1042_C29144;

                $datos[$i]['G1042_C29150'] = $key->G1042_C29150;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1042";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1042_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1042_ConsInte__b as id,  G1042_C14728 as camp1 , G1042_C14808 as camp2 
                     FROM ".$BaseDatos.".G1042  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1042_ConsInte__b as id,  G1042_C14728 as camp1 , G1042_C14808 as camp2  
                    FROM ".$BaseDatos.".G1042  JOIN ".$BaseDatos.".G1042_M".$_POST['muestra']." ON G1042_ConsInte__b = G1042_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1042_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1042_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1042_C14728 LIKE '%".$B."%' OR G1042_C14808 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1042_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1042");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1042_ConsInte__b, G1042_FechaInsercion , G1042_Usuario ,  G1042_CodigoMiembro  , G1042_PoblacionOrigen , G1042_EstadoDiligenciamiento ,  G1042_IdLlamada , G1042_C14728 as principal ,G1042_C14728,G1042_C29145,G1042_C14808,G1042_C14732,G1042_C14731, a.LISOPC_Nombre____b as G1042_C29146,G1042_C23293,G1042_C23294, b.LISOPC_Nombre____b as G1042_C14737, c.LISOPC_Nombre____b as G1042_C14738,G1042_C14739,G1042_C14740,G1042_C14741,G1042_C14742,G1042_C14743,G1042_C14744,G1042_C14745,G1042_C32124,G1042_C32128,G1042_C32130,G1042_C32131,G1042_C32134,G1042_C32135,G1042_C32136,G1042_C32137,G1042_C32140,G1042_C32141,G1042_C32118,G1042_C32142,G1042_C32120,G1042_C32145,G1042_C32119,G1042_C32123,G1042_C32146,G1042_C32125,G1042_C32129, d.LISOPC_Nombre____b as G1042_C32114,G1042_C32113,G1042_C32115,G1042_C32116, e.LISOPC_Nombre____b as G1042_C29142,G1042_C29143, f.LISOPC_Nombre____b as G1042_C29144,G1042_C29150 FROM '.$BaseDatos.'.G1042 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1042_C29146 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1042_C14737 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1042_C14738 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1042_C32114 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1042_C29142 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1042_C29144';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1042_C14740)){
                    $hora_a = explode(' ', $fila->G1042_C14740)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1042_C23294)){
                    $hora_b = explode(' ', $fila->G1042_C23294)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1042_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1042_ConsInte__b , ($fila->G1042_C14728) , ($fila->G1042_C29145) , ($fila->G1042_C14808) , ($fila->G1042_C14732) , ($fila->G1042_C14731) , ($fila->G1042_C29146) , explode(' ', $fila->G1042_C23293)[0] , $hora_a , ($fila->G1042_C14737) , ($fila->G1042_C14738) , explode(' ', $fila->G1042_C14739)[0] , $hora_b , ($fila->G1042_C14741) , ($fila->G1042_C14742) , ($fila->G1042_C14743) , ($fila->G1042_C14744) , ($fila->G1042_C14745) , ($fila->G1042_C32124) , ($fila->G1042_C32128) , ($fila->G1042_C32130) , ($fila->G1042_C32131) , ($fila->G1042_C32134) , ($fila->G1042_C32135) , ($fila->G1042_C32136) , ($fila->G1042_C32137) , ($fila->G1042_C32140) , ($fila->G1042_C32141) , ($fila->G1042_C32118) , ($fila->G1042_C32142) , ($fila->G1042_C32120) , ($fila->G1042_C32145) , ($fila->G1042_C32119) , ($fila->G1042_C32123) , ($fila->G1042_C32146) , ($fila->G1042_C32125) , ($fila->G1042_C32129) , ($fila->G1042_C32114) , ($fila->G1042_C32113) , ($fila->G1042_C32115) , ($fila->G1042_C32116) , ($fila->G1042_C29142) , ($fila->G1042_C29143) , ($fila->G1042_C29144) , ($fila->G1042_C29150) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1042 WHERE G1042_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1042";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1042_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1042_ConsInte__b as id,  G1042_C14728 as camp1 , G1042_C14808 as camp2  FROM '.$BaseDatos.'.G1042 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1042_ConsInte__b as id,  G1042_C14728 as camp1 , G1042_C14808 as camp2  
                    FROM ".$BaseDatos.".G1042  JOIN ".$BaseDatos.".G1042_M".$_POST['muestra']." ON G1042_ConsInte__b = G1042_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1042_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1042_C14728 LIKE "%'.$B.'%" OR G1042_C14808 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1042_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1042 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1042(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1042_C14728"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14728 = '".$_POST["G1042_C14728"]."'";
                $LsqlI .= $separador."G1042_C14728";
                $LsqlV .= $separador."'".$_POST["G1042_C14728"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C29145"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C29145 = '".$_POST["G1042_C29145"]."'";
                $LsqlI .= $separador."G1042_C29145";
                $LsqlV .= $separador."'".$_POST["G1042_C29145"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14808"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14808 = '".$_POST["G1042_C14808"]."'";
                $LsqlI .= $separador."G1042_C14808";
                $LsqlV .= $separador."'".$_POST["G1042_C14808"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14732"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14732 = '".$_POST["G1042_C14732"]."'";
                $LsqlI .= $separador."G1042_C14732";
                $LsqlV .= $separador."'".$_POST["G1042_C14732"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14731"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14731 = '".$_POST["G1042_C14731"]."'";
                $LsqlI .= $separador."G1042_C14731";
                $LsqlV .= $separador."'".$_POST["G1042_C14731"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C29146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C29146 = '".$_POST["G1042_C29146"]."'";
                $LsqlI .= $separador."G1042_C29146";
                $LsqlV .= $separador."'".$_POST["G1042_C29146"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C23297"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C23297 = '".$_POST["G1042_C23297"]."'";
                $LsqlI .= $separador."G1042_C23297";
                $LsqlV .= $separador."'".$_POST["G1042_C23297"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C23300"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C23300 = '".$_POST["G1042_C23300"]."'";
                $LsqlI .= $separador."G1042_C23300";
                $LsqlV .= $separador."'".$_POST["G1042_C23300"]."'";
                $validar = 1;
            }
             
 
            $G1042_C23293 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1042_C23293"])){    
                if($_POST["G1042_C23293"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1042_C23293"]);
                    if(count($tieneHora) > 1){
                        $G1042_C23293 = "'".$_POST["G1042_C23293"]."'";
                    }else{
                        $G1042_C23293 = "'".str_replace(' ', '',$_POST["G1042_C23293"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1042_C23293 = ".$G1042_C23293;
                    $LsqlI .= $separador." G1042_C23293";
                    $LsqlV .= $separador.$G1042_C23293;
                    $validar = 1;
                }
            }
  
            $G1042_C23294 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1042_C23294"])){   
                if($_POST["G1042_C23294"] != '' && $_POST["G1042_C23294"] != 'undefined' && $_POST["G1042_C23294"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C23294 = "'".$fecha." ".str_replace(' ', '',$_POST["G1042_C23294"])."'";
                    $LsqlU .= $separador." G1042_C23294 = ".$G1042_C23294."";
                    $LsqlI .= $separador." G1042_C23294";
                    $LsqlV .= $separador.$G1042_C23294;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C23298"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C23298 = '".$_POST["G1042_C23298"]."'";
                $LsqlI .= $separador."G1042_C23298";
                $LsqlV .= $separador."'".$_POST["G1042_C23298"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C23299"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C23299 = '".$_POST["G1042_C23299"]."'";
                $LsqlI .= $separador."G1042_C23299";
                $LsqlV .= $separador."'".$_POST["G1042_C23299"]."'";
                $validar = 1;
            }
             
 
            $G1042_C14737 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1042_C14737 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1042_C14737 = ".$G1042_C14737;
                    $LsqlI .= $separador." G1042_C14737";
                    $LsqlV .= $separador.$G1042_C14737;
                    $validar = 1;

                    
                }
            }
 
            $G1042_C14738 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1042_C14738 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1042_C14738 = ".$G1042_C14738;
                    $LsqlI .= $separador." G1042_C14738";
                    $LsqlV .= $separador.$G1042_C14738;
                    $validar = 1;
                }
            }
 
            $G1042_C14739 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1042_C14739 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1042_C14739 = ".$G1042_C14739;
                    $LsqlI .= $separador." G1042_C14739";
                    $LsqlV .= $separador.$G1042_C14739;
                    $validar = 1;
                }
            }
 
            $G1042_C14740 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1042_C14740 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1042_C14740 = ".$G1042_C14740;
                    $LsqlI .= $separador." G1042_C14740";
                    $LsqlV .= $separador.$G1042_C14740;
                    $validar = 1;
                }
            }
 
            $G1042_C14741 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1042_C14741 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1042_C14741 = ".$G1042_C14741;
                    $LsqlI .= $separador." G1042_C14741";
                    $LsqlV .= $separador.$G1042_C14741;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C14742"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14742 = '".$_POST["G1042_C14742"]."'";
                $LsqlI .= $separador."G1042_C14742";
                $LsqlV .= $separador."'".$_POST["G1042_C14742"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14743"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14743 = '".$_POST["G1042_C14743"]."'";
                $LsqlI .= $separador."G1042_C14743";
                $LsqlV .= $separador."'".$_POST["G1042_C14743"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14744"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14744 = '".$_POST["G1042_C14744"]."'";
                $LsqlI .= $separador."G1042_C14744";
                $LsqlV .= $separador."'".$_POST["G1042_C14744"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14745"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14745 = '".$_POST["G1042_C14745"]."'";
                $LsqlI .= $separador."G1042_C14745";
                $LsqlV .= $separador."'".$_POST["G1042_C14745"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C32122"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32122 = '".$_POST["G1042_C32122"]."'";
                $LsqlI .= $separador."G1042_C32122";
                $LsqlV .= $separador."'".$_POST["G1042_C32122"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32124 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32124"])){
                if($_POST["G1042_C32124"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32124 = $_POST["G1042_C32124"];
                    $LsqlU .= $separador." G1042_C32124 = ".$G1042_C32124."";
                    $LsqlI .= $separador." G1042_C32124";
                    $LsqlV .= $separador.$G1042_C32124;
                    $validar = 1;
                }
            }
  
            $G1042_C32128 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32128"])){
                if($_POST["G1042_C32128"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32128 = $_POST["G1042_C32128"];
                    $LsqlU .= $separador." G1042_C32128 = ".$G1042_C32128."";
                    $LsqlI .= $separador." G1042_C32128";
                    $LsqlV .= $separador.$G1042_C32128;
                    $validar = 1;
                }
            }
  
            $G1042_C32130 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32130"])){
                if($_POST["G1042_C32130"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32130 = $_POST["G1042_C32130"];
                    $LsqlU .= $separador." G1042_C32130 = ".$G1042_C32130."";
                    $LsqlI .= $separador." G1042_C32130";
                    $LsqlV .= $separador.$G1042_C32130;
                    $validar = 1;
                }
            }
  
            $G1042_C32131 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32131"])){
                if($_POST["G1042_C32131"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32131 = $_POST["G1042_C32131"];
                    $LsqlU .= $separador." G1042_C32131 = ".$G1042_C32131."";
                    $LsqlI .= $separador." G1042_C32131";
                    $LsqlV .= $separador.$G1042_C32131;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32133"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32133 = '".$_POST["G1042_C32133"]."'";
                $LsqlI .= $separador."G1042_C32133";
                $LsqlV .= $separador."'".$_POST["G1042_C32133"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32134 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32134"])){
                if($_POST["G1042_C32134"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32134 = $_POST["G1042_C32134"];
                    $LsqlU .= $separador." G1042_C32134 = ".$G1042_C32134."";
                    $LsqlI .= $separador." G1042_C32134";
                    $LsqlV .= $separador.$G1042_C32134;
                    $validar = 1;
                }
            }
  
            $G1042_C32135 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32135"])){
                if($_POST["G1042_C32135"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32135 = $_POST["G1042_C32135"];
                    $LsqlU .= $separador." G1042_C32135 = ".$G1042_C32135."";
                    $LsqlI .= $separador." G1042_C32135";
                    $LsqlV .= $separador.$G1042_C32135;
                    $validar = 1;
                }
            }
  
            $G1042_C32136 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32136"])){
                if($_POST["G1042_C32136"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32136 = $_POST["G1042_C32136"];
                    $LsqlU .= $separador." G1042_C32136 = ".$G1042_C32136."";
                    $LsqlI .= $separador." G1042_C32136";
                    $LsqlV .= $separador.$G1042_C32136;
                    $validar = 1;
                }
            }
  
            $G1042_C32137 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32137"])){
                if($_POST["G1042_C32137"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32137 = $_POST["G1042_C32137"];
                    $LsqlU .= $separador." G1042_C32137 = ".$G1042_C32137."";
                    $LsqlI .= $separador." G1042_C32137";
                    $LsqlV .= $separador.$G1042_C32137;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32139 = '".$_POST["G1042_C32139"]."'";
                $LsqlI .= $separador."G1042_C32139";
                $LsqlV .= $separador."'".$_POST["G1042_C32139"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32140 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32140"])){
                if($_POST["G1042_C32140"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32140 = $_POST["G1042_C32140"];
                    $LsqlU .= $separador." G1042_C32140 = ".$G1042_C32140."";
                    $LsqlI .= $separador." G1042_C32140";
                    $LsqlV .= $separador.$G1042_C32140;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32117"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32117 = '".$_POST["G1042_C32117"]."'";
                $LsqlI .= $separador."G1042_C32117";
                $LsqlV .= $separador."'".$_POST["G1042_C32117"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32141 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32141"])){
                if($_POST["G1042_C32141"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32141 = $_POST["G1042_C32141"];
                    $LsqlU .= $separador." G1042_C32141 = ".$G1042_C32141."";
                    $LsqlI .= $separador." G1042_C32141";
                    $LsqlV .= $separador.$G1042_C32141;
                    $validar = 1;
                }
            }
  
            $G1042_C32118 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32118"])){
                if($_POST["G1042_C32118"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32118 = $_POST["G1042_C32118"];
                    $LsqlU .= $separador." G1042_C32118 = ".$G1042_C32118."";
                    $LsqlI .= $separador." G1042_C32118";
                    $LsqlV .= $separador.$G1042_C32118;
                    $validar = 1;
                }
            }
  
            $G1042_C32142 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32142"])){
                if($_POST["G1042_C32142"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32142 = $_POST["G1042_C32142"];
                    $LsqlU .= $separador." G1042_C32142 = ".$G1042_C32142."";
                    $LsqlI .= $separador." G1042_C32142";
                    $LsqlV .= $separador.$G1042_C32142;
                    $validar = 1;
                }
            }
  
            $G1042_C32120 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32120"])){
                if($_POST["G1042_C32120"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32120 = $_POST["G1042_C32120"];
                    $LsqlU .= $separador." G1042_C32120 = ".$G1042_C32120."";
                    $LsqlI .= $separador." G1042_C32120";
                    $LsqlV .= $separador.$G1042_C32120;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32144 = '".$_POST["G1042_C32144"]."'";
                $LsqlI .= $separador."G1042_C32144";
                $LsqlV .= $separador."'".$_POST["G1042_C32144"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32145 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32145"])){
                if($_POST["G1042_C32145"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32145 = $_POST["G1042_C32145"];
                    $LsqlU .= $separador." G1042_C32145 = ".$G1042_C32145."";
                    $LsqlI .= $separador." G1042_C32145";
                    $LsqlV .= $separador.$G1042_C32145;
                    $validar = 1;
                }
            }
  
            $G1042_C32119 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32119"])){
                if($_POST["G1042_C32119"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32119 = $_POST["G1042_C32119"];
                    $LsqlU .= $separador." G1042_C32119 = ".$G1042_C32119."";
                    $LsqlI .= $separador." G1042_C32119";
                    $LsqlV .= $separador.$G1042_C32119;
                    $validar = 1;
                }
            }
  
            $G1042_C32123 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32123"])){
                if($_POST["G1042_C32123"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32123 = $_POST["G1042_C32123"];
                    $LsqlU .= $separador." G1042_C32123 = ".$G1042_C32123."";
                    $LsqlI .= $separador." G1042_C32123";
                    $LsqlV .= $separador.$G1042_C32123;
                    $validar = 1;
                }
            }
  
            $G1042_C32146 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32146"])){
                if($_POST["G1042_C32146"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32146 = $_POST["G1042_C32146"];
                    $LsqlU .= $separador." G1042_C32146 = ".$G1042_C32146."";
                    $LsqlI .= $separador." G1042_C32146";
                    $LsqlV .= $separador.$G1042_C32146;
                    $validar = 1;
                }
            }
  
            $G1042_C32125 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32125"])){
                if($_POST["G1042_C32125"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32125 = $_POST["G1042_C32125"];
                    $LsqlU .= $separador." G1042_C32125 = ".$G1042_C32125."";
                    $LsqlI .= $separador." G1042_C32125";
                    $LsqlV .= $separador.$G1042_C32125;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32127"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32127 = '".$_POST["G1042_C32127"]."'";
                $LsqlI .= $separador."G1042_C32127";
                $LsqlV .= $separador."'".$_POST["G1042_C32127"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C32148"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32148 = '".$_POST["G1042_C32148"]."'";
                $LsqlI .= $separador."G1042_C32148";
                $LsqlV .= $separador."'".$_POST["G1042_C32148"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32129 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32129"])){
                if($_POST["G1042_C32129"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32129 = $_POST["G1042_C32129"];
                    $LsqlU .= $separador." G1042_C32129 = ".$G1042_C32129."";
                    $LsqlI .= $separador." G1042_C32129";
                    $LsqlV .= $separador.$G1042_C32129;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32114"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32114 = '".$_POST["G1042_C32114"]."'";
                $LsqlI .= $separador."G1042_C32114";
                $LsqlV .= $separador."'".$_POST["G1042_C32114"]."'";
                $validar = 1;
            }
             
  
            $G1042_C32113 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1042_C32113"])){
                if($_POST["G1042_C32113"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1042_C32113 = $_POST["G1042_C32113"];
                    $LsqlU .= $separador." G1042_C32113 = ".$G1042_C32113."";
                    $LsqlI .= $separador." G1042_C32113";
                    $LsqlV .= $separador.$G1042_C32113;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1042_C32115"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32115 = '".$_POST["G1042_C32115"]."'";
                $LsqlI .= $separador."G1042_C32115";
                $LsqlV .= $separador."'".$_POST["G1042_C32115"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C32116"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C32116 = '".$_POST["G1042_C32116"]."'";
                $LsqlI .= $separador."G1042_C32116";
                $LsqlV .= $separador."'".$_POST["G1042_C32116"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14746"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14746 = '".$_POST["G1042_C14746"]."'";
                $LsqlI .= $separador."G1042_C14746";
                $LsqlV .= $separador."'".$_POST["G1042_C14746"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14747"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14747 = '".$_POST["G1042_C14747"]."'";
                $LsqlI .= $separador."G1042_C14747";
                $LsqlV .= $separador."'".$_POST["G1042_C14747"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C14806"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C14806 = '".$_POST["G1042_C14806"]."'";
                $LsqlI .= $separador."G1042_C14806";
                $LsqlV .= $separador."'".$_POST["G1042_C14806"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C23296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C23296 = '".$_POST["G1042_C23296"]."'";
                $LsqlI .= $separador."G1042_C23296";
                $LsqlV .= $separador."'".$_POST["G1042_C23296"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C29142"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C29142 = '".$_POST["G1042_C29142"]."'";
                $LsqlI .= $separador."G1042_C29142";
                $LsqlV .= $separador."'".$_POST["G1042_C29142"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C29143"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C29143 = '".$_POST["G1042_C29143"]."'";
                $LsqlI .= $separador."G1042_C29143";
                $LsqlV .= $separador."'".$_POST["G1042_C29143"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C29144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C29144 = '".$_POST["G1042_C29144"]."'";
                $LsqlI .= $separador."G1042_C29144";
                $LsqlV .= $separador."'".$_POST["G1042_C29144"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1042_C29150"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_C29150 = '".$_POST["G1042_C29150"]."'";
                $LsqlI .= $separador."G1042_C29150";
                $LsqlV .= $separador."'".$_POST["G1042_C29150"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1042_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1042_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1042_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1042_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1042_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1042_Usuario , G1042_FechaInsercion, G1042_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1042_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1042 WHERE G1042_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
