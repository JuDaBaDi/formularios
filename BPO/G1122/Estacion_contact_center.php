
<?php

-	session_start();
	include("funciones.php");
	include("conexion.php");
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	date_default_timezone_set('America/Bogota');
    if(!isset($_SESSION['LOGIN_OK']) && !isset($_GET['token'])){
        header('Location: login.php');
    }

    $http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
    
    
    

    $token = null;


    if(isset($_GET['token']) && $_GET['token'] != '' && $_GET['token'] != null){
        $token  = $_GET['token'];
        $userid = getIdToken($token);
        $tokenSql = "SELECT * FROM ".$BaseDatos_systema.".SESSIONS WHERE SESSIONS__USUARI_ConsInte__b = ". $userid ." AND SESSIONS__Token = '".$token."' AND SESSIONS__Estado__b = 1 ";
        $query = $mysqli->query($tokenSql) or trigger_error($mysqli->error." [$tokenSql]"); ;
        if($query->num_rows > 0) {

        }else{
            header('Location: message.php?token=false');
        }
    }else{
    	header('Location: message.php?token=false');
    }

    $tiempoDesdeInicio = date('Y-m-d H:i:s');









?>
<!DOCTYPE html>
<html>
	<head>
		<title>Estación contact center</title>
		<meta charset="utf-8">
		<meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<META HTTP-EQUIV="Access-Control-Allow-Origin" CONTENT="*">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<!-- Date Picker -->
		<link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="assets/css/alertify.core.css">
        <link rel="stylesheet" href="assets/css/alertify.default.css">
        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
        <link rel="stylesheet" href="assets/plugins/sweetalert/sweetalert.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="assets/Guriddo_jqGrid_/css/ui.jqgrid-bootstrap.css" />

        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
		<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
		<style type="text/css">
            [class^='select2'] {
                border-radius: 0px !important;
            }

            .modal-lg {
                width: 1200px;
            }
        </style>
        <script type="text/javascript">
            function bindEvent(element, eventName, eventHandler) {
                if (element.addEventListener) {
                    element.addEventListener(eventName, eventHandler, false);
                } else if (element.attachEvent) {
                    element.attachEvent('on' + eventName, eventHandler);
                }
            }


            // Listen to message from child window
            bindEvent(window, 'message', function (e) {
                console.log(e.data);
                window.parent.postMessage(e.data, '*');
            });         

        	if (window.addEventListener) {
			    // For standards-compliant web browsers
			    window.addEventListener("message", displayMessage, false);
			    		       
			}
			else {
			    window.attachEvent("onmessage", displayMessage);
			   
			}

		    
		    function displayMessage(e){
		    	
		    	<?php
		    		if(isset($_GET['campana_crm'])){
		    			$LsqL = "SELECT * FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET['campana_crm'];
		    		}else{
		    			$LsqL = "SELECT * FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET['id_campana_crm'];
		    		}
		    		
		    		$res = $mysqli->query($LsqL);
		    		$rs = $res->fetch_array();
		    		$url_crud = "formularios/G".$rs['CAMPAN_ConsInte__GUION__Gui_b']."/G".$rs['CAMPAN_ConsInte__GUION__Gui_b']."_CRUD.php";

		    	?>
		        if(e.data == 'FinalizacionDesdeCBXBLEND'){
		        	var meses = new Array(12);
			    	meses[0] = "01";
			    	meses[1] = "02";
			    	meses[2] = "03";
			    	meses[3] = "04";
			    	meses[4] = "05";
			    	meses[5] = "06";
			    	meses[6] = "07";
			    	meses[7] = "08";
			    	meses[8] = "09";
			    	meses[9] = "10";
			    	meses[10] = "11";
			    	meses[11] = "12";
		        	//var bol_respuesta = before_save();
		        	var d = new Date();
                    var n = new Date();
                    n.setFullYear(n.getFullYear() -10);
		            var h = d.getHours();
		            var horas = (h < 10) ? '0' + h : h;
		            var dia = d.getDate();
		            var dias = (dia < 10) ? '0' + dia : dia;
		            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
		            $("#FechaFinal").val(fechaFinal);
                    var fechaAgenda= n.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias;
		            var valido = 0;



		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
                    
                    var formData=new FormData($("#frameContenedor").contents().find("#FormularioDatos")[0]);
		            formData.append('Efectividad', 0);
		            formData.append('MonoEf', -1);
		            formData.append('TipNoEF', 2);
		            formData.append('FechaInicio', '<?php echo $tiempoDesdeInicio;?>');
		            formData.append('FechaFinal', fechaFinal);
		            formData.append('MonoEfPeso', 100);
		            formData.append('ContactoMonoEf', 5);
		            formData.append('reintento', 2);
                    var tipificacion=formData.get("tipificacion");
                    if(tipificacion == 0 || tipificacion == null){
                        formData.append('tipificacion', -1);
                    }
		            formData.append('TxtFechaReintento', fechaAgenda);
		            formData.append('TxtHoraReintento', '08:00:00');
		            formData.append('textAreaComentarios', 'Registro cerrado por Blend');
		            formData.append('hidId', '<?php if(isset($_GET['consinte'])) { echo $_GET["consinte"]; }else{ echo "0";  } ?>');
		            formData.append('oper', 'add');
		            formData.append('id_gestion_cbx', '<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET['id_gestion_cbx']; }?>');
		            formData.append('cbx_sentido', '<?php if(isset($_GET['sentido'])){ echo $_GET['sentido']; }else{ echo "0"; } ?>');
		            formData.append('cbx_canal__', '<?php if(isset($_GET['id_gestion_cbx'])) { echo explode('_', $_GET['id_gestion_cbx'])[0]; }else{ echo "0"; } ?>');
		            formData.append('Padre', 0);

		            formData.append('datoContacto', '<?php if(isset($_GET['ani'])){ echo $_GET['ani']; }else{ echo "0"; } ?>');


		            $.ajax({
						url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['consinte'])) { echo $_GET["consinte"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&LlamadoExterno=si',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){

		                    if(data){
	                        	$.ajax({
	                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['consinte'])) { echo $_GET["consinte"]; }else{ echo "-1";  } ?>&ConsInteRegresado='+ data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['id_campana_crm'])){ echo $_GET['id_campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarForzado=true',
	                        		type  : "post",
	                        		data  : formData,
	                    		 	cache: false,
				                    contentType: false,
				                    processData: false,
	                        		success : function(xt){
	                        			console.log("Esta gestión fue cerrada por Blend hora de cierre <?php echo date('Y-m-d H:i:s');?>");
	                        			window.location.href = "quitar.php";;
	                        		}
	                        	});
				                        	           
		                    }               
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		
		        }else if(e.data == 'FinalizacionDesdeCBXTIMEOUT'){
		        		var meses = new Array(12);
				    	meses[0] = "01";
				    	meses[1] = "02";
				    	meses[2] = "03";
				    	meses[3] = "04";
				    	meses[4] = "05";
				    	meses[5] = "06";
				    	meses[6] = "07";
				    	meses[7] = "08";
				    	meses[8] = "09";
				    	meses[9] = "10";
				    	meses[10] = "11";
				    	meses[11] = "12";
			        	//var bol_respuesta = before_save();
			        	var d = new Date();
			            var h = d.getHours();
			            var horas = (h < 10) ? '0' + h : h;
			            var dia = d.getDate();
			            var dias = (dia < 10) ? '0' + dia : dia;
			            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();

			           


			            var form = $("#FormularioDatos");
			            //Se crean un array con los datos a enviar, apartir del formulario 
			            //var formData = new FormData($("#FormularioDatos")[0]);

			            var formData=new FormData($("#frameContenedor").contents().find("#FormularioDatos")[0]);
			           


			            formData.append('Efectividad', 0);
			            formData.append('MonoEf', -2);
			            formData.append('TipNoEF', 1);
			            formData.append('FechaInicio', '<?php echo $tiempoDesdeInicio;?>');
			            formData.append('FechaFinal', fechaFinal);
			            formData.append('MonoEfPeso', 100);
			            formData.append('ContactoMonoEf', 4);
			            formData.append('reintento', 1);
                        var tipificacion=formData.get("tipificacion");
                        if(tipificacion == 0 || tipificacion == null){
                            formData.append('tipificacion', -2);
                        }
			            formData.append('TxtFechaReintento', '');
			            formData.append('TxtHoraReintento', '');
			            formData.append('textAreaComentarios', 'Registro cerrado por TimeOut');
			            formData.append('hidId', '<?php if(isset($_GET['consinte'])) { echo $_GET["consinte"]; }else{ echo "-1";  } ?>');
			            formData.append('oper', 'add');
			            formData.append('id_gestion_cbx', '<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET['id_gestion_cbx']; }?>');
			            formData.append('cbx_sentido', '<?php if(isset($_GET['sentido'])){ echo $_GET['sentido']; }else{ echo "0"; } ?>');
			            formData.append('cbx_canal__', '<?php if(isset($_GET['id_gestion_cbx'])) { echo explode('_', $_GET['id_gestion_cbx'])[0]; }else{ echo "0"; } ?>');
			            formData.append('Padre', 0);
			            formData.append('datoContacto', '<?php if(isset($_GET['ani'])){ echo $_GET['ani']; }else{ echo "0"; } ?>');
			
			            $.ajax({
			            	url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['consinte'])) { echo $_GET["consinte"]; }else{ echo "-1";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&LlamadoExterno=si', 

			                type: 'POST',
			                data: formData,
			                cache: false,
			                contentType: false,
			                processData: false,
			                //una vez finalizado correctamente
			                success: function(data){
			                	
			                    if(data){
		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['consinte'])) { echo $_GET["consinte"]; }else{ echo "-1";  } ?>&ConsInteRegresado='+ data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['id_campana_crm'])){ echo $_GET['id_campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarForzado=true',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
					                        	           
			                    }               
			                },
			                //si ha ocurrido un error
			                error: function(){
			                    after_save_error();
			                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
			                }
			            });
		        	
		        
		        }else{
		        	eval(e.data);
		        }
		    }

		    /**YCR 2019-10-24
			*Funcion que cambia el Id de la llamada
		    */
		    function cambiarUniqueId(id){
		    	$("#idLlamada").val(id);
		    	$("#main #frameContenedor").contents().find("#idLlamada").val(id);
		    }
		    /*YCR 2019-10-21
			*Function que tipifica despues de presinar el boton verde del telefono
		    **/
		    function guardarGestionParaNuevaLlamada(intIdLisopc_p,intIdMonoef_p,intTipoReintento_p,datFechaAgenda_p,datHoraAgenda_p,strObservaciones_p){

		    	var intIdLisopc_t =  traerIdLisopc(intIdMonoef_p);

		    	var codigoMiembro = $("#main #frameContenedor").contents().find("#codigoMiembro").val();
		    	var tiempoInicio = $("#main #frameContenedor").contents().find("#tiempoInicio").val();
		    	var fechaAgenda ='';
		    	var horaAgenda = '';
		    	var observaciones = '';
		    	
		    	if(intTipoReintento_p == '2'){
		    		fechaAgenda = datFechaAgenda_p;
		    		horaAgenda = datHoraAgenda_p;
		    	}
		    	
		    	if(strObservaciones_p != '' && strObservaciones_p != null){
		    		observaciones = strObservaciones_p;
		    	}
		    	
		    	var meses = new Array(12);
		    	meses[0] = "01";
		    	meses[1] = "02";
		    	meses[2] = "03";
		    	meses[3] = "04";
		    	meses[4] = "05";
		    	meses[5] = "06";
		    	meses[6] = "07";
		    	meses[7] = "08";
		    	meses[8] = "09";
		    	meses[9] = "10";
		    	meses[10] = "11";
		    	meses[11] = "12";
	        	var d = new Date();
	            var h = d.getHours();
	            var horas = (h < 10) ? '0' + h : h;
	            var dia = d.getDate();
	            var dias = (dia < 10) ? '0' + dia : dia;
	            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();

	            var formData=new FormData($("#frameContenedor").contents().find("#FormularioDatos")[0]);
	            formData.append('Efectividad', 0);
	            formData.append('MonoEf', intIdMonoef_p);
	            formData.append('TipNoEF', intTipoReintento_p);
	            formData.append('FechaInicio', '<?php echo $tiempoDesdeInicio;?>');
	            formData.append('FechaFinal', fechaFinal);
	            formData.append('MonoEfPeso', 100);
	            formData.append('ContactoMonoEf', 0);
	            formData.append('reintento', intTipoReintento_p);
	            formData.append('tipificacion', intIdLisopc_t);
	            formData.append('TxtFechaReintento', fechaAgenda);
	            formData.append('TxtHoraReintento', horaAgenda);
	            formData.append('textAreaComentarios', observaciones);
	            formData.append('hidId', codigoMiembro);
	            formData.append('oper', 'add');
	            formData.append('id_gestion_cbx', '<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET['id_gestion_cbx']; }?>');
	            formData.append('cbx_sentido', '<?php if(isset($_GET['sentido'])){ echo $_GET['sentido']; }else{ echo "0"; } ?>');
	            formData.append('cbx_canal__', '<?php if(isset($_GET['id_gestion_cbx'])) { echo explode('_', $_GET['id_gestion_cbx'])[0]; }else{ echo "0"; } ?>');
	            formData.append('Padre', 0);
	            formData.append('datoContacto', '<?php if(isset($_GET['ani'])){ echo $_GET['ani']; }else{ echo "0"; } ?>');
	            formData.append('llamarApi', 'no');

	            $.ajax({
	            	url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro='+codigoMiembro+'<?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&LlamadoExterno=si', 

	                type: 'POST',
	                data: formData,
	                cache: false,
	                contentType: false,
	                processData: false,
	                //una vez finalizado correctamente
	                success: function(data){
	                	
	                    if(data){
	                    	$.ajax({
	                    		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo='+tiempoInicio+'&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro='+codigoMiembro+'&ConsInteRegresado='+ data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['id_campana_crm'])){ echo $_GET['id_campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarForzado=true',
	                    		type  : "post",
	                    		data  : formData,
	                		 	cache: false,
			                    contentType: false,
			                    processData: false,
	                    		success : function(xt){
	                    			
	                    			$("#main #frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?consinte='+codigoMiembro+'&campan=true&user='+ codigoMiembro +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET["predictiva"])) { echo "&predictiva=".$_GET["predictiva"]; }?><?php if(isset($_GET["id_campana_crm"])) { echo "&campana_crm=".$_GET["id_campana_crm"]; }?><?php if(isset($_GET["sentido"])) { echo "&sentido=".$_GET["sentido"]; }?><?php if(isset($_GET["ani"])){ echo "&ani=".$_GET["ani"]; }?>');	                    			
	                    		}
	                    	});
			                        	           
	                    }               
	                },
	                //si ha ocurrido un error
	                error: function(){
	                    after_save_error();
	                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                }
	            });

		    }
		    
		    /**YCR 2019-10-24
			*Funcion para identificar cuando un usuario esta ubicado en un script
			*@return - si devuelve true, es que esta ubicado en un script
		    */
		    function estaEnScript(){
		    	var strScript = $("#main #frameContenedor").contents().find("#script").val();
		    	var bolScript = false;
		    	var data;
		    	if(strScript == 'script'){
		    		bolScript = true;
		    	}
                
                data={
                    accion:'agenteEnScript',
                    valor: bolScript
                };
		    	window.parent.postMessage(data, '*');	
			}
            
			function traerIdLisopc(intIdMonoef_p){

				intIdMonoef_t = $.ajax({
				                        url      : "formularios/generados/PHP_Ejecutar.php?traerMonoef=si",
				                        type     : "POST",
				                        data     : {intIdMonoef_t : intIdMonoef_p},
										cache : false,
				                        async    :false,
				                        success  : function(data) {
				                            return data;
				                        }
				                     }).responseText;

				return intIdMonoef_t;
			}
        </script>
	</head>
	<body id="main">
		<?php

			if(isset($_GET['id_campana_crm'])){

				if(!is_null($_GET['id_campana_crm']) && $_GET['id_campana_crm'] != 'null'){
					$Lsql = "SELECT CAMPAN_TipoCamp__b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b =".$_GET['id_campana_crm'];
			        $resultado = $mysqli->query($Lsql);
			        $CAMPAN_TipoCamp__b = 1;
			        $CAMPAN_ConsInte__GUION__Gui_b = null;
			        $CAMPAN_ConsInte__GUION__Pob_b = null;
			        $nombreCampana = Null;
			        while ($key = $resultado->fetch_object()) {
			            $CAMPAN_TipoCamp__b = $key->CAMPAN_TipoCamp__b;
			            $CAMPAN_ConsInte__GUION__Gui_b = $key->CAMPAN_ConsInte__GUION__Gui_b;
			            $CAMPAN_ConsInte__GUION__Pob_b = $key->CAMPAN_ConsInte__GUION__Pob_b;
			            $nombreCampana = $key->CAMPAN_Nombre____b;
			        }

			        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){
						
						$data = array(	"strToken_t" => $_GET['token'], 
										"strIdGestion_t" => $_GET['id_gestion_cbx'],
										"strNombreCampanaCRM_t" => $nombreCampana );                                                                    
						$data_string = json_encode($data);    

						$ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
						//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						//le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
						curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
						    'Content-Type: application/json',                                                                                
						    'Content-Length: ' . strlen($data_string))                                                                      
						); 
						//recogemos la respuesta
						$respuesta = curl_exec ($ch);
						//o el error, por si falla
						$error = curl_error($ch);
						//y finalmente cerramos curl
						//echo "Respuesta =>  ". $respuesta;
						//echo "<br/>Error => ".$error;
						//include "Log.class.php";
						//$log = new Log("log", "./Log/");
						//$log->insert($error, $respuesta, false, true, false);
						//echo "nada";
						curl_close ($ch);
					}

					if(isset($_GET['busqueda_manual_forzada'])){
						if($_GET['busqueda_manual_forzada'] == 'true'){

							include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Manual.php');
						}else{
							mostrar_guion($CAMPAN_TipoCamp__b, $CAMPAN_ConsInte__GUION__Pob_b);
						}
					}else{
						mostrar_guion($CAMPAN_TipoCamp__b , $CAMPAN_ConsInte__GUION__Pob_b);
			        }
				}else{
					echo "<div class='row'>
							<div style='text-align:center;' class='col-md-12'>
								<div class='alert alert-info'>
									Lo sentimos, pero el identificador de la campaña que se ha enviado, esta vacio.
								</div>
							</div>
						</div>";
				}
				
		    }

		    if(isset($_GET['formulario'])){
				$LsqlGUION = "SELECT GUION__ConsInte__b, GUION__Nombre____b FROM ".$BaseDatos_systema.".GUION_ WHERE GUION__ConsInte__b =".$_GET['formulario'];
				$results = $mysqli->query($LsqlGUION);
				while($key = $results->fetch_object()){
					$GION_TITLE = utf8_encode($key->GUION__Nombre____b);
				} 
		        if(isset($_GET['busqueda'])){
		            include ('formularios/G'.$_GET['formulario'].'/G'.$_GET['formulario'].'_Busqueda.php');
		        }else{
		            include ('formularios/G'.$_GET['formulario'].'/G'.$_GET['formulario'].'.php');
		        }
		        
		    }


		    if(isset($_GET['campan'])){
	            $Guion = 0;//id de la campaña
	            $tabla = 0;// $_GET['user'];//ide del usuario
	            $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET['campana_crm'];

	            $result = $mysqli->query($Lsql);
	            while($obj = $result->fetch_object()){
	                $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
	                $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
	            } 
	            //SELECT de la camic
	            $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_GET['campana_crm'];


	            

				
	            
	            
				$LsqlGUION = "SELECT GUION__ConsInte__b, GUION__Nombre____b FROM ".$BaseDatos_systema.".GUION_ WHERE GUION__ConsInte__b =".$Guion;
			
				$results = $mysqli->query($LsqlGUION);
				//var_dump($results->fetch_object());

			

				while($key = $results->fetch_object()){
					$GION_TITLE = utf8_encode($key->GUION__Nombre____b);

				} 
	            if(isset($_GET['busqueda'])){
	                include ('formularios/G'.$Guion.'/G'.$Guion.'_Busqueda.php');
	            }else{
	                include ('formularios/G'.$Guion.'/G'.$Guion.'.php');
	            }
	        }

	        function mostrar_guion($CAMPAN_TipoCamp__b, $CAMPAN_ConsInte__GUION__Pob_b ){
	        	
	        	if(file_exists('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Manual.php')){
	        		switch ($CAMPAN_TipoCamp__b) {

			            case 1:
			                include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Manual.php');
			                break;
			            case 2:
			                include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Telefono.php');
			                break;
			            case 3:
			                if(isset($_GET['consinte']) && $_GET['consinte'] == ''  ){
			            		 include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Manual.php');
			                	
			            	}else{
			            		include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
			            	}
			                break;
			           	case 4:
			                include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
			                break;
			            case 5:
			            	

			                 include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
			                break;

			            case 18:
			                include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
			                break;

			            case 6:
			                include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
			                break;
			            case 7:
			                include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
			                break;
			        }
	        	}else{
	        		echo "<div class='row'>
							<div style='text-align:center;' class='col-md-12'>
								<div class='alert alert-info'>
									Lo sentimos, por favor genere las carpetas de esta campaña, no se han generado los formularios.
								</div>
							</div>
						</div>";
	        	}
	        	
				
	        }
		?>	




		<!-- Scripts de las paginas de busqueda -->
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		 <!-- jQuery UI 1.11.4 -->
        
        <!-- Bootstrap 3.3.6 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        
        <!-- daterangepicker -->
        <script src="assets/js/moment.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Date Picker -->
		<script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>      

        <script src="assets/Guriddo_jqGrid_/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <script src="assets/Guriddo_jqGrid_/js/jquery.jqGrid.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="assets/js/numeric.js"></script>
        <script src="assets/js/alertify.js"></script>

        <script type="text/javascript">
        	$(document).ready(function() {


        		//document.getElementById("FormularioDatos").contentWindow.document.getElementById('Save').click();
    		 	/*$("#FormularioDatos").load(function () {                        
			        frames["FormularioDatos"].getElementById('Save').click();
			    });*/
        	});


        </script>


	</body>
</html>