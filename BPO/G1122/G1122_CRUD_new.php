<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1122_ConsInte__b, G1122_FechaInsercion , G1122_Usuario ,  G1122_CodigoMiembro  , G1122_PoblacionOrigen , G1122_EstadoDiligenciamiento ,  G1122_IdLlamada , G1122_C17121 as principal ,G1122_C20661,G1122_C17121,G1122_C17122,G1122_C18857,G1122_C20662,G1122_C20663,G1122_C17123,G1122_C17124,G1122_C17125,G1122_C17126,G1122_C17127,G1122_C17128,G1122_C17129,G1122_C17130,G1122_C17131,G1122_C17132,G1122_C18858,G1122_C18351,G1122_C18859,G1122_C18860,G1122_C17133,G1122_C17134,G1122_C17135,G1122_C17136,G1122_C17137,G1122_C17138,G1122_C17139,G1122_C17140,G1122_C17141,G1122_C17144,G1122_C17145,G1122_C17146,G1122_C18721,G1122_C18720,G1122_C17147,G1122_C17149,G1122_C17150,G1122_C17151,G1122_C17152,G1122_C20691 FROM '.$BaseDatos.'.G1122 WHERE G1122_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1122_C20661'] = $key->G1122_C20661;

                $datos[$i]['G1122_C17121'] = $key->G1122_C17121;

                $datos[$i]['G1122_C17122'] = $key->G1122_C17122;

                $datos[$i]['G1122_C18857'] = $key->G1122_C18857;

                $datos[$i]['G1122_C20662'] = $key->G1122_C20662;

                $datos[$i]['G1122_C20663'] = explode(' ', $key->G1122_C20663)[0];

                $datos[$i]['G1122_C17123'] = $key->G1122_C17123;

                $datos[$i]['G1122_C17124'] = $key->G1122_C17124;

                $datos[$i]['G1122_C17125'] = $key->G1122_C17125;

                $datos[$i]['G1122_C17126'] = $key->G1122_C17126;

                $datos[$i]['G1122_C17127'] = $key->G1122_C17127;

                $datos[$i]['G1122_C17128'] = $key->G1122_C17128;

                $datos[$i]['G1122_C17129'] = $key->G1122_C17129;

                $datos[$i]['G1122_C17130'] = $key->G1122_C17130;

                $datos[$i]['G1122_C17131'] = $key->G1122_C17131;

                $datos[$i]['G1122_C17132'] = $key->G1122_C17132;

                $datos[$i]['G1122_C18858'] = $key->G1122_C18858;

                $datos[$i]['G1122_C18351'] = $key->G1122_C18351;

                $datos[$i]['G1122_C18859'] = explode(' ', $key->G1122_C18859)[0];

                $datos[$i]['G1122_C18860'] = explode(' ', $key->G1122_C18860)[0];

                $datos[$i]['G1122_C17133'] = $key->G1122_C17133;

                $datos[$i]['G1122_C17134'] = $key->G1122_C17134;

                $datos[$i]['G1122_C17135'] = explode(' ', $key->G1122_C17135)[0];
  
                $hora = '';
                if(!is_null($key->G1122_C17136)){
                    $hora = explode(' ', $key->G1122_C17136)[1];
                }

                $datos[$i]['G1122_C17137'] = $key->G1122_C17137;

                $datos[$i]['G1122_C17138'] = $key->G1122_C17138;

                $datos[$i]['G1122_C17139'] = $key->G1122_C17139;

                $datos[$i]['G1122_C17140'] = $key->G1122_C17140;

                $datos[$i]['G1122_C17141'] = $key->G1122_C17141;

                $datos[$i]['G1122_C17144'] = $key->G1122_C17144;

                $datos[$i]['G1122_C17145'] = $key->G1122_C17145;

                $datos[$i]['G1122_C17146'] = $key->G1122_C17146;

                $datos[$i]['G1122_C18721'] = $key->G1122_C18721;

                $datos[$i]['G1122_C18720'] = $key->G1122_C18720;

                $datos[$i]['G1122_C17147'] = $key->G1122_C17147;

                $datos[$i]['G1122_C17149'] = $key->G1122_C17149;

                $datos[$i]['G1122_C17150'] = $key->G1122_C17150;

                $datos[$i]['G1122_C17151'] = $key->G1122_C17151;

                $datos[$i]['G1122_C17152'] = $key->G1122_C17152;

                $datos[$i]['G1122_C20691'] = $key->G1122_C20691;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1122_ConsInte__b as id,  G1122_C17121 as camp1 , G1122_C17122 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1122 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1122_C17121 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1122_C17122 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1122_C17146'])){
            echo '<select class="form-control input-sm"  name="G1122_C17146" id="G1122_C17146">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1122_C17146'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1122_C17146'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1122_C17146'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1137_C19160'])){
            echo '<select class="form-control input-sm"  name="1137_C19160" id="1137_C19160">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1137_C19160'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1137_C19160'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1137_C19160'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1133_C18940'])){
            echo '<select class="form-control input-sm"  name="1133_C18940" id="1133_C18940">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1133_C18940'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1133_C18940'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1133_C18940'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1134_C18918'])){
            echo '<select class="form-control input-sm"  name="1134_C18918" id="1134_C18918">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1134_C18918'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1134_C18918'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1134_C18918'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1135_C18908'])){
            echo '<select class="form-control input-sm"  name="1135_C18908" id="1135_C18908">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1135_C18908'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1135_C18908'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1135_C18908'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1136_C18897'])){
            echo '<select class="form-control input-sm"  name="1136_C18897" id="1136_C18897">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1136_C18897'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1136_C18897'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1136_C18897'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1138_C18929'])){
            echo '<select class="form-control input-sm"  name="1138_C18929" id="1138_C18929">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1138_C18929'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1138_C18929'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1138_C18929'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1139_C19171'])){
            echo '<select class="form-control input-sm"  name="1139_C19171" id="1139_C19171">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1139_C19171'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1139_C19171'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1139_C19171'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        

		if(isset($_GET['MostrarCombo_Guion_1140_C20233'])){
            echo '<select class="form-control input-sm"  name="1140_C20233" id="1140_C20233">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_1140_C20233'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_1140_C20233'])){
            $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_1140_C20233'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	     		
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1122");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1122_ConsInte__b, G1122_FechaInsercion , G1122_Usuario ,  G1122_CodigoMiembro  , G1122_PoblacionOrigen , G1122_EstadoDiligenciamiento ,  G1122_IdLlamada , G1122_C17121 as principal ,G1122_C20661,G1122_C17121,G1122_C17122,G1122_C18857,G1122_C20662,G1122_C20663,G1122_C17123,G1122_C17124,G1122_C17125,G1122_C17126,G1122_C17127,G1122_C17128,G1122_C17129,G1122_C17130,G1122_C17131,G1122_C17132,G1122_C18858, a.LISOPC_Nombre____b as G1122_C18351,G1122_C18859,G1122_C18860, b.LISOPC_Nombre____b as G1122_C17133, c.LISOPC_Nombre____b as G1122_C17134,G1122_C17135,G1122_C17136,G1122_C17137,G1122_C17138,G1122_C17139,G1122_C17140,G1122_C17141, d.LISOPC_Nombre____b as G1122_C17144, e.LISOPC_Nombre____b as G1122_C17145, G1188_C18652,G1122_C18721,G1122_C18720,G1122_C17147, f.LISOPC_Nombre____b as G1122_C17149, g.LISOPC_Nombre____b as G1122_C17150, h.LISOPC_Nombre____b as G1122_C17151, i.LISOPC_Nombre____b as G1122_C17152,G1122_C20691 FROM '.$BaseDatos.'.G1122 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1122_C18351 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1122_C17133 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1122_C17134 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1122_C17144 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1122_C17145 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1122_C17146 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1122_C17149 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1122_C17150 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1122_C17151 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1122_C17152';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1122_C17136)){
                    $hora_a = explode(' ', $fila->G1122_C17136)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1122_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1122_ConsInte__b , ($fila->G1122_C20661) , ($fila->G1122_C17121) , ($fila->G1122_C17122) , ($fila->G1122_C18857) , ($fila->G1122_C20662) , explode(' ', $fila->G1122_C20663)[0] , ($fila->G1122_C17123) , ($fila->G1122_C17124) , ($fila->G1122_C17125) , ($fila->G1122_C17126) , ($fila->G1122_C17127) , ($fila->G1122_C17128) , ($fila->G1122_C17129) , ($fila->G1122_C17130) , ($fila->G1122_C17131) , ($fila->G1122_C17132) , ($fila->G1122_C18858) , ($fila->G1122_C18351) , explode(' ', $fila->G1122_C18859)[0] , explode(' ', $fila->G1122_C18860)[0] , ($fila->G1122_C17133) , ($fila->G1122_C17134) , explode(' ', $fila->G1122_C17135)[0] , $hora_a , ($fila->G1122_C17137) , ($fila->G1122_C17138) , ($fila->G1122_C17139) , ($fila->G1122_C17140) , ($fila->G1122_C17141) , ($fila->G1122_C17144) , ($fila->G1122_C17145) , ($fila->G1188_C18652) , ($fila->G1122_C18721) , ($fila->G1122_C18720) , ($fila->G1122_C17147) , ($fila->G1122_C17149) , ($fila->G1122_C17150) , ($fila->G1122_C17151) , ($fila->G1122_C17152) , ($fila->G1122_C20691) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1122 WHERE G1122_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1122_ConsInte__b as id,  G1122_C17121 as camp1 , G1122_C17122 as camp2  FROM '.$BaseDatos.'.G1122 ORDER BY G1122_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1122 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1122(";
            $LsqlV = " VALUES ("; 
  
            $G1122_C20661 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C20661"])){
                if($_POST["G1122_C20661"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C20661 = $_POST["G1122_C20661"];
                    $LsqlU .= $separador." G1122_C20661 = ".$G1122_C20661."";
                    $LsqlI .= $separador." G1122_C20661";
                    $LsqlV .= $separador.$G1122_C20661;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17121"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17121 = '".$_POST["G1122_C17121"]."'";
                $LsqlI .= $separador."G1122_C17121";
                $LsqlV .= $separador."'".$_POST["G1122_C17121"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17122"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17122 = '".$_POST["G1122_C17122"]."'";
                $LsqlI .= $separador."G1122_C17122";
                $LsqlV .= $separador."'".$_POST["G1122_C17122"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C18857"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C18857 = '".$_POST["G1122_C18857"]."'";
                $LsqlI .= $separador."G1122_C18857";
                $LsqlV .= $separador."'".$_POST["G1122_C18857"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C20662"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C20662 = '".$_POST["G1122_C20662"]."'";
                $LsqlI .= $separador."G1122_C20662";
                $LsqlV .= $separador."'".$_POST["G1122_C20662"]."'";
                $validar = 1;
            }
             
 
            $G1122_C20663 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1122_C20663"])){    
                if($_POST["G1122_C20663"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1122_C20663"]);
                    if(count($tieneHora) > 1){
                    	$G1122_C20663 = "'".$_POST["G1122_C20663"]."'";
                    }else{
                    	$G1122_C20663 = "'".str_replace(' ', '',$_POST["G1122_C20663"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1122_C20663 = ".$G1122_C20663;
                    $LsqlI .= $separador." G1122_C20663";
                    $LsqlV .= $separador.$G1122_C20663;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17123"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17123 = '".$_POST["G1122_C17123"]."'";
                $LsqlI .= $separador."G1122_C17123";
                $LsqlV .= $separador."'".$_POST["G1122_C17123"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17124"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17124 = '".$_POST["G1122_C17124"]."'";
                $LsqlI .= $separador."G1122_C17124";
                $LsqlV .= $separador."'".$_POST["G1122_C17124"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17125"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17125 = '".$_POST["G1122_C17125"]."'";
                $LsqlI .= $separador."G1122_C17125";
                $LsqlV .= $separador."'".$_POST["G1122_C17125"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17126"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17126 = '".$_POST["G1122_C17126"]."'";
                $LsqlI .= $separador."G1122_C17126";
                $LsqlV .= $separador."'".$_POST["G1122_C17126"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17127"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17127 = '".$_POST["G1122_C17127"]."'";
                $LsqlI .= $separador."G1122_C17127";
                $LsqlV .= $separador."'".$_POST["G1122_C17127"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17128"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17128 = '".$_POST["G1122_C17128"]."'";
                $LsqlI .= $separador."G1122_C17128";
                $LsqlV .= $separador."'".$_POST["G1122_C17128"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17129"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17129 = '".$_POST["G1122_C17129"]."'";
                $LsqlI .= $separador."G1122_C17129";
                $LsqlV .= $separador."'".$_POST["G1122_C17129"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17130"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17130 = '".$_POST["G1122_C17130"]."'";
                $LsqlI .= $separador."G1122_C17130";
                $LsqlV .= $separador."'".$_POST["G1122_C17130"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17131"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17131 = '".$_POST["G1122_C17131"]."'";
                $LsqlI .= $separador."G1122_C17131";
                $LsqlV .= $separador."'".$_POST["G1122_C17131"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17132"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17132 = '".$_POST["G1122_C17132"]."'";
                $LsqlI .= $separador."G1122_C17132";
                $LsqlV .= $separador."'".$_POST["G1122_C17132"]."'";
                $validar = 1;
            }
             
  
            $G1122_C18858 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C18858"])){
                if($_POST["G1122_C18858"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C18858 = $_POST["G1122_C18858"];
                    $LsqlU .= $separador." G1122_C18858 = ".$G1122_C18858."";
                    $LsqlI .= $separador." G1122_C18858";
                    $LsqlV .= $separador.$G1122_C18858;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C18351"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C18351 = '".$_POST["G1122_C18351"]."'";
                $LsqlI .= $separador."G1122_C18351";
                $LsqlV .= $separador."'".$_POST["G1122_C18351"]."'";
                $validar = 1;
            }
             
 
            $G1122_C18859 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1122_C18859"])){    
                if($_POST["G1122_C18859"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1122_C18859"]);
                    if(count($tieneHora) > 1){
                    	$G1122_C18859 = "'".$_POST["G1122_C18859"]."'";
                    }else{
                    	$G1122_C18859 = "'".str_replace(' ', '',$_POST["G1122_C18859"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1122_C18859 = ".$G1122_C18859;
                    $LsqlI .= $separador." G1122_C18859";
                    $LsqlV .= $separador.$G1122_C18859;
                    $validar = 1;
                }
            }
 
            $G1122_C18860 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1122_C18860"])){    
                if($_POST["G1122_C18860"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1122_C18860"]);
                    if(count($tieneHora) > 1){
                    	$G1122_C18860 = "'".$_POST["G1122_C18860"]."'";
                    }else{
                    	$G1122_C18860 = "'".str_replace(' ', '',$_POST["G1122_C18860"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1122_C18860 = ".$G1122_C18860;
                    $LsqlI .= $separador." G1122_C18860";
                    $LsqlV .= $separador.$G1122_C18860;
                    $validar = 1;
                }
            }
 
            $G1122_C17133 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17133 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1122_C17133 = ".$G1122_C17133;
                    $LsqlI .= $separador." G1122_C17133";
                    $LsqlV .= $separador.$G1122_C17133;
                    $validar = 1;

                    
                }
            }
 
            $G1122_C17134 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17134 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1122_C17134 = ".$G1122_C17134;
                    $LsqlI .= $separador." G1122_C17134";
                    $LsqlV .= $separador.$G1122_C17134;
                    $validar = 1;
                }
            }
 
            $G1122_C17135 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17135 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1122_C17135 = ".$G1122_C17135;
                    $LsqlI .= $separador." G1122_C17135";
                    $LsqlV .= $separador.$G1122_C17135;
                    $validar = 1;
                }
            }
 
            $G1122_C17136 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17136 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1122_C17136 = ".$G1122_C17136;
                    $LsqlI .= $separador." G1122_C17136";
                    $LsqlV .= $separador.$G1122_C17136;
                    $validar = 1;
                }
            }
 
            $G1122_C17137 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17137 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1122_C17137 = ".$G1122_C17137;
                    $LsqlI .= $separador." G1122_C17137";
                    $LsqlV .= $separador.$G1122_C17137;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17138"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17138 = '".$_POST["G1122_C17138"]."'";
                $LsqlI .= $separador."G1122_C17138";
                $LsqlV .= $separador."'".$_POST["G1122_C17138"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17139 = '".$_POST["G1122_C17139"]."'";
                $LsqlI .= $separador."G1122_C17139";
                $LsqlV .= $separador."'".$_POST["G1122_C17139"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17140"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17140 = '".$_POST["G1122_C17140"]."'";
                $LsqlI .= $separador."G1122_C17140";
                $LsqlV .= $separador."'".$_POST["G1122_C17140"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17141"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17141 = '".$_POST["G1122_C17141"]."'";
                $LsqlI .= $separador."G1122_C17141";
                $LsqlV .= $separador."'".$_POST["G1122_C17141"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17142"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17142 = '".$_POST["G1122_C17142"]."'";
                $LsqlI .= $separador."G1122_C17142";
                $LsqlV .= $separador."'".$_POST["G1122_C17142"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17143"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17143 = '".$_POST["G1122_C17143"]."'";
                $LsqlI .= $separador."G1122_C17143";
                $LsqlV .= $separador."'".$_POST["G1122_C17143"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17144 = '".$_POST["G1122_C17144"]."'";
                $LsqlI .= $separador."G1122_C17144";
                $LsqlV .= $separador."'".$_POST["G1122_C17144"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17145"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17145 = '".$_POST["G1122_C17145"]."'";
                $LsqlI .= $separador."G1122_C17145";
                $LsqlV .= $separador."'".$_POST["G1122_C17145"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17146 = '".$_POST["G1122_C17146"]."'";
                $LsqlI .= $separador."G1122_C17146";
                $LsqlV .= $separador."'".$_POST["G1122_C17146"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C18721"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C18721 = '".$_POST["G1122_C18721"]."'";
                $LsqlI .= $separador."G1122_C18721";
                $LsqlV .= $separador."'".$_POST["G1122_C18721"]."'";
                $validar = 1;
            }
             
  
            $G1122_C18720 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C18720"])){
                if($_POST["G1122_C18720"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C18720 = $_POST["G1122_C18720"];
                    $LsqlU .= $separador." G1122_C18720 = ".$G1122_C18720."";
                    $LsqlI .= $separador." G1122_C18720";
                    $LsqlV .= $separador.$G1122_C18720;
                    $validar = 1;
                }
            }
  
            $G1122_C17147 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C17147"])){
                if($_POST["G1122_C17147"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C17147 = $_POST["G1122_C17147"];
                    $LsqlU .= $separador." G1122_C17147 = ".$G1122_C17147."";
                    $LsqlI .= $separador." G1122_C17147";
                    $LsqlV .= $separador.$G1122_C17147;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17149"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17149 = '".$_POST["G1122_C17149"]."'";
                $LsqlI .= $separador."G1122_C17149";
                $LsqlV .= $separador."'".$_POST["G1122_C17149"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17150"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17150 = '".$_POST["G1122_C17150"]."'";
                $LsqlI .= $separador."G1122_C17150";
                $LsqlV .= $separador."'".$_POST["G1122_C17150"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17151"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17151 = '".$_POST["G1122_C17151"]."'";
                $LsqlI .= $separador."G1122_C17151";
                $LsqlV .= $separador."'".$_POST["G1122_C17151"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17152"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17152 = '".$_POST["G1122_C17152"]."'";
                $LsqlI .= $separador."G1122_C17152";
                $LsqlV .= $separador."'".$_POST["G1122_C17152"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C20691"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C20691 = '".$_POST["G1122_C20691"]."'";
                $LsqlI .= $separador."G1122_C20691";
                $LsqlV .= $separador."'".$_POST["G1122_C20691"]."'";
                $validar = 1;
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1122_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1122_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1122_Usuario , G1122_FechaInsercion, G1122_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1122_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1122 WHERE G1122_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                    	echo $mysqli->insert_id;
                	}else{
                		echo "1";    		
                	}
                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1226_ConsInte__b, G1226_C20657, G1226_C20658 FROM ".$BaseDatos.".G1226  ";

        $SQL .= " WHERE G1226_C20657 = '".$numero."'"; 

        $SQL .= " ORDER BY G1226_C20657";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1226_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1226_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1226_C20657)."</cell>";

            echo "<cell>". ($fila->G1226_C20658)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1137_ConsInte__b, G1137_C20679, G1137_C17541, G1137_C17542, G1137_C17543, G1137_C20680, G1137_C20681, G1137_C17544, G1137_C17545, G1137_C17546, G1137_C17547, G1137_C19153, G1137_C17548, G1137_C17549, G1137_C17550, G1137_C17551, G1137_C17552, G1137_C19154, r.LISOPC_Nombre____b as  G1137_C19155, G1137_C19156, G1137_C19157, G1137_C17560, G1137_C17561, G1137_C17562, G1137_C17563, y.LISOPC_Nombre____b as  G1137_C19158, z.LISOPC_Nombre____b as  G1137_C19159, G1137_C19161, G1137_C19162, G1137_C19163, G1137_C17564, ag.LISOPC_Nombre____b as  G1137_C17553, ah.LISOPC_Nombre____b as  G1137_C17554, G1137_C17555, G1137_C17556, G1137_C17557, G1137_C17558, G1137_C17559 FROM ".$BaseDatos.".G1137  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1137_C19155 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1137_C19158 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1137_C19159 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1137_C19160 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ag ON ag.LISOPC_ConsInte__b =  G1137_C17553 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ah ON ah.LISOPC_ConsInte__b =  G1137_C17554 ";

        $SQL .= " WHERE G1137_C0 = '".$numero."'"; 

        $SQL .= " ORDER BY G1137_C20679";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1137_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1137_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1137_C20679)."</cell>";

            echo "<cell>". ($fila->G1137_C17541)."</cell>";

            echo "<cell>". ($fila->G1137_C17542)."</cell>";

            echo "<cell>". ($fila->G1137_C17543)."</cell>";

            echo "<cell>". ($fila->G1137_C20680)."</cell>";

            echo "<cell>". ($fila->G1137_C20681)."</cell>";

            echo "<cell>". ($fila->G1137_C17544)."</cell>";

            echo "<cell>". ($fila->G1137_C17545)."</cell>";

            echo "<cell>". ($fila->G1137_C17546)."</cell>";

            echo "<cell>". ($fila->G1137_C17547)."</cell>";

            echo "<cell>". ($fila->G1137_C19153)."</cell>";

            echo "<cell>". ($fila->G1137_C17548)."</cell>";

            echo "<cell>". ($fila->G1137_C17549)."</cell>";

            echo "<cell>". ($fila->G1137_C17550)."</cell>";

            echo "<cell>". ($fila->G1137_C17551)."</cell>";

            echo "<cell>". ($fila->G1137_C17552)."</cell>";

            echo "<cell>". $fila->G1137_C19154."</cell>"; 

            echo "<cell>". ($fila->G1137_C19155)."</cell>";

            if($fila->G1137_C19156 != ''){
                echo "<cell>". explode(' ', $fila->G1137_C19156)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1137_C19157 != ''){
                echo "<cell>". explode(' ', $fila->G1137_C19157)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1137_C17560)."</cell>";

            echo "<cell>". ($fila->G1137_C17561)."</cell>";

            echo "<cell>". ($fila->G1137_C17562)."</cell>";

            echo "<cell>". ($fila->G1137_C17563)."</cell>";

            echo "<cell>". ($fila->G1137_C19158)."</cell>";

            echo "<cell>". ($fila->G1137_C19159)."</cell>";

            echo "<cell>". ($fila->G1137_C19160)."</cell>";

            echo "<cell>". $fila->G1137_C19161."</cell>"; 

            echo "<cell>". ($fila->G1137_C19162)."</cell>";

            echo "<cell>". $fila->G1137_C19163."</cell>"; 

            echo "<cell><![CDATA[". ($fila->G1137_C17564)."]]></cell>";

            echo "<cell>". ($fila->G1137_C17553)."</cell>";

            echo "<cell>". ($fila->G1137_C17554)."</cell>";

            echo "<cell>". $fila->G1137_C17555."</cell>"; 

            echo "<cell>". $fila->G1137_C17556."</cell>"; 

            if($fila->G1137_C17557 != ''){
                echo "<cell>". explode(' ', $fila->G1137_C17557)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1137_C17558 != ''){
                echo "<cell>". explode(' ', $fila->G1137_C17558)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1137_C17559)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_2"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1133_ConsInte__b, G1133_C20529, G1133_C20530, G1133_C20676, G1133_C17434, G1133_C17435, G1133_C17436, G1133_C20677, G1133_C20678, G1133_C17437, G1133_C17438, G1133_C17439, G1133_C17440, G1133_C18933, G1133_C17441, G1133_C17442, G1133_C17443, G1133_C17444, G1133_C17445, G1133_C18934, t.LISOPC_Nombre____b as  G1133_C18935, G1133_C18936, G1133_C18937, G1133_C17451, G1133_C17452, G1133_C17453, G1133_C17454, ab.LISOPC_Nombre____b as  G1133_C18938, ac.LISOPC_Nombre____b as  G1133_C18939, G1133_C18941, G1133_C18942, G1133_C18943, G1133_C18554, G1133_C17456, G1133_C18555, G1133_C18556, G1133_C18557, G1133_C18558, G1133_C18559, G1133_C18560, G1133_C18561, aq.LISOPC_Nombre____b as  G1133_C17446, ar.LISOPC_Nombre____b as  G1133_C17447, G1133_C17448, G1133_C17449, G1133_C17450 FROM ".$BaseDatos.".G1133  LEFT JOIN ".$BaseDatos_systema.".LISOPC as t ON t.LISOPC_ConsInte__b =  G1133_C18935 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ab ON ab.LISOPC_ConsInte__b =  G1133_C18938 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ac ON ac.LISOPC_ConsInte__b =  G1133_C18939 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1133_C18940 LEFT JOIN ".$BaseDatos_systema.".LISOPC as aq ON aq.LISOPC_ConsInte__b =  G1133_C17446 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ar ON ar.LISOPC_ConsInte__b =  G1133_C17447 ";

        $SQL .= " WHERE G1133_C20676 = '".$numero."'"; 

        $SQL .= " ORDER BY G1133_C20529";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1133_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1133_ConsInte__b)."</cell>"; 
            

            echo "<cell>". $fila->G1133_C20529."</cell>"; 

            echo "<cell>". $fila->G1133_C20530."</cell>"; 

            echo "<cell>". ($fila->G1133_C20676)."</cell>";

            echo "<cell>". ($fila->G1133_C17434)."</cell>";

            echo "<cell>". ($fila->G1133_C17435)."</cell>";

            echo "<cell>". ($fila->G1133_C17436)."</cell>";

            echo "<cell>". ($fila->G1133_C20677)."</cell>";

            echo "<cell>". ($fila->G1133_C20678)."</cell>";

            echo "<cell>". ($fila->G1133_C17437)."</cell>";

            echo "<cell>". ($fila->G1133_C17438)."</cell>";

            echo "<cell>". ($fila->G1133_C17439)."</cell>";

            echo "<cell>". ($fila->G1133_C17440)."</cell>";

            echo "<cell>". ($fila->G1133_C18933)."</cell>";

            echo "<cell>". ($fila->G1133_C17441)."</cell>";

            echo "<cell>". ($fila->G1133_C17442)."</cell>";

            echo "<cell>". ($fila->G1133_C17443)."</cell>";

            echo "<cell>". ($fila->G1133_C17444)."</cell>";

            echo "<cell>". ($fila->G1133_C17445)."</cell>";

            echo "<cell>". $fila->G1133_C18934."</cell>"; 

            echo "<cell>". ($fila->G1133_C18935)."</cell>";

            if($fila->G1133_C18936 != ''){
                echo "<cell>". explode(' ', $fila->G1133_C18936)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1133_C18937 != ''){
                echo "<cell>". explode(' ', $fila->G1133_C18937)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1133_C17451)."</cell>";

            echo "<cell>". ($fila->G1133_C17452)."</cell>";

            echo "<cell>". ($fila->G1133_C17453)."</cell>";

            echo "<cell>". ($fila->G1133_C17454)."</cell>";

            echo "<cell>". ($fila->G1133_C18938)."</cell>";

            echo "<cell>". ($fila->G1133_C18939)."</cell>";

            echo "<cell>". ($fila->G1133_C18940)."</cell>";

            echo "<cell>". $fila->G1133_C18941."</cell>"; 

            echo "<cell>". ($fila->G1133_C18942)."</cell>";

            echo "<cell>". $fila->G1133_C18943."</cell>"; 

            echo "<cell>". ($fila->G1133_C18554)."</cell>";

            echo "<cell>". $fila->G1133_C17456."</cell>"; 

            echo "<cell>". ($fila->G1133_C18555)."</cell>";

            echo "<cell>". $fila->G1133_C18556."</cell>"; 

            echo "<cell>". $fila->G1133_C18557."</cell>"; 

            echo "<cell>". $fila->G1133_C18558."</cell>"; 

            if($fila->G1133_C18559 != ''){
                echo "<cell>". explode(' ', $fila->G1133_C18559)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1133_C18560)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1133_C18561)."]]></cell>";

            echo "<cell>". ($fila->G1133_C17446)."</cell>";

            echo "<cell>". ($fila->G1133_C17447)."</cell>";

            if($fila->G1133_C17448 != ''){
                echo "<cell>". explode(' ', $fila->G1133_C17448)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1133_C17449 != ''){
                echo "<cell>". explode(' ', $fila->G1133_C17449)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1133_C17450)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_3"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1134_ConsInte__b, G1134_C20670, G1134_C17457, G1134_C17458, G1134_C20333, G1134_C20671, G1134_C20672, G1134_C17459, G1134_C17460, G1134_C17461, G1134_C17462, G1134_C17463, G1134_C18912, G1134_C17464, G1134_C17465, G1134_C17466, G1134_C17467, G1134_C17468, r.LISOPC_Nombre____b as  G1134_C18913, G1134_C18914, G1134_C18915, G1134_C17476, G1134_C17477, G1134_C17478, G1134_C17479, y.LISOPC_Nombre____b as  G1134_C18916, z.LISOPC_Nombre____b as  G1134_C18917, G1134_C18919, G1134_C18920, G1134_C18921, G1134_C18344, G1134_C17481, G1134_C18345, G1134_C18346, G1134_C18347, G1134_C18348, G1134_C18349, G1134_C18350, G1134_C17482, ao.LISOPC_Nombre____b as  G1134_C17469, ap.LISOPC_Nombre____b as  G1134_C17470, G1134_C17471, G1134_C17472, G1134_C17473, G1134_C17474, G1134_C17475 FROM ".$BaseDatos.".G1134  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1134_C18913 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1134_C18916 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1134_C18917 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1134_C18918 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ao ON ao.LISOPC_ConsInte__b =  G1134_C17469 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ap ON ap.LISOPC_ConsInte__b =  G1134_C17470 ";

        $SQL .= " WHERE G1134_C20670 = '".$numero."'"; 

        $SQL .= " ORDER BY G1134_C20670";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1134_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1134_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1134_C20670)."</cell>";

            echo "<cell>". ($fila->G1134_C17457)."</cell>";

            echo "<cell>". ($fila->G1134_C17458)."</cell>";

            echo "<cell>". ($fila->G1134_C20333)."</cell>";

            echo "<cell>". ($fila->G1134_C20671)."</cell>";

            echo "<cell>". ($fila->G1134_C20672)."</cell>";

            echo "<cell>". ($fila->G1134_C17459)."</cell>";

            echo "<cell>". ($fila->G1134_C17460)."</cell>";

            echo "<cell>". ($fila->G1134_C17461)."</cell>";

            echo "<cell>". ($fila->G1134_C17462)."</cell>";

            echo "<cell>". ($fila->G1134_C17463)."</cell>";

            echo "<cell>". $fila->G1134_C18912."</cell>"; 

            echo "<cell>". ($fila->G1134_C17464)."</cell>";

            echo "<cell>". ($fila->G1134_C17465)."</cell>";

            echo "<cell>". ($fila->G1134_C17466)."</cell>";

            echo "<cell>". ($fila->G1134_C17467)."</cell>";

            echo "<cell>". ($fila->G1134_C17468)."</cell>";

            echo "<cell>". ($fila->G1134_C18913)."</cell>";

            if($fila->G1134_C18914 != ''){
                echo "<cell>". explode(' ', $fila->G1134_C18914)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1134_C18915 != ''){
                echo "<cell>". explode(' ', $fila->G1134_C18915)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1134_C17476)."</cell>";

            echo "<cell>". ($fila->G1134_C17477)."</cell>";

            echo "<cell>". ($fila->G1134_C17478)."</cell>";

            echo "<cell>". ($fila->G1134_C17479)."</cell>";

            echo "<cell>". ($fila->G1134_C18916)."</cell>";

            echo "<cell>". ($fila->G1134_C18917)."</cell>";

            echo "<cell>". ($fila->G1134_C18918)."</cell>";

            echo "<cell>". $fila->G1134_C18919."</cell>"; 

            echo "<cell>". ($fila->G1134_C18920)."</cell>";

            echo "<cell>". $fila->G1134_C18921."</cell>"; 

            echo "<cell>". ($fila->G1134_C18344)."</cell>";

            echo "<cell>". $fila->G1134_C17481."</cell>"; 

            echo "<cell>". ($fila->G1134_C18345)."</cell>";

            echo "<cell>". $fila->G1134_C18346."</cell>"; 

            echo "<cell>". $fila->G1134_C18347."</cell>"; 

            echo "<cell>". $fila->G1134_C18348."</cell>"; 

            if($fila->G1134_C18349 != ''){
                echo "<cell>". explode(' ', $fila->G1134_C18349)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1134_C18350)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1134_C17482)."]]></cell>";

            echo "<cell>". ($fila->G1134_C17469)."</cell>";

            echo "<cell>". ($fila->G1134_C17470)."</cell>";

            echo "<cell>". $fila->G1134_C17471."</cell>"; 

            echo "<cell>". $fila->G1134_C17472."</cell>"; 

            if($fila->G1134_C17473 != ''){
                echo "<cell>". explode(' ', $fila->G1134_C17473)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1134_C17474 != ''){
                echo "<cell>". explode(' ', $fila->G1134_C17474)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1134_C17475)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_4"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1135_ConsInte__b, G1135_C20667, G1135_C17483, G1135_C17484, G1135_C17485, G1135_C20668, G1135_C20669, G1135_C17486, G1135_C17487, G1135_C17488, G1135_C17489, G1135_C18901, G1135_C17490, G1135_C17491, G1135_C17492, G1135_C17493, G1135_C17494, G1135_C18902, r.LISOPC_Nombre____b as  G1135_C18903, G1135_C18904, G1135_C18905, G1135_C17502, G1135_C17503, G1135_C17504, G1135_C17505, y.LISOPC_Nombre____b as  G1135_C18906, z.LISOPC_Nombre____b as  G1135_C18907, G1135_C18909, G1135_C18910, G1135_C18911, af.LISOPC_Nombre____b as  G1135_C18645, G1135_C17507, G1135_C17508, G1135_C17509, G1135_C18646, G1135_C18647, G1135_C18648, G1135_C18649, G1135_C18650, G1135_C18651, G1135_C17512, ar.LISOPC_Nombre____b as  G1135_C17495, as.LISOPC_Nombre____b as  G1135_C17496, G1135_C17497, G1135_C17498, G1135_C17499, G1135_C17500, G1135_C17501 FROM ".$BaseDatos.".G1135  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1135_C18903 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1135_C18906 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1135_C18907 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1135_C18908 LEFT JOIN ".$BaseDatos_systema.".LISOPC as af ON af.LISOPC_ConsInte__b =  G1135_C18645 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ar ON ar.LISOPC_ConsInte__b =  G1135_C17495 LEFT JOIN ".$BaseDatos_systema.".LISOPC as as ON as.LISOPC_ConsInte__b =  G1135_C17496 ";

        $SQL .= " WHERE G1135_C20667 = '".$numero."'"; 

        $SQL .= " ORDER BY G1135_C20667";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1135_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1135_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1135_C20667)."</cell>";

            echo "<cell>". ($fila->G1135_C17483)."</cell>";

            echo "<cell>". ($fila->G1135_C17484)."</cell>";

            echo "<cell>". ($fila->G1135_C17485)."</cell>";

            echo "<cell>". ($fila->G1135_C20668)."</cell>";

            echo "<cell>". ($fila->G1135_C20669)."</cell>";

            echo "<cell>". ($fila->G1135_C17486)."</cell>";

            echo "<cell>". ($fila->G1135_C17487)."</cell>";

            echo "<cell>". ($fila->G1135_C17488)."</cell>";

            echo "<cell>". ($fila->G1135_C17489)."</cell>";

            echo "<cell>". ($fila->G1135_C18901)."</cell>";

            echo "<cell>". ($fila->G1135_C17490)."</cell>";

            echo "<cell>". ($fila->G1135_C17491)."</cell>";

            echo "<cell>". ($fila->G1135_C17492)."</cell>";

            echo "<cell>". ($fila->G1135_C17493)."</cell>";

            echo "<cell>". ($fila->G1135_C17494)."</cell>";

            echo "<cell>". ($fila->G1135_C18902)."</cell>";

            echo "<cell>". ($fila->G1135_C18903)."</cell>";

            if($fila->G1135_C18904 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C18904)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1135_C18905 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C18905)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1135_C17502)."</cell>";

            echo "<cell>". ($fila->G1135_C17503)."</cell>";

            echo "<cell>". ($fila->G1135_C17504)."</cell>";

            echo "<cell>". ($fila->G1135_C17505)."</cell>";

            echo "<cell>". ($fila->G1135_C18906)."</cell>";

            echo "<cell>". ($fila->G1135_C18907)."</cell>";

            echo "<cell>". ($fila->G1135_C18908)."</cell>";

            echo "<cell>". $fila->G1135_C18909."</cell>"; 

            echo "<cell>". ($fila->G1135_C18910)."</cell>";

            echo "<cell>". $fila->G1135_C18911."</cell>"; 

            echo "<cell>". ($fila->G1135_C18645)."</cell>";

            if($fila->G1135_C17507 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C17507)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1135_C17508 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C17508)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1135_C17509)."</cell>";

            echo "<cell>". ($fila->G1135_C18646)."</cell>";

            echo "<cell>". $fila->G1135_C18647."</cell>"; 

            echo "<cell>". ($fila->G1135_C18648)."</cell>";

            echo "<cell>". ($fila->G1135_C18649)."</cell>";

            if($fila->G1135_C18650 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C18650)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1135_C18651 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C18651)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1135_C17512)."]]></cell>";

            echo "<cell>". ($fila->G1135_C17495)."</cell>";

            echo "<cell>". ($fila->G1135_C17496)."</cell>";

            echo "<cell>". $fila->G1135_C17497."</cell>"; 

            echo "<cell>". $fila->G1135_C17498."</cell>"; 

            if($fila->G1135_C17499 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C17499)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1135_C17500 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C17500)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1135_C17501)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_5"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1136_ConsInte__b, G1136_C20664, G1136_C17513, G1136_C17514, G1136_C20334, G1136_C20665, G1136_C20666, G1136_C17515, G1136_C17516, G1136_C17517, G1136_C17518, G1136_C17519, G1136_C17520, G1136_C17521, G1136_C17522, G1136_C17523, G1136_C17524, G1136_C18891, r.LISOPC_Nombre____b as  G1136_C18892, G1136_C18893, G1136_C18894, G1136_C17532, G1136_C17533, G1136_C17534, G1136_C17535, y.LISOPC_Nombre____b as  G1136_C18895, z.LISOPC_Nombre____b as  G1136_C18896, G1136_C18898, G1136_C18899, G1136_C18900, G1136_C17537, G1136_C17536, G1136_C17539, G1136_C17538, G1136_C17540, ak.LISOPC_Nombre____b as  G1136_C18644, al.LISOPC_Nombre____b as  G1136_C17525, am.LISOPC_Nombre____b as  G1136_C17526, G1136_C17527, G1136_C17528, G1136_C17529, G1136_C17530, G1136_C17531 FROM ".$BaseDatos.".G1136  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1136_C18892 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1136_C18895 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1136_C18896 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1136_C18897 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ak ON ak.LISOPC_ConsInte__b =  G1136_C18644 LEFT JOIN ".$BaseDatos_systema.".LISOPC as al ON al.LISOPC_ConsInte__b =  G1136_C17525 LEFT JOIN ".$BaseDatos_systema.".LISOPC as am ON am.LISOPC_ConsInte__b =  G1136_C17526 ";

        $SQL .= " WHERE G1136_C20664 = '".$numero."'"; 

        $SQL .= " ORDER BY G1136_C20664";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1136_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1136_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1136_C20664)."</cell>";

            echo "<cell>". ($fila->G1136_C17513)."</cell>";

            echo "<cell>". ($fila->G1136_C17514)."</cell>";

            echo "<cell>". ($fila->G1136_C20334)."</cell>";

            echo "<cell>". ($fila->G1136_C20665)."</cell>";

            echo "<cell>". ($fila->G1136_C20666)."</cell>";

            echo "<cell>". ($fila->G1136_C17515)."</cell>";

            echo "<cell>". ($fila->G1136_C17516)."</cell>";

            echo "<cell>". ($fila->G1136_C17517)."</cell>";

            echo "<cell>". ($fila->G1136_C17518)."</cell>";

            echo "<cell>". ($fila->G1136_C17519)."</cell>";

            echo "<cell>". ($fila->G1136_C17520)."</cell>";

            echo "<cell>". ($fila->G1136_C17521)."</cell>";

            echo "<cell>". ($fila->G1136_C17522)."</cell>";

            echo "<cell>". ($fila->G1136_C17523)."</cell>";

            echo "<cell>". ($fila->G1136_C17524)."</cell>";

            echo "<cell>". $fila->G1136_C18891."</cell>"; 

            echo "<cell>". ($fila->G1136_C18892)."</cell>";

            if($fila->G1136_C18893 != ''){
                echo "<cell>". explode(' ', $fila->G1136_C18893)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1136_C18894 != ''){
                echo "<cell>". explode(' ', $fila->G1136_C18894)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1136_C17532)."</cell>";

            echo "<cell>". ($fila->G1136_C17533)."</cell>";

            echo "<cell>". ($fila->G1136_C17534)."</cell>";

            echo "<cell>". ($fila->G1136_C17535)."</cell>";

            echo "<cell>". ($fila->G1136_C18895)."</cell>";

            echo "<cell>". ($fila->G1136_C18896)."</cell>";

            echo "<cell>". ($fila->G1136_C18897)."</cell>";

            echo "<cell>". $fila->G1136_C18898."</cell>"; 

            echo "<cell>". ($fila->G1136_C18899)."</cell>";

            echo "<cell>". $fila->G1136_C18900."</cell>"; 

            echo "<cell>". $fila->G1136_C17537."</cell>"; 

            echo "<cell>". $fila->G1136_C17536."</cell>"; 

            echo "<cell>". $fila->G1136_C17539."</cell>"; 

            echo "<cell>". $fila->G1136_C17538."</cell>"; 

            echo "<cell><![CDATA[". ($fila->G1136_C17540)."]]></cell>";

            echo "<cell>". ($fila->G1136_C18644)."</cell>";

            echo "<cell>". ($fila->G1136_C17525)."</cell>";

            echo "<cell>". ($fila->G1136_C17526)."</cell>";

            echo "<cell>". $fila->G1136_C17527."</cell>"; 

            echo "<cell>". $fila->G1136_C17528."</cell>"; 

            if($fila->G1136_C17529 != ''){
                echo "<cell>". explode(' ', $fila->G1136_C17529)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1136_C17530 != ''){
                echo "<cell>". explode(' ', $fila->G1136_C17530)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1136_C17531)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_6"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1138_ConsInte__b, G1138_C20673, G1138_C17565, G1138_C17566, G1138_C17567, G1138_C20674, G1138_C20675, G1138_C17568, G1138_C17569, G1138_C17570, G1138_C17571, G1138_C17572, G1138_C18922, G1138_C17573, G1138_C17574, G1138_C17575, G1138_C17576, G1138_C18923, r.LISOPC_Nombre____b as  G1138_C18924, G1138_C18925, G1138_C18926, G1138_C17584, G1138_C17585, G1138_C17586, G1138_C17587, y.LISOPC_Nombre____b as  G1138_C18927, z.LISOPC_Nombre____b as  G1138_C18928, G1138_C18930, G1138_C18931, G1138_C18932, G1138_C17588, ag.LISOPC_Nombre____b as  G1138_C17577, ah.LISOPC_Nombre____b as  G1138_C17578, G1138_C17579, G1138_C17580, G1138_C17581, G1138_C17582, G1138_C17583 FROM ".$BaseDatos.".G1138  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1138_C18924 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1138_C18927 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1138_C18928 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1138_C18929 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ag ON ag.LISOPC_ConsInte__b =  G1138_C17577 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ah ON ah.LISOPC_ConsInte__b =  G1138_C17578 ";

        $SQL .= " WHERE G1138_C20673 = '".$numero."'"; 

        $SQL .= " ORDER BY G1138_C20673";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1138_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1138_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1138_C20673)."</cell>";

            echo "<cell>". ($fila->G1138_C17565)."</cell>";

            echo "<cell>". ($fila->G1138_C17566)."</cell>";

            echo "<cell>". ($fila->G1138_C17567)."</cell>";

            echo "<cell>". ($fila->G1138_C20674)."</cell>";

            echo "<cell>". ($fila->G1138_C20675)."</cell>";

            echo "<cell>". ($fila->G1138_C17568)."</cell>";

            echo "<cell>". ($fila->G1138_C17569)."</cell>";

            echo "<cell>". ($fila->G1138_C17570)."</cell>";

            echo "<cell>". ($fila->G1138_C17571)."</cell>";

            echo "<cell>". ($fila->G1138_C17572)."</cell>";

            echo "<cell>". ($fila->G1138_C18922)."</cell>";

            echo "<cell>". ($fila->G1138_C17573)."</cell>";

            echo "<cell>". ($fila->G1138_C17574)."</cell>";

            echo "<cell>". ($fila->G1138_C17575)."</cell>";

            echo "<cell>". ($fila->G1138_C17576)."</cell>";

            echo "<cell>". $fila->G1138_C18923."</cell>"; 

            echo "<cell>". ($fila->G1138_C18924)."</cell>";

            if($fila->G1138_C18925 != ''){
                echo "<cell>". explode(' ', $fila->G1138_C18925)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1138_C18926 != ''){
                echo "<cell>". explode(' ', $fila->G1138_C18926)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1138_C17584)."</cell>";

            echo "<cell>". ($fila->G1138_C17585)."</cell>";

            echo "<cell>". ($fila->G1138_C17586)."</cell>";

            echo "<cell>". ($fila->G1138_C17587)."</cell>";

            echo "<cell>". ($fila->G1138_C18927)."</cell>";

            echo "<cell>". ($fila->G1138_C18928)."</cell>";

            echo "<cell>". ($fila->G1138_C18929)."</cell>";

            echo "<cell>". $fila->G1138_C18930."</cell>"; 

            echo "<cell>". ($fila->G1138_C18931)."</cell>";

            echo "<cell>". $fila->G1138_C18932."</cell>"; 

            echo "<cell><![CDATA[". ($fila->G1138_C17588)."]]></cell>";

            echo "<cell>". ($fila->G1138_C17577)."</cell>";

            echo "<cell>". ($fila->G1138_C17578)."</cell>";

            echo "<cell>". $fila->G1138_C17579."</cell>"; 

            echo "<cell>". $fila->G1138_C17580."</cell>"; 

            if($fila->G1138_C17581 != ''){
                echo "<cell>". explode(' ', $fila->G1138_C17581)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1138_C17582 != ''){
                echo "<cell>". explode(' ', $fila->G1138_C17582)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1138_C17583)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_7"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1139_ConsInte__b, G1139_C20682, G1139_C17589, G1139_C17590, G1139_C19164, G1139_C20683, G1139_C20684, G1139_C17591, G1139_C17592, G1139_C17593, G1139_C17594, G1139_C17595, G1139_C17596, G1139_C17597, G1139_C17598, G1139_C17599, G1139_C17600, G1139_C19165, r.LISOPC_Nombre____b as  G1139_C19166, G1139_C19167, G1139_C19168, G1139_C17608, G1139_C17609, G1139_C17610, G1139_C17611, y.LISOPC_Nombre____b as  G1139_C19169, z.LISOPC_Nombre____b as  G1139_C19170, G1139_C19172, G1139_C19173, G1139_C19174, af.LISOPC_Nombre____b as  G1139_C17612, G1139_C17613, ah.LISOPC_Nombre____b as  G1139_C17601, ai.LISOPC_Nombre____b as  G1139_C17602, G1139_C17603, G1139_C17604, G1139_C17605, G1139_C17606, G1139_C17607 FROM ".$BaseDatos.".G1139  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1139_C19166 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1139_C19169 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1139_C19170 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1139_C19171 LEFT JOIN ".$BaseDatos_systema.".LISOPC as af ON af.LISOPC_ConsInte__b =  G1139_C17612 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ah ON ah.LISOPC_ConsInte__b =  G1139_C17601 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ai ON ai.LISOPC_ConsInte__b =  G1139_C17602 ";

        $SQL .= " WHERE G1139_C20682 = '".$numero."'"; 

        $SQL .= " ORDER BY G1139_C20682";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1139_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1139_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1139_C20682)."</cell>";

            echo "<cell>". ($fila->G1139_C17589)."</cell>";

            echo "<cell>". ($fila->G1139_C17590)."</cell>";

            echo "<cell>". ($fila->G1139_C19164)."</cell>";

            echo "<cell>". ($fila->G1139_C20683)."</cell>";

            echo "<cell>". ($fila->G1139_C20684)."</cell>";

            echo "<cell>". ($fila->G1139_C17591)."</cell>";

            echo "<cell>". ($fila->G1139_C17592)."</cell>";

            echo "<cell>". ($fila->G1139_C17593)."</cell>";

            echo "<cell>". ($fila->G1139_C17594)."</cell>";

            echo "<cell>". ($fila->G1139_C17595)."</cell>";

            echo "<cell>". ($fila->G1139_C17596)."</cell>";

            echo "<cell>". ($fila->G1139_C17597)."</cell>";

            echo "<cell>". ($fila->G1139_C17598)."</cell>";

            echo "<cell>". ($fila->G1139_C17599)."</cell>";

            echo "<cell>". ($fila->G1139_C17600)."</cell>";

            echo "<cell>". $fila->G1139_C19165."</cell>"; 

            echo "<cell>". ($fila->G1139_C19166)."</cell>";

            if($fila->G1139_C19167 != ''){
                echo "<cell>". explode(' ', $fila->G1139_C19167)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1139_C19168 != ''){
                echo "<cell>". explode(' ', $fila->G1139_C19168)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1139_C17608)."</cell>";

            echo "<cell>". ($fila->G1139_C17609)."</cell>";

            echo "<cell>". ($fila->G1139_C17610)."</cell>";

            echo "<cell>". ($fila->G1139_C17611)."</cell>";

            echo "<cell>". ($fila->G1139_C19169)."</cell>";

            echo "<cell>". ($fila->G1139_C19170)."</cell>";

            echo "<cell>". ($fila->G1139_C19171)."</cell>";

            echo "<cell>". $fila->G1139_C19172."</cell>"; 

            echo "<cell>". ($fila->G1139_C19173)."</cell>";

            echo "<cell>". $fila->G1139_C19174."</cell>"; 

            echo "<cell>". ($fila->G1139_C17612)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1139_C17613)."]]></cell>";

            echo "<cell>". ($fila->G1139_C17601)."</cell>";

            echo "<cell>". ($fila->G1139_C17602)."</cell>";

            echo "<cell>". $fila->G1139_C17603."</cell>"; 

            echo "<cell>". $fila->G1139_C17604."</cell>"; 

            if($fila->G1139_C17605 != ''){
                echo "<cell>". explode(' ', $fila->G1139_C17605)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1139_C17606 != ''){
                echo "<cell>". explode(' ', $fila->G1139_C17606)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1139_C17607)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_8"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1140_ConsInte__b, G1140_C20685, G1140_C17614, G1140_C17615, G1140_C17616, G1140_C20686, G1140_C20687, G1140_C17617, G1140_C17618, G1140_C17619, G1140_C17620, G1140_C20202, G1140_C17621, G1140_C17622, G1140_C17623, G1140_C17624, G1140_C17625, G1140_C20203, r.LISOPC_Nombre____b as  G1140_C20204, G1140_C20205, G1140_C20206, G1140_C17633, G1140_C17634, G1140_C17635, G1140_C17636, y.LISOPC_Nombre____b as  G1140_C20231, z.LISOPC_Nombre____b as  G1140_C20232, G1140_C20234, G1140_C20235, G1140_C20236, af.LISOPC_Nombre____b as  G1140_C17637, G1140_C17638, ah.LISOPC_Nombre____b as  G1140_C17626, ai.LISOPC_Nombre____b as  G1140_C17627, G1140_C17628, G1140_C17629, G1140_C17630, G1140_C17631, G1140_C17632 FROM ".$BaseDatos.".G1140  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1140_C20204 LEFT JOIN ".$BaseDatos_systema.".LISOPC as y ON y.LISOPC_ConsInte__b =  G1140_C20231 LEFT JOIN ".$BaseDatos_systema.".LISOPC as z ON z.LISOPC_ConsInte__b =  G1140_C20232 LEFT JOIN ".$BaseDatos.".G1188 ON G1188_ConsInte__b  =  G1140_C20233 LEFT JOIN ".$BaseDatos_systema.".LISOPC as af ON af.LISOPC_ConsInte__b =  G1140_C17637 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ah ON ah.LISOPC_ConsInte__b =  G1140_C17626 LEFT JOIN ".$BaseDatos_systema.".LISOPC as ai ON ai.LISOPC_ConsInte__b =  G1140_C17627 ";

        $SQL .= " WHERE G1140_C20685 = '".$numero."'"; 

        $SQL .= " ORDER BY G1140_C20685";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1140_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1140_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1140_C20685)."</cell>";

            echo "<cell>". ($fila->G1140_C17614)."</cell>";

            echo "<cell>". ($fila->G1140_C17615)."</cell>";

            echo "<cell>". ($fila->G1140_C17616)."</cell>";

            echo "<cell>". ($fila->G1140_C20686)."</cell>";

            echo "<cell>". ($fila->G1140_C20687)."</cell>";

            echo "<cell>". ($fila->G1140_C17617)."</cell>";

            echo "<cell>". ($fila->G1140_C17618)."</cell>";

            echo "<cell>". ($fila->G1140_C17619)."</cell>";

            echo "<cell>". ($fila->G1140_C17620)."</cell>";

            echo "<cell>". ($fila->G1140_C20202)."</cell>";

            echo "<cell>". ($fila->G1140_C17621)."</cell>";

            echo "<cell>". ($fila->G1140_C17622)."</cell>";

            echo "<cell>". ($fila->G1140_C17623)."</cell>";

            echo "<cell>". ($fila->G1140_C17624)."</cell>";

            echo "<cell>". ($fila->G1140_C17625)."</cell>";

            echo "<cell>". $fila->G1140_C20203."</cell>"; 

            echo "<cell>". ($fila->G1140_C20204)."</cell>";

            if($fila->G1140_C20205 != ''){
                echo "<cell>". explode(' ', $fila->G1140_C20205)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1140_C20206 != ''){
                echo "<cell>". explode(' ', $fila->G1140_C20206)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1140_C17633)."</cell>";

            echo "<cell>". ($fila->G1140_C17634)."</cell>";

            echo "<cell>". ($fila->G1140_C17635)."</cell>";

            echo "<cell>". ($fila->G1140_C17636)."</cell>";

            echo "<cell>". ($fila->G1140_C20231)."</cell>";

            echo "<cell>". ($fila->G1140_C20232)."</cell>";

            echo "<cell>". ($fila->G1140_C20233)."</cell>";

            echo "<cell>". $fila->G1140_C20234."</cell>"; 

            echo "<cell>". ($fila->G1140_C20235)."</cell>";

            echo "<cell>". $fila->G1140_C20236."</cell>"; 

            echo "<cell>". ($fila->G1140_C17637)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1140_C17638)."]]></cell>";

            echo "<cell>". ($fila->G1140_C17626)."</cell>";

            echo "<cell>". ($fila->G1140_C17627)."</cell>";

            echo "<cell>". $fila->G1140_C17628."</cell>"; 

            echo "<cell>". $fila->G1140_C17629."</cell>"; 

            if($fila->G1140_C17630 != ''){
                echo "<cell>". explode(' ', $fila->G1140_C17630)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1140_C17631 != ''){
                echo "<cell>". explode(' ', $fila->G1140_C17631)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1140_C17632)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1226 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1226(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1226_C20658"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1226_C20658 = '".$_POST["G1226_C20658"]."'";
                $LsqlI .= $separador."G1226_C20658";
                $LsqlV .= $separador."'".$_POST["G1226_C20658"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1226_C20657 = $numero;
                    $LsqlU .= ", G1226_C20657 = ".$G1226_C20657."";
                    $LsqlI .= ", G1226_C20657";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1226_Usuario ,  G1226_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1226_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1226 WHERE  G1226_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1137 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1137(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1137_C20679"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C20679 = '".$_POST["G1137_C20679"]."'";
                $LsqlI .= $separador."G1137_C20679";
                $LsqlV .= $separador."'".$_POST["G1137_C20679"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17541"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17541 = '".$_POST["G1137_C17541"]."'";
                $LsqlI .= $separador."G1137_C17541";
                $LsqlV .= $separador."'".$_POST["G1137_C17541"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17542"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17542 = '".$_POST["G1137_C17542"]."'";
                $LsqlI .= $separador."G1137_C17542";
                $LsqlV .= $separador."'".$_POST["G1137_C17542"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17543"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17543 = '".$_POST["G1137_C17543"]."'";
                $LsqlI .= $separador."G1137_C17543";
                $LsqlV .= $separador."'".$_POST["G1137_C17543"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C20680"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C20680 = '".$_POST["G1137_C20680"]."'";
                $LsqlI .= $separador."G1137_C20680";
                $LsqlV .= $separador."'".$_POST["G1137_C20680"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C20681"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C20681 = '".$_POST["G1137_C20681"]."'";
                $LsqlI .= $separador."G1137_C20681";
                $LsqlV .= $separador."'".$_POST["G1137_C20681"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17544"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17544 = '".$_POST["G1137_C17544"]."'";
                $LsqlI .= $separador."G1137_C17544";
                $LsqlV .= $separador."'".$_POST["G1137_C17544"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17545 = '".$_POST["G1137_C17545"]."'";
                $LsqlI .= $separador."G1137_C17545";
                $LsqlV .= $separador."'".$_POST["G1137_C17545"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17546"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17546 = '".$_POST["G1137_C17546"]."'";
                $LsqlI .= $separador."G1137_C17546";
                $LsqlV .= $separador."'".$_POST["G1137_C17546"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17547"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17547 = '".$_POST["G1137_C17547"]."'";
                $LsqlI .= $separador."G1137_C17547";
                $LsqlV .= $separador."'".$_POST["G1137_C17547"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C19153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19153 = '".$_POST["G1137_C19153"]."'";
                $LsqlI .= $separador."G1137_C19153";
                $LsqlV .= $separador."'".$_POST["G1137_C19153"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17548 = '".$_POST["G1137_C17548"]."'";
                $LsqlI .= $separador."G1137_C17548";
                $LsqlV .= $separador."'".$_POST["G1137_C17548"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17549"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17549 = '".$_POST["G1137_C17549"]."'";
                $LsqlI .= $separador."G1137_C17549";
                $LsqlV .= $separador."'".$_POST["G1137_C17549"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17550"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17550 = '".$_POST["G1137_C17550"]."'";
                $LsqlI .= $separador."G1137_C17550";
                $LsqlV .= $separador."'".$_POST["G1137_C17550"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17551 = '".$_POST["G1137_C17551"]."'";
                $LsqlI .= $separador."G1137_C17551";
                $LsqlV .= $separador."'".$_POST["G1137_C17551"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17552"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17552 = '".$_POST["G1137_C17552"]."'";
                $LsqlI .= $separador."G1137_C17552";
                $LsqlV .= $separador."'".$_POST["G1137_C17552"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1137_C19154= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C19154"])){    
                if($_POST["G1137_C19154"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19154 = $_POST["G1137_C19154"];
                    $LsqlU .= $separador." G1137_C19154 = '".$G1137_C19154."'";
                    $LsqlI .= $separador." G1137_C19154";
                    $LsqlV .= $separador."'".$G1137_C19154."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1137_C19155"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19155 = '".$_POST["G1137_C19155"]."'";
                $LsqlI .= $separador."G1137_C19155";
                $LsqlV .= $separador."'".$_POST["G1137_C19155"]."'";
                $validar = 1;
            }

            $G1137_C19156 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1137_C19156"])){    
                if($_POST["G1137_C19156"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19156 = "'".str_replace(' ', '',$_POST["G1137_C19156"])." 00:00:00'";
                    $LsqlU .= $separador." G1137_C19156 = ".$G1137_C19156;
                    $LsqlI .= $separador." G1137_C19156";
                    $LsqlV .= $separador.$G1137_C19156;
                    $validar = 1;
                }
            }

            $G1137_C19157 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1137_C19157"])){    
                if($_POST["G1137_C19157"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19157 = "'".str_replace(' ', '',$_POST["G1137_C19157"])." 00:00:00'";
                    $LsqlU .= $separador." G1137_C19157 = ".$G1137_C19157;
                    $LsqlI .= $separador." G1137_C19157";
                    $LsqlV .= $separador.$G1137_C19157;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1137_C17560"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17560 = '".$_POST["G1137_C17560"]."'";
                $LsqlI .= $separador."G1137_C17560";
                $LsqlV .= $separador."'".$_POST["G1137_C17560"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17561"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17561 = '".$_POST["G1137_C17561"]."'";
                $LsqlI .= $separador."G1137_C17561";
                $LsqlV .= $separador."'".$_POST["G1137_C17561"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17562"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17562 = '".$_POST["G1137_C17562"]."'";
                $LsqlI .= $separador."G1137_C17562";
                $LsqlV .= $separador."'".$_POST["G1137_C17562"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1137_C17563"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17563 = '".$_POST["G1137_C17563"]."'";
                $LsqlI .= $separador."G1137_C17563";
                $LsqlV .= $separador."'".$_POST["G1137_C17563"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1137_C19158"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19158 = '".$_POST["G1137_C19158"]."'";
                $LsqlI .= $separador."G1137_C19158";
                $LsqlV .= $separador."'".$_POST["G1137_C19158"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1137_C19159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19159 = '".$_POST["G1137_C19159"]."'";
                $LsqlI .= $separador."G1137_C19159";
                $LsqlV .= $separador."'".$_POST["G1137_C19159"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1137_C19160"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19160 = '".$_POST["G1137_C19160"]."'";
                $LsqlI .= $separador."G1137_C19160";
                $LsqlV .= $separador."'".$_POST["G1137_C19160"]."'";
                $validar = 1;
            }
            
 
            $G1137_C19161= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C19161"])){    
                if($_POST["G1137_C19161"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19161 = $_POST["G1137_C19161"];
                    $LsqlU .= $separador." G1137_C19161 = '".$G1137_C19161."'";
                    $LsqlI .= $separador." G1137_C19161";
                    $LsqlV .= $separador."'".$G1137_C19161."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1137_C19162"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19162 = '".$_POST["G1137_C19162"]."'";
                $LsqlI .= $separador."G1137_C19162";
                $LsqlV .= $separador."'".$_POST["G1137_C19162"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1137_C19163= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C19163"])){    
                if($_POST["G1137_C19163"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19163 = $_POST["G1137_C19163"];
                    $LsqlU .= $separador." G1137_C19163 = '".$G1137_C19163."'";
                    $LsqlI .= $separador." G1137_C19163";
                    $LsqlV .= $separador."'".$G1137_C19163."'";
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1137_C17564"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17564 = '".$_POST["G1137_C17564"]."'";
                $LsqlI .= $separador."G1137_C17564";
                $LsqlV .= $separador."'".$_POST["G1137_C17564"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1137_C17553"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17553 = '".$_POST["G1137_C17553"]."'";
                $LsqlI .= $separador."G1137_C17553";
                $LsqlV .= $separador."'".$_POST["G1137_C17553"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1137_C17554"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17554 = '".$_POST["G1137_C17554"]."'";
                $LsqlI .= $separador."G1137_C17554";
                $LsqlV .= $separador."'".$_POST["G1137_C17554"]."'";
                $validar = 1;
            }
 
            $G1137_C17555= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C17555"])){    
                if($_POST["G1137_C17555"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C17555 = $_POST["G1137_C17555"];
                    $LsqlU .= $separador." G1137_C17555 = '".$G1137_C17555."'";
                    $LsqlI .= $separador." G1137_C17555";
                    $LsqlV .= $separador."'".$G1137_C17555."'";
                    $validar = 1;
                }
            }
 
            $G1137_C17556= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C17556"])){    
                if($_POST["G1137_C17556"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C17556 = $_POST["G1137_C17556"];
                    $LsqlU .= $separador." G1137_C17556 = '".$G1137_C17556."'";
                    $LsqlI .= $separador." G1137_C17556";
                    $LsqlV .= $separador."'".$G1137_C17556."'";
                    $validar = 1;
                }
            }

            $G1137_C17557 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1137_C17557"])){    
                if($_POST["G1137_C17557"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C17557 = "'".str_replace(' ', '',$_POST["G1137_C17557"])." 00:00:00'";
                    $LsqlU .= $separador." G1137_C17557 = ".$G1137_C17557;
                    $LsqlI .= $separador." G1137_C17557";
                    $LsqlV .= $separador.$G1137_C17557;
                    $validar = 1;
                }
            }
 
            $G1137_C17558 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1137_C17558"])){    
                if($_POST["G1137_C17558"] != '' && $_POST["G1137_C17558"] != 'undefined' && $_POST["G1137_C17558"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C17558 = "'".$fecha." ".str_replace(' ', '',$_POST["G1137_C17558"])."'";
                    $LsqlU .= $separador."  G1137_C17558 = ".$G1137_C17558."";
                    $LsqlI .= $separador."  G1137_C17558";
                    $LsqlV .= $separador.$G1137_C17558;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1137_C17559"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17559 = '".$_POST["G1137_C17559"]."'";
                $LsqlI .= $separador."G1137_C17559";
                $LsqlV .= $separador."'".$_POST["G1137_C17559"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1137_C0 = $numero;
                    $LsqlU .= ", G1137_C0 = ".$G1137_C0."";
                    $LsqlI .= ", G1137_C0";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1137_Usuario ,  G1137_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1137_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1137 WHERE  G1137_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_2"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1133 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1133(";
            $LsqlV = " VALUES ("; 
 
            $G1133_C20529= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C20529"])){    
                if($_POST["G1133_C20529"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C20529 = $_POST["G1133_C20529"];
                    $LsqlU .= $separador." G1133_C20529 = '".$G1133_C20529."'";
                    $LsqlI .= $separador." G1133_C20529";
                    $LsqlV .= $separador."'".$G1133_C20529."'";
                    $validar = 1;
                }
            }
 
            $G1133_C20530= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C20530"])){    
                if($_POST["G1133_C20530"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C20530 = $_POST["G1133_C20530"];
                    $LsqlU .= $separador." G1133_C20530 = '".$G1133_C20530."'";
                    $LsqlI .= $separador." G1133_C20530";
                    $LsqlV .= $separador."'".$G1133_C20530."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1133_C17434"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17434 = '".$_POST["G1133_C17434"]."'";
                $LsqlI .= $separador."G1133_C17434";
                $LsqlV .= $separador."'".$_POST["G1133_C17434"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17435"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17435 = '".$_POST["G1133_C17435"]."'";
                $LsqlI .= $separador."G1133_C17435";
                $LsqlV .= $separador."'".$_POST["G1133_C17435"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17436"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17436 = '".$_POST["G1133_C17436"]."'";
                $LsqlI .= $separador."G1133_C17436";
                $LsqlV .= $separador."'".$_POST["G1133_C17436"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C20677"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C20677 = '".$_POST["G1133_C20677"]."'";
                $LsqlI .= $separador."G1133_C20677";
                $LsqlV .= $separador."'".$_POST["G1133_C20677"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C20678"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C20678 = '".$_POST["G1133_C20678"]."'";
                $LsqlI .= $separador."G1133_C20678";
                $LsqlV .= $separador."'".$_POST["G1133_C20678"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17437"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17437 = '".$_POST["G1133_C17437"]."'";
                $LsqlI .= $separador."G1133_C17437";
                $LsqlV .= $separador."'".$_POST["G1133_C17437"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17438"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17438 = '".$_POST["G1133_C17438"]."'";
                $LsqlI .= $separador."G1133_C17438";
                $LsqlV .= $separador."'".$_POST["G1133_C17438"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17439"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17439 = '".$_POST["G1133_C17439"]."'";
                $LsqlI .= $separador."G1133_C17439";
                $LsqlV .= $separador."'".$_POST["G1133_C17439"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17440"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17440 = '".$_POST["G1133_C17440"]."'";
                $LsqlI .= $separador."G1133_C17440";
                $LsqlV .= $separador."'".$_POST["G1133_C17440"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C18933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18933 = '".$_POST["G1133_C18933"]."'";
                $LsqlI .= $separador."G1133_C18933";
                $LsqlV .= $separador."'".$_POST["G1133_C18933"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17441"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17441 = '".$_POST["G1133_C17441"]."'";
                $LsqlI .= $separador."G1133_C17441";
                $LsqlV .= $separador."'".$_POST["G1133_C17441"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17442"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17442 = '".$_POST["G1133_C17442"]."'";
                $LsqlI .= $separador."G1133_C17442";
                $LsqlV .= $separador."'".$_POST["G1133_C17442"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17443"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17443 = '".$_POST["G1133_C17443"]."'";
                $LsqlI .= $separador."G1133_C17443";
                $LsqlV .= $separador."'".$_POST["G1133_C17443"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17444"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17444 = '".$_POST["G1133_C17444"]."'";
                $LsqlI .= $separador."G1133_C17444";
                $LsqlV .= $separador."'".$_POST["G1133_C17444"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17445"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17445 = '".$_POST["G1133_C17445"]."'";
                $LsqlI .= $separador."G1133_C17445";
                $LsqlV .= $separador."'".$_POST["G1133_C17445"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1133_C18934= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18934"])){    
                if($_POST["G1133_C18934"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18934 = $_POST["G1133_C18934"];
                    $LsqlU .= $separador." G1133_C18934 = '".$G1133_C18934."'";
                    $LsqlI .= $separador." G1133_C18934";
                    $LsqlV .= $separador."'".$G1133_C18934."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1133_C18935"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18935 = '".$_POST["G1133_C18935"]."'";
                $LsqlI .= $separador."G1133_C18935";
                $LsqlV .= $separador."'".$_POST["G1133_C18935"]."'";
                $validar = 1;
            }

            $G1133_C18936 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C18936"])){    
                if($_POST["G1133_C18936"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18936 = "'".str_replace(' ', '',$_POST["G1133_C18936"])." 00:00:00'";
                    $LsqlU .= $separador." G1133_C18936 = ".$G1133_C18936;
                    $LsqlI .= $separador." G1133_C18936";
                    $LsqlV .= $separador.$G1133_C18936;
                    $validar = 1;
                }
            }

            $G1133_C18937 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C18937"])){    
                if($_POST["G1133_C18937"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18937 = "'".str_replace(' ', '',$_POST["G1133_C18937"])." 00:00:00'";
                    $LsqlU .= $separador." G1133_C18937 = ".$G1133_C18937;
                    $LsqlI .= $separador." G1133_C18937";
                    $LsqlV .= $separador.$G1133_C18937;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1133_C17451"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17451 = '".$_POST["G1133_C17451"]."'";
                $LsqlI .= $separador."G1133_C17451";
                $LsqlV .= $separador."'".$_POST["G1133_C17451"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17452"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17452 = '".$_POST["G1133_C17452"]."'";
                $LsqlI .= $separador."G1133_C17452";
                $LsqlV .= $separador."'".$_POST["G1133_C17452"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17453"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17453 = '".$_POST["G1133_C17453"]."'";
                $LsqlI .= $separador."G1133_C17453";
                $LsqlV .= $separador."'".$_POST["G1133_C17453"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1133_C17454"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17454 = '".$_POST["G1133_C17454"]."'";
                $LsqlI .= $separador."G1133_C17454";
                $LsqlV .= $separador."'".$_POST["G1133_C17454"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1133_C18938"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18938 = '".$_POST["G1133_C18938"]."'";
                $LsqlI .= $separador."G1133_C18938";
                $LsqlV .= $separador."'".$_POST["G1133_C18938"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1133_C18939"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18939 = '".$_POST["G1133_C18939"]."'";
                $LsqlI .= $separador."G1133_C18939";
                $LsqlV .= $separador."'".$_POST["G1133_C18939"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1133_C18940"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18940 = '".$_POST["G1133_C18940"]."'";
                $LsqlI .= $separador."G1133_C18940";
                $LsqlV .= $separador."'".$_POST["G1133_C18940"]."'";
                $validar = 1;
            }
            
 
            $G1133_C18941= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18941"])){    
                if($_POST["G1133_C18941"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18941 = $_POST["G1133_C18941"];
                    $LsqlU .= $separador." G1133_C18941 = '".$G1133_C18941."'";
                    $LsqlI .= $separador." G1133_C18941";
                    $LsqlV .= $separador."'".$G1133_C18941."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1133_C18942"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18942 = '".$_POST["G1133_C18942"]."'";
                $LsqlI .= $separador."G1133_C18942";
                $LsqlV .= $separador."'".$_POST["G1133_C18942"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1133_C18943= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18943"])){    
                if($_POST["G1133_C18943"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18943 = $_POST["G1133_C18943"];
                    $LsqlU .= $separador." G1133_C18943 = '".$G1133_C18943."'";
                    $LsqlI .= $separador." G1133_C18943";
                    $LsqlV .= $separador."'".$G1133_C18943."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1133_C18554"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18554 = '".$_POST["G1133_C18554"]."'";
                $LsqlI .= $separador."G1133_C18554";
                $LsqlV .= $separador."'".$_POST["G1133_C18554"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  
            $G1133_C17456 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C17456"])){    
                if($_POST["G1133_C17456"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C17456 = $_POST["G1133_C17456"];
                    $LsqlU .= $separador." G1133_C17456 = '".$G1133_C17456."'";
                    $LsqlI .= $separador." G1133_C17456";
                    $LsqlV .= $separador."'".$G1133_C17456."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1133_C18555"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18555 = '".$_POST["G1133_C18555"]."'";
                $LsqlI .= $separador."G1133_C18555";
                $LsqlV .= $separador."'".$_POST["G1133_C18555"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  
            $G1133_C18556 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18556"])){    
                if($_POST["G1133_C18556"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18556 = $_POST["G1133_C18556"];
                    $LsqlU .= $separador." G1133_C18556 = '".$G1133_C18556."'";
                    $LsqlI .= $separador." G1133_C18556";
                    $LsqlV .= $separador."'".$G1133_C18556."'";
                    $validar = 1;
                }
            }
  
            $G1133_C18557 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18557"])){    
                if($_POST["G1133_C18557"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18557 = $_POST["G1133_C18557"];
                    $LsqlU .= $separador." G1133_C18557 = '".$G1133_C18557."'";
                    $LsqlI .= $separador." G1133_C18557";
                    $LsqlV .= $separador."'".$G1133_C18557."'";
                    $validar = 1;
                }
            }
  
            $G1133_C18558 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18558"])){    
                if($_POST["G1133_C18558"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18558 = $_POST["G1133_C18558"];
                    $LsqlU .= $separador." G1133_C18558 = '".$G1133_C18558."'";
                    $LsqlI .= $separador." G1133_C18558";
                    $LsqlV .= $separador."'".$G1133_C18558."'";
                    $validar = 1;
                }
            }

            $G1133_C18559 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C18559"])){    
                if($_POST["G1133_C18559"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18559 = "'".str_replace(' ', '',$_POST["G1133_C18559"])." 00:00:00'";
                    $LsqlU .= $separador." G1133_C18559 = ".$G1133_C18559;
                    $LsqlI .= $separador." G1133_C18559";
                    $LsqlV .= $separador.$G1133_C18559;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1133_C18560"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18560 = '".$_POST["G1133_C18560"]."'";
                $LsqlI .= $separador."G1133_C18560";
                $LsqlV .= $separador."'".$_POST["G1133_C18560"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1133_C18561"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18561 = '".$_POST["G1133_C18561"]."'";
                $LsqlI .= $separador."G1133_C18561";
                $LsqlV .= $separador."'".$_POST["G1133_C18561"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1133_C17446"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17446 = '".$_POST["G1133_C17446"]."'";
                $LsqlI .= $separador."G1133_C17446";
                $LsqlV .= $separador."'".$_POST["G1133_C17446"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1133_C17447"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17447 = '".$_POST["G1133_C17447"]."'";
                $LsqlI .= $separador."G1133_C17447";
                $LsqlV .= $separador."'".$_POST["G1133_C17447"]."'";
                $validar = 1;
            }

            $G1133_C17448 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C17448"])){    
                if($_POST["G1133_C17448"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C17448 = "'".str_replace(' ', '',$_POST["G1133_C17448"])." 00:00:00'";
                    $LsqlU .= $separador." G1133_C17448 = ".$G1133_C17448;
                    $LsqlI .= $separador." G1133_C17448";
                    $LsqlV .= $separador.$G1133_C17448;
                    $validar = 1;
                }
            }
 
            $G1133_C17449 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1133_C17449"])){    
                if($_POST["G1133_C17449"] != '' && $_POST["G1133_C17449"] != 'undefined' && $_POST["G1133_C17449"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C17449 = "'".$fecha." ".str_replace(' ', '',$_POST["G1133_C17449"])."'";
                    $LsqlU .= $separador."  G1133_C17449 = ".$G1133_C17449."";
                    $LsqlI .= $separador."  G1133_C17449";
                    $LsqlV .= $separador.$G1133_C17449;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1133_C17450"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17450 = '".$_POST["G1133_C17450"]."'";
                $LsqlI .= $separador."G1133_C17450";
                $LsqlV .= $separador."'".$_POST["G1133_C17450"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1133_C20676 = $numero;
                    $LsqlU .= ", G1133_C20676 = ".$G1133_C20676."";
                    $LsqlI .= ", G1133_C20676";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1133_Usuario ,  G1133_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1133_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1133 WHERE  G1133_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_3"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1134 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1134(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1134_C17457"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17457 = '".$_POST["G1134_C17457"]."'";
                $LsqlI .= $separador."G1134_C17457";
                $LsqlV .= $separador."'".$_POST["G1134_C17457"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17458"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17458 = '".$_POST["G1134_C17458"]."'";
                $LsqlI .= $separador."G1134_C17458";
                $LsqlV .= $separador."'".$_POST["G1134_C17458"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C20333"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20333 = '".$_POST["G1134_C20333"]."'";
                $LsqlI .= $separador."G1134_C20333";
                $LsqlV .= $separador."'".$_POST["G1134_C20333"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C20671"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20671 = '".$_POST["G1134_C20671"]."'";
                $LsqlI .= $separador."G1134_C20671";
                $LsqlV .= $separador."'".$_POST["G1134_C20671"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C20672"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20672 = '".$_POST["G1134_C20672"]."'";
                $LsqlI .= $separador."G1134_C20672";
                $LsqlV .= $separador."'".$_POST["G1134_C20672"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17459"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17459 = '".$_POST["G1134_C17459"]."'";
                $LsqlI .= $separador."G1134_C17459";
                $LsqlV .= $separador."'".$_POST["G1134_C17459"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17460"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17460 = '".$_POST["G1134_C17460"]."'";
                $LsqlI .= $separador."G1134_C17460";
                $LsqlV .= $separador."'".$_POST["G1134_C17460"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17461"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17461 = '".$_POST["G1134_C17461"]."'";
                $LsqlI .= $separador."G1134_C17461";
                $LsqlV .= $separador."'".$_POST["G1134_C17461"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17462"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17462 = '".$_POST["G1134_C17462"]."'";
                $LsqlI .= $separador."G1134_C17462";
                $LsqlV .= $separador."'".$_POST["G1134_C17462"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17463"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17463 = '".$_POST["G1134_C17463"]."'";
                $LsqlI .= $separador."G1134_C17463";
                $LsqlV .= $separador."'".$_POST["G1134_C17463"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1134_C18912= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18912"])){    
                if($_POST["G1134_C18912"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18912 = $_POST["G1134_C18912"];
                    $LsqlU .= $separador." G1134_C18912 = '".$G1134_C18912."'";
                    $LsqlI .= $separador." G1134_C18912";
                    $LsqlV .= $separador."'".$G1134_C18912."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1134_C17464"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17464 = '".$_POST["G1134_C17464"]."'";
                $LsqlI .= $separador."G1134_C17464";
                $LsqlV .= $separador."'".$_POST["G1134_C17464"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17465"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17465 = '".$_POST["G1134_C17465"]."'";
                $LsqlI .= $separador."G1134_C17465";
                $LsqlV .= $separador."'".$_POST["G1134_C17465"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17466"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17466 = '".$_POST["G1134_C17466"]."'";
                $LsqlI .= $separador."G1134_C17466";
                $LsqlV .= $separador."'".$_POST["G1134_C17466"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17467"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17467 = '".$_POST["G1134_C17467"]."'";
                $LsqlI .= $separador."G1134_C17467";
                $LsqlV .= $separador."'".$_POST["G1134_C17467"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17468"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17468 = '".$_POST["G1134_C17468"]."'";
                $LsqlI .= $separador."G1134_C17468";
                $LsqlV .= $separador."'".$_POST["G1134_C17468"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1134_C18913"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18913 = '".$_POST["G1134_C18913"]."'";
                $LsqlI .= $separador."G1134_C18913";
                $LsqlV .= $separador."'".$_POST["G1134_C18913"]."'";
                $validar = 1;
            }

            $G1134_C18914 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C18914"])){    
                if($_POST["G1134_C18914"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18914 = "'".str_replace(' ', '',$_POST["G1134_C18914"])." 00:00:00'";
                    $LsqlU .= $separador." G1134_C18914 = ".$G1134_C18914;
                    $LsqlI .= $separador." G1134_C18914";
                    $LsqlV .= $separador.$G1134_C18914;
                    $validar = 1;
                }
            }

            $G1134_C18915 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C18915"])){    
                if($_POST["G1134_C18915"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18915 = "'".str_replace(' ', '',$_POST["G1134_C18915"])." 00:00:00'";
                    $LsqlU .= $separador." G1134_C18915 = ".$G1134_C18915;
                    $LsqlI .= $separador." G1134_C18915";
                    $LsqlV .= $separador.$G1134_C18915;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1134_C17476"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17476 = '".$_POST["G1134_C17476"]."'";
                $LsqlI .= $separador."G1134_C17476";
                $LsqlV .= $separador."'".$_POST["G1134_C17476"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17477"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17477 = '".$_POST["G1134_C17477"]."'";
                $LsqlI .= $separador."G1134_C17477";
                $LsqlV .= $separador."'".$_POST["G1134_C17477"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17478"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17478 = '".$_POST["G1134_C17478"]."'";
                $LsqlI .= $separador."G1134_C17478";
                $LsqlV .= $separador."'".$_POST["G1134_C17478"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1134_C17479"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17479 = '".$_POST["G1134_C17479"]."'";
                $LsqlI .= $separador."G1134_C17479";
                $LsqlV .= $separador."'".$_POST["G1134_C17479"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1134_C18916"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18916 = '".$_POST["G1134_C18916"]."'";
                $LsqlI .= $separador."G1134_C18916";
                $LsqlV .= $separador."'".$_POST["G1134_C18916"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1134_C18917"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18917 = '".$_POST["G1134_C18917"]."'";
                $LsqlI .= $separador."G1134_C18917";
                $LsqlV .= $separador."'".$_POST["G1134_C18917"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1134_C18918"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18918 = '".$_POST["G1134_C18918"]."'";
                $LsqlI .= $separador."G1134_C18918";
                $LsqlV .= $separador."'".$_POST["G1134_C18918"]."'";
                $validar = 1;
            }
            
 
            $G1134_C18919= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18919"])){    
                if($_POST["G1134_C18919"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18919 = $_POST["G1134_C18919"];
                    $LsqlU .= $separador." G1134_C18919 = '".$G1134_C18919."'";
                    $LsqlI .= $separador." G1134_C18919";
                    $LsqlV .= $separador."'".$G1134_C18919."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1134_C18920"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18920 = '".$_POST["G1134_C18920"]."'";
                $LsqlI .= $separador."G1134_C18920";
                $LsqlV .= $separador."'".$_POST["G1134_C18920"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1134_C18921= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18921"])){    
                if($_POST["G1134_C18921"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18921 = $_POST["G1134_C18921"];
                    $LsqlU .= $separador." G1134_C18921 = '".$G1134_C18921."'";
                    $LsqlI .= $separador." G1134_C18921";
                    $LsqlV .= $separador."'".$G1134_C18921."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1134_C18344"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18344 = '".$_POST["G1134_C18344"]."'";
                $LsqlI .= $separador."G1134_C18344";
                $LsqlV .= $separador."'".$_POST["G1134_C18344"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  
            $G1134_C17481 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C17481"])){    
                if($_POST["G1134_C17481"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17481 = $_POST["G1134_C17481"];
                    $LsqlU .= $separador." G1134_C17481 = '".$G1134_C17481."'";
                    $LsqlI .= $separador." G1134_C17481";
                    $LsqlV .= $separador."'".$G1134_C17481."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1134_C18345"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18345 = '".$_POST["G1134_C18345"]."'";
                $LsqlI .= $separador."G1134_C18345";
                $LsqlV .= $separador."'".$_POST["G1134_C18345"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  
            $G1134_C18346 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18346"])){    
                if($_POST["G1134_C18346"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18346 = $_POST["G1134_C18346"];
                    $LsqlU .= $separador." G1134_C18346 = '".$G1134_C18346."'";
                    $LsqlI .= $separador." G1134_C18346";
                    $LsqlV .= $separador."'".$G1134_C18346."'";
                    $validar = 1;
                }
            }
  
            $G1134_C18347 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18347"])){    
                if($_POST["G1134_C18347"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18347 = $_POST["G1134_C18347"];
                    $LsqlU .= $separador." G1134_C18347 = '".$G1134_C18347."'";
                    $LsqlI .= $separador." G1134_C18347";
                    $LsqlV .= $separador."'".$G1134_C18347."'";
                    $validar = 1;
                }
            }
  
            $G1134_C18348 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18348"])){    
                if($_POST["G1134_C18348"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18348 = $_POST["G1134_C18348"];
                    $LsqlU .= $separador." G1134_C18348 = '".$G1134_C18348."'";
                    $LsqlI .= $separador." G1134_C18348";
                    $LsqlV .= $separador."'".$G1134_C18348."'";
                    $validar = 1;
                }
            }

            $G1134_C18349 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C18349"])){    
                if($_POST["G1134_C18349"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18349 = "'".str_replace(' ', '',$_POST["G1134_C18349"])." 00:00:00'";
                    $LsqlU .= $separador." G1134_C18349 = ".$G1134_C18349;
                    $LsqlI .= $separador." G1134_C18349";
                    $LsqlV .= $separador.$G1134_C18349;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1134_C18350"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18350 = '".$_POST["G1134_C18350"]."'";
                $LsqlI .= $separador."G1134_C18350";
                $LsqlV .= $separador."'".$_POST["G1134_C18350"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1134_C17482"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17482 = '".$_POST["G1134_C17482"]."'";
                $LsqlI .= $separador."G1134_C17482";
                $LsqlV .= $separador."'".$_POST["G1134_C17482"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1134_C17469"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17469 = '".$_POST["G1134_C17469"]."'";
                $LsqlI .= $separador."G1134_C17469";
                $LsqlV .= $separador."'".$_POST["G1134_C17469"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1134_C17470"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17470 = '".$_POST["G1134_C17470"]."'";
                $LsqlI .= $separador."G1134_C17470";
                $LsqlV .= $separador."'".$_POST["G1134_C17470"]."'";
                $validar = 1;
            }
 
            $G1134_C17471= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C17471"])){    
                if($_POST["G1134_C17471"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17471 = $_POST["G1134_C17471"];
                    $LsqlU .= $separador." G1134_C17471 = '".$G1134_C17471."'";
                    $LsqlI .= $separador." G1134_C17471";
                    $LsqlV .= $separador."'".$G1134_C17471."'";
                    $validar = 1;
                }
            }
 
            $G1134_C17472= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C17472"])){    
                if($_POST["G1134_C17472"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17472 = $_POST["G1134_C17472"];
                    $LsqlU .= $separador." G1134_C17472 = '".$G1134_C17472."'";
                    $LsqlI .= $separador." G1134_C17472";
                    $LsqlV .= $separador."'".$G1134_C17472."'";
                    $validar = 1;
                }
            }

            $G1134_C17473 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C17473"])){    
                if($_POST["G1134_C17473"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17473 = "'".str_replace(' ', '',$_POST["G1134_C17473"])." 00:00:00'";
                    $LsqlU .= $separador." G1134_C17473 = ".$G1134_C17473;
                    $LsqlI .= $separador." G1134_C17473";
                    $LsqlV .= $separador.$G1134_C17473;
                    $validar = 1;
                }
            }
 
            $G1134_C17474 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1134_C17474"])){    
                if($_POST["G1134_C17474"] != '' && $_POST["G1134_C17474"] != 'undefined' && $_POST["G1134_C17474"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17474 = "'".$fecha." ".str_replace(' ', '',$_POST["G1134_C17474"])."'";
                    $LsqlU .= $separador."  G1134_C17474 = ".$G1134_C17474."";
                    $LsqlI .= $separador."  G1134_C17474";
                    $LsqlV .= $separador.$G1134_C17474;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1134_C17475"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17475 = '".$_POST["G1134_C17475"]."'";
                $LsqlI .= $separador."G1134_C17475";
                $LsqlV .= $separador."'".$_POST["G1134_C17475"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1134_C20670 = $numero;
                    $LsqlU .= ", G1134_C20670 = ".$G1134_C20670."";
                    $LsqlI .= ", G1134_C20670";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1134_Usuario ,  G1134_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1134_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1134 WHERE  G1134_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_4"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1135 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1135(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1135_C17483"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17483 = '".$_POST["G1135_C17483"]."'";
                $LsqlI .= $separador."G1135_C17483";
                $LsqlV .= $separador."'".$_POST["G1135_C17483"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17484"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17484 = '".$_POST["G1135_C17484"]."'";
                $LsqlI .= $separador."G1135_C17484";
                $LsqlV .= $separador."'".$_POST["G1135_C17484"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17485"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17485 = '".$_POST["G1135_C17485"]."'";
                $LsqlI .= $separador."G1135_C17485";
                $LsqlV .= $separador."'".$_POST["G1135_C17485"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C20668"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C20668 = '".$_POST["G1135_C20668"]."'";
                $LsqlI .= $separador."G1135_C20668";
                $LsqlV .= $separador."'".$_POST["G1135_C20668"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C20669"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C20669 = '".$_POST["G1135_C20669"]."'";
                $LsqlI .= $separador."G1135_C20669";
                $LsqlV .= $separador."'".$_POST["G1135_C20669"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17486"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17486 = '".$_POST["G1135_C17486"]."'";
                $LsqlI .= $separador."G1135_C17486";
                $LsqlV .= $separador."'".$_POST["G1135_C17486"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17487"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17487 = '".$_POST["G1135_C17487"]."'";
                $LsqlI .= $separador."G1135_C17487";
                $LsqlV .= $separador."'".$_POST["G1135_C17487"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17488"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17488 = '".$_POST["G1135_C17488"]."'";
                $LsqlI .= $separador."G1135_C17488";
                $LsqlV .= $separador."'".$_POST["G1135_C17488"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17489"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17489 = '".$_POST["G1135_C17489"]."'";
                $LsqlI .= $separador."G1135_C17489";
                $LsqlV .= $separador."'".$_POST["G1135_C17489"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C18901"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18901 = '".$_POST["G1135_C18901"]."'";
                $LsqlI .= $separador."G1135_C18901";
                $LsqlV .= $separador."'".$_POST["G1135_C18901"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17490"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17490 = '".$_POST["G1135_C17490"]."'";
                $LsqlI .= $separador."G1135_C17490";
                $LsqlV .= $separador."'".$_POST["G1135_C17490"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17491"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17491 = '".$_POST["G1135_C17491"]."'";
                $LsqlI .= $separador."G1135_C17491";
                $LsqlV .= $separador."'".$_POST["G1135_C17491"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17492"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17492 = '".$_POST["G1135_C17492"]."'";
                $LsqlI .= $separador."G1135_C17492";
                $LsqlV .= $separador."'".$_POST["G1135_C17492"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17493"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17493 = '".$_POST["G1135_C17493"]."'";
                $LsqlI .= $separador."G1135_C17493";
                $LsqlV .= $separador."'".$_POST["G1135_C17493"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17494"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17494 = '".$_POST["G1135_C17494"]."'";
                $LsqlI .= $separador."G1135_C17494";
                $LsqlV .= $separador."'".$_POST["G1135_C17494"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C18902"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18902 = '".$_POST["G1135_C18902"]."'";
                $LsqlI .= $separador."G1135_C18902";
                $LsqlV .= $separador."'".$_POST["G1135_C18902"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1135_C18903"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18903 = '".$_POST["G1135_C18903"]."'";
                $LsqlI .= $separador."G1135_C18903";
                $LsqlV .= $separador."'".$_POST["G1135_C18903"]."'";
                $validar = 1;
            }

            $G1135_C18904 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18904"])){    
                if($_POST["G1135_C18904"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18904 = "'".str_replace(' ', '',$_POST["G1135_C18904"])." 00:00:00'";
                    $LsqlU .= $separador." G1135_C18904 = ".$G1135_C18904;
                    $LsqlI .= $separador." G1135_C18904";
                    $LsqlV .= $separador.$G1135_C18904;
                    $validar = 1;
                }
            }

            $G1135_C18905 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18905"])){    
                if($_POST["G1135_C18905"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18905 = "'".str_replace(' ', '',$_POST["G1135_C18905"])." 00:00:00'";
                    $LsqlU .= $separador." G1135_C18905 = ".$G1135_C18905;
                    $LsqlI .= $separador." G1135_C18905";
                    $LsqlV .= $separador.$G1135_C18905;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1135_C17502"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17502 = '".$_POST["G1135_C17502"]."'";
                $LsqlI .= $separador."G1135_C17502";
                $LsqlV .= $separador."'".$_POST["G1135_C17502"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17503"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17503 = '".$_POST["G1135_C17503"]."'";
                $LsqlI .= $separador."G1135_C17503";
                $LsqlV .= $separador."'".$_POST["G1135_C17503"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17504"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17504 = '".$_POST["G1135_C17504"]."'";
                $LsqlI .= $separador."G1135_C17504";
                $LsqlV .= $separador."'".$_POST["G1135_C17504"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C17505"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17505 = '".$_POST["G1135_C17505"]."'";
                $LsqlI .= $separador."G1135_C17505";
                $LsqlV .= $separador."'".$_POST["G1135_C17505"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1135_C18906"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18906 = '".$_POST["G1135_C18906"]."'";
                $LsqlI .= $separador."G1135_C18906";
                $LsqlV .= $separador."'".$_POST["G1135_C18906"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1135_C18907"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18907 = '".$_POST["G1135_C18907"]."'";
                $LsqlI .= $separador."G1135_C18907";
                $LsqlV .= $separador."'".$_POST["G1135_C18907"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1135_C18908"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18908 = '".$_POST["G1135_C18908"]."'";
                $LsqlI .= $separador."G1135_C18908";
                $LsqlV .= $separador."'".$_POST["G1135_C18908"]."'";
                $validar = 1;
            }
            
 
            $G1135_C18909= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C18909"])){    
                if($_POST["G1135_C18909"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18909 = $_POST["G1135_C18909"];
                    $LsqlU .= $separador." G1135_C18909 = '".$G1135_C18909."'";
                    $LsqlI .= $separador." G1135_C18909";
                    $LsqlV .= $separador."'".$G1135_C18909."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1135_C18910"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18910 = '".$_POST["G1135_C18910"]."'";
                $LsqlI .= $separador."G1135_C18910";
                $LsqlV .= $separador."'".$_POST["G1135_C18910"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1135_C18911= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C18911"])){    
                if($_POST["G1135_C18911"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18911 = $_POST["G1135_C18911"];
                    $LsqlU .= $separador." G1135_C18911 = '".$G1135_C18911."'";
                    $LsqlI .= $separador." G1135_C18911";
                    $LsqlV .= $separador."'".$G1135_C18911."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1135_C18645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18645 = '".$_POST["G1135_C18645"]."'";
                $LsqlI .= $separador."G1135_C18645";
                $LsqlV .= $separador."'".$_POST["G1135_C18645"]."'";
                $validar = 1;
            }

            $G1135_C17507 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C17507"])){    
                if($_POST["G1135_C17507"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17507 = "'".str_replace(' ', '',$_POST["G1135_C17507"])." 00:00:00'";
                    $LsqlU .= $separador." G1135_C17507 = ".$G1135_C17507;
                    $LsqlI .= $separador." G1135_C17507";
                    $LsqlV .= $separador.$G1135_C17507;
                    $validar = 1;
                }
            }
 
            $G1135_C17508 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1135_C17508"])){    
                if($_POST["G1135_C17508"] != '' && $_POST["G1135_C17508"] != 'undefined' && $_POST["G1135_C17508"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17508 = "'".$fecha." ".str_replace(' ', '',$_POST["G1135_C17508"])."'";
                    $LsqlU .= $separador."  G1135_C17508 = ".$G1135_C17508."";
                    $LsqlI .= $separador."  G1135_C17508";
                    $LsqlV .= $separador.$G1135_C17508;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1135_C17509"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17509 = '".$_POST["G1135_C17509"]."'";
                $LsqlI .= $separador."G1135_C17509";
                $LsqlV .= $separador."'".$_POST["G1135_C17509"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C18646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18646 = '".$_POST["G1135_C18646"]."'";
                $LsqlI .= $separador."G1135_C18646";
                $LsqlV .= $separador."'".$_POST["G1135_C18646"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  
            $G1135_C18647 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C18647"])){    
                if($_POST["G1135_C18647"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18647 = $_POST["G1135_C18647"];
                    $LsqlU .= $separador." G1135_C18647 = '".$G1135_C18647."'";
                    $LsqlI .= $separador." G1135_C18647";
                    $LsqlV .= $separador."'".$G1135_C18647."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1135_C18648"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18648 = '".$_POST["G1135_C18648"]."'";
                $LsqlI .= $separador."G1135_C18648";
                $LsqlV .= $separador."'".$_POST["G1135_C18648"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1135_C18649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C18649 = '".$_POST["G1135_C18649"]."'";
                $LsqlI .= $separador."G1135_C18649";
                $LsqlV .= $separador."'".$_POST["G1135_C18649"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1135_C18650 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18650"])){    
                if($_POST["G1135_C18650"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18650 = "'".str_replace(' ', '',$_POST["G1135_C18650"])." 00:00:00'";
                    $LsqlU .= $separador." G1135_C18650 = ".$G1135_C18650;
                    $LsqlI .= $separador." G1135_C18650";
                    $LsqlV .= $separador.$G1135_C18650;
                    $validar = 1;
                }
            }

            $G1135_C18651 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C18651"])){    
                if($_POST["G1135_C18651"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C18651 = "'".str_replace(' ', '',$_POST["G1135_C18651"])." 00:00:00'";
                    $LsqlU .= $separador." G1135_C18651 = ".$G1135_C18651;
                    $LsqlI .= $separador." G1135_C18651";
                    $LsqlV .= $separador.$G1135_C18651;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1135_C17512"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17512 = '".$_POST["G1135_C17512"]."'";
                $LsqlI .= $separador."G1135_C17512";
                $LsqlV .= $separador."'".$_POST["G1135_C17512"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1135_C17495"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17495 = '".$_POST["G1135_C17495"]."'";
                $LsqlI .= $separador."G1135_C17495";
                $LsqlV .= $separador."'".$_POST["G1135_C17495"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1135_C17496"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17496 = '".$_POST["G1135_C17496"]."'";
                $LsqlI .= $separador."G1135_C17496";
                $LsqlV .= $separador."'".$_POST["G1135_C17496"]."'";
                $validar = 1;
            }
 
            $G1135_C17497= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C17497"])){    
                if($_POST["G1135_C17497"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17497 = $_POST["G1135_C17497"];
                    $LsqlU .= $separador." G1135_C17497 = '".$G1135_C17497."'";
                    $LsqlI .= $separador." G1135_C17497";
                    $LsqlV .= $separador."'".$G1135_C17497."'";
                    $validar = 1;
                }
            }
 
            $G1135_C17498= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1135_C17498"])){    
                if($_POST["G1135_C17498"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17498 = $_POST["G1135_C17498"];
                    $LsqlU .= $separador." G1135_C17498 = '".$G1135_C17498."'";
                    $LsqlI .= $separador." G1135_C17498";
                    $LsqlV .= $separador."'".$G1135_C17498."'";
                    $validar = 1;
                }
            }

            $G1135_C17499 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1135_C17499"])){    
                if($_POST["G1135_C17499"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17499 = "'".str_replace(' ', '',$_POST["G1135_C17499"])." 00:00:00'";
                    $LsqlU .= $separador." G1135_C17499 = ".$G1135_C17499;
                    $LsqlI .= $separador." G1135_C17499";
                    $LsqlV .= $separador.$G1135_C17499;
                    $validar = 1;
                }
            }
 
            $G1135_C17500 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1135_C17500"])){    
                if($_POST["G1135_C17500"] != '' && $_POST["G1135_C17500"] != 'undefined' && $_POST["G1135_C17500"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1135_C17500 = "'".$fecha." ".str_replace(' ', '',$_POST["G1135_C17500"])."'";
                    $LsqlU .= $separador."  G1135_C17500 = ".$G1135_C17500."";
                    $LsqlI .= $separador."  G1135_C17500";
                    $LsqlV .= $separador.$G1135_C17500;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1135_C17501"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1135_C17501 = '".$_POST["G1135_C17501"]."'";
                $LsqlI .= $separador."G1135_C17501";
                $LsqlV .= $separador."'".$_POST["G1135_C17501"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1135_C20667 = $numero;
                    $LsqlU .= ", G1135_C20667 = ".$G1135_C20667."";
                    $LsqlI .= ", G1135_C20667";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1135_Usuario ,  G1135_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1135_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1135 WHERE  G1135_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_5"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1136 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1136(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1136_C17513"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17513 = '".$_POST["G1136_C17513"]."'";
                $LsqlI .= $separador."G1136_C17513";
                $LsqlV .= $separador."'".$_POST["G1136_C17513"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17514"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17514 = '".$_POST["G1136_C17514"]."'";
                $LsqlI .= $separador."G1136_C17514";
                $LsqlV .= $separador."'".$_POST["G1136_C17514"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C20334"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20334 = '".$_POST["G1136_C20334"]."'";
                $LsqlI .= $separador."G1136_C20334";
                $LsqlV .= $separador."'".$_POST["G1136_C20334"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C20665"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20665 = '".$_POST["G1136_C20665"]."'";
                $LsqlI .= $separador."G1136_C20665";
                $LsqlV .= $separador."'".$_POST["G1136_C20665"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C20666"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20666 = '".$_POST["G1136_C20666"]."'";
                $LsqlI .= $separador."G1136_C20666";
                $LsqlV .= $separador."'".$_POST["G1136_C20666"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17515"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17515 = '".$_POST["G1136_C17515"]."'";
                $LsqlI .= $separador."G1136_C17515";
                $LsqlV .= $separador."'".$_POST["G1136_C17515"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17516"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17516 = '".$_POST["G1136_C17516"]."'";
                $LsqlI .= $separador."G1136_C17516";
                $LsqlV .= $separador."'".$_POST["G1136_C17516"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17517"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17517 = '".$_POST["G1136_C17517"]."'";
                $LsqlI .= $separador."G1136_C17517";
                $LsqlV .= $separador."'".$_POST["G1136_C17517"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17518"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17518 = '".$_POST["G1136_C17518"]."'";
                $LsqlI .= $separador."G1136_C17518";
                $LsqlV .= $separador."'".$_POST["G1136_C17518"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17519"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17519 = '".$_POST["G1136_C17519"]."'";
                $LsqlI .= $separador."G1136_C17519";
                $LsqlV .= $separador."'".$_POST["G1136_C17519"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17520"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17520 = '".$_POST["G1136_C17520"]."'";
                $LsqlI .= $separador."G1136_C17520";
                $LsqlV .= $separador."'".$_POST["G1136_C17520"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17521"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17521 = '".$_POST["G1136_C17521"]."'";
                $LsqlI .= $separador."G1136_C17521";
                $LsqlV .= $separador."'".$_POST["G1136_C17521"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17522"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17522 = '".$_POST["G1136_C17522"]."'";
                $LsqlI .= $separador."G1136_C17522";
                $LsqlV .= $separador."'".$_POST["G1136_C17522"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17523"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17523 = '".$_POST["G1136_C17523"]."'";
                $LsqlI .= $separador."G1136_C17523";
                $LsqlV .= $separador."'".$_POST["G1136_C17523"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17524"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17524 = '".$_POST["G1136_C17524"]."'";
                $LsqlI .= $separador."G1136_C17524";
                $LsqlV .= $separador."'".$_POST["G1136_C17524"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1136_C18891= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C18891"])){    
                if($_POST["G1136_C18891"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18891 = $_POST["G1136_C18891"];
                    $LsqlU .= $separador." G1136_C18891 = '".$G1136_C18891."'";
                    $LsqlI .= $separador." G1136_C18891";
                    $LsqlV .= $separador."'".$G1136_C18891."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1136_C18892"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18892 = '".$_POST["G1136_C18892"]."'";
                $LsqlI .= $separador."G1136_C18892";
                $LsqlV .= $separador."'".$_POST["G1136_C18892"]."'";
                $validar = 1;
            }

            $G1136_C18893 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1136_C18893"])){    
                if($_POST["G1136_C18893"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18893 = "'".str_replace(' ', '',$_POST["G1136_C18893"])." 00:00:00'";
                    $LsqlU .= $separador." G1136_C18893 = ".$G1136_C18893;
                    $LsqlI .= $separador." G1136_C18893";
                    $LsqlV .= $separador.$G1136_C18893;
                    $validar = 1;
                }
            }

            $G1136_C18894 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1136_C18894"])){    
                if($_POST["G1136_C18894"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18894 = "'".str_replace(' ', '',$_POST["G1136_C18894"])." 00:00:00'";
                    $LsqlU .= $separador." G1136_C18894 = ".$G1136_C18894;
                    $LsqlI .= $separador." G1136_C18894";
                    $LsqlV .= $separador.$G1136_C18894;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1136_C17532"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17532 = '".$_POST["G1136_C17532"]."'";
                $LsqlI .= $separador."G1136_C17532";
                $LsqlV .= $separador."'".$_POST["G1136_C17532"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17533"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17533 = '".$_POST["G1136_C17533"]."'";
                $LsqlI .= $separador."G1136_C17533";
                $LsqlV .= $separador."'".$_POST["G1136_C17533"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17534"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17534 = '".$_POST["G1136_C17534"]."'";
                $LsqlI .= $separador."G1136_C17534";
                $LsqlV .= $separador."'".$_POST["G1136_C17534"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1136_C17535"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17535 = '".$_POST["G1136_C17535"]."'";
                $LsqlI .= $separador."G1136_C17535";
                $LsqlV .= $separador."'".$_POST["G1136_C17535"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1136_C18895"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18895 = '".$_POST["G1136_C18895"]."'";
                $LsqlI .= $separador."G1136_C18895";
                $LsqlV .= $separador."'".$_POST["G1136_C18895"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1136_C18896"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18896 = '".$_POST["G1136_C18896"]."'";
                $LsqlI .= $separador."G1136_C18896";
                $LsqlV .= $separador."'".$_POST["G1136_C18896"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1136_C18897"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18897 = '".$_POST["G1136_C18897"]."'";
                $LsqlI .= $separador."G1136_C18897";
                $LsqlV .= $separador."'".$_POST["G1136_C18897"]."'";
                $validar = 1;
            }
            
 
            $G1136_C18898= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C18898"])){    
                if($_POST["G1136_C18898"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18898 = $_POST["G1136_C18898"];
                    $LsqlU .= $separador." G1136_C18898 = '".$G1136_C18898."'";
                    $LsqlI .= $separador." G1136_C18898";
                    $LsqlV .= $separador."'".$G1136_C18898."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1136_C18899"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18899 = '".$_POST["G1136_C18899"]."'";
                $LsqlI .= $separador."G1136_C18899";
                $LsqlV .= $separador."'".$_POST["G1136_C18899"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1136_C18900= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C18900"])){    
                if($_POST["G1136_C18900"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18900 = $_POST["G1136_C18900"];
                    $LsqlU .= $separador." G1136_C18900 = '".$G1136_C18900."'";
                    $LsqlI .= $separador." G1136_C18900";
                    $LsqlV .= $separador."'".$G1136_C18900."'";
                    $validar = 1;
                }
            }
  
            $G1136_C17537 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17537"])){    
                if($_POST["G1136_C17537"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17537 = $_POST["G1136_C17537"];
                    $LsqlU .= $separador." G1136_C17537 = '".$G1136_C17537."'";
                    $LsqlI .= $separador." G1136_C17537";
                    $LsqlV .= $separador."'".$G1136_C17537."'";
                    $validar = 1;
                }
            }
 
            $G1136_C17536 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1136_C17536"])){
                if($_POST["G1136_C17536"] == 'Yes'){
                    $G1136_C17536 = 1;
                }else if($_POST["G1136_C17536"] == 'off'){
                    $G1136_C17536 = 0;
                }else if($_POST["G1136_C17536"] == 'on'){
                    $G1136_C17536 = 1;
                }else if($_POST["G1136_C17536"] == 'No'){
                    $G1136_C17536 = 1;
                }else{
                    $G1136_C17536 = $_POST["G1136_C17536"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1136_C17536 = ".$G1136_C17536."";
                $LsqlI .= $separador." G1136_C17536";
                $LsqlV .= $separador.$G1136_C17536;

                $validar = 1;
            }else{
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1136_C17536 = ".$G1136_C17536."";
                $LsqlI .= $separador." G1136_C17536";
                $LsqlV .= $separador.$G1136_C17536;

                $validar = 1;

            }
  
            $G1136_C17539 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17539"])){    
                if($_POST["G1136_C17539"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17539 = $_POST["G1136_C17539"];
                    $LsqlU .= $separador." G1136_C17539 = '".$G1136_C17539."'";
                    $LsqlI .= $separador." G1136_C17539";
                    $LsqlV .= $separador."'".$G1136_C17539."'";
                    $validar = 1;
                }
            }
 
            $G1136_C17538 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1136_C17538"])){
                if($_POST["G1136_C17538"] == 'Yes'){
                    $G1136_C17538 = 1;
                }else if($_POST["G1136_C17538"] == 'off'){
                    $G1136_C17538 = 0;
                }else if($_POST["G1136_C17538"] == 'on'){
                    $G1136_C17538 = 1;
                }else if($_POST["G1136_C17538"] == 'No'){
                    $G1136_C17538 = 1;
                }else{
                    $G1136_C17538 = $_POST["G1136_C17538"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1136_C17538 = ".$G1136_C17538."";
                $LsqlI .= $separador." G1136_C17538";
                $LsqlV .= $separador.$G1136_C17538;

                $validar = 1;
            }else{
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1136_C17538 = ".$G1136_C17538."";
                $LsqlI .= $separador." G1136_C17538";
                $LsqlV .= $separador.$G1136_C17538;

                $validar = 1;

            }
  

            if(isset($_POST["G1136_C17540"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17540 = '".$_POST["G1136_C17540"]."'";
                $LsqlI .= $separador."G1136_C17540";
                $LsqlV .= $separador."'".$_POST["G1136_C17540"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1136_C18644"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18644 = '".$_POST["G1136_C18644"]."'";
                $LsqlI .= $separador."G1136_C18644";
                $LsqlV .= $separador."'".$_POST["G1136_C18644"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1136_C17525"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17525 = '".$_POST["G1136_C17525"]."'";
                $LsqlI .= $separador."G1136_C17525";
                $LsqlV .= $separador."'".$_POST["G1136_C17525"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1136_C17526"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17526 = '".$_POST["G1136_C17526"]."'";
                $LsqlI .= $separador."G1136_C17526";
                $LsqlV .= $separador."'".$_POST["G1136_C17526"]."'";
                $validar = 1;
            }
 
            $G1136_C17527= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17527"])){    
                if($_POST["G1136_C17527"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17527 = $_POST["G1136_C17527"];
                    $LsqlU .= $separador." G1136_C17527 = '".$G1136_C17527."'";
                    $LsqlI .= $separador." G1136_C17527";
                    $LsqlV .= $separador."'".$G1136_C17527."'";
                    $validar = 1;
                }
            }
 
            $G1136_C17528= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17528"])){    
                if($_POST["G1136_C17528"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17528 = $_POST["G1136_C17528"];
                    $LsqlU .= $separador." G1136_C17528 = '".$G1136_C17528."'";
                    $LsqlI .= $separador." G1136_C17528";
                    $LsqlV .= $separador."'".$G1136_C17528."'";
                    $validar = 1;
                }
            }

            $G1136_C17529 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1136_C17529"])){    
                if($_POST["G1136_C17529"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17529 = "'".str_replace(' ', '',$_POST["G1136_C17529"])." 00:00:00'";
                    $LsqlU .= $separador." G1136_C17529 = ".$G1136_C17529;
                    $LsqlI .= $separador." G1136_C17529";
                    $LsqlV .= $separador.$G1136_C17529;
                    $validar = 1;
                }
            }
 
            $G1136_C17530 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1136_C17530"])){    
                if($_POST["G1136_C17530"] != '' && $_POST["G1136_C17530"] != 'undefined' && $_POST["G1136_C17530"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17530 = "'".$fecha." ".str_replace(' ', '',$_POST["G1136_C17530"])."'";
                    $LsqlU .= $separador."  G1136_C17530 = ".$G1136_C17530."";
                    $LsqlI .= $separador."  G1136_C17530";
                    $LsqlV .= $separador.$G1136_C17530;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1136_C17531"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17531 = '".$_POST["G1136_C17531"]."'";
                $LsqlI .= $separador."G1136_C17531";
                $LsqlV .= $separador."'".$_POST["G1136_C17531"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1136_C20664 = $numero;
                    $LsqlU .= ", G1136_C20664 = ".$G1136_C20664."";
                    $LsqlI .= ", G1136_C20664";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1136_Usuario ,  G1136_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1136_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1136 WHERE  G1136_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_6"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1138 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1138(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1138_C17565"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17565 = '".$_POST["G1138_C17565"]."'";
                $LsqlI .= $separador."G1138_C17565";
                $LsqlV .= $separador."'".$_POST["G1138_C17565"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17566"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17566 = '".$_POST["G1138_C17566"]."'";
                $LsqlI .= $separador."G1138_C17566";
                $LsqlV .= $separador."'".$_POST["G1138_C17566"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17567"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17567 = '".$_POST["G1138_C17567"]."'";
                $LsqlI .= $separador."G1138_C17567";
                $LsqlV .= $separador."'".$_POST["G1138_C17567"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C20674"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C20674 = '".$_POST["G1138_C20674"]."'";
                $LsqlI .= $separador."G1138_C20674";
                $LsqlV .= $separador."'".$_POST["G1138_C20674"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C20675"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C20675 = '".$_POST["G1138_C20675"]."'";
                $LsqlI .= $separador."G1138_C20675";
                $LsqlV .= $separador."'".$_POST["G1138_C20675"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17568"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17568 = '".$_POST["G1138_C17568"]."'";
                $LsqlI .= $separador."G1138_C17568";
                $LsqlV .= $separador."'".$_POST["G1138_C17568"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17569"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17569 = '".$_POST["G1138_C17569"]."'";
                $LsqlI .= $separador."G1138_C17569";
                $LsqlV .= $separador."'".$_POST["G1138_C17569"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17570"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17570 = '".$_POST["G1138_C17570"]."'";
                $LsqlI .= $separador."G1138_C17570";
                $LsqlV .= $separador."'".$_POST["G1138_C17570"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17571"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17571 = '".$_POST["G1138_C17571"]."'";
                $LsqlI .= $separador."G1138_C17571";
                $LsqlV .= $separador."'".$_POST["G1138_C17571"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17572"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17572 = '".$_POST["G1138_C17572"]."'";
                $LsqlI .= $separador."G1138_C17572";
                $LsqlV .= $separador."'".$_POST["G1138_C17572"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C18922"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18922 = '".$_POST["G1138_C18922"]."'";
                $LsqlI .= $separador."G1138_C18922";
                $LsqlV .= $separador."'".$_POST["G1138_C18922"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17573"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17573 = '".$_POST["G1138_C17573"]."'";
                $LsqlI .= $separador."G1138_C17573";
                $LsqlV .= $separador."'".$_POST["G1138_C17573"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17574"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17574 = '".$_POST["G1138_C17574"]."'";
                $LsqlI .= $separador."G1138_C17574";
                $LsqlV .= $separador."'".$_POST["G1138_C17574"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17575"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17575 = '".$_POST["G1138_C17575"]."'";
                $LsqlI .= $separador."G1138_C17575";
                $LsqlV .= $separador."'".$_POST["G1138_C17575"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17576"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17576 = '".$_POST["G1138_C17576"]."'";
                $LsqlI .= $separador."G1138_C17576";
                $LsqlV .= $separador."'".$_POST["G1138_C17576"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1138_C18923= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C18923"])){    
                if($_POST["G1138_C18923"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18923 = $_POST["G1138_C18923"];
                    $LsqlU .= $separador." G1138_C18923 = '".$G1138_C18923."'";
                    $LsqlI .= $separador." G1138_C18923";
                    $LsqlV .= $separador."'".$G1138_C18923."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1138_C18924"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18924 = '".$_POST["G1138_C18924"]."'";
                $LsqlI .= $separador."G1138_C18924";
                $LsqlV .= $separador."'".$_POST["G1138_C18924"]."'";
                $validar = 1;
            }

            $G1138_C18925 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1138_C18925"])){    
                if($_POST["G1138_C18925"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18925 = "'".str_replace(' ', '',$_POST["G1138_C18925"])." 00:00:00'";
                    $LsqlU .= $separador." G1138_C18925 = ".$G1138_C18925;
                    $LsqlI .= $separador." G1138_C18925";
                    $LsqlV .= $separador.$G1138_C18925;
                    $validar = 1;
                }
            }

            $G1138_C18926 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1138_C18926"])){    
                if($_POST["G1138_C18926"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18926 = "'".str_replace(' ', '',$_POST["G1138_C18926"])." 00:00:00'";
                    $LsqlU .= $separador." G1138_C18926 = ".$G1138_C18926;
                    $LsqlI .= $separador." G1138_C18926";
                    $LsqlV .= $separador.$G1138_C18926;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1138_C17584"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17584 = '".$_POST["G1138_C17584"]."'";
                $LsqlI .= $separador."G1138_C17584";
                $LsqlV .= $separador."'".$_POST["G1138_C17584"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17585"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17585 = '".$_POST["G1138_C17585"]."'";
                $LsqlI .= $separador."G1138_C17585";
                $LsqlV .= $separador."'".$_POST["G1138_C17585"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17586"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17586 = '".$_POST["G1138_C17586"]."'";
                $LsqlI .= $separador."G1138_C17586";
                $LsqlV .= $separador."'".$_POST["G1138_C17586"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1138_C17587"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17587 = '".$_POST["G1138_C17587"]."'";
                $LsqlI .= $separador."G1138_C17587";
                $LsqlV .= $separador."'".$_POST["G1138_C17587"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1138_C18927"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18927 = '".$_POST["G1138_C18927"]."'";
                $LsqlI .= $separador."G1138_C18927";
                $LsqlV .= $separador."'".$_POST["G1138_C18927"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1138_C18928"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18928 = '".$_POST["G1138_C18928"]."'";
                $LsqlI .= $separador."G1138_C18928";
                $LsqlV .= $separador."'".$_POST["G1138_C18928"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1138_C18929"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18929 = '".$_POST["G1138_C18929"]."'";
                $LsqlI .= $separador."G1138_C18929";
                $LsqlV .= $separador."'".$_POST["G1138_C18929"]."'";
                $validar = 1;
            }
            
 
            $G1138_C18930= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C18930"])){    
                if($_POST["G1138_C18930"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18930 = $_POST["G1138_C18930"];
                    $LsqlU .= $separador." G1138_C18930 = '".$G1138_C18930."'";
                    $LsqlI .= $separador." G1138_C18930";
                    $LsqlV .= $separador."'".$G1138_C18930."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1138_C18931"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18931 = '".$_POST["G1138_C18931"]."'";
                $LsqlI .= $separador."G1138_C18931";
                $LsqlV .= $separador."'".$_POST["G1138_C18931"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1138_C18932= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C18932"])){    
                if($_POST["G1138_C18932"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18932 = $_POST["G1138_C18932"];
                    $LsqlU .= $separador." G1138_C18932 = '".$G1138_C18932."'";
                    $LsqlI .= $separador." G1138_C18932";
                    $LsqlV .= $separador."'".$G1138_C18932."'";
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1138_C17588"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17588 = '".$_POST["G1138_C17588"]."'";
                $LsqlI .= $separador."G1138_C17588";
                $LsqlV .= $separador."'".$_POST["G1138_C17588"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1138_C17577"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17577 = '".$_POST["G1138_C17577"]."'";
                $LsqlI .= $separador."G1138_C17577";
                $LsqlV .= $separador."'".$_POST["G1138_C17577"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1138_C17578"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17578 = '".$_POST["G1138_C17578"]."'";
                $LsqlI .= $separador."G1138_C17578";
                $LsqlV .= $separador."'".$_POST["G1138_C17578"]."'";
                $validar = 1;
            }
 
            $G1138_C17579= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C17579"])){    
                if($_POST["G1138_C17579"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C17579 = $_POST["G1138_C17579"];
                    $LsqlU .= $separador." G1138_C17579 = '".$G1138_C17579."'";
                    $LsqlI .= $separador." G1138_C17579";
                    $LsqlV .= $separador."'".$G1138_C17579."'";
                    $validar = 1;
                }
            }
 
            $G1138_C17580= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C17580"])){    
                if($_POST["G1138_C17580"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C17580 = $_POST["G1138_C17580"];
                    $LsqlU .= $separador." G1138_C17580 = '".$G1138_C17580."'";
                    $LsqlI .= $separador." G1138_C17580";
                    $LsqlV .= $separador."'".$G1138_C17580."'";
                    $validar = 1;
                }
            }

            $G1138_C17581 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1138_C17581"])){    
                if($_POST["G1138_C17581"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C17581 = "'".str_replace(' ', '',$_POST["G1138_C17581"])." 00:00:00'";
                    $LsqlU .= $separador." G1138_C17581 = ".$G1138_C17581;
                    $LsqlI .= $separador." G1138_C17581";
                    $LsqlV .= $separador.$G1138_C17581;
                    $validar = 1;
                }
            }
 
            $G1138_C17582 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1138_C17582"])){    
                if($_POST["G1138_C17582"] != '' && $_POST["G1138_C17582"] != 'undefined' && $_POST["G1138_C17582"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C17582 = "'".$fecha." ".str_replace(' ', '',$_POST["G1138_C17582"])."'";
                    $LsqlU .= $separador."  G1138_C17582 = ".$G1138_C17582."";
                    $LsqlI .= $separador."  G1138_C17582";
                    $LsqlV .= $separador.$G1138_C17582;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1138_C17583"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17583 = '".$_POST["G1138_C17583"]."'";
                $LsqlI .= $separador."G1138_C17583";
                $LsqlV .= $separador."'".$_POST["G1138_C17583"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1138_C20673 = $numero;
                    $LsqlU .= ", G1138_C20673 = ".$G1138_C20673."";
                    $LsqlI .= ", G1138_C20673";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1138_Usuario ,  G1138_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1138_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1138 WHERE  G1138_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_7"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1139 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1139(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1139_C17589"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17589 = '".$_POST["G1139_C17589"]."'";
                $LsqlI .= $separador."G1139_C17589";
                $LsqlV .= $separador."'".$_POST["G1139_C17589"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17590 = '".$_POST["G1139_C17590"]."'";
                $LsqlI .= $separador."G1139_C17590";
                $LsqlV .= $separador."'".$_POST["G1139_C17590"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C19164"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19164 = '".$_POST["G1139_C19164"]."'";
                $LsqlI .= $separador."G1139_C19164";
                $LsqlV .= $separador."'".$_POST["G1139_C19164"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C20683"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C20683 = '".$_POST["G1139_C20683"]."'";
                $LsqlI .= $separador."G1139_C20683";
                $LsqlV .= $separador."'".$_POST["G1139_C20683"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C20684"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C20684 = '".$_POST["G1139_C20684"]."'";
                $LsqlI .= $separador."G1139_C20684";
                $LsqlV .= $separador."'".$_POST["G1139_C20684"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17591"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17591 = '".$_POST["G1139_C17591"]."'";
                $LsqlI .= $separador."G1139_C17591";
                $LsqlV .= $separador."'".$_POST["G1139_C17591"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17592"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17592 = '".$_POST["G1139_C17592"]."'";
                $LsqlI .= $separador."G1139_C17592";
                $LsqlV .= $separador."'".$_POST["G1139_C17592"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17593"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17593 = '".$_POST["G1139_C17593"]."'";
                $LsqlI .= $separador."G1139_C17593";
                $LsqlV .= $separador."'".$_POST["G1139_C17593"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17594"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17594 = '".$_POST["G1139_C17594"]."'";
                $LsqlI .= $separador."G1139_C17594";
                $LsqlV .= $separador."'".$_POST["G1139_C17594"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17595"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17595 = '".$_POST["G1139_C17595"]."'";
                $LsqlI .= $separador."G1139_C17595";
                $LsqlV .= $separador."'".$_POST["G1139_C17595"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17596"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17596 = '".$_POST["G1139_C17596"]."'";
                $LsqlI .= $separador."G1139_C17596";
                $LsqlV .= $separador."'".$_POST["G1139_C17596"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17597"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17597 = '".$_POST["G1139_C17597"]."'";
                $LsqlI .= $separador."G1139_C17597";
                $LsqlV .= $separador."'".$_POST["G1139_C17597"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17598"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17598 = '".$_POST["G1139_C17598"]."'";
                $LsqlI .= $separador."G1139_C17598";
                $LsqlV .= $separador."'".$_POST["G1139_C17598"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17599"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17599 = '".$_POST["G1139_C17599"]."'";
                $LsqlI .= $separador."G1139_C17599";
                $LsqlV .= $separador."'".$_POST["G1139_C17599"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17600"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17600 = '".$_POST["G1139_C17600"]."'";
                $LsqlI .= $separador."G1139_C17600";
                $LsqlV .= $separador."'".$_POST["G1139_C17600"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1139_C19165= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C19165"])){    
                if($_POST["G1139_C19165"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19165 = $_POST["G1139_C19165"];
                    $LsqlU .= $separador." G1139_C19165 = '".$G1139_C19165."'";
                    $LsqlI .= $separador." G1139_C19165";
                    $LsqlV .= $separador."'".$G1139_C19165."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1139_C19166"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19166 = '".$_POST["G1139_C19166"]."'";
                $LsqlI .= $separador."G1139_C19166";
                $LsqlV .= $separador."'".$_POST["G1139_C19166"]."'";
                $validar = 1;
            }

            $G1139_C19167 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1139_C19167"])){    
                if($_POST["G1139_C19167"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19167 = "'".str_replace(' ', '',$_POST["G1139_C19167"])." 00:00:00'";
                    $LsqlU .= $separador." G1139_C19167 = ".$G1139_C19167;
                    $LsqlI .= $separador." G1139_C19167";
                    $LsqlV .= $separador.$G1139_C19167;
                    $validar = 1;
                }
            }

            $G1139_C19168 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1139_C19168"])){    
                if($_POST["G1139_C19168"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19168 = "'".str_replace(' ', '',$_POST["G1139_C19168"])." 00:00:00'";
                    $LsqlU .= $separador." G1139_C19168 = ".$G1139_C19168;
                    $LsqlI .= $separador." G1139_C19168";
                    $LsqlV .= $separador.$G1139_C19168;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1139_C17608"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17608 = '".$_POST["G1139_C17608"]."'";
                $LsqlI .= $separador."G1139_C17608";
                $LsqlV .= $separador."'".$_POST["G1139_C17608"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17609"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17609 = '".$_POST["G1139_C17609"]."'";
                $LsqlI .= $separador."G1139_C17609";
                $LsqlV .= $separador."'".$_POST["G1139_C17609"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17610"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17610 = '".$_POST["G1139_C17610"]."'";
                $LsqlI .= $separador."G1139_C17610";
                $LsqlV .= $separador."'".$_POST["G1139_C17610"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1139_C17611"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17611 = '".$_POST["G1139_C17611"]."'";
                $LsqlI .= $separador."G1139_C17611";
                $LsqlV .= $separador."'".$_POST["G1139_C17611"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1139_C19169"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19169 = '".$_POST["G1139_C19169"]."'";
                $LsqlI .= $separador."G1139_C19169";
                $LsqlV .= $separador."'".$_POST["G1139_C19169"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1139_C19170"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19170 = '".$_POST["G1139_C19170"]."'";
                $LsqlI .= $separador."G1139_C19170";
                $LsqlV .= $separador."'".$_POST["G1139_C19170"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1139_C19171"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19171 = '".$_POST["G1139_C19171"]."'";
                $LsqlI .= $separador."G1139_C19171";
                $LsqlV .= $separador."'".$_POST["G1139_C19171"]."'";
                $validar = 1;
            }
            
 
            $G1139_C19172= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C19172"])){    
                if($_POST["G1139_C19172"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19172 = $_POST["G1139_C19172"];
                    $LsqlU .= $separador." G1139_C19172 = '".$G1139_C19172."'";
                    $LsqlI .= $separador." G1139_C19172";
                    $LsqlV .= $separador."'".$G1139_C19172."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1139_C19173"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19173 = '".$_POST["G1139_C19173"]."'";
                $LsqlI .= $separador."G1139_C19173";
                $LsqlV .= $separador."'".$_POST["G1139_C19173"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1139_C19174= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C19174"])){    
                if($_POST["G1139_C19174"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19174 = $_POST["G1139_C19174"];
                    $LsqlU .= $separador." G1139_C19174 = '".$G1139_C19174."'";
                    $LsqlI .= $separador." G1139_C19174";
                    $LsqlV .= $separador."'".$G1139_C19174."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1139_C17612"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17612 = '".$_POST["G1139_C17612"]."'";
                $LsqlI .= $separador."G1139_C17612";
                $LsqlV .= $separador."'".$_POST["G1139_C17612"]."'";
                $validar = 1;
            }
  

            if(isset($_POST["G1139_C17613"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17613 = '".$_POST["G1139_C17613"]."'";
                $LsqlI .= $separador."G1139_C17613";
                $LsqlV .= $separador."'".$_POST["G1139_C17613"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1139_C17601"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17601 = '".$_POST["G1139_C17601"]."'";
                $LsqlI .= $separador."G1139_C17601";
                $LsqlV .= $separador."'".$_POST["G1139_C17601"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1139_C17602"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17602 = '".$_POST["G1139_C17602"]."'";
                $LsqlI .= $separador."G1139_C17602";
                $LsqlV .= $separador."'".$_POST["G1139_C17602"]."'";
                $validar = 1;
            }
 
            $G1139_C17603= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C17603"])){    
                if($_POST["G1139_C17603"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C17603 = $_POST["G1139_C17603"];
                    $LsqlU .= $separador." G1139_C17603 = '".$G1139_C17603."'";
                    $LsqlI .= $separador." G1139_C17603";
                    $LsqlV .= $separador."'".$G1139_C17603."'";
                    $validar = 1;
                }
            }
 
            $G1139_C17604= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C17604"])){    
                if($_POST["G1139_C17604"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C17604 = $_POST["G1139_C17604"];
                    $LsqlU .= $separador." G1139_C17604 = '".$G1139_C17604."'";
                    $LsqlI .= $separador." G1139_C17604";
                    $LsqlV .= $separador."'".$G1139_C17604."'";
                    $validar = 1;
                }
            }

            $G1139_C17605 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1139_C17605"])){    
                if($_POST["G1139_C17605"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C17605 = "'".str_replace(' ', '',$_POST["G1139_C17605"])." 00:00:00'";
                    $LsqlU .= $separador." G1139_C17605 = ".$G1139_C17605;
                    $LsqlI .= $separador." G1139_C17605";
                    $LsqlV .= $separador.$G1139_C17605;
                    $validar = 1;
                }
            }
 
            $G1139_C17606 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1139_C17606"])){    
                if($_POST["G1139_C17606"] != '' && $_POST["G1139_C17606"] != 'undefined' && $_POST["G1139_C17606"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C17606 = "'".$fecha." ".str_replace(' ', '',$_POST["G1139_C17606"])."'";
                    $LsqlU .= $separador."  G1139_C17606 = ".$G1139_C17606."";
                    $LsqlI .= $separador."  G1139_C17606";
                    $LsqlV .= $separador.$G1139_C17606;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1139_C17607"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17607 = '".$_POST["G1139_C17607"]."'";
                $LsqlI .= $separador."G1139_C17607";
                $LsqlV .= $separador."'".$_POST["G1139_C17607"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1139_C20682 = $numero;
                    $LsqlU .= ", G1139_C20682 = ".$G1139_C20682."";
                    $LsqlI .= ", G1139_C20682";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1139_Usuario ,  G1139_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1139_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1139 WHERE  G1139_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_8"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1140 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1140(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1140_C17614"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17614 = '".$_POST["G1140_C17614"]."'";
                $LsqlI .= $separador."G1140_C17614";
                $LsqlV .= $separador."'".$_POST["G1140_C17614"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17615"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17615 = '".$_POST["G1140_C17615"]."'";
                $LsqlI .= $separador."G1140_C17615";
                $LsqlV .= $separador."'".$_POST["G1140_C17615"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17616"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17616 = '".$_POST["G1140_C17616"]."'";
                $LsqlI .= $separador."G1140_C17616";
                $LsqlV .= $separador."'".$_POST["G1140_C17616"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C20686"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20686 = '".$_POST["G1140_C20686"]."'";
                $LsqlI .= $separador."G1140_C20686";
                $LsqlV .= $separador."'".$_POST["G1140_C20686"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C20687"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20687 = '".$_POST["G1140_C20687"]."'";
                $LsqlI .= $separador."G1140_C20687";
                $LsqlV .= $separador."'".$_POST["G1140_C20687"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17617"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17617 = '".$_POST["G1140_C17617"]."'";
                $LsqlI .= $separador."G1140_C17617";
                $LsqlV .= $separador."'".$_POST["G1140_C17617"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17618"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17618 = '".$_POST["G1140_C17618"]."'";
                $LsqlI .= $separador."G1140_C17618";
                $LsqlV .= $separador."'".$_POST["G1140_C17618"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17619"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17619 = '".$_POST["G1140_C17619"]."'";
                $LsqlI .= $separador."G1140_C17619";
                $LsqlV .= $separador."'".$_POST["G1140_C17619"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17620"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17620 = '".$_POST["G1140_C17620"]."'";
                $LsqlI .= $separador."G1140_C17620";
                $LsqlV .= $separador."'".$_POST["G1140_C17620"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C20202"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20202 = '".$_POST["G1140_C20202"]."'";
                $LsqlI .= $separador."G1140_C20202";
                $LsqlV .= $separador."'".$_POST["G1140_C20202"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17621"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17621 = '".$_POST["G1140_C17621"]."'";
                $LsqlI .= $separador."G1140_C17621";
                $LsqlV .= $separador."'".$_POST["G1140_C17621"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17622"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17622 = '".$_POST["G1140_C17622"]."'";
                $LsqlI .= $separador."G1140_C17622";
                $LsqlV .= $separador."'".$_POST["G1140_C17622"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17623"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17623 = '".$_POST["G1140_C17623"]."'";
                $LsqlI .= $separador."G1140_C17623";
                $LsqlV .= $separador."'".$_POST["G1140_C17623"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17624"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17624 = '".$_POST["G1140_C17624"]."'";
                $LsqlI .= $separador."G1140_C17624";
                $LsqlV .= $separador."'".$_POST["G1140_C17624"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17625"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17625 = '".$_POST["G1140_C17625"]."'";
                $LsqlI .= $separador."G1140_C17625";
                $LsqlV .= $separador."'".$_POST["G1140_C17625"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1140_C20203= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C20203"])){    
                if($_POST["G1140_C20203"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20203 = $_POST["G1140_C20203"];
                    $LsqlU .= $separador." G1140_C20203 = '".$G1140_C20203."'";
                    $LsqlI .= $separador." G1140_C20203";
                    $LsqlV .= $separador."'".$G1140_C20203."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1140_C20204"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20204 = '".$_POST["G1140_C20204"]."'";
                $LsqlI .= $separador."G1140_C20204";
                $LsqlV .= $separador."'".$_POST["G1140_C20204"]."'";
                $validar = 1;
            }

            $G1140_C20205 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1140_C20205"])){    
                if($_POST["G1140_C20205"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20205 = "'".str_replace(' ', '',$_POST["G1140_C20205"])." 00:00:00'";
                    $LsqlU .= $separador." G1140_C20205 = ".$G1140_C20205;
                    $LsqlI .= $separador." G1140_C20205";
                    $LsqlV .= $separador.$G1140_C20205;
                    $validar = 1;
                }
            }

            $G1140_C20206 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1140_C20206"])){    
                if($_POST["G1140_C20206"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20206 = "'".str_replace(' ', '',$_POST["G1140_C20206"])." 00:00:00'";
                    $LsqlU .= $separador." G1140_C20206 = ".$G1140_C20206;
                    $LsqlI .= $separador." G1140_C20206";
                    $LsqlV .= $separador.$G1140_C20206;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1140_C17633"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17633 = '".$_POST["G1140_C17633"]."'";
                $LsqlI .= $separador."G1140_C17633";
                $LsqlV .= $separador."'".$_POST["G1140_C17633"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17634"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17634 = '".$_POST["G1140_C17634"]."'";
                $LsqlI .= $separador."G1140_C17634";
                $LsqlV .= $separador."'".$_POST["G1140_C17634"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17635"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17635 = '".$_POST["G1140_C17635"]."'";
                $LsqlI .= $separador."G1140_C17635";
                $LsqlV .= $separador."'".$_POST["G1140_C17635"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1140_C17636"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17636 = '".$_POST["G1140_C17636"]."'";
                $LsqlI .= $separador."G1140_C17636";
                $LsqlV .= $separador."'".$_POST["G1140_C17636"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1140_C20231"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20231 = '".$_POST["G1140_C20231"]."'";
                $LsqlI .= $separador."G1140_C20231";
                $LsqlV .= $separador."'".$_POST["G1140_C20231"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1140_C20232"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20232 = '".$_POST["G1140_C20232"]."'";
                $LsqlI .= $separador."G1140_C20232";
                $LsqlV .= $separador."'".$_POST["G1140_C20232"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1140_C20233"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20233 = '".$_POST["G1140_C20233"]."'";
                $LsqlI .= $separador."G1140_C20233";
                $LsqlV .= $separador."'".$_POST["G1140_C20233"]."'";
                $validar = 1;
            }
            
 
            $G1140_C20234= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C20234"])){    
                if($_POST["G1140_C20234"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20234 = $_POST["G1140_C20234"];
                    $LsqlU .= $separador." G1140_C20234 = '".$G1140_C20234."'";
                    $LsqlI .= $separador." G1140_C20234";
                    $LsqlV .= $separador."'".$G1140_C20234."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1140_C20235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20235 = '".$_POST["G1140_C20235"]."'";
                $LsqlI .= $separador."G1140_C20235";
                $LsqlV .= $separador."'".$_POST["G1140_C20235"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1140_C20236= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C20236"])){    
                if($_POST["G1140_C20236"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20236 = $_POST["G1140_C20236"];
                    $LsqlU .= $separador." G1140_C20236 = '".$G1140_C20236."'";
                    $LsqlI .= $separador." G1140_C20236";
                    $LsqlV .= $separador."'".$G1140_C20236."'";
                    $validar = 1;
                }
            }
 
            if(isset($_POST["G1140_C17637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17637 = '".$_POST["G1140_C17637"]."'";
                $LsqlI .= $separador."G1140_C17637";
                $LsqlV .= $separador."'".$_POST["G1140_C17637"]."'";
                $validar = 1;
            }
  

            if(isset($_POST["G1140_C17638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17638 = '".$_POST["G1140_C17638"]."'";
                $LsqlI .= $separador."G1140_C17638";
                $LsqlV .= $separador."'".$_POST["G1140_C17638"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1140_C17626"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17626 = '".$_POST["G1140_C17626"]."'";
                $LsqlI .= $separador."G1140_C17626";
                $LsqlV .= $separador."'".$_POST["G1140_C17626"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1140_C17627"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17627 = '".$_POST["G1140_C17627"]."'";
                $LsqlI .= $separador."G1140_C17627";
                $LsqlV .= $separador."'".$_POST["G1140_C17627"]."'";
                $validar = 1;
            }
 
            $G1140_C17628= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C17628"])){    
                if($_POST["G1140_C17628"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C17628 = $_POST["G1140_C17628"];
                    $LsqlU .= $separador." G1140_C17628 = '".$G1140_C17628."'";
                    $LsqlI .= $separador." G1140_C17628";
                    $LsqlV .= $separador."'".$G1140_C17628."'";
                    $validar = 1;
                }
            }
 
            $G1140_C17629= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C17629"])){    
                if($_POST["G1140_C17629"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C17629 = $_POST["G1140_C17629"];
                    $LsqlU .= $separador." G1140_C17629 = '".$G1140_C17629."'";
                    $LsqlI .= $separador." G1140_C17629";
                    $LsqlV .= $separador."'".$G1140_C17629."'";
                    $validar = 1;
                }
            }

            $G1140_C17630 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1140_C17630"])){    
                if($_POST["G1140_C17630"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C17630 = "'".str_replace(' ', '',$_POST["G1140_C17630"])." 00:00:00'";
                    $LsqlU .= $separador." G1140_C17630 = ".$G1140_C17630;
                    $LsqlI .= $separador." G1140_C17630";
                    $LsqlV .= $separador.$G1140_C17630;
                    $validar = 1;
                }
            }
 
            $G1140_C17631 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1140_C17631"])){    
                if($_POST["G1140_C17631"] != '' && $_POST["G1140_C17631"] != 'undefined' && $_POST["G1140_C17631"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C17631 = "'".$fecha." ".str_replace(' ', '',$_POST["G1140_C17631"])."'";
                    $LsqlU .= $separador."  G1140_C17631 = ".$G1140_C17631."";
                    $LsqlI .= $separador."  G1140_C17631";
                    $LsqlV .= $separador.$G1140_C17631;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1140_C17632"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17632 = '".$_POST["G1140_C17632"]."'";
                $LsqlI .= $separador."G1140_C17632";
                $LsqlV .= $separador."'".$_POST["G1140_C17632"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1140_C20685 = $numero;
                    $LsqlU .= ", G1140_C20685 = ".$G1140_C20685."";
                    $LsqlI .= ", G1140_C20685";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1140_Usuario ,  G1140_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1140_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1140 WHERE  G1140_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
