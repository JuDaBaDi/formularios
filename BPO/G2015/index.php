<?php 
    /*
        Document   : index
        Created on : 2020-11-09 12:33:28
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjAxNQ==  
    */
    $url_crud =  "formularios/G2015/G2015_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G2015/G2015_CRUD_web.php" method="post" id="formLogin">
  
                            <!-- lIBRETO O LABEL -->
                            <h3>En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido.    </h3>
                            <!-- FIN LIBRETO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39193" id="LblG2015_C39193">Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. </label>
								<input type="text" class="form-control input-sm" id="G2015_C39193" value=""  name="G2015_C39193"  placeholder="Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39194" id="LblG2015_C39194">De la logística se encarga MUNI, no desaproveches esta gran oportunidad. ¿Cuéntanos, como te llamas y de qué ciudad te comunicas?</label>
								<input type="text" class="form-control input-sm" id="G2015_C39194" value=""  name="G2015_C39194"  placeholder="De la logística se encarga MUNI, no desaproveches esta gran oportunidad. ¿Cuéntanos, como te llamas y de qué ciudad te comunicas?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Empecemos con un primer pedido de 2 categorías para tí y así pruebas MUNI antes de vender a tus clientes. </h3>
                            <!-- FIN LIBRETO -->
 
                            <?php 
                            $str_Lsql = "SELECT  G_ConsInte__b as id  FROM ".$BaseDatos_systema.".G";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G2015_C39176" id="LblG2015_C39176">Beneficio actual: Si realizas tu primer pedido el día de hoy, te damos XXX [Explicamos Beneficio Actual]</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G2015_C39176" id="G2015_C39176">
                                    <option></option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0'></option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2015_C39177" id="LblG2015_C39177">Descargar la app: Lo primero a hacer es descargar la aplicación. ¡Hazlo aquí!: https://play.google.com/store/apps/details?id=com.muni.android.  RTA: Ya la descargue RTA: No la he descargado, ir a preguntas frecuentes. </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2015_C39177" id="G2015_C39177">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2320 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39178" id="LblG2015_C39178">Perfecto, para hacer el primer pedido llenamos la canasta de los productos que queremos y generas la órden, ¡el pedido te estará llegando el siguiente día de entrega y haces el pago una vez te llegue!</label>
								<input type="text" class="form-control input-sm" id="G2015_C39178" value=""  name="G2015_C39178"  placeholder="Perfecto, para hacer el primer pedido llenamos la canasta de los productos que queremos y generas la órden, ¡el pedido te estará llegando el siguiente día de entrega y haces el pago una vez te llegue!">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39179" id="LblG2015_C39179">PARA GENERAR COMPRA: Tus clientes te hacen el pedido a través del catálogo digital, y cuando te lo envían tú los abres y pides desde la app. Con esto el pedido llegará separado por cliente a tu domicilio para que puedas venderselo a tus clientes  *Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.</label>
								<input type="text" class="form-control input-sm" id="G2015_C39179" value=""  name="G2015_C39179"  placeholder="PARA GENERAR COMPRA: Tus clientes te hacen el pedido a través del catálogo digital, y cuando te lo envían tú los abres y pides desde la app. Con esto el pedido llegará separado por cliente a tu domicilio para que puedas venderselo a tus clientes  *Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39180" id="LblG2015_C39180">DESPUES DE LA CONVERSIÓN: ¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS</label>
								<input type="text" class="form-control input-sm" id="G2015_C39180" value=""  name="G2015_C39180"  placeholder="DESPUES DE LA CONVERSIÓN: ¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos .  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente ️.  ¡Que tengas un muy buen día!”</h3>
                            <!-- FIN LIBRETO -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G2015_C39172" id="LblG2015_C39172">PREGUNTAS FRECUENTES BOGOTÁ</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2015_C39172" id="G2015_C39172">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2317 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G2015_C39172" id="respuesta_LblG2015_C39172">Respuesta</label>
                        <textarea id="respuesta_G2015_C39172" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>a) Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>b) Cashback:  Después de la 1era compra con MUNI, no vas a poder parar. Haz tu pedido ya desde la app Líder Muni y te regalamos el 20% de ESTA compra en municreditos, para tu SIGUIENTE compra. *Máximo $20.000*</h3>
                            <!-- FIN LIBRETO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39182" id="LblG2015_C39182">Hola! gracias por contactarte con MUNI! MUNI es la plataforma que te permite generar ingresos adicionales brindando una gran variedad de productos a tu comunidad  ¿Empezamos?</label>
								<input type="text" class="form-control input-sm" id="G2015_C39182" value=""  name="G2015_C39182"  placeholder="Hola! gracias por contactarte con MUNI! MUNI es la plataforma que te permite generar ingresos adicionales brindando una gran variedad de productos a tu comunidad  ¿Empezamos?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39183" id="LblG2015_C39183">En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido. Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. De la logística se encarga MUNI, no desaproveches esta gran oportunidad. Cuéntanos, ¿Cómo te llamas y de qué ciudad te comunicas? RTA: Medellín</label>
								<input type="text" class="form-control input-sm" id="G2015_C39183" value=""  name="G2015_C39183"  placeholder="En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido. Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. De la logística se encarga MUNI, no desaproveches esta gran oportunidad. Cuéntanos, ¿Cómo te llamas y de qué ciudad te comunicas? RTA: Medellín">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39185" id="LblG2015_C39185">Recibir Catalogo: Lo primero a hacer es escribir al  310 8952449 para recibir el catálogo y hacer tus pedidos!</label>
								<input type="text" class="form-control input-sm" id="G2015_C39185" value=""  name="G2015_C39185"  placeholder="Recibir Catalogo: Lo primero a hacer es escribir al  310 8952449 para recibir el catálogo y hacer tus pedidos!">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39186" id="LblG2015_C39186">Aclaración de  dudas para generar su compra: Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.</label>
								<input type="text" class="form-control input-sm" id="G2015_C39186" value=""  name="G2015_C39186"  placeholder="Aclaración de  dudas para generar su compra: Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39187" id="LblG2015_C39187">¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS.</label>
								<input type="text" class="form-control input-sm" id="G2015_C39187" value=""  name="G2015_C39187"  placeholder="¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39188" id="LblG2015_C39188">No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos.  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente .  ¡Que tengas un muy buen día!”</label>
								<input type="text" class="form-control input-sm" id="G2015_C39188" value=""  name="G2015_C39188"  placeholder="No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos.  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente .  ¡Que tengas un muy buen día!”">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G2015_C39173" id="LblG2015_C39173">PREGUNTAS FRECUENTES MEDELLIN </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2015_C39173" id="G2015_C39173">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2318 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G2015_C39173" id="respuesta_LblG2015_C39173">Respuesta</label>
                        <textarea id="respuesta_G2015_C39173" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39191" id="LblG2015_C39191">Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!</label>
								<input type="text" class="form-control input-sm" id="G2015_C39191" value=""  name="G2015_C39191"  placeholder="Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2015_C39192" id="LblG2015_C39192">Por cada referido que genere una compra mayor a $60.000, te ganas un bono MUNI de $15.000 en tu siguiente compra!</label>
								<input type="text" class="form-control input-sm" id="G2015_C39192" value=""  name="G2015_C39192"  placeholder="Por cada referido que genere una compra mayor a $60.000, te ganas un bono MUNI de $15.000 en tu siguiente compra!">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2015/G2015_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


        $("#G2015_C39176").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                        
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G2015_C39176").change(function(){
            var valores = $("#G2015_C39176 option:selected").text();
            var campos = $("#G2015_C39176 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

                //datepickers
                

            $("#G2015_C36807").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G2015_C36808").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para Descargar la app: Lo primero a hacer es descargar la aplicación. ¡Hazlo aquí!: https://play.google.com/store/apps/details?id=com.muni.android.  RTA: Ya la descargue RTA: No la he descargado, ir a preguntas frecuentes.  

    $("#G2015_C39177").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PREGUNTAS FRECUENTES BOGOTÁ 

    $("#G2015_C39172").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PREGUNTAS FRECUENTES MEDELLIN  

    $("#G2015_C39173").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
