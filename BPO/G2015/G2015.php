
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2015/G2015_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2015_ConsInte__b as id, G2015_C39172 as camp1 , G2015_C39173 as camp2 FROM ".$BaseDatos.".G2015  WHERE G2015_Usuario = ".$idUsuario." ORDER BY G2015_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2015_ConsInte__b as id, G2015_C39172 as camp1 , G2015_C39173 as camp2 FROM ".$BaseDatos.".G2015  ORDER BY G2015_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2015_ConsInte__b as id, G2015_C39172 as camp1 , G2015_C39173 as camp2 FROM ".$BaseDatos.".G2015 JOIN ".$BaseDatos.".G2015_M".$resultEstpas->muestr." ON G2015_ConsInte__b = G2015_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2015_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2015_ConsInte__b as id, G2015_C39172 as camp1 , G2015_C39173 as camp2 FROM ".$BaseDatos.".G2015 JOIN ".$BaseDatos.".G2015_M".$resultEstpas->muestr." ON G2015_ConsInte__b = G2015_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2015_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2015_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $Zsql = "SELECT G2015_ConsInte__b as id, G2015_C39172 as camp1 , G2015_C39173 as camp2 FROM ".$BaseDatos.".G2015  ORDER BY G2015_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    
    //JDBD - Validamos si se pudo obtener el dato principal.
    if ($nombre = $mysqli->query($XLsql)) {

        $nombreUsuario = NULL;
        //echo $XLsql;
        while ($key = $nombre->fetch_object()) {
            echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
            $nombreUsuario = $key->nombre;
            break;
        } 


        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                        
            $data = array(  "strToken_t" => $_GET['token'], 
                            "strIdGestion_t" => $_GET['id_gestion_cbx'],
                            "strDatoPrincipal_t" => $nombreUsuario,
                            "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
            $data_string = json_encode($data);    

            $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                      
            ); 
            //recogemos la respuesta
            $respuesta = curl_exec ($ch);
            //o el error, por si falla
            $error = curl_error($ch);
            //y finalmente cerramos curl
            //echo "Respuesta =>  ". $respuesta;
            //echo "<br/>Error => ".$error;
            //include "Log.class.php";
            //$log = new Log("log", "./Log/");
            //$log->insert($error, $respuesta, false, true, false);
            //echo "nada";
            curl_close ($ch);
        }

    }else{
        echo "<script>alert('NO SE PUDO OBTENER EL DATO PRINCIPAL DEL REGISTRO.');</script>";
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="5829" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5829c">
                GUION BOGOTA
            </a>
        </h4>
        
    </div>
    <div id="s_5829c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido.    </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39193" id="LblG2015_C39193">Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. </label><input type="text" class="form-control input-sm" id="G2015_C39193" value="<?php if (isset($_GET['G2015_C39193'])) {
                            echo $_GET['G2015_C39193'];
                        } ?>"  name="G2015_C39193"  placeholder="Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39194" id="LblG2015_C39194">De la logística se encarga MUNI, no desaproveches esta gran oportunidad. ¿Cuéntanos, como te llamas y de qué ciudad te comunicas?</label><input type="text" class="form-control input-sm" id="G2015_C39194" value="<?php if (isset($_GET['G2015_C39194'])) {
                            echo $_GET['G2015_C39194'];
                        } ?>"  name="G2015_C39194"  placeholder="De la logística se encarga MUNI, no desaproveches esta gran oportunidad. ¿Cuéntanos, como te llamas y de qué ciudad te comunicas?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Empecemos con un primer pedido de 2 categorías para tí y así pruebas MUNI antes de vender a tus clientes. </p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G2015_C39176" id="LblG2015_C39176">Beneficio actual: Si realizas tu primer pedido el día de hoy, te damos XXX [Explicamos Beneficio Actual]</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G2015_C39176" id="G2015_C39176">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2015_C39177" id="LblG2015_C39177">Descargar la app: Lo primero a hacer es descargar la aplicación. ¡Hazlo aquí!: https://play.google.com/store/apps/details?id=com.muni.android.  RTA: Ya la descargue RTA: No la he descargado, ir a preguntas frecuentes. </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2015_C39177" id="G2015_C39177">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2320 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39178" id="LblG2015_C39178">Perfecto, para hacer el primer pedido llenamos la canasta de los productos que queremos y generas la órden, ¡el pedido te estará llegando el siguiente día de entrega y haces el pago una vez te llegue!</label><input type="text" class="form-control input-sm" id="G2015_C39178" value="<?php if (isset($_GET['G2015_C39178'])) {
                            echo $_GET['G2015_C39178'];
                        } ?>"  name="G2015_C39178"  placeholder="Perfecto, para hacer el primer pedido llenamos la canasta de los productos que queremos y generas la órden, ¡el pedido te estará llegando el siguiente día de entrega y haces el pago una vez te llegue!"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39179" id="LblG2015_C39179">PARA GENERAR COMPRA: Tus clientes te hacen el pedido a través del catálogo digital, y cuando te lo envían tú los abres y pides desde la app. Con esto el pedido llegará separado por cliente a tu domicilio para que puedas venderselo a tus clientes  *Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.</label><input type="text" class="form-control input-sm" id="G2015_C39179" value="<?php if (isset($_GET['G2015_C39179'])) {
                            echo $_GET['G2015_C39179'];
                        } ?>"  name="G2015_C39179"  placeholder="PARA GENERAR COMPRA: Tus clientes te hacen el pedido a través del catálogo digital, y cuando te lo envían tú los abres y pides desde la app. Con esto el pedido llegará separado por cliente a tu domicilio para que puedas venderselo a tus clientes  *Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39180" id="LblG2015_C39180">DESPUES DE LA CONVERSIÓN: ¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS</label><input type="text" class="form-control input-sm" id="G2015_C39180" value="<?php if (isset($_GET['G2015_C39180'])) {
                            echo $_GET['G2015_C39180'];
                        } ?>"  name="G2015_C39180"  placeholder="DESPUES DE LA CONVERSIÓN: ¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos .  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente ️.  ¡Que tengas un muy buen día!”</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="5377" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C36810" id="LblG2015_C36810">Agente</label><input type="text" class="form-control input-sm" id="G2015_C36810" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G2015_C36810"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C36811" id="LblG2015_C36811">Fecha</label><input type="text" class="form-control input-sm" id="G2015_C36811" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G2015_C36811"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C36812" id="LblG2015_C36812">Hora</label><input type="text" class="form-control input-sm" id="G2015_C36812" value="<?php echo date('H:i:s');?>" readonly name="G2015_C36812"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C36813" id="LblG2015_C36813">Campaña</label><input type="text" class="form-control input-sm" id="G2015_C36813" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G2015_C36813"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="5827" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5827c">
                PREGUNTAS FRECUENTES BOGOTA
            </a>
        </h4>
        
    </div>
    <div id="s_5827c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G2015_C39172" id="LblG2015_C39172">PREGUNTAS FRECUENTES BOGOTÁ</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2015_C39172" id="G2015_C39172">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2317 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G2015_C39172" id="respuesta_LblG2015_C39172">Respuesta</label>
                        <textarea id="respuesta_G2015_C39172" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5831" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5831c">
                BENEFICIO BOGOTA 
            </a>
        </h4>
        
    </div>
    <div id="s_5831c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">a) Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">b) Cashback:  Después de la 1era compra con MUNI, no vas a poder parar. Haz tu pedido ya desde la app Líder Muni y te regalamos el 20% de ESTA compra en municreditos, para tu SIGUIENTE compra. *Máximo $20.000*</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="5376" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="5830" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5830c">
                GUION MEDELLIN
            </a>
        </h4>
        
    </div>
    <div id="s_5830c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39182" id="LblG2015_C39182">Hola! gracias por contactarte con MUNI! MUNI es la plataforma que te permite generar ingresos adicionales brindando una gran variedad de productos a tu comunidad  ¿Empezamos?</label><input type="text" class="form-control input-sm" id="G2015_C39182" value="<?php if (isset($_GET['G2015_C39182'])) {
                            echo $_GET['G2015_C39182'];
                        } ?>"  name="G2015_C39182"  placeholder="Hola! gracias por contactarte con MUNI! MUNI es la plataforma que te permite generar ingresos adicionales brindando una gran variedad de productos a tu comunidad  ¿Empezamos?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39183" id="LblG2015_C39183">En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido. Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. De la logística se encarga MUNI, no desaproveches esta gran oportunidad. Cuéntanos, ¿Cómo te llamas y de qué ciudad te comunicas? RTA: Medellín</label><input type="text" class="form-control input-sm" id="G2015_C39183" value="<?php if (isset($_GET['G2015_C39183'])) {
                            echo $_GET['G2015_C39183'];
                        } ?>"  name="G2015_C39183"  placeholder="En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido. Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. De la logística se encarga MUNI, no desaproveches esta gran oportunidad. Cuéntanos, ¿Cómo te llamas y de qué ciudad te comunicas? RTA: Medellín"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    
                    <!-- JDBD-2020-05-10 : Campo tipo adjunto. -->
                    <div class="panel">
                        <label for="FG2015_C39184" id="LblFG2015_C39184">Invitar a hacer primera compra: (Sumar beneficio vigente: descuento,bono,etc) Empecemos con un primer pedido de 2 categorías para tí y así pruebas MUNI antes de vender a tus clientes. Beneficio actual: Si realizas tu primer pedido el día de hoy, te damos XXX [Explicamos Beneficio Actual]</label>
                        <input type="file" class="adjuntos" id="FG2015_C39184" name="FG2015_C39184">
                        <p class="help-block">
                            Peso maximo del archivo 9 MB
                        </p>
                        <input name="G2015_C39184" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G2015_C39184" name="G2015_C39184" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
                    </div>
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39185" id="LblG2015_C39185">Recibir Catalogo: Lo primero a hacer es escribir al  310 8952449 para recibir el catálogo y hacer tus pedidos!</label><input type="text" class="form-control input-sm" id="G2015_C39185" value="<?php if (isset($_GET['G2015_C39185'])) {
                            echo $_GET['G2015_C39185'];
                        } ?>"  name="G2015_C39185"  placeholder="Recibir Catalogo: Lo primero a hacer es escribir al  310 8952449 para recibir el catálogo y hacer tus pedidos!"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39186" id="LblG2015_C39186">Aclaración de  dudas para generar su compra: Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.</label><input type="text" class="form-control input-sm" id="G2015_C39186" value="<?php if (isset($_GET['G2015_C39186'])) {
                            echo $_GET['G2015_C39186'];
                        } ?>"  name="G2015_C39186"  placeholder="Aclaración de  dudas para generar su compra: Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39187" id="LblG2015_C39187">¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS.</label><input type="text" class="form-control input-sm" id="G2015_C39187" value="<?php if (isset($_GET['G2015_C39187'])) {
                            echo $_GET['G2015_C39187'];
                        } ?>"  name="G2015_C39187"  placeholder="¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39188" id="LblG2015_C39188">No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos.  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente .  ¡Que tengas un muy buen día!”</label><input type="text" class="form-control input-sm" id="G2015_C39188" value="<?php if (isset($_GET['G2015_C39188'])) {
                            echo $_GET['G2015_C39188'];
                        } ?>"  name="G2015_C39188"  placeholder="No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos.  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente .  ¡Que tengas un muy buen día!”"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5828" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5828c">
                PREGUNTAS FRECUENTES MEDELLIN 
            </a>
        </h4>
        
    </div>
    <div id="s_5828c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G2015_C39173" id="LblG2015_C39173">PREGUNTAS FRECUENTES MEDELLIN </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2015_C39173" id="G2015_C39173">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2318 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G2015_C39173" id="respuesta_LblG2015_C39173">Respuesta</label>
                        <textarea id="respuesta_G2015_C39173" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5832" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5832c">
                BENEFICIO MEDELLIN 
            </a>
        </h4>
        
    </div>
    <div id="s_5832c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39191" id="LblG2015_C39191">Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!</label><input type="text" class="form-control input-sm" id="G2015_C39191" value="<?php if (isset($_GET['G2015_C39191'])) {
                            echo $_GET['G2015_C39191'];
                        } ?>"  name="G2015_C39191"  placeholder="Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5833" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5833c">
                BENEFICIO REFERIDO
            </a>
        </h4>
        
    </div>
    <div id="s_5833c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2015_C39192" id="LblG2015_C39192">Por cada referido que genere una compra mayor a $60.000, te ganas un bono MUNI de $15.000 en tu siguiente compra!</label><input type="text" class="form-control input-sm" id="G2015_C39192" value="<?php if (isset($_GET['G2015_C39192'])) {
                            echo $_GET['G2015_C39192'];
                        } ?>"  name="G2015_C39192"  placeholder="Por cada referido que genere una compra mayor a $60.000, te ganas un bono MUNI de $15.000 en tu siguiente compra!"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2015_C36805">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2133;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G2015_C36805">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 2133;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G2015_C36806">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G2015_C36807" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G2015_C36808" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G2015_C36809" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G2015/G2015_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2015_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2015_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2015_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    $(".adjuntos").change(function(){
        var imax = $(this).attr("valor");
        var imagen = this.files[0];

        // if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" && imagen["type"] != "application/pdf"){
        //     $(".NuevaFoto").val(");
        //     swal({
        //         title : "Error al subir el archivo",
        //         text  : "El archivo debe estar en formato PNG , JPG, PDF",
        //         type  : "error",
        //         confirmButtonText : "Cerrar"
        //     });
        // }else 
        if(imagen["size"] > 9000000 ) {
            $(".NuevaFoto").val("");
            $(this).val("");
            swal({
                title : "Error al subir el archivo",
                text  : "El archivo no debe pesar mas de 9 MB",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }
    });
    
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2015_C36805").val(item.G2015_C36805).trigger("change");  
                $("#G2015_C36806").val(item.G2015_C36806).trigger("change");  
                $("#G2015_C36807").val(item.G2015_C36807); 
                $("#G2015_C36808").val(item.G2015_C36808); 
                $("#G2015_C36809").val(item.G2015_C36809); 
                $("#G2015_C36810").val(item.G2015_C36810); 
                $("#G2015_C36811").val(item.G2015_C36811); 
                $("#G2015_C36812").val(item.G2015_C36812); 
                $("#G2015_C36813").val(item.G2015_C36813); 
                $("#G2015_C39172").val(item.G2015_C39172).trigger("change");  
                $("#G2015_C39173").val(item.G2015_C39173).trigger("change");    
                if(item.G2015_C39174 == 1){
                    $("#G2015_C39174").attr('checked', true);
                }  
                $("#G2015_C39193").val(item.G2015_C39193); 
                $("#G2015_C39194").val(item.G2015_C39194);   
                if(item.G2015_C39175 == 1){
                    $("#G2015_C39175").attr('checked', true);
                }  
                $("#G2015_C39176").attr("opt",item.G2015_C39176);
                $("#G2015_C39176").val(item.G2015_C39176).trigger("change"); 
                $("#G2015_C39177").val(item.G2015_C39177).trigger("change");  
                $("#G2015_C39178").val(item.G2015_C39178); 
                $("#G2015_C39179").val(item.G2015_C39179); 
                $("#G2015_C39180").val(item.G2015_C39180);   
                if(item.G2015_C39181 == 1){
                    $("#G2015_C39181").attr('checked', true);
                }  
                $("#G2015_C39182").val(item.G2015_C39182); 
                $("#G2015_C39183").val(item.G2015_C39183); 
                if (item.G2015_C39184){
                    $("#G2015_C39184").val(item.G2015_C39184);
                }else{
                    $("#G2015_C39184").val("Sin Adjunto");
                } 
                $("#G2015_C39185").val(item.G2015_C39185); 
                $("#G2015_C39186").val(item.G2015_C39186); 
                $("#G2015_C39187").val(item.G2015_C39187); 
                $("#G2015_C39188").val(item.G2015_C39188);   
                if(item.G2015_C39189 == 1){
                    $("#G2015_C39189").attr('checked', true);
                }    
                if(item.G2015_C39190 == 1){
                    $("#G2015_C39190").attr('checked', true);
                }  
                $("#G2015_C39191").val(item.G2015_C39191); 
                $("#G2015_C39192").val(item.G2015_C39192);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        

                            $("#G2015_C39176").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G2015_C39176=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G2015_C39176").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G2015_C39176 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G2015_C39176").html('<option value="'+data.G_ConsInte__b+'" >'+data+'</option>');
                                        
                                    }
                                });
                            });

    $("#G2015_C39177").select2();

    $("#G2015_C39172").select2();

    $("#G2015_C39173").select2();
        //datepickers
        

        $("#G2015_C36807").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G2015_C36808").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Descargar la app: Lo primero a hacer es descargar la aplicación. ¡Hazlo aquí!: https://play.google.com/store/apps/details?id=com.muni.android.  RTA: Ya la descargue RTA: No la he descargado, ir a preguntas frecuentes.  

    $("#G2015_C39177").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PREGUNTAS FRECUENTES BOGOTÁ 

    $("#G2015_C39172").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G2015_C39172 option:selected").attr('respuesta');
        $("#respuesta_G2015_C39172").val(respuesta);
        

    });

    //function para PREGUNTAS FRECUENTES MEDELLIN  

    $("#G2015_C39173").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G2015_C39173 option:selected").attr('respuesta');
        $("#respuesta_G2015_C39173").val(respuesta);
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                    $("#G2015_C36805").val(item.G2015_C36805).trigger("change"); 
 
                    $("#G2015_C36806").val(item.G2015_C36806).trigger("change"); 
 
                                                $("#G2015_C36807").val(item.G2015_C36807);
 
                                                $("#G2015_C36808").val(item.G2015_C36808);
 
                                                $("#G2015_C36809").val(item.G2015_C36809);
 
                                                $("#G2015_C36810").val(item.G2015_C36810);
 
                                                $("#G2015_C36811").val(item.G2015_C36811);
 
                                                $("#G2015_C36812").val(item.G2015_C36812);
 
                                                $("#G2015_C36813").val(item.G2015_C36813);
 
                    $("#G2015_C39172").val(item.G2015_C39172).trigger("change"); 
 
                    $("#G2015_C39173").val(item.G2015_C39173).trigger("change"); 
      
                                                if(item.G2015_C39174 == 1){
                                                   $("#G2015_C39174").attr('checked', true);
                                                } 
 
                                                $("#G2015_C39193").val(item.G2015_C39193);
 
                                                $("#G2015_C39194").val(item.G2015_C39194);
      
                                                if(item.G2015_C39175 == 1){
                                                   $("#G2015_C39175").attr('checked', true);
                                                } 
 
                    $("#G2015_C39176").attr("opt",item.G2015_C39176);
                    $("#G2015_C39176").val(item.G2015_C39176).trigger("change");
 
                    $("#G2015_C39177").val(item.G2015_C39177).trigger("change"); 
 
                                                $("#G2015_C39178").val(item.G2015_C39178);
 
                                                $("#G2015_C39179").val(item.G2015_C39179);
 
                                                $("#G2015_C39180").val(item.G2015_C39180);
      
                                                if(item.G2015_C39181 == 1){
                                                   $("#G2015_C39181").attr('checked', true);
                                                } 
 
                                                $("#G2015_C39182").val(item.G2015_C39182);
 
                                                $("#G2015_C39183").val(item.G2015_C39183);
 
                                                if (item.G2015_C39184) {
                                                    $("#G2015_C39184").val(item.G2015_C39184);
                                                }else{
                                                    $("#G2015_C39184").val("Sin Adjunto");
                                                }
 
                                                $("#G2015_C39185").val(item.G2015_C39185);
 
                                                $("#G2015_C39186").val(item.G2015_C39186);
 
                                                $("#G2015_C39187").val(item.G2015_C39187);
 
                                                $("#G2015_C39188").val(item.G2015_C39188);
      
                                                if(item.G2015_C39189 == 1){
                                                   $("#G2015_C39189").attr('checked', true);
                                                } 
      
                                                if(item.G2015_C39190 == 1){
                                                   $("#G2015_C39190").attr('checked', true);
                                                } 
 
                                                $("#G2015_C39191").val(item.G2015_C39191);
 
                                                $("#G2015_C39192").val(item.G2015_C39192);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Agente','Fecha','Hora','Campaña','PREGUNTAS FRECUENTES BOGOTÁ','PREGUNTAS FRECUENTES MEDELLIN ','Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. ','De la logística se encarga MUNI, no desaproveches esta gran oportunidad. ¿Cuéntanos, como te llamas y de qué ciudad te comunicas?','Beneficio actual: Si realizas tu primer pedido el día de hoy, te damos XXX [Explicamos Beneficio Actual]','Descargar la app: Lo primero a hacer es descargar la aplicación. ¡Hazlo aquí!: https://play.google.com/store/apps/details?id=com.muni.android.  RTA: Ya la descargue RTA: No la he descargado, ir a preguntas frecuentes. ','Perfecto, para hacer el primer pedido llenamos la canasta de los productos que queremos y generas la órden, ¡el pedido te estará llegando el siguiente día de entrega y haces el pago una vez te llegue!','PARA GENERAR COMPRA: Tus clientes te hacen el pedido a través del catálogo digital, y cuando te lo envían tú los abres y pides desde la app. Con esto el pedido llegará separado por cliente a tu domicilio para que puedas venderselo a tus clientes  *Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.','DESPUES DE LA CONVERSIÓN: ¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS','Hola! gracias por contactarte con MUNI! MUNI es la plataforma que te permite generar ingresos adicionales brindando una gran variedad de productos a tu comunidad  ¿Empezamos?','En MUNI ayudamos a las personas a generar ingresos desde sus casas, lo hacemos a través de una app tecnológica y muy amigable, la idea es aprovechar tu red de contactos para que seas su líder de confianza y proveedor de mercado sin necesidad de tener una tienda, solo debes compartir el catálogo y por medio de 3 clicks generas tu pedido. Nosotros lo entregamos en la puerta de tu casa sin ningún costo adicional, recaudas el dinero y ganas de inmediato tu comisión. De la logística se encarga MUNI, no desaproveches esta gran oportunidad. Cuéntanos, ¿Cómo te llamas y de qué ciudad te comunicas? RTA: Medellín','Invitar a hacer primera compra: (Sumar beneficio vigente: descuento,bono,etc) Empecemos con un primer pedido de 2 categorías para tí y así pruebas MUNI antes de vender a tus clientes. Beneficio actual: Si realizas tu primer pedido el día de hoy, te damos XXX [Explicamos Beneficio Actual]','Recibir Catalogo: Lo primero a hacer es escribir al  310 8952449 para recibir el catálogo y hacer tus pedidos!','Aclaración de  dudas para generar su compra: Aclaramos sus dudas, siempre con una comunicación positiva e invitando a la acción para que genere su primera compra con MUNI y empiece a vender.','¡Vamoos por más! Ya hiciste tu primer pedido y ganaste tu comisión. Recuerda que entre más pedidos hagas, más ganas. Así que abre Whatsapp, mándale el catálogo a tus vecinos y amigos y convéncelos de que contigo, comprar tiene MUCHOS BENEFICIOS.','No está interesado: [Nombre cliente], muchas gracias por tu tiempo. Nos gustaría contarte sobre nuestro programa de referidos.  Por cada Líder Muni que nos refieras …[Explicamos beneficio Referidos actual]   Envíales nuestro contacto para que se comuniquen con nosotros o mándanos su número y nosotros los contactamos directamente .  ¡Que tengas un muy buen día!”','Descuento $10.000: Prueba MUNI en las próximas 2 horas y gana un descuento de $10.000! Si compras el día de hoy te damos un descuento adicional para que pruebes los productos Muni y empiecen a generar ingresos adicionales!','Por cada referido que genere una compra mayor a $60.000, te ganas un bono MUNI de $15.000 en tu siguiente compra!'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2015_C36810', 
                        index: 'G2015_C36810', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C36811', 
                        index: 'G2015_C36811', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C36812', 
                        index: 'G2015_C36812', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C36813', 
                        index: 'G2015_C36813', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39193', 
                        index: 'G2015_C39193', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39194', 
                        index: 'G2015_C39194', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39176', 
                        index:'G2015_C39176', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G2015_C39176=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G2015_C39177', 
                        index:'G2015_C39177', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2320&campo=G2015_C39177'
                        }
                    }

                    ,
                    { 
                        name:'G2015_C39178', 
                        index: 'G2015_C39178', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39179', 
                        index: 'G2015_C39179', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39180', 
                        index: 'G2015_C39180', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39182', 
                        index: 'G2015_C39182', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39183', 
                        index: 'G2015_C39183', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39184', 
                        index: 'G2015_C39184', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39185', 
                        index: 'G2015_C39185', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39186', 
                        index: 'G2015_C39186', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39187', 
                        index: 'G2015_C39187', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39188', 
                        index: 'G2015_C39188', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39191', 
                        index: 'G2015_C39191', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2015_C39192', 
                        index: 'G2015_C39192', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2015_C39172',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        
 
                    $("#G2015_C36805").val(item.G2015_C36805).trigger("change"); 
 
                    $("#G2015_C36806").val(item.G2015_C36806).trigger("change"); 

                        $("#G2015_C36807").val(item.G2015_C36807);

                        $("#G2015_C36808").val(item.G2015_C36808);

                        $("#G2015_C36809").val(item.G2015_C36809);

                        $("#G2015_C36810").val(item.G2015_C36810);

                        $("#G2015_C36811").val(item.G2015_C36811);

                        $("#G2015_C36812").val(item.G2015_C36812);

                        $("#G2015_C36813").val(item.G2015_C36813);
 
                    $("#G2015_C39172").val(item.G2015_C39172).trigger("change"); 
 
                    $("#G2015_C39173").val(item.G2015_C39173).trigger("change"); 
    
                        if(item.G2015_C39174 == 1){
                           $("#G2015_C39174").attr('checked', true);
                        } 

                        $("#G2015_C39193").val(item.G2015_C39193);

                        $("#G2015_C39194").val(item.G2015_C39194);
    
                        if(item.G2015_C39175 == 1){
                           $("#G2015_C39175").attr('checked', true);
                        } 
 
                    $("#G2015_C39176").attr("opt",item.G2015_C39176);
                    $("#G2015_C39176").val(item.G2015_C39176).trigger("change");
 
                    $("#G2015_C39177").val(item.G2015_C39177).trigger("change"); 

                        $("#G2015_C39178").val(item.G2015_C39178);

                        $("#G2015_C39179").val(item.G2015_C39179);

                        $("#G2015_C39180").val(item.G2015_C39180);
    
                        if(item.G2015_C39181 == 1){
                           $("#G2015_C39181").attr('checked', true);
                        } 

                        $("#G2015_C39182").val(item.G2015_C39182);

                        $("#G2015_C39183").val(item.G2015_C39183);

                        if (item.G2015_C39184) {
                            $("#G2015_C39184").val(item.G2015_C39184);
                        }else{
                            $("#G2015_C39184").val("Sin Adjunto");
                        }

                        $("#G2015_C39185").val(item.G2015_C39185);

                        $("#G2015_C39186").val(item.G2015_C39186);

                        $("#G2015_C39187").val(item.G2015_C39187);

                        $("#G2015_C39188").val(item.G2015_C39188);
    
                        if(item.G2015_C39189 == 1){
                           $("#G2015_C39189").attr('checked', true);
                        } 
    
                        if(item.G2015_C39190 == 1){
                           $("#G2015_C39190").attr('checked', true);
                        } 

                        $("#G2015_C39191").val(item.G2015_C39191);

                        $("#G2015_C39192").val(item.G2015_C39192);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
