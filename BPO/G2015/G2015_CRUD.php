<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G2015/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G2015/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G2015/".$archivo);
                        $tamaÃ±o = filesize("/Dyalogo/tmp/G2015/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaÃ±o);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G2015/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2015_ConsInte__b, G2015_FechaInsercion , G2015_Usuario ,  G2015_CodigoMiembro  , G2015_PoblacionOrigen , G2015_EstadoDiligenciamiento ,  G2015_IdLlamada , G2015_C39172 as principal ,G2015_C36805,G2015_C36806,G2015_C36807,G2015_C36808,G2015_C36809,G2015_C36810,G2015_C36811,G2015_C36812,G2015_C36813,G2015_C39172,G2015_C39173,G2015_C39193,G2015_C39194,G2015_C39176,G2015_C39177,G2015_C39178,G2015_C39179,G2015_C39180,G2015_C39182,G2015_C39183,G2015_C39184,G2015_C39185,G2015_C39186,G2015_C39187,G2015_C39188,G2015_C39191,G2015_C39192 FROM '.$BaseDatos.'.G2015 WHERE G2015_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2015_C36805'] = $key->G2015_C36805;

                $datos[$i]['G2015_C36806'] = $key->G2015_C36806;

                $datos[$i]['G2015_C36807'] = explode(' ', $key->G2015_C36807)[0];
  
                $hora = '';
                if(!is_null($key->G2015_C36808)){
                    $hora = explode(' ', $key->G2015_C36808)[1];
                }

                $datos[$i]['G2015_C36808'] = $hora;

                $datos[$i]['G2015_C36809'] = $key->G2015_C36809;

                $datos[$i]['G2015_C36810'] = $key->G2015_C36810;

                $datos[$i]['G2015_C36811'] = $key->G2015_C36811;

                $datos[$i]['G2015_C36812'] = $key->G2015_C36812;

                $datos[$i]['G2015_C36813'] = $key->G2015_C36813;

                $datos[$i]['G2015_C39172'] = $key->G2015_C39172;

                $datos[$i]['G2015_C39173'] = $key->G2015_C39173;

                $datos[$i]['G2015_C39193'] = $key->G2015_C39193;

                $datos[$i]['G2015_C39194'] = $key->G2015_C39194;

                $datos[$i]['G2015_C39176'] = $key->G2015_C39176;

                $datos[$i]['G2015_C39177'] = $key->G2015_C39177;

                $datos[$i]['G2015_C39178'] = $key->G2015_C39178;

                $datos[$i]['G2015_C39179'] = $key->G2015_C39179;

                $datos[$i]['G2015_C39180'] = $key->G2015_C39180;

                $datos[$i]['G2015_C39182'] = $key->G2015_C39182;

                $datos[$i]['G2015_C39183'] = $key->G2015_C39183;

                $datos[$i]['G2015_C39184'] = $key->G2015_C39184;

                $datos[$i]['G2015_C39185'] = $key->G2015_C39185;

                $datos[$i]['G2015_C39186'] = $key->G2015_C39186;

                $datos[$i]['G2015_C39187'] = $key->G2015_C39187;

                $datos[$i]['G2015_C39188'] = $key->G2015_C39188;

                $datos[$i]['G2015_C39191'] = $key->G2015_C39191;

                $datos[$i]['G2015_C39192'] = $key->G2015_C39192;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2015";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2015_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2015_ConsInte__b as id,  G2015_C39172 as camp1 , G2015_C39173 as camp2 
                     FROM ".$BaseDatos.".G2015  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2015_ConsInte__b as id,  G2015_C39172 as camp1 , G2015_C39173 as camp2  
                    FROM ".$BaseDatos.".G2015  JOIN ".$BaseDatos.".G2015_M".$_POST['muestra']." ON G2015_ConsInte__b = G2015_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2015_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2015_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2015_C39172 LIKE '%".$B."%' OR G2015_C39173 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2015_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2015_C39176'])){
                                $Ysql = "SELECT G_ConsInte__b as id,  as text FROM ".$BaseDatos.".G WHERE  LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2015_C39176"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G WHERE G_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2015_C39176"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2015");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2015_ConsInte__b, G2015_FechaInsercion , G2015_Usuario ,  G2015_CodigoMiembro  , G2015_PoblacionOrigen , G2015_EstadoDiligenciamiento ,  G2015_IdLlamada , G2015_C39172 as principal , a.LISOPC_Nombre____b as G2015_C36805, b.LISOPC_Nombre____b as G2015_C36806,G2015_C36807,G2015_C36808,G2015_C36809,G2015_C36810,G2015_C36811,G2015_C36812,G2015_C36813,G2015_C39172,G2015_C39173,G2015_C39193,G2015_C39194, c.LISOPC_Nombre____b as G2015_C39177,G2015_C39178,G2015_C39179,G2015_C39180,G2015_C39182,G2015_C39183,G2015_C39184,G2015_C39185,G2015_C39186,G2015_C39187,G2015_C39188,G2015_C39191,G2015_C39192 FROM '.$BaseDatos.'.G2015 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2015_C36805 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2015_C36806 LEFT JOIN '.$BaseDatos.'.G ON G_ConsInte__b  =  G2015_C39176 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2015_C39177';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2015 WHERE G2015_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2015";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2015_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2015_ConsInte__b as id,  G2015_C39172 as camp1 , G2015_C39173 as camp2  FROM '.$BaseDatos.'.G2015 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2015_ConsInte__b as id,  G2015_C39172 as camp1 , G2015_C39173 as camp2  
                    FROM ".$BaseDatos.".G2015  JOIN ".$BaseDatos.".G2015_M".$_POST['muestra']." ON G2015_ConsInte__b = G2015_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2015_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2015_C39172 LIKE "%'.$B.'%" OR G2015_C39173 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2015_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2015 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2015(";
            $LsqlV = " VALUES ("; 
 
            $G2015_C36805 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2015_C36805 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2015_C36805 = ".$G2015_C36805;
                    $LsqlI .= $separador." G2015_C36805";
                    $LsqlV .= $separador.$G2015_C36805;
                    $validar = 1;

                    
                }
            }
 
            $G2015_C36806 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2015_C36806 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2015_C36806 = ".$G2015_C36806;
                    $LsqlI .= $separador." G2015_C36806";
                    $LsqlV .= $separador.$G2015_C36806;
                    $validar = 1;
                }
            }
 
            $G2015_C36807 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2015_C36807 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2015_C36807 = ".$G2015_C36807;
                    $LsqlI .= $separador." G2015_C36807";
                    $LsqlV .= $separador.$G2015_C36807;
                    $validar = 1;
                }
            }
 
            $G2015_C36808 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2015_C36808 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2015_C36808 = ".$G2015_C36808;
                    $LsqlI .= $separador." G2015_C36808";
                    $LsqlV .= $separador.$G2015_C36808;
                    $validar = 1;
                }
            }
 
            $G2015_C36809 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2015_C36809 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2015_C36809 = ".$G2015_C36809;
                    $LsqlI .= $separador." G2015_C36809";
                    $LsqlV .= $separador.$G2015_C36809;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2015_C36810"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C36810 = '".$_POST["G2015_C36810"]."'";
                $LsqlI .= $separador."G2015_C36810";
                $LsqlV .= $separador."'".$_POST["G2015_C36810"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C36811"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C36811 = '".$_POST["G2015_C36811"]."'";
                $LsqlI .= $separador."G2015_C36811";
                $LsqlV .= $separador."'".$_POST["G2015_C36811"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C36812"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C36812 = '".$_POST["G2015_C36812"]."'";
                $LsqlI .= $separador."G2015_C36812";
                $LsqlV .= $separador."'".$_POST["G2015_C36812"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C36813"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C36813 = '".$_POST["G2015_C36813"]."'";
                $LsqlI .= $separador."G2015_C36813";
                $LsqlV .= $separador."'".$_POST["G2015_C36813"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39172"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39172 = '".$_POST["G2015_C39172"]."'";
                $LsqlI .= $separador."G2015_C39172";
                $LsqlV .= $separador."'".$_POST["G2015_C39172"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39173"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39173 = '".$_POST["G2015_C39173"]."'";
                $LsqlI .= $separador."G2015_C39173";
                $LsqlV .= $separador."'".$_POST["G2015_C39173"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39174"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39174 = '".$_POST["G2015_C39174"]."'";
                $LsqlI .= $separador."G2015_C39174";
                $LsqlV .= $separador."'".$_POST["G2015_C39174"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39193"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39193 = '".$_POST["G2015_C39193"]."'";
                $LsqlI .= $separador."G2015_C39193";
                $LsqlV .= $separador."'".$_POST["G2015_C39193"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39194"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39194 = '".$_POST["G2015_C39194"]."'";
                $LsqlI .= $separador."G2015_C39194";
                $LsqlV .= $separador."'".$_POST["G2015_C39194"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39175"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39175 = '".$_POST["G2015_C39175"]."'";
                $LsqlI .= $separador."G2015_C39175";
                $LsqlV .= $separador."'".$_POST["G2015_C39175"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39176"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39176 = '".$_POST["G2015_C39176"]."'";
                $LsqlI .= $separador."G2015_C39176";
                $LsqlV .= $separador."'".$_POST["G2015_C39176"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39177"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39177 = '".$_POST["G2015_C39177"]."'";
                $LsqlI .= $separador."G2015_C39177";
                $LsqlV .= $separador."'".$_POST["G2015_C39177"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39178"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39178 = '".$_POST["G2015_C39178"]."'";
                $LsqlI .= $separador."G2015_C39178";
                $LsqlV .= $separador."'".$_POST["G2015_C39178"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39179"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39179 = '".$_POST["G2015_C39179"]."'";
                $LsqlI .= $separador."G2015_C39179";
                $LsqlV .= $separador."'".$_POST["G2015_C39179"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39180"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39180 = '".$_POST["G2015_C39180"]."'";
                $LsqlI .= $separador."G2015_C39180";
                $LsqlV .= $separador."'".$_POST["G2015_C39180"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39181"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39181 = '".$_POST["G2015_C39181"]."'";
                $LsqlI .= $separador."G2015_C39181";
                $LsqlV .= $separador."'".$_POST["G2015_C39181"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39182"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39182 = '".$_POST["G2015_C39182"]."'";
                $LsqlI .= $separador."G2015_C39182";
                $LsqlV .= $separador."'".$_POST["G2015_C39182"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39183"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39183 = '".$_POST["G2015_C39183"]."'";
                $LsqlI .= $separador."G2015_C39183";
                $LsqlV .= $separador."'".$_POST["G2015_C39183"]."'";
                $validar = 1;
            }
             
  
            if (isset($_FILES["FG2015_C39184"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G2015/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G2015")){
                    mkdir("/Dyalogo/tmp/G2015", 0777);
                }
                if ($_FILES["FG2015_C39184"]["size"] != 0) {
                    $G2015_C39184 = $_FILES["FG2015_C39184"]["tmp_name"];
                    $nG2015_C39184 = $fechUp."_".$_FILES["FG2015_C39184"]["name"];
                    $rutaFinal = $destinoFile.$nG2015_C39184;
                    if (is_uploaded_file($G2015_C39184)) {
                        move_uploaded_file($G2015_C39184, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2015_C39184 = '".$nG2015_C39184."'";
                    $LsqlI .= $separador."G2015_C39184";
                    $LsqlV .= $separador."'".$nG2015_C39184."'";
                    $validar = 1;
                }
            }
            
  
            if(isset($_POST["G2015_C39185"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39185 = '".$_POST["G2015_C39185"]."'";
                $LsqlI .= $separador."G2015_C39185";
                $LsqlV .= $separador."'".$_POST["G2015_C39185"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39186"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39186 = '".$_POST["G2015_C39186"]."'";
                $LsqlI .= $separador."G2015_C39186";
                $LsqlV .= $separador."'".$_POST["G2015_C39186"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39187"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39187 = '".$_POST["G2015_C39187"]."'";
                $LsqlI .= $separador."G2015_C39187";
                $LsqlV .= $separador."'".$_POST["G2015_C39187"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39188"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39188 = '".$_POST["G2015_C39188"]."'";
                $LsqlI .= $separador."G2015_C39188";
                $LsqlV .= $separador."'".$_POST["G2015_C39188"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39189"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39189 = '".$_POST["G2015_C39189"]."'";
                $LsqlI .= $separador."G2015_C39189";
                $LsqlV .= $separador."'".$_POST["G2015_C39189"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39190"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39190 = '".$_POST["G2015_C39190"]."'";
                $LsqlI .= $separador."G2015_C39190";
                $LsqlV .= $separador."'".$_POST["G2015_C39190"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39191"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39191 = '".$_POST["G2015_C39191"]."'";
                $LsqlI .= $separador."G2015_C39191";
                $LsqlV .= $separador."'".$_POST["G2015_C39191"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2015_C39192"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_C39192 = '".$_POST["G2015_C39192"]."'";
                $LsqlI .= $separador."G2015_C39192";
                $LsqlV .= $separador."'".$_POST["G2015_C39192"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2015_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2015_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2015_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2015_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2015_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2015_Usuario , G2015_FechaInsercion, G2015_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2015_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2015 WHERE G2015_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
