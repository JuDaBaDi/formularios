
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1875/G1875_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1875_ConsInte__b as id, G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1875  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1875_C34469 WHERE G1875_Usuario = ".$idUsuario." ORDER BY G1875_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1875_ConsInte__b as id, G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1875  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1875_C34469 ORDER BY G1875_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1875_ConsInte__b as id, G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1875 JOIN ".$BaseDatos.".G1875_M".$resultEstpas->muestr." ON G1875_ConsInte__b = G1875_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1875_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1875_ConsInte__b as id, G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1875 JOIN ".$BaseDatos.".G1875_M".$resultEstpas->muestr." ON G1875_ConsInte__b = G1875_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1875_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1875_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1875_ConsInte__b as id, G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1875  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1875_C34469 ORDER BY G1875_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="4889" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4889c">
                DATOS BASICOS
            </a>
        </h4>
        
    </div>
    <div id="s_4889c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34468" id="LblG1875_C34468">Nombres y Apellidos</label><input type="text" class="form-control input-sm" id="G1875_C34468" value="<?php if (isset($_GET['G1875_C34468'])) {
                            echo $_GET['G1875_C34468'];
                        } ?>"  name="G1875_C34468"  placeholder="Nombres y Apellidos"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34469" id="LblG1875_C34469">¿Qué Persona se Está Comunicando?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34469" id="G1875_C34469">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1924 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34470" id="LblG1875_C34470">Número de contacto</label><input type="text" class="form-control input-sm" id="G1875_C34470" value="<?php if (isset($_GET['G1875_C34470'])) {
                            echo $_GET['G1875_C34470'];
                        } ?>"  name="G1875_C34470"  placeholder="Número de contacto"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="4891" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34462" id="LblG1875_C34462">Agente</label><input type="text" class="form-control input-sm" id="G1875_C34462" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G1875_C34462"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34463" id="LblG1875_C34463">Fecha</label><input type="text" class="form-control input-sm" id="G1875_C34463" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G1875_C34463"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34464" id="LblG1875_C34464">Hora</label><input type="text" class="form-control input-sm" id="G1875_C34464" value="<?php echo date('H:i:s');?>" readonly name="G1875_C34464"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34465" id="LblG1875_C34465">Campaña</label><input type="text" class="form-control input-sm" id="G1875_C34465" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G1875_C34465"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="4892" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4892c">
                VISTA PARA LLAMADAS DE FAMILIAR
            </a>
        </h4>
        
    </div>
    <div id="s_4892c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34466" id="LblG1875_C34466">Número de Identificación del estudiante</label><input type="text" class="form-control input-sm" id="G1875_C34466" value="<?php if (isset($_GET['G1875_C34466'])) {
                            echo $_GET['G1875_C34466'];
                        } ?>"  name="G1875_C34466"  placeholder="Número de Identificación del estudiante"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34467" id="LblG1875_C34467">¿En qué grado se encuentra el estudiante?</label><input type="text" class="form-control input-sm" id="G1875_C34467" value="<?php if (isset($_GET['G1875_C34467'])) {
                            echo $_GET['G1875_C34467'];
                        } ?>"  name="G1875_C34467"  placeholder="¿En qué grado se encuentra el estudiante?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34471" id="LblG1875_C34471">Nombres y Apellidos del Estudiante que Representa</label><input type="text" class="form-control input-sm" id="G1875_C34471" value="<?php if (isset($_GET['G1875_C34471'])) {
                            echo $_GET['G1875_C34471'];
                        } ?>"  name="G1875_C34471"  placeholder="Nombres y Apellidos del Estudiante que Representa"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34472" id="LblG1875_C34472">¿En qué colegio se encuentra el estudiante?</label><input type="text" class="form-control input-sm" id="G1875_C34472" value="<?php if (isset($_GET['G1875_C34472'])) {
                            echo $_GET['G1875_C34472'];
                        } ?>"  name="G1875_C34472"  placeholder="¿En qué colegio se encuentra el estudiante?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34473" id="LblG1875_C34473">Número Celular de familiar</label><input type="text" class="form-control input-sm" id="G1875_C34473" value="<?php if (isset($_GET['G1875_C34473'])) {
                            echo $_GET['G1875_C34473'];
                        } ?>"  name="G1875_C34473"  placeholder="Número Celular de familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34474" id="LblG1875_C34474">Teléfono Adicional de familiar</label><input type="text" class="form-control input-sm" id="G1875_C34474" value="<?php if (isset($_GET['G1875_C34474'])) {
                            echo $_GET['G1875_C34474'];
                        } ?>"  name="G1875_C34474"  placeholder="Teléfono Adicional de familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34475" id="LblG1875_C34475">Correo Electrónico de familiar</label><input type="text" class="form-control input-sm" id="G1875_C34475" value="<?php if (isset($_GET['G1875_C34475'])) {
                            echo $_GET['G1875_C34475'];
                        } ?>"  name="G1875_C34475"  placeholder="Correo Electrónico de familiar"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34639" id="LblG1875_C34639">Edad del estudiante</label><input type="text" class="form-control input-sm" id="G1875_C34639" value="<?php if (isset($_GET['G1875_C34639'])) {
                            echo $_GET['G1875_C34639'];
                        } ?>"  name="G1875_C34639"  placeholder="Edad del estudiante"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34640" id="LblG1875_C34640">Ciudad</label><input type="text" class="form-control input-sm" id="G1875_C34640" value="<?php if (isset($_GET['G1875_C34640'])) {
                            echo $_GET['G1875_C34640'];
                        } ?>"  name="G1875_C34640"  placeholder="Ciudad"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34641" id="LblG1875_C34641">Localidad</label><input type="text" class="form-control input-sm" id="G1875_C34641" value="<?php if (isset($_GET['G1875_C34641'])) {
                            echo $_GET['G1875_C34641'];
                        } ?>"  name="G1875_C34641"  placeholder="Localidad"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34642" id="LblG1875_C34642">Barrio</label><input type="text" class="form-control input-sm" id="G1875_C34642" value="<?php if (isset($_GET['G1875_C34642'])) {
                            echo $_GET['G1875_C34642'];
                        } ?>"  name="G1875_C34642"  placeholder="Barrio"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4893" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4893c">
                VISTA PARA LLAMADAS DE ESTUDIANTE
            </a>
        </h4>
        
    </div>
    <div id="s_4893c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34476" id="LblG1875_C34476">Tiene autorización o está acompañado de un adulto </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34476" id="G1875_C34476">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34643" id="LblG1875_C34643">Edad</label><input type="text" class="form-control input-sm" id="G1875_C34643" value="<?php if (isset($_GET['G1875_C34643'])) {
                            echo $_GET['G1875_C34643'];
                        } ?>"  name="G1875_C34643"  placeholder="Edad"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34477" id="LblG1875_C34477">Colegio Donde Estudia</label><input type="text" class="form-control input-sm" id="G1875_C34477" value="<?php if (isset($_GET['G1875_C34477'])) {
                            echo $_GET['G1875_C34477'];
                        } ?>"  name="G1875_C34477"  placeholder="Colegio Donde Estudia"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34480" id="LblG1875_C34480">Grado Que Está Cursando</label><input type="text" class="form-control input-sm" id="G1875_C34480" value="<?php if (isset($_GET['G1875_C34480'])) {
                            echo $_GET['G1875_C34480'];
                        } ?>"  name="G1875_C34480"  placeholder="Grado Que Está Cursando"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34478" id="LblG1875_C34478">Ciudad del Estudiante</label><input type="text" class="form-control input-sm" id="G1875_C34478" value="<?php if (isset($_GET['G1875_C34478'])) {
                            echo $_GET['G1875_C34478'];
                        } ?>"  name="G1875_C34478"  placeholder="Ciudad del Estudiante"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34479" id="LblG1875_C34479">Barrio del Estudiante</label><input type="text" class="form-control input-sm" id="G1875_C34479" value="<?php if (isset($_GET['G1875_C34479'])) {
                            echo $_GET['G1875_C34479'];
                        } ?>"  name="G1875_C34479"  placeholder="Barrio del Estudiante"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="4890" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="4894" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4894c">
                VISTA PARA LLAMADAS DE DOCENTE, RECTOR, COORDINADOR, ORIENTADOR
            </a>
        </h4>
        
    </div>
    <div id="s_4894c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34481" id="LblG1875_C34481">Localidad.</label><input type="text" class="form-control input-sm" id="G1875_C34481" value="<?php if (isset($_GET['G1875_C34481'])) {
                            echo $_GET['G1875_C34481'];
                        } ?>"  name="G1875_C34481"  placeholder="Localidad."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34486" id="LblG1875_C34486">Barrio.</label><input type="text" class="form-control input-sm" id="G1875_C34486" value="<?php if (isset($_GET['G1875_C34486'])) {
                            echo $_GET['G1875_C34486'];
                        } ?>"  name="G1875_C34486"  placeholder="Barrio."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34482" id="LblG1875_C34482">Colegio donde trabaja.</label><input type="text" class="form-control input-sm" id="G1875_C34482" value="<?php if (isset($_GET['G1875_C34482'])) {
                            echo $_GET['G1875_C34482'];
                        } ?>"  name="G1875_C34482"  placeholder="Colegio donde trabaja."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34485" id="LblG1875_C34485">Ciudad.</label><input type="text" class="form-control input-sm" id="G1875_C34485" value="<?php if (isset($_GET['G1875_C34485'])) {
                            echo $_GET['G1875_C34485'];
                        } ?>"  name="G1875_C34485"  placeholder="Ciudad."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34483" id="LblG1875_C34483">Nivel que está enseñando este año.</label><input type="text" class="form-control input-sm" id="G1875_C34483" value="<?php if (isset($_GET['G1875_C34483'])) {
                            echo $_GET['G1875_C34483'];
                        } ?>"  name="G1875_C34483"  placeholder="Nivel que está enseñando este año."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34484" id="LblG1875_C34484">Área que está enseñando este año.</label><input type="text" class="form-control input-sm" id="G1875_C34484" value="<?php if (isset($_GET['G1875_C34484'])) {
                            echo $_GET['G1875_C34484'];
                        } ?>"  name="G1875_C34484"  placeholder="Área que está enseñando este año."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34487" id="LblG1875_C34487">Número Celular.</label><input type="text" class="form-control input-sm" id="G1875_C34487" value="<?php if (isset($_GET['G1875_C34487'])) {
                            echo $_GET['G1875_C34487'];
                        } ?>"  name="G1875_C34487"  placeholder="Número Celular."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34488" id="LblG1875_C34488">Teléfono Adicional.</label><input type="text" class="form-control input-sm" id="G1875_C34488" value="<?php if (isset($_GET['G1875_C34488'])) {
                            echo $_GET['G1875_C34488'];
                        } ?>"  name="G1875_C34488"  placeholder="Teléfono Adicional."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34489" id="LblG1875_C34489">Correo Electrónico.</label><input type="text" class="form-control input-sm" id="G1875_C34489" value="<?php if (isset($_GET['G1875_C34489'])) {
                            echo $_GET['G1875_C34489'];
                        } ?>"  name="G1875_C34489"  placeholder="Correo Electrónico."></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34645" id="LblG1875_C34645">¿Es Docente de un Establecimiento Educativo Oficial?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34645" id="G1875_C34645">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34646" id="LblG1875_C34646">¿Tiene nombramiento en propiedad o provisional?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34646" id="G1875_C34646">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1947 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4895" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4895c">
                VISTA PARA LLAMADAS DE OTROS
            </a>
        </h4>
        
    </div>
    <div id="s_4895c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34490" id="LblG1875_C34490">Número de Identificación de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34490" value="<?php if (isset($_GET['G1875_C34490'])) {
                            echo $_GET['G1875_C34490'];
                        } ?>"  name="G1875_C34490"  placeholder="Número de Identificación de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34491" id="LblG1875_C34491">Correo Electrónico de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34491" value="<?php if (isset($_GET['G1875_C34491'])) {
                            echo $_GET['G1875_C34491'];
                        } ?>"  name="G1875_C34491"  placeholder="Correo Electrónico de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34492" id="LblG1875_C34492">Número Celular de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34492" value="<?php if (isset($_GET['G1875_C34492'])) {
                            echo $_GET['G1875_C34492'];
                        } ?>"  name="G1875_C34492"  placeholder="Número Celular de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34493" id="LblG1875_C34493">Teléfono Adicional de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34493" value="<?php if (isset($_GET['G1875_C34493'])) {
                            echo $_GET['G1875_C34493'];
                        } ?>"  name="G1875_C34493"  placeholder="Teléfono Adicional de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34636" id="LblG1875_C34636">Ciudad de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34636" value="<?php if (isset($_GET['G1875_C34636'])) {
                            echo $_GET['G1875_C34636'];
                        } ?>"  name="G1875_C34636"  placeholder="Ciudad de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34637" id="LblG1875_C34637">Localidad de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34637" value="<?php if (isset($_GET['G1875_C34637'])) {
                            echo $_GET['G1875_C34637'];
                        } ?>"  name="G1875_C34637"  placeholder="Localidad de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1875_C34638" id="LblG1875_C34638">Barrio de Otro</label><input type="text" class="form-control input-sm" id="G1875_C34638" value="<?php if (isset($_GET['G1875_C34638'])) {
                            echo $_GET['G1875_C34638'];
                        } ?>"  name="G1875_C34638"  placeholder="Barrio de Otro"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4899" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4899c">
                TPIFICACIÓN
            </a>
        </h4>
        
    </div>
    <div id="s_4899c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34501" id="LblG1875_C34501">TIPIFICACION</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34501" id="G1875_C34501">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1929 ORDER BY LISOPC_ConsInte__b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34502" id="LblG1875_C34502">SUBTIPIFICACION</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34502" id="G1875_C34502">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1930 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1875_C34570" id="LblG1875_C34570">OBSERVACIONES AGENTE</label>
                        <textarea class="form-control input-sm" name="G1875_C34570" id="G1875_C34570"  value="<?php if (isset($_GET['G1875_C34570'])) {
                            echo $_GET['G1875_C34570'];
                        } ?>" placeholder="OBSERVACIONES AGENTE"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4896" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4896c">
                AGENDAMIENTO DE CITA
            </a>
        </h4>
        
    </div>
    <div id="s_4896c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34496" id="LblG1875_C34496">CONSULTOR</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34496" id="G1875_C34496">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1927 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1875_C34494" id="LblG1875_C34494">FECHA DE CITA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1875_C34494'])) {
                            echo $_GET['G1875_C34494'];
                        } ?>"  name="G1875_C34494" id="G1875_C34494" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G1875_C34495" id="LblG1875_C34495">HORA DE LA CITA</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora"  name="G1875_C34495" id="G1875_C34495" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G1875_C34495">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4897" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4897c">
                DISPONIBILIDAD DE CITAS
            </a>
        </h4>
        
    </div>
    <div id="s_4897c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">DISPONIBILIDAD DE CITAS</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear DISPONIBILIDAD DE CITAS" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1875_C34457">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1928;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1875_C34457">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1928;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1875_C34458">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1875_C34459" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1875_C34460" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1875_C34461" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G1875/G1875_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1875_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1875_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1875_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G1875_C34468").val(item.G1875_C34468); 
                $("#G1875_C34469").val(item.G1875_C34469).trigger("change");  
                $("#G1875_C34470").val(item.G1875_C34470); 
                $("#G1875_C34457").val(item.G1875_C34457).trigger("change");  
                $("#G1875_C34458").val(item.G1875_C34458).trigger("change");  
                $("#G1875_C34459").val(item.G1875_C34459); 
                $("#G1875_C34460").val(item.G1875_C34460); 
                $("#G1875_C34461").val(item.G1875_C34461); 
                $("#G1875_C34462").val(item.G1875_C34462); 
                $("#G1875_C34463").val(item.G1875_C34463); 
                $("#G1875_C34464").val(item.G1875_C34464); 
                $("#G1875_C34465").val(item.G1875_C34465); 
                $("#G1875_C34466").val(item.G1875_C34466); 
                $("#G1875_C34467").val(item.G1875_C34467); 
                $("#G1875_C34471").val(item.G1875_C34471); 
                $("#G1875_C34472").val(item.G1875_C34472); 
                $("#G1875_C34473").val(item.G1875_C34473); 
                $("#G1875_C34474").val(item.G1875_C34474); 
                $("#G1875_C34475").val(item.G1875_C34475); 
                $("#G1875_C34639").val(item.G1875_C34639); 
                $("#G1875_C34640").val(item.G1875_C34640); 
                $("#G1875_C34641").val(item.G1875_C34641); 
                $("#G1875_C34642").val(item.G1875_C34642); 
                $("#G1875_C34476").val(item.G1875_C34476).trigger("change");  
                $("#G1875_C34643").val(item.G1875_C34643); 
                $("#G1875_C34477").val(item.G1875_C34477); 
                $("#G1875_C34480").val(item.G1875_C34480); 
                $("#G1875_C34478").val(item.G1875_C34478); 
                $("#G1875_C34479").val(item.G1875_C34479); 
                $("#G1875_C34481").val(item.G1875_C34481); 
                $("#G1875_C34486").val(item.G1875_C34486); 
                $("#G1875_C34482").val(item.G1875_C34482); 
                $("#G1875_C34485").val(item.G1875_C34485); 
                $("#G1875_C34483").val(item.G1875_C34483); 
                $("#G1875_C34484").val(item.G1875_C34484); 
                $("#G1875_C34487").val(item.G1875_C34487); 
                $("#G1875_C34488").val(item.G1875_C34488); 
                $("#G1875_C34489").val(item.G1875_C34489); 
                $("#G1875_C34645").val(item.G1875_C34645).trigger("change");  
                $("#G1875_C34646").val(item.G1875_C34646).trigger("change");  
                $("#G1875_C34490").val(item.G1875_C34490); 
                $("#G1875_C34491").val(item.G1875_C34491); 
                $("#G1875_C34492").val(item.G1875_C34492); 
                $("#G1875_C34493").val(item.G1875_C34493); 
                $("#G1875_C34636").val(item.G1875_C34636); 
                $("#G1875_C34637").val(item.G1875_C34637); 
                $("#G1875_C34638").val(item.G1875_C34638); 
                $("#G1875_C34496").val(item.G1875_C34496).trigger("change");  
                $("#G1875_C34494").val(item.G1875_C34494); 
                $("#G1875_C34495").val(item.G1875_C34495); 
                $("#G1875_C34501").val(item.G1875_C34501).trigger("change");  
                $("#G1875_C34502").attr("opt",item.G1875_C34502);  
                $("#G1875_C34570").val(item.G1875_C34570);
                
                cargarHijos_0(
        $("#G1875_C34496").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G1875_C34496").val());
            var id_0 = $("#G1875_C34496").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G1875_C34496").val());
            var id_0 = $("#G1875_C34496").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1875_C34496").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1874&view=si&formaDetalle=si&formularioPadre=1875&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=34453<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1874&view=si&formaDetalle=si&formularioPadre=1875&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=34453&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1874&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1875&pincheCampo=34453&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G1875_C34469").select2();

    $("#G1875_C34476").select2();

    $("#G1875_C34645").select2();

    $("#G1875_C34646").select2();

    $("#G1875_C34501").select2();

    $("#G1875_C34502").select2();

    $("#G1875_C34496").select2();
        //datepickers
        

        $("#G1875_C34459").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1875_C34494").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1875_C34460").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'HORA DE LA CITA', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1875_C34495").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ¿Qué Persona se Está Comunicando? 

    $("#G1875_C34469").change(function(){ 
        $("#G1875_C34476").prop('disabled', false);
    
        $("#G1875_C34477").prop('disabled', false);
    
        $("#G1875_C34478").prop('disabled', false);
    
        $("#G1875_C34479").prop('disabled', false);
    
        $("#G1875_C34480").prop('disabled', false);
    
        $("#G1875_C34481").prop('disabled', false);
    
        $("#G1875_C34482").prop('disabled', false);
    
        $("#G1875_C34483").prop('disabled', false);
    
        $("#G1875_C34484").prop('disabled', false);
    
        $("#G1875_C34485").prop('disabled', false);
    
        $("#G1875_C34486").prop('disabled', false);
    
        $("#G1875_C34487").prop('disabled', false);
    
        $("#G1875_C34488").prop('disabled', false);
    
        $("#G1875_C34489").prop('disabled', false);
    
        $("#G1875_C34490").prop('disabled', false);
    
        $("#G1875_C34491").prop('disabled', false);
    
        $("#G1875_C34492").prop('disabled', false);
    
        $("#G1875_C34493").prop('disabled', false);
    
        $("#G1875_C34466").prop('disabled', false);
    
        $("#G1875_C34467").prop('disabled', false);
    
        $("#G1875_C34471").prop('disabled', false);
    
        $("#G1875_C34472").prop('disabled', false);
    
        $("#G1875_C34473").prop('disabled', false);
    
        $("#G1875_C34474").prop('disabled', false);
    
        $("#G1875_C34475").prop('disabled', false);
    
        $("#G1875_C34481").prop('disabled', false);
    
        $("#G1875_C34482").prop('disabled', false);
    
        $("#G1875_C34483").prop('disabled', false);
    
        $("#G1875_C34484").prop('disabled', false);
    
        $("#G1875_C34485").prop('disabled', false);
    
        $("#G1875_C34486").prop('disabled', false);
    
        $("#G1875_C34487").prop('disabled', false);
    
        $("#G1875_C34488").prop('disabled', false);
    
        $("#G1875_C34489").prop('disabled', false);
    
        $("#G1875_C34490").prop('disabled', false);
    
        $("#G1875_C34491").prop('disabled', false);
    
        $("#G1875_C34492").prop('disabled', false);
    
        $("#G1875_C34493").prop('disabled', false);
    
        $("#G1875_C34466").prop('disabled', false);
    
        $("#G1875_C34467").prop('disabled', false);
    
        $("#G1875_C34471").prop('disabled', false);
    
        $("#G1875_C34472").prop('disabled', false);
    
        $("#G1875_C34473").prop('disabled', false);
    
        $("#G1875_C34474").prop('disabled', false);
    
        $("#G1875_C34475").prop('disabled', false);
    
        $("#G1875_C34476").prop('disabled', false);
    
        $("#G1875_C34477").prop('disabled', false);
    
        $("#G1875_C34478").prop('disabled', false);
    
        $("#G1875_C34479").prop('disabled', false);
    
        $("#G1875_C34480").prop('disabled', false);
    
        $("#G1875_C34490").prop('disabled', false);
    
        $("#G1875_C34491").prop('disabled', false);
    
        $("#G1875_C34492").prop('disabled', false);
    
        $("#G1875_C34493").prop('disabled', false);
    
        $("#G1875_C34466").prop('disabled', false);
    
        $("#G1875_C34467").prop('disabled', false);
    
        $("#G1875_C34471").prop('disabled', false);
    
        $("#G1875_C34472").prop('disabled', false);
    
        $("#G1875_C34473").prop('disabled', false);
    
        $("#G1875_C34474").prop('disabled', false);
    
        $("#G1875_C34475").prop('disabled', false);
    
        $("#G1875_C34476").prop('disabled', false);
    
        $("#G1875_C34477").prop('disabled', false);
    
        $("#G1875_C34478").prop('disabled', false);
    
        $("#G1875_C34479").prop('disabled', false);
    
        $("#G1875_C34480").prop('disabled', false);
    
        $("#G1875_C34490").prop('disabled', false);
    
        $("#G1875_C34491").prop('disabled', false);
    
        $("#G1875_C34492").prop('disabled', false);
    
        $("#G1875_C34493").prop('disabled', false);
    
        $("#G1875_C34466").prop('disabled', false);
    
        $("#G1875_C34467").prop('disabled', false);
    
        $("#G1875_C34471").prop('disabled', false);
    
        $("#G1875_C34472").prop('disabled', false);
    
        $("#G1875_C34473").prop('disabled', false);
    
        $("#G1875_C34474").prop('disabled', false);
    
        $("#G1875_C34475").prop('disabled', false);
    
        $("#G1875_C34476").prop('disabled', false);
    
        $("#G1875_C34477").prop('disabled', false);
    
        $("#G1875_C34478").prop('disabled', false);
    
        $("#G1875_C34479").prop('disabled', false);
    
        $("#G1875_C34480").prop('disabled', false);
    
        $("#G1875_C34490").prop('disabled', false);
    
        $("#G1875_C34491").prop('disabled', false);
    
        $("#G1875_C34492").prop('disabled', false);
    
        $("#G1875_C34493").prop('disabled', false);
    
        $("#G1875_C34466").prop('disabled', false);
    
        $("#G1875_C34467").prop('disabled', false);
    
        $("#G1875_C34471").prop('disabled', false);
    
        $("#G1875_C34472").prop('disabled', false);
    
        $("#G1875_C34473").prop('disabled', false);
    
        $("#G1875_C34474").prop('disabled', false);
    
        $("#G1875_C34475").prop('disabled', false);
    
        $("#G1875_C34476").prop('disabled', false);
    
        $("#G1875_C34477").prop('disabled', false);
    
        $("#G1875_C34478").prop('disabled', false);
    
        $("#G1875_C34479").prop('disabled', false);
    
        $("#G1875_C34480").prop('disabled', false);
    
        $("#G1875_C34490").prop('disabled', false);
    
        $("#G1875_C34491").prop('disabled', false);
    
        $("#G1875_C34492").prop('disabled', false);
    
        $("#G1875_C34493").prop('disabled', false);
    
        $("#G1875_C34466").prop('disabled', false);
    
        $("#G1875_C34467").prop('disabled', false);
    
        $("#G1875_C34471").prop('disabled', false);
    
        $("#G1875_C34472").prop('disabled', false);
    
        $("#G1875_C34473").prop('disabled', false);
    
        $("#G1875_C34474").prop('disabled', false);
    
        $("#G1875_C34475").prop('disabled', false);
    
        $("#G1875_C34476").prop('disabled', false);
    
        $("#G1875_C34477").prop('disabled', false);
    
        $("#G1875_C34478").prop('disabled', false);
    
        $("#G1875_C34479").prop('disabled', false);
    
        $("#G1875_C34480").prop('disabled', false);
    
        $("#G1875_C34481").prop('disabled', false);
    
        $("#G1875_C34482").prop('disabled', false);
    
        $("#G1875_C34483").prop('disabled', false);
    
        $("#G1875_C34484").prop('disabled', false);
    
        $("#G1875_C34485").prop('disabled', false);
    
        $("#G1875_C34486").prop('disabled', false);
    
        $("#G1875_C34487").prop('disabled', false);
    
        $("#G1875_C34488").prop('disabled', false);
    
        $("#G1875_C34489").prop('disabled', false);
    
        $("#G1875_C34636").prop('disabled', false);
    
        $("#G1875_C34637").prop('disabled', false);
    
        $("#G1875_C34638").prop('disabled', false);
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169807'){
            $("#G1875_C34476").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34481").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34481").prop('disabled', true); 
          
            $("#G1875_C34482").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34482").prop('disabled', true); 
          
            $("#G1875_C34483").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34483").prop('disabled', true); 
          
            $("#G1875_C34484").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34484").prop('disabled', true); 
          
            $("#G1875_C34485").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34485").prop('disabled', true); 
          
            $("#G1875_C34486").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34486").prop('disabled', true); 
          
            $("#G1875_C34487").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34487").prop('disabled', true); 
          
            $("#G1875_C34488").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34488").prop('disabled', true); 
          
            $("#G1875_C34489").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34489").prop('disabled', true); 
          
            $("#G1875_C34490").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34493").prop('disabled', true); 
          
        }
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169808'){
            $("#G1875_C34466").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34481").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34481").prop('disabled', true); 
          
            $("#G1875_C34482").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34482").prop('disabled', true); 
          
            $("#G1875_C34483").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34483").prop('disabled', true); 
          
            $("#G1875_C34484").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34484").prop('disabled', true); 
          
            $("#G1875_C34485").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34485").prop('disabled', true); 
          
            $("#G1875_C34486").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34486").prop('disabled', true); 
          
            $("#G1875_C34487").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34487").prop('disabled', true); 
          
            $("#G1875_C34488").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34488").prop('disabled', true); 
          
            $("#G1875_C34489").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34489").prop('disabled', true); 
          
            $("#G1875_C34490").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34493").prop('disabled', true); 
          
        }
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169809'){
            $("#G1875_C34466").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34493").prop('disabled', true); 
          
        }
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169810'){
            $("#G1875_C34466").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34493").prop('disabled', true); 
          
        }
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169811'){
            $("#G1875_C34466").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34493").prop('disabled', true); 
          
        }
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169812'){
            $("#G1875_C34466").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34493").prop('disabled', true); 
          
        }
    
        //JDBD-20-05-17: funcion para saltos.
        if($(this).val() == '169813'){
            $("#G1875_C34466").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34481").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34481").prop('disabled', true); 
          
            $("#G1875_C34482").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34482").prop('disabled', true); 
          
            $("#G1875_C34483").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34483").prop('disabled', true); 
          
            $("#G1875_C34484").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34484").prop('disabled', true); 
          
            $("#G1875_C34485").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34485").prop('disabled', true); 
          
            $("#G1875_C34486").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34486").prop('disabled', true); 
          
            $("#G1875_C34487").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34487").prop('disabled', true); 
          
            $("#G1875_C34488").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34488").prop('disabled', true); 
          
            $("#G1875_C34489").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34489").prop('disabled', true); 
          
            $("#G1875_C34636").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34636").prop('disabled', true); 
          
            $("#G1875_C34637").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34637").prop('disabled', true); 
          
            $("#G1875_C34638").closest(".form-group").removeClass("has-error"); 
            $("#G1875_C34638").prop('disabled', true); 
          
        }
     
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tiene autorización o está acompañado de un adulto  

    $("#G1875_C34476").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Es Docente de un Establecimiento Educativo Oficial? 

    $("#G1875_C34645").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Tiene nombramiento en propiedad o provisional? 

    $("#G1875_C34646").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPIFICACION 

    $("#G1875_C34501").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '1930' , idPadre : $(this).val() },
            success : function(data){
                var optG1875_C34502 = $("#G1875_C34502").attr("opt");
                $("#G1875_C34502").html(data);
                if (optG1875_C34502 != null) {
                    $("#G1875_C34502").val(optG1875_C34502).trigger("change");
                }
            }
        });
        
    });

    //function para SUBTIPIFICACION 

    $("#G1875_C34502").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para CONSULTOR 

    $("#G1875_C34496").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1875_C34468").val(item.G1875_C34468);
 
                    $("#G1875_C34469").val(item.G1875_C34469).trigger("change"); 
 
                                                $("#G1875_C34470").val(item.G1875_C34470);
 
                    $("#G1875_C34457").val(item.G1875_C34457).trigger("change"); 
 
                    $("#G1875_C34458").val(item.G1875_C34458).trigger("change"); 
 
                                                $("#G1875_C34459").val(item.G1875_C34459);
 
                                                $("#G1875_C34460").val(item.G1875_C34460);
 
                                                $("#G1875_C34461").val(item.G1875_C34461);
 
                                                $("#G1875_C34462").val(item.G1875_C34462);
 
                                                $("#G1875_C34463").val(item.G1875_C34463);
 
                                                $("#G1875_C34464").val(item.G1875_C34464);
 
                                                $("#G1875_C34465").val(item.G1875_C34465);
 
                                                $("#G1875_C34466").val(item.G1875_C34466);
 
                                                $("#G1875_C34467").val(item.G1875_C34467);
 
                                                $("#G1875_C34471").val(item.G1875_C34471);
 
                                                $("#G1875_C34472").val(item.G1875_C34472);
 
                                                $("#G1875_C34473").val(item.G1875_C34473);
 
                                                $("#G1875_C34474").val(item.G1875_C34474);
 
                                                $("#G1875_C34475").val(item.G1875_C34475);
 
                                                $("#G1875_C34639").val(item.G1875_C34639);
 
                                                $("#G1875_C34640").val(item.G1875_C34640);
 
                                                $("#G1875_C34641").val(item.G1875_C34641);
 
                                                $("#G1875_C34642").val(item.G1875_C34642);
 
                    $("#G1875_C34476").val(item.G1875_C34476).trigger("change"); 
 
                                                $("#G1875_C34643").val(item.G1875_C34643);
 
                                                $("#G1875_C34477").val(item.G1875_C34477);
 
                                                $("#G1875_C34480").val(item.G1875_C34480);
 
                                                $("#G1875_C34478").val(item.G1875_C34478);
 
                                                $("#G1875_C34479").val(item.G1875_C34479);
 
                                                $("#G1875_C34481").val(item.G1875_C34481);
 
                                                $("#G1875_C34486").val(item.G1875_C34486);
 
                                                $("#G1875_C34482").val(item.G1875_C34482);
 
                                                $("#G1875_C34485").val(item.G1875_C34485);
 
                                                $("#G1875_C34483").val(item.G1875_C34483);
 
                                                $("#G1875_C34484").val(item.G1875_C34484);
 
                                                $("#G1875_C34487").val(item.G1875_C34487);
 
                                                $("#G1875_C34488").val(item.G1875_C34488);
 
                                                $("#G1875_C34489").val(item.G1875_C34489);
 
                    $("#G1875_C34645").val(item.G1875_C34645).trigger("change"); 
 
                    $("#G1875_C34646").val(item.G1875_C34646).trigger("change"); 
 
                                                $("#G1875_C34490").val(item.G1875_C34490);
 
                                                $("#G1875_C34491").val(item.G1875_C34491);
 
                                                $("#G1875_C34492").val(item.G1875_C34492);
 
                                                $("#G1875_C34493").val(item.G1875_C34493);
 
                                                $("#G1875_C34636").val(item.G1875_C34636);
 
                                                $("#G1875_C34637").val(item.G1875_C34637);
 
                                                $("#G1875_C34638").val(item.G1875_C34638);
 
                    $("#G1875_C34496").val(item.G1875_C34496).trigger("change"); 
 
                                                $("#G1875_C34494").val(item.G1875_C34494);
 
                                                $("#G1875_C34495").val(item.G1875_C34495);
 
                    $("#G1875_C34501").val(item.G1875_C34501).trigger("change"); 
 
                    $("#G1875_C34502").attr("opt",item.G1875_C34502); 
 
                                                $("#G1875_C34570").val(item.G1875_C34570);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Nombres y Apellidos','¿Qué Persona se Está Comunicando?','Número de contacto','Agente','Fecha','Hora','Campaña','Número de Identificación del estudiante','¿En qué grado se encuentra el estudiante?','Nombres y Apellidos del Estudiante que Representa','¿En qué colegio se encuentra el estudiante?','Número Celular de familiar','Teléfono Adicional de familiar','Correo Electrónico de familiar','Edad del estudiante','Ciudad','Localidad','Barrio','Tiene autorización o está acompañado de un adulto ','Edad','Colegio Donde Estudia','Grado Que Está Cursando','Ciudad del Estudiante','Barrio del Estudiante','Localidad.','Barrio.','Colegio donde trabaja.','Ciudad.','Nivel que está enseñando este año.','Área que está enseñando este año.','Número Celular.','Teléfono Adicional.','Correo Electrónico.','¿Es Docente de un Establecimiento Educativo Oficial?','¿Tiene nombramiento en propiedad o provisional?','Número de Identificación de Otro','Correo Electrónico de Otro','Número Celular de Otro','Teléfono Adicional de Otro','Ciudad de Otro','Localidad de Otro','Barrio de Otro','CONSULTOR','FECHA DE CITA','HORA DE LA CITA','TIPIFICACION','SUBTIPIFICACION','OBSERVACIONES AGENTE'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1875_C34468', 
                        index: 'G1875_C34468', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34469', 
                        index:'G1875_C34469', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1924&campo=G1875_C34469'
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34470', 
                        index: 'G1875_C34470', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34462', 
                        index: 'G1875_C34462', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34463', 
                        index: 'G1875_C34463', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34464', 
                        index: 'G1875_C34464', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34465', 
                        index: 'G1875_C34465', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34466', 
                        index: 'G1875_C34466', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34467', 
                        index: 'G1875_C34467', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34471', 
                        index: 'G1875_C34471', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34472', 
                        index: 'G1875_C34472', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34473', 
                        index: 'G1875_C34473', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34474', 
                        index: 'G1875_C34474', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34475', 
                        index: 'G1875_C34475', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34639', 
                        index: 'G1875_C34639', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34640', 
                        index: 'G1875_C34640', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34641', 
                        index: 'G1875_C34641', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34642', 
                        index: 'G1875_C34642', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34476', 
                        index:'G1875_C34476', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1925&campo=G1875_C34476'
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34643', 
                        index: 'G1875_C34643', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34477', 
                        index: 'G1875_C34477', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34480', 
                        index: 'G1875_C34480', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34478', 
                        index: 'G1875_C34478', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34479', 
                        index: 'G1875_C34479', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34481', 
                        index: 'G1875_C34481', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34486', 
                        index: 'G1875_C34486', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34482', 
                        index: 'G1875_C34482', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34485', 
                        index: 'G1875_C34485', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34483', 
                        index: 'G1875_C34483', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34484', 
                        index: 'G1875_C34484', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34487', 
                        index: 'G1875_C34487', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34488', 
                        index: 'G1875_C34488', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34489', 
                        index: 'G1875_C34489', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34645', 
                        index:'G1875_C34645', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1925&campo=G1875_C34645'
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34646', 
                        index:'G1875_C34646', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1947&campo=G1875_C34646'
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34490', 
                        index: 'G1875_C34490', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34491', 
                        index: 'G1875_C34491', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34492', 
                        index: 'G1875_C34492', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34493', 
                        index: 'G1875_C34493', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34636', 
                        index: 'G1875_C34636', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34637', 
                        index: 'G1875_C34637', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34638', 
                        index: 'G1875_C34638', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1875_C34496', 
                        index:'G1875_C34496', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1927&campo=G1875_C34496'
                        }
                    }

                    ,
                    {  
                        name:'G1875_C34494', 
                        index:'G1875_C34494', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G1875_C34495', 
                        index:'G1875_C34495', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA DE LA CITA', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34501', 
                        index:'G1875_C34501', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1929&campo=G1875_C34501'
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34502', 
                        index:'G1875_C34502', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1930&campo=G1875_C34502'
                        }
                    }

                    ,
                    { 
                        name:'G1875_C34570', 
                        index:'G1875_C34570', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1875_C34468',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
                ,subGrid: true,
                subGridRowExpanded: function(subgrid_id, row_id) { 
                    // we pass two parameters 
                    // subgrid_id is a id of the div tag created whitin a table data 
                    // the id of this elemenet is a combination of the "sg_" + id of the row 
                    // the row_id is the id of the row 
                    // If we wan to pass additinal parameters to the url we can use 
                    // a method getRowData(row_id) - which returns associative array in type name-value 
                    // here we can easy construct the flowing 
                    $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','CONSULTOR','FECHA DE CITA','HORA DE CITA','ESTUDIANTE', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            {  
                                name:'G1874_C34453', 
                                index:'G1874_C34453', 
                                width:120 ,
                                editable: true, 
                                edittype:"select" , 
                                editoptions: {
                                    dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1927&campo=G1874_C34453'
                                }
                            }

                            ,
                            {  
                                name:'G1874_C34454', 
                                index:'G1874_C34454', 
                                width:120 ,
                                editable: true ,
                                formatter: 'text', 
                                searchoptions: {
                                    sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                                }, 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        $(el).datepicker({
                                            language: "es",
                                            autoclose: true,
                                            todayHighlight: true
                                        });
                                    },
                                    defaultValue: function(){
                                        var currentTime = new Date();
                                        var month = parseInt(currentTime.getMonth() + 1);
                                        month = month <= 9 ? "0"+month : month;
                                        var day = currentTime.getDate();
                                        day = day <= 9 ? "0"+day : day;
                                        var year = currentTime.getFullYear();
                                        return year+"-"+month + "-"+day;
                                    }
                                }
                            }

                            ,
                            {  
                                name:'G1874_C34455', 
                                index:'G1874_C34455', 
                                width:70 ,
                                editable: true ,
                                formatter: 'text', 
                                editoptions:{
                                    size:20,
                                    dataInit:function(el){
                                        //Timepicker
                                         var options = {  //hh:mm 24 hour format only, defaults to current time
                                            timeFormat: 'HH:mm:ss',
                                            interval: 5,
                                            minTime: '10',
                                            dynamic: false,
                                            dropdown: true,
                                            scrollbar: true
                                        }; 
                                        $(el).timepicker(options);


                                    }
                                }
                            }

                            ,
                            { 
                                name:'G1874_C34456', 
                                index: 'G1874_C34456', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G1875_C34468").val(item.G1875_C34468);
 
                    $("#G1875_C34469").val(item.G1875_C34469).trigger("change"); 

                        $("#G1875_C34470").val(item.G1875_C34470);
 
                    $("#G1875_C34457").val(item.G1875_C34457).trigger("change"); 
 
                    $("#G1875_C34458").val(item.G1875_C34458).trigger("change"); 

                        $("#G1875_C34459").val(item.G1875_C34459);

                        $("#G1875_C34460").val(item.G1875_C34460);

                        $("#G1875_C34461").val(item.G1875_C34461);

                        $("#G1875_C34462").val(item.G1875_C34462);

                        $("#G1875_C34463").val(item.G1875_C34463);

                        $("#G1875_C34464").val(item.G1875_C34464);

                        $("#G1875_C34465").val(item.G1875_C34465);

                        $("#G1875_C34466").val(item.G1875_C34466);

                        $("#G1875_C34467").val(item.G1875_C34467);

                        $("#G1875_C34471").val(item.G1875_C34471);

                        $("#G1875_C34472").val(item.G1875_C34472);

                        $("#G1875_C34473").val(item.G1875_C34473);

                        $("#G1875_C34474").val(item.G1875_C34474);

                        $("#G1875_C34475").val(item.G1875_C34475);

                        $("#G1875_C34639").val(item.G1875_C34639);

                        $("#G1875_C34640").val(item.G1875_C34640);

                        $("#G1875_C34641").val(item.G1875_C34641);

                        $("#G1875_C34642").val(item.G1875_C34642);
 
                    $("#G1875_C34476").val(item.G1875_C34476).trigger("change"); 

                        $("#G1875_C34643").val(item.G1875_C34643);

                        $("#G1875_C34477").val(item.G1875_C34477);

                        $("#G1875_C34480").val(item.G1875_C34480);

                        $("#G1875_C34478").val(item.G1875_C34478);

                        $("#G1875_C34479").val(item.G1875_C34479);

                        $("#G1875_C34481").val(item.G1875_C34481);

                        $("#G1875_C34486").val(item.G1875_C34486);

                        $("#G1875_C34482").val(item.G1875_C34482);

                        $("#G1875_C34485").val(item.G1875_C34485);

                        $("#G1875_C34483").val(item.G1875_C34483);

                        $("#G1875_C34484").val(item.G1875_C34484);

                        $("#G1875_C34487").val(item.G1875_C34487);

                        $("#G1875_C34488").val(item.G1875_C34488);

                        $("#G1875_C34489").val(item.G1875_C34489);
 
                    $("#G1875_C34645").val(item.G1875_C34645).trigger("change"); 
 
                    $("#G1875_C34646").val(item.G1875_C34646).trigger("change"); 

                        $("#G1875_C34490").val(item.G1875_C34490);

                        $("#G1875_C34491").val(item.G1875_C34491);

                        $("#G1875_C34492").val(item.G1875_C34492);

                        $("#G1875_C34493").val(item.G1875_C34493);

                        $("#G1875_C34636").val(item.G1875_C34636);

                        $("#G1875_C34637").val(item.G1875_C34637);

                        $("#G1875_C34638").val(item.G1875_C34638);
 
                    $("#G1875_C34496").val(item.G1875_C34496).trigger("change"); 

                        $("#G1875_C34494").val(item.G1875_C34494);

                        $("#G1875_C34495").val(item.G1875_C34495);
 
                    $("#G1875_C34501").val(item.G1875_C34501).trigger("change"); 
 
                    $("#G1875_C34502").attr("opt",item.G1875_C34502); 

                        $("#G1875_C34570").val(item.G1875_C34570);
                        
            cargarHijos_0(
        $("#G1875_C34496").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','CONSULTOR','FECHA DE CITA','HORA DE CITA','ESTUDIANTE', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    {  
                        name:'G1874_C34453', 
                        index:'G1874_C34453', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1927&campo=G1874_C34453'
                        }
                    }

                    ,
                    {  
                        name:'G1874_C34454', 
                        index:'G1874_C34454', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G1874_C34455', 
                        index:'G1874_C34455', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = {  //hh:mm 24 hour format only, defaults to current time
                                    timeFormat: 'HH:mm:ss',
                                    interval: 5,
                                    minTime: '10',
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                }; 
                                $(el).timepicker(options);
                                $(".timepicker").css("z-index", 99999 );
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1874_C34456', 
                        index: 'G1874_C34456', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1874_C34453',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'DISPONIBILIDAD DE CITAS',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1874&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=34453&formularioPadre=1875<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G1875_C34496").val());
            var id_0 = $("#G1875_C34496").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G1875_C34496").val());
            var id_0 = $("#G1875_C34496").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
