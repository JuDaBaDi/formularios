<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {


        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1136 WHERE G1136_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }


      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1136_ConsInte__b, G1136_FechaInsercion , USUARI_Nombre____b , G1136_CodigoMiembro  , G1136_PoblacionOrigen , G1136_EstadoDiligenciamiento ,  G1136_IdLlamada , G1136_C17513 as principal ,G1136_C20664,G1136_CodigoMiembro,G1136_C17513,G1136_C17514,G1136_C20334,G1136_C20665,G1136_C20666,G1136_C17515,G1136_C17516,G1136_C17517,G1136_C17518,G1136_C17519,G1136_C17520,G1136_C17521,G1136_C17522,G1136_C17523,G1136_C17524,G1136_C18891,G1136_C18892,G1136_C18893,G1136_C20953,G1136_C18894,G1136_C22655,G1136_C17525,G1136_C17526,G1136_C17527,G1136_C17528,G1136_C17529,G1136_C17530,G1136_C17531,G1136_C17532,G1136_C17533,G1136_C17534,G1136_C17535,G1136_C17537,G1136_C17536,G1136_C17539,G1136_C17538,G1136_C17540,G1136_C18644,G1136_C20948,G1136_C20949,G1136_C20950,G1136_C20951,G1136_C20952,G1136_C18895,G1136_C18896,G1136_C18897,G1136_C18898,G1136_C18899,G1136_C18900 FROM '.$BaseDatos.'.G1136 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1136_Usuario  WHERE G1136_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1136_C20664'] = $key->G1136_C20664;

                $datos[$i]['G1136_CodigoMiembro'] = $key->G1136_CodigoMiembro;

                $datos[$i]['G1136_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1136_C17513'] = $key->G1136_C17513;

                $datos[$i]['G1136_C17514'] = $key->G1136_C17514;

                $datos[$i]['G1136_C20334'] = $key->G1136_C20334;

                $datos[$i]['G1136_C20665'] = $key->G1136_C20665;

                $datos[$i]['G1136_C20666'] = $key->G1136_C20666;

                $datos[$i]['G1136_C17515'] = $key->G1136_C17515;

                $datos[$i]['G1136_C17516'] = $key->G1136_C17516;

                $datos[$i]['G1136_C17517'] = $key->G1136_C17517;

                $datos[$i]['G1136_C17518'] = $key->G1136_C17518;

                $datos[$i]['G1136_C17519'] = $key->G1136_C17519;

                $datos[$i]['G1136_C17520'] = $key->G1136_C17520;

                $datos[$i]['G1136_C17521'] = $key->G1136_C17521;

                $datos[$i]['G1136_C17522'] = $key->G1136_C17522;

                $datos[$i]['G1136_C17523'] = $key->G1136_C17523;

                $datos[$i]['G1136_C17524'] = $key->G1136_C17524;

                $datos[$i]['G1136_C18891'] = $key->G1136_C18891;

                $datos[$i]['G1136_C18892'] = $key->G1136_C18892;

                $datos[$i]['G1136_C18893'] = explode(' ', $key->G1136_C18893)[0];

                $datos[$i]['G1136_C20953'] = $key->G1136_C20953;

                $datos[$i]['G1136_C18894'] = explode(' ', $key->G1136_C18894)[0];

                // $datos[$i]['G1136_C22654'] = $key->G1136_C22654;

                $datos[$i]['G1136_C22655'] = $key->G1136_C22655;

                $datos[$i]['G1136_C17525'] = $key->G1136_C17525;

                $datos[$i]['G1136_C17526'] = $key->G1136_C17526;

                $datos[$i]['G1136_C17527'] = $key->G1136_C17527;

                $datos[$i]['G1136_C17528'] = $key->G1136_C17528;

                $datos[$i]['G1136_C17529'] = explode(' ', $key->G1136_C17529)[0];
  
                $hora = '';
                if(!is_null($key->G1136_C17530)){
                    $hora = explode(' ', $key->G1136_C17530)[1];
                }

                $datos[$i]['G1136_C17531'] = $key->G1136_C17531;

                $datos[$i]['G1136_C17532'] = $key->G1136_C17532;

                $datos[$i]['G1136_C17533'] = $key->G1136_C17533;

                $datos[$i]['G1136_C17534'] = $key->G1136_C17534;

                $datos[$i]['G1136_C17535'] = $key->G1136_C17535;

                $datos[$i]['G1136_C17537'] = $key->G1136_C17537;

                $datos[$i]['G1136_C17536'] = $key->G1136_C17536;

                $datos[$i]['G1136_C17539'] = $key->G1136_C17539;

                $datos[$i]['G1136_C17538'] = $key->G1136_C17538;

                $datos[$i]['G1136_C17540'] = $key->G1136_C17540;

                $datos[$i]['G1136_C18644'] = $key->G1136_C18644;

                $datos[$i]['G1136_C20948'] = $key->G1136_C20948;

                $datos[$i]['G1136_C20949'] = $key->G1136_C20949;

                $datos[$i]['G1136_C20950'] = $key->G1136_C20950;

                $datos[$i]['G1136_C20951'] = explode(' ', $key->G1136_C20951)[0];
  
                $hora = '';
                if(!is_null($key->G1136_C20952)){
                    $hora = explode(' ', $key->G1136_C20952)[1];
                }

                $datos[$i]['G1136_C18895'] = $key->G1136_C18895;

                $datos[$i]['G1136_C18896'] = $key->G1136_C18896;

                $datos[$i]['G1136_C18897'] = $key->G1136_C18897;

                $datos[$i]['G1136_C18898'] = $key->G1136_C18898;

                $datos[$i]['G1136_C18899'] = $key->G1136_C18899;

                $datos[$i]['G1136_C18900'] = $key->G1136_C18900;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1136_ConsInte__b as id,  G1136_C17513 as camp1 , G1136_C17515 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1136 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1136_C17526  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1136_C17513 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1136_C17515 like '%".$_POST['Busqueda']."%' ) ";
            }

            if (!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != '') {
                $Lsql .= " AND G1136_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1136_C20664']) && $_POST['G1136_C20664'] != ''){
				$Lsql .= " AND G1136_C20664 LIKE '%". $_POST['G1136_C20664'] ."%' ";
			}
			if(!is_null($_POST['G1136_C17513']) && $_POST['G1136_C17513'] != ''){
				$Lsql .= " AND G1136_C17513 LIKE '%". $_POST['G1136_C17513'] ."%' ";
			}
			if(!is_null($_POST['G1136_C17514']) && $_POST['G1136_C17514'] != ''){
				$Lsql .= " AND G1136_C17514 LIKE '%". $_POST['G1136_C17514'] ."%' ";
			}
			if(!is_null($_POST['G1136_C17515']) && $_POST['G1136_C17515'] != ''){
				$Lsql .= " AND G1136_C17515 LIKE '%". $_POST['G1136_C17515'] ."%' ";
			}

            $Lsql .= " ORDER BY FIELD(G1136_C17526,14280,14281,14282,14283,14284), G1136_FechaInsercion DESC  LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

                $color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }else if($key->estado == 'Sin gestión'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = '4';
                }    


                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1136_C18897'])){
            echo '<select class="form-control input-sm"  name="G1136_C18897" id="G1136_C18897">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1136_C18897'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1136_C18897'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1136_C18897'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1136");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1136_ConsInte__b, G1136_FechaInsercion , G1136_Usuario ,  G1136_CodigoMiembro  , G1136_PoblacionOrigen , G1136_EstadoDiligenciamiento ,  G1136_IdLlamada , G1136_C17513 as principal ,G1136_C20664,G1136_C17513,G1136_C17514,G1136_C20334,G1136_C20665,G1136_C20666,G1136_C17515,G1136_C17516,G1136_C17517,G1136_C17518,G1136_C17519,G1136_C17520,G1136_C17521,G1136_C17522,G1136_C17523,G1136_C17524,G1136_C18891, a.LISOPC_Nombre____b as G1136_C18892,G1136_C18893,G1136_C20953,G1136_C18894,G1136_C22655, b.LISOPC_Nombre____b as G1136_C17525, c.LISOPC_Nombre____b as G1136_C17526,G1136_C17527,G1136_C17528,G1136_C17529,G1136_C17530,G1136_C17531,G1136_C17532,G1136_C17533,G1136_C17534,G1136_C17535,G1136_C17537,G1136_C17536,G1136_C17539,G1136_C17538,G1136_C17540, d.LISOPC_Nombre____b as G1136_C18644, e.LISOPC_Nombre____b as G1136_C20948, f.LISOPC_Nombre____b as G1136_C20949, g.LISOPC_Nombre____b as G1136_C20950,G1136_C20951,G1136_C20952, h.LISOPC_Nombre____b as G1136_C18895, i.LISOPC_Nombre____b as G1136_C18896,G1136_C18898, j.LISOPC_Nombre____b as G1136_C18899,G1136_C18900 FROM '.$BaseDatos.'.G1136 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1136_C18892 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1136_C17525 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1136_C17526 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1136_C18644 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1136_C20948 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1136_C20949 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1136_C20950 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1136_C18895 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1136_C18896 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1136_C18897 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1136_C18899';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1136_C17530)){
                    $hora_a = explode(' ', $fila->G1136_C17530)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1136_C20952)){
                    $hora_b = explode(' ', $fila->G1136_C20952)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1136_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1136_ConsInte__b , ($fila->G1136_C20664) , ($fila->G1136_C17513) , ($fila->G1136_C17514) , ($fila->G1136_C20334) , ($fila->G1136_C20665) , ($fila->G1136_C20666) , ($fila->G1136_C17515) , ($fila->G1136_C17516) , ($fila->G1136_C17517) , ($fila->G1136_C17518) , ($fila->G1136_C17519) , ($fila->G1136_C17520) , ($fila->G1136_C17521) , ($fila->G1136_C17522) , ($fila->G1136_C17523) , ($fila->G1136_C17524) , ($fila->G1136_C18891) , ($fila->G1136_C18892) , explode(' ', $fila->G1136_C18893)[0] , ($fila->G1136_C20953) , explode(' ', $fila->G1136_C18894)[0]  , ($fila->G1136_C22655) , ($fila->G1136_C17525) , ($fila->G1136_C17526) , ($fila->G1136_C17527) , ($fila->G1136_C17528) , explode(' ', $fila->G1136_C17529)[0] , $hora_a , ($fila->G1136_C17531) , ($fila->G1136_C17532) , ($fila->G1136_C17533) , ($fila->G1136_C17534) , ($fila->G1136_C17535) , ($fila->G1136_C17537) , ($fila->G1136_C17536) , ($fila->G1136_C17539) , ($fila->G1136_C17538) , ($fila->G1136_C17540) , ($fila->G1136_C18644) , ($fila->G1136_C20948) , ($fila->G1136_C20949) , ($fila->G1136_C20950) , explode(' ', $fila->G1136_C20951)[0] , $hora_b , ($fila->G1136_C18895) , ($fila->G1136_C18896) , ($fila->G1136_C18898) , ($fila->G1136_C18899) , ($fila->G1136_C18900) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1136 WHERE G1136_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1136_ConsInte__b as id,  G1136_C17513 as camp1 , G1136_C17515 as camp2, G1136_C17526 as estado FROM '.$BaseDatos.'.G1136 ORDER BY FIELD(G1136_C17526,14280,14281,14282,14283,14284), G1136_FechaInsercion DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){

                $color = 'color:green;';
                $strIconoBackOffice = 'Cerrada';
                if($obj->estado == '14281' || $obj->estado == '14282'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14284'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }
                else if($obj->estado == '14280'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1136 SET "; 
  
            if(isset($_POST["G1136_C20664"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20664 = '".$_POST["G1136_C20664"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17513"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17513 = '".$_POST["G1136_C17513"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17514"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17514 = '".$_POST["G1136_C17514"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C20334"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20334 = '".$_POST["G1136_C20334"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C20665"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20665 = '".$_POST["G1136_C20665"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C20666"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20666 = '".$_POST["G1136_C20666"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17515"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17515 = '".$_POST["G1136_C17515"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17516"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17516 = '".$_POST["G1136_C17516"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17517"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17517 = '".$_POST["G1136_C17517"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17518"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17518 = '".$_POST["G1136_C17518"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17519"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17519 = '".$_POST["G1136_C17519"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17520"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17520 = '".$_POST["G1136_C17520"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17521"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17521 = '".$_POST["G1136_C17521"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17522"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17522 = '".$_POST["G1136_C17522"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17523"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17523 = '".$_POST["G1136_C17523"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17524"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17524 = '".$_POST["G1136_C17524"]."'";
                $validar = 1;
            }
             
  
            $G1136_C18891 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C18891"])){
                if($_POST["G1136_C18891"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18891 = $_POST["G1136_C18891"];
                    $LsqlU .= $separador." G1136_C18891 = ".$G1136_C18891."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1136_C18892"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18892 = '".$_POST["G1136_C18892"]."'";
                $validar = 1;
            }
             
 
            $G1136_C18893 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1136_C18893"])){    
                if($_POST["G1136_C18893"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1136_C18893"]);
                    if(count($tieneHora) > 1){
                    	$G1136_C18893 = "'".$_POST["G1136_C18893"]."'";
                    }else{
                    	$G1136_C18893 = "'".str_replace(' ', '',$_POST["G1136_C18893"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1136_C18893 = ".$G1136_C18893;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1136_C20953"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20953 = '".$_POST["G1136_C20953"]."'";
                $validar = 1;
            }
             
 
            $G1136_C18894 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1136_C18894"])){    
                if($_POST["G1136_C18894"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1136_C18894"]);
                    if(count($tieneHora) > 1){
                    	$G1136_C18894 = "'".$_POST["G1136_C18894"]."'";
                    }else{
                    	$G1136_C18894 = "'".str_replace(' ', '',$_POST["G1136_C18894"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1136_C18894 = ".$G1136_C18894;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1136_C22654"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C22654 = '".$_POST["G1136_C22654"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C22655"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C22655 = '".$_POST["G1136_C22655"]."'";
                $validar = 1;
            }
             
 
            $G1136_C17525 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1136_C17525 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1136_C17525 = ".$G1136_C17525;
                    $validar = 1;

                    
                }
            }
 
            $G1136_C17526 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1136_C17526 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1136_C17526 = ".$G1136_C17526;
                    $validar = 1;
                }
            }
  
            $G1136_C17527 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17527"])){
                if($_POST["G1136_C17527"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17527 = $_POST["G1136_C17527"];
                    $LsqlU .= $separador." G1136_C17527 = ".$G1136_C17527."";
                    $validar = 1;
                }
            }
  
            $G1136_C17528 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17528"])){
                if($_POST["G1136_C17528"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17528 = $_POST["G1136_C17528"];
                    $LsqlU .= $separador." G1136_C17528 = ".$G1136_C17528."";
                    $validar = 1;
                }
            }
 
            $G1136_C17531 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1136_C17531 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1136_C17531 = ".$G1136_C17531;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1136_C17532"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17532 = '".$_POST["G1136_C17532"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17533"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17533 = '".$_POST["G1136_C17533"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17534"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17534 = '".$_POST["G1136_C17534"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C17535"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17535 = '".$_POST["G1136_C17535"]."'";
                $validar = 1;
            }
             
  
            $G1136_C17537 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17537"])){
                if($_POST["G1136_C17537"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17537 = $_POST["G1136_C17537"];
                    $LsqlU .= $separador." G1136_C17537 = ".$G1136_C17537."";
                    $validar = 1;
                }
            }
  
            $G1136_C17536 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1136_C17536"])){
                if($_POST["G1136_C17536"] == 'Yes'){
                    $G1136_C17536 = 1;
                }else if($_POST["G1136_C17536"] == 'off'){
                    $G1136_C17536 = 0;
                }else if($_POST["G1136_C17536"] == 'on'){
                    $G1136_C17536 = 1;
                }else if($_POST["G1136_C17536"] == 'No'){
                    $G1136_C17536 = 1;
                }else{
                    $G1136_C17536 = $_POST["G1136_C17536"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1136_C17536 = ".$G1136_C17536."";

                $validar = 1;
            }
  
            $G1136_C17539 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C17539"])){
                if($_POST["G1136_C17539"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C17539 = $_POST["G1136_C17539"];
                    $LsqlU .= $separador." G1136_C17539 = ".$G1136_C17539."";
                    $validar = 1;
                }
            }
  
            $G1136_C17538 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1136_C17538"])){
                if($_POST["G1136_C17538"] == 'Yes'){
                    $G1136_C17538 = 1;
                }else if($_POST["G1136_C17538"] == 'off'){
                    $G1136_C17538 = 0;
                }else if($_POST["G1136_C17538"] == 'on'){
                    $G1136_C17538 = 1;
                }else if($_POST["G1136_C17538"] == 'No'){
                    $G1136_C17538 = 1;
                }else{
                    $G1136_C17538 = $_POST["G1136_C17538"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1136_C17538 = ".$G1136_C17538."";

                $validar = 1;
            }
  
            if(isset($_POST["G1136_C17540"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C17540 = '".$_POST["G1136_C17540"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C18644"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18644 = '".$_POST["G1136_C18644"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C20948"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20948 = '".$_POST["G1136_C20948"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C20949"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20949 = '".$_POST["G1136_C20949"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C20950"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C20950 = '".$_POST["G1136_C20950"]."'";
                $validar = 1;
            }
             
 
            // $G1136_C20951 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no
            // if(isset($_POST["G1136_C20951"])){    
            //     if($_POST["G1136_C20951"] != ''){
            //         $separador = "";
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $tieneHora = explode(' ' , $_POST["G1136_C20951"]);
            //         if(count($tieneHora) > 1){
            //         	$G1136_C20951 = "'".$_POST["G1136_C20951"]."'";
            //         }else{
            //         	$G1136_C20951 = "'".str_replace(' ', '',$_POST["G1136_C20951"])." 00:00:00'";
            //         }


            //         $LsqlU .= $separador." G1136_C20951 = ".$G1136_C20951;
            //         $validar = 1;
            //     }
            // }
  
            // $G1136_C20952 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1136_C20952"])){   
            //     if($_POST["G1136_C20952"] != '' && $_POST["G1136_C20952"] != 'undefined' && $_POST["G1136_C20952"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1136_C20952 = "'".$fecha." ".str_replace(' ', '',$_POST["G1136_C20952"])."'";
            //         $LsqlU .= $separador." G1136_C20952 = ".$G1136_C20952."";
            //         $validar = 1;
            //     }
            // }
  
            if(isset($_POST["G1136_C18895"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18895 = '".$_POST["G1136_C18895"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C18896"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18896 = '".$_POST["G1136_C18896"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1136_C18897"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18897 = '".$_POST["G1136_C18897"]."'";
                $validar = 1;
            }
             
  
            $G1136_C18898 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C18898"])){
                if($_POST["G1136_C18898"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18898 = $_POST["G1136_C18898"];
                    $LsqlU .= $separador." G1136_C18898 = ".$G1136_C18898."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1136_C18899"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_C18899 = '".$_POST["G1136_C18899"]."'";
                $validar = 1;
            }
             
  
            $G1136_C18900 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1136_C18900"])){
                if($_POST["G1136_C18900"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1136_C18900 = $_POST["G1136_C18900"];
                    $LsqlU .= $separador." G1136_C18900 = ".$G1136_C18900."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1136_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
             if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1136_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }
            
			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1136_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            // echo $Lsql; die();
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
