<?php

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
        
    //Inserciones o actualizaciones

    if(isset($_POST['getListaHija'])){
        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }
        
    if(isset($_POST["oper"])){
            $str_Lsql  = '';

            $validar = 0;
            $str_LsqlU = "UPDATE ".$BaseDatos.".G1136 SET "; 
            $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1136( G1136_FechaInsercion ,";
            $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G1136_C22654"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C22654 = '".$_POST["G1136_C22654"]."'";
            $str_LsqlI .= $separador."G1136_C22654";
            $str_LsqlV .= $separador."'".$_POST["G1136_C22654"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1136_C17525"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C17525 = '".$_POST["G1136_C17525"]."'";
            $str_LsqlI .= $separador."G1136_C17525";
            $str_LsqlV .= $separador."'".$_POST["G1136_C17525"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1136_C17526"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C17526 = '".$_POST["G1136_C17526"]."'";
            $str_LsqlI .= $separador."G1136_C17526";
            $str_LsqlV .= $separador."'".$_POST["G1136_C17526"]."'";
            $validar = 1;
        }
         
  
        $G1136_C17527 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1136_C17527"])){
            if($_POST["G1136_C17527"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1136_C17527 = $_POST["G1136_C17527"];
                $G1136_C17527 = str_replace(".", "", $_POST["G1136_C17527"]);
                $G1136_C17527 =  str_replace(",", ".", $G1136_C17527);
                $str_LsqlU .= $separador." G1136_C17527 = '".$G1136_C17527."'";
                $str_LsqlI .= $separador." G1136_C17527";
                $str_LsqlV .= $separador."'".$G1136_C17527."'";
                $validar = 1;
            }
        }
  
        $G1136_C17528 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1136_C17528"])){
            if($_POST["G1136_C17528"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1136_C17528 = $_POST["G1136_C17528"];
                $G1136_C17528 = str_replace(".", "", $_POST["G1136_C17528"]);
                $G1136_C17528 =  str_replace(",", ".", $G1136_C17528);
                $str_LsqlU .= $separador." G1136_C17528 = '".$G1136_C17528."'";
                $str_LsqlI .= $separador." G1136_C17528";
                $str_LsqlV .= $separador."'".$G1136_C17528."'";
                $validar = 1;
            }
        }
 
        $G1136_C17529 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1136_C17529"])){    
            if($_POST["G1136_C17529"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1136_C17529 = "'".str_replace(' ', '',$_POST["G1136_C17529"])." 00:00:00'";
                $str_LsqlU .= $separador." G1136_C17529 = ".$G1136_C17529;
                $str_LsqlI .= $separador." G1136_C17529";
                $str_LsqlV .= $separador.$G1136_C17529;
                $validar = 1;
            }
        }
  
        $G1136_C17530 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1136_C17530"])){   
            if($_POST["G1136_C17530"] != '' && $_POST["G1136_C17530"] != 'undefined' && $_POST["G1136_C17530"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1136_C17530 = "'".$fecha." ".str_replace(' ', '',$_POST["G1136_C17530"])."'";
                $str_LsqlU .= $separador." G1136_C17530 = ".$G1136_C17530."";
                $str_LsqlI .= $separador." G1136_C17530";
                $str_LsqlV .= $separador.$G1136_C17530;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1136_C17531"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C17531 = '".$_POST["G1136_C17531"]."'";
            $str_LsqlI .= $separador."G1136_C17531";
            $str_LsqlV .= $separador."'".$_POST["G1136_C17531"]."'";
            $validar = 1;
        }
         
  
        $G1136_C17537 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1136_C17537"])){
            if($_POST["G1136_C17537"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1136_C17537 = str_replace(".", "", $_POST["G1136_C17537"]);
                $G1136_C17537 =  str_replace(",", ".", $G1136_C17537);
                $str_LsqlU .= $separador." G1136_C17537 = '".$G1136_C17537."'";
                $str_LsqlI .= $separador." G1136_C17537";
                $str_LsqlV .= $separador."'".$G1136_C17537."'";
                $validar = 1;
            }
        }
  
        $G1136_C17536 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G1136_C17536"])){
            if($_POST["G1136_C17536"] == 'Yes'){
                $G1136_C17536 = 1;
            }else if($_POST["G1136_C17536"] == 'off'){
                $G1136_C17536 = 0;
            }else if($_POST["G1136_C17536"] == 'on'){
                $G1136_C17536 = 1;
            }else if($_POST["G1136_C17536"] == 'No'){
                $G1136_C17536 = 0;
            }else{
                $G1136_C17536 = $_POST["G1136_C17536"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G1136_C17536 = ".$G1136_C17536."";
            $str_LsqlI .= $separador." G1136_C17536";
            $str_LsqlV .= $separador.$G1136_C17536;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G1136_C17536 = ".$G1136_C17536."";
            $str_LsqlI .= $separador." G1136_C17536";
            $str_LsqlV .= $separador.$G1136_C17536;

            $validar = 1;
        }
  
        $G1136_C17539 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1136_C17539"])){
            if($_POST["G1136_C17539"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1136_C17539 = str_replace(".", "", $_POST["G1136_C17539"]);
                $G1136_C17539 =  str_replace(",", ".", $G1136_C17539);
                $str_LsqlU .= $separador." G1136_C17539 = '".$G1136_C17539."'";
                $str_LsqlI .= $separador." G1136_C17539";
                $str_LsqlV .= $separador."'".$G1136_C17539."'";
                $validar = 1;
            }
        }
  
        $G1136_C17538 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G1136_C17538"])){
            if($_POST["G1136_C17538"] == 'Yes'){
                $G1136_C17538 = 1;
            }else if($_POST["G1136_C17538"] == 'off'){
                $G1136_C17538 = 0;
            }else if($_POST["G1136_C17538"] == 'on'){
                $G1136_C17538 = 1;
            }else if($_POST["G1136_C17538"] == 'No'){
                $G1136_C17538 = 0;
            }else{
                $G1136_C17538 = $_POST["G1136_C17538"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G1136_C17538 = ".$G1136_C17538."";
            $str_LsqlI .= $separador." G1136_C17538";
            $str_LsqlV .= $separador.$G1136_C17538;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G1136_C17538 = ".$G1136_C17538."";
            $str_LsqlI .= $separador." G1136_C17538";
            $str_LsqlV .= $separador.$G1136_C17538;

            $validar = 1;
        }
  
        if(isset($_POST["G1136_C17540"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C17540 = '".$_POST["G1136_C17540"]."'";
            $str_LsqlI .= $separador."G1136_C17540";
            $str_LsqlV .= $separador."'".$_POST["G1136_C17540"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1136_C18644"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C18644 = '".$_POST["G1136_C18644"]."'";
            $str_LsqlI .= $separador."G1136_C18644";
            $str_LsqlV .= $separador."'".$_POST["G1136_C18644"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1136_C20948"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C20948 = '".$_POST["G1136_C20948"]."'";
            $str_LsqlI .= $separador."G1136_C20948";
            $str_LsqlV .= $separador."'".$_POST["G1136_C20948"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1136_C20949"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C20949 = '".$_POST["G1136_C20949"]."'";
            $str_LsqlI .= $separador."G1136_C20949";
            $str_LsqlV .= $separador."'".$_POST["G1136_C20949"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1136_C20950"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_C20950 = '".$_POST["G1136_C20950"]."'";
            $str_LsqlI .= $separador."G1136_C20950";
            $str_LsqlV .= $separador."'".$_POST["G1136_C20950"]."'";
            $validar = 1;
        }
         
 
        $G1136_C20951 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1136_C20951"])){    
            if($_POST["G1136_C20951"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1136_C20951 = "'".str_replace(' ', '',$_POST["G1136_C20951"])." 00:00:00'";
                $str_LsqlU .= $separador." G1136_C20951 = ".$G1136_C20951;
                $str_LsqlI .= $separador." G1136_C20951";
                $str_LsqlV .= $separador.$G1136_C20951;
                $validar = 1;
            }
        }
  
        $G1136_C20952 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1136_C20952"])){   
            if($_POST["G1136_C20952"] != '' && $_POST["G1136_C20952"] != 'undefined' && $_POST["G1136_C20952"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1136_C20952 = "'".$fecha." ".str_replace(' ', '',$_POST["G1136_C20952"])."'";
                $str_LsqlU .= $separador." G1136_C20952 = ".$G1136_C20952."";
                $str_LsqlI .= $separador." G1136_C20952";
                $str_LsqlV .= $separador.$G1136_C20952;
                $validar = 1;
            }
        }
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1136_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1136_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1136_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //Si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;
                //ahora toca ver lo de la muestra asi que toca ver que pasa 
                /* primero buscamos la campaña que nos esta llegando */
                $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana"];

                //echo $Lsql_Campan;

                $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
                $datoCampan = $res_Lsql_Campan->fetch_array();
                $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
                $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
                $int_CAMPAN_ActPo = $datoCampan['CAMPAN_ActPobGui_b'];


                if($int_CAMPAN_ActPo == '-1'){
                    /* toca hacer actualizacion desde Script */
                    
                    $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana"];
                    $resultcampSql = $mysqli->query($campSql);
                    $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
                    $i=0;
                    while($key = $resultcampSql->fetch_object()){

                        if($i == 0){
                            $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }else{
                            $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }
                        $i++;
                    } 
                    $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$ultimoResgistroInsertado.' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
                    //echo "Esta ".$Lsql;
                    if($mysqli->query($Lsql) === TRUE ){

                    }else{
                        echo "NO sE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
                    }
                }


                //Ahora toca actualizar la muestra
                $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 3, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = '-9' , 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".date('Y-m-d H:i:s')."', 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = 3,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = 'No desea participar',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = NULL,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = '-9',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b ='".date('Y-m-d H:i:s')."'";
                $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_POST['id'];
                // echo $MuestraSql;
                if($mysqli->query($MuestraSql) === true){

                }else{
                    echo "Error insertando la muesta => ".$mysqli->error;
                }
                
                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTEzNg==&result=1');

            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
