<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1138 WHERE G1138_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }

      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1138_ConsInte__b, G1138_FechaInsercion ,USUARI_Nombre____b , G1138_CodigoMiembro, G1138_PoblacionOrigen , G1138_EstadoDiligenciamiento ,  G1138_IdLlamada , G1138_C17565 as principal ,G1138_C20673,G1138_CodigoMiembro,G1138_C17565,G1138_C17566,G1138_C17567,G1138_C20674,G1138_C20675,G1138_C17568,G1138_C17569,G1138_C17570,G1138_C17571,G1138_C17572,G1138_C18922,G1138_C17573,G1138_C17574,G1138_C17575,G1138_C17576,G1138_C18923,G1138_C18924,G1138_C18925,G1138_C18926,G1138_C22658,G1138_C17577,G1138_C17578,G1138_C17579,G1138_C17580,G1138_C17581,G1138_C17582,G1138_C17583,G1138_C17584,G1138_C17585,G1138_C17586,G1138_C17587,G1138_C17588,G1138_C18927,G1138_C18928,G1138_C18929,G1138_C18930,G1138_C18931,G1138_C18932 FROM '.$BaseDatos.'.G1138 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1138_Usuario WHERE G1138_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1138_C20673'] = $key->G1138_C20673;

                $datos[$i]['G1138_CodigoMiembro'] = $key->G1138_CodigoMiembro;

                $datos[$i]['G1138_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1138_C17565'] = $key->G1138_C17565;

                $datos[$i]['G1138_C17566'] = $key->G1138_C17566;

                $datos[$i]['G1138_C17567'] = $key->G1138_C17567;

                $datos[$i]['G1138_C20674'] = $key->G1138_C20674;

                $datos[$i]['G1138_C20675'] = $key->G1138_C20675;

                $datos[$i]['G1138_C17568'] = $key->G1138_C17568;

                $datos[$i]['G1138_C17569'] = $key->G1138_C17569;

                $datos[$i]['G1138_C17570'] = $key->G1138_C17570;

                $datos[$i]['G1138_C17571'] = $key->G1138_C17571;

                $datos[$i]['G1138_C17572'] = $key->G1138_C17572;

                $datos[$i]['G1138_C18922'] = $key->G1138_C18922;

                $datos[$i]['G1138_C17573'] = $key->G1138_C17573;

                $datos[$i]['G1138_C17574'] = $key->G1138_C17574;

                $datos[$i]['G1138_C17575'] = $key->G1138_C17575;

                $datos[$i]['G1138_C17576'] = $key->G1138_C17576;

                $datos[$i]['G1138_C18923'] = $key->G1138_C18923;

                $datos[$i]['G1138_C18924'] = $key->G1138_C18924;

                $datos[$i]['G1138_C18925'] = explode(' ', $key->G1138_C18925)[0];

                $datos[$i]['G1138_C18926'] = explode(' ', $key->G1138_C18926)[0];

                $datos[$i]['G1138_C22658'] = $key->G1138_C22658;

                $datos[$i]['G1138_C17577'] = $key->G1138_C17577;

                $datos[$i]['G1138_C17578'] = $key->G1138_C17578;

                $datos[$i]['G1138_C17579'] = $key->G1138_C17579;

                $datos[$i]['G1138_C17580'] = $key->G1138_C17580;

                $datos[$i]['G1138_C17581'] = explode(' ', $key->G1138_C17581)[0];
  
                $hora = '';
                if(!is_null($key->G1138_C17582)){
                    $hora = explode(' ', $key->G1138_C17582)[1];
                }

                $datos[$i]['G1138_C17583'] = $key->G1138_C17583;

                $datos[$i]['G1138_C17584'] = $key->G1138_C17584;

                $datos[$i]['G1138_C17585'] = $key->G1138_C17585;

                $datos[$i]['G1138_C17586'] = $key->G1138_C17586;

                $datos[$i]['G1138_C17587'] = $key->G1138_C17587;

                $datos[$i]['G1138_C17588'] = $key->G1138_C17588;

                $datos[$i]['G1138_C18927'] = $key->G1138_C18927;

                $datos[$i]['G1138_C18928'] = $key->G1138_C18928;

                $datos[$i]['G1138_C18929'] = $key->G1138_C18929;

                $datos[$i]['G1138_C18930'] = $key->G1138_C18930;

                $datos[$i]['G1138_C18931'] = $key->G1138_C18931;

                $datos[$i]['G1138_C18932'] = $key->G1138_C18932;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1138_ConsInte__b as id,  G1138_C17565 as camp1 , G1138_C17567 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1138 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1138_C17578  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1138_C17565 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1138_C17567 like '%".$_POST['Busqueda']."%' ) ";
            }
            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1138_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1138_C20673']) && $_POST['G1138_C20673'] != ''){
				$Lsql .= " AND G1138_C20673 LIKE '%". $_POST['G1138_C20673'] ."%' ";
			}
			if(!is_null($_POST['G1138_C17565']) && $_POST['G1138_C17565'] != ''){
				$Lsql .= " AND G1138_C17565 LIKE '%". $_POST['G1138_C17565'] ."%' ";
			}
			if(!is_null($_POST['G1138_C17566']) && $_POST['G1138_C17566'] != ''){
				$Lsql .= " AND G1138_C17566 LIKE '%". $_POST['G1138_C17566'] ."%' ";
			}
			if(!is_null($_POST['G1138_C17568']) && $_POST['G1138_C17568'] != ''){
				$Lsql .= " AND G1138_C17568 LIKE '%". $_POST['G1138_C17568'] ."%' ";
			}

            $Lsql .= "  ORDER BY  FIELD(G1138_C17578,14296,14297,14298,14299,14300), G1138_FechaInsercion DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1138_C18929'])){
            echo '<select class="form-control input-sm"  name="G1138_C18929" id="G1138_C18929">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1138_C18929'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1138_C18929'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1138_C18929'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1138");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1138_ConsInte__b, G1138_FechaInsercion , G1138_Usuario ,  G1138_CodigoMiembro  , G1138_PoblacionOrigen , G1138_EstadoDiligenciamiento ,  G1138_IdLlamada , G1138_C17565 as principal ,G1138_C20673,G1138_C17565,G1138_C17566,G1138_C17567,G1138_C20674,G1138_C20675,G1138_C17568,G1138_C17569,G1138_C17570,G1138_C17571,G1138_C17572,G1138_C18922,G1138_C17573,G1138_C17574,G1138_C17575,G1138_C17576,G1138_C18923, a.LISOPC_Nombre____b as G1138_C18924,G1138_C18925,G1138_C18926,G1138_C22658, b.LISOPC_Nombre____b as G1138_C17577, c.LISOPC_Nombre____b as G1138_C17578,G1138_C17579,G1138_C17580,G1138_C17581,G1138_C17582,G1138_C17583,G1138_C17584,G1138_C17585,G1138_C17586,G1138_C17587,G1138_C17588, d.LISOPC_Nombre____b as G1138_C18927, e.LISOPC_Nombre____b as G1138_C18928,G1138_C18930,G1138_C18931,G1138_C18932 FROM '.$BaseDatos.'.G1138 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1138_C18924 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1138_C17577 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1138_C17578 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1138_C18927 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1138_C18928 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1138_C18929';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1138_C17582)){
                    $hora_a = explode(' ', $fila->G1138_C17582)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1138_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1138_ConsInte__b , ($fila->G1138_C20673) , ($fila->G1138_C17565) , ($fila->G1138_C17566) , ($fila->G1138_C17567) , ($fila->G1138_C20674) , ($fila->G1138_C20675) , ($fila->G1138_C17568) , ($fila->G1138_C17569) , ($fila->G1138_C17570) , ($fila->G1138_C17571) , ($fila->G1138_C17572) , ($fila->G1138_C18922) , ($fila->G1138_C17573) , ($fila->G1138_C17574) , ($fila->G1138_C17575) , ($fila->G1138_C17576) , ($fila->G1138_C18923) , ($fila->G1138_C18924) , explode(' ', $fila->G1138_C18925)[0] , explode(' ', $fila->G1138_C18926)[0] , ($fila->G1138_C22658) , ($fila->G1138_C17577) , ($fila->G1138_C17578) , ($fila->G1138_C17579) , ($fila->G1138_C17580) , explode(' ', $fila->G1138_C17581)[0] , $hora_a , ($fila->G1138_C17583) , ($fila->G1138_C17584) , ($fila->G1138_C17585) , ($fila->G1138_C17586) , ($fila->G1138_C17587) , ($fila->G1138_C17588) , ($fila->G1138_C18927) , ($fila->G1138_C18928) , ($fila->G1138_C18930) , ($fila->G1138_C18931) , ($fila->G1138_C18932) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1138 WHERE G1138_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1138_ConsInte__b as id,  G1138_C17565 as camp1 , G1138_C17567 as camp2, G1138_C17578 as estado FROM '.$BaseDatos.'.G1138 ORDER BY FIELD(G1138_C17578,14280,14281,14282,14283,14284), G1138_FechaInsercion DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                
                $color = 'color:green;';
                $strIconoBackOffice = 'Cerrada';
                if($obj->estado == '14281' || $obj->estado == '14282'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14283'){
                    $color = 'color:green;';
                    $strIconoBackOffice = 'Cerrada';
                }else if($obj->estado == '14284'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }
                else if($obj->estado == '14280'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1138 SET "; 
  
            if(isset($_POST["G1138_C20673"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C20673 = '".$_POST["G1138_C20673"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17565"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17565 = '".$_POST["G1138_C17565"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17566"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17566 = '".$_POST["G1138_C17566"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17567"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17567 = '".$_POST["G1138_C17567"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C20674"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C20674 = '".$_POST["G1138_C20674"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C20675"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C20675 = '".$_POST["G1138_C20675"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17568"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17568 = '".$_POST["G1138_C17568"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17569"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17569 = '".$_POST["G1138_C17569"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17570"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17570 = '".$_POST["G1138_C17570"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17571"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17571 = '".$_POST["G1138_C17571"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17572"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17572 = '".$_POST["G1138_C17572"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C18922"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18922 = '".$_POST["G1138_C18922"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17573"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17573 = '".$_POST["G1138_C17573"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17574"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17574 = '".$_POST["G1138_C17574"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17575"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17575 = '".$_POST["G1138_C17575"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17576"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17576 = '".$_POST["G1138_C17576"]."'";
                $validar = 1;
            }
             
  
            $G1138_C18923 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C18923"])){
                if($_POST["G1138_C18923"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18923 = $_POST["G1138_C18923"];
                    $LsqlU .= $separador." G1138_C18923 = ".$G1138_C18923."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1138_C18924"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18924 = '".$_POST["G1138_C18924"]."'";
                $validar = 1;
            }
             
 
            $G1138_C18925 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1138_C18925"])){    
                if($_POST["G1138_C18925"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1138_C18925"]);
                    if(count($tieneHora) > 1){
                    	$G1138_C18925 = "'".$_POST["G1138_C18925"]."'";
                    }else{
                    	$G1138_C18925 = "'".str_replace(' ', '',$_POST["G1138_C18925"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1138_C18925 = ".$G1138_C18925;
                    $validar = 1;
                }
            }
 
            $G1138_C18926 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1138_C18926"])){    
                if($_POST["G1138_C18926"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1138_C18926"]);
                    if(count($tieneHora) > 1){
                    	$G1138_C18926 = "'".$_POST["G1138_C18926"]."'";
                    }else{
                    	$G1138_C18926 = "'".str_replace(' ', '',$_POST["G1138_C18926"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1138_C18926 = ".$G1138_C18926;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1138_C22658"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C22658 = '".$_POST["G1138_C22658"]."'";
                $validar = 1;
            }
             
 
            $G1138_C17577 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1138_C17577 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1138_C17577 = ".$G1138_C17577;
                    $validar = 1;

                    
                }
            }
 
            $G1138_C17578 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1138_C17578 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1138_C17578 = ".$G1138_C17578;
                    $validar = 1;
                }
            }
  
            $G1138_C17579 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C17579"])){
                if($_POST["G1138_C17579"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C17579 = $_POST["G1138_C17579"];
                    $LsqlU .= $separador." G1138_C17579 = ".$G1138_C17579."";
                    $validar = 1;
                }
            }
  
            $G1138_C17580 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C17580"])){
                if($_POST["G1138_C17580"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C17580 = $_POST["G1138_C17580"];
                    $LsqlU .= $separador." G1138_C17580 = ".$G1138_C17580."";
                    $validar = 1;
                }
            }
 
            $G1138_C17583 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1138_C17583 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1138_C17583 = ".$G1138_C17583;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1138_C17584"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17584 = '".$_POST["G1138_C17584"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17585"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17585 = '".$_POST["G1138_C17585"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17586"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17586 = '".$_POST["G1138_C17586"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17587"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17587 = '".$_POST["G1138_C17587"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C17588"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C17588 = '".$_POST["G1138_C17588"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C18927"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18927 = '".$_POST["G1138_C18927"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C18928"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18928 = '".$_POST["G1138_C18928"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1138_C18929"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18929 = '".$_POST["G1138_C18929"]."'";
                $validar = 1;
            }
             
  
            $G1138_C18930 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C18930"])){
                if($_POST["G1138_C18930"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18930 = $_POST["G1138_C18930"];
                    $LsqlU .= $separador." G1138_C18930 = ".$G1138_C18930."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1138_C18931"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_C18931 = '".$_POST["G1138_C18931"]."'";
                $validar = 1;
            }
             
  
            $G1138_C18932 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1138_C18932"])){
                if($_POST["G1138_C18932"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1138_C18932 = $_POST["G1138_C18932"];
                    $LsqlU .= $separador." G1138_C18932 = ".$G1138_C18932."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1138_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
             if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1138_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }
          
			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1138_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
