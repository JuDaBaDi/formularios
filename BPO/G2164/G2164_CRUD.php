<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2164;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2164
                  SET G2164_C42159 = -201
                  WHERE G2164_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2164 
                    WHERE G2164_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2164_C42158"]) || $gestion["G2164_C42158"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2164_C42158"];
        }

        if (is_null($gestion["G2164_C42160"]) || $gestion["G2164_C42160"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2164_C42160"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2164_FechaInsercion"]."',".$gestion["G2164_Usuario"].",'".$gestion["G2164_C".$P["P"]]."','".$gestion["G2164_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "bpo.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2164 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2164_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2164_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2164_LinkContenido as url FROM ".$BaseDatos.".G2164 WHERE G2164_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2164_ConsInte__b, G2164_FechaInsercion , G2164_Usuario ,  G2164_CodigoMiembro  , G2164_PoblacionOrigen , G2164_EstadoDiligenciamiento ,  G2164_IdLlamada , G2164_C42157 as principal ,G2164_C39806,G2164_C39807,G2164_C39808,G2164_C39809,G2164_C39810,G2164_C39811,G2164_C39812,G2164_C39813,G2164_C39814,G2164_C42166,G2164_C42167,G2164_C42169,G2164_C42170,G2164_C42172,G2164_C42174,G2164_C42175,G2164_C42176,G2164_C42178,G2164_C42179,G2164_C42180,G2164_C42182,G2164_C42183,G2164_C42184,G2164_C42185,G2164_C42186,G2164_C42187,G2164_C42188,G2164_C42159,G2164_C42158,G2164_C42160,G2164_C42161,G2164_C42162,G2164_C42163,G2164_C42153,G2164_C42154,G2164_C42155,G2164_C42156,G2164_C42157 FROM '.$BaseDatos.'.G2164 WHERE G2164_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2164_C39806'] = $key->G2164_C39806;

                $datos[$i]['G2164_C39807'] = $key->G2164_C39807;

                $datos[$i]['G2164_C39808'] = explode(' ', $key->G2164_C39808)[0];
  
                $hora = '';
                if(!is_null($key->G2164_C39809)){
                    $hora = explode(' ', $key->G2164_C39809)[1];
                }

                $datos[$i]['G2164_C39809'] = $hora;

                $datos[$i]['G2164_C39810'] = $key->G2164_C39810;

                $datos[$i]['G2164_C39811'] = $key->G2164_C39811;

                $datos[$i]['G2164_C39812'] = $key->G2164_C39812;

                $datos[$i]['G2164_C39813'] = $key->G2164_C39813;

                $datos[$i]['G2164_C39814'] = $key->G2164_C39814;

                $datos[$i]['G2164_C42166'] = $key->G2164_C42166;

                $datos[$i]['G2164_C42167'] = $key->G2164_C42167;

                $datos[$i]['G2164_C42169'] = $key->G2164_C42169;

                $datos[$i]['G2164_C42170'] = $key->G2164_C42170;

                $datos[$i]['G2164_C42172'] = $key->G2164_C42172;

                $datos[$i]['G2164_C42174'] = $key->G2164_C42174;

                $datos[$i]['G2164_C42175'] = $key->G2164_C42175;

                $datos[$i]['G2164_C42176'] = $key->G2164_C42176;

                $datos[$i]['G2164_C42178'] = $key->G2164_C42178;

                $datos[$i]['G2164_C42179'] = $key->G2164_C42179;

                $datos[$i]['G2164_C42180'] = $key->G2164_C42180;

                $datos[$i]['G2164_C42182'] = $key->G2164_C42182;

                $datos[$i]['G2164_C42183'] = $key->G2164_C42183;

                $datos[$i]['G2164_C42184'] = $key->G2164_C42184;

                $datos[$i]['G2164_C42185'] = $key->G2164_C42185;

                $datos[$i]['G2164_C42186'] = $key->G2164_C42186;

                $datos[$i]['G2164_C42187'] = $key->G2164_C42187;

                $datos[$i]['G2164_C42188'] = $key->G2164_C42188;

                $datos[$i]['G2164_C42159'] = $key->G2164_C42159;

                $datos[$i]['G2164_C42158'] = $key->G2164_C42158;

                $datos[$i]['G2164_C42160'] = $key->G2164_C42160;

                $datos[$i]['G2164_C42161'] = $key->G2164_C42161;

                $datos[$i]['G2164_C42162'] = explode(' ', $key->G2164_C42162)[0];

                $datos[$i]['G2164_C42163'] = $key->G2164_C42163;

                $datos[$i]['G2164_C42153'] = $key->G2164_C42153;

                $datos[$i]['G2164_C42154'] = $key->G2164_C42154;

                $datos[$i]['G2164_C42155'] = $key->G2164_C42155;

                $datos[$i]['G2164_C42156'] = $key->G2164_C42156;

                $datos[$i]['G2164_C42157'] = $key->G2164_C42157;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2164";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2164_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2164_ConsInte__b as id,  G2164_C42153 as camp2 , G2164_C42157 as camp1 
                     FROM ".$BaseDatos.".G2164  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2164_ConsInte__b as id,  G2164_C42153 as camp2 , G2164_C42157 as camp1  
                    FROM ".$BaseDatos.".G2164  JOIN ".$BaseDatos.".G2164_M".$_POST['muestra']." ON G2164_ConsInte__b = G2164_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2164_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2164_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2164_C42153 LIKE '%".$B."%' OR G2164_C42157 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2164_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2164");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2164_ConsInte__b, G2164_FechaInsercion , G2164_Usuario ,  G2164_CodigoMiembro  , G2164_PoblacionOrigen , G2164_EstadoDiligenciamiento ,  G2164_IdLlamada , G2164_C42157 as principal , a.LISOPC_Nombre____b as G2164_C39806, b.LISOPC_Nombre____b as G2164_C39807,G2164_C39808,G2164_C39809,G2164_C39810,G2164_C39811,G2164_C39812,G2164_C39813,G2164_C39814,G2164_C42166,G2164_C42167,G2164_C42169,G2164_C42170,G2164_C42172,G2164_C42174,G2164_C42175,G2164_C42176,G2164_C42178,G2164_C42179,G2164_C42180,G2164_C42182,G2164_C42183,G2164_C42184,G2164_C42185,G2164_C42186,G2164_C42187,G2164_C42188, c.LISOPC_Nombre____b as G2164_C42159,G2164_C42158,G2164_C42160,G2164_C42161,G2164_C42162,G2164_C42163,G2164_C42153,G2164_C42154,G2164_C42155,G2164_C42156,G2164_C42157 FROM '.$BaseDatos.'.G2164 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2164_C39806 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2164_C39807 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2164_C42159';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2164_C39809)){
                    $hora_a = explode(' ', $fila->G2164_C39809)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2164_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2164_ConsInte__b , ($fila->G2164_C39806) , ($fila->G2164_C39807) , explode(' ', $fila->G2164_C39808)[0] , $hora_a , ($fila->G2164_C39810) , ($fila->G2164_C39811) , ($fila->G2164_C39812) , ($fila->G2164_C39813) , ($fila->G2164_C39814) , ($fila->G2164_C42166) , ($fila->G2164_C42167) , ($fila->G2164_C42169) , ($fila->G2164_C42170) , ($fila->G2164_C42172) , ($fila->G2164_C42174) , ($fila->G2164_C42175) , ($fila->G2164_C42176) , ($fila->G2164_C42178) , ($fila->G2164_C42179) , ($fila->G2164_C42180) , ($fila->G2164_C42182) , ($fila->G2164_C42183) , ($fila->G2164_C42184) , ($fila->G2164_C42185) , ($fila->G2164_C42186) , ($fila->G2164_C42187) , ($fila->G2164_C42188) , ($fila->G2164_C42159) , ($fila->G2164_C42158) , ($fila->G2164_C42160) , ($fila->G2164_C42161) , explode(' ', $fila->G2164_C42162)[0] , ($fila->G2164_C42163) , ($fila->G2164_C42153) , ($fila->G2164_C42154) , ($fila->G2164_C42155) , ($fila->G2164_C42156) , ($fila->G2164_C42157) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2164 WHERE G2164_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2164";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2164_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2164_ConsInte__b as id,  G2164_C42153 as camp2 , G2164_C42157 as camp1  FROM '.$BaseDatos.'.G2164 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2164_ConsInte__b as id,  G2164_C42153 as camp2 , G2164_C42157 as camp1  
                    FROM ".$BaseDatos.".G2164  JOIN ".$BaseDatos.".G2164_M".$_POST['muestra']." ON G2164_ConsInte__b = G2164_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2164_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2164_C42153 LIKE "%'.$B.'%" OR G2164_C42157 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2164_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2164 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2164(";
            $LsqlV = " VALUES ("; 
 
            $G2164_C39806 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2164_C39806 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2164_C39806 = ".$G2164_C39806;
                    $LsqlI .= $separador." G2164_C39806";
                    $LsqlV .= $separador.$G2164_C39806;
                    $validar = 1;

                    
                }
            }
 
            $G2164_C39807 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2164_C39807 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2164_C39807 = ".$G2164_C39807;
                    $LsqlI .= $separador." G2164_C39807";
                    $LsqlV .= $separador.$G2164_C39807;
                    $validar = 1;
                }
            }
 
            $G2164_C39808 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2164_C39808 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2164_C39808 = ".$G2164_C39808;
                    $LsqlI .= $separador." G2164_C39808";
                    $LsqlV .= $separador.$G2164_C39808;
                    $validar = 1;
                }
            }
 
            $G2164_C39809 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2164_C39809 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2164_C39809 = ".$G2164_C39809;
                    $LsqlI .= $separador." G2164_C39809";
                    $LsqlV .= $separador.$G2164_C39809;
                    $validar = 1;
                }
            }
 
            $G2164_C39810 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2164_C39810 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2164_C39810 = ".$G2164_C39810;
                    $LsqlI .= $separador." G2164_C39810";
                    $LsqlV .= $separador.$G2164_C39810;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C39811"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C39811 = '".$_POST["G2164_C39811"]."'";
                $LsqlI .= $separador."G2164_C39811";
                $LsqlV .= $separador."'".$_POST["G2164_C39811"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C39812"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C39812 = '".$_POST["G2164_C39812"]."'";
                $LsqlI .= $separador."G2164_C39812";
                $LsqlV .= $separador."'".$_POST["G2164_C39812"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C39813"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C39813 = '".$_POST["G2164_C39813"]."'";
                $LsqlI .= $separador."G2164_C39813";
                $LsqlV .= $separador."'".$_POST["G2164_C39813"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C39814"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C39814 = '".$_POST["G2164_C39814"]."'";
                $LsqlI .= $separador."G2164_C39814";
                $LsqlV .= $separador."'".$_POST["G2164_C39814"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42165"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42165 = '".$_POST["G2164_C42165"]."'";
                $LsqlI .= $separador."G2164_C42165";
                $LsqlV .= $separador."'".$_POST["G2164_C42165"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42166 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42166"])){
                if($_POST["G2164_C42166"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42166 = $_POST["G2164_C42166"];
                    $LsqlU .= $separador." G2164_C42166 = ".$G2164_C42166."";
                    $LsqlI .= $separador." G2164_C42166";
                    $LsqlV .= $separador.$G2164_C42166;
                    $validar = 1;
                }
            }
  
            $G2164_C42167 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42167"])){
                if($_POST["G2164_C42167"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42167 = $_POST["G2164_C42167"];
                    $LsqlU .= $separador." G2164_C42167 = ".$G2164_C42167."";
                    $LsqlI .= $separador." G2164_C42167";
                    $LsqlV .= $separador.$G2164_C42167;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42168"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42168 = '".$_POST["G2164_C42168"]."'";
                $LsqlI .= $separador."G2164_C42168";
                $LsqlV .= $separador."'".$_POST["G2164_C42168"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42169 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42169"])){
                if($_POST["G2164_C42169"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42169 = $_POST["G2164_C42169"];
                    $LsqlU .= $separador." G2164_C42169 = ".$G2164_C42169."";
                    $LsqlI .= $separador." G2164_C42169";
                    $LsqlV .= $separador.$G2164_C42169;
                    $validar = 1;
                }
            }
  
            $G2164_C42170 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42170"])){
                if($_POST["G2164_C42170"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42170 = $_POST["G2164_C42170"];
                    $LsqlU .= $separador." G2164_C42170 = ".$G2164_C42170."";
                    $LsqlI .= $separador." G2164_C42170";
                    $LsqlV .= $separador.$G2164_C42170;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42171"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42171 = '".$_POST["G2164_C42171"]."'";
                $LsqlI .= $separador."G2164_C42171";
                $LsqlV .= $separador."'".$_POST["G2164_C42171"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42172 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42172"])){
                if($_POST["G2164_C42172"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42172 = $_POST["G2164_C42172"];
                    $LsqlU .= $separador." G2164_C42172 = ".$G2164_C42172."";
                    $LsqlI .= $separador." G2164_C42172";
                    $LsqlV .= $separador.$G2164_C42172;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42173"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42173 = '".$_POST["G2164_C42173"]."'";
                $LsqlI .= $separador."G2164_C42173";
                $LsqlV .= $separador."'".$_POST["G2164_C42173"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42174 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42174"])){
                if($_POST["G2164_C42174"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42174 = $_POST["G2164_C42174"];
                    $LsqlU .= $separador." G2164_C42174 = ".$G2164_C42174."";
                    $LsqlI .= $separador." G2164_C42174";
                    $LsqlV .= $separador.$G2164_C42174;
                    $validar = 1;
                }
            }
  
            $G2164_C42175 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42175"])){
                if($_POST["G2164_C42175"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42175 = $_POST["G2164_C42175"];
                    $LsqlU .= $separador." G2164_C42175 = ".$G2164_C42175."";
                    $LsqlI .= $separador." G2164_C42175";
                    $LsqlV .= $separador.$G2164_C42175;
                    $validar = 1;
                }
            }
  
            $G2164_C42176 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42176"])){
                if($_POST["G2164_C42176"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42176 = $_POST["G2164_C42176"];
                    $LsqlU .= $separador." G2164_C42176 = ".$G2164_C42176."";
                    $LsqlI .= $separador." G2164_C42176";
                    $LsqlV .= $separador.$G2164_C42176;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42177"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42177 = '".$_POST["G2164_C42177"]."'";
                $LsqlI .= $separador."G2164_C42177";
                $LsqlV .= $separador."'".$_POST["G2164_C42177"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42178 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42178"])){
                if($_POST["G2164_C42178"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42178 = $_POST["G2164_C42178"];
                    $LsqlU .= $separador." G2164_C42178 = ".$G2164_C42178."";
                    $LsqlI .= $separador." G2164_C42178";
                    $LsqlV .= $separador.$G2164_C42178;
                    $validar = 1;
                }
            }
  
            $G2164_C42179 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42179"])){
                if($_POST["G2164_C42179"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42179 = $_POST["G2164_C42179"];
                    $LsqlU .= $separador." G2164_C42179 = ".$G2164_C42179."";
                    $LsqlI .= $separador." G2164_C42179";
                    $LsqlV .= $separador.$G2164_C42179;
                    $validar = 1;
                }
            }
  
            $G2164_C42180 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42180"])){
                if($_POST["G2164_C42180"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42180 = $_POST["G2164_C42180"];
                    $LsqlU .= $separador." G2164_C42180 = ".$G2164_C42180."";
                    $LsqlI .= $separador." G2164_C42180";
                    $LsqlV .= $separador.$G2164_C42180;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42181"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42181 = '".$_POST["G2164_C42181"]."'";
                $LsqlI .= $separador."G2164_C42181";
                $LsqlV .= $separador."'".$_POST["G2164_C42181"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42182 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42182"])){
                if($_POST["G2164_C42182"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42182 = $_POST["G2164_C42182"];
                    $LsqlU .= $separador." G2164_C42182 = ".$G2164_C42182."";
                    $LsqlI .= $separador." G2164_C42182";
                    $LsqlV .= $separador.$G2164_C42182;
                    $validar = 1;
                }
            }
  
            $G2164_C42183 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42183"])){
                if($_POST["G2164_C42183"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42183 = $_POST["G2164_C42183"];
                    $LsqlU .= $separador." G2164_C42183 = ".$G2164_C42183."";
                    $LsqlI .= $separador." G2164_C42183";
                    $LsqlV .= $separador.$G2164_C42183;
                    $validar = 1;
                }
            }
  
            $G2164_C42184 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42184"])){
                if($_POST["G2164_C42184"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42184 = $_POST["G2164_C42184"];
                    $LsqlU .= $separador." G2164_C42184 = ".$G2164_C42184."";
                    $LsqlI .= $separador." G2164_C42184";
                    $LsqlV .= $separador.$G2164_C42184;
                    $validar = 1;
                }
            }
  
            $G2164_C42185 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42185"])){
                if($_POST["G2164_C42185"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42185 = $_POST["G2164_C42185"];
                    $LsqlU .= $separador." G2164_C42185 = ".$G2164_C42185."";
                    $LsqlI .= $separador." G2164_C42185";
                    $LsqlV .= $separador.$G2164_C42185;
                    $validar = 1;
                }
            }
  
            $G2164_C42186 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42186"])){
                if($_POST["G2164_C42186"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42186 = $_POST["G2164_C42186"];
                    $LsqlU .= $separador." G2164_C42186 = ".$G2164_C42186."";
                    $LsqlI .= $separador." G2164_C42186";
                    $LsqlV .= $separador.$G2164_C42186;
                    $validar = 1;
                }
            }
  
            $G2164_C42187 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42187"])){
                if($_POST["G2164_C42187"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42187 = $_POST["G2164_C42187"];
                    $LsqlU .= $separador." G2164_C42187 = ".$G2164_C42187."";
                    $LsqlI .= $separador." G2164_C42187";
                    $LsqlV .= $separador.$G2164_C42187;
                    $validar = 1;
                }
            }
  
            $G2164_C42188 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42188"])){
                if($_POST["G2164_C42188"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42188 = $_POST["G2164_C42188"];
                    $LsqlU .= $separador." G2164_C42188 = ".$G2164_C42188."";
                    $LsqlI .= $separador." G2164_C42188";
                    $LsqlV .= $separador.$G2164_C42188;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42159 = '".$_POST["G2164_C42159"]."'";
                $LsqlI .= $separador."G2164_C42159";
                $LsqlV .= $separador."'".$_POST["G2164_C42159"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42158 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42158"])){
                if($_POST["G2164_C42158"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42158 = $_POST["G2164_C42158"];
                    $LsqlU .= $separador." G2164_C42158 = ".$G2164_C42158."";
                    $LsqlI .= $separador." G2164_C42158";
                    $LsqlV .= $separador.$G2164_C42158;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42160"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42160 = '".$_POST["G2164_C42160"]."'";
                $LsqlI .= $separador."G2164_C42160";
                $LsqlV .= $separador."'".$_POST["G2164_C42160"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42161"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42161 = '".$_POST["G2164_C42161"]."'";
                $LsqlI .= $separador."G2164_C42161";
                $LsqlV .= $separador."'".$_POST["G2164_C42161"]."'";
                $validar = 1;
            }
             
 
            $G2164_C42162 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2164_C42162"])){    
                if($_POST["G2164_C42162"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2164_C42162"]);
                    if(count($tieneHora) > 1){
                        $G2164_C42162 = "'".$_POST["G2164_C42162"]."'";
                    }else{
                        $G2164_C42162 = "'".str_replace(' ', '',$_POST["G2164_C42162"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2164_C42162 = ".$G2164_C42162;
                    $LsqlI .= $separador." G2164_C42162";
                    $LsqlV .= $separador.$G2164_C42162;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2164_C42163"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42163 = '".$_POST["G2164_C42163"]."'";
                $LsqlI .= $separador."G2164_C42163";
                $LsqlV .= $separador."'".$_POST["G2164_C42163"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42152"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42152 = '".$_POST["G2164_C42152"]."'";
                $LsqlI .= $separador."G2164_C42152";
                $LsqlV .= $separador."'".$_POST["G2164_C42152"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42153 = '".$_POST["G2164_C42153"]."'";
                $LsqlI .= $separador."G2164_C42153";
                $LsqlV .= $separador."'".$_POST["G2164_C42153"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42154"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42154 = '".$_POST["G2164_C42154"]."'";
                $LsqlI .= $separador."G2164_C42154";
                $LsqlV .= $separador."'".$_POST["G2164_C42154"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42155"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42155 = '".$_POST["G2164_C42155"]."'";
                $LsqlI .= $separador."G2164_C42155";
                $LsqlV .= $separador."'".$_POST["G2164_C42155"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2164_C42156"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_C42156 = '".$_POST["G2164_C42156"]."'";
                $LsqlI .= $separador."G2164_C42156";
                $LsqlV .= $separador."'".$_POST["G2164_C42156"]."'";
                $validar = 1;
            }
             
  
            $G2164_C42157 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2164_C42157"])){
                if($_POST["G2164_C42157"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2164_C42157 = $_POST["G2164_C42157"];
                    $LsqlU .= $separador." G2164_C42157 = ".$G2164_C42157."";
                    $LsqlI .= $separador." G2164_C42157";
                    $LsqlV .= $separador.$G2164_C42157;
                    $validar = 1;
                }
            }

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2164_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2164_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2164_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2164_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2164_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2164_Usuario , G2164_FechaInsercion, G2164_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2164_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2164 WHERE G2164_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                            $UltimoID = $_POST["id"];
                            echo $UltimoID;
                        }
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
