<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1831_ConsInte__b, G1831_FechaInsercion , G1831_Usuario ,  G1831_CodigoMiembro  , G1831_PoblacionOrigen , G1831_EstadoDiligenciamiento ,  G1831_IdLlamada , G1831_C33788 as principal ,G1831_C33788,G1831_C33789,G1831_C33790,G1831_C33791,G1831_C33792,G1831_C33793,G1831_C33794,G1831_C33795,G1831_C33796,G1831_C33797,G1831_C33808,G1831_C33798,G1831_C33785,G1831_C33786,G1831_C33787 FROM '.$BaseDatos.'.G1831 WHERE G1831_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1831_C33788'] = $key->G1831_C33788;

                $datos[$i]['G1831_C33789'] = $key->G1831_C33789;

                $datos[$i]['G1831_C33790'] = $key->G1831_C33790;

                $datos[$i]['G1831_C33791'] = $key->G1831_C33791;

                $datos[$i]['G1831_C33792'] = $key->G1831_C33792;

                $datos[$i]['G1831_C33793'] = $key->G1831_C33793;

                $datos[$i]['G1831_C33794'] = $key->G1831_C33794;

                $datos[$i]['G1831_C33795'] = explode(' ', $key->G1831_C33795)[0];
  
                $hora = '';
                if(!is_null($key->G1831_C33796)){
                    $hora = explode(' ', $key->G1831_C33796)[1];
                }

                $datos[$i]['G1831_C33796'] = $hora;

                $datos[$i]['G1831_C33797'] = $key->G1831_C33797;

                $datos[$i]['G1831_C33808'] = $key->G1831_C33808;

                $datos[$i]['G1831_C33798'] = $key->G1831_C33798;

                $datos[$i]['G1831_C33785'] = $key->G1831_C33785;

                $datos[$i]['G1831_C33786'] = $key->G1831_C33786;

                $datos[$i]['G1831_C33787'] = $key->G1831_C33787;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1831";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1831_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1831_ConsInte__b as id,  G1831_C33788 as camp1 , G1831_C33789 as camp2 
                     FROM ".$BaseDatos.".G1831  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1831_ConsInte__b as id,  G1831_C33788 as camp1 , G1831_C33789 as camp2  
                    FROM ".$BaseDatos.".G1831  JOIN ".$BaseDatos.".G1831_M".$_POST['muestra']." ON G1831_ConsInte__b = G1831_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1831_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1831_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1831_C33788 LIKE '%".$B."%' OR G1831_C33789 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1831_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G1831_C33808'])){
                                $Ysql = "SELECT G1833_ConsInte__b as id, G1833_C33802 as text FROM ".$BaseDatos.".G1833 WHERE G1833_C33802 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1831_C33808"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G1833 WHERE G1833_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1831_C33808"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1831");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1831_ConsInte__b, G1831_FechaInsercion , G1831_Usuario ,  G1831_CodigoMiembro  , G1831_PoblacionOrigen , G1831_EstadoDiligenciamiento ,  G1831_IdLlamada , G1831_C33788 as principal ,G1831_C33788,G1831_C33789,G1831_C33790,G1831_C33791,G1831_C33792,G1831_C33793,G1831_C33794,G1831_C33795,G1831_C33796, a.LISOPC_Nombre____b as G1831_C33797, G1833_C33802,G1831_C33798,G1831_C33785,G1831_C33786, b.LISOPC_Nombre____b as G1831_C33787 FROM '.$BaseDatos.'.G1831 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1831_C33797 LEFT JOIN '.$BaseDatos.'.G1833 ON G1833_ConsInte__b  =  G1831_C33808 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1831_C33787';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1831_C33796)){
                    $hora_a = explode(' ', $fila->G1831_C33796)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1831_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1831_ConsInte__b , ($fila->G1831_C33788) , ($fila->G1831_C33789) , ($fila->G1831_C33790) , ($fila->G1831_C33791) , ($fila->G1831_C33792) , ($fila->G1831_C33793) , ($fila->G1831_C33794) , explode(' ', $fila->G1831_C33795)[0] , $hora_a , ($fila->G1831_C33797) , ($fila->G1833_C33802) , ($fila->G1831_C33798) , ($fila->G1831_C33785) , ($fila->G1831_C33786) , ($fila->G1831_C33787) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1831 WHERE G1831_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1831";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1831_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1831_ConsInte__b as id,  G1831_C33788 as camp1 , G1831_C33789 as camp2  FROM '.$BaseDatos.'.G1831 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1831_ConsInte__b as id,  G1831_C33788 as camp1 , G1831_C33789 as camp2  
                    FROM ".$BaseDatos.".G1831  JOIN ".$BaseDatos.".G1831_M".$_POST['muestra']." ON G1831_ConsInte__b = G1831_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1831_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1831_C33788 LIKE "%'.$B.'%" OR G1831_C33789 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1831_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1831 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1831(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1831_C33788"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33788 = '".$_POST["G1831_C33788"]."'";
                $LsqlI .= $separador."G1831_C33788";
                $LsqlV .= $separador."'".$_POST["G1831_C33788"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33789"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33789 = '".$_POST["G1831_C33789"]."'";
                $LsqlI .= $separador."G1831_C33789";
                $LsqlV .= $separador."'".$_POST["G1831_C33789"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33790"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33790 = '".$_POST["G1831_C33790"]."'";
                $LsqlI .= $separador."G1831_C33790";
                $LsqlV .= $separador."'".$_POST["G1831_C33790"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33791"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33791 = '".$_POST["G1831_C33791"]."'";
                $LsqlI .= $separador."G1831_C33791";
                $LsqlV .= $separador."'".$_POST["G1831_C33791"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33792"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33792 = '".$_POST["G1831_C33792"]."'";
                $LsqlI .= $separador."G1831_C33792";
                $LsqlV .= $separador."'".$_POST["G1831_C33792"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33793"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33793 = '".$_POST["G1831_C33793"]."'";
                $LsqlI .= $separador."G1831_C33793";
                $LsqlV .= $separador."'".$_POST["G1831_C33793"]."'";
                $validar = 1;
            }
             
  
            $G1831_C33794 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1831_C33794"])){
                if($_POST["G1831_C33794"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1831_C33794 = $_POST["G1831_C33794"];
                    $LsqlU .= $separador." G1831_C33794 = ".$G1831_C33794."";
                    $LsqlI .= $separador." G1831_C33794";
                    $LsqlV .= $separador.$G1831_C33794;
                    $validar = 1;
                }
            }
 
            $G1831_C33795 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1831_C33795"])){    
                if($_POST["G1831_C33795"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1831_C33795"]);
                    if(count($tieneHora) > 1){
                        $G1831_C33795 = "'".$_POST["G1831_C33795"]."'";
                    }else{
                        $G1831_C33795 = "'".str_replace(' ', '',$_POST["G1831_C33795"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1831_C33795 = ".$G1831_C33795;
                    $LsqlI .= $separador." G1831_C33795";
                    $LsqlV .= $separador.$G1831_C33795;
                    $validar = 1;
                }
            }
  
            $G1831_C33796 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1831_C33796"])){   
                if($_POST["G1831_C33796"] != '' && $_POST["G1831_C33796"] != 'undefined' && $_POST["G1831_C33796"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1831_C33796 = "'".$fecha." ".str_replace(' ', '',$_POST["G1831_C33796"])."'";
                    $LsqlU .= $separador." G1831_C33796 = ".$G1831_C33796."";
                    $LsqlI .= $separador." G1831_C33796";
                    $LsqlV .= $separador.$G1831_C33796;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1831_C33797"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33797 = '".$_POST["G1831_C33797"]."'";
                $LsqlI .= $separador."G1831_C33797";
                $LsqlV .= $separador."'".$_POST["G1831_C33797"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33808"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33808 = '".$_POST["G1831_C33808"]."'";
                $LsqlI .= $separador."G1831_C33808";
                $LsqlV .= $separador."'".$_POST["G1831_C33808"]."'";
                $validar = 1;
            }
             
  
            $G1831_C33798 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1831_C33798"])){
                if($_POST["G1831_C33798"] == 'Yes'){
                    $G1831_C33798 = 1;
                }else if($_POST["G1831_C33798"] == 'off'){
                    $G1831_C33798 = 0;
                }else if($_POST["G1831_C33798"] == 'on'){
                    $G1831_C33798 = 1;
                }else if($_POST["G1831_C33798"] == 'No'){
                    $G1831_C33798 = 1;
                }else{
                    $G1831_C33798 = $_POST["G1831_C33798"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1831_C33798 = ".$G1831_C33798."";
                $LsqlI .= $separador." G1831_C33798";
                $LsqlV .= $separador.$G1831_C33798;

                $validar = 1;
            }
  
            if(isset($_POST["G1831_C33785"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33785 = '".$_POST["G1831_C33785"]."'";
                $LsqlI .= $separador."G1831_C33785";
                $LsqlV .= $separador."'".$_POST["G1831_C33785"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33786"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33786 = '".$_POST["G1831_C33786"]."'";
                $LsqlI .= $separador."G1831_C33786";
                $LsqlV .= $separador."'".$_POST["G1831_C33786"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1831_C33787"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_C33787 = '".$_POST["G1831_C33787"]."'";
                $LsqlI .= $separador."G1831_C33787";
                $LsqlV .= $separador."'".$_POST["G1831_C33787"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1831_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1831_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1831_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1831_Usuario , G1831_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1831_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1831 WHERE G1831_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){



        $data =[
            "strUsuario_t"   =>  'local',
            "strToken_t"     =>  'local'
        ];
    //Codificamos el arreglo en formato JSON
    $strDatosJSON_t = json_encode($data);
    
    //Inicializamos la conexion CURL al web service local para ser consumido
    $objCURL_t = curl_init("http://127.0.0.1:8080/dy_distribuidor_trabajo/api/cache/refresca");
    
    //Asignamos todos los parametros del consumo
    curl_setopt($objCURL_t, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($objCURL_t, CURLOPT_POSTFIELDS, $strDatosJSON_t); 
    curl_setopt($objCURL_t,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($objCURL_t, CURLOPT_HTTPHEADER, array(
        'Accept: application/json',
        'Content-Type: application/json',
        'Content-Length: ' . strlen($strDatosJSON_t))                                                                      
    );

    //Obtenemos la respuesta
    $objRespuestaCURL_t = curl_exec($objCURL_t);

    //Obtenemos el error 
    $objRespuestaError_t = curl_error($objCURL_t);

    //Cerramos la conexion
    curl_close ($objCURL_t);

echo $objRespuestaCURL_t."haberrr";


                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G1831 SET G1831_UltiGest__b =-14, G1831_GesMasImp_b =-14, G1831_TipoReintentoUG_b =0, G1831_TipoReintentoGMI_b =0, G1831_EstadoUG_b =-14, G1831_EstadoGMI_b =-14, G1831_CantidadIntentos =0, G1831_CantidadIntentosGMI_b =0 WHERE G1831_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
