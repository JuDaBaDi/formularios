
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1831/G1831_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1831_ConsInte__b as id, G1831_C33788 as camp1 , G1831_C33789 as camp2 FROM ".$BaseDatos.".G1831  WHERE G1831_Usuario = ".$idUsuario." ORDER BY G1831_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1831_ConsInte__b as id, G1831_C33788 as camp1 , G1831_C33789 as camp2 FROM ".$BaseDatos.".G1831  ORDER BY G1831_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1831_ConsInte__b as id, G1831_C33788 as camp1 , G1831_C33789 as camp2 FROM ".$BaseDatos.".G1831 JOIN ".$BaseDatos.".G1831_M".$resultEstpas->muestr." ON G1831_ConsInte__b = G1831_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1831_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1831_ConsInte__b as id, G1831_C33788 as camp1 , G1831_C33789 as camp2 FROM ".$BaseDatos.".G1831 JOIN ".$BaseDatos.".G1831_M".$resultEstpas->muestr." ON G1831_ConsInte__b = G1831_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1831_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1831_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1831_ConsInte__b as id, G1831_C33788 as camp1 , G1831_C33789 as camp2 FROM ".$BaseDatos.".G1831  ORDER BY G1831_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  id="4731" >
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33788" id="LblG1831_C33788">CEDULA</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33788" value="<?php if (isset($_GET['G1831_C33788'])) {
                            echo $_GET['G1831_C33788'];
                        } ?>"  name="G1831_C33788"  placeholder="CEDULA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33789" id="LblG1831_C33789">TELEFONO</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33789" value="<?php if (isset($_GET['G1831_C33789'])) {
                            echo $_GET['G1831_C33789'];
                        } ?>"  name="G1831_C33789"  placeholder="TELEFONO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33790" id="LblG1831_C33790">TEXTO</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33790" value="<?php if (isset($_GET['G1831_C33790'])) {
                            echo $_GET['G1831_C33790'];
                        } ?>"  name="G1831_C33790"  placeholder="TEXTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33791" id="LblG1831_C33791">MEMO</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33791" value="<?php if (isset($_GET['G1831_C33791'])) {
                            echo $_GET['G1831_C33791'];
                        } ?>"  name="G1831_C33791"  placeholder="MEMO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33792" id="LblG1831_C33792">EMAIL</label>
                        <input type="email" class="form-control input-sm" id="G1831_C33792" value="<?php if (isset($_GET['G1831_C33792'])) {
                            echo $_GET['G1831_C33792'];
                        } ?>"  name="G1831_C33792"  placeholder="EMAIL">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33793" id="LblG1831_C33793">NUMERICO</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33793" value="<?php if (isset($_GET['G1831_C33793'])) {
                            echo $_GET['G1831_C33793'];
                        } ?>"  name="G1831_C33793"  placeholder="NUMERICO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO DECIMAL -->
                    <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1831_C33794" id="LblG1831_C33794">DECIMAL</label>
                        <input type="text" class="form-control input-sm Decimal" value="<?php if (isset($_GET['G1831_C33794'])) {
                            echo $_GET['G1831_C33794'];
                        } ?>"  name="G1831_C33794" id="G1831_C33794" placeholder="DECIMAL">
                    </div>
                    <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1831_C33795" id="LblG1831_C33795">FECHA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1831_C33795'])) {
                            echo $_GET['G1831_C33795'];
                        } ?>"  name="G1831_C33795" id="G1831_C33795" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G1831_C33796" id="LblG1831_C33796">HORA</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora"  name="G1831_C33796" id="G1831_C33796" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G1831_C33796">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1831_C33797" id="LblG1831_C33797">LISTA</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1831_C33797" id="G1831_C33797">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1873 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G1831_C33808" id="LblG1831_C33808">LISTA COMPLEJA</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G1831_C33808" id="G1831_C33808">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                    <div class="form-group">
                        <label for="G1831_C33798" id="LblG1831_C33798">CASILLA</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="G1831_C33798" id="G1831_C33798" data-error="Before you wreck yourself"  > 
                            </label>
                        </div>
                    </div>
                    <!-- FIN DEL CAMPO SI/NO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="4732" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33785" id="LblG1831_C33785">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33785" value="<?php if (isset($_GET['G1831_C33785'])) {
                            echo $_GET['G1831_C33785'];
                        } ?>" readonly name="G1831_C33785"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1831_C33786" id="LblG1831_C33786">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1831_C33786" value="<?php if (isset($_GET['G1831_C33786'])) {
                            echo $_GET['G1831_C33786'];
                        } ?>" readonly name="G1831_C33786"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1831_C33787" id="LblG1831_C33787">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1831_C33787" id="G1831_C33787">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1872 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1831/G1831_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1831_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1831_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1831_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G1831_C33788").val(item.G1831_C33788); 
                $("#G1831_C33789").val(item.G1831_C33789); 
                $("#G1831_C33790").val(item.G1831_C33790); 
                $("#G1831_C33791").val(item.G1831_C33791); 
                $("#G1831_C33792").val(item.G1831_C33792); 
                $("#G1831_C33793").val(item.G1831_C33793); 
                $("#G1831_C33794").val(item.G1831_C33794); 
                $("#G1831_C33795").val(item.G1831_C33795); 
                $("#G1831_C33796").val(item.G1831_C33796); 
                $("#G1831_C33797").val(item.G1831_C33797).trigger("change");  
                $("#G1831_C33808").attr("opt",item.G1831_C33808);
                $("#G1831_C33808").val(item.G1831_C33808).trigger("change");   
                if(item.G1831_C33798 == 1){
                    $("#G1831_C33798").attr('checked', true);
                }  
                $("#G1831_C33785").val(item.G1831_C33785); 
                $("#G1831_C33786").val(item.G1831_C33786); 
                $("#G1831_C33787").val(item.G1831_C33787).trigger("change"); 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1831_C33797").select2();
                            $("#G1831_C33808").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1831_C33808=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G1831_C33808").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G1831_C33808 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G1831_C33808").html('<option value="'+data.G1833_ConsInte__b+'" >'+data.G1833_C33802+'</option>');
                                        
                                    }
                                });
                            });

    $("#G1831_C33787").select2();
        //datepickers
        

        $("#G1831_C33795").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'HORA', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1831_C33796").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        

        $("#G1831_C33794").numeric({ decimal : ".",  negative : false, scale: 4 });
                

        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para LISTA 

    $("#G1831_C33797").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G1831_C33787").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1831_C33788").val(item.G1831_C33788);
 
                                                $("#G1831_C33789").val(item.G1831_C33789);
 
                                                $("#G1831_C33790").val(item.G1831_C33790);
 
                                                $("#G1831_C33791").val(item.G1831_C33791);
 
                                                $("#G1831_C33792").val(item.G1831_C33792);
 
                                                $("#G1831_C33793").val(item.G1831_C33793);
 
                                                $("#G1831_C33794").val(item.G1831_C33794);
 
                                                $("#G1831_C33795").val(item.G1831_C33795);
 
                                                $("#G1831_C33796").val(item.G1831_C33796);
 
                    $("#G1831_C33797").val(item.G1831_C33797).trigger("change"); 
 
                    $("#G1831_C33808").attr("opt",item.G1831_C33808);
                    $("#G1831_C33808").val(item.G1831_C33808).trigger("change");
      
                                                if(item.G1831_C33798 == 1){
                                                   $("#G1831_C33798").attr('checked', true);
                                                } 
 
                                                $("#G1831_C33785").val(item.G1831_C33785);
 
                                                $("#G1831_C33786").val(item.G1831_C33786);
 
                    $("#G1831_C33787").val(item.G1831_C33787).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CEDULA','TELEFONO','TEXTO','MEMO','EMAIL','NUMERICO','DECIMAL','FECHA','HORA','LISTA','LISTA COMPLEJA','CASILLA','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1831_C33788', 
                        index: 'G1831_C33788', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1831_C33789', 
                        index: 'G1831_C33789', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1831_C33790', 
                        index: 'G1831_C33790', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1831_C33791', 
                        index: 'G1831_C33791', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1831_C33793', 
                        index: 'G1831_C33793', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G1831_C33794', 
                        index:'G1831_C33794', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                            }
                        } 
                    }

                    ,
                    {  
                        name:'G1831_C33795', 
                        index:'G1831_C33795', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G1831_C33796', 
                        index:'G1831_C33796', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1831_C33797', 
                        index:'G1831_C33797', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1873&campo=G1831_C33797'
                        }
                    }

                    ,
                    { 
                        name:'G1831_C33808', 
                        index:'G1831_C33808', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1831_C33808=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1831_C33798', 
                        index:'G1831_C33798', 
                        width:70 ,
                        editable: true, 
                        edittype:"checkbox",
                        editoptions: {
                            value:"1:0"
                        } 
                    }

                    ,
                    { 
                        name:'G1831_C33785', 
                        index: 'G1831_C33785', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1831_C33786', 
                        index: 'G1831_C33786', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1831_C33787', 
                        index:'G1831_C33787', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1872&campo=G1831_C33787'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1831_C33788',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1831_C33788").val(item.G1831_C33788);

                        $("#G1831_C33789").val(item.G1831_C33789);

                        $("#G1831_C33790").val(item.G1831_C33790);

                        $("#G1831_C33791").val(item.G1831_C33791);

                        $("#G1831_C33792").val(item.G1831_C33792);

                        $("#G1831_C33793").val(item.G1831_C33793);

                        $("#G1831_C33794").val(item.G1831_C33794);

                        $("#G1831_C33795").val(item.G1831_C33795);

                        $("#G1831_C33796").val(item.G1831_C33796);
 
                    $("#G1831_C33797").val(item.G1831_C33797).trigger("change"); 
 
                    $("#G1831_C33808").attr("opt",item.G1831_C33808);
                    $("#G1831_C33808").val(item.G1831_C33808).trigger("change");
    
                        if(item.G1831_C33798 == 1){
                           $("#G1831_C33798").attr('checked', true);
                        } 

                        $("#G1831_C33785").val(item.G1831_C33785);

                        $("#G1831_C33786").val(item.G1831_C33786);
 
                    $("#G1831_C33787").val(item.G1831_C33787).trigger("change"); 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
