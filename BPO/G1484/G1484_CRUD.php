<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1484;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1484
                  SET G1484_C31831 = -201
                  WHERE G1484_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1484 
                    WHERE G1484_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1484_C31830"]) || $gestion["G1484_C31830"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1484_C31830"];
        }

        if (is_null($gestion["G1484_C31832"]) || $gestion["G1484_C31832"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1484_C31832"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1484_FechaInsercion"]."',".$gestion["G1484_Usuario"].",'".$gestion["G1484_C".$P["P"]]."','".$gestion["G1484_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "bpo.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>Añadir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1484 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1484_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1484_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1484_LinkContenido as url FROM ".$BaseDatos.".G1484 WHERE G1484_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1484_ConsInte__b, G1484_FechaInsercion , G1484_Usuario ,  G1484_CodigoMiembro  , G1484_PoblacionOrigen , G1484_EstadoDiligenciamiento ,  G1484_IdLlamada , G1484_C26521 as principal ,G1484_C26520,G1484_C29133,G1484_C29134,G1484_C29135,G1484_C26523,G1484_C26521,G1484_C26522,G1484_C26518,G1484_C26526,G1484_C26527,G1484_C29075,G1484_C26528,G1484_C26529,G1484_C26530,G1484_C26532,G1484_C26533,G1484_C26534,G1484_C26535,G1484_C26536,G1484_C26537,G1484_C26538,G1484_C26539,G1484_C26540,G1484_C31835,G1484_C31836,G1484_C31837,G1484_C31838,G1484_C31840,G1484_C31841,G1484_C31842,G1484_C31843,G1484_C31845,G1484_C31831,G1484_C31830,G1484_C31832,G1484_C31833,G1484_C29136,G1484_C29137,G1484_C29138,G1484_C29139,G1484_C29140 FROM '.$BaseDatos.'.G1484 WHERE G1484_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1484_C26520'] = $key->G1484_C26520;

                $datos[$i]['G1484_C29133'] = $key->G1484_C29133;

                $datos[$i]['G1484_C29134'] = $key->G1484_C29134;

                $datos[$i]['G1484_C29135'] = $key->G1484_C29135;

                $datos[$i]['G1484_C26523'] = $key->G1484_C26523;

                $datos[$i]['G1484_C26521'] = $key->G1484_C26521;

                $datos[$i]['G1484_C26522'] = $key->G1484_C26522;

                $datos[$i]['G1484_C26518'] = $key->G1484_C26518;

                $datos[$i]['G1484_C26526'] = $key->G1484_C26526;
  
                $hora = '';
                if(!is_null($key->G1484_C26527)){
                    $hora = explode(' ', $key->G1484_C26527)[1];
                }

                $datos[$i]['G1484_C26527'] = $hora;

                $datos[$i]['G1484_C29075'] = $key->G1484_C29075;

                $datos[$i]['G1484_C26528'] = $key->G1484_C26528;

                $datos[$i]['G1484_C26529'] = $key->G1484_C26529;

                $datos[$i]['G1484_C26530'] = $key->G1484_C26530;

                $datos[$i]['G1484_C26532'] = $key->G1484_C26532;

                $datos[$i]['G1484_C26533'] = $key->G1484_C26533;

                $datos[$i]['G1484_C26534'] = explode(' ', $key->G1484_C26534)[0];
  
                $hora = '';
                if(!is_null($key->G1484_C26535)){
                    $hora = explode(' ', $key->G1484_C26535)[1];
                }

                $datos[$i]['G1484_C26535'] = $hora;

                $datos[$i]['G1484_C26536'] = $key->G1484_C26536;

                $datos[$i]['G1484_C26537'] = $key->G1484_C26537;

                $datos[$i]['G1484_C26538'] = $key->G1484_C26538;

                $datos[$i]['G1484_C26539'] = $key->G1484_C26539;

                $datos[$i]['G1484_C26540'] = $key->G1484_C26540;

                $datos[$i]['G1484_C31835'] = $key->G1484_C31835;

                $datos[$i]['G1484_C31836'] = $key->G1484_C31836;

                $datos[$i]['G1484_C31837'] = $key->G1484_C31837;

                $datos[$i]['G1484_C31838'] = $key->G1484_C31838;

                $datos[$i]['G1484_C31840'] = $key->G1484_C31840;

                $datos[$i]['G1484_C31841'] = $key->G1484_C31841;

                $datos[$i]['G1484_C31842'] = $key->G1484_C31842;

                $datos[$i]['G1484_C31843'] = $key->G1484_C31843;

                $datos[$i]['G1484_C31845'] = $key->G1484_C31845;

                $datos[$i]['G1484_C31831'] = $key->G1484_C31831;

                $datos[$i]['G1484_C31830'] = $key->G1484_C31830;

                $datos[$i]['G1484_C31832'] = $key->G1484_C31832;

                $datos[$i]['G1484_C31833'] = $key->G1484_C31833;

                $datos[$i]['G1484_C29136'] = $key->G1484_C29136;

                $datos[$i]['G1484_C29137'] = $key->G1484_C29137;

                $datos[$i]['G1484_C29138'] = $key->G1484_C29138;

                $datos[$i]['G1484_C29139'] = $key->G1484_C29139;

                $datos[$i]['G1484_C29140'] = $key->G1484_C29140;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1484";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1484_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1484_ConsInte__b as id,  G1484_C26521 as camp1 , b.LISOPC_Nombre____b as camp2 
                     FROM ".$BaseDatos.".G1484  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1484_C26522 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1484_ConsInte__b as id,  G1484_C26521 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G1484  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1484_C26522 JOIN ".$BaseDatos.".G1484_M".$_POST['muestra']." ON G1484_ConsInte__b = G1484_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1484_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1484_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1484_C26521 LIKE '%".$B."%' OR G1484_C26522 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1484_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1484");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1484_ConsInte__b, G1484_FechaInsercion , G1484_Usuario ,  G1484_CodigoMiembro  , G1484_PoblacionOrigen , G1484_EstadoDiligenciamiento ,  G1484_IdLlamada , G1484_C26521 as principal ,G1484_C26520, a.LISOPC_Nombre____b as G1484_C29133,G1484_C29134, b.LISOPC_Nombre____b as G1484_C29135,G1484_C26523,G1484_C26521, c.LISOPC_Nombre____b as G1484_C26522, d.LISOPC_Nombre____b as G1484_C26518,G1484_C26526,G1484_C26527, e.LISOPC_Nombre____b as G1484_C29075,G1484_C26528,G1484_C26529,G1484_C26530, f.LISOPC_Nombre____b as G1484_C26532, g.LISOPC_Nombre____b as G1484_C26533,G1484_C26534,G1484_C26535,G1484_C26536,G1484_C26537,G1484_C26538,G1484_C26539,G1484_C26540,G1484_C31835,G1484_C31836,G1484_C31837,G1484_C31838,G1484_C31840,G1484_C31841,G1484_C31842,G1484_C31843,G1484_C31845, h.LISOPC_Nombre____b as G1484_C31831,G1484_C31830,G1484_C31832,G1484_C31833,G1484_C29136,G1484_C29137, i.LISOPC_Nombre____b as G1484_C29138, j.LISOPC_Nombre____b as G1484_C29139,G1484_C29140 FROM '.$BaseDatos.'.G1484 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1484_C29133 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1484_C29135 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1484_C26522 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1484_C26518 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1484_C29075 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1484_C26532 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1484_C26533 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1484_C31831 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1484_C29138 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1484_C29139';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1484_C26535)){
                    $hora_a = explode(' ', $fila->G1484_C26535)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1484_C26527)){
                    $hora_b = explode(' ', $fila->G1484_C26527)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1484_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1484_ConsInte__b , ($fila->G1484_C26520) , ($fila->G1484_C29133) , ($fila->G1484_C29134) , ($fila->G1484_C29135) , ($fila->G1484_C26523) , ($fila->G1484_C26521) , ($fila->G1484_C26522) , ($fila->G1484_C26518) , ($fila->G1484_C26526) , $hora_a , ($fila->G1484_C29075) , ($fila->G1484_C26528) , ($fila->G1484_C26529) , ($fila->G1484_C26530) , ($fila->G1484_C26532) , ($fila->G1484_C26533) , explode(' ', $fila->G1484_C26534)[0] , $hora_b , ($fila->G1484_C26536) , ($fila->G1484_C26537) , ($fila->G1484_C26538) , ($fila->G1484_C26539) , ($fila->G1484_C26540) , ($fila->G1484_C31835) , ($fila->G1484_C31836) , ($fila->G1484_C31837) , ($fila->G1484_C31838) , ($fila->G1484_C31840) , ($fila->G1484_C31841) , ($fila->G1484_C31842) , ($fila->G1484_C31843) , ($fila->G1484_C31845) , ($fila->G1484_C31831) , ($fila->G1484_C31830) , ($fila->G1484_C31832) , ($fila->G1484_C31833) , ($fila->G1484_C29136) , ($fila->G1484_C29137) , ($fila->G1484_C29138) , ($fila->G1484_C29139) , ($fila->G1484_C29140) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1484 WHERE G1484_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1484";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1484_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1484_ConsInte__b as id,  G1484_C26521 as camp1 , b.LISOPC_Nombre____b as camp2  FROM '.$BaseDatos.'.G1484 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1484_ConsInte__b as id,  G1484_C26521 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G1484  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1484_C26522 JOIN ".$BaseDatos.".G1484_M".$_POST['muestra']." ON G1484_ConsInte__b = G1484_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1484_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1484_C26521 LIKE "%'.$B.'%" OR G1484_C26522 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1484_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1484 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1484(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1484_C29074"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29074 = '".$_POST["G1484_C29074"]."'";
                $LsqlI .= $separador."G1484_C29074";
                $LsqlV .= $separador."'".$_POST["G1484_C29074"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26520"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26520 = '".$_POST["G1484_C26520"]."'";
                $LsqlI .= $separador."G1484_C26520";
                $LsqlV .= $separador."'".$_POST["G1484_C26520"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29133"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29133 = '".$_POST["G1484_C29133"]."'";
                $LsqlI .= $separador."G1484_C29133";
                $LsqlV .= $separador."'".$_POST["G1484_C29133"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29134"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29134 = '".$_POST["G1484_C29134"]."'";
                $LsqlI .= $separador."G1484_C29134";
                $LsqlV .= $separador."'".$_POST["G1484_C29134"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29135"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29135 = '".$_POST["G1484_C29135"]."'";
                $LsqlI .= $separador."G1484_C29135";
                $LsqlV .= $separador."'".$_POST["G1484_C29135"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26523"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26523 = '".$_POST["G1484_C26523"]."'";
                $LsqlI .= $separador."G1484_C26523";
                $LsqlV .= $separador."'".$_POST["G1484_C26523"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26521"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26521 = '".$_POST["G1484_C26521"]."'";
                $LsqlI .= $separador."G1484_C26521";
                $LsqlV .= $separador."'".$_POST["G1484_C26521"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26522"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26522 = '".$_POST["G1484_C26522"]."'";
                $LsqlI .= $separador."G1484_C26522";
                $LsqlV .= $separador."'".$_POST["G1484_C26522"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26518"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26518 = '".$_POST["G1484_C26518"]."'";
                $LsqlI .= $separador."G1484_C26518";
                $LsqlV .= $separador."'".$_POST["G1484_C26518"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26526"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26526 = '".$_POST["G1484_C26526"]."'";
                $LsqlI .= $separador."G1484_C26526";
                $LsqlV .= $separador."'".$_POST["G1484_C26526"]."'";
                $validar = 1;
            }
             
  
            $G1484_C26527 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1484_C26527"])){   
                if($_POST["G1484_C26527"] != '' && $_POST["G1484_C26527"] != 'undefined' && $_POST["G1484_C26527"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C26527 = "'".$fecha." ".str_replace(' ', '',$_POST["G1484_C26527"])."'";
                    $LsqlU .= $separador." G1484_C26527 = ".$G1484_C26527."";
                    $LsqlI .= $separador." G1484_C26527";
                    $LsqlV .= $separador.$G1484_C26527;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1484_C29075"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29075 = '".$_POST["G1484_C29075"]."'";
                $LsqlI .= $separador."G1484_C29075";
                $LsqlV .= $separador."'".$_POST["G1484_C29075"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26528"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26528 = '".$_POST["G1484_C26528"]."'";
                $LsqlI .= $separador."G1484_C26528";
                $LsqlV .= $separador."'".$_POST["G1484_C26528"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26529"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26529 = '".$_POST["G1484_C26529"]."'";
                $LsqlI .= $separador."G1484_C26529";
                $LsqlV .= $separador."'".$_POST["G1484_C26529"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26530"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26530 = '".$_POST["G1484_C26530"]."'";
                $LsqlI .= $separador."G1484_C26530";
                $LsqlV .= $separador."'".$_POST["G1484_C26530"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29141"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29141 = '".$_POST["G1484_C29141"]."'";
                $LsqlI .= $separador."G1484_C29141";
                $LsqlV .= $separador."'".$_POST["G1484_C29141"]."'";
                $validar = 1;
            }
             
 
            $G1484_C26532 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1484_C26532 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1484_C26532 = ".$G1484_C26532;
                    $LsqlI .= $separador." G1484_C26532";
                    $LsqlV .= $separador.$G1484_C26532;
                    $validar = 1;

                    
                }
            }
 
            $G1484_C26533 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1484_C26533 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1484_C26533 = ".$G1484_C26533;
                    $LsqlI .= $separador." G1484_C26533";
                    $LsqlV .= $separador.$G1484_C26533;
                    $validar = 1;
                }
            }
 
            $G1484_C26534 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1484_C26534 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1484_C26534 = ".$G1484_C26534;
                    $LsqlI .= $separador." G1484_C26534";
                    $LsqlV .= $separador.$G1484_C26534;
                    $validar = 1;
                }
            }
 
            $G1484_C26535 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1484_C26535 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1484_C26535 = ".$G1484_C26535;
                    $LsqlI .= $separador." G1484_C26535";
                    $LsqlV .= $separador.$G1484_C26535;
                    $validar = 1;
                }
            }
 
            $G1484_C26536 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1484_C26536 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1484_C26536 = ".$G1484_C26536;
                    $LsqlI .= $separador." G1484_C26536";
                    $LsqlV .= $separador.$G1484_C26536;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1484_C26537"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26537 = '".$_POST["G1484_C26537"]."'";
                $LsqlI .= $separador."G1484_C26537";
                $LsqlV .= $separador."'".$_POST["G1484_C26537"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26538"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26538 = '".$_POST["G1484_C26538"]."'";
                $LsqlI .= $separador."G1484_C26538";
                $LsqlV .= $separador."'".$_POST["G1484_C26538"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26539"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26539 = '".$_POST["G1484_C26539"]."'";
                $LsqlI .= $separador."G1484_C26539";
                $LsqlV .= $separador."'".$_POST["G1484_C26539"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C26540"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C26540 = '".$_POST["G1484_C26540"]."'";
                $LsqlI .= $separador."G1484_C26540";
                $LsqlV .= $separador."'".$_POST["G1484_C26540"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C31834"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C31834 = '".$_POST["G1484_C31834"]."'";
                $LsqlI .= $separador."G1484_C31834";
                $LsqlV .= $separador."'".$_POST["G1484_C31834"]."'";
                $validar = 1;
            }
             
  
            $G1484_C31835 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31835"])){
                if($_POST["G1484_C31835"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31835 = $_POST["G1484_C31835"];
                    $LsqlU .= $separador." G1484_C31835 = ".$G1484_C31835."";
                    $LsqlI .= $separador." G1484_C31835";
                    $LsqlV .= $separador.$G1484_C31835;
                    $validar = 1;
                }
            }
  
            $G1484_C31836 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31836"])){
                if($_POST["G1484_C31836"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31836 = $_POST["G1484_C31836"];
                    $LsqlU .= $separador." G1484_C31836 = ".$G1484_C31836."";
                    $LsqlI .= $separador." G1484_C31836";
                    $LsqlV .= $separador.$G1484_C31836;
                    $validar = 1;
                }
            }
  
            $G1484_C31837 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31837"])){
                if($_POST["G1484_C31837"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31837 = $_POST["G1484_C31837"];
                    $LsqlU .= $separador." G1484_C31837 = ".$G1484_C31837."";
                    $LsqlI .= $separador." G1484_C31837";
                    $LsqlV .= $separador.$G1484_C31837;
                    $validar = 1;
                }
            }
  
            $G1484_C31838 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31838"])){
                if($_POST["G1484_C31838"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31838 = $_POST["G1484_C31838"];
                    $LsqlU .= $separador." G1484_C31838 = ".$G1484_C31838."";
                    $LsqlI .= $separador." G1484_C31838";
                    $LsqlV .= $separador.$G1484_C31838;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1484_C31839"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C31839 = '".$_POST["G1484_C31839"]."'";
                $LsqlI .= $separador."G1484_C31839";
                $LsqlV .= $separador."'".$_POST["G1484_C31839"]."'";
                $validar = 1;
            }
             
  
            $G1484_C31840 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31840"])){
                if($_POST["G1484_C31840"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31840 = $_POST["G1484_C31840"];
                    $LsqlU .= $separador." G1484_C31840 = ".$G1484_C31840."";
                    $LsqlI .= $separador." G1484_C31840";
                    $LsqlV .= $separador.$G1484_C31840;
                    $validar = 1;
                }
            }
  
            $G1484_C31841 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31841"])){
                if($_POST["G1484_C31841"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31841 = $_POST["G1484_C31841"];
                    $LsqlU .= $separador." G1484_C31841 = ".$G1484_C31841."";
                    $LsqlI .= $separador." G1484_C31841";
                    $LsqlV .= $separador.$G1484_C31841;
                    $validar = 1;
                }
            }
  
            $G1484_C31842 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31842"])){
                if($_POST["G1484_C31842"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31842 = $_POST["G1484_C31842"];
                    $LsqlU .= $separador." G1484_C31842 = ".$G1484_C31842."";
                    $LsqlI .= $separador." G1484_C31842";
                    $LsqlV .= $separador.$G1484_C31842;
                    $validar = 1;
                }
            }
  
            $G1484_C31843 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31843"])){
                if($_POST["G1484_C31843"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31843 = $_POST["G1484_C31843"];
                    $LsqlU .= $separador." G1484_C31843 = ".$G1484_C31843."";
                    $LsqlI .= $separador." G1484_C31843";
                    $LsqlV .= $separador.$G1484_C31843;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1484_C31844"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C31844 = '".$_POST["G1484_C31844"]."'";
                $LsqlI .= $separador."G1484_C31844";
                $LsqlV .= $separador."'".$_POST["G1484_C31844"]."'";
                $validar = 1;
            }
             
  
            $G1484_C31845 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31845"])){
                if($_POST["G1484_C31845"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31845 = $_POST["G1484_C31845"];
                    $LsqlU .= $separador." G1484_C31845 = ".$G1484_C31845."";
                    $LsqlI .= $separador." G1484_C31845";
                    $LsqlV .= $separador.$G1484_C31845;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1484_C31831"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C31831 = '".$_POST["G1484_C31831"]."'";
                $LsqlI .= $separador."G1484_C31831";
                $LsqlV .= $separador."'".$_POST["G1484_C31831"]."'";
                $validar = 1;
            }
             
  
            $G1484_C31830 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1484_C31830"])){
                if($_POST["G1484_C31830"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1484_C31830 = $_POST["G1484_C31830"];
                    $LsqlU .= $separador." G1484_C31830 = ".$G1484_C31830."";
                    $LsqlI .= $separador." G1484_C31830";
                    $LsqlV .= $separador.$G1484_C31830;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1484_C31832"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C31832 = '".$_POST["G1484_C31832"]."'";
                $LsqlI .= $separador."G1484_C31832";
                $LsqlV .= $separador."'".$_POST["G1484_C31832"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C31833"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C31833 = '".$_POST["G1484_C31833"]."'";
                $LsqlI .= $separador."G1484_C31833";
                $LsqlV .= $separador."'".$_POST["G1484_C31833"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29136"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29136 = '".$_POST["G1484_C29136"]."'";
                $LsqlI .= $separador."G1484_C29136";
                $LsqlV .= $separador."'".$_POST["G1484_C29136"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29137"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29137 = '".$_POST["G1484_C29137"]."'";
                $LsqlI .= $separador."G1484_C29137";
                $LsqlV .= $separador."'".$_POST["G1484_C29137"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29138"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29138 = '".$_POST["G1484_C29138"]."'";
                $LsqlI .= $separador."G1484_C29138";
                $LsqlV .= $separador."'".$_POST["G1484_C29138"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29139 = '".$_POST["G1484_C29139"]."'";
                $LsqlI .= $separador."G1484_C29139";
                $LsqlV .= $separador."'".$_POST["G1484_C29139"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1484_C29140"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_C29140 = '".$_POST["G1484_C29140"]."'";
                $LsqlI .= $separador."G1484_C29140";
                $LsqlV .= $separador."'".$_POST["G1484_C29140"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1484_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1484_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1484_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1484_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1484_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1484_Usuario , G1484_FechaInsercion, G1484_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1484_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1484 WHERE G1484_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
