<?php 
    /*
        Document   : index
        Created on : 2020-11-10 16:25:42
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MTgyNA==  
    */
    $url_crud =  "formularios/G1824/G1824_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G1824/G1824_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33638" id="LblG1824_C33638">Buenos días/tardes/noches, Me presento mi nombre es (NOMBRE DEL AGENTE), agente especializado del FONDO NACIONAL DEL TURISMO, tengo el gusto de comunicarme a la empresa (NOMBRE RAZON SOCIAL)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33638" value="" disabled name="G1824_C33638"  placeholder="Buenos días/tardes/noches, Me presento mi nombre es (NOMBRE DEL AGENTE), agente especializado del FONDO NACIONAL DEL TURISMO, tengo el gusto de comunicarme a la empresa (NOMBRE RAZON SOCIAL)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Como le informaba hace un momento. Me comunico del FONDO NACIONAL DEL TURISMO, en este momento nos encontramos realizando una actualización de datos de las empresas, que se encuentran afiladas al FONDO NACIONAL DEL TURISMO y a nuestro programa tarjeta joven.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Para continuar con la llamada Sr/Sra. (NOMBRE RL), Quiero hacerle una consulta tiene alguna pregunta sobre FONDO NACIONAL DEL TURISMO o sobre el programa tarjeta joven?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>¿Continuando Sr/Sra. Confirmo me registra que su nombre es?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Informar lo referente al programa tarjeta joven ver las ayudas la página del FONDO NACIONAL DEL TURISMO, Luego de hacer la aclaración al RL, Confirmarle si tiene otra pregunta. Si es así resolverla con la información que se tiene a disposición y a la mano.</h3>
                            <!-- FIN LIBRETO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33642" id="LblG1824_C33642">Me podría confirmar Sr/Sra. (NOMBRE RL), ¿el nombre de la razón social de la empresa? ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33642" value=""  name="G1824_C33642"  placeholder="Me podría confirmar Sr/Sra. (NOMBRE RL), ¿el nombre de la razón social de la empresa? ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33672" id="LblG1824_C33672">Me podría confirmar Sr/Sra. (NOMBRE RL), ¿Su nombre completo es (DAR NOMBRE DEL RL) Representante legal de la empresa (NOMBRE RAZON SOCIAL) ? ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33672" value=""  name="G1824_C33672"  placeholder="Me podría confirmar Sr/Sra. (NOMBRE RL), ¿Su nombre completo es (DAR NOMBRE DEL RL) Representante legal de la empresa (NOMBRE RAZON SOCIAL) ? ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33644" id="LblG1824_C33644">Le confirmo Sr/Sra. (NOMBRE RL), me registra el numero de NIT de la empresa. ¿Es correcto? ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33644" value=""  name="G1824_C33644"  placeholder="Le confirmo Sr/Sra. (NOMBRE RL), me registra el numero de NIT de la empresa. ¿Es correcto? ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33645" id="LblG1824_C33645">Me podría confirmar Sr/Sra. NOMBRE RL, en qué departamento y municipio, se encuentran ubicados, ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33645" value=""  name="G1824_C33645"  placeholder="Me podría confirmar Sr/Sra. NOMBRE RL, en qué departamento y municipio, se encuentran ubicados, ESPERAR RESPUESTA DEL CLIENTE, (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33646" id="LblG1824_C33646"> Me podría confirmar Sr/Sra. (NOMBRE RL), su número de teléfono de contacto, (CONFIRMAR EL NUMERO DE CONTACTO)  (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33646" value=""  name="G1824_C33646"  placeholder=" Me podría confirmar Sr/Sra. (NOMBRE RL), su número de teléfono de contacto, (CONFIRMAR EL NUMERO DE CONTACTO)  (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33679" id="LblG1824_C33679"> Sr/Sra. (NOMBRE RL) ¿Me puede confirmar por favor su correo electronico?</label>
								<input type="text" class="form-control input-sm" id="G1824_C33679" value=""  name="G1824_C33679"  placeholder=" Sr/Sra. (NOMBRE RL) ¿Me puede confirmar por favor su correo electronico?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33680" id="LblG1824_C33680"> Sr/Sra. (NOMBRE RL) ¿Cuenta con una direccion de correo adicional?</label>
								<input type="text" class="form-control input-sm" id="G1824_C33680" value=""  name="G1824_C33680"  placeholder=" Sr/Sra. (NOMBRE RL) ¿Cuenta con una direccion de correo adicional?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33674" id="LblG1824_C33674">Me podría confirmar Sr/Sra. ¿Cuenta con un numero de telefono adicional suyo o de la empresa?  (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33674" value=""  name="G1824_C33674"  placeholder="Me podría confirmar Sr/Sra. ¿Cuenta con un numero de telefono adicional suyo o de la empresa?  (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33671" id="LblG1824_C33671">Me podría confirmar Sr/Sra. (NOMBRE RL), la dirección del sitio web</label>
								<input type="text" class="form-control input-sm" id="G1824_C33671" value=""  name="G1824_C33671"  placeholder="Me podría confirmar Sr/Sra. (NOMBRE RL), la dirección del sitio web">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33648" id="LblG1824_C33648">Me podría confirmar Sr/Sra. (NOMBRE RL), la dirección de la empresa, (ESPERAR RESPUESTA DEL CLIENTE), (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33648" value=""  name="G1824_C33648"  placeholder="Me podría confirmar Sr/Sra. (NOMBRE RL), la dirección de la empresa, (ESPERAR RESPUESTA DEL CLIENTE), (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33643" id="LblG1824_C33643">Me podría confirmar Sr/Sra. (NOMBRE RL), Me informa como aparecen en redes sociales Pag de Instagram y Facebook de la empresa para nuestro registro, (ESPERAR RESPUESTA DEL CLIENTE), (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33643" value=""  name="G1824_C33643"  placeholder="Me podría confirmar Sr/Sra. (NOMBRE RL), Me informa como aparecen en redes sociales Pag de Instagram y Facebook de la empresa para nuestro registro, (ESPERAR RESPUESTA DEL CLIENTE), (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33647" id="LblG1824_C33647"> Me podría confirmar Sr/Sra. (NOMBRE RL), el nombre de una segunda persona de contacto (ESPERAR RESPUESTA DEL CLIENTE), (REGISTRARLA EN EL FORMULARIO)</label>
								<input type="text" class="form-control input-sm" id="G1824_C33647" value=""  name="G1824_C33647"  placeholder=" Me podría confirmar Sr/Sra. (NOMBRE RL), el nombre de una segunda persona de contacto (ESPERAR RESPUESTA DEL CLIENTE), (REGISTRARLA EN EL FORMULARIO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33678" id="LblG1824_C33678"> Sr/Sra. (NOMBRE RL) ¿Me confirma que cargo tiene la persona de contacto?</label>
								<input type="text" class="form-control input-sm" id="G1824_C33678" value=""  name="G1824_C33678"  placeholder=" Sr/Sra. (NOMBRE RL) ¿Me confirma que cargo tiene la persona de contacto?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33676" id="LblG1824_C33676"> Sr/Sra. (NOMBRE RL) ¿Cual es el numero de la segunda persona de contacto?</label>
								<input type="text" class="form-control input-sm" id="G1824_C33676" value=""  name="G1824_C33676"  placeholder=" Sr/Sra. (NOMBRE RL) ¿Cual es el numero de la segunda persona de contacto?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33677" id="LblG1824_C33677"> Sr/Sra. (NOMBRE RL) Le confirmo la categoria de la empresa es.</label>
								<input type="text" class="form-control input-sm" id="G1824_C33677" value=""  name="G1824_C33677"  placeholder=" Sr/Sra. (NOMBRE RL) Le confirmo la categoria de la empresa es.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1824_C33649" id="LblG1824_C33649">Gracias por haber atendido mi llamada, recuerde que hablo con (NOMBRE DEL AGENTE),agente especializado del FONDO NACIONAL DEL TURISMO, que tenga un excelente día.</label>
								<input type="text" class="form-control input-sm" id="G1824_C33649" value="" disabled name="G1824_C33649"  placeholder="Gracias por haber atendido mi llamada, recuerde que hablo con (NOMBRE DEL AGENTE),agente especializado del FONDO NACIONAL DEL TURISMO, que tenga un excelente día.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G1824/G1824_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G1824_C33629").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G1824_C33630").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               

                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
