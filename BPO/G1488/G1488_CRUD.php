<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1488;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1488
                  SET G1488_C = -201
                  WHERE G1488_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1488 
                    WHERE G1488_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1488_C"]) || $gestion["G1488_C"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1488_C"];
        }

        if (is_null($gestion["G1488_C"]) || $gestion["G1488_C"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1488_C"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1488_FechaInsercion"]."',".$gestion["G1488_Usuario"].",'".$gestion["G1488_C".$P["P"]]."','".$gestion["G1488_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "bpo.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1488 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1488_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1488_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1488_LinkContenido as url FROM ".$BaseDatos.".G1488 WHERE G1488_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1488_ConsInte__b, G1488_FechaInsercion , G1488_Usuario ,  G1488_CodigoMiembro  , G1488_PoblacionOrigen , G1488_EstadoDiligenciamiento ,  G1488_IdLlamada , G1488_C26603 as principal ,G1488_C26603,G1488_C26604,G1488_C26605,G1488_C26606,G1488_C26607,G1488_C26608,G1488_C26609,G1488_C26610,G1488_C26611,G1488_C26612,G1488_C26614,G1488_C26613,G1488_C26615,G1488_C26616,G1488_C26617,G1488_C26618,G1488_C26619,G1488_C26620,G1488_C26621,G1488_C26622,G1488_C26623,G1488_C37446,G1488_C37447,G1488_C37448,G1488_C37449,G1488_C37450,G1488_C37451,G1488_C37452,G1488_C37453,G1488_C37454,G1488_C37455,G1488_C37456,G1488_C37457 FROM '.$BaseDatos.'.G1488 WHERE G1488_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1488_C26603'] = $key->G1488_C26603;

                $datos[$i]['G1488_C26604'] = $key->G1488_C26604;

                $datos[$i]['G1488_C26605'] = $key->G1488_C26605;

                $datos[$i]['G1488_C26606'] = $key->G1488_C26606;

                $datos[$i]['G1488_C26607'] = $key->G1488_C26607;

                $datos[$i]['G1488_C26608'] = $key->G1488_C26608;

                $datos[$i]['G1488_C26609'] = $key->G1488_C26609;

                $datos[$i]['G1488_C26610'] = $key->G1488_C26610;

                $datos[$i]['G1488_C26611'] = $key->G1488_C26611;

                $datos[$i]['G1488_C26612'] = $key->G1488_C26612;

                $datos[$i]['G1488_C26614'] = $key->G1488_C26614;

                $datos[$i]['G1488_C26613'] = $key->G1488_C26613;

                $datos[$i]['G1488_C26615'] = $key->G1488_C26615;

                $datos[$i]['G1488_C26616'] = $key->G1488_C26616;

                $datos[$i]['G1488_C26617'] = explode(' ', $key->G1488_C26617)[0];
  
                $hora = '';
                if(!is_null($key->G1488_C26618)){
                    $hora = explode(' ', $key->G1488_C26618)[1];
                }

                $datos[$i]['G1488_C26618'] = $hora;

                $datos[$i]['G1488_C26619'] = $key->G1488_C26619;

                $datos[$i]['G1488_C26620'] = $key->G1488_C26620;

                $datos[$i]['G1488_C26621'] = $key->G1488_C26621;

                $datos[$i]['G1488_C26622'] = $key->G1488_C26622;

                $datos[$i]['G1488_C26623'] = $key->G1488_C26623;

                $datos[$i]['G1488_C37446'] = $key->G1488_C37446;

                $datos[$i]['G1488_C37447'] = $key->G1488_C37447;

                $datos[$i]['G1488_C37448'] = $key->G1488_C37448;

                $datos[$i]['G1488_C37449'] = $key->G1488_C37449;

                $datos[$i]['G1488_C37450'] = $key->G1488_C37450;

                $datos[$i]['G1488_C37451'] = $key->G1488_C37451;

                $datos[$i]['G1488_C37452'] = $key->G1488_C37452;

                $datos[$i]['G1488_C37453'] = $key->G1488_C37453;

                $datos[$i]['G1488_C37454'] = $key->G1488_C37454;

                $datos[$i]['G1488_C37455'] = $key->G1488_C37455;

                $datos[$i]['G1488_C37456'] = $key->G1488_C37456;

                $datos[$i]['G1488_C37457'] = $key->G1488_C37457;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1488";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1488_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1488_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G1488_C26604 as camp2 
                     FROM ".$BaseDatos.".G1488  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1488_C26603 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1488_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G1488_C26604 as camp2  
                    FROM ".$BaseDatos.".G1488  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1488_C26603 JOIN ".$BaseDatos.".G1488_M".$_POST['muestra']." ON G1488_ConsInte__b = G1488_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1488_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1488_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1488_C26603 LIKE '%".$B."%' OR G1488_C26604 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1488_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1488");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1488_ConsInte__b, G1488_FechaInsercion , G1488_Usuario ,  G1488_CodigoMiembro  , G1488_PoblacionOrigen , G1488_EstadoDiligenciamiento ,  G1488_IdLlamada , G1488_C26603 as principal , a.LISOPC_Nombre____b as G1488_C26603,G1488_C26604,G1488_C26605,G1488_C26606,G1488_C26607,G1488_C26608,G1488_C26609,G1488_C26610, b.LISOPC_Nombre____b as G1488_C26611,G1488_C26612,G1488_C26614,G1488_C26613, c.LISOPC_Nombre____b as G1488_C26615, d.LISOPC_Nombre____b as G1488_C26616,G1488_C26617,G1488_C26618,G1488_C26619,G1488_C26620,G1488_C26621,G1488_C26622,G1488_C26623, e.LISOPC_Nombre____b as G1488_C37446, f.LISOPC_Nombre____b as G1488_C37447, g.LISOPC_Nombre____b as G1488_C37448, h.LISOPC_Nombre____b as G1488_C37449, i.LISOPC_Nombre____b as G1488_C37450, j.LISOPC_Nombre____b as G1488_C37451, k.LISOPC_Nombre____b as G1488_C37452, l.LISOPC_Nombre____b as G1488_C37453, m.LISOPC_Nombre____b as G1488_C37454, n.LISOPC_Nombre____b as G1488_C37455, o.LISOPC_Nombre____b as G1488_C37456, p.LISOPC_Nombre____b as G1488_C37457 FROM '.$BaseDatos.'.G1488 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1488_C26603 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1488_C26611 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1488_C26615 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1488_C26616 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1488_C37446 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1488_C37447 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1488_C37448 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1488_C37449 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1488_C37450 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1488_C37451 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1488_C37452 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G1488_C37453 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G1488_C37454 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G1488_C37455 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G1488_C37456 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G1488_C37457';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1488 WHERE G1488_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1488";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1488_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1488_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G1488_C26604 as camp2  FROM '.$BaseDatos.'.G1488 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1488_ConsInte__b as id,  a.LISOPC_Nombre____b as camp1 , G1488_C26604 as camp2  
                    FROM ".$BaseDatos.".G1488  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b = G1488_C26603 JOIN ".$BaseDatos.".G1488_M".$_POST['muestra']." ON G1488_ConsInte__b = G1488_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1488_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1488_C26603 LIKE "%'.$B.'%" OR G1488_C26604 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1488_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1488 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1488(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1488_C26603"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26603 = '".$_POST["G1488_C26603"]."'";
                $LsqlI .= $separador."G1488_C26603";
                $LsqlV .= $separador."'".$_POST["G1488_C26603"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26604"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26604 = '".$_POST["G1488_C26604"]."'";
                $LsqlI .= $separador."G1488_C26604";
                $LsqlV .= $separador."'".$_POST["G1488_C26604"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26605"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26605 = '".$_POST["G1488_C26605"]."'";
                $LsqlI .= $separador."G1488_C26605";
                $LsqlV .= $separador."'".$_POST["G1488_C26605"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26606"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26606 = '".$_POST["G1488_C26606"]."'";
                $LsqlI .= $separador."G1488_C26606";
                $LsqlV .= $separador."'".$_POST["G1488_C26606"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26607"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26607 = '".$_POST["G1488_C26607"]."'";
                $LsqlI .= $separador."G1488_C26607";
                $LsqlV .= $separador."'".$_POST["G1488_C26607"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26608"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26608 = '".$_POST["G1488_C26608"]."'";
                $LsqlI .= $separador."G1488_C26608";
                $LsqlV .= $separador."'".$_POST["G1488_C26608"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26609"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26609 = '".$_POST["G1488_C26609"]."'";
                $LsqlI .= $separador."G1488_C26609";
                $LsqlV .= $separador."'".$_POST["G1488_C26609"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26610"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26610 = '".$_POST["G1488_C26610"]."'";
                $LsqlI .= $separador."G1488_C26610";
                $LsqlV .= $separador."'".$_POST["G1488_C26610"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26611"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26611 = '".$_POST["G1488_C26611"]."'";
                $LsqlI .= $separador."G1488_C26611";
                $LsqlV .= $separador."'".$_POST["G1488_C26611"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26612"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26612 = '".$_POST["G1488_C26612"]."'";
                $LsqlI .= $separador."G1488_C26612";
                $LsqlV .= $separador."'".$_POST["G1488_C26612"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26614"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26614 = '".$_POST["G1488_C26614"]."'";
                $LsqlI .= $separador."G1488_C26614";
                $LsqlV .= $separador."'".$_POST["G1488_C26614"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26613"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26613 = '".$_POST["G1488_C26613"]."'";
                $LsqlI .= $separador."G1488_C26613";
                $LsqlV .= $separador."'".$_POST["G1488_C26613"]."'";
                $validar = 1;
            }
             
 
            $G1488_C26615 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1488_C26615 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1488_C26615 = ".$G1488_C26615;
                    $LsqlI .= $separador." G1488_C26615";
                    $LsqlV .= $separador.$G1488_C26615;
                    $validar = 1;

                    
                }
            }
 
            $G1488_C26616 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1488_C26616 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1488_C26616 = ".$G1488_C26616;
                    $LsqlI .= $separador." G1488_C26616";
                    $LsqlV .= $separador.$G1488_C26616;
                    $validar = 1;
                }
            }
 
            $G1488_C26617 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1488_C26617 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1488_C26617 = ".$G1488_C26617;
                    $LsqlI .= $separador." G1488_C26617";
                    $LsqlV .= $separador.$G1488_C26617;
                    $validar = 1;
                }
            }
 
            $G1488_C26618 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1488_C26618 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1488_C26618 = ".$G1488_C26618;
                    $LsqlI .= $separador." G1488_C26618";
                    $LsqlV .= $separador.$G1488_C26618;
                    $validar = 1;
                }
            }
 
            $G1488_C26619 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1488_C26619 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1488_C26619 = ".$G1488_C26619;
                    $LsqlI .= $separador." G1488_C26619";
                    $LsqlV .= $separador.$G1488_C26619;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1488_C26620"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26620 = '".$_POST["G1488_C26620"]."'";
                $LsqlI .= $separador."G1488_C26620";
                $LsqlV .= $separador."'".$_POST["G1488_C26620"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26621"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date('Y-m-d H:i:s');

                $LsqlU .= $separador."G1488_C26621 = '".$strFecha_t."'";
                $LsqlI .= $separador."G1488_C26621";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26622"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date('H:i:s');

                $LsqlU .= $separador."G1488_C26622 = '".$strHora_t."'";
                $LsqlI .= $separador."G1488_C26622";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26623"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26623 = '".$_POST["G1488_C26623"]."'";
                $LsqlI .= $separador."G1488_C26623";
                $LsqlV .= $separador."'".$_POST["G1488_C26623"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26624"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26624 = '".$_POST["G1488_C26624"]."'";
                $LsqlI .= $separador."G1488_C26624";
                $LsqlV .= $separador."'".$_POST["G1488_C26624"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C26625"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C26625 = '".$_POST["G1488_C26625"]."'";
                $LsqlI .= $separador."G1488_C26625";
                $LsqlV .= $separador."'".$_POST["G1488_C26625"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37446"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37446 = '".$_POST["G1488_C37446"]."'";
                $LsqlI .= $separador."G1488_C37446";
                $LsqlV .= $separador."'".$_POST["G1488_C37446"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37447"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37447 = '".$_POST["G1488_C37447"]."'";
                $LsqlI .= $separador."G1488_C37447";
                $LsqlV .= $separador."'".$_POST["G1488_C37447"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37448"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37448 = '".$_POST["G1488_C37448"]."'";
                $LsqlI .= $separador."G1488_C37448";
                $LsqlV .= $separador."'".$_POST["G1488_C37448"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37449"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37449 = '".$_POST["G1488_C37449"]."'";
                $LsqlI .= $separador."G1488_C37449";
                $LsqlV .= $separador."'".$_POST["G1488_C37449"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37450"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37450 = '".$_POST["G1488_C37450"]."'";
                $LsqlI .= $separador."G1488_C37450";
                $LsqlV .= $separador."'".$_POST["G1488_C37450"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37451"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37451 = '".$_POST["G1488_C37451"]."'";
                $LsqlI .= $separador."G1488_C37451";
                $LsqlV .= $separador."'".$_POST["G1488_C37451"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37452"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37452 = '".$_POST["G1488_C37452"]."'";
                $LsqlI .= $separador."G1488_C37452";
                $LsqlV .= $separador."'".$_POST["G1488_C37452"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37453"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37453 = '".$_POST["G1488_C37453"]."'";
                $LsqlI .= $separador."G1488_C37453";
                $LsqlV .= $separador."'".$_POST["G1488_C37453"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37454"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37454 = '".$_POST["G1488_C37454"]."'";
                $LsqlI .= $separador."G1488_C37454";
                $LsqlV .= $separador."'".$_POST["G1488_C37454"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37455"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37455 = '".$_POST["G1488_C37455"]."'";
                $LsqlI .= $separador."G1488_C37455";
                $LsqlV .= $separador."'".$_POST["G1488_C37455"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37456"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37456 = '".$_POST["G1488_C37456"]."'";
                $LsqlI .= $separador."G1488_C37456";
                $LsqlV .= $separador."'".$_POST["G1488_C37456"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1488_C37457"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_C37457 = '".$_POST["G1488_C37457"]."'";
                $LsqlI .= $separador."G1488_C37457";
                $LsqlV .= $separador."'".$_POST["G1488_C37457"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1488_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1488_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1488_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1488_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1488_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1488_Usuario , G1488_FechaInsercion, G1488_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1488_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1488 WHERE G1488_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
