<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1140 WHERE G1140_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }

      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1140_ConsInte__b, G1140_FechaInsercion , USUARI_Nombre____b ,G1140_CodigoMiembro,G1140_PoblacionOrigen , G1140_EstadoDiligenciamiento ,  G1140_IdLlamada , G1140_C17614 as principal ,G1140_C20685,G1140_CodigoMiembro,G1140_C17614,G1140_C17615,G1140_C17616,G1140_C20686,G1140_C20687,G1140_C17617,G1140_C17618,G1140_C17619,G1140_C17620,G1140_C20202,G1140_C17621,G1140_C17622,G1140_C17623,G1140_C17624,G1140_C17625,G1140_C20203,G1140_C20204,G1140_C20205,G1140_C20206,G1140_C22662,G1140_C17626,G1140_C17627,G1140_C17628,G1140_C17629,G1140_C17630,G1140_C17631,G1140_C17632,G1140_C17633,G1140_C17634,G1140_C17635,G1140_C17636,G1140_C17637,G1140_C17638,G1140_C20231,G1140_C20232,G1140_C20233,G1140_C20234,G1140_C20235,G1140_C20236 FROM '.$BaseDatos.'.G1140 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1140_Usuario  WHERE G1140_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1140_C20685'] = $key->G1140_C20685;

                $datos[$i]['G1140_CodigoMiembro'] = $key->G1140_CodigoMiembro;

                $datos[$i]['G1140_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1140_C17614'] = $key->G1140_C17614;

                $datos[$i]['G1140_C17615'] = $key->G1140_C17615;

                $datos[$i]['G1140_C17616'] = $key->G1140_C17616;

                $datos[$i]['G1140_C20686'] = $key->G1140_C20686;

                $datos[$i]['G1140_C20687'] = $key->G1140_C20687;

                $datos[$i]['G1140_C17617'] = $key->G1140_C17617;

                $datos[$i]['G1140_C17618'] = $key->G1140_C17618;

                $datos[$i]['G1140_C17619'] = $key->G1140_C17619;

                $datos[$i]['G1140_C17620'] = $key->G1140_C17620;

                $datos[$i]['G1140_C20202'] = $key->G1140_C20202;

                $datos[$i]['G1140_C17621'] = $key->G1140_C17621;

                $datos[$i]['G1140_C17622'] = $key->G1140_C17622;

                $datos[$i]['G1140_C17623'] = $key->G1140_C17623;

                $datos[$i]['G1140_C17624'] = $key->G1140_C17624;

                $datos[$i]['G1140_C17625'] = $key->G1140_C17625;

                $datos[$i]['G1140_C20203'] = $key->G1140_C20203;

                $datos[$i]['G1140_C20204'] = $key->G1140_C20204;

                $datos[$i]['G1140_C20205'] = explode(' ', $key->G1140_C20205)[0];

                $datos[$i]['G1140_C20206'] = explode(' ', $key->G1140_C20206)[0];

                $datos[$i]['G1140_C22662'] = $key->G1140_C22662;

                $datos[$i]['G1140_C17626'] = $key->G1140_C17626;

                $datos[$i]['G1140_C17627'] = $key->G1140_C17627;

                $datos[$i]['G1140_C17628'] = $key->G1140_C17628;

                $datos[$i]['G1140_C17629'] = $key->G1140_C17629;

                $datos[$i]['G1140_C17630'] = explode(' ', $key->G1140_C17630)[0];
  
                $hora = '';
                if(!is_null($key->G1140_C17631)){
                    $hora = explode(' ', $key->G1140_C17631)[1];
                }

                $datos[$i]['G1140_C17632'] = $key->G1140_C17632;

                $datos[$i]['G1140_C17633'] = $key->G1140_C17633;

                $datos[$i]['G1140_C17634'] = $key->G1140_C17634;

                $datos[$i]['G1140_C17635'] = $key->G1140_C17635;

                $datos[$i]['G1140_C17636'] = $key->G1140_C17636;

                $datos[$i]['G1140_C17637'] = $key->G1140_C17637;

                $datos[$i]['G1140_C17638'] = $key->G1140_C17638;

                $datos[$i]['G1140_C20231'] = $key->G1140_C20231;

                $datos[$i]['G1140_C20232'] = $key->G1140_C20232;

                $datos[$i]['G1140_C20233'] = $key->G1140_C20233;

                $datos[$i]['G1140_C20234'] = $key->G1140_C20234;

                $datos[$i]['G1140_C20235'] = $key->G1140_C20235;

                $datos[$i]['G1140_C20236'] = $key->G1140_C20236;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1140_ConsInte__b as id,  G1140_C17614 as camp1 , G1140_C17616 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1140 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1140_C17627  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1140_C17614 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1140_C17616 like '%".$_POST['Busqueda']."%' ) ";
            }
            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1140_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1140_C20685']) && $_POST['G1140_C20685'] != ''){
				$Lsql .= " AND G1140_C20685 LIKE '%". $_POST['G1140_C20685'] ."%' ";
			}
			if(!is_null($_POST['G1140_C17614']) && $_POST['G1140_C17614'] != ''){
				$Lsql .= " AND G1140_C17614 LIKE '%". $_POST['G1140_C17614'] ."%' ";
			}
			if(!is_null($_POST['G1140_C17615']) && $_POST['G1140_C17615'] != ''){
				$Lsql .= " AND G1140_C17615 LIKE '%". $_POST['G1140_C17615'] ."%' ";
			}
			if(!is_null($_POST['G1140_C17617']) && $_POST['G1140_C17617'] != ''){
				$Lsql .= " AND G1140_C17617 LIKE '%". $_POST['G1140_C17617'] ."%' ";
			}

            $Lsql .= " ORDER BY field (G1140_C17627,14317,14318,14319,14320,14321)  LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }else if($key->estado == 'Sin gestión'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = '4';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1140_C20233'])){
            echo '<select class="form-control input-sm"  name="G1140_C20233" id="G1140_C20233">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1140_C20233'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1140_C20233'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1140_C20233'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1140");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1140_ConsInte__b, G1140_FechaInsercion , G1140_Usuario ,  G1140_CodigoMiembro  , G1140_PoblacionOrigen , G1140_EstadoDiligenciamiento ,  G1140_IdLlamada , G1140_C17614 as principal ,G1140_C20685,G1140_C17614,G1140_C17615,G1140_C17616,G1140_C20686,G1140_C20687,G1140_C17617,G1140_C17618,G1140_C17619,G1140_C17620,G1140_C20202,G1140_C17621,G1140_C17622,G1140_C17623,G1140_C17624,G1140_C17625,G1140_C20203, a.LISOPC_Nombre____b as G1140_C20204,G1140_C20205,G1140_C20206,G1140_C22662, b.LISOPC_Nombre____b as G1140_C17626, c.LISOPC_Nombre____b as G1140_C17627,G1140_C17628,G1140_C17629,G1140_C17630,G1140_C17631,G1140_C17632,G1140_C17633,G1140_C17634,G1140_C17635,G1140_C17636, d.LISOPC_Nombre____b as G1140_C17637,G1140_C17638, e.LISOPC_Nombre____b as G1140_C20231, f.LISOPC_Nombre____b as G1140_C20232,G1140_C20234,G1140_C20235,G1140_C20236 FROM '.$BaseDatos.'.G1140 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1140_C20204 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1140_C17626 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1140_C17627 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1140_C17637 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1140_C20231 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1140_C20232 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1140_C20233';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1140_C17631)){
                    $hora_a = explode(' ', $fila->G1140_C17631)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1140_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1140_ConsInte__b , ($fila->G1140_C20685) , ($fila->G1140_C17614) , ($fila->G1140_C17615) , ($fila->G1140_C17616) , ($fila->G1140_C20686) , ($fila->G1140_C20687) , ($fila->G1140_C17617) , ($fila->G1140_C17618) , ($fila->G1140_C17619) , ($fila->G1140_C17620) , ($fila->G1140_C20202) , ($fila->G1140_C17621) , ($fila->G1140_C17622) , ($fila->G1140_C17623) , ($fila->G1140_C17624) , ($fila->G1140_C17625) , ($fila->G1140_C20203) , ($fila->G1140_C20204) , explode(' ', $fila->G1140_C20205)[0] , explode(' ', $fila->G1140_C20206)[0] , ($fila->G1140_C22662) , ($fila->G1140_C17626) , ($fila->G1140_C17627) , ($fila->G1140_C17628) , ($fila->G1140_C17629) , explode(' ', $fila->G1140_C17630)[0] , $hora_a , ($fila->G1140_C17632) , ($fila->G1140_C17633) , ($fila->G1140_C17634) , ($fila->G1140_C17635) , ($fila->G1140_C17636) , ($fila->G1140_C17637) , ($fila->G1140_C17638) , ($fila->G1140_C20231) , ($fila->G1140_C20232) , ($fila->G1140_C20234) , ($fila->G1140_C20235) , ($fila->G1140_C20236) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1140 WHERE G1140_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1140_ConsInte__b as id,  G1140_C17614 as camp1 , G1140_C17616 as camp2 ,G1140_C17627 as estado  FROM '.$BaseDatos.'.G1140 ORDER BY FIELD(G1140_C17627,14317,14318,14319,14320,14321), G1140_FechaInsercion DESC  LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){

                $color = 'color:green;';
                $strIconoBackOffice = 'Cerrada';
                if($obj->estado == '14318' || $obj->estado == '14319'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14321'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }else if($obj->estado == '14317'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1140 SET "; 
  
            if(isset($_POST["G1140_C20685"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20685 = '".$_POST["G1140_C20685"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17614"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17614 = '".$_POST["G1140_C17614"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17615"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17615 = '".$_POST["G1140_C17615"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17616"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17616 = '".$_POST["G1140_C17616"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C20686"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20686 = '".$_POST["G1140_C20686"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C20687"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20687 = '".$_POST["G1140_C20687"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17617"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17617 = '".$_POST["G1140_C17617"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17618"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17618 = '".$_POST["G1140_C17618"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17619"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17619 = '".$_POST["G1140_C17619"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17620"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17620 = '".$_POST["G1140_C17620"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C20202"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20202 = '".$_POST["G1140_C20202"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17621"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17621 = '".$_POST["G1140_C17621"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17622"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17622 = '".$_POST["G1140_C17622"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17623"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17623 = '".$_POST["G1140_C17623"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17624"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17624 = '".$_POST["G1140_C17624"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17625"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17625 = '".$_POST["G1140_C17625"]."'";
                $validar = 1;
            }
             
  
            $G1140_C20203 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C20203"])){
                if($_POST["G1140_C20203"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20203 = $_POST["G1140_C20203"];
                    $LsqlU .= $separador." G1140_C20203 = ".$G1140_C20203."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1140_C20204"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20204 = '".$_POST["G1140_C20204"]."'";
                $validar = 1;
            }
             
 
            $G1140_C20205 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1140_C20205"])){    
                if($_POST["G1140_C20205"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1140_C20205"]);
                    if(count($tieneHora) > 1){
                    	$G1140_C20205 = "'".$_POST["G1140_C20205"]."'";
                    }else{
                    	$G1140_C20205 = "'".str_replace(' ', '',$_POST["G1140_C20205"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1140_C20205 = ".$G1140_C20205;
                    $validar = 1;
                }
            }
 
            $G1140_C20206 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1140_C20206"])){    
                if($_POST["G1140_C20206"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1140_C20206"]);
                    if(count($tieneHora) > 1){
                    	$G1140_C20206 = "'".$_POST["G1140_C20206"]."'";
                    }else{
                    	$G1140_C20206 = "'".str_replace(' ', '',$_POST["G1140_C20206"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1140_C20206 = ".$G1140_C20206;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1140_C22662"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C22662 = '".$_POST["G1140_C22662"]."'";
                $validar = 1;
            }
             
 
            $G1140_C17626 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1140_C17626 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1140_C17626 = ".$G1140_C17626;
                    $validar = 1;

                    
                }
            }
 
            $G1140_C17627 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1140_C17627 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1140_C17627 = ".$G1140_C17627;
                    $validar = 1;
                }
            }
  
            $G1140_C17628 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C17628"])){
                if($_POST["G1140_C17628"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C17628 = $_POST["G1140_C17628"];
                    $LsqlU .= $separador." G1140_C17628 = ".$G1140_C17628."";
                    $validar = 1;
                }
            }
  
            $G1140_C17629 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C17629"])){
                if($_POST["G1140_C17629"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C17629 = $_POST["G1140_C17629"];
                    $LsqlU .= $separador." G1140_C17629 = ".$G1140_C17629."";
                    $validar = 1;
                }
            }
 
            $G1140_C17632 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1140_C17632 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1140_C17632 = ".$G1140_C17632;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1140_C17633"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17633 = '".$_POST["G1140_C17633"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17634"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17634 = '".$_POST["G1140_C17634"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17635"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17635 = '".$_POST["G1140_C17635"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17636"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17636 = '".$_POST["G1140_C17636"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17637 = '".$_POST["G1140_C17637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C17638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C17638 = '".$_POST["G1140_C17638"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C20231"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20231 = '".$_POST["G1140_C20231"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C20232"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20232 = '".$_POST["G1140_C20232"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1140_C20233"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20233 = '".$_POST["G1140_C20233"]."'";
                $validar = 1;
            }
             
  
            $G1140_C20234 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C20234"])){
                if($_POST["G1140_C20234"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20234 = $_POST["G1140_C20234"];
                    $LsqlU .= $separador." G1140_C20234 = ".$G1140_C20234."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1140_C20235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_C20235 = '".$_POST["G1140_C20235"]."'";
                $validar = 1;
            }
             
  
            $G1140_C20236 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1140_C20236"])){
                if($_POST["G1140_C20236"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1140_C20236 = $_POST["G1140_C20236"];
                    $LsqlU .= $separador." G1140_C20236 = ".$G1140_C20236."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1140_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
              if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1140_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }
			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1140_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
