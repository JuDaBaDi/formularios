<?php

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
        
    //Inserciones o actualizaciones

    if(isset($_POST['getListaHija'])){
        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }
        
    if(isset($_POST["oper"])){
            $str_Lsql  = '';

            $validar = 0;
            $str_LsqlU = "UPDATE ".$BaseDatos.".G1140 SET "; 
            $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1140( G1140_FechaInsercion ,";
            $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G1140_C17626"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1140_C17626 = '".$_POST["G1140_C17626"]."'";
            $str_LsqlI .= $separador."G1140_C17626";
            $str_LsqlV .= $separador."'".$_POST["G1140_C17626"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1140_C17627"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1140_C17627 = '".$_POST["G1140_C17627"]."'";
            $str_LsqlI .= $separador."G1140_C17627";
            $str_LsqlV .= $separador."'".$_POST["G1140_C17627"]."'";
            $validar = 1;
        }
         
  
        $G1140_C17628 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1140_C17628"])){
            if($_POST["G1140_C17628"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1140_C17628 = $_POST["G1140_C17628"];
                $G1140_C17628 = str_replace(".", "", $_POST["G1140_C17628"]);
                $G1140_C17628 =  str_replace(",", ".", $G1140_C17628);
                $str_LsqlU .= $separador." G1140_C17628 = '".$G1140_C17628."'";
                $str_LsqlI .= $separador." G1140_C17628";
                $str_LsqlV .= $separador."'".$G1140_C17628."'";
                $validar = 1;
            }
        }
  
        $G1140_C17629 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1140_C17629"])){
            if($_POST["G1140_C17629"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1140_C17629 = $_POST["G1140_C17629"];
                $G1140_C17629 = str_replace(".", "", $_POST["G1140_C17629"]);
                $G1140_C17629 =  str_replace(",", ".", $G1140_C17629);
                $str_LsqlU .= $separador." G1140_C17629 = '".$G1140_C17629."'";
                $str_LsqlI .= $separador." G1140_C17629";
                $str_LsqlV .= $separador."'".$G1140_C17629."'";
                $validar = 1;
            }
        }
 
        $G1140_C17630 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1140_C17630"])){    
            if($_POST["G1140_C17630"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1140_C17630 = "'".str_replace(' ', '',$_POST["G1140_C17630"])." 00:00:00'";
                $str_LsqlU .= $separador." G1140_C17630 = ".$G1140_C17630;
                $str_LsqlI .= $separador." G1140_C17630";
                $str_LsqlV .= $separador.$G1140_C17630;
                $validar = 1;
            }
        }
  
        $G1140_C17631 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1140_C17631"])){   
            if($_POST["G1140_C17631"] != '' && $_POST["G1140_C17631"] != 'undefined' && $_POST["G1140_C17631"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1140_C17631 = "'".$fecha." ".str_replace(' ', '',$_POST["G1140_C17631"])."'";
                $str_LsqlU .= $separador." G1140_C17631 = ".$G1140_C17631."";
                $str_LsqlI .= $separador." G1140_C17631";
                $str_LsqlV .= $separador.$G1140_C17631;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1140_C17632"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1140_C17632 = '".$_POST["G1140_C17632"]."'";
            $str_LsqlI .= $separador."G1140_C17632";
            $str_LsqlV .= $separador."'".$_POST["G1140_C17632"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1140_C17637"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1140_C17637 = '".$_POST["G1140_C17637"]."'";
            $str_LsqlI .= $separador."G1140_C17637";
            $str_LsqlV .= $separador."'".$_POST["G1140_C17637"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1140_C17638"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1140_C17638 = '".$_POST["G1140_C17638"]."'";
            $str_LsqlI .= $separador."G1140_C17638";
            $str_LsqlV .= $separador."'".$_POST["G1140_C17638"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1140_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1140_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1140_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //Si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;
                //ahora toca ver lo de la muestra asi que toca ver que pasa 
                /* primero buscamos la campaña que nos esta llegando */
                $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana"];

                //echo $Lsql_Campan;

                $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
                $datoCampan = $res_Lsql_Campan->fetch_array();
                $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
                $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
                $int_CAMPAN_ActPo = $datoCampan['CAMPAN_ActPobGui_b'];


                if($int_CAMPAN_ActPo == '-1'){
                    /* toca hacer actualizacion desde Script */
                    
                    $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana"];
                    $resultcampSql = $mysqli->query($campSql);
                    $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
                    $i=0;
                    while($key = $resultcampSql->fetch_object()){

                        if($i == 0){
                            $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }else{
                            $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }
                        $i++;
                    } 
                    $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$ultimoResgistroInsertado.' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
                    //echo "Esta ".$Lsql;
                    if($mysqli->query($Lsql) === TRUE ){

                    }else{
                        echo "NO sE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
                    }
                }


                //Ahora toca actualizar la muestra
                $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 3, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = '-9' , 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".date('Y-m-d H:i:s')."', 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = 3,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = 'No desea participar',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = NULL,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = '-9',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b ='".date('Y-m-d H:i:s')."'";
                $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_POST['id'];
                // echo $MuestraSql;
                if($mysqli->query($MuestraSql) === true){

                }else{
                    echo "Error insertando la muesta => ".$mysqli->error;
                }
                
                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTE0MA==&result=1');

            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
