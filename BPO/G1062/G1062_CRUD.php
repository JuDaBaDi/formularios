<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1062_ConsInte__b, G1062_FechaInsercion , G1062_Usuario ,  G1062_CodigoMiembro  , G1062_PoblacionOrigen , G1062_EstadoDiligenciamiento ,  G1062_IdLlamada , G1062_C15286 as principal ,G1062_C15281,G1062_C15282,G1062_C15283,G1062_C15284,G1062_C15285,G1062_C15286,G1062_C15287,G1062_C15288,G1062_C15289,G1062_C15290,G1062_C15291,G1062_C15292,G1062_C15293,G1062_C15294,G1062_C15295,G1062_C15297,G1062_C15298,G1062_C15299,G1062_C15300,G1062_C15301,G1062_C15302,G1062_C15303,G1062_C15304,G1062_C15305,G1062_C15371,G1062_C15372,G1062_C15373, G1062_C38970 FROM '.$BaseDatos.'.G1062 WHERE G1062_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1062_C15281'] = $key->G1062_C15281;

                $datos[$i]['G1062_C15282'] = explode(' ', $key->G1062_C15282)[0];

                $datos[$i]['G1062_C15283'] = explode(' ', $key->G1062_C15283)[0];

                $datos[$i]['G1062_C15284'] = $key->G1062_C15284;

                $datos[$i]['G1062_C15285'] = $key->G1062_C15285;

                $datos[$i]['G1062_C15286'] = $key->G1062_C15286;

                $datos[$i]['G1062_C15287'] = $key->G1062_C15287;

                $datos[$i]['G1062_C15288'] = $key->G1062_C15288;

                $datos[$i]['G1062_C15289'] = $key->G1062_C15289;

                $datos[$i]['G1062_C15290'] = $key->G1062_C15290;

                $datos[$i]['G1062_C15291'] = $key->G1062_C15291;

                $datos[$i]['G1062_C15292'] = $key->G1062_C15292;

                $datos[$i]['G1062_C15293'] = $key->G1062_C15293;

                $datos[$i]['G1062_C15294'] = $key->G1062_C15294;

                $datos[$i]['G1062_C15295'] = $key->G1062_C15295;

                $datos[$i]['G1062_C15297'] = $key->G1062_C15297;

                $datos[$i]['G1062_C15298'] = $key->G1062_C15298;

                $datos[$i]['G1062_C15299'] = explode(' ', $key->G1062_C15299)[0];
  
                $hora = '';
                if(!is_null($key->G1062_C15300)){
                    $hora = explode(' ', $key->G1062_C15300)[1];
                }

                $datos[$i]['G1062_C15300'] = $hora;

                $datos[$i]['G1062_C15301'] = $key->G1062_C15301;

                $datos[$i]['G1062_C15302'] = $key->G1062_C15302;

                $datos[$i]['G1062_C15303'] = $key->G1062_C15303;

                $datos[$i]['G1062_C15304'] = $key->G1062_C15304;

                $datos[$i]['G1062_C15305'] = $key->G1062_C15305;

                $datos[$i]['G1062_C15371'] = $key->G1062_C15371;

                $datos[$i]['G1062_C15372'] = $key->G1062_C15372;

                $datos[$i]['G1062_C15373'] = $key->G1062_C15373;

                $datos[$i]['G1062_C38970'] = $key->G1062_C38970;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1062_ConsInte__b as id,  G1062_C15285 as camp2 , G1062_C15286 as camp1 ";
            $Lsql .= " FROM ".$BaseDatos.".G1062 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1062_C15285 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1062_C15286 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1062_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1062");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1062_ConsInte__b, G1062_FechaInsercion , G1062_Usuario ,  G1062_CodigoMiembro  , G1062_PoblacionOrigen , G1062_EstadoDiligenciamiento ,  G1062_IdLlamada , G1062_C15286 as principal ,G1062_C15281,G1062_C15282,G1062_C15283,G1062_C15284,G1062_C15285,G1062_C15286,G1062_C15287,G1062_C15288,G1062_C15289,G1062_C15290,G1062_C15291,G1062_C15292,G1062_C15293,G1062_C15294,G1062_C15295, a.LISOPC_Nombre____b as G1062_C15297, b.LISOPC_Nombre____b as G1062_C15298,G1062_C15299,G1062_C15300,G1062_C15301,G1062_C15302,G1062_C15303,G1062_C15304,G1062_C15305, c.LISOPC_Nombre____b as G1062_C15371, d.LISOPC_Nombre____b as G1062_C15372, e.LISOPC_Nombre____b as G1062_C15373 FROM '.$BaseDatos.'.G1062 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1062_C15297 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1062_C15298 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1062_C15371 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1062_C15372 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1062_C15373';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1062_C15300)){
                    $hora_a = explode(' ', $fila->G1062_C15300)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1062_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1062_ConsInte__b , ($fila->G1062_C15281) , explode(' ', $fila->G1062_C15282)[0] , explode(' ', $fila->G1062_C15283)[0] , ($fila->G1062_C15284) , ($fila->G1062_C15285) , ($fila->G1062_C15286) , ($fila->G1062_C15287) , ($fila->G1062_C15288) , ($fila->G1062_C15289) , ($fila->G1062_C15290) , ($fila->G1062_C15291) , ($fila->G1062_C15292) , ($fila->G1062_C15293) , ($fila->G1062_C15294) , ($fila->G1062_C15295) , ($fila->G1062_C15297) , ($fila->G1062_C15298) , explode(' ', $fila->G1062_C15299)[0] , $hora_a , ($fila->G1062_C15301) , ($fila->G1062_C15302) , ($fila->G1062_C15303) , ($fila->G1062_C15304) , ($fila->G1062_C15305) , ($fila->G1062_C15371) , ($fila->G1062_C15372) , ($fila->G1062_C15373) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1062 WHERE G1062_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    //echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1062_ConsInte__b as id,  G1062_C15285 as camp2 , G1062_C15286 as camp1  FROM '.$BaseDatos.'.G1062 ORDER BY G1062_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1062 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1062(";
            $LsqlUfri = "UPDATE ".$BaseDatos.".G1059 SET ";
            $LsqlUato = "UPDATE ".$BaseDatos.".G1007 SET "; 
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1062_C15281"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15281 = '".$_POST["G1062_C15281"]."'";
                $LsqlI .= $separador."G1062_C15281";
                $LsqlV .= $separador."'".$_POST["G1062_C15281"]."'";
                $validar = 1;
            }
             
 
            $G1062_C15282 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1062_C15282"])){    
                if($_POST["G1062_C15282"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1062_C15282"]);
                    if(count($tieneHora) > 1){
                    	$G1062_C15282 = "'".$_POST["G1062_C15282"]."'";
                    }else{
                    	$G1062_C15282 = "'".str_replace(' ', '',$_POST["G1062_C15282"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1062_C15282 = ".$G1062_C15282;
                    $LsqlI .= $separador." G1062_C15282";
                    $LsqlV .= $separador.$G1062_C15282;
                    $validar = 1;
                }
            }
 
            $G1062_C15283 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1062_C15283"])){    
                if($_POST["G1062_C15283"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1062_C15283"]);
                    if(count($tieneHora) > 1){
                    	$G1062_C15283 = "'".$_POST["G1062_C15283"]."'";
                    }else{
                    	$G1062_C15283 = "'".str_replace(' ', '',$_POST["G1062_C15283"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1062_C15283 = ".$G1062_C15283;
                    $LsqlI .= $separador." G1062_C15283";
                    $LsqlV .= $separador.$G1062_C15283;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1062_C15284"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15284 = '".$_POST["G1062_C15284"]."'";
                $LsqlI .= $separador."G1062_C15284";
                $LsqlV .= $separador."'".$_POST["G1062_C15284"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15285"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15285 = '".$_POST["G1062_C15285"]."'";
                $LsqlI .= $separador."G1062_C15285";
                $LsqlV .= $separador."'".$_POST["G1062_C15285"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15286"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15286 = '".$_POST["G1062_C15286"]."'";
                $LsqlI .= $separador."G1062_C15286";
                $LsqlV .= $separador."'".$_POST["G1062_C15286"]."'";
                $LsqlUfri .= "G1059_C15217 = '".$_POST["G1062_C15286"]."'";
                $LsqlUato .= "G1007_C14027 = '".$_POST["G1062_C15286"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15287"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15287 = '".$_POST["G1062_C15287"]."'";
                $LsqlI .= $separador."G1062_C15287";
                $LsqlV .= $separador."'".$_POST["G1062_C15287"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15288"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15288 = '".$_POST["G1062_C15288"]."'";
                $LsqlI .= $separador."G1062_C15288";
                $LsqlV .= $separador."'".$_POST["G1062_C15288"]."'";
                $LsqlUfri .= $separador."G1059_C15218 = '".$_POST["G1062_C15288"]."'";
                $LsqlUato .= $separador."G1007_C14028 = '".$_POST["G1062_C15288"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15289"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15289 = '".$_POST["G1062_C15289"]."'";
                $LsqlI .= $separador."G1062_C15289";
                $LsqlV .= $separador."'".$_POST["G1062_C15289"]."'";
                $LsqlUfri .= $separador."G1059_C15219 = '".$_POST["G1062_C15289"]."'";
                $LsqlUato .= $separador."G1007_C14029 = '".$_POST["G1062_C15289"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15290"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15290 = '".$_POST["G1062_C15290"]."'";
                $LsqlI .= $separador."G1062_C15290";
                $LsqlV .= $separador."'".$_POST["G1062_C15290"]."'";
                $LsqlUfri .= $separador."G1059_C15220 = '".$_POST["G1062_C15290"]."'";
                $LsqlUato .= $separador."G1007_C14030 = '".$_POST["G1062_C15290"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15291"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15291 = '".$_POST["G1062_C15291"]."'";
                $LsqlI .= $separador."G1062_C15291";
                $LsqlV .= $separador."'".$_POST["G1062_C15291"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15292"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15292 = '".$_POST["G1062_C15292"]."'";
                $LsqlI .= $separador."G1062_C15292";
                $LsqlV .= $separador."'".$_POST["G1062_C15292"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15293"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15293 = '".$_POST["G1062_C15293"]."'";
                $LsqlI .= $separador."G1062_C15293";
                $LsqlV .= $separador."'".$_POST["G1062_C15293"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15294"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15294 = '".$_POST["G1062_C15294"]."'";
                $LsqlI .= $separador."G1062_C15294";
                $LsqlV .= $separador."'".$_POST["G1062_C15294"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15295 = '".$_POST["G1062_C15295"]."'";
                $LsqlI .= $separador."G1062_C15295";
                $LsqlV .= $separador."'".$_POST["G1062_C15295"]."'";
                $validar = 1;
            }
             
 
            $G1062_C15297 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1062_C15297 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1062_C15297 = ".$G1062_C15297;
                    $LsqlI .= $separador." G1062_C15297";
                    $LsqlV .= $separador.$G1062_C15297;
                    $validar = 1;

                    
                }
            }
 
            $G1062_C15298 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1062_C15298 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1062_C15298 = ".$G1062_C15298;
                    $LsqlI .= $separador." G1062_C15298";
                    $LsqlV .= $separador.$G1062_C15298;
                    $validar = 1;
                }
            }
 
            $G1062_C15299 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1062_C15299 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1062_C15299 = ".$G1062_C15299;
                    $LsqlI .= $separador." G1062_C15299";
                    $LsqlV .= $separador.$G1062_C15299;
                    $validar = 1;
                }
            }
 
            $G1062_C15300 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1062_C15300 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1062_C15300 = ".$G1062_C15300;
                    $LsqlI .= $separador." G1062_C15300";
                    $LsqlV .= $separador.$G1062_C15300;
                    $validar = 1;
                }
            }
 
            $G1062_C15301 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1062_C15301 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1062_C15301 = ".$G1062_C15301;
                    $LsqlI .= $separador." G1062_C15301";
                    $LsqlV .= $separador.$G1062_C15301;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1062_C15302"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15302 = '".$_POST["G1062_C15302"]."'";
                $LsqlI .= $separador."G1062_C15302";
                $LsqlV .= $separador."'".$_POST["G1062_C15302"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15303"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15303 = '".$_POST["G1062_C15303"]."'";
                $LsqlI .= $separador."G1062_C15303";
                $LsqlV .= $separador."'".$_POST["G1062_C15303"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15304"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15304 = '".$_POST["G1062_C15304"]."'";
                $LsqlI .= $separador."G1062_C15304";
                $LsqlV .= $separador."'".$_POST["G1062_C15304"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15305"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15305 = '".$_POST["G1062_C15305"]."'";
                $LsqlI .= $separador."G1062_C15305";
                $LsqlV .= $separador."'".$_POST["G1062_C15305"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15306"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15306 = '".$_POST["G1062_C15306"]."'";
                $LsqlI .= $separador."G1062_C15306";
                $LsqlV .= $separador."'".$_POST["G1062_C15306"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15307"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15307 = '".$_POST["G1062_C15307"]."'";
                $LsqlI .= $separador."G1062_C15307";
                $LsqlV .= $separador."'".$_POST["G1062_C15307"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15335"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15335 = '".$_POST["G1062_C15335"]."'";
                $LsqlI .= $separador."G1062_C15335";
                $LsqlV .= $separador."'".$_POST["G1062_C15335"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15336"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15336 = '".$_POST["G1062_C15336"]."'";
                $LsqlI .= $separador."G1062_C15336";
                $LsqlV .= $separador."'".$_POST["G1062_C15336"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15337"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15337 = '".$_POST["G1062_C15337"]."'";
                $LsqlI .= $separador."G1062_C15337";
                $LsqlV .= $separador."'".$_POST["G1062_C15337"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15338"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15338 = '".$_POST["G1062_C15338"]."'";
                $LsqlI .= $separador."G1062_C15338";
                $LsqlV .= $separador."'".$_POST["G1062_C15338"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15339"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15339 = '".$_POST["G1062_C15339"]."'";
                $LsqlI .= $separador."G1062_C15339";
                $LsqlV .= $separador."'".$_POST["G1062_C15339"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15340"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15340 = '".$_POST["G1062_C15340"]."'";
                $LsqlI .= $separador."G1062_C15340";
                $LsqlV .= $separador."'".$_POST["G1062_C15340"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15341"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15341 = '".$_POST["G1062_C15341"]."'";
                $LsqlI .= $separador."G1062_C15341";
                $LsqlV .= $separador."'".$_POST["G1062_C15341"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15371"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15371 = '".$_POST["G1062_C15371"]."'";
                $LsqlI .= $separador."G1062_C15371";
                $LsqlV .= $separador."'".$_POST["G1062_C15371"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15372"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15372 = '".$_POST["G1062_C15372"]."'";
                $LsqlI .= $separador."G1062_C15372";
                $LsqlV .= $separador."'".$_POST["G1062_C15372"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1062_C15373"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C15373 = '".$_POST["G1062_C15373"]."'";
                $LsqlI .= $separador."G1062_C15373";
                $LsqlV .= $separador."'".$_POST["G1062_C15373"]."'";
                $validar = 1;
            }

            if(isset($_POST["G1062_C38970"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_C38970 = '".$_POST["G1062_C38970"]."'";
                $LsqlI .= $separador."G1062_C38970";
                $LsqlV .= $separador."'".$_POST["G1062_C38970"]."'";
                $validar = 1;
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1062_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1062_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1062_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1062_Usuario , G1062_FechaInsercion, G1062_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1062_ConsInte__b =".$_POST["id"]; 
                    if(isset($_POST["G1062_C15285"]) && $_POST["G1062_C15285"]!=0 && $_POST["G1062_C15285"]!=""){
                        $LsqlUpfrios = $LsqlUfri." WHERE G1059_C15216 = ".$_POST["G1062_C15285"];
                        $LsqlUpatorados = $LsqlUato." WHERE G1007_C14026 = ".$_POST["G1062_C15285"];
                        $upFri = $mysqli->query($LsqlUpfrios);
                        $upAto = $mysqli->query($LsqlUpatorados);
                    }
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1062 WHERE G1062_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'edit' ){

                        if (isset($_POST['TipNoEF']) && isset($_GET['CodigoMiembro'])) {

                            if ($_POST['TipNoEF'] == 2) {

                                $strSQLAgenda_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G1061_M571 WHERE G1061_M571_CoInMiPo__b = ".$_GET['CodigoMiembro'];

                                $intCantidad_t = $mysqli->query($strSQLAgenda_t)->fetch_object()->cantidad;

                                if ($intCantidad_t == 0) {

                                    $mysqli->query("INSERT INTO ".$BaseDatos.".G1061_M571 (G1061_M571_CoInMiPo__b,G1061_M571_Activo____b,G1061_M571_ConIntUsu_b) VALUES ('".$_GET['CodigoMiembro']."',-1,".$_GET["usuario"].")");

                                }

                            }

                        }

                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }


                        //JDBD - Insertamos la gestion en el subformulario.
                            $strSQLInsertG1101_t = "INSERT INTO ".$BaseDatos.".G1101 (G1101_C16729,G1101_C16719,G1101_C16716,G1101_C16723,G1101_C16722,G1101_C30105,G1101_C16715,G1101_C16717,G1101_C16721,G1101_C16720,G1101_C16718,G1101_C38969,G1101_C16730) SELECT G1062_C15373,G1062_C15372,G1062_C15301,G1062_C15285,G1062_C15300,G1062_FechaInsercion,G1062_C15371,G1062_FechaInsercion,G1062_C15299,'CADUCADOS' AS campana,G1062_C15302, G1062_C38970, '".$UltimoID."' AS id_dy FROM ".$BaseDatos.".G1062 WHERE G1062_ConsInte__b = ".$UltimoID;

                            $mysqli->query($strSQLInsertG1101_t);

                        //JDBD - Insertamos en CADUCADOS lo que no tengamos de FRIOS.
                            $strSQLInsertarBDFrios_t = "INSERT INTO ".$BaseDatos.".G1061 (G1061_C15266,G1061_C15267,G1061_C15269,G1061_C15270,G1061_C15271,G1061_C15273,G1061_C15272,G1061_C15268,G1061_C15274,G1061_C15276,G1061_C15278,G1061_C15279,G1061_C15280,G1061_C15263,G1061_C15262,G1061_C15368,G1061_C15369,G1061_C15370,G1061_PoblacionOrigen) SELECT G1059_C15216,G1059_C15217,G1059_C15218,G1059_C15219,G1059_C15220,G1059_C15221,G1059_C15222,G1059_C15223,G1059_C15224,G1059_C15225,G1059_C15230,G1059_C15231,G1059_C15232,G1059_C15258,G1059_C15259,G1059_C15374,G1059_C15375,G1059_C15376,IFNULL(G1059_PoblacionOrigen, 1059) FROM ".$BaseDatos.".G1059 LEFT JOIN ".$BaseDatos.".G1061 ON G1059_C15216 = G1061_C15266 WHERE G1061_C15266 IS NULL AND (NOT (G1059_C15216 IS NULL))";

                            $mysqli->query($strSQLInsertarBDFrios_t);

                        //JDBD - Insertamos en CADUCADOS lo que no tengamos de ATORADOS.
                            $strSQLInsertarBDAtorados_t = "INSERT INTO ".$BaseDatos.".G1061 (G1061_C15266,G1061_C15267,G1061_C15269,G1061_C15270,G1061_C15271,G1061_C15273,G1061_C15272,G1061_C15268,G1061_C15274,G1061_C15278,G1061_C15279,G1061_C15263,G1061_C15262,G1061_C15368,G1061_C15369,G1061_C15370,G1061_PoblacionOrigen) SELECT G1007_C14026,G1007_C14027,G1007_C14028,G1007_C14029,G1007_C14030,G1007_C14024,G1007_C14023,G1007_C14025,G1007_C14031,G1007_C14033,G1007_C14034,G1007_C14020,G1007_C14021,G1007_C15540,G1007_C15541,G1007_C15542,IFNULL(G1007_PoblacionOrigen,1007) FROM ".$BaseDatos.".G1007 LEFT JOIN ".$BaseDatos.".G1061 ON G1007_C14026 = G1061_C15266 WHERE G1007_C14026 IS NULL AND (NOT (G1007_C14026 IS NULL))";

                            $mysqli->query($strSQLInsertarBDAtorados_t);
                            
                        echo $UltimoID;

                    }else{

                        $UltimoID = $mysqli->insert_id;

                		echo $UltimoID;    		
                	}

                	

                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        if (strlen($numero)>0) {

            $SQL = "SELECT G1101_ConsInte__b, G1101_C16723, b.LISOPC_Nombre____b as  G1101_C16715, c.LISOPC_Nombre____b as  G1101_C16719, d.LISOPC_Nombre____b as  G1101_C16729, e.LISOPC_Nombre____b as  G1101_C38969, G1101_C16717, G1101_C16718, G1101_C16716, G1101_C16720, G1101_C30105, G1101_C16721, G1101_C16722, G1101_C16730 FROM ".$BaseDatos.".G1101  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G1101_C16715 LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G1101_C16719 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G1101_C16729 LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b = G1101_C38969 ";

            $SQL .= " WHERE G1101_C16723 = '".$numero."'"; 

            $SQL .= " ORDER BY G1101_C16717 DESC";

            // echo $SQL;
            if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
                header("Content-type: application/xhtml+xml;charset=utf-8"); 
            } else { 
                header("Content-type: text/xml;charset=utf-8"); 
            } 

            $et = ">"; 
            echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
            echo "<rows>"; // be sure to put text data in CDATA
            $result = $mysqli->query($SQL);
            while( $fila = $result->fetch_object() ) {
                echo "<row asin='".$fila->G1101_ConsInte__b."'>"; 
                echo "<cell>". ($fila->G1101_ConsInte__b)."</cell>"; 
                

                echo "<cell>". ($fila->G1101_C16723)."</cell>";

                echo "<cell>". ($fila->G1101_C16715)."</cell>";

                echo "<cell>". ($fila->G1101_C16719)."</cell>";

                echo "<cell>". ($fila->G1101_C16729)."</cell>";
                
                echo "<cell>". ($fila->G1101_C38969)."</cell>";

                if($fila->G1101_C16717 != ''){
                    echo "<cell>". explode(' ', $fila->G1101_C16717)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G1101_C30105 != ''){
                    echo "<cell>". explode(' ', $fila->G1101_C30105)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G1101_C16718)."</cell>";

                echo "<cell><![CDATA[". ($fila->G1101_C16716)."]]></cell>";

                echo "<cell>". ($fila->G1101_C16720)."</cell>";

                if($fila->G1101_C16721 != ''){
                    echo "<cell>". explode(' ', $fila->G1101_C16721)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G1101_C16722 != ''){
                    echo "<cell>". explode(' ', $fila->G1101_C16722)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". $fila->G1101_C16730."</cell>"; 
                echo "</row>"; 
            } 
            echo "</rows>"; 
            
        }

    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1101 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1101(";
            $LsqlV = " VALUES ("; 
 
            if(isset($_POST["G1101_C16715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16715 = '".$_POST["G1101_C16715"]."'";
                $LsqlI .= $separador."G1101_C16715";
                $LsqlV .= $separador."'".$_POST["G1101_C16715"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16719"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16719 = '".$_POST["G1101_C16719"]."'";
                $LsqlI .= $separador."G1101_C16719";
                $LsqlV .= $separador."'".$_POST["G1101_C16719"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16729"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16729 = '".$_POST["G1101_C16729"]."'";
                $LsqlI .= $separador."G1101_C16729";
                $LsqlV .= $separador."'".$_POST["G1101_C16729"]."'";
                $validar = 1;
            }

            $G1101_C16717 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16717"])){    
                if($_POST["G1101_C16717"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16717 = "'".str_replace(' ', '',$_POST["G1101_C16717"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16717 = ".$G1101_C16717;
                    $LsqlI .= $separador." G1101_C16717";
                    $LsqlV .= $separador.$G1101_C16717;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1101_C16718"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16718 = '".$_POST["G1101_C16718"]."'";
                $LsqlI .= $separador."G1101_C16718";
                $LsqlV .= $separador."'".$_POST["G1101_C16718"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1101_C16716"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16716 = '".$_POST["G1101_C16716"]."'";
                $LsqlI .= $separador."G1101_C16716";
                $LsqlV .= $separador."'".$_POST["G1101_C16716"]."'";
                $validar = 1;
            }
                                                                           
 
                                                                         
            if(isset($_POST["G1101_C16720"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16720 = '".$_POST["G1101_C16720"]."'";
                $LsqlI .= $separador."G1101_C16720";
                $LsqlV .= $separador."'".$_POST["G1101_C16720"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1101_C16721 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16721"])){    
                if($_POST["G1101_C16721"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16721 = "'".str_replace(' ', '',$_POST["G1101_C16721"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16721 = ".$G1101_C16721;
                    $LsqlI .= $separador." G1101_C16721";
                    $LsqlV .= $separador.$G1101_C16721;
                    $validar = 1;
                }
            }
 
            $G1101_C16722 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1101_C16722"])){    
                if($_POST["G1101_C16722"] != '' && $_POST["G1101_C16722"] != 'undefined' && $_POST["G1101_C16722"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16722 = "'".$fecha." ".str_replace(' ', '',$_POST["G1101_C16722"])."'";
                    $LsqlU .= $separador."  G1101_C16722 = ".$G1101_C16722."";
                    $LsqlI .= $separador."  G1101_C16722";
                    $LsqlV .= $separador.$G1101_C16722;
                    $validar = 1;
                }
            }
 
            $G1101_C16730= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1101_C16730"])){    
                if($_POST["G1101_C16730"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16730 = $_POST["G1101_C16730"];
                    $LsqlU .= $separador." G1101_C16730 = '".$G1101_C16730."'";
                    $LsqlI .= $separador." G1101_C16730";
                    $LsqlV .= $separador."'".$G1101_C16730."'";
                    $validar = 1;
                }
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1101_C16723 = $numero;
                    $LsqlU .= ", G1101_C16723 = ".$G1101_C16723."";
                    $LsqlI .= ", G1101_C16723";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1101_Usuario ,  G1101_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1101_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1101 WHERE  G1101_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
