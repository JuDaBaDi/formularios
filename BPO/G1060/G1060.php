
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>

<div class="modal fade-in" id="verAgenda" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md" style="width: 91% !important">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">VER AGENDAS</h4>
            </div>
            <div class="modal-body">
            	<div class="table-responsive">
            		<table class="table table-bordered table-hover" id="misagendasTable">
						<thead>
							<tr>
								<th>#</th>
								<th>CLIENTE</th>
								<th>CODIGO CLIENTE</th>
								<th>TELEFONO 1</th>
								<th>TELEFONO 2</th>
								<th>CORREO</th>
								<th>FECHA AGENDA</th>
							</tr>
						</thead>
						<tbody>
							<?php 
			    				$idUsuario = getIdentificacionUser($token);

							$Lsql = "SELECT G1059_C15217 AS cliente, G1059_C15216 AS codigo, G1059_C15218 AS tel1, G1059_C15219 AS tel2, G1059_C15220 AS correo, G1059_M570_FecHorAge_b AS agenda, dyalogo_general.fn_nombre_USUARI(G1059_M570_ConIntUsu_b) AS agente FROM DYALOGOCRM_WEB.G1059 JOIN DYALOGOCRM_WEB.G1059_M570 ON G1059_ConsInte__b = G1059_M570_CoInMiPo__b WHERE G1059_M570_Estado____b = 2 AND G1059_M570_ConIntUsu_b = ".$idUsuario." AND G1059_M570_FecHorAge_b IS NOT NULL AND (G1059_C15217 IS NOT NULL OR G1059_C15216 IS NOT NULL OR G1059_C15218 IS NOT NULL OR G1059_C15219 IS NOT NULL OR G1059_C15220 IS NOT NULL) ORDER BY G1059_M570_FecHorAge_b LIMIT 1000";
									////

								//echo $Lsql;
								$res = $mysqli->query($Lsql);
								if($res){
									$i = 1;
									while ($mikey = $res->fetch_object()) {
										echo "<tr>";
											echo "<td>".$i."</td>";
											echo "<td>".$mikey->cliente."</td>";
											echo "<td>".$mikey->codigo."</td>";
											echo "<td>".$mikey->tel1."</td>";
											echo "<td>".$mikey->tel2."</td>";
											echo "<td>".$mikey->correo."</td>";
											echo "<td>".$mikey->agenda."</td>";
										echo "</tr>";
										$i++;
									}
								}
			                ?>
						</tbody>
					</table>
            	</div>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-default" type="button"  data-dismiss="modal" >
                    CERRAR
                </button>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1060/G1060_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1060_ConsInte__b as id, G1060_C15234 as camp1 , G1060_C15233 as camp2 FROM ".$BaseDatos.".G1060  WHERE G1060_Usuario = ".$idUsuario." ORDER BY G1060_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1060_ConsInte__b as id, G1060_C15234 as camp1 , G1060_C15233 as camp2 FROM ".$BaseDatos.".G1060  ORDER BY G1060_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1060_ConsInte__b as id, G1060_C15234 as camp1 , G1060_C15233 as camp2 FROM ".$BaseDatos.".G1060  ORDER BY G1060_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php
if(isset($_GET['user'])){


	$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

	$XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
	$nombre = $mysqli->query($XLsql);
	$nombreUsuario = NULL;
	//echo $XLsql;
	while ($key = $nombre->fetch_object()) {
	 	echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
	 	$nombreUsuario = $key->nombre;
	 	break;
	} 


	if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


					
		$data = array(	"strToken_t" => $_GET['token'], 
						"strIdGestion_t" => $_GET['id_gestion_cbx'],
						"strDatoPrincipal_t" => $nombreUsuario,
						"strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
		$data_string = json_encode($data);    

		$ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                      
		); 
		//recogemos la respuesta
		$respuesta = curl_exec ($ch);
		//o el error, por si falla
		$error = curl_error($ch);
		//y finalmente cerramos curl
		//echo "Respuesta =>  ". $respuesta;
		//echo "<br/>Error => ".$error;
		//include "Log.class.php";
		//$log = new Log("log", "./Log/");
		//$log->insert($error, $respuesta, false, true, false);
		//echo "nada";
		curl_close ($ch);
	}
}else{
	echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";	
}
?>

<div class="panel box box-primary" >
    <div class="box-header with-border">

	    	<div class="row">
		    	<div class="col-md-6 col-xs-6">
			        <h4 class="box-title">
			            <a data-toggle="collapse" data-parent="#accordion" href="#s_1859">
			                CONVERSACION
			            </a>
			        </h4>
		    	</div>
		    	<div class="col-md-6 col-xs-6">
					<?php if (isset($_GET["user"])) { ?>
						<div>
							<button type="button" data-toggle="modal" data-target="#verAgenda" class="btn btn-default pull-right">
							<i class="fa fa-calendar"></i>&nbsp;VER MIS AGENDAS</button>
						</div>
					<?php } ?>
		    	</div>
	    	</div>
    </div>
    <div id="s_1859" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Buenas tardes Sr xxxx habla con xxxxx asesor comercial de fincaraiz.com.co el portal digital. </p>
			        <!-- FIN LIBRETO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Si el contacto es por primera vez se debe indicar el guion de protección de datos: </p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Por razones de calidad su llamada podrá ser grabada o monitoreada. Para restarle nuestros servicios sus datos personales serán tratados conforme a la política general de privacidad que podrá consultar en nuestra página web</p>
			        <!-- FIN LIBRETO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">El motivo de la llamada es para confirmar como le ha ido con la publicación del anuncio que tiene con nosotros? </p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">El motivo de mi llamada es para ayudarle a realizar algunas mejoras en el anuncio:</p>
			        <!-- FIN LIBRETO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Confirmar datos de inmueble, Sugerir fotografías, Sugerir ubicación etc.</p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">También podemos ayudarle con alguno de los productos que tenemos para posicionar o destacar los anuncios en la página – Ha visto que algunos anuncios son diferentes?</p>
			        <!-- FIN LIBRETO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">En su caso podemos agregar un  Destacado Especial  dejaremos su anuncio en primeras posiciones  rotando en primeras páginas de la lista de resultados, con una etiqueta amarilla y  sombreado gris; Además va a ser visible en las APP y en los dispositivos Móviles. Este producto tiene un valor de $146.900 por 60 días. Le parece si lo agregamos a su publicación?</p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            DATOS DE LA BASE DE DATOS
        </h4>
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1060_C15260" id="LblG1060_C15260">FECHA CREACION </label>
			            <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1060_C15260'])) {
	                		echo $_GET['G1060_C15260'];
	                	} ?>"  name="G1060_C15260" id="G1060_C15260" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15238" id="LblG1060_C15238">CODIGO AVISO</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15238" value="<?php if (isset($_GET['G1060_C15238'])) {
	                		echo $_GET['G1060_C15238'];
	                	} ?>"  name="G1060_C15238"  placeholder="CODIGO AVISO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15234" id="LblG1060_C15234">CLIENTE</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15234" value="<?php if (isset($_GET['G1060_C15234'])) {
	                		echo $_GET['G1060_C15234'];
	                	} ?>"  name="G1060_C15234"  placeholder="CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15233" id="LblG1060_C15233">CODIGO CLIENTE </label>
			            <input type="text" class="form-control input-sm" id="G1060_C15233" value="<?php if (isset($_GET['G1060_C15233'])) {
	                		echo $_GET['G1060_C15233'];
	                	} ?>"  name="G1060_C15233"  placeholder="CODIGO CLIENTE ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15239" id="LblG1060_C15239">CON PRODUCTO</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15239" value="<?php if (isset($_GET['G1060_C15239'])) {
	                		echo $_GET['G1060_C15239'];
	                	} ?>"  name="G1060_C15239"  placeholder="CON PRODUCTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1060_C15240" id="LblG1060_C15240">DESCRIPCION</label>
			            <textarea class="form-control input-sm" name="G1060_C15240" id="G1060_C15240"  value="<?php if (isset($_GET['G1060_C15240'])) {
	                		echo $_GET['G1060_C15240'];
	                	} ?>" placeholder="DESCRIPCION"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15241" id="LblG1060_C15241">CON FOTO</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15241" value="<?php if (isset($_GET['G1060_C15241'])) {
	                		echo $_GET['G1060_C15241'];
	                	} ?>"  name="G1060_C15241"  placeholder="CON FOTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15235" id="LblG1060_C15235">TELEFONO 1 </label>
			            <input type="text" class="form-control input-sm" id="G1060_C15235" value="<?php if (isset($_GET['G1060_C15235'])) {
	                		echo $_GET['G1060_C15235'];
	                	} ?>"  name="G1060_C15235"  placeholder="TELEFONO 1 ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15236" id="LblG1060_C15236">TELEFONO 2 </label>
			            <input type="text" class="form-control input-sm" id="G1060_C15236" value="<?php if (isset($_GET['G1060_C15236'])) {
	                		echo $_GET['G1060_C15236'];
	                	} ?>"  name="G1060_C15236"  placeholder="TELEFONO 2 ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15242" id="LblG1060_C15242">TIPO DE OFERTA</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15242" value="<?php if (isset($_GET['G1060_C15242'])) {
	                		echo $_GET['G1060_C15242'];
	                	} ?>"  name="G1060_C15242"  placeholder="TIPO DE OFERTA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15243" id="LblG1060_C15243">TIPO DE INMUEBLE</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15243" value="<?php if (isset($_GET['G1060_C15243'])) {
	                		echo $_GET['G1060_C15243'];
	                	} ?>"  name="G1060_C15243"  placeholder="TIPO DE INMUEBLE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15244" id="LblG1060_C15244">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15244" value="<?php if (isset($_GET['G1060_C15244'])) {
	                		echo $_GET['G1060_C15244'];
	                	} ?>"  name="G1060_C15244"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15237" id="LblG1060_C15237">CORREO ELECTRONICO </label>
			            <input type="text" class="form-control input-sm" id="G1060_C15237" value="<?php if (isset($_GET['G1060_C15237'])) {
	                		echo $_GET['G1060_C15237'];
	                	} ?>"  name="G1060_C15237"  placeholder="CORREO ELECTRONICO ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15245" id="LblG1060_C15245">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15245" value="<?php if (isset($_GET['G1060_C15245'])) {
	                		echo $_GET['G1060_C15245'];
	                	} ?>"  name="G1060_C15245"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15261" id="LblG1060_C15261">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15261" value="<?php if (isset($_GET['G1060_C15261'])) {
	                		echo $_GET['G1060_C15261'];
	                	} ?>"  name="G1060_C15261"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


        </div>


    </div>
</div>

<div id="1857" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15252" id="LblG1060_C15252">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15252" value="<?php echo getNombreUser($token);?>" readonly name="G1060_C15252"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15253" id="LblG1060_C15253">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15253" value="<?php echo date('Y-m-d');?>" readonly name="G1060_C15253"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15254" id="LblG1060_C15254">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15254" value="<?php echo date('H:i:s');?>" readonly name="G1060_C15254"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1060_C15255" id="LblG1060_C15255">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1060_C15255" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1060_C15255"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div id="1856" >


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_1878">
                TIPIFICACION FR CD
            </a>
        </h4>
    </div>
    <div id="s_1878" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1060_C15377" id="LblG1060_C15377">GESTION</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1060_C15377" id="G1060_C15377">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 747 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1060_C15378" id="LblG1060_C15378">TIPIFICACION 1</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1060_C15378" id="G1060_C15378">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 748 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1060_C15379" id="LblG1060_C15379">TIPIFICACION 2</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1060_C15379" id="G1060_C15379">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 743 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1060_C38968" id="LblG1060_C38968">CANAL</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1060_C38968" id="G1060_C38968">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2292 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2054">
                GESTIÓN DE OTRAS BASES 
            </a>
        </h4>
    </div>
    <div id="s_2054" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


        </div>


        </div>
    </div>
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">GESTION</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear GESTION" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>

<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1060_C15247">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 742;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
   	<div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1060_C15247">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 742;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1060_C15248">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1060_C15249" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1060_C15250" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1060_C15251" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1060/G1060_eventos.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(function(){
	//JDBD - Esta seccion es solo para la interaccion con el formulario Padre
	/////////////////////////////////////////////////////////////////////////
	<?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
		<?php if($_GET["yourfather"] != "NULL"){ ?>
			$("#G1060_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
		<?php }else{ ?>
			if(document.getElementById("G1060_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
				$.ajax({
					url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
					type     : 'POST',
					data     : { q : <?php echo $_GET["idFather"]; ?> },
					success  : function(data){
						$("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
					}
				});
			}else{
				$("#G1060_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
			}
		<?php } ?>
	<?php } ?>
	/////////////////////////////////////////////////////////////////////////
	<?php if (!isset($_GET["view"])) {?>
		$("#add").click(function(){
							
		});
	<?php } ?>;	

    	$("#misagendasTable").DataTable({
    		"language" : {
		        "sProcessing":     "Procesando...",
		        "sLengthMenu":     "Mostrar _MENU_ registros",
		        "sZeroRecords":    "No se encontraron resultados",
		        "sEmptyTable":     "Ningún dato disponible en esta tabla",
		        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		        "sInfoPostFix":    "",
		        "sSearch":         "Buscar:",
		        "sUrl":            "",
		        "sInfoThousands":  ",",
		        "sLoadingRecords": "Cargando...",
		        "oPaginate": {
		            "sFirst":    "Primero",
		            "sLast":     "Último",
		            "sNext":     "Siguiente",
		            "sPrevious": "Anterior"
		        },

		        "oAria": {
		            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		        }
		    } ,
		    "scrollX": false
    	});
	var meses = new Array(12);
	meses[0] = "01";
	meses[1] = "02";
	meses[2] = "03";
	meses[3] = "04";
	meses[4] = "05";
	meses[5] = "06";
	meses[6] = "07";
	meses[7] = "08";
	meses[8] = "09";
	meses[9] = "10";
	meses[10] = "11";
	meses[11] = "12";

	var d = new Date();
	var h = d.getHours();
	var horas = (h < 10) ? '0' + h : h;
	var dia = d.getDate();
	var dias = (dia < 10) ? '0' + dia : dia;
	var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
	$("#FechaInicio").val(fechaInicial);
            

	//Esta es por si lo llaman en modo formulario de edicion LigthBox
	<?php if(isset($_GET['registroId'])){ ?>
	$.ajax({
		url      : '<?=$url_crud;?>',
		type     : 'POST',
		data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
		dataType : 'json',
		success  : function(data){
			//recorrer datos y enviarlos al formulario
			$.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
				$("#G1060_C15260").val(item.G1060_C15260); 
				$("#G1060_C15238").val(item.G1060_C15238); 
				$("#G1060_C15234").val(item.G1060_C15234); 
				$("#G1060_C15233").val(item.G1060_C15233); 
				$("#G1060_C15239").val(item.G1060_C15239); 
				$("#G1060_C15240").val(item.G1060_C15240); 
				$("#G1060_C15241").val(item.G1060_C15241); 
				$("#G1060_C15235").val(item.G1060_C15235); 
				$("#G1060_C15236").val(item.G1060_C15236); 
				$("#G1060_C15242").val(item.G1060_C15242); 
				$("#G1060_C15243").val(item.G1060_C15243); 
				$("#G1060_C15244").val(item.G1060_C15244); 
				$("#G1060_C15237").val(item.G1060_C15237); 
				$("#G1060_C15245").val(item.G1060_C15245); 
				$("#G1060_C15261").val(item.G1060_C15261); 
				$("#G1060_C15247").val(item.G1060_C15247).trigger("change");  
				$("#G1060_C15248").val(item.G1060_C15248).trigger("change");  
				$("#G1060_C15249").val(item.G1060_C15249); 
				$("#G1060_C15250").val(item.G1060_C15250); 
				$("#G1060_C15251").val(item.G1060_C15251); 
				$("#G1060_C15252").val(item.G1060_C15252); 
				$("#G1060_C15253").val(item.G1060_C15253); 
				$("#G1060_C15254").val(item.G1060_C15254); 
				$("#G1060_C15255").val(item.G1060_C15255);   
				if(item.G1060_C15256 == 1){
					$("#G1060_C15256").attr('checked', true);
				}    
				if(item.G1060_C15257 == 1){
					$("#G1060_C15257").attr('checked', true);
				}    
				if(item.G1060_C15342 == 1){
					$("#G1060_C15342").attr('checked', true);
				}    
				if(item.G1060_C15343 == 1){
					$("#G1060_C15343").attr('checked', true);
				}    
				if(item.G1060_C15344 == 1){
					$("#G1060_C15344").attr('checked', true);
				}    
				if(item.G1060_C15345 == 1){
					$("#G1060_C15345").attr('checked', true);
				}    
				if(item.G1060_C15346 == 1){
					$("#G1060_C15346").attr('checked', true);
				}    
				if(item.G1060_C15347 == 1){
					$("#G1060_C15347").attr('checked', true);
				}  
				$("#G1060_C15377").val(item.G1060_C15377).trigger("change");  
				$("#G1060_C15378").attr("opt",item.G1060_C15378);  
				$("#G1060_C15379").attr("opt",item.G1060_C15379);  
				$("#G1060_C38968").val(item.G1060_C38968).trigger("change");  
				
				$("#h3mio").html(item.principal);

			});

			//Deshabilitar los campos 3

			//Habilitar todos los campos para edicion
			$('#FormularioDatos :input').each(function(){
				$(this).attr('disabled', true);
			});              

			//Habilidar los botones de operacion, add, editar, eliminar
			$("#add").attr('disabled', false);
			$("#edit").attr('disabled', false);
			$("#delete").attr('disabled', false);

			//Desahabiliatra los botones de salvar y seleccionar_registro
			$("#cancel").attr('disabled', true);
			$("#Save").attr('disabled', true);
		} 
	});

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
    	$("#btnLlamar_0").attr('padre', $("#G1060_C15233").val());
    		var id_0 = $("#G1060_C15233").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G1060_C15233").val());
    		var id_0 = $("#G1060_C15233").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1060_C15233").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&formaDetalle=si&formularioPadre=1060&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=16723<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    

    		if($("#G1060_C15377").prop("selectedIndex")==0 || $("#G1060_C15377").prop("selectedIndex") == -1){
    			alertify.error('GESTION debe ser diligenciado');
    			$("#G1060_C15377").closest(".form-group").addClass("has-error");
    			valido = 1;
    		}

    		if($("#G1060_C15378").prop("selectedIndex")==0 || $("#G1060_C15378").prop("selectedIndex") == -1){
    			alertify.error('TIPIFICACION 1 debe ser diligenciado');
    			$("#G1060_C15378").closest(".form-group").addClass("has-error");
    			valido = 1;
    		}
                    if (validado == '0') {
                    	var form = $("#FormularioDatos");
	                    //Se crean un array con los datos a enviar, apartir del formulario 
	                    var formData = new FormData($("#FormularioDatos")[0]);
	                    $.ajax({
	                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
	                        type: 'POST',
	                        data: formData,
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        //una vez finalizado correctamente
	                        success: function(data){
	                            if(data){
	                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
	                                if($("#oper").val() == 'add'){
	                                    idTotal = data;
	                                }else{
	                                    idTotal= $("#hidId").val();
	                                }
	                                $("#hidId").val(idTotal);

	                                int_guardo = 1;
	                                $(".llamadores").attr('padre', idTotal);
	                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&formaDetalle=si&formularioPadre=1060&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=16723&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                                $("#editarDatos").modal('show');
	                                $("#oper").val('edit');

	                            }else{
	                                //Algo paso, hay un error
	                                alertify.error('Un error ha ocurrido');
	                            }                
	                        },
	                        //si ha ocurrido un error
	                        error: function(){
	                            after_save_error();
	                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                        }
	                    });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1060&pincheCampo=16723&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G1060_C15377").select2();

    $("#G1060_C15378").select2();

    $("#G1060_C15379").select2();
        //datepickers
    $("#G1060_C38968").select2();
        

        $("#G1060_C15260").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1060_C15249").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1060_C15250").wickedpicker(options);

        $("#G1060_C15233").numeric();

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para GESTION 

    $("#G1060_C15377").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '748' , idPadre : $(this).val() },
			success : function(data){
				var optG1060_C15378 = $("#G1060_C15378").attr("opt");
				$("#G1060_C15378").html(data);
				if (optG1060_C15378 != null) {
					$("#G1060_C15378").val(optG1060_C15378).trigger("change");
				}
			}
		});
		
    });

    //function para TIPIFICACION 1 

    $("#G1060_C15378").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '743' , idPadre : $(this).val() },
			success : function(data){
				var optG1060_C15379 = $("#G1060_C15379").attr("opt");
				$("#G1060_C15379").html(data);
				if (optG1060_C15379 != null) {
					$("#G1060_C15379").val(optG1060_C15379).trigger("change");
				}
			}
		});
		
    });

    //function para TIPIFICACION 2 

    $("#G1060_C15379").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if (Number($("#G1060_C15377").val())<1) {
            	alertify.error('"GESTION" debe ser diligenciado!');
            	$("#G1060_C15377").closest(".form-group").addClass("has-error");
            	valido = 1;
            }

            if (Number($("#G1060_C15378").val())<1) {
            	alertify.error('"TIPIFICACION 1" debe ser diligenciado!');
            	$("#G1060_C15378").closest(".form-group").addClass("has-error");
            	valido = 1;
            }

            if (Number($("#G1060_C15379 > option").length) > 1) {

	            if (Number($("#G1060_C15379").val())<1) {
	            	alertify.error('"TIPIFICACION 2" debe ser diligenciado!');
	            	$("#G1060_C15379").closest(".form-group").addClass("has-error");
	            	valido = 1;
	            }

            }

            if ($("#G1060_C15233").val().length < 1) {

        		alertify.error("Debe tener CODIGO CLIENTE!");
        		alertify.error("Debe tener CODIGO CLIENTE!");
        		alertify.error("Debe tener CODIGO CLIENTE!");
        		$("#G1060_C15233").closest(".form-group").addClass("has-error");
        		valido = 1;

            }

            if ($("#G1060_C15377").val() == "13308") {

	            if (Number($("#G1060_C38968").val())<1) {
	            	alertify.error('"CANAL" debe ser diligenciado!');
	            	$("#G1060_C38968").closest(".form-group").addClass("has-error");
	            	valido = 1;
	            }

            }

            if($(".tipificacion").val() == ''){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }
            
            if($("#G1060_C15251").val() == ''){
            	alertify.error("Es necesaria la observación!");
                $("#G1060_C15251").closest(".form-group").addClass("has-error");
            	valido = 1;
            }            

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        	var ID = <?=$_GET['registroId'];?>
			                        <?php }else{ ?>	
			                        	var ID = data
			                        <?php } ?>	
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : ID },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    
 
		                                    	$("#G1060_C15260").val(item.G1060_C15260);
 
		                                    	$("#G1060_C15238").val(item.G1060_C15238);
 
		                                    	$("#G1060_C15234").val(item.G1060_C15234);
 
		                                    	$("#G1060_C15233").val(item.G1060_C15233);
 
		                                    	$("#G1060_C15239").val(item.G1060_C15239);
 
		                                    	$("#G1060_C15240").val(item.G1060_C15240);
 
		                                    	$("#G1060_C15241").val(item.G1060_C15241);
 
		                                    	$("#G1060_C15235").val(item.G1060_C15235);
 
		                                    	$("#G1060_C15236").val(item.G1060_C15236);
 
		                                    	$("#G1060_C15242").val(item.G1060_C15242);
 
		                                    	$("#G1060_C15243").val(item.G1060_C15243);
 
		                                    	$("#G1060_C15244").val(item.G1060_C15244);
 
		                                    	$("#G1060_C15237").val(item.G1060_C15237);
 
		                                    	$("#G1060_C15245").val(item.G1060_C15245);
 
		                                    	$("#G1060_C15261").val(item.G1060_C15261);
 
                    $("#G1060_C15247").val(item.G1060_C15247).trigger("change"); 
 
                    $("#G1060_C15248").val(item.G1060_C15248).trigger("change"); 
 
		                                    	$("#G1060_C15249").val(item.G1060_C15249);
 
		                                    	$("#G1060_C15250").val(item.G1060_C15250);
 
		                                    	$("#G1060_C15251").val(item.G1060_C15251);
 
		                                    	$("#G1060_C15252").val(item.G1060_C15252);
 
		                                    	$("#G1060_C15253").val(item.G1060_C15253);
 
		                                    	$("#G1060_C15254").val(item.G1060_C15254);
 
		                                    	$("#G1060_C15255").val(item.G1060_C15255);
      
			                                    if(item.G1060_C15256 == 1){
			                                       $("#G1060_C15256").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15257 == 1){
			                                       $("#G1060_C15257").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15342 == 1){
			                                       $("#G1060_C15342").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15343 == 1){
			                                       $("#G1060_C15343").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15344 == 1){
			                                       $("#G1060_C15344").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15345 == 1){
			                                       $("#G1060_C15345").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15346 == 1){
			                                       $("#G1060_C15346").attr('checked', true);
			                                    } 
      
			                                    if(item.G1060_C15347 == 1){
			                                       $("#G1060_C15347").attr('checked', true);
			                                    } 
 
							                    $("#G1060_C15377").val(item.G1060_C15377).trigger("change"); 
							 
							                    $("#G1060_C15378").attr("opt",item.G1060_C15378); 
							 
							                    $("#G1060_C15379").attr("opt",item.G1060_C15379);  

							                    $("#G1060_C38968").val(item.G1060_C38968).trigger("change"); 

		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos 2

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(ID);  

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			console.log(xt);
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
		                        	
				
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','FECHA CREACION ','CODIGO AVISO','CLIENTE','CODIGO CLIENTE ','CON PRODUCTO','DESCRIPCION','CON FOTO','TELEFONO 1 ','TELEFONO 2 ','TIPO DE OFERTA','TIPO DE INMUEBLE','CIUDAD','CORREO ELECTRONICO ','DEPARTAMENTO','CLASIFICACION','Agente','Fecha','Hora','Campaña','GESTION','TIPIFICACION 1','TIPIFICACION 2'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                {  
	                    name:'G1060_C15260', 
	                    index:'G1060_C15260', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1060_C15238', 
	                    index: 'G1060_C15238', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15234', 
	                    index: 'G1060_C15234', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15233', 
	                    index: 'G1060_C15233', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15239', 
	                    index: 'G1060_C15239', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15240', 
	                    index:'G1060_C15240', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15241', 
	                    index: 'G1060_C15241', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15235', 
	                    index: 'G1060_C15235', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15236', 
	                    index: 'G1060_C15236', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15242', 
	                    index: 'G1060_C15242', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15243', 
	                    index: 'G1060_C15243', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15244', 
	                    index: 'G1060_C15244', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15237', 
	                    index: 'G1060_C15237', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15245', 
	                    index: 'G1060_C15245', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15261', 
	                    index: 'G1060_C15261', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15252', 
	                    index: 'G1060_C15252', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15253', 
	                    index: 'G1060_C15253', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15254', 
	                    index: 'G1060_C15254', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15255', 
	                    index: 'G1060_C15255', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1060_C15377', 
	                    index:'G1060_C15377', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1060_C15377'
	                    }
	                }

	                ,
	                { 
	                    name:'G1060_C15378', 
	                    index:'G1060_C15378', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1060_C15378'
	                    }
	                }

	                ,
	                { 
	                    name:'G1060_C15379', 
	                    index:'G1060_C15379', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1060_C15379'
	                    }
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1060_C15234',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
	            ,subGrid: true,
	            subGridRowExpanded: function(subgrid_id, row_id) { 
	                // we pass two parameters 
	                // subgrid_id is a id of the div tag created whitin a table data 
	                // the id of this elemenet is a combination of the "sg_" + id of the row 
	                // the row_id is the id of the row 
	                // If we wan to pass additinal parameters to the url we can use 
	                // a method getRowData(row_id) - which returns associative array in type name-value 
	                // here we can easy construct the flowing 
	                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID CLIENTE','GESTIÓN','TIPIFICACION 1','TIPIFICACION 2','FECHA DE TIPIFICACION ','HORA TIPIFICACION','AGENTE ','OBSERVACIONES ','CAMPAÑA','FECHA AGENDA ','HORA AGENDA ','ID DY', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1101_C16723', 
                            index: 'G1101_C16723', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1101_C16715', 
                            index:'G1101_C16715', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1101_C16715'
                            }
                        }

                        ,
                        {  
                            name:'G1101_C16719', 
                            index:'G1101_C16719', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1101_C16719'
                            }
                        }

                        ,
                        {  
                            name:'G1101_C16729', 
                            index:'G1101_C16729', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1101_C16729'
                            }
                        }

                        ,
                        {  
                            name:'G1101_C16717', 
                            index:'G1101_C16717', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1101_C30105', 
                            index: 'G1101_C30105', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
                        ,
                        { 
                            name:'G1101_C16718', 
                            index: 'G1101_C16718', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1101_C16716', 
                            index:'G1101_C16716', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1101_C16720', 
                            index: 'G1101_C16720', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1101_C16721', 
                            index:'G1101_C16721', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1101_C16722', 
                            index:'G1101_C16722', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1101_C16730', 
                            index:'G1101_C16730', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G1060_C15260").val(item.G1060_C15260);

                        $("#G1060_C15238").val(item.G1060_C15238);

                        $("#G1060_C15234").val(item.G1060_C15234);

                        $("#G1060_C15233").val(item.G1060_C15233);

                        $("#G1060_C15239").val(item.G1060_C15239);

                        $("#G1060_C15240").val(item.G1060_C15240);

                        $("#G1060_C15241").val(item.G1060_C15241);

                        $("#G1060_C15235").val(item.G1060_C15235);

                        $("#G1060_C15236").val(item.G1060_C15236);

                        $("#G1060_C15242").val(item.G1060_C15242);

                        $("#G1060_C15243").val(item.G1060_C15243);

                        $("#G1060_C15244").val(item.G1060_C15244);

                        $("#G1060_C15237").val(item.G1060_C15237);

                        $("#G1060_C15245").val(item.G1060_C15245);

                        $("#G1060_C15261").val(item.G1060_C15261);
 
                    $("#G1060_C15247").val(item.G1060_C15247).trigger("change"); 
 
                    $("#G1060_C15248").val(item.G1060_C15248).trigger("change"); 

                        $("#G1060_C15249").val(item.G1060_C15249);

                        $("#G1060_C15250").val(item.G1060_C15250);

                        $("#G1060_C15251").val(item.G1060_C15251);

                        $("#G1060_C15252").val(item.G1060_C15252);

                        $("#G1060_C15253").val(item.G1060_C15253);

                        $("#G1060_C15254").val(item.G1060_C15254);

                        $("#G1060_C15255").val(item.G1060_C15255);
    
                        if(item.G1060_C15256 == 1){
                           $("#G1060_C15256").attr('checked', true);
                        } 
    
                        if(item.G1060_C15257 == 1){
                           $("#G1060_C15257").attr('checked', true);
                        } 
    
                        if(item.G1060_C15342 == 1){
                           $("#G1060_C15342").attr('checked', true);
                        } 
    
                        if(item.G1060_C15343 == 1){
                           $("#G1060_C15343").attr('checked', true);
                        } 
    
                        if(item.G1060_C15344 == 1){
                           $("#G1060_C15344").attr('checked', true);
                        } 
    
                        if(item.G1060_C15345 == 1){
                           $("#G1060_C15345").attr('checked', true);
                        } 
    
                        if(item.G1060_C15346 == 1){
                           $("#G1060_C15346").attr('checked', true);
                        } 
    
                        if(item.G1060_C15347 == 1){
                           $("#G1060_C15347").attr('checked', true);
                        } 
 
                    $("#G1060_C15377").val(item.G1060_C15377).trigger("change"); 
 
                    $("#G1060_C15378").attr("opt",item.G1060_C15378); 
 
                    $("#G1060_C15379").attr("opt",item.G1060_C15379); 

                    $("#G1060_C38968").val(item.G1060_C38968).trigger("change");
			            
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID CLIENTE','GESTIÓN','TIPIFICACION 1','TIPIFICACION 2','CANAL','FECHA DE TIPIFICACION ','HORA TIPIFICACION ','AGENTE ','OBSERVACIONES ','CAMPAÑA','FECHA AGENDA ','HORA AGENDA ','ID DY', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1101_C16723', 
                    index: 'G1101_C16723', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1101_C16715', 
                    index:'G1101_C16715', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1101_C16715'
                    }
                }

                ,
                {  
                    name:'G1101_C16719', 
                    index:'G1101_C16719', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1101_C16719'
                    }
                }

                ,
                {  
                    name:'G1101_C16729', 
                    index:'G1101_C16729', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1101_C16729'
                    }
                }

                ,
                {  
                    name:'G1101_C38969', 
                    index:'G1101_C38969', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2292&campo=G1101_C38969'
                    }
                }

                ,
                {  
                    name:'G1101_C16717', 
                    index:'G1101_C16717', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1101_C30105', 
                    index: 'G1101_C30105', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                ,
                { 
                    name:'G1101_C16718', 
                    index: 'G1101_C16718', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {
                    name:'G1101_C16716', 
                    index:'G1101_C16716', 
                    width:150, 
                    editable: true 
                }

                ,
                { 
                    name:'G1101_C16720', 
                    index: 'G1101_C16720', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1101_C16721', 
                    index:'G1101_C16721', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1101_C16722', 
                    index:'G1101_C16722', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }
 
                ,
                {  
                    name:'G1101_C16730', 
                    index:'G1101_C16730', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1101_C16723',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'GESTION',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=16723&formularioPadre=1060<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
    	$("#btnLlamar_0").attr('padre', $("#G1060_C15233").val());
    		var id_0 = $("#G1060_C15233").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
    	$("#btnLlamar_0").attr('padre', $("#G1060_C15233").val());
    		var id_0 = $("#G1060_C15233").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
