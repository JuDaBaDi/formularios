
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1699/G1699_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÃ“N

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $userid= isset($userid) ? $userid : "-10";
        $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1699_ConsInte__b as id, G1699_C36754 as camp1 , G1699_C36755 as camp2 FROM ".$BaseDatos.".G1699  WHERE G1699_Usuario = ".$idUsuario." ORDER BY G1699_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1699_ConsInte__b as id, G1699_C36754 as camp1 , G1699_C36755 as camp2 FROM ".$BaseDatos.".G1699  ORDER BY G1699_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1699_ConsInte__b as id, G1699_C36754 as camp1 , G1699_C36755 as camp2 FROM ".$BaseDatos.".G1699 JOIN ".$BaseDatos.".G1699_M".$resultEstpas->muestr." ON G1699_ConsInte__b = G1699_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1699_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1699_ConsInte__b as id, G1699_C36754 as camp1 , G1699_C36755 as camp2 FROM ".$BaseDatos.".G1699 JOIN ".$BaseDatos.".G1699_M".$resultEstpas->muestr." ON G1699_ConsInte__b = G1699_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1699_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1699_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1699_ConsInte__b as id, G1699_C36754 as camp1 , G1699_C36755 as camp2 FROM ".$BaseDatos.".G1699  ORDER BY G1699_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){
    $idUsuario = isset($_GET["usuario"]) ? $_GET["usuario"] : $userid;

    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary"  id="4258">
    <div class="box-header with-border">
        <h4 class="box-title">
            CONTACTO
        </h4>
        
    </div>
    <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C37402" id="LblG1699_C37402">CODINTERNO</label><input type="text" class="form-control input-sm" id="G1699_C37402" value="<?php if (isset($_GET['G1699_C37402'])) {
                            echo $_GET['G1699_C37402'];
                        } ?>"  name="G1699_C37402"  placeholder="CODINTERNO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buen día, mi nombre es _________, soy ejecutivo de actualización y vinculación de audiencia de la revista IALIMENTOS. ¿Como está usted el día de hoy?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C30647" id="LblG1699_C30647">Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía </label><input type="text" class="form-control input-sm" id="G1699_C30647" value="<?php if (isset($_GET['G1699_C30647'])) {
                            echo $_GET['G1699_C30647'];
                        } ?>"  name="G1699_C30647"  placeholder="Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía "></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36702" id="LblG1699_C36702">no tendría ningún costo. ¿Hablo con el Sr./Sra.?</label><input type="text" class="form-control input-sm" id="G1699_C36702" value="<?php if (isset($_GET['G1699_C36702'])) {
                            echo $_GET['G1699_C36702'];
                        } ?>"  name="G1699_C36702"  placeholder="no tendría ningún costo. ¿Hablo con el Sr./Sra.?"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36739" id="LblG1699_C36739">Si si habla con el Sr./Sra / si no habla con el Sr./Sra</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36739" id="G1699_C36739">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2125 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36739" id="respuesta_LblG1699_C36739">Respuesta</label>
                        <textarea id="respuesta_G1699_C36739" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36753" id="LblG1699_C36753">Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo? </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36753" id="G1699_C36753">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1527 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36753" id="respuesta_LblG1699_C36753">Respuesta</label>
                        <textarea id="respuesta_G1699_C36753" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36741" id="LblG1699_C36741">1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36741" id="G1699_C36741">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2127 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36741" id="respuesta_LblG1699_C36741">Respuesta</label>
                        <textarea id="respuesta_G1699_C36741" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


    </div>
</div>

<div  id="4260" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C30640" id="LblG1699_C30640">Agente</label><input type="text" class="form-control input-sm" id="G1699_C30640" value="<?php isset($userid) ? NombreAgente($userid) : getNombreUser($token);?>" readonly name="G1699_C30640"  placeholder="Agente"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C30641" id="LblG1699_C30641">Fecha</label><input type="text" class="form-control input-sm" id="G1699_C30641" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G1699_C30641"  placeholder="Fecha"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C30642" id="LblG1699_C30642">Hora</label><input type="text" class="form-control input-sm" id="G1699_C30642" value="<?php echo date('H:i:s');?>" readonly name="G1699_C30642"  placeholder="Hora"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C30643" id="LblG1699_C30643">Campaña</label><input type="text" class="form-control input-sm" id="G1699_C30643" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÃ‘A";}?>" readonly name="G1699_C30643"  placeholder="Campaña"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  id="4259" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="5367" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5367c">
                2.0 DATOS EMPRESA
            </a>
        </h4>
        
    </div>
    <div id="s_5367c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36742" id="LblG1699_C36742">NOMBRE COMERCIAL DE LA EMPRESA (*)</label><input type="text" class="form-control input-sm" id="G1699_C36742" value="<?php if (isset($_GET['G1699_C36742'])) {
                            echo $_GET['G1699_C36742'];
                        } ?>"  name="G1699_C36742"  placeholder="NOMBRE COMERCIAL DE LA EMPRESA (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1699_C36743" id="LblG1699_C36743">NIT</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1699_C36743'])) {
                            echo $_GET['G1699_C36743'];
                        } ?>"  name="G1699_C36743" id="G1699_C36743" placeholder="NIT">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36744" id="LblG1699_C36744">NÚMERO DE EMPLEADOS</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36744" id="G1699_C36744">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1544 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G1699_C36748" id="LblG1699_C36748">D.E CIUDAD (*)</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G1699_C36748" id="G1699_C36748">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36747" id="LblG1699_C36747">D.E DEPARTAMENTO (*)</label><input type="text" class="form-control input-sm" id="G1699_C36747" value="<?php if (isset($_GET['G1699_C36747'])) {
                            echo $_GET['G1699_C36747'];
                        } ?>"  name="G1699_C36747"  placeholder="D.E DEPARTAMENTO (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36750" id="LblG1699_C36750">PERFIL(*)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36750" id="G1699_C36750">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2104 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36751" id="LblG1699_C36751">ACTIVIDAD(*)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36751" id="G1699_C36751">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2106 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36779" id="LblG1699_C36779">¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36779" id="G1699_C36779">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2109 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36780" id="LblG1699_C36780">CODIGO DE EMPRESA</label><input type="text" class="form-control input-sm" id="G1699_C36780" value="<?php if (isset($_GET['G1699_C36780'])) {
                            echo $_GET['G1699_C36780'];
                        } ?>"  name="G1699_C36780"  placeholder="CODIGO DE EMPRESA"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36781" id="LblG1699_C36781">PAGINA_WEB</label><input type="text" class="form-control input-sm" id="G1699_C36781" value="<?php if (isset($_GET['G1699_C36781'])) {
                            echo $_GET['G1699_C36781'];
                        } ?>"  name="G1699_C36781"  placeholder="PAGINA_WEB"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36782" id="LblG1699_C36782">¿SU EMPRESA IMPORTA O EXPORTA PRODUCTO TERMINADO?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36782" id="G1699_C36782">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2107 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36783" id="LblG1699_C36783">¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36783" id="G1699_C36783">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2108 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5368" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5368c">
                3.0 DATOS SUSCRIPTOR
            </a>
        </h4>
        
    </div>
    <div id="s_5368c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36754" id="LblG1699_C36754">NOMBRES (*)</label><input type="text" class="form-control input-sm" id="G1699_C36754" value="<?php if (isset($_GET['G1699_C36754'])) {
                            echo $_GET['G1699_C36754'];
                        } ?>"  name="G1699_C36754"  placeholder="NOMBRES (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36755" id="LblG1699_C36755">APELLIDOS (*)</label><input type="text" class="form-control input-sm" id="G1699_C36755" value="<?php if (isset($_GET['G1699_C36755'])) {
                            echo $_GET['G1699_C36755'];
                        } ?>"  name="G1699_C36755"  placeholder="APELLIDOS (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36756" id="LblG1699_C36756">DOCUMENTO</label><input type="text" class="form-control input-sm" id="G1699_C36756" value="<?php if (isset($_GET['G1699_C36756'])) {
                            echo $_GET['G1699_C36756'];
                        } ?>"  name="G1699_C36756"  placeholder="DOCUMENTO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36757" id="LblG1699_C36757">SEXO(*)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36757" id="G1699_C36757">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2112 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1699_C36758" id="LblG1699_C36758">FECHA DE NACIMIENTO</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1699_C36758'])) {
                            echo $_GET['G1699_C36758'];
                        } ?>"  name="G1699_C36758" id="G1699_C36758" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36759" id="LblG1699_C36759">EMAIL (*) (Dar prioridad al correo coorporativo, pero en caso de que no tenga, se captura el correo personal, adicional se debe deletrear el correo para confirmar que quedó bien registrado)</label>
                        <input type="email" class="form-control input-sm" id="G1699_C36759" value="<?php if (isset($_GET['G1699_C36759'])) {
                            echo $_GET['G1699_C36759'];
                        } ?>"  name="G1699_C36759"  placeholder="EMAIL (*) (Dar prioridad al correo coorporativo, pero en caso de que no tenga, se captura el correo personal, adicional se debe deletrear el correo para confirmar que quedó bien registrado)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36772" id="LblG1699_C36772">¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36772" id="G1699_C36772">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1519 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G1699_C36778" id="LblG1699_C36778">CARGO(*)</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G1699_C36778" id="G1699_C36778">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C37299" id="LblG1699_C37299">GRUPO CARGO</label><input type="text" class="form-control input-sm" id="G1699_C37299" value="<?php if (isset($_GET['G1699_C37299'])) {
                            echo $_GET['G1699_C37299'];
                        } ?>"  name="G1699_C37299"  placeholder="GRUPO CARGO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36771" id="LblG1699_C36771">TIPO SUSCRIPCIÓN(*)</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36771" id="G1699_C36771">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2115 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36760" id="LblG1699_C36760">D.P PAÍS (*)</label><input type="text" class="form-control input-sm" id="G1699_C36760" value="<?php if (isset($_GET['G1699_C36760'])) {
                            echo $_GET['G1699_C36760'];
                        } ?>"  name="G1699_C36760"  placeholder="D.P PAÍS (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                            <!-- JDBD-20-05-11: CAMPO DE TIPO LISTA COMPLEJA -->
                            <div class="form-group">
                                <label for="G1699_C36762" id="LblG1699_C36762">D.P CIUDAD (*)</label>
                                <select class="form-control input-sm select2" style="width: 100%;"  name="G1699_C36762" id="G1699_C36762">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                            <!-- JDBD-20-05-11: FIN DEL CAMPO TIPO LISTA COMPLEJA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36761" id="LblG1699_C36761">D.P DEPARTAMENTO (*)</label><input type="text" class="form-control input-sm" id="G1699_C36761" value="<?php if (isset($_GET['G1699_C36761'])) {
                            echo $_GET['G1699_C36761'];
                        } ?>"  name="G1699_C36761"  placeholder="D.P DEPARTAMENTO (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36763" id="LblG1699_C36763">D.P BARRIO</label><input type="text" class="form-control input-sm" id="G1699_C36763" value="<?php if (isset($_GET['G1699_C36763'])) {
                            echo $_GET['G1699_C36763'];
                        } ?>"  name="G1699_C36763"  placeholder="D.P BARRIO"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36764" id="LblG1699_C36764">DIRECCIÓN (*)</label><input type="text" class="form-control input-sm" id="G1699_C36764" value="<?php if (isset($_GET['G1699_C36764'])) {
                            echo $_GET['G1699_C36764'];
                        } ?>"  name="G1699_C36764"  placeholder="DIRECCIÓN (*)"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1699_C36765" id="LblG1699_C36765">DIRECCIÓN 2</label><input type="text" class="form-control input-sm" id="G1699_C36765" value="<?php if (isset($_GET['G1699_C36765'])) {
                            echo $_GET['G1699_C36765'];
                        } ?>"  name="G1699_C36765"  placeholder="DIRECCIÓN 2"></div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1699_C36766" id="LblG1699_C36766">TELEFONO 1 (*)</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1699_C36766'])) {
                            echo $_GET['G1699_C36766'];
                        } ?>"  name="G1699_C36766" id="G1699_C36766" placeholder="TELEFONO 1 (*)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1699_C36767" id="LblG1699_C36767">TELEFONO 2</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1699_C36767'])) {
                            echo $_GET['G1699_C36767'];
                        } ?>"  name="G1699_C36767" id="G1699_C36767" placeholder="TELEFONO 2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO ENTERO -->
                    <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1699_C36768" id="LblG1699_C36768">CELULAR(*)</label>
                        <input type="text" class="form-control input-sm Numerico" value="<?php if (isset($_GET['G1699_C36768'])) {
                            echo $_GET['G1699_C36768'];
                        } ?>"  name="G1699_C36768" id="G1699_C36768" placeholder="CELULAR(*)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36769" id="LblG1699_C36769">¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36769" id="G1699_C36769">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1519 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1699_C36770" id="LblG1699_C36770">OBSERVACIONES (CALIFICACIÓN)(*)</label>
                        <textarea class="form-control input-sm" name="G1699_C36770" id="G1699_C36770"  value="<?php if (isset($_GET['G1699_C36770'])) {
                            echo $_GET['G1699_C36770'];
                        } ?>" placeholder="OBSERVACIONES (CALIFICACIÓN)(*)"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5369" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5369c">
                4.0 OBJECIONES
            </a>
        </h4>
        
    </div>
    <div id="s_5369c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36773" id="LblG1699_C36773">TIPO</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36773" id="G1699_C36773">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2129 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36774" id="LblG1699_C36774">OBJECION </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36774" id="G1699_C36774">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2130 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36774" id="respuesta_LblG1699_C36774">Respuesta</label>
                        <textarea id="respuesta_G1699_C36774" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5370" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5370c">
                5.0 PREGUNTA FRECUENTES
            </a>
        </h4>
        
    </div>
    <div id="s_5370c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36775" id="LblG1699_C36775">PREGUNTA FRECUENTES</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36775" id="G1699_C36775">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2131 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36775" id="respuesta_LblG1699_C36775">Respuesta</label>
                        <textarea id="respuesta_G1699_C36775" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5371" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5371c">
                LEGAL
            </a>
        </h4>
        
    </div>
    <div id="s_5371c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Recuerde que esta información será almacenada en nuestra base de datos; además podrá consultar nuestro aviso de privacidad y política de datos personales, en nuestra página web. WWW.AXIOMAB2B.COM</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5372" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5372c">
                DESPEDIDA
            </a>
        </h4>
        
    </div>
    <div id="s_5372c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr./Sra.__________, muchas gracias por atender mi llamada, le recuerdo que hablo con _____ de la revista ________ que tenga un feliz día/tarde.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5538" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5538c">
                REFERIDOS
            </a>
        </h4>
        
    </div>
    <div id="s_5538c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">REFERIDOS</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear REFERIDOS" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1699_C30635">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1699;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1699_C30635">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1699;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1699_C30636">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1699_C30637" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1699_C30638" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1699_C30639" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include(__DIR__ ."/../pies.php");

?>
<script type="text/javascript" src="formularios/G1699/G1699_eventos.js"></script>
<script type="text/javascript">    
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la secciÃ³n de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1699_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1699_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1699_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
             
                $("#G1699_C37402").val(item.G1699_C37402);   
                if(item.G1699_C30646 == 1){
                    $("#G1699_C30646").attr('checked', true);
                }  
                $("#G1699_C30647").val(item.G1699_C30647); 
                $("#G1699_C36702").val(item.G1699_C36702); 
                $("#G1699_C36739").val(item.G1699_C36739).trigger("change");  
                $("#G1699_C36753").val(item.G1699_C36753).trigger("change");  
                $("#G1699_C36741").val(item.G1699_C36741).trigger("change");  
                $("#G1699_C30635").val(item.G1699_C30635).trigger("change");  
                $("#G1699_C30636").val(item.G1699_C30636).trigger("change");  
                $("#G1699_C30637").val(item.G1699_C30637); 
                $("#G1699_C30638").val(item.G1699_C30638); 
                $("#G1699_C30639").val(item.G1699_C30639); 
                $("#G1699_C30640").val(item.G1699_C30640); 
                $("#G1699_C30641").val(item.G1699_C30641); 
                $("#G1699_C30642").val(item.G1699_C30642); 
                $("#G1699_C30643").val(item.G1699_C30643); 
                $("#G1699_C36742").val(item.G1699_C36742); 
                $("#G1699_C36743").val(item.G1699_C36743); 
                $("#G1699_C36744").val(item.G1699_C36744).trigger("change");  
                $("#G1699_C36748").attr("opt",item.G1699_C36748);
                $("#G1699_C36748").val(item.G1699_C36748).trigger("change"); 
                $("#G1699_C36747").val(item.G1699_C36747); 
                $("#G1699_C36750").val(item.G1699_C36750).trigger("change");  
                $("#G1699_C36751").attr("opt",item.G1699_C36751);  
                $("#G1699_C36779").val(item.G1699_C36779).trigger("change");  
                $("#G1699_C36780").val(item.G1699_C36780); 
                $("#G1699_C36781").val(item.G1699_C36781); 
                $("#G1699_C36782").val(item.G1699_C36782).trigger("change");  
                $("#G1699_C36783").val(item.G1699_C36783).trigger("change");  
                $("#G1699_C36754").val(item.G1699_C36754); 
                $("#G1699_C36755").val(item.G1699_C36755); 
                $("#G1699_C36756").val(item.G1699_C36756); 
                $("#G1699_C36757").val(item.G1699_C36757).trigger("change");  
                $("#G1699_C36758").val(item.G1699_C36758); 
                $("#G1699_C36759").val(item.G1699_C36759); 
                $("#G1699_C36772").val(item.G1699_C36772).trigger("change");  
                $("#G1699_C36778").attr("opt",item.G1699_C36778);
                $("#G1699_C36778").val(item.G1699_C36778).trigger("change"); 
                $("#G1699_C37299").val(item.G1699_C37299); 
                $("#G1699_C36771").val(item.G1699_C36771).trigger("change");  
                $("#G1699_C36760").val(item.G1699_C36760); 
                $("#G1699_C36762").attr("opt",item.G1699_C36762);
                $("#G1699_C36762").val(item.G1699_C36762).trigger("change"); 
                $("#G1699_C36761").val(item.G1699_C36761); 
                $("#G1699_C36763").val(item.G1699_C36763); 
                $("#G1699_C36764").val(item.G1699_C36764); 
                $("#G1699_C36765").val(item.G1699_C36765); 
                $("#G1699_C36766").val(item.G1699_C36766); 
                $("#G1699_C36767").val(item.G1699_C36767); 
                $("#G1699_C36768").val(item.G1699_C36768); 
                $("#G1699_C36769").val(item.G1699_C36769).trigger("change");  
                $("#G1699_C36770").val(item.G1699_C36770); 
                $("#G1699_C36773").val(item.G1699_C36773).trigger("change");  
                $("#G1699_C36774").val(item.G1699_C36774).trigger("change");  
                $("#G1699_C36775").val(item.G1699_C36775).trigger("change");    
                if(item.G1699_C36776 == 1){
                    $("#G1699_C36776").attr('checked', true);
                }    
                if(item.G1699_C36777 == 1){
                    $("#G1699_C36777").attr('checked', true);
                } 
                
                cargarHijos_0(
        $("#G1699_C37402").val());
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
        $("#btnLlamar_0").attr('padre', $("#G1699_C37402").val());
            var id_0 = $("#G1699_C37402").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G1699_C37402").val());
            var id_0 = $("#G1699_C37402").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1699_C37402").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2050&view=si&formaDetalle=si&formularioPadre=1699&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=37665<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                        var form = $("#FormularioDatos");
                        //Se crean un array con los datos a enviar, apartir del formulario 
                        var formData = new FormData($("#FormularioDatos")[0]);
                        $.ajax({
                           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            //una vez finalizado correctamente
                            success: function(data){
                                if(data){
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                    $("#hidId").val(idTotal);

                                    int_guardo = 1;
                                    $(".llamadores").attr('padre', idTotal);
                                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2050&view=si&formaDetalle=si&formularioPadre=1699&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=37665&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                                    $("#editarDatos").modal('show');
                                    $("#oper").val('edit');

                                }else{
                                    //Algo paso, hay un error
                                    alertify.error('Un error ha ocurrido');
                                }                
                            },
                            //si ha ocurrido un error
                            error: function(){
                                after_save_error();
                                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                            }
                        });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2050&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1699&pincheCampo=37665&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&usuario=<?=$idUsuario?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G1699_C36739").select2();

    $("#G1699_C36753").select2();

    $("#G1699_C36741").select2();

    $("#G1699_C36744").select2();
                            $("#G1699_C36748").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1699_C36748=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G1699_C36748").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G1699_C36748 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G1699_C36748").html('<option value="'+data.G2043_ConsInte__b+'" >'+data.G2043_C37408+'</option>');
                                        
                                $("#G1699_C36747").val(data.G2043_C37407);
                                    }
                                });
                            });

    $("#G1699_C36750").select2();

    $("#G1699_C36751").select2();

    $("#G1699_C36779").select2();

    $("#G1699_C36782").select2();

    $("#G1699_C36783").select2();

    $("#G1699_C36757").select2();

    $("#G1699_C36772").select2();
                            $("#G1699_C36778").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1699_C36778=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G1699_C36778").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G1699_C36778 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G1699_C36778").html('<option value="'+data.G2005_ConsInte__b+'" >'+data.G2005_C36697+'</option>');
                                        
                                $("#G1699_C37299").val(data.G2005_C36696);
                                    }
                                });
                            });

    $("#G1699_C36771").select2();
                            $("#G1699_C36762").select2({
                                placeholder: "Buscar",
                                allowClear: false,
                                minimumInputLength: 3,
                                ajax:{
                                    url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1699_C36762=si',
                                    dataType: 'json',
                                    type : 'post',
                                    delay: 250,
                                    data: function (params) {
                                        return {
                                            q: params.term
                                        };
                                    },
                                    processResults: function(data) {
                                        return {
                                            results: $.map(data, function(obj) {
                                                return {id: obj.id,text: obj.text};
                                            })
                                        };
                                    },
                                    cache: true
                                }
                            });

                            $("#G1699_C36762").change(function(){
                                var valor = $(this).attr("opt");
                                if ($(this).val()) {
                                    valor = $(this).val();
                                }
                                $.ajax({
                                    url   : "<?php echo $url_crud;?>",
                                    data  : { dameValoresCamposDinamicos_Guion_G1699_C36762 : valor},
                                    type  : "post",
                                    dataType : "json",
                                    success  : function(data){
                                        $("#G1699_C36762").html('<option value="'+data.G2043_ConsInte__b+'" >'+data.G2043_C37408+'</option>');
                                        
                                $("#G1699_C36761").val(data.G2043_C37407);
                                    }
                                });
                            });

    $("#G1699_C36769").select2();

    $("#G1699_C36773").select2();

    $("#G1699_C36774").select2();

    $("#G1699_C36775").select2();
                $("#G1699_C30635").change(function(){
                    $(".ReqForTip").closest(".form-group").removeClass("has-error");
                    $(".ReqForTip").removeClass("ReqForTip");
                        
                    if($(this).val() == "166869"){
                        $("#G1699_C36742").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36743").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36747").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36748").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36750").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36751").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36754").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36755").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36757").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36759").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36760").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36761").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36762").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36764").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36766").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36768").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36771").addClass("ReqForTip");
                    }
                    if($(this).val() == "166869"){
                        $("#G1699_C36778").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36742").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36747").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36748").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36750").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36751").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36754").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36755").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36757").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36759").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36760").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36761").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36762").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36764").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36766").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36768").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36771").addClass("ReqForTip");
                    }
                    if($(this).val() == "166865"){
                        $("#G1699_C36778").addClass("ReqForTip");
                    }
                    if($(this).val() == "166863"){
                        $("#G1699_C36754").addClass("ReqForTip");
                    }
                    if($(this).val() == "166863"){
                        $("#G1699_C36755").addClass("ReqForTip");
                    }
                    if($(this).val() == "166863"){
                        $("#G1699_C36768").addClass("ReqForTip");
                    }
                    if($(this).val() == "166863"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166864"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166866"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166868"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166870"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166871"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166872"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166873"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166874"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166876"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166877"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166878"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "166882"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "171763"){
                        $("#G1699_C36754").addClass("ReqForTip");
                    }
                    if($(this).val() == "171763"){
                        $("#G1699_C36755").addClass("ReqForTip");
                    }
                    if($(this).val() == "171763"){
                        $("#G1699_C36768").addClass("ReqForTip");
                    }
                    if($(this).val() == "171763"){
                        $("#G1699_C36742").addClass("ReqForTip");
                    }
                    if($(this).val() == "171763"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "171764"){
                        $("#G1699_C36754").addClass("ReqForTip");
                    }
                    if($(this).val() == "171764"){
                        $("#G1699_C36755").addClass("ReqForTip");
                    }
                    if($(this).val() == "171764"){
                        $("#G1699_C36768").addClass("ReqForTip");
                    }
                    if($(this).val() == "171764"){
                        $("#G1699_C36770").addClass("ReqForTip");
                    }
                    if($(this).val() == "171765"){
                        $("#G1699_C36754").addClass("ReqForTip");
                    }
                    if($(this).val() == "171765"){
                        $("#G1699_C36755").addClass("ReqForTip");
                    }
                    if($(this).val() == "171765"){
                        $("#G1699_C36742").addClass("ReqForTip");
                    }
                    if($(this).val() == "171765"){
                        $("#G1699_C36759").addClass("ReqForTip");
                    }
                    if($(this).val() == "171765"){
                        $("#G1699_C36768").addClass("ReqForTip");
                    }
                });
        //datepickers
        

        $("#G1699_C30637").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1699_C36758").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1699_C30638").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G1699_C36743").numeric();
                
        $("#G1699_C36766").numeric();
                
        $("#G1699_C36767").numeric();
                
        $("#G1699_C36768").numeric();
                

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Si si habla con el Sr./Sra / si no habla con el Sr./Sra 

    $("#G1699_C36739").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1699_C36739 option:selected").attr('respuesta');
        $("#respuesta_G1699_C36739").val(respuesta);
        

    });

    //function para Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo?  

    $("#G1699_C36753").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1699_C36753 option:selected").attr('respuesta');
        $("#respuesta_G1699_C36753").val(respuesta);
        

    });

    //function para 1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo? 

    $("#G1699_C36741").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1699_C36741 option:selected").attr('respuesta');
        $("#respuesta_G1699_C36741").val(respuesta);
        

    });

    //function para NÚMERO DE EMPLEADOS 

    $("#G1699_C36744").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PERFIL(*) 

    $("#G1699_C36750").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2106' , idPadre : $(this).val() },
            success : function(data){
                var optG1699_C36751 = $("#G1699_C36751").attr("opt");
                $("#G1699_C36751").html(data);
                if (optG1699_C36751 != null) {
                    $("#G1699_C36751").val(optG1699_C36751).trigger("change");
                }
            }
        });
        
    });

    //function para ACTIVIDAD(*) 

    $("#G1699_C36751").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA? 

    $("#G1699_C36779").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿SU EMPRESA IMPORTA O EXPORTA PRODUCTO TERMINADO? 

    $("#G1699_C36782").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO? 

    $("#G1699_C36783").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SEXO(*) 

    $("#G1699_C36757").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido? 

    $("#G1699_C36772").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO SUSCRIPCIÓN(*) 

    $("#G1699_C36771").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria? 

    $("#G1699_C36769").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO 

    $("#G1699_C36773").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OBJECION  

    $("#G1699_C36774").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1699_C36774 option:selected").attr('respuesta');
        $("#respuesta_G1699_C36774").val(respuesta);
        

    });

    //function para PREGUNTA FRECUENTES 

    $("#G1699_C36775").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1699_C36775 option:selected").attr('respuesta');
        $("#respuesta_G1699_C36775").val(respuesta);
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificaciÃ³n!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1699_C37402").val(item.G1699_C37402);
      
                                                if(item.G1699_C30646 == 1){
                                                   $("#G1699_C30646").attr('checked', true);
                                                } 
 
                                                $("#G1699_C30647").val(item.G1699_C30647);
 
                                                $("#G1699_C36702").val(item.G1699_C36702);
 
                    $("#G1699_C36739").val(item.G1699_C36739).trigger("change"); 
 
                    $("#G1699_C36753").val(item.G1699_C36753).trigger("change"); 
 
                    $("#G1699_C36741").val(item.G1699_C36741).trigger("change"); 
 
                    $("#G1699_C30635").val(item.G1699_C30635).trigger("change"); 
 
                    $("#G1699_C30636").val(item.G1699_C30636).trigger("change"); 
 
                                                $("#G1699_C30637").val(item.G1699_C30637);
 
                                                $("#G1699_C30638").val(item.G1699_C30638);
 
                                                $("#G1699_C30639").val(item.G1699_C30639);
 
                                                $("#G1699_C30640").val(item.G1699_C30640);
 
                                                $("#G1699_C30641").val(item.G1699_C30641);
 
                                                $("#G1699_C30642").val(item.G1699_C30642);
 
                                                $("#G1699_C30643").val(item.G1699_C30643);
 
                                                $("#G1699_C36742").val(item.G1699_C36742);
 
                                                $("#G1699_C36743").val(item.G1699_C36743);
 
                    $("#G1699_C36744").val(item.G1699_C36744).trigger("change"); 
 
                    $("#G1699_C36748").attr("opt",item.G1699_C36748);
                    $("#G1699_C36748").val(item.G1699_C36748).trigger("change");
 
                                                $("#G1699_C36747").val(item.G1699_C36747);
 
                    $("#G1699_C36750").val(item.G1699_C36750).trigger("change"); 
 
                    $("#G1699_C36751").attr("opt",item.G1699_C36751); 
 
                    $("#G1699_C36779").val(item.G1699_C36779).trigger("change"); 
 
                                                $("#G1699_C36780").val(item.G1699_C36780);
 
                                                $("#G1699_C36781").val(item.G1699_C36781);
 
                    $("#G1699_C36782").val(item.G1699_C36782).trigger("change"); 
 
                    $("#G1699_C36783").val(item.G1699_C36783).trigger("change"); 
 
                                                $("#G1699_C36754").val(item.G1699_C36754);
 
                                                $("#G1699_C36755").val(item.G1699_C36755);
 
                                                $("#G1699_C36756").val(item.G1699_C36756);
 
                    $("#G1699_C36757").val(item.G1699_C36757).trigger("change"); 
 
                                                $("#G1699_C36758").val(item.G1699_C36758);
 
                                                $("#G1699_C36759").val(item.G1699_C36759);
 
                    $("#G1699_C36772").val(item.G1699_C36772).trigger("change"); 
 
                    $("#G1699_C36778").attr("opt",item.G1699_C36778);
                    $("#G1699_C36778").val(item.G1699_C36778).trigger("change");
 
                                                $("#G1699_C37299").val(item.G1699_C37299);
 
                    $("#G1699_C36771").val(item.G1699_C36771).trigger("change"); 
 
                                                $("#G1699_C36760").val(item.G1699_C36760);
 
                    $("#G1699_C36762").attr("opt",item.G1699_C36762);
                    $("#G1699_C36762").val(item.G1699_C36762).trigger("change");
 
                                                $("#G1699_C36761").val(item.G1699_C36761);
 
                                                $("#G1699_C36763").val(item.G1699_C36763);
 
                                                $("#G1699_C36764").val(item.G1699_C36764);
 
                                                $("#G1699_C36765").val(item.G1699_C36765);
 
                                                $("#G1699_C36766").val(item.G1699_C36766);
 
                                                $("#G1699_C36767").val(item.G1699_C36767);
 
                                                $("#G1699_C36768").val(item.G1699_C36768);
 
                    $("#G1699_C36769").val(item.G1699_C36769).trigger("change"); 
 
                                                $("#G1699_C36770").val(item.G1699_C36770);
 
                    $("#G1699_C36773").val(item.G1699_C36773).trigger("change"); 
 
                    $("#G1699_C36774").val(item.G1699_C36774).trigger("change"); 
 
                    $("#G1699_C36775").val(item.G1699_C36775).trigger("change"); 
      
                                                if(item.G1699_C36776 == 1){
                                                   $("#G1699_C36776").attr('checked', true);
                                                } 
      
                                                if(item.G1699_C36777 == 1){
                                                   $("#G1699_C36777").attr('checked', true);
                                                } 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?=$idUsuario?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>&cerrarViaPost=true',
                                        type  : "post",
                                        dataType : "json",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
                                            try{
                                                var origen="formulario";
                                                finalizarGestion(xt,origen);
                                            }catch{
                                                var data={
                                                    accion:"cerrargestion",
                                                    datos:xt
                                                };
                                                parent.postMessage(data, '*');
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la informaciÃ³n');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CODINTERNO','Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía ','no tendría ningún costo. ¿Hablo con el Sr./Sra.?','Si si habla con el Sr./Sra / si no habla con el Sr./Sra','Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo?	','1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo?','Agente','Fecha','Hora','Campaña','NOMBRE COMERCIAL DE LA EMPRESA (*)','NIT','NÚMERO DE EMPLEADOS','D.E CIUDAD (*)','D.E DEPARTAMENTO (*)','PERFIL(*)','ACTIVIDAD(*)','¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA?','CODIGO DE EMPRESA','PAGINA_WEB','¿SU EMPRESA IMPORTA O EXPORTA PRODUCTO TERMINADO?','¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO?','NOMBRES (*)','APELLIDOS (*)','DOCUMENTO','SEXO(*)','FECHA DE NACIMIENTO','EMAIL (*) (Dar prioridad al correo coorporativo, pero en caso de que no tenga, se captura el correo personal, adicional se debe deletrear el correo para confirmar que quedó bien registrado)','¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido?','CARGO(*)','GRUPO CARGO','TIPO SUSCRIPCIÓN(*)','D.P PAÍS (*)','D.P CIUDAD (*)','D.P DEPARTAMENTO (*)','D.P BARRIO','DIRECCIÓN (*)','DIRECCIÓN 2','TELEFONO 1 (*)','TELEFONO 2','CELULAR(*)','¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria?','OBSERVACIONES (CALIFICACIÓN)(*)','TIPO','OBJECION	','PREGUNTA FRECUENTES'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1699_C37402', 
                        index: 'G1699_C37402', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C30647', 
                        index: 'G1699_C30647', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36702', 
                        index: 'G1699_C36702', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C30640', 
                        index: 'G1699_C30640', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C30641', 
                        index: 'G1699_C30641', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C30642', 
                        index: 'G1699_C30642', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C30643', 
                        index: 'G1699_C30643', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36742', 
                        index: 'G1699_C36742', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G1699_C36743', 
                        index:'G1699_C36743', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G1699_C36744', 
                        index:'G1699_C36744', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1544&campo=G1699_C36744'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36748', 
                        index:'G1699_C36748', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1699_C36748=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36747', 
                        index: 'G1699_C36747', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36750', 
                        index:'G1699_C36750', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2104&campo=G1699_C36750'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36751', 
                        index:'G1699_C36751', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2106&campo=G1699_C36751'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36779', 
                        index:'G1699_C36779', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2109&campo=G1699_C36779'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36780', 
                        index: 'G1699_C36780', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36781', 
                        index: 'G1699_C36781', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36782', 
                        index:'G1699_C36782', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2107&campo=G1699_C36782'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36783', 
                        index:'G1699_C36783', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2108&campo=G1699_C36783'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36754', 
                        index: 'G1699_C36754', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36755', 
                        index: 'G1699_C36755', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36756', 
                        index: 'G1699_C36756', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36757', 
                        index:'G1699_C36757', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2112&campo=G1699_C36757'
                        }
                    }

                    ,
                    {  
                        name:'G1699_C36758', 
                        index:'G1699_C36758', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36772', 
                        index:'G1699_C36772', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1519&campo=G1699_C36772'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36778', 
                        index:'G1699_C36778', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1699_C36778=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1699_C37299', 
                        index: 'G1699_C37299', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36771', 
                        index:'G1699_C36771', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2115&campo=G1699_C36771'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36760', 
                        index: 'G1699_C36760', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36762', 
                        index:'G1699_C36762', 
                        width:300 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1699_C36762=si',
                            dataInit:function(el){
                                $(el).select2();
                                /*$(el).select2({ 
                                    templateResult: function(data) {
                                        var r = data.text.split('|');
                                        var row = '<div class="row">';
                                        var totalRows = 12 / r.length;
                                        for(i= 0; i < r.length; i++){
                                            row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                        }
                                        row += '</div>';
                                        var $result = $(row);
                                        return $result;
                                    },
                                    templateSelection : function(data){
                                        var r = data.text.split('|');
                                        return r[0];
                                    }
                                });*/
                                $(el).change(function(){
                                    var valores = $(el + " option:selected").attr("llenadores");
                                    var campos =  $(el + " option:selected").attr("dinammicos");
                                    var r = valores.split('|');
                                    if(r.length > 1){

                                        var c = campos.split('|');
                                        for(i = 1; i < r.length; i++){
                                            $("#"+ rowid +"_"+c[i]).val(r[i]);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36761', 
                        index: 'G1699_C36761', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36763', 
                        index: 'G1699_C36763', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36764', 
                        index: 'G1699_C36764', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36765', 
                        index: 'G1699_C36765', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
 
                    ,
                    {  
                        name:'G1699_C36766', 
                        index:'G1699_C36766', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1699_C36767', 
                        index:'G1699_C36767', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }
 
                    ,
                    {  
                        name:'G1699_C36768', 
                        index:'G1699_C36768', 
                        width:80 ,
                        editable: true, 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).numeric();
                            }
                        } 
                    }

                    ,
                    { 
                        name:'G1699_C36769', 
                        index:'G1699_C36769', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1519&campo=G1699_C36769'
                        }
                    }

                    ,
                    { 
                        name:'G1699_C36770', 
                        index:'G1699_C36770', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1699_C36773', 
                        index:'G1699_C36773', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2129&campo=G1699_C36773'
                        }
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1699_C36754',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?=$idUsuario?>",
            autowidth: true
            
                ,subGrid: true,
                subGridRowExpanded: function(subgrid_id, row_id) { 
                    // we pass two parameters 
                    // subgrid_id is a id of the div tag created whitin a table data 
                    // the id of this elemenet is a combination of the "sg_" + id of the row 
                    // the row_id is the id of the row 
                    // If we wan to pass additinal parameters to the url we can use 
                    // a method getRowData(row_id) - which returns associative array in type name-value 
                    // here we can easy construct the flowing 
                    $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Cod Interno','Nombres','Apellidos','Correo','Telefono','Cargo','Empresa','NIT', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                            ,
                            { 
                                name:'G2050_C37665', 
                                index: 'G2050_C37665', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37666', 
                                index: 'G2050_C37666', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37667', 
                                index: 'G2050_C37667', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37668', 
                                index: 'G2050_C37668', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37669', 
                                index: 'G2050_C37669', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37670', 
                                index: 'G2050_C37670', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37671', 
                                index: 'G2050_C37671', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                            ,
                            { 
                                name:'G2050_C37694', 
                                index: 'G2050_C37694', 
                                width:160, 
                                resizable:false, 
                                sortable:true , 
                                editable: true 
                            }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).val(id); 
                                } 
                            }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G1699_C37402").val(item.G1699_C37402);
    
                        if(item.G1699_C30646 == 1){
                           $("#G1699_C30646").attr('checked', true);
                        } 

                        $("#G1699_C30647").val(item.G1699_C30647);

                        $("#G1699_C36702").val(item.G1699_C36702);
 
                    $("#G1699_C36739").val(item.G1699_C36739).trigger("change"); 
 
                    $("#G1699_C36753").val(item.G1699_C36753).trigger("change"); 
 
                    $("#G1699_C36741").val(item.G1699_C36741).trigger("change"); 
 
                    $("#G1699_C30635").val(item.G1699_C30635).trigger("change"); 
 
                    $("#G1699_C30636").val(item.G1699_C30636).trigger("change"); 

                        $("#G1699_C30637").val(item.G1699_C30637);

                        $("#G1699_C30638").val(item.G1699_C30638);

                        $("#G1699_C30639").val(item.G1699_C30639);

                        $("#G1699_C30640").val(item.G1699_C30640);

                        $("#G1699_C30641").val(item.G1699_C30641);

                        $("#G1699_C30642").val(item.G1699_C30642);

                        $("#G1699_C30643").val(item.G1699_C30643);

                        $("#G1699_C36742").val(item.G1699_C36742);

                        $("#G1699_C36743").val(item.G1699_C36743);
 
                    $("#G1699_C36744").val(item.G1699_C36744).trigger("change"); 
 
                    $("#G1699_C36748").attr("opt",item.G1699_C36748);
                    $("#G1699_C36748").val(item.G1699_C36748).trigger("change");

                        $("#G1699_C36747").val(item.G1699_C36747);
 
                    $("#G1699_C36750").val(item.G1699_C36750).trigger("change"); 
 
                    $("#G1699_C36751").attr("opt",item.G1699_C36751); 
 
                    $("#G1699_C36779").val(item.G1699_C36779).trigger("change"); 

                        $("#G1699_C36780").val(item.G1699_C36780);

                        $("#G1699_C36781").val(item.G1699_C36781);
 
                    $("#G1699_C36782").val(item.G1699_C36782).trigger("change"); 
 
                    $("#G1699_C36783").val(item.G1699_C36783).trigger("change"); 

                        $("#G1699_C36754").val(item.G1699_C36754);

                        $("#G1699_C36755").val(item.G1699_C36755);

                        $("#G1699_C36756").val(item.G1699_C36756);
 
                    $("#G1699_C36757").val(item.G1699_C36757).trigger("change"); 

                        $("#G1699_C36758").val(item.G1699_C36758);

                        $("#G1699_C36759").val(item.G1699_C36759);
 
                    $("#G1699_C36772").val(item.G1699_C36772).trigger("change"); 
 
                    $("#G1699_C36778").attr("opt",item.G1699_C36778);
                    $("#G1699_C36778").val(item.G1699_C36778).trigger("change");

                        $("#G1699_C37299").val(item.G1699_C37299);
 
                    $("#G1699_C36771").val(item.G1699_C36771).trigger("change"); 

                        $("#G1699_C36760").val(item.G1699_C36760);
 
                    $("#G1699_C36762").attr("opt",item.G1699_C36762);
                    $("#G1699_C36762").val(item.G1699_C36762).trigger("change");

                        $("#G1699_C36761").val(item.G1699_C36761);

                        $("#G1699_C36763").val(item.G1699_C36763);

                        $("#G1699_C36764").val(item.G1699_C36764);

                        $("#G1699_C36765").val(item.G1699_C36765);

                        $("#G1699_C36766").val(item.G1699_C36766);

                        $("#G1699_C36767").val(item.G1699_C36767);

                        $("#G1699_C36768").val(item.G1699_C36768);
 
                    $("#G1699_C36769").val(item.G1699_C36769).trigger("change"); 

                        $("#G1699_C36770").val(item.G1699_C36770);
 
                    $("#G1699_C36773").val(item.G1699_C36773).trigger("change"); 
 
                    $("#G1699_C36774").val(item.G1699_C36774).trigger("change"); 
 
                    $("#G1699_C36775").val(item.G1699_C36775).trigger("change"); 
    
                        if(item.G1699_C36776 == 1){
                           $("#G1699_C36776").attr('checked', true);
                        } 
    
                        if(item.G1699_C36777 == 1){
                           $("#G1699_C36777").attr('checked', true);
                        } 
                        
            cargarHijos_0(
        $("#G1699_C37402").val());
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Cod Interno','Nombres','Apellidos','Correo','Telefono','Cargo','Empresa','NIT', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2050_C37665', 
                        index: 'G2050_C37665', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37666', 
                        index: 'G2050_C37666', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37667', 
                        index: 'G2050_C37667', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37668', 
                        index: 'G2050_C37668', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37669', 
                        index: 'G2050_C37669', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37670', 
                        index: 'G2050_C37670', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37671', 
                        index: 'G2050_C37671', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2050_C37694', 
                        index: 'G2050_C37694', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G2050_C37665',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'REFERIDOS',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?=$idUsuario?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=2050&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=37665&formularioPadre=1699<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
        $("#btnLlamar_0").attr('padre', $("#G1699_C37402").val());
            var id_0 = $("#G1699_C37402").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
    }
    
    function llamarDesdeBtnTelefono(telefono){
        <?php 
            $campana=0;
            if(isset($_GET["campana_crm"])){
                $campana=$_GET["campana_crm"];
            } 
        ?>
        
        var data={
            accion:"llamadaDesdeG",
            telefono: "A<?=$campana?>"+telefono,
            validarScript: false
        };
        parent.postMessage(data, '*');
    }   
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaÃ±a para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
        $("#btnLlamar_0").attr('padre', $("#G1699_C37402").val());
            var id_0 = $("#G1699_C37402").val();
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id_0);
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
