<?php 
    /*
        Document   : index
        Created on : 2020-09-29 09:17:35
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MTY5OQ==  
    */
    $url_crud =  "formularios/G1699/G1699_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G1699/G1699_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C37402" id="LblG1699_C37402">CODINTERNO</label>
								<input type="text" class="form-control input-sm" id="G1699_C37402" value=""  name="G1699_C37402"  placeholder="CODINTERNO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Buen día, mi nombre es _________, soy ejecutivo de actualización y vinculación de audiencia de la revista IALIMENTOS. ¿Como está usted el día de hoy?</h3>
                            <!-- FIN LIBRETO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C30647" id="LblG1699_C30647">Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía </label>
								<input type="text" class="form-control input-sm" id="G1699_C30647" value=""  name="G1699_C30647"  placeholder="Lo estamos comunicando para realizar una actualización de datos que nos permita continuar garantizando el envío de la Revista , que para la compañía ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36702" id="LblG1699_C36702">no tendría ningún costo. ¿Hablo con el Sr./Sra.?</label>
								<input type="text" class="form-control input-sm" id="G1699_C36702" value=""  name="G1699_C36702"  placeholder="no tendría ningún costo. ¿Hablo con el Sr./Sra.?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36739" id="LblG1699_C36739">Si si habla con el Sr./Sra / si no habla con el Sr./Sra</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36739" id="G1699_C36739">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2125 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36739" id="respuesta_LblG1699_C36739">Respuesta</label>
                        <textarea id="respuesta_G1699_C36739" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36753" id="LblG1699_C36753">Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo?	</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36753" id="G1699_C36753">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1527 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36753" id="respuesta_LblG1699_C36753">Respuesta</label>
                        <textarea id="respuesta_G1699_C36753" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36741" id="LblG1699_C36741">1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36741" id="G1699_C36741">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2127 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36741" id="respuesta_LblG1699_C36741">Respuesta</label>
                        <textarea id="respuesta_G1699_C36741" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36742" id="LblG1699_C36742">NOMBRE COMERCIAL DE LA EMPRESA (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36742" value=""  name="G1699_C36742"  placeholder="NOMBRE COMERCIAL DE LA EMPRESA (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1699_C36743" id="LblG1699_C36743">NIT</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G1699_C36743" id="G1699_C36743" placeholder="NIT">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36744" id="LblG1699_C36744">NÚMERO DE EMPLEADOS</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36744" id="G1699_C36744">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1544 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <?php 
                            $str_Lsql = "SELECT  G2043_ConsInte__b as id , G2043_C37408, G2043_C37407 FROM ".$BaseDatos_systema.".G2043";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1699_C36748" id="LblG1699_C36748">D.E CIUDAD (*)</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1699_C36748" id="G1699_C36748">
                                    <option>MUNICIPIO | DEPARTAMENTO</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0|G1699_C36747'>".($obj->G2043_C37408)." | ".($obj->G2043_C37407)."</option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36747" id="LblG1699_C36747">D.E DEPARTAMENTO (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36747" value=""  name="G1699_C36747"  placeholder="D.E DEPARTAMENTO (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36750" id="LblG1699_C36750">PERFIL(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36750" id="G1699_C36750">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2104 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36751" id="LblG1699_C36751">ACTIVIDAD(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36751" id="G1699_C36751">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2106 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36779" id="LblG1699_C36779">¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36779" id="G1699_C36779">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2109 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36780" id="LblG1699_C36780">CODIGO DE EMPRESA</label>
								<input type="text" class="form-control input-sm" id="G1699_C36780" value=""  name="G1699_C36780"  placeholder="CODIGO DE EMPRESA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36781" id="LblG1699_C36781">PAGINA_WEB</label>
								<input type="text" class="form-control input-sm" id="G1699_C36781" value=""  name="G1699_C36781"  placeholder="PAGINA_WEB">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36782" id="LblG1699_C36782">¿SU EMPRESA IMPORTA O EXPORTA PRODUCTO TERMINADO?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36782" id="G1699_C36782">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2107 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36783" id="LblG1699_C36783">¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36783" id="G1699_C36783">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2108 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36754" id="LblG1699_C36754">NOMBRES (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36754" value=""  name="G1699_C36754"  placeholder="NOMBRES (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36755" id="LblG1699_C36755">APELLIDOS (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36755" value=""  name="G1699_C36755"  placeholder="APELLIDOS (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36756" id="LblG1699_C36756">DOCUMENTO</label>
								<input type="text" class="form-control input-sm" id="G1699_C36756" value=""  name="G1699_C36756"  placeholder="DOCUMENTO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36757" id="LblG1699_C36757">SEXO(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36757" id="G1699_C36757">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2112 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1699_C36758" id="LblG1699_C36758">FECHA DE NACIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G1699_C36758" id="G1699_C36758" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36772" id="LblG1699_C36772">¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36772" id="G1699_C36772">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1519 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <?php 
                            $str_Lsql = "SELECT  G2005_ConsInte__b as id , G2005_C36697, G2005_C36696 FROM ".$BaseDatos_systema.".G2005";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1699_C36778" id="LblG1699_C36778">CARGO(*)</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1699_C36778" id="G1699_C36778">
                                    <option>CARGO | GRUPOCARGO</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0|G1699_C37299'>".($obj->G2005_C36697)." | ".($obj->G2005_C36696)."</option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C37299" id="LblG1699_C37299">GRUPO CARGO</label>
								<input type="text" class="form-control input-sm" id="G1699_C37299" value=""  name="G1699_C37299"  placeholder="GRUPO CARGO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36771" id="LblG1699_C36771">TIPO SUSCRIPCIÓN(*)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36771" id="G1699_C36771">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2115 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36760" id="LblG1699_C36760">D.P PAÍS (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36760" value=""  name="G1699_C36760"  placeholder="D.P PAÍS (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <?php 
                            $str_Lsql = "SELECT  G2043_ConsInte__b as id , G2043_C37408, G2043_C37407 FROM ".$BaseDatos_systema.".G2043";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1699_C36762" id="LblG1699_C36762">D.P CIUDAD (*)</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1699_C36762" id="G1699_C36762">
                                    <option>MUNICIPIO | DEPARTAMENTO</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0|G1699_C36761'>".($obj->G2043_C37408)." | ".($obj->G2043_C37407)."</option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36761" id="LblG1699_C36761">D.P DEPARTAMENTO (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36761" value=""  name="G1699_C36761"  placeholder="D.P DEPARTAMENTO (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36763" id="LblG1699_C36763">D.P BARRIO</label>
								<input type="text" class="form-control input-sm" id="G1699_C36763" value=""  name="G1699_C36763"  placeholder="D.P BARRIO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36764" id="LblG1699_C36764">DIRECCIÓN (*)</label>
								<input type="text" class="form-control input-sm" id="G1699_C36764" value=""  name="G1699_C36764"  placeholder="DIRECCIÓN (*)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1699_C36765" id="LblG1699_C36765">DIRECCIÓN 2</label>
								<input type="text" class="form-control input-sm" id="G1699_C36765" value=""  name="G1699_C36765"  placeholder="DIRECCIÓN 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1699_C36766" id="LblG1699_C36766">TELEFONO 1 (*)</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G1699_C36766" id="G1699_C36766" placeholder="TELEFONO 1 (*)">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1699_C36767" id="LblG1699_C36767">TELEFONO 2</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G1699_C36767" id="G1699_C36767" placeholder="TELEFONO 2">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1699_C36768" id="LblG1699_C36768">CELULAR(*)</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G1699_C36768" id="G1699_C36768" placeholder="CELULAR(*)">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36769" id="LblG1699_C36769">¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36769" id="G1699_C36769">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1519 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G1699_C36770" id="LblG1699_C36770">OBSERVACIONES (CALIFICACIÓN)(*)</label>
                                <textarea class="form-control input-sm" name="G1699_C36770" id="G1699_C36770"  value="" placeholder="OBSERVACIONES (CALIFICACIÓN)(*)"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1699_C36773" id="LblG1699_C36773">TIPO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36773" id="G1699_C36773">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2129 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36774" id="LblG1699_C36774">OBJECION	</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36774" id="G1699_C36774">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2130 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36774" id="respuesta_LblG1699_C36774">Respuesta</label>
                        <textarea id="respuesta_G1699_C36774" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1699_C36775" id="LblG1699_C36775">PREGUNTA FRECUENTES</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1699_C36775" id="G1699_C36775">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2131 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1699_C36775" id="respuesta_LblG1699_C36775">Respuesta</label>
                        <textarea id="respuesta_G1699_C36775" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Recuerde que esta información será almacenada en nuestra base de datos; además podrá consultar nuestro aviso de privacidad y política de datos personales, en nuestra página web. WWW.AXIOMAB2B.COM</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr./Sra.__________, muchas gracias por atender mi llamada, le recuerdo que hablo con _____ de la revista ________ que tenga un feliz día/tarde.</h3>
                            <!-- FIN LIBRETO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G1699/G1699_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


        $("#G1699_C36748").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                         
                                '<div class="col-md-6">' + r[0] + '</div>' + 
                                '<div class="col-md-6">' + r[1] + '</div>' +
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1699_C36748").change(function(){
            var valores = $("#G1699_C36748 option:selected").text();
            var campos = $("#G1699_C36748 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

        $("#G1699_C36778").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                         
                                '<div class="col-md-6">' + r[0] + '</div>' + 
                                '<div class="col-md-6">' + r[1] + '</div>' +
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1699_C36778").change(function(){
            var valores = $("#G1699_C36778 option:selected").text();
            var campos = $("#G1699_C36778 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

        $("#G1699_C36762").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                         
                                '<div class="col-md-6">' + r[0] + '</div>' + 
                                '<div class="col-md-6">' + r[1] + '</div>' +
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1699_C36762").change(function(){
            var valores = $("#G1699_C36762 option:selected").text();
            var campos = $("#G1699_C36762 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

                //datepickers
                

            $("#G1699_C30637").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1699_C36758").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G1699_C30638").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                

            $("#G1699_C36743").numeric();
            
            $("#G1699_C36766").numeric();
            
            $("#G1699_C36767").numeric();
            
            $("#G1699_C36768").numeric();
            

                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para Si si habla con el Sr./Sra / si no habla con el Sr./Sra 

    $("#G1699_C36739").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Le recuerdo que esta llamada está siendo grabada y monitoreada por su seguridad. Le agradezco me permita unos minutos para actualizar los datos y continuar con el envío de nuestras publicaciones. ¿Está de Acuerdo?	 

    $("#G1699_C36753").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para 1.1 De acuerdo a lo anterior, le confirmo que durante la llamada vamos a solicitar datos de contacto;  los mismos serán tratados bajo cumplimiento de la ley de protección de datos personales. ¿De acuerdo? 

    $("#G1699_C36741").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para NÚMERO DE EMPLEADOS 

    $("#G1699_C36744").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PERFIL(*) 

    $("#G1699_C36750").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2106' , idPadre : $(this).val() },
            success : function(data){
                $("#G1699_C36751").html(data);
            }
        });
        
    });

    //function para ACTIVIDAD(*) 

    $("#G1699_C36751").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿CUAL ES SU IMPACTO O INFLUENCIA EN CUANTO  A LAS DECISIONES DE COMPRA? 

    $("#G1699_C36779").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿SU EMPRESA IMPORTA O EXPORTA PRODUCTO TERMINADO? 

    $("#G1699_C36782").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿CUAL ES EL RANGO DE INGRESOS OPERACIONALES DE LA EMPRESA AL AÑO? 

    $("#G1699_C36783").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SEXO(*) 

    $("#G1699_C36757").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Nos Recomendaría a algunos de sus colegas de su empresa o de otra compañía del sector para solicitarle la autorización de envío de nuestro contenido? 

    $("#G1699_C36772").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO SUSCRIPCIÓN(*) 

    $("#G1699_C36771").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Nos autoriza a compartir su información con proveedores de contenido relacionado y grandes referentes de la indústria? 

    $("#G1699_C36769").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO 

    $("#G1699_C36773").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OBJECION	 

    $("#G1699_C36774").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PREGUNTA FRECUENTES 

    $("#G1699_C36775").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
