<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1699_ConsInte__b, G1699_FechaInsercion , G1699_Usuario ,  G1699_CodigoMiembro  , G1699_PoblacionOrigen , G1699_EstadoDiligenciamiento ,  G1699_IdLlamada , G1699_C36754 as principal ,G1699_C37402,G1699_C30647,G1699_C36702,G1699_C36739,G1699_C36753,G1699_C36741,G1699_C30635,G1699_C30636,G1699_C30637,G1699_C30638,G1699_C30639,G1699_C30640,G1699_C30641,G1699_C30642,G1699_C30643,G1699_C36742,G1699_C36743,G1699_C36744,G1699_C36748,G1699_C36747,G1699_C36750,G1699_C36751,G1699_C36779,G1699_C36780,G1699_C36781,G1699_C36782,G1699_C36783,G1699_C36754,G1699_C36755,G1699_C36756,G1699_C36757,G1699_C36758,G1699_C36759,G1699_C36772,G1699_C36778,G1699_C37299,G1699_C36771,G1699_C36760,G1699_C36762,G1699_C36761,G1699_C36763,G1699_C36764,G1699_C36765,G1699_C36766,G1699_C36767,G1699_C36768,G1699_C36769,G1699_C36770,G1699_C36773,G1699_C36774,G1699_C36775 FROM '.$BaseDatos.'.G1699 WHERE G1699_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1699_C37402'] = $key->G1699_C37402;

                $datos[$i]['G1699_C30647'] = $key->G1699_C30647;

                $datos[$i]['G1699_C36702'] = $key->G1699_C36702;

                $datos[$i]['G1699_C36739'] = $key->G1699_C36739;

                $datos[$i]['G1699_C36753'] = $key->G1699_C36753;

                $datos[$i]['G1699_C36741'] = $key->G1699_C36741;

                $datos[$i]['G1699_C30635'] = $key->G1699_C30635;

                $datos[$i]['G1699_C30636'] = $key->G1699_C30636;

                $datos[$i]['G1699_C30637'] = explode(' ', $key->G1699_C30637)[0];
  
                $hora = '';
                if(!is_null($key->G1699_C30638)){
                    $hora = explode(' ', $key->G1699_C30638)[1];
                }

                $datos[$i]['G1699_C30638'] = $hora;

                $datos[$i]['G1699_C30639'] = $key->G1699_C30639;

                $datos[$i]['G1699_C30640'] = $key->G1699_C30640;

                $datos[$i]['G1699_C30641'] = $key->G1699_C30641;

                $datos[$i]['G1699_C30642'] = $key->G1699_C30642;

                $datos[$i]['G1699_C30643'] = $key->G1699_C30643;

                $datos[$i]['G1699_C36742'] = $key->G1699_C36742;

                $datos[$i]['G1699_C36743'] = $key->G1699_C36743;

                $datos[$i]['G1699_C36744'] = $key->G1699_C36744;

                $datos[$i]['G1699_C36748'] = $key->G1699_C36748;

                $datos[$i]['G1699_C36747'] = $key->G1699_C36747;

                $datos[$i]['G1699_C36750'] = $key->G1699_C36750;

                $datos[$i]['G1699_C36751'] = $key->G1699_C36751;

                $datos[$i]['G1699_C36779'] = $key->G1699_C36779;

                $datos[$i]['G1699_C36780'] = $key->G1699_C36780;

                $datos[$i]['G1699_C36781'] = $key->G1699_C36781;

                $datos[$i]['G1699_C36782'] = $key->G1699_C36782;

                $datos[$i]['G1699_C36783'] = $key->G1699_C36783;

                $datos[$i]['G1699_C36754'] = $key->G1699_C36754;

                $datos[$i]['G1699_C36755'] = $key->G1699_C36755;

                $datos[$i]['G1699_C36756'] = $key->G1699_C36756;

                $datos[$i]['G1699_C36757'] = $key->G1699_C36757;

                $datos[$i]['G1699_C36758'] = explode(' ', $key->G1699_C36758)[0];

                $datos[$i]['G1699_C36759'] = $key->G1699_C36759;

                $datos[$i]['G1699_C36772'] = $key->G1699_C36772;

                $datos[$i]['G1699_C36778'] = $key->G1699_C36778;

                $datos[$i]['G1699_C37299'] = $key->G1699_C37299;

                $datos[$i]['G1699_C36771'] = $key->G1699_C36771;

                $datos[$i]['G1699_C36760'] = $key->G1699_C36760;

                $datos[$i]['G1699_C36762'] = $key->G1699_C36762;

                $datos[$i]['G1699_C36761'] = $key->G1699_C36761;

                $datos[$i]['G1699_C36763'] = $key->G1699_C36763;

                $datos[$i]['G1699_C36764'] = $key->G1699_C36764;

                $datos[$i]['G1699_C36765'] = $key->G1699_C36765;

                $datos[$i]['G1699_C36766'] = $key->G1699_C36766;

                $datos[$i]['G1699_C36767'] = $key->G1699_C36767;

                $datos[$i]['G1699_C36768'] = $key->G1699_C36768;

                $datos[$i]['G1699_C36769'] = $key->G1699_C36769;

                $datos[$i]['G1699_C36770'] = $key->G1699_C36770;

                $datos[$i]['G1699_C36773'] = $key->G1699_C36773;

                $datos[$i]['G1699_C36774'] = $key->G1699_C36774;

                $datos[$i]['G1699_C36775'] = $key->G1699_C36775;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1699";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1699_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1699_ConsInte__b as id,  G1699_C36754 as camp1 , G1699_C36755 as camp2 
                     FROM ".$BaseDatos.".G1699  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1699_ConsInte__b as id,  G1699_C36754 as camp1 , G1699_C36755 as camp2  
                    FROM ".$BaseDatos.".G1699  JOIN ".$BaseDatos.".G1699_M".$_POST['muestra']." ON G1699_ConsInte__b = G1699_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1699_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1699_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1699_C36754 LIKE '%".$B."%' OR G1699_C36755 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1699_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G1699_C36748'])){
                                $Ysql = "SELECT G2043_ConsInte__b as id, G2043_C37408 as text FROM ".$BaseDatos.".G2043 WHERE G2043_C37408 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1699_C36748"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2043 WHERE G2043_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1699_C36748"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G1699_C36778'])){
                                $Ysql = "SELECT G2005_ConsInte__b as id, G2005_C36697 as text FROM ".$BaseDatos.".G2005 WHERE G2005_C36697 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1699_C36778"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2005 WHERE G2005_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1699_C36778"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }
                            if(isset($_GET['CallDatosCombo_Guion_G1699_C36762'])){
                                $Ysql = "SELECT G2043_ConsInte__b as id, G2043_C37408 as text FROM ".$BaseDatos.".G2043 WHERE G2043_C37408 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1699_C36762"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2043 WHERE G2043_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1699_C36762"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1699");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1699_ConsInte__b, G1699_FechaInsercion , G1699_Usuario ,  G1699_CodigoMiembro  , G1699_PoblacionOrigen , G1699_EstadoDiligenciamiento ,  G1699_IdLlamada , G1699_C36754 as principal ,G1699_C37402,G1699_C30647,G1699_C36702,G1699_C36739,G1699_C36753,G1699_C36741, a.LISOPC_Nombre____b as G1699_C30635, b.LISOPC_Nombre____b as G1699_C30636,G1699_C30637,G1699_C30638,G1699_C30639,G1699_C30640,G1699_C30641,G1699_C30642,G1699_C30643,G1699_C36742,G1699_C36743, c.LISOPC_Nombre____b as G1699_C36744, G2043_C37408,G1699_C36747, d.LISOPC_Nombre____b as G1699_C36750, e.LISOPC_Nombre____b as G1699_C36751, f.LISOPC_Nombre____b as G1699_C36779,G1699_C36780,G1699_C36781, g.LISOPC_Nombre____b as G1699_C36782, h.LISOPC_Nombre____b as G1699_C36783,G1699_C36754,G1699_C36755,G1699_C36756, i.LISOPC_Nombre____b as G1699_C36757,G1699_C36758,G1699_C36759, j.LISOPC_Nombre____b as G1699_C36772, G2005_C36697,G1699_C37299, k.LISOPC_Nombre____b as G1699_C36771,G1699_C36760, G2043_C37408,G1699_C36761,G1699_C36763,G1699_C36764,G1699_C36765,G1699_C36766,G1699_C36767,G1699_C36768, l.LISOPC_Nombre____b as G1699_C36769,G1699_C36770, m.LISOPC_Nombre____b as G1699_C36773,G1699_C36774,G1699_C36775 FROM '.$BaseDatos.'.G1699 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1699_C30635 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1699_C30636 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1699_C36744 LEFT JOIN '.$BaseDatos.'.G2043 ON G2043_ConsInte__b  =  G1699_C36748 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1699_C36750 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1699_C36751 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1699_C36779 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1699_C36782 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1699_C36783 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1699_C36757 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1699_C36772 LEFT JOIN '.$BaseDatos.'.G2005 ON G2005_ConsInte__b  =  G1699_C36778 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1699_C36771 LEFT JOIN '.$BaseDatos.'.G2043 ON G2043_ConsInte__b  =  G1699_C36762 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G1699_C36769 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G1699_C36773';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1699_C30638)){
                    $hora_a = explode(' ', $fila->G1699_C30638)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1699_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1699_ConsInte__b , ($fila->G1699_C37402) , ($fila->G1699_C30647) , ($fila->G1699_C36702) , ($fila->G1699_C36739) , ($fila->G1699_C36753) , ($fila->G1699_C36741) , ($fila->G1699_C30635) , ($fila->G1699_C30636) , explode(' ', $fila->G1699_C30637)[0] , $hora_a , ($fila->G1699_C30639) , ($fila->G1699_C30640) , ($fila->G1699_C30641) , ($fila->G1699_C30642) , ($fila->G1699_C30643) , ($fila->G1699_C36742) , ($fila->G1699_C36743) , ($fila->G1699_C36744) , ($fila->G2043_C37408) , ($fila->G1699_C36747) , ($fila->G1699_C36750) , ($fila->G1699_C36751) , ($fila->G1699_C36779) , ($fila->G1699_C36780) , ($fila->G1699_C36781) , ($fila->G1699_C36782) , ($fila->G1699_C36783) , ($fila->G1699_C36754) , ($fila->G1699_C36755) , ($fila->G1699_C36756) , ($fila->G1699_C36757) , explode(' ', $fila->G1699_C36758)[0] , ($fila->G1699_C36759) , ($fila->G1699_C36772) , ($fila->G2005_C36697) , ($fila->G1699_C37299) , ($fila->G1699_C36771) , ($fila->G1699_C36760) , ($fila->G2043_C37408) , ($fila->G1699_C36761) , ($fila->G1699_C36763) , ($fila->G1699_C36764) , ($fila->G1699_C36765) , ($fila->G1699_C36766) , ($fila->G1699_C36767) , ($fila->G1699_C36768) , ($fila->G1699_C36769) , ($fila->G1699_C36770) , ($fila->G1699_C36773) , ($fila->G1699_C36774) , ($fila->G1699_C36775) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1699 WHERE G1699_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1699";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1699_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1699_ConsInte__b as id,  G1699_C36754 as camp1 , G1699_C36755 as camp2  FROM '.$BaseDatos.'.G1699 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1699_ConsInte__b as id,  G1699_C36754 as camp1 , G1699_C36755 as camp2  
                    FROM ".$BaseDatos.".G1699  JOIN ".$BaseDatos.".G1699_M".$_POST['muestra']." ON G1699_ConsInte__b = G1699_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1699_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1699_C36754 LIKE "%'.$B.'%" OR G1699_C36755 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1699_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1699 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1699(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1699_C37402"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C37402 = '".$_POST["G1699_C37402"]."'";
                $LsqlI .= $separador."G1699_C37402";
                $LsqlV .= $separador."'".$_POST["G1699_C37402"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C30646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C30646 = '".$_POST["G1699_C30646"]."'";
                $LsqlI .= $separador."G1699_C30646";
                $LsqlV .= $separador."'".$_POST["G1699_C30646"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C30647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C30647 = '".$_POST["G1699_C30647"]."'";
                $LsqlI .= $separador."G1699_C30647";
                $LsqlV .= $separador."'".$_POST["G1699_C30647"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36702"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36702 = '".$_POST["G1699_C36702"]."'";
                $LsqlI .= $separador."G1699_C36702";
                $LsqlV .= $separador."'".$_POST["G1699_C36702"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36739"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36739 = '".$_POST["G1699_C36739"]."'";
                $LsqlI .= $separador."G1699_C36739";
                $LsqlV .= $separador."'".$_POST["G1699_C36739"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36753"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36753 = '".$_POST["G1699_C36753"]."'";
                $LsqlI .= $separador."G1699_C36753";
                $LsqlV .= $separador."'".$_POST["G1699_C36753"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36741"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36741 = '".$_POST["G1699_C36741"]."'";
                $LsqlI .= $separador."G1699_C36741";
                $LsqlV .= $separador."'".$_POST["G1699_C36741"]."'";
                $validar = 1;
            }
             
 
            $G1699_C30635 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1699_C30635 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1699_C30635 = ".$G1699_C30635;
                    $LsqlI .= $separador." G1699_C30635";
                    $LsqlV .= $separador.$G1699_C30635;
                    $validar = 1;

                    
                }
            }
 
            $G1699_C30636 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1699_C30636 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1699_C30636 = ".$G1699_C30636;
                    $LsqlI .= $separador." G1699_C30636";
                    $LsqlV .= $separador.$G1699_C30636;
                    $validar = 1;
                }
            }
 
            $G1699_C30637 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1699_C30637 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1699_C30637 = ".$G1699_C30637;
                    $LsqlI .= $separador." G1699_C30637";
                    $LsqlV .= $separador.$G1699_C30637;
                    $validar = 1;
                }
            }
 
            $G1699_C30638 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1699_C30638 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1699_C30638 = ".$G1699_C30638;
                    $LsqlI .= $separador." G1699_C30638";
                    $LsqlV .= $separador.$G1699_C30638;
                    $validar = 1;
                }
            }
 
            $G1699_C30639 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1699_C30639 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1699_C30639 = ".$G1699_C30639;
                    $LsqlI .= $separador." G1699_C30639";
                    $LsqlV .= $separador.$G1699_C30639;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1699_C30640"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C30640 = '".$_POST["G1699_C30640"]."'";
                $LsqlI .= $separador."G1699_C30640";
                $LsqlV .= $separador."'".$_POST["G1699_C30640"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C30641"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C30641 = '".$_POST["G1699_C30641"]."'";
                $LsqlI .= $separador."G1699_C30641";
                $LsqlV .= $separador."'".$_POST["G1699_C30641"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C30642"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C30642 = '".$_POST["G1699_C30642"]."'";
                $LsqlI .= $separador."G1699_C30642";
                $LsqlV .= $separador."'".$_POST["G1699_C30642"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C30643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C30643 = '".$_POST["G1699_C30643"]."'";
                $LsqlI .= $separador."G1699_C30643";
                $LsqlV .= $separador."'".$_POST["G1699_C30643"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36742"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36742 = '".$_POST["G1699_C36742"]."'";
                $LsqlI .= $separador."G1699_C36742";
                $LsqlV .= $separador."'".$_POST["G1699_C36742"]."'";
                $validar = 1;
            }
             
  
            $G1699_C36743 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1699_C36743"])){
                if($_POST["G1699_C36743"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1699_C36743 = $_POST["G1699_C36743"];
                    $LsqlU .= $separador." G1699_C36743 = ".$G1699_C36743."";
                    $LsqlI .= $separador." G1699_C36743";
                    $LsqlV .= $separador.$G1699_C36743;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1699_C36744"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36744 = '".$_POST["G1699_C36744"]."'";
                $LsqlI .= $separador."G1699_C36744";
                $LsqlV .= $separador."'".$_POST["G1699_C36744"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36748"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36748 = '".$_POST["G1699_C36748"]."'";
                $LsqlI .= $separador."G1699_C36748";
                $LsqlV .= $separador."'".$_POST["G1699_C36748"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36747"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36747 = '".$_POST["G1699_C36747"]."'";
                $LsqlI .= $separador."G1699_C36747";
                $LsqlV .= $separador."'".$_POST["G1699_C36747"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36750"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36750 = '".$_POST["G1699_C36750"]."'";
                $LsqlI .= $separador."G1699_C36750";
                $LsqlV .= $separador."'".$_POST["G1699_C36750"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36751"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36751 = '".$_POST["G1699_C36751"]."'";
                $LsqlI .= $separador."G1699_C36751";
                $LsqlV .= $separador."'".$_POST["G1699_C36751"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36779"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36779 = '".$_POST["G1699_C36779"]."'";
                $LsqlI .= $separador."G1699_C36779";
                $LsqlV .= $separador."'".$_POST["G1699_C36779"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36780"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36780 = '".$_POST["G1699_C36780"]."'";
                $LsqlI .= $separador."G1699_C36780";
                $LsqlV .= $separador."'".$_POST["G1699_C36780"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36781"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36781 = '".$_POST["G1699_C36781"]."'";
                $LsqlI .= $separador."G1699_C36781";
                $LsqlV .= $separador."'".$_POST["G1699_C36781"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36782"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36782 = '".$_POST["G1699_C36782"]."'";
                $LsqlI .= $separador."G1699_C36782";
                $LsqlV .= $separador."'".$_POST["G1699_C36782"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36783"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36783 = '".$_POST["G1699_C36783"]."'";
                $LsqlI .= $separador."G1699_C36783";
                $LsqlV .= $separador."'".$_POST["G1699_C36783"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36754"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36754 = '".$_POST["G1699_C36754"]."'";
                $LsqlI .= $separador."G1699_C36754";
                $LsqlV .= $separador."'".$_POST["G1699_C36754"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36755"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36755 = '".$_POST["G1699_C36755"]."'";
                $LsqlI .= $separador."G1699_C36755";
                $LsqlV .= $separador."'".$_POST["G1699_C36755"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36756"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36756 = '".$_POST["G1699_C36756"]."'";
                $LsqlI .= $separador."G1699_C36756";
                $LsqlV .= $separador."'".$_POST["G1699_C36756"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36757"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36757 = '".$_POST["G1699_C36757"]."'";
                $LsqlI .= $separador."G1699_C36757";
                $LsqlV .= $separador."'".$_POST["G1699_C36757"]."'";
                $validar = 1;
            }
             
 
            $G1699_C36758 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1699_C36758"])){    
                if($_POST["G1699_C36758"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1699_C36758"]);
                    if(count($tieneHora) > 1){
                        $G1699_C36758 = "'".$_POST["G1699_C36758"]."'";
                    }else{
                        $G1699_C36758 = "'".str_replace(' ', '',$_POST["G1699_C36758"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1699_C36758 = ".$G1699_C36758;
                    $LsqlI .= $separador." G1699_C36758";
                    $LsqlV .= $separador.$G1699_C36758;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1699_C36759"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36759 = '".$_POST["G1699_C36759"]."'";
                $LsqlI .= $separador."G1699_C36759";
                $LsqlV .= $separador."'".$_POST["G1699_C36759"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36772"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36772 = '".$_POST["G1699_C36772"]."'";
                $LsqlI .= $separador."G1699_C36772";
                $LsqlV .= $separador."'".$_POST["G1699_C36772"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36778"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36778 = '".$_POST["G1699_C36778"]."'";
                $LsqlI .= $separador."G1699_C36778";
                $LsqlV .= $separador."'".$_POST["G1699_C36778"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C37299"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C37299 = '".$_POST["G1699_C37299"]."'";
                $LsqlI .= $separador."G1699_C37299";
                $LsqlV .= $separador."'".$_POST["G1699_C37299"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36771"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36771 = '".$_POST["G1699_C36771"]."'";
                $LsqlI .= $separador."G1699_C36771";
                $LsqlV .= $separador."'".$_POST["G1699_C36771"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36760"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36760 = '".$_POST["G1699_C36760"]."'";
                $LsqlI .= $separador."G1699_C36760";
                $LsqlV .= $separador."'".$_POST["G1699_C36760"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36762"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36762 = '".$_POST["G1699_C36762"]."'";
                $LsqlI .= $separador."G1699_C36762";
                $LsqlV .= $separador."'".$_POST["G1699_C36762"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36761"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36761 = '".$_POST["G1699_C36761"]."'";
                $LsqlI .= $separador."G1699_C36761";
                $LsqlV .= $separador."'".$_POST["G1699_C36761"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36763"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36763 = '".$_POST["G1699_C36763"]."'";
                $LsqlI .= $separador."G1699_C36763";
                $LsqlV .= $separador."'".$_POST["G1699_C36763"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36764"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36764 = '".$_POST["G1699_C36764"]."'";
                $LsqlI .= $separador."G1699_C36764";
                $LsqlV .= $separador."'".$_POST["G1699_C36764"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36765"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36765 = '".$_POST["G1699_C36765"]."'";
                $LsqlI .= $separador."G1699_C36765";
                $LsqlV .= $separador."'".$_POST["G1699_C36765"]."'";
                $validar = 1;
            }
             
  
            $G1699_C36766 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1699_C36766"])){
                if($_POST["G1699_C36766"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1699_C36766 = $_POST["G1699_C36766"];
                    $LsqlU .= $separador." G1699_C36766 = ".$G1699_C36766."";
                    $LsqlI .= $separador." G1699_C36766";
                    $LsqlV .= $separador.$G1699_C36766;
                    $validar = 1;
                }
            }
  
            $G1699_C36767 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1699_C36767"])){
                if($_POST["G1699_C36767"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1699_C36767 = $_POST["G1699_C36767"];
                    $LsqlU .= $separador." G1699_C36767 = ".$G1699_C36767."";
                    $LsqlI .= $separador." G1699_C36767";
                    $LsqlV .= $separador.$G1699_C36767;
                    $validar = 1;
                }
            }
  
            $G1699_C36768 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1699_C36768"])){
                if($_POST["G1699_C36768"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1699_C36768 = $_POST["G1699_C36768"];
                    $LsqlU .= $separador." G1699_C36768 = ".$G1699_C36768."";
                    $LsqlI .= $separador." G1699_C36768";
                    $LsqlV .= $separador.$G1699_C36768;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1699_C36769"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36769 = '".$_POST["G1699_C36769"]."'";
                $LsqlI .= $separador."G1699_C36769";
                $LsqlV .= $separador."'".$_POST["G1699_C36769"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36770"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36770 = '".$_POST["G1699_C36770"]."'";
                $LsqlI .= $separador."G1699_C36770";
                $LsqlV .= $separador."'".$_POST["G1699_C36770"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36773"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36773 = '".$_POST["G1699_C36773"]."'";
                $LsqlI .= $separador."G1699_C36773";
                $LsqlV .= $separador."'".$_POST["G1699_C36773"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36774"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36774 = '".$_POST["G1699_C36774"]."'";
                $LsqlI .= $separador."G1699_C36774";
                $LsqlV .= $separador."'".$_POST["G1699_C36774"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36775"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36775 = '".$_POST["G1699_C36775"]."'";
                $LsqlI .= $separador."G1699_C36775";
                $LsqlV .= $separador."'".$_POST["G1699_C36775"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36776"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36776 = '".$_POST["G1699_C36776"]."'";
                $LsqlI .= $separador."G1699_C36776";
                $LsqlV .= $separador."'".$_POST["G1699_C36776"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1699_C36777"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_C36777 = '".$_POST["G1699_C36777"]."'";
                $LsqlI .= $separador."G1699_C36777";
                $LsqlV .= $separador."'".$_POST["G1699_C36777"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1699_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1699_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1699_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1699_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1699_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1699_Usuario , G1699_FechaInsercion, G1699_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1699_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1699 WHERE G1699_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2050_ConsInte__b, G2050_C37665, G2050_C37666, G2050_C37667, G2050_C37668, G2050_C37669, G2050_C37670, G2050_C37671, G2050_C37694 FROM ".$BaseDatos.".G2050  ";

        $SQL .= " WHERE G2050_C37665 = '".$numero."'"; 

        $SQL .= " ORDER BY G2050_C37665";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2050_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2050_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2050_C37665)."</cell>";

                echo "<cell>". ($fila->G2050_C37666)."</cell>";

                echo "<cell>". ($fila->G2050_C37667)."</cell>";

                echo "<cell>". ($fila->G2050_C37668)."</cell>";

                echo "<cell>". ($fila->G2050_C37669)."</cell>";

                echo "<cell>". ($fila->G2050_C37670)."</cell>";

                echo "<cell>". ($fila->G2050_C37671)."</cell>";

                echo "<cell>". ($fila->G2050_C37694)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2050 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2050(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2050_C37666"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37666 = '".$_POST["G2050_C37666"]."'";
                    $LsqlI .= $separador."G2050_C37666";
                    $LsqlV .= $separador."'".$_POST["G2050_C37666"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2050_C37667"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37667 = '".$_POST["G2050_C37667"]."'";
                    $LsqlI .= $separador."G2050_C37667";
                    $LsqlV .= $separador."'".$_POST["G2050_C37667"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2050_C37668"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37668 = '".$_POST["G2050_C37668"]."'";
                    $LsqlI .= $separador."G2050_C37668";
                    $LsqlV .= $separador."'".$_POST["G2050_C37668"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2050_C37669"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37669 = '".$_POST["G2050_C37669"]."'";
                    $LsqlI .= $separador."G2050_C37669";
                    $LsqlV .= $separador."'".$_POST["G2050_C37669"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2050_C37670"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37670 = '".$_POST["G2050_C37670"]."'";
                    $LsqlI .= $separador."G2050_C37670";
                    $LsqlV .= $separador."'".$_POST["G2050_C37670"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2050_C37671"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37671 = '".$_POST["G2050_C37671"]."'";
                    $LsqlI .= $separador."G2050_C37671";
                    $LsqlV .= $separador."'".$_POST["G2050_C37671"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2050_C37694"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2050_C37694 = '".$_POST["G2050_C37694"]."'";
                    $LsqlI .= $separador."G2050_C37694";
                    $LsqlV .= $separador."'".$_POST["G2050_C37694"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2050_C37665 = $numero;
                    $LsqlU .= ", G2050_C37665 = ".$G2050_C37665."";
                    $LsqlI .= ", G2050_C37665";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2050_Usuario ,  G2050_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2050_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2050 WHERE  G2050_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
