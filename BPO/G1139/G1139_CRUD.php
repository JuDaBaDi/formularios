<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1139 WHERE G1139_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }


      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1139_ConsInte__b, G1139_FechaInsercion , USUARI_Nombre____b ,G1139_CodigoMiembro,G1139_PoblacionOrigen , G1139_EstadoDiligenciamiento ,  G1139_IdLlamada , G1139_C17589 as principal ,G1139_C20682,G1139_CodigoMiembro,G1139_C17589,G1139_C17590,G1139_C19164,G1139_C20683,G1139_C20684,G1139_C17591,G1139_C17592,G1139_C17593,G1139_C17594,G1139_C17595,G1139_C17596,G1139_C17597,G1139_C17598,G1139_C17599,G1139_C17600,G1139_C19165,G1139_C19166,G1139_C19167,G1139_C19168,G1139_C22661,G1139_C17601,G1139_C17602,G1139_C17603,G1139_C17604,G1139_C17605,G1139_C17606,G1139_C17607,G1139_C17608,G1139_C17609,G1139_C17610,G1139_C17611,G1139_C17612,G1139_C17613,G1139_C19169,G1139_C19170,G1139_C19171,G1139_C19172,G1139_C19173,G1139_C19174 FROM '.$BaseDatos.'.G1139 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1139_Usuario WHERE G1139_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1139_C20682'] = $key->G1139_C20682;

                $datos[$i]['G1139_CodigoMiembro'] = $key->G1139_CodigoMiembro;

                $datos[$i]['G1139_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1139_C17589'] = $key->G1139_C17589;

                $datos[$i]['G1139_C17590'] = $key->G1139_C17590;

                $datos[$i]['G1139_C19164'] = $key->G1139_C19164;

                $datos[$i]['G1139_C20683'] = $key->G1139_C20683;

                $datos[$i]['G1139_C20684'] = $key->G1139_C20684;

                $datos[$i]['G1139_C17591'] = $key->G1139_C17591;

                $datos[$i]['G1139_C17592'] = $key->G1139_C17592;

                $datos[$i]['G1139_C17593'] = $key->G1139_C17593;

                $datos[$i]['G1139_C17594'] = $key->G1139_C17594;

                $datos[$i]['G1139_C17595'] = $key->G1139_C17595;

                $datos[$i]['G1139_C17596'] = $key->G1139_C17596;

                $datos[$i]['G1139_C17597'] = $key->G1139_C17597;

                $datos[$i]['G1139_C17598'] = $key->G1139_C17598;

                $datos[$i]['G1139_C17599'] = $key->G1139_C17599;

                $datos[$i]['G1139_C17600'] = $key->G1139_C17600;

                $datos[$i]['G1139_C19165'] = $key->G1139_C19165;

                $datos[$i]['G1139_C19166'] = $key->G1139_C19166;

                $datos[$i]['G1139_C19167'] = explode(' ', $key->G1139_C19167)[0];

                $datos[$i]['G1139_C19168'] = explode(' ', $key->G1139_C19168)[0];

                $datos[$i]['G1139_C22661'] = $key->G1139_C22661;

                $datos[$i]['G1139_C17601'] = $key->G1139_C17601;

                $datos[$i]['G1139_C17602'] = $key->G1139_C17602;

                $datos[$i]['G1139_C17603'] = $key->G1139_C17603;

                $datos[$i]['G1139_C17604'] = $key->G1139_C17604;

                $datos[$i]['G1139_C17605'] = explode(' ', $key->G1139_C17605)[0];
  
                $hora = '';
                if(!is_null($key->G1139_C17606)){
                    $hora = explode(' ', $key->G1139_C17606)[1];
                }

                $datos[$i]['G1139_C17607'] = $key->G1139_C17607;

                $datos[$i]['G1139_C17608'] = $key->G1139_C17608;

                $datos[$i]['G1139_C17609'] = $key->G1139_C17609;

                $datos[$i]['G1139_C17610'] = $key->G1139_C17610;

                $datos[$i]['G1139_C17611'] = $key->G1139_C17611;

                $datos[$i]['G1139_C17612'] = $key->G1139_C17612;

                $datos[$i]['G1139_C17613'] = $key->G1139_C17613;

                $datos[$i]['G1139_C19169'] = $key->G1139_C19169;

                $datos[$i]['G1139_C19170'] = $key->G1139_C19170;

                $datos[$i]['G1139_C19171'] = $key->G1139_C19171;

                $datos[$i]['G1139_C19172'] = $key->G1139_C19172;

                $datos[$i]['G1139_C19173'] = $key->G1139_C19173;

                $datos[$i]['G1139_C19174'] = $key->G1139_C19174;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b  WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1139_ConsInte__b as id,  G1139_C17589 as camp1 , G1139_C17591 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1139 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1139_C17602  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1139_C17589 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1139_C17591 like '%".$_POST['Busqueda']."%' ) ";
            }
            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1139_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1139_C20682']) && $_POST['G1139_C20682'] != ''){
				$Lsql .= " AND G1139_C20682 LIKE '%". $_POST['G1139_C20682'] ."%' ";
			}
			if(!is_null($_POST['G1139_C17589']) && $_POST['G1139_C17589'] != ''){
				$Lsql .= " AND G1139_C17589 LIKE '%". $_POST['G1139_C17589'] ."%' ";
			}
			if(!is_null($_POST['G1139_C17590']) && $_POST['G1139_C17590'] != ''){
				$Lsql .= " AND G1139_C17590 LIKE '%". $_POST['G1139_C17590'] ."%' ";
			}
			if(!is_null($_POST['G1139_C17591']) && $_POST['G1139_C17591'] != ''){
				$Lsql .= " AND G1139_C17591 LIKE '%". $_POST['G1139_C17591'] ."%' ";
			}

            $Lsql .= " ORDER BY FIELD(G1139_C17602,14304,14305,14306,14307,14308), G1139_FechaInsercion DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1139_C19171'])){
            echo '<select class="form-control input-sm"  name="G1139_C19171" id="G1139_C19171">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1139_C19171'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1139_C19171'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1139_C19171'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1139");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1139_ConsInte__b, G1139_FechaInsercion , G1139_Usuario ,  G1139_CodigoMiembro  , G1139_PoblacionOrigen , G1139_EstadoDiligenciamiento ,  G1139_IdLlamada , G1139_C17589 as principal ,G1139_C20682,G1139_C17589,G1139_C17590,G1139_C19164,G1139_C20683,G1139_C20684,G1139_C17591,G1139_C17592,G1139_C17593,G1139_C17594,G1139_C17595,G1139_C17596,G1139_C17597,G1139_C17598,G1139_C17599,G1139_C17600,G1139_C19165, a.LISOPC_Nombre____b as G1139_C19166,G1139_C19167,G1139_C19168,G1139_C22661, b.LISOPC_Nombre____b as G1139_C17601, c.LISOPC_Nombre____b as G1139_C17602,G1139_C17603,G1139_C17604,G1139_C17605,G1139_C17606,G1139_C17607,G1139_C17608,G1139_C17609,G1139_C17610,G1139_C17611, d.LISOPC_Nombre____b as G1139_C17612,G1139_C17613, e.LISOPC_Nombre____b as G1139_C19169, f.LISOPC_Nombre____b as G1139_C19170,G1139_C19172,G1139_C19173,G1139_C19174 FROM '.$BaseDatos.'.G1139 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1139_C19166 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1139_C17601 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1139_C17602 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1139_C17612 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1139_C19169 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1139_C19170 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1139_C19171';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1139_C17606)){
                    $hora_a = explode(' ', $fila->G1139_C17606)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1139_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1139_ConsInte__b , ($fila->G1139_C20682) , ($fila->G1139_C17589) , ($fila->G1139_C17590) , ($fila->G1139_C19164) , ($fila->G1139_C20683) , ($fila->G1139_C20684) , ($fila->G1139_C17591) , ($fila->G1139_C17592) , ($fila->G1139_C17593) , ($fila->G1139_C17594) , ($fila->G1139_C17595) , ($fila->G1139_C17596) , ($fila->G1139_C17597) , ($fila->G1139_C17598) , ($fila->G1139_C17599) , ($fila->G1139_C17600) , ($fila->G1139_C19165) , ($fila->G1139_C19166) , explode(' ', $fila->G1139_C19167)[0] , explode(' ', $fila->G1139_C19168)[0] , ($fila->G1139_C22661) , ($fila->G1139_C17601) , ($fila->G1139_C17602) , ($fila->G1139_C17603) , ($fila->G1139_C17604) , explode(' ', $fila->G1139_C17605)[0] , $hora_a , ($fila->G1139_C17607) , ($fila->G1139_C17608) , ($fila->G1139_C17609) , ($fila->G1139_C17610) , ($fila->G1139_C17611) , ($fila->G1139_C17612) , ($fila->G1139_C17613) , ($fila->G1139_C19169) , ($fila->G1139_C19170) , ($fila->G1139_C19172) , ($fila->G1139_C19173) , ($fila->G1139_C19174) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1139 WHERE G1139_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1139_ConsInte__b as id,  G1139_C17589 as camp1 , G1139_C17591 as camp2, G1139_C17602 as estado FROM '.$BaseDatos.'.G1139 ORDER BY FIELD(G1139_C17602,14280,14281,14282,14283,14284), G1139_FechaInsercion DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                
                $color = 'color:green;';
                $strIconoBackOffice = 'Cerrada';
                if($obj->estado == '14281' || $obj->estado == '14282'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14283'){
                    $color = 'color:green;';
                    $strIconoBackOffice = 'Cerrada';
                }else if($obj->estado == '14284'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }
                else if($obj->estado == '14280'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1139 SET "; 
  
            if(isset($_POST["G1139_C20682"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C20682 = '".$_POST["G1139_C20682"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17589"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17589 = '".$_POST["G1139_C17589"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17590 = '".$_POST["G1139_C17590"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C19164"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19164 = '".$_POST["G1139_C19164"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C20683"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C20683 = '".$_POST["G1139_C20683"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C20684"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C20684 = '".$_POST["G1139_C20684"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17591"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17591 = '".$_POST["G1139_C17591"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17592"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17592 = '".$_POST["G1139_C17592"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17593"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17593 = '".$_POST["G1139_C17593"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17594"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17594 = '".$_POST["G1139_C17594"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17595"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17595 = '".$_POST["G1139_C17595"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17596"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17596 = '".$_POST["G1139_C17596"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17597"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17597 = '".$_POST["G1139_C17597"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17598"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17598 = '".$_POST["G1139_C17598"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17599"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17599 = '".$_POST["G1139_C17599"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17600"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17600 = '".$_POST["G1139_C17600"]."'";
                $validar = 1;
            }
             
  
            $G1139_C19165 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C19165"])){
                if($_POST["G1139_C19165"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19165 = $_POST["G1139_C19165"];
                    $LsqlU .= $separador." G1139_C19165 = ".$G1139_C19165."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1139_C19166"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19166 = '".$_POST["G1139_C19166"]."'";
                $validar = 1;
            }
             
 
            $G1139_C19167 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1139_C19167"])){    
                if($_POST["G1139_C19167"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1139_C19167"]);
                    if(count($tieneHora) > 1){
                    	$G1139_C19167 = "'".$_POST["G1139_C19167"]."'";
                    }else{
                    	$G1139_C19167 = "'".str_replace(' ', '',$_POST["G1139_C19167"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1139_C19167 = ".$G1139_C19167;
                    $validar = 1;
                }
            }
 
            $G1139_C19168 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1139_C19168"])){    
                if($_POST["G1139_C19168"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1139_C19168"]);
                    if(count($tieneHora) > 1){
                    	$G1139_C19168 = "'".$_POST["G1139_C19168"]."'";
                    }else{
                    	$G1139_C19168 = "'".str_replace(' ', '',$_POST["G1139_C19168"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1139_C19168 = ".$G1139_C19168;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1139_C22661"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C22661 = '".$_POST["G1139_C22661"]."'";
                $validar = 1;
            }
             
 
            $G1139_C17601 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1139_C17601 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1139_C17601 = ".$G1139_C17601;
                    $validar = 1;

                    
                }
            }
 
            $G1139_C17602 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1139_C17602 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1139_C17602 = ".$G1139_C17602;
                    $validar = 1;
                }
            }
  
            $G1139_C17603 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C17603"])){
                if($_POST["G1139_C17603"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C17603 = $_POST["G1139_C17603"];
                    $LsqlU .= $separador." G1139_C17603 = ".$G1139_C17603."";
                    $validar = 1;
                }
            }
  
            $G1139_C17604 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C17604"])){
                if($_POST["G1139_C17604"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C17604 = $_POST["G1139_C17604"];
                    $LsqlU .= $separador." G1139_C17604 = ".$G1139_C17604."";
                    $validar = 1;
                }
            }
 
            $G1139_C17607 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1139_C17607 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1139_C17607 = ".$G1139_C17607;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1139_C17608"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17608 = '".$_POST["G1139_C17608"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17609"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17609 = '".$_POST["G1139_C17609"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17610"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17610 = '".$_POST["G1139_C17610"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17611"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17611 = '".$_POST["G1139_C17611"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17612"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17612 = '".$_POST["G1139_C17612"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C17613"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C17613 = '".$_POST["G1139_C17613"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C19169"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19169 = '".$_POST["G1139_C19169"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C19170"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19170 = '".$_POST["G1139_C19170"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1139_C19171"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19171 = '".$_POST["G1139_C19171"]."'";
                $validar = 1;
            }
             
  
            $G1139_C19172 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C19172"])){
                if($_POST["G1139_C19172"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19172 = $_POST["G1139_C19172"];
                    $LsqlU .= $separador." G1139_C19172 = ".$G1139_C19172."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1139_C19173"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_C19173 = '".$_POST["G1139_C19173"]."'";
                $validar = 1;
            }
             
  
            $G1139_C19174 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1139_C19174"])){
                if($_POST["G1139_C19174"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1139_C19174 = $_POST["G1139_C19174"];
                    $LsqlU .= $separador." G1139_C19174 = ".$G1139_C19174."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1139_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
             if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1139_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
            }
            
			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1139_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
