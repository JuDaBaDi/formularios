<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2105_ConsInte__b, G2105_FechaInsercion , G2105_Usuario ,  G2105_CodigoMiembro  , G2105_PoblacionOrigen , G2105_EstadoDiligenciamiento ,  G2105_IdLlamada , G2105_C38764 as principal ,G2105_C38603,G2105_C38604,G2105_C38605,G2105_C38606,G2105_C38607,G2105_C38608,G2105_C38609,G2105_C38610,G2105_C38611,G2105_C38626,G2105_C38627,G2105_C38628,G2105_C38629,G2105_C38630,G2105_C38631,G2105_C38764,G2105_C38765,G2105_C38766,G2105_C38792,G2105_C38636,G2105_C39073,G2105_C38637,G2105_C39167,G2105_C39171,G2105_C39169 FROM '.$BaseDatos.'.G2105 WHERE G2105_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2105_C38603'] = $key->G2105_C38603;

                $datos[$i]['G2105_C38604'] = $key->G2105_C38604;

                $datos[$i]['G2105_C38605'] = explode(' ', $key->G2105_C38605)[0];
  
                $hora = '';
                if(!is_null($key->G2105_C38606)){
                    $hora = explode(' ', $key->G2105_C38606)[1];
                }

                $datos[$i]['G2105_C38606'] = $hora;

                $datos[$i]['G2105_C38607'] = $key->G2105_C38607;

                $datos[$i]['G2105_C38608'] = $key->G2105_C38608;

                $datos[$i]['G2105_C38609'] = $key->G2105_C38609;

                $datos[$i]['G2105_C38610'] = $key->G2105_C38610;

                $datos[$i]['G2105_C38611'] = $key->G2105_C38611;

                $datos[$i]['G2105_C38626'] = $key->G2105_C38626;

                $datos[$i]['G2105_C38627'] = $key->G2105_C38627;

                $datos[$i]['G2105_C38628'] = $key->G2105_C38628;

                $datos[$i]['G2105_C38629'] = $key->G2105_C38629;

                $datos[$i]['G2105_C38630'] = $key->G2105_C38630;

                $datos[$i]['G2105_C38631'] = $key->G2105_C38631;

                $datos[$i]['G2105_C38764'] = $key->G2105_C38764;

                $datos[$i]['G2105_C38765'] = $key->G2105_C38765;

                $datos[$i]['G2105_C38766'] = $key->G2105_C38766;

                $datos[$i]['G2105_C38792'] = $key->G2105_C38792;

                $datos[$i]['G2105_C38636'] = $key->G2105_C38636;

                $datos[$i]['G2105_C39073'] = $key->G2105_C39073;

                $datos[$i]['G2105_C38637'] = $key->G2105_C38637;

                $datos[$i]['G2105_C39167'] = $key->G2105_C39167;

                $datos[$i]['G2105_C39171'] = $key->G2105_C39171;

                $datos[$i]['G2105_C39169'] = $key->G2105_C39169;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2105";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2105_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2105_ConsInte__b as id,  G2105_C38629 as camp2 , G2105_C38764 as camp1 
                     FROM ".$BaseDatos.".G2105  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2105_ConsInte__b as id,  G2105_C38629 as camp2 , G2105_C38764 as camp1  
                    FROM ".$BaseDatos.".G2105  JOIN ".$BaseDatos.".G2105_M".$_POST['muestra']." ON G2105_ConsInte__b = G2105_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2105_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2105_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2105_C38629 LIKE '%".$B."%' OR G2105_C38764 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2105_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2105");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2105_ConsInte__b, G2105_FechaInsercion , G2105_Usuario ,  G2105_CodigoMiembro  , G2105_PoblacionOrigen , G2105_EstadoDiligenciamiento ,  G2105_IdLlamada , G2105_C38764 as principal , a.LISOPC_Nombre____b as G2105_C38603, b.LISOPC_Nombre____b as G2105_C38604,G2105_C38605,G2105_C38606,G2105_C38607,G2105_C38608,G2105_C38609,G2105_C38610,G2105_C38611,G2105_C38626,G2105_C38627,G2105_C38628,G2105_C38629,G2105_C38630,G2105_C38631,G2105_C38764,G2105_C38765,G2105_C38766,G2105_C38792,G2105_C38636,G2105_C39073,G2105_C38637, c.LISOPC_Nombre____b as G2105_C39167,G2105_C39171,G2105_C39169 FROM '.$BaseDatos.'.G2105 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2105_C38603 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2105_C38604 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2105_C39167';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2105 WHERE G2105_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2105";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2105_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2105_ConsInte__b as id,  G2105_C38629 as camp2 , G2105_C38764 as camp1  FROM '.$BaseDatos.'.G2105 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2105_ConsInte__b as id,  G2105_C38629 as camp2 , G2105_C38764 as camp1  
                    FROM ".$BaseDatos.".G2105  JOIN ".$BaseDatos.".G2105_M".$_POST['muestra']." ON G2105_ConsInte__b = G2105_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2105_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2105_C38629 LIKE "%'.$B.'%" OR G2105_C38764 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2105_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2105 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2105(";
            $LsqlV = " VALUES ("; 
 
            $G2105_C38603 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2105_C38603 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2105_C38603 = ".$G2105_C38603;
                    $LsqlI .= $separador." G2105_C38603";
                    $LsqlV .= $separador.$G2105_C38603;
                    $validar = 1;

                    
                }
            }
 
            $G2105_C38604 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2105_C38604 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2105_C38604 = ".$G2105_C38604;
                    $LsqlI .= $separador." G2105_C38604";
                    $LsqlV .= $separador.$G2105_C38604;
                    $validar = 1;
                }
            }
 
            $G2105_C38605 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2105_C38605 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2105_C38605 = ".$G2105_C38605;
                    $LsqlI .= $separador." G2105_C38605";
                    $LsqlV .= $separador.$G2105_C38605;
                    $validar = 1;
                }
            }
 
            $G2105_C38606 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2105_C38606 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2105_C38606 = ".$G2105_C38606;
                    $LsqlI .= $separador." G2105_C38606";
                    $LsqlV .= $separador.$G2105_C38606;
                    $validar = 1;
                }
            }
 
            $G2105_C38607 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2105_C38607 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2105_C38607 = ".$G2105_C38607;
                    $LsqlI .= $separador." G2105_C38607";
                    $LsqlV .= $separador.$G2105_C38607;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2105_C38608"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38608 = '".$_POST["G2105_C38608"]."'";
                $LsqlI .= $separador."G2105_C38608";
                $LsqlV .= $separador."'".$_POST["G2105_C38608"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38609"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date('Y-m-d H:i:s');

                $LsqlU .= $separador."G2105_C38609 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2105_C38609";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38610"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date('H:i:s');

                $LsqlU .= $separador."G2105_C38610 = '".$strHora_t."'";
                $LsqlI .= $separador."G2105_C38610";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38611"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38611 = '".$_POST["G2105_C38611"]."'";
                $LsqlI .= $separador."G2105_C38611";
                $LsqlV .= $separador."'".$_POST["G2105_C38611"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38626"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38626 = '".$_POST["G2105_C38626"]."'";
                $LsqlI .= $separador."G2105_C38626";
                $LsqlV .= $separador."'".$_POST["G2105_C38626"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38627"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38627 = '".$_POST["G2105_C38627"]."'";
                $LsqlI .= $separador."G2105_C38627";
                $LsqlV .= $separador."'".$_POST["G2105_C38627"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38628"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38628 = '".$_POST["G2105_C38628"]."'";
                $LsqlI .= $separador."G2105_C38628";
                $LsqlV .= $separador."'".$_POST["G2105_C38628"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38629"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38629 = '".$_POST["G2105_C38629"]."'";
                $LsqlI .= $separador."G2105_C38629";
                $LsqlV .= $separador."'".$_POST["G2105_C38629"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38630"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38630 = '".$_POST["G2105_C38630"]."'";
                $LsqlI .= $separador."G2105_C38630";
                $LsqlV .= $separador."'".$_POST["G2105_C38630"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38631"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38631 = '".$_POST["G2105_C38631"]."'";
                $LsqlI .= $separador."G2105_C38631";
                $LsqlV .= $separador."'".$_POST["G2105_C38631"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38764"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38764 = '".$_POST["G2105_C38764"]."'";
                $LsqlI .= $separador."G2105_C38764";
                $LsqlV .= $separador."'".$_POST["G2105_C38764"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38765"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38765 = '".$_POST["G2105_C38765"]."'";
                $LsqlI .= $separador."G2105_C38765";
                $LsqlV .= $separador."'".$_POST["G2105_C38765"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38766"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38766 = '".$_POST["G2105_C38766"]."'";
                $LsqlI .= $separador."G2105_C38766";
                $LsqlV .= $separador."'".$_POST["G2105_C38766"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38792"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38792 = '".$_POST["G2105_C38792"]."'";
                $LsqlI .= $separador."G2105_C38792";
                $LsqlV .= $separador."'".$_POST["G2105_C38792"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38635"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38635 = '".$_POST["G2105_C38635"]."'";
                $LsqlI .= $separador."G2105_C38635";
                $LsqlV .= $separador."'".$_POST["G2105_C38635"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38636"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38636 = '".$_POST["G2105_C38636"]."'";
                $LsqlI .= $separador."G2105_C38636";
                $LsqlV .= $separador."'".$_POST["G2105_C38636"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39195"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39195 = '".$_POST["G2105_C39195"]."'";
                $LsqlI .= $separador."G2105_C39195";
                $LsqlV .= $separador."'".$_POST["G2105_C39195"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39073"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39073 = '".$_POST["G2105_C39073"]."'";
                $LsqlI .= $separador."G2105_C39073";
                $LsqlV .= $separador."'".$_POST["G2105_C39073"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39074"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39074 = '".$_POST["G2105_C39074"]."'";
                $LsqlI .= $separador."G2105_C39074";
                $LsqlV .= $separador."'".$_POST["G2105_C39074"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39075"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39075 = '".$_POST["G2105_C39075"]."'";
                $LsqlI .= $separador."G2105_C39075";
                $LsqlV .= $separador."'".$_POST["G2105_C39075"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C38637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C38637 = '".$_POST["G2105_C38637"]."'";
                $LsqlI .= $separador."G2105_C38637";
                $LsqlV .= $separador."'".$_POST["G2105_C38637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39164"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39164 = '".$_POST["G2105_C39164"]."'";
                $LsqlI .= $separador."G2105_C39164";
                $LsqlV .= $separador."'".$_POST["G2105_C39164"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39165"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39165 = '".$_POST["G2105_C39165"]."'";
                $LsqlI .= $separador."G2105_C39165";
                $LsqlV .= $separador."'".$_POST["G2105_C39165"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39166"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39166 = '".$_POST["G2105_C39166"]."'";
                $LsqlI .= $separador."G2105_C39166";
                $LsqlV .= $separador."'".$_POST["G2105_C39166"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39167"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39167 = '".$_POST["G2105_C39167"]."'";
                $LsqlI .= $separador."G2105_C39167";
                $LsqlV .= $separador."'".$_POST["G2105_C39167"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39171"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39171 = '".$_POST["G2105_C39171"]."'";
                $LsqlI .= $separador."G2105_C39171";
                $LsqlV .= $separador."'".$_POST["G2105_C39171"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39168"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39168 = '".$_POST["G2105_C39168"]."'";
                $LsqlI .= $separador."G2105_C39168";
                $LsqlV .= $separador."'".$_POST["G2105_C39168"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39169"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39169 = '".$_POST["G2105_C39169"]."'";
                $LsqlI .= $separador."G2105_C39169";
                $LsqlV .= $separador."'".$_POST["G2105_C39169"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2105_C39170"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_C39170 = '".$_POST["G2105_C39170"]."'";
                $LsqlI .= $separador."G2105_C39170";
                $LsqlV .= $separador."'".$_POST["G2105_C39170"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2105_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2105_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2105_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2105_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2105_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2105_Usuario , G2105_FechaInsercion, G2105_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2105_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2105 WHERE G2105_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
