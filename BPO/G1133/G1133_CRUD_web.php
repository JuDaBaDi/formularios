<?php

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
        
    //Inserciones o actualizaciones

    if(isset($_POST['getListaHija'])){
        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }
        
    if(isset($_POST["oper"])){
            $str_Lsql  = '';

            $validar = 0;
            $str_LsqlU = "UPDATE ".$BaseDatos.".G1133 SET "; 
            $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1133( G1133_FechaInsercion ,";
            $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G1133_C17446"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C17446 = '".$_POST["G1133_C17446"]."'";
            $str_LsqlI .= $separador."G1133_C17446";
            $str_LsqlV .= $separador."'".$_POST["G1133_C17446"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C17447"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C17447 = '".$_POST["G1133_C17447"]."'";
            $str_LsqlI .= $separador."G1133_C17447";
            $str_LsqlV .= $separador."'".$_POST["G1133_C17447"]."'";
            $validar = 1;
        }
         
 
        $G1133_C17448 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1133_C17448"])){    
            if($_POST["G1133_C17448"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1133_C17448 = "'".str_replace(' ', '',$_POST["G1133_C17448"])." 00:00:00'";
                $str_LsqlU .= $separador." G1133_C17448 = ".$G1133_C17448;
                $str_LsqlI .= $separador." G1133_C17448";
                $str_LsqlV .= $separador.$G1133_C17448;
                $validar = 1;
            }
        }
  
        $G1133_C17449 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1133_C17449"])){   
            if($_POST["G1133_C17449"] != '' && $_POST["G1133_C17449"] != 'undefined' && $_POST["G1133_C17449"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1133_C17449 = "'".$fecha." ".str_replace(' ', '',$_POST["G1133_C17449"])."'";
                $str_LsqlU .= $separador." G1133_C17449 = ".$G1133_C17449."";
                $str_LsqlI .= $separador." G1133_C17449";
                $str_LsqlV .= $separador.$G1133_C17449;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1133_C17450"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C17450 = '".$_POST["G1133_C17450"]."'";
            $str_LsqlI .= $separador."G1133_C17450";
            $str_LsqlV .= $separador."'".$_POST["G1133_C17450"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C21451"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C21451 = '".$_POST["G1133_C21451"]."'";
            $str_LsqlI .= $separador."G1133_C21451";
            $str_LsqlV .= $separador."'".$_POST["G1133_C21451"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C21452"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C21452 = '".$_POST["G1133_C21452"]."'";
            $str_LsqlI .= $separador."G1133_C21452";
            $str_LsqlV .= $separador."'".$_POST["G1133_C21452"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C18554"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C18554 = '".$_POST["G1133_C18554"]."'";
            $str_LsqlI .= $separador."G1133_C18554";
            $str_LsqlV .= $separador."'".$_POST["G1133_C18554"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C27095"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C27095 = '".$_POST["G1133_C27095"]."'";
            $str_LsqlI .= $separador."G1133_C27095";
            $str_LsqlV .= $separador."'".$_POST["G1133_C27095"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C27093"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C27093 = '".$_POST["G1133_C27093"]."'";
            $str_LsqlI .= $separador."G1133_C27093";
            $str_LsqlV .= $separador."'".$_POST["G1133_C27093"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C27094"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C27094 = '".$_POST["G1133_C27094"]."'";
            $str_LsqlI .= $separador."G1133_C27094";
            $str_LsqlV .= $separador."'".$_POST["G1133_C27094"]."'";
            $validar = 1;
        }
         
 
        $G1133_C20962 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1133_C20962"])){    
            if($_POST["G1133_C20962"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1133_C20962 = "'".str_replace(' ', '',$_POST["G1133_C20962"])." 00:00:00'";
                $str_LsqlU .= $separador." G1133_C20962 = ".$G1133_C20962;
                $str_LsqlI .= $separador." G1133_C20962";
                $str_LsqlV .= $separador.$G1133_C20962;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1133_C18555"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C18555 = '".$_POST["G1133_C18555"]."'";
            $str_LsqlI .= $separador."G1133_C18555";
            $str_LsqlV .= $separador."'".$_POST["G1133_C18555"]."'";
            $validar = 1;
        }
         
  
        $G1133_C18556 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1133_C18556"])){
            if($_POST["G1133_C18556"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1133_C18556 = str_replace(".", "", $_POST["G1133_C18556"]);
                $G1133_C18556 =  str_replace(",", ".", $G1133_C18556);
                $str_LsqlU .= $separador." G1133_C18556 = '".$G1133_C18556."'";
                $str_LsqlI .= $separador." G1133_C18556";
                $str_LsqlV .= $separador."'".$G1133_C18556."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1133_C18557"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C18557 = '".$_POST["G1133_C18557"]."'";
            $str_LsqlI .= $separador."G1133_C18557";
            $str_LsqlV .= $separador."'".$_POST["G1133_C18557"]."'";
            $validar = 1;
        }
         
  
        $G1133_C18558 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1133_C18558"])){
            if($_POST["G1133_C18558"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1133_C18558 = str_replace(".", "", $_POST["G1133_C18558"]);
                $G1133_C18558 =  str_replace(",", ".", $G1133_C18558);
                $str_LsqlU .= $separador." G1133_C18558 = '".$G1133_C18558."'";
                $str_LsqlI .= $separador." G1133_C18558";
                $str_LsqlV .= $separador."'".$G1133_C18558."'";
                $validar = 1;
            }
        }
 
        $G1133_C18559 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1133_C18559"])){    
            if($_POST["G1133_C18559"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1133_C18559 = "'".str_replace(' ', '',$_POST["G1133_C18559"])." 00:00:00'";
                $str_LsqlU .= $separador." G1133_C18559 = ".$G1133_C18559;
                $str_LsqlI .= $separador." G1133_C18559";
                $str_LsqlV .= $separador.$G1133_C18559;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1133_C18560"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C18560 = '".$_POST["G1133_C18560"]."'";
            $str_LsqlI .= $separador."G1133_C18560";
            $str_LsqlV .= $separador."'".$_POST["G1133_C18560"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1133_C18561"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_C18561 = '".$_POST["G1133_C18561"]."'";
            $str_LsqlI .= $separador."G1133_C18561";
            $str_LsqlV .= $separador."'".$_POST["G1133_C18561"]."'";
            $validar = 1;
        }
         
 
        $G1133_C20960 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1133_C20960"])){    
            if($_POST["G1133_C20960"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1133_C20960 = "'".str_replace(' ', '',$_POST["G1133_C20960"])." 00:00:00'";
                $str_LsqlU .= $separador." G1133_C20960 = ".$G1133_C20960;
                $str_LsqlI .= $separador." G1133_C20960";
                $str_LsqlV .= $separador.$G1133_C20960;
                $validar = 1;
            }
        }
  
        $G1133_C20961 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1133_C20961"])){   
            if($_POST["G1133_C20961"] != '' && $_POST["G1133_C20961"] != 'undefined' && $_POST["G1133_C20961"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1133_C20961 = "'".$fecha." ".str_replace(' ', '',$_POST["G1133_C20961"])."'";
                $str_LsqlU .= $separador." G1133_C20961 = ".$G1133_C20961."";
                $str_LsqlI .= $separador." G1133_C20961";
                $str_LsqlV .= $separador.$G1133_C20961;
                $validar = 1;
            }
        }
  
        $G1133_C20963 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1133_C20963"])){   
            if($_POST["G1133_C20963"] != '' && $_POST["G1133_C20963"] != 'undefined' && $_POST["G1133_C20963"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1133_C20963 = "'".$fecha." ".str_replace(' ', '',$_POST["G1133_C20963"])."'";
                $str_LsqlU .= $separador." G1133_C20963 = ".$G1133_C20963."";
                $str_LsqlI .= $separador." G1133_C20963";
                $str_LsqlV .= $separador.$G1133_C20963;
                $validar = 1;
            }
        }
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1133_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1133_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1133_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //Si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;
                //ahora toca ver lo de la muestra asi que toca ver que pasa 
                /* primero buscamos la campaña que nos esta llegando */
                $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana"];

                //echo $Lsql_Campan;

                $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
                $datoCampan = $res_Lsql_Campan->fetch_array();
                $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
                $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
                $int_CAMPAN_ActPo = $datoCampan['CAMPAN_ActPobGui_b'];


                if($int_CAMPAN_ActPo == '-1'){
                    /* toca hacer actualizacion desde Script */
                    
                    $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana"];
                    $resultcampSql = $mysqli->query($campSql);
                    $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
                    $i=0;
                    while($key = $resultcampSql->fetch_object()){

                        if($i == 0){
                            $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }else{
                            $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }
                        $i++;
                    } 
                    $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$ultimoResgistroInsertado.' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
                    //echo "Esta ".$Lsql;
                    if($mysqli->query($Lsql) === TRUE ){

                    }else{
                        echo "NO sE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
                    }
                }


                //Ahora toca actualizar la muestra
                $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 3, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = '-9' , 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".date('Y-m-d H:i:s')."', 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = 3,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = 'No desea participar',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = NULL,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = '-9',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b ='".date('Y-m-d H:i:s')."'";
                $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_POST['id'];
                // echo $MuestraSql;
                if($mysqli->query($MuestraSql) === true){

                }else{
                    echo "Error insertando la muesta => ".$mysqli->error;
                }
                
                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTEzMw==&result=1');

            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
