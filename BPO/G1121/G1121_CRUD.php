<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1121_ConsInte__b, G1121_FechaInsercion , G1121_Usuario ,  G1121_CodigoMiembro  , G1121_PoblacionOrigen , G1121_EstadoDiligenciamiento ,  G1121_IdLlamada , G1121_C17106 as principal ,G1121_C20659,G1121_C17106,G1121_C17107,G1121_C17108,G1121_C17643,G1121_C20660,G1121_C20688,G1121_C17109,G1121_C17110,G1121_C17111,G1121_C17112,G1121_C17113,G1121_C17114,G1121_C17115,G1121_C17116,G1121_C17117,G1121_C17644,G1121_C17645,G1121_C17647,G1121_C17646,G1121_C20335,G1121_C17118,G1121_C17119,G1121_C17120,G1121_C17639,G1121_C17640,G1121_C17641,G1121_C17642,G1121_C22653,G1121_C18885,G1121_C18886,G1121_C18887,G1121_C18888,G1121_C18889,G1121_C18890,G1121_C20690,G1121_C21549,G1121_C21550,G1121_C21551,G1121_C21552,G1121_C21553,G1121_C21554,G1121_C21555,G1121_C21556,G1121_C21557,G1121_C21558,G1121_C21559,G1121_C21560,G1121_C21561,G1121_C21562,G1121_C21563 FROM '.$BaseDatos.'.G1121 WHERE G1121_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1121_C20659'] = $key->G1121_C20659;

                $datos[$i]['G1121_C17106'] = $key->G1121_C17106;

                $datos[$i]['G1121_C17107'] = $key->G1121_C17107;

                $datos[$i]['G1121_C17108'] = $key->G1121_C17108;

                $datos[$i]['G1121_C17643'] = $key->G1121_C17643;

                $datos[$i]['G1121_C20660'] = $key->G1121_C20660;

                $datos[$i]['G1121_C20688'] = $key->G1121_C20688;

                $datos[$i]['G1121_C17109'] = $key->G1121_C17109;

                $datos[$i]['G1121_C17110'] = $key->G1121_C17110;

                $datos[$i]['G1121_C17111'] = $key->G1121_C17111;

                $datos[$i]['G1121_C17112'] = $key->G1121_C17112;

                $datos[$i]['G1121_C17113'] = $key->G1121_C17113;

                $datos[$i]['G1121_C17114'] = $key->G1121_C17114;

                $datos[$i]['G1121_C17115'] = $key->G1121_C17115;

                $datos[$i]['G1121_C17116'] = $key->G1121_C17116;

                $datos[$i]['G1121_C17117'] = $key->G1121_C17117;

                $datos[$i]['G1121_C17644'] = $key->G1121_C17644;

                $datos[$i]['G1121_C17645'] = $key->G1121_C17645;

                $datos[$i]['G1121_C17647'] = explode(' ', $key->G1121_C17647)[0];

                $datos[$i]['G1121_C17646'] = explode(' ', $key->G1121_C17646)[0];

                $datos[$i]['G1121_C20335'] = $key->G1121_C20335;

                $datos[$i]['G1121_C17118'] = $key->G1121_C17118;

                $datos[$i]['G1121_C17119'] = $key->G1121_C17119;

                $datos[$i]['G1121_C17120'] = $key->G1121_C17120;

                $datos[$i]['G1121_C17639'] = $key->G1121_C17639;

                $datos[$i]['G1121_C17640'] = $key->G1121_C17640;

                $datos[$i]['G1121_C17641'] = $key->G1121_C17641;

                $datos[$i]['G1121_C17642'] = $key->G1121_C17642;

                $datos[$i]['G1121_C22653'] = $key->G1121_C22653;

                $datos[$i]['G1121_C18885'] = $key->G1121_C18885;

                $datos[$i]['G1121_C18886'] = $key->G1121_C18886;

                $datos[$i]['G1121_C18887'] = $key->G1121_C18887;

                $datos[$i]['G1121_C18888'] = $key->G1121_C18888;

                $datos[$i]['G1121_C18889'] = $key->G1121_C18889;

                $datos[$i]['G1121_C18890'] = $key->G1121_C18890;

                $datos[$i]['G1121_C20690'] = $key->G1121_C20690;

                $datos[$i]['G1121_C21549'] = $key->G1121_C21549;

                $datos[$i]['G1121_C21550'] = $key->G1121_C21550;

                $datos[$i]['G1121_C21551'] = $key->G1121_C21551;

                $datos[$i]['G1121_C21552'] = $key->G1121_C21552;

                $datos[$i]['G1121_C21553'] = $key->G1121_C21553;

                $datos[$i]['G1121_C21554'] = $key->G1121_C21554;

                $datos[$i]['G1121_C21555'] = $key->G1121_C21555;

                $datos[$i]['G1121_C21556'] = $key->G1121_C21556;

                $datos[$i]['G1121_C21557'] = explode(' ', $key->G1121_C21557)[0];

                $datos[$i]['G1121_C21558'] = $key->G1121_C21558;

                $datos[$i]['G1121_C21559'] = $key->G1121_C21559;

                $datos[$i]['G1121_C21560'] = explode(' ', $key->G1121_C21560)[0];
  
                $hora = '';
                if(!is_null($key->G1121_C21561)){
                    $hora = explode(' ', $key->G1121_C21561)[1];
                }

                $datos[$i]['G1121_C21562'] = explode(' ', $key->G1121_C21562)[0];
  
                $hora = '';
                if(!is_null($key->G1121_C21563)){
                    $hora = explode(' ', $key->G1121_C21563)[1];
                }
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1121_ConsInte__b as id,  G1121_C17106 as camp1 , G1121_C17107 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1121 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1121_C17106 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1121_C17107 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1121_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1121_C18887'])){
            echo '<select class="form-control input-sm"  name="G1121_C18887" id="G1121_C18887">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1121_C18887'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1121_C18887'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1121_C18887'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1121");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1121_ConsInte__b, G1121_FechaInsercion , G1121_Usuario ,  G1121_CodigoMiembro  , G1121_PoblacionOrigen , G1121_EstadoDiligenciamiento ,  G1121_IdLlamada , G1121_C17106 as principal ,G1121_C20659,G1121_C17106,G1121_C17107,G1121_C17108,G1121_C17643,G1121_C20660,G1121_C20688,G1121_C17109,G1121_C17110,G1121_C17111,G1121_C17112,G1121_C17113,G1121_C17114,G1121_C17115,G1121_C17116,G1121_C17117,G1121_C17644, a.LISOPC_Nombre____b as G1121_C17645,G1121_C17647,G1121_C17646,G1121_C20335,G1121_C17118,G1121_C17119, b.LISOPC_Nombre____b as G1121_C17120, c.LISOPC_Nombre____b as G1121_C17639, d.LISOPC_Nombre____b as G1121_C17640, e.LISOPC_Nombre____b as G1121_C17641, f.LISOPC_Nombre____b as G1121_C17642,G1121_C22653, g.LISOPC_Nombre____b as G1121_C18885, h.LISOPC_Nombre____b as G1121_C18886,G1121_C18888,G1121_C18889,G1121_C18890,G1121_C20690,G1121_C21549,G1121_C21550, i.LISOPC_Nombre____b as G1121_C21551, j.LISOPC_Nombre____b as G1121_C21552, k.LISOPC_Nombre____b as G1121_C21553,G1121_C21554, l.LISOPC_Nombre____b as G1121_C21555,G1121_C21556,G1121_C21557,G1121_C21558,G1121_C21559,G1121_C21560,G1121_C21561,G1121_C21562,G1121_C21563 FROM '.$BaseDatos.'.G1121 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1121_C17645 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1121_C17120 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1121_C17639 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1121_C17640 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1121_C17641 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1121_C17642 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1121_C18885 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1121_C18886 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1121_C18887 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1121_C21551 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1121_C21552 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1121_C21553 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G1121_C21555';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1121_C21561)){
                    $hora_a = explode(' ', $fila->G1121_C21561)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1121_C21563)){
                    $hora_b = explode(' ', $fila->G1121_C21563)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1121_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1121_ConsInte__b , ($fila->G1121_C20659) , ($fila->G1121_C17106) , ($fila->G1121_C17107) , ($fila->G1121_C17108) , ($fila->G1121_C17643) , ($fila->G1121_C20660) , ($fila->G1121_C20688) , ($fila->G1121_C17109) , ($fila->G1121_C17110) , ($fila->G1121_C17111) , ($fila->G1121_C17112) , ($fila->G1121_C17113) , ($fila->G1121_C17114) , ($fila->G1121_C17115) , ($fila->G1121_C17116) , ($fila->G1121_C17117) , ($fila->G1121_C17644) , ($fila->G1121_C17645) , explode(' ', $fila->G1121_C17647)[0] , explode(' ', $fila->G1121_C17646)[0] , ($fila->G1121_C20335) , ($fila->G1121_C17118) , ($fila->G1121_C17119) , ($fila->G1121_C17120) , ($fila->G1121_C17639) , ($fila->G1121_C17640) , ($fila->G1121_C17641) , ($fila->G1121_C17642) , ($fila->G1121_C22653) , ($fila->G1121_C18885) , ($fila->G1121_C18886) , ($fila->G1121_C18888) , ($fila->G1121_C18889) , ($fila->G1121_C18890) , ($fila->G1121_C20690) , ($fila->G1121_C21549) , ($fila->G1121_C21550) , ($fila->G1121_C21551) , ($fila->G1121_C21552) , ($fila->G1121_C21553) , ($fila->G1121_C21554) , ($fila->G1121_C21555) , ($fila->G1121_C21556) , explode(' ', $fila->G1121_C21557)[0] , ($fila->G1121_C21558) , ($fila->G1121_C21559) , explode(' ', $fila->G1121_C21560)[0] , $hora_a , explode(' ', $fila->G1121_C21562)[0] , $hora_b );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1121 WHERE G1121_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1121_ConsInte__b as id,  G1121_C17106 as camp1 , G1121_C17107 as camp2  FROM '.$BaseDatos.'.G1121 ORDER BY G1121_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1121 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1121(";
            $LsqlV = " VALUES ("; 
  
            $G1121_C20659 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C20659"])){
                if($_POST["G1121_C20659"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C20659 = $_POST["G1121_C20659"];
                    $LsqlU .= $separador." G1121_C20659 = ".$G1121_C20659."";
                    $LsqlI .= $separador." G1121_C20659";
                    $LsqlV .= $separador.$G1121_C20659;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1121_C17106"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17106 = '".$_POST["G1121_C17106"]."'";
                $LsqlI .= $separador."G1121_C17106";
                $LsqlV .= $separador."'".$_POST["G1121_C17106"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17107"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17107 = '".$_POST["G1121_C17107"]."'";
                $LsqlI .= $separador."G1121_C17107";
                $LsqlV .= $separador."'".$_POST["G1121_C17107"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17108"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17108 = '".$_POST["G1121_C17108"]."'";
                $LsqlI .= $separador."G1121_C17108";
                $LsqlV .= $separador."'".$_POST["G1121_C17108"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17643 = '".$_POST["G1121_C17643"]."'";
                $LsqlI .= $separador."G1121_C17643";
                $LsqlV .= $separador."'".$_POST["G1121_C17643"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C20660"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C20660 = '".$_POST["G1121_C20660"]."'";
                $LsqlI .= $separador."G1121_C20660";
                $LsqlV .= $separador."'".$_POST["G1121_C20660"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C20688"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C20688 = '".$_POST["G1121_C20688"]."'";
                $LsqlI .= $separador."G1121_C20688";
                $LsqlV .= $separador."'".$_POST["G1121_C20688"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17109"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17109 = '".$_POST["G1121_C17109"]."'";
                $LsqlI .= $separador."G1121_C17109";
                $LsqlV .= $separador."'".$_POST["G1121_C17109"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17110"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17110 = '".$_POST["G1121_C17110"]."'";
                $LsqlI .= $separador."G1121_C17110";
                $LsqlV .= $separador."'".$_POST["G1121_C17110"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17111"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17111 = '".$_POST["G1121_C17111"]."'";
                $LsqlI .= $separador."G1121_C17111";
                $LsqlV .= $separador."'".$_POST["G1121_C17111"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17112"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17112 = '".$_POST["G1121_C17112"]."'";
                $LsqlI .= $separador."G1121_C17112";
                $LsqlV .= $separador."'".$_POST["G1121_C17112"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17113 = '".$_POST["G1121_C17113"]."'";
                $LsqlI .= $separador."G1121_C17113";
                $LsqlV .= $separador."'".$_POST["G1121_C17113"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17114"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17114 = '".$_POST["G1121_C17114"]."'";
                $LsqlI .= $separador."G1121_C17114";
                $LsqlV .= $separador."'".$_POST["G1121_C17114"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17115"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17115 = '".$_POST["G1121_C17115"]."'";
                $LsqlI .= $separador."G1121_C17115";
                $LsqlV .= $separador."'".$_POST["G1121_C17115"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17116"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17116 = '".$_POST["G1121_C17116"]."'";
                $LsqlI .= $separador."G1121_C17116";
                $LsqlV .= $separador."'".$_POST["G1121_C17116"]."'";
                $validar = 1;
            }
             
  
            $G1121_C17117 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C17117"])){
                if($_POST["G1121_C17117"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C17117 = $_POST["G1121_C17117"];
                    //$LsqlU .= $separador." G1121_C17117 = ".$G1121_C17117."";
                    $LsqlU .= $separador." G1121_C17117 = '".$G1121_C17117."'  ";
			$LsqlI .= $separador." G1121_C17117";
                    $LsqlV .= $separador.$G1121_C17117;
                    $validar = 1;
                }
            }
  
            $G1121_C17644 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C17644"])){
                if($_POST["G1121_C17644"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C17644 = $_POST["G1121_C17644"];
                    $LsqlU .= $separador." G1121_C17644 = ".$G1121_C17644."";
                    $LsqlI .= $separador." G1121_C17644";
                    $LsqlV .= $separador.$G1121_C17644;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1121_C17645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17645 = '".$_POST["G1121_C17645"]."'";
                $LsqlI .= $separador."G1121_C17645";
                $LsqlV .= $separador."'".$_POST["G1121_C17645"]."'";
                $validar = 1;
            }
             
 
            $G1121_C17647 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1121_C17647"])){    
                if($_POST["G1121_C17647"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1121_C17647"]);
                    if(count($tieneHora) > 1){
                    	$G1121_C17647 = "'".$_POST["G1121_C17647"]."'";
                    }else{
                    	$G1121_C17647 = "'".str_replace(' ', '',$_POST["G1121_C17647"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1121_C17647 = ".$G1121_C17647;
                    $LsqlI .= $separador." G1121_C17647";
                    $LsqlV .= $separador.$G1121_C17647;
                    $validar = 1;
                }
            }
 
            $G1121_C17646 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1121_C17646"])){    
                if($_POST["G1121_C17646"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1121_C17646"]);
                    if(count($tieneHora) > 1){
                    	$G1121_C17646 = "'".$_POST["G1121_C17646"]."'";
                    }else{
                    	$G1121_C17646 = "'".str_replace(' ', '',$_POST["G1121_C17646"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1121_C17646 = ".$G1121_C17646;
                    $LsqlI .= $separador." G1121_C17646";
                    $LsqlV .= $separador.$G1121_C17646;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1121_C20335"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C20335 = '".$_POST["G1121_C20335"]."'";
                $LsqlI .= $separador."G1121_C20335";
                $LsqlV .= $separador."'".$_POST["G1121_C20335"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17118"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17118 = '".$_POST["G1121_C17118"]."'";
                $LsqlI .= $separador."G1121_C17118";
                $LsqlV .= $separador."'".$_POST["G1121_C17118"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17119"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17119 = '".$_POST["G1121_C17119"]."'";
                $LsqlI .= $separador."G1121_C17119";
                $LsqlV .= $separador."'".$_POST["G1121_C17119"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17120"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17120 = '".$_POST["G1121_C17120"]."'";
                $LsqlI .= $separador."G1121_C17120";
                $LsqlV .= $separador."'".$_POST["G1121_C17120"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17639"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17639 = '".$_POST["G1121_C17639"]."'";
                $LsqlI .= $separador."G1121_C17639";
                $LsqlV .= $separador."'".$_POST["G1121_C17639"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17640"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17640 = '".$_POST["G1121_C17640"]."'";
                $LsqlI .= $separador."G1121_C17640";
                $LsqlV .= $separador."'".$_POST["G1121_C17640"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17641"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17641 = '".$_POST["G1121_C17641"]."'";
                $LsqlI .= $separador."G1121_C17641";
                $LsqlV .= $separador."'".$_POST["G1121_C17641"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C17642"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C17642 = '".$_POST["G1121_C17642"]."'";
                $LsqlI .= $separador."G1121_C17642";
                $LsqlV .= $separador."'".$_POST["G1121_C17642"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C22653"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C22653 = '".$_POST["G1121_C22653"]."'";
                $LsqlI .= $separador."G1121_C22653";
                $LsqlV .= $separador."'".$_POST["G1121_C22653"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C18885"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C18885 = '".$_POST["G1121_C18885"]."'";
                $LsqlI .= $separador."G1121_C18885";
                $LsqlV .= $separador."'".$_POST["G1121_C18885"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C18886"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C18886 = '".$_POST["G1121_C18886"]."'";
                $LsqlI .= $separador."G1121_C18886";
                $LsqlV .= $separador."'".$_POST["G1121_C18886"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C18887"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C18887 = '".$_POST["G1121_C18887"]."'";
                $LsqlI .= $separador."G1121_C18887";
                $LsqlV .= $separador."'".$_POST["G1121_C18887"]."'";
                $validar = 1;
            }
             
  
            $G1121_C18888 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C18888"])){
                if($_POST["G1121_C18888"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C18888 = $_POST["G1121_C18888"];
                    $LsqlU .= $separador." G1121_C18888 = ".$G1121_C18888."";
                    $LsqlI .= $separador." G1121_C18888";
                    $LsqlV .= $separador.$G1121_C18888;
                    $validar = 1;
                }
            }
  
            $G1121_C18889 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C18889"])){
                if($_POST["G1121_C18889"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C18889 = $_POST["G1121_C18889"];
                    $LsqlU .= $separador." G1121_C18889 = ".$G1121_C18889."";
                    $LsqlI .= $separador." G1121_C18889";
                    $LsqlV .= $separador.$G1121_C18889;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1121_C18890"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C18890 = '".$_POST["G1121_C18890"]."'";
                $LsqlI .= $separador."G1121_C18890";
                $LsqlV .= $separador."'".$_POST["G1121_C18890"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C20690"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C20690 = '".$_POST["G1121_C20690"]."'";
                $LsqlI .= $separador."G1121_C20690";
                $LsqlV .= $separador."'".$_POST["G1121_C20690"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C21549"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21549 = '".$_POST["G1121_C21549"]."'";
                $LsqlI .= $separador."G1121_C21549";
                $LsqlV .= $separador."'".$_POST["G1121_C21549"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C21550"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21550 = '".$_POST["G1121_C21550"]."'";
                $LsqlI .= $separador."G1121_C21550";
                $LsqlV .= $separador."'".$_POST["G1121_C21550"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C21551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21551 = '".$_POST["G1121_C21551"]."'";
                $LsqlI .= $separador."G1121_C21551";
                $LsqlV .= $separador."'".$_POST["G1121_C21551"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C21552"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21552 = '".$_POST["G1121_C21552"]."'";
                $LsqlI .= $separador."G1121_C21552";
                $LsqlV .= $separador."'".$_POST["G1121_C21552"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C21553"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21553 = '".$_POST["G1121_C21553"]."'";
                $LsqlI .= $separador."G1121_C21553";
                $LsqlV .= $separador."'".$_POST["G1121_C21553"]."'";
                $validar = 1;
            }
             
  
            $G1121_C21554 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C21554"])){
                if($_POST["G1121_C21554"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C21554 = $_POST["G1121_C21554"];
                    $LsqlU .= $separador." G1121_C21554 = ".$G1121_C21554."";
                    $LsqlI .= $separador." G1121_C21554";
                    $LsqlV .= $separador.$G1121_C21554;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1121_C21555"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21555 = '".$_POST["G1121_C21555"]."'";
                $LsqlI .= $separador."G1121_C21555";
                $LsqlV .= $separador."'".$_POST["G1121_C21555"]."'";
                $validar = 1;
            }
             
  
            $G1121_C21556 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1121_C21556"])){
                if($_POST["G1121_C21556"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C21556 = $_POST["G1121_C21556"];
                    $LsqlU .= $separador." G1121_C21556 = ".$G1121_C21556."";
                    $LsqlI .= $separador." G1121_C21556";
                    $LsqlV .= $separador.$G1121_C21556;
                    $validar = 1;
                }
            }
 
            $G1121_C21557 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1121_C21557"])){    
                if($_POST["G1121_C21557"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1121_C21557"]);
                    if(count($tieneHora) > 1){
                    	$G1121_C21557 = "'".$_POST["G1121_C21557"]."'";
                    }else{
                    	$G1121_C21557 = "'".str_replace(' ', '',$_POST["G1121_C21557"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1121_C21557 = ".$G1121_C21557;
                    $LsqlI .= $separador." G1121_C21557";
                    $LsqlV .= $separador.$G1121_C21557;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1121_C21558"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21558 = '".$_POST["G1121_C21558"]."'";
                $LsqlI .= $separador."G1121_C21558";
                $LsqlV .= $separador."'".$_POST["G1121_C21558"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1121_C21559"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_C21559 = '".$_POST["G1121_C21559"]."'";
                $LsqlI .= $separador."G1121_C21559";
                $LsqlV .= $separador."'".$_POST["G1121_C21559"]."'";
                $validar = 1;
            }
             
 
            $G1121_C21560 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1121_C21560"])){    
                if($_POST["G1121_C21560"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1121_C21560"]);
                    if(count($tieneHora) > 1){
                    	$G1121_C21560 = "'".$_POST["G1121_C21560"]."'";
                    }else{
                    	$G1121_C21560 = "'".str_replace(' ', '',$_POST["G1121_C21560"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1121_C21560 = ".$G1121_C21560;
                    $LsqlI .= $separador." G1121_C21560";
                    $LsqlV .= $separador.$G1121_C21560;
                    $validar = 1;
                }
            }
  
            $G1121_C21561 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1121_C21561"])){   
                if($_POST["G1121_C21561"] != '' && $_POST["G1121_C21561"] != 'undefined' && $_POST["G1121_C21561"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C21561 = "'".$fecha." ".str_replace(' ', '',$_POST["G1121_C21561"])."'";
                    $LsqlU .= $separador." G1121_C21561 = ".$G1121_C21561."";
                    $LsqlI .= $separador." G1121_C21561";
                    $LsqlV .= $separador.$G1121_C21561;
                    $validar = 1;
                }
            }
 
            $G1121_C21562 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1121_C21562"])){    
                if($_POST["G1121_C21562"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1121_C21562"]);
                    if(count($tieneHora) > 1){
                    	$G1121_C21562 = "'".$_POST["G1121_C21562"]."'";
                    }else{
                    	$G1121_C21562 = "'".str_replace(' ', '',$_POST["G1121_C21562"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1121_C21562 = ".$G1121_C21562;
                    $LsqlI .= $separador." G1121_C21562";
                    $LsqlV .= $separador.$G1121_C21562;
                    $validar = 1;
                }
            }
  
            $G1121_C21563 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1121_C21563"])){   
                if($_POST["G1121_C21563"] != '' && $_POST["G1121_C21563"] != 'undefined' && $_POST["G1121_C21563"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1121_C21563 = "'".$fecha." ".str_replace(' ', '',$_POST["G1121_C21563"])."'";
                    $LsqlU .= $separador." G1121_C21563 = ".$G1121_C21563."";
                    $LsqlI .= $separador." G1121_C21563";
                    $LsqlV .= $separador.$G1121_C21563;
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1121_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1121_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1121_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1121_Usuario , G1121_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1121_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1121 WHERE G1121_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                    	echo $mysqli->insert_id;
                	}else{
                		echo "1";    		
                	}
                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
