<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1137 WHERE G1137_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }
      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1137_ConsInte__b, G1137_FechaInsercion ,USUARI_Nombre____b, G1137_CodigoMiembro, G1137_PoblacionOrigen , G1137_EstadoDiligenciamiento ,  G1137_IdLlamada , G1137_C17541 as principal ,G1137_C20679,G1137_CodigoMiembro,G1137_C17541,G1137_C17542,G1137_C17543,G1137_C20680,G1137_C20681,G1137_C17544,G1137_C17545,G1137_C17546,G1137_C17547,G1137_C19153,G1137_C17548,G1137_C17549,G1137_C17550,G1137_C17551,G1137_C17552,G1137_C19154,G1137_C19155,G1137_C19156,G1137_C19157,G1137_C22660,G1137_C17553,G1137_C17554,G1137_C17555,G1137_C17556,G1137_C17557,G1137_C17558,G1137_C17559,G1137_C17560,G1137_C17561,G1137_C17562,G1137_C17563,G1137_C17564,G1137_C19158,G1137_C19159,G1137_C19160,G1137_C19161,G1137_C19162,G1137_C19163 FROM '.$BaseDatos.'.G1137 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1137_Usuario  WHERE G1137_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1137_C20679'] = $key->G1137_C20679;

                $datos[$i]['G1137_CodigoMiembro'] = $key->G1137_CodigoMiembro;

                $datos[$i]['G1137_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1137_C17541'] = $key->G1137_C17541;

                $datos[$i]['G1137_C17542'] = $key->G1137_C17542;

                $datos[$i]['G1137_C17543'] = $key->G1137_C17543;

                $datos[$i]['G1137_C20680'] = $key->G1137_C20680;

                $datos[$i]['G1137_C20681'] = $key->G1137_C20681;

                $datos[$i]['G1137_C17544'] = $key->G1137_C17544;

                $datos[$i]['G1137_C17545'] = $key->G1137_C17545;

                $datos[$i]['G1137_C17546'] = $key->G1137_C17546;

                $datos[$i]['G1137_C17547'] = $key->G1137_C17547;

                $datos[$i]['G1137_C19153'] = $key->G1137_C19153;

                $datos[$i]['G1137_C17548'] = $key->G1137_C17548;

                $datos[$i]['G1137_C17549'] = $key->G1137_C17549;

                $datos[$i]['G1137_C17550'] = $key->G1137_C17550;

                $datos[$i]['G1137_C17551'] = $key->G1137_C17551;

                $datos[$i]['G1137_C17552'] = $key->G1137_C17552;

                $datos[$i]['G1137_C19154'] = $key->G1137_C19154;

                $datos[$i]['G1137_C19155'] = $key->G1137_C19155;

                $datos[$i]['G1137_C19156'] = explode(' ', $key->G1137_C19156)[0];

                $datos[$i]['G1137_C19157'] = explode(' ', $key->G1137_C19157)[0];

                $datos[$i]['G1137_C22660'] = $key->G1137_C22660;

                $datos[$i]['G1137_C17553'] = $key->G1137_C17553;

                $datos[$i]['G1137_C17554'] = $key->G1137_C17554;

                $datos[$i]['G1137_C17555'] = $key->G1137_C17555;

                $datos[$i]['G1137_C17556'] = $key->G1137_C17556;

                $datos[$i]['G1137_C17557'] = explode(' ', $key->G1137_C17557)[0];
  
                $hora = '';
                if(!is_null($key->G1137_C17558)){
                    $hora = explode(' ', $key->G1137_C17558)[1];
                }

                $datos[$i]['G1137_C17559'] = $key->G1137_C17559;

                $datos[$i]['G1137_C17560'] = $key->G1137_C17560;

                $datos[$i]['G1137_C17561'] = $key->G1137_C17561;

                $datos[$i]['G1137_C17562'] = $key->G1137_C17562;

                $datos[$i]['G1137_C17563'] = $key->G1137_C17563;

                $datos[$i]['G1137_C17564'] = $key->G1137_C17564;

                $datos[$i]['G1137_C19158'] = $key->G1137_C19158;

                $datos[$i]['G1137_C19159'] = $key->G1137_C19159;

                $datos[$i]['G1137_C19160'] = $key->G1137_C19160;

                $datos[$i]['G1137_C19161'] = $key->G1137_C19161;

                $datos[$i]['G1137_C19162'] = $key->G1137_C19162;

                $datos[$i]['G1137_C19163'] = $key->G1137_C19163;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1137_ConsInte__b as id,  G1137_C17541 as camp1 , G1137_C17543 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1137 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1137_C17554  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1137_C17541 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1137_C17543 like '%".$_POST['Busqueda']."%' ) ";
            }
            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1137_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1137_C20679']) && $_POST['G1137_C20679'] != ''){
				$Lsql .= " AND G1137_C20679 LIKE '%". $_POST['G1137_C20679'] ."%' ";
			}
			if(!is_null($_POST['G1137_C17541']) && $_POST['G1137_C17541'] != ''){
				$Lsql .= " AND G1137_C17541 LIKE '%". $_POST['G1137_C17541'] ."%' ";
			}
			if(!is_null($_POST['G1137_C17542']) && $_POST['G1137_C17542'] != ''){
				$Lsql .= " AND G1137_C17542 LIKE '%". $_POST['G1137_C17542'] ."%' ";
			}
			if(!is_null($_POST['G1137_C17544']) && $_POST['G1137_C17544'] != ''){
				$Lsql .= " AND G1137_C17544 LIKE '%". $_POST['G1137_C17544'] ."%' ";
			}

            $Lsql .= "ORDER BY FIELD(G1137_C17554,14288,14289,14290,14291,14292), G1137_FechaInsercion DESC  LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1137_C19160'])){
            echo '<select class="form-control input-sm"  name="G1137_C19160" id="G1137_C19160">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1137_C19160'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1137_C19160'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1137_C19160'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1137");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1137_ConsInte__b, G1137_FechaInsercion , G1137_Usuario ,G1137_CodigoMiembro , G1137_PoblacionOrigen , G1137_EstadoDiligenciamiento ,  G1137_IdLlamada , G1137_C17541 as principal ,G1137_C20679,G1137_C17541,G1137_C17542,G1137_C17543,G1137_C20680,G1137_C20681,G1137_C17544,G1137_C17545,G1137_C17546,G1137_C17547,G1137_C19153,G1137_C17548,G1137_C17549,G1137_C17550,G1137_C17551,G1137_C17552,G1137_C19154, a.LISOPC_Nombre____b as G1137_C19155,G1137_C19156,G1137_C19157,G1137_C22660, b.LISOPC_Nombre____b as G1137_C17553, c.LISOPC_Nombre____b as G1137_C17554,G1137_C17555,G1137_C17556,G1137_C17557,G1137_C17558,G1137_C17559,G1137_C17560,G1137_C17561,G1137_C17562,G1137_C17563,G1137_C17564, d.LISOPC_Nombre____b as G1137_C19158, e.LISOPC_Nombre____b as G1137_C19159,G1137_C19161,G1137_C19162,G1137_C19163 FROM '.$BaseDatos.'.G1137 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1137_C19155 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1137_C17553 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1137_C17554 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1137_C19158 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1137_C19159 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1137_C19160';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1137_C17558)){
                    $hora_a = explode(' ', $fila->G1137_C17558)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1137_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1137_ConsInte__b , ($fila->G1137_C20679) , ($fila->G1137_C17541) , ($fila->G1137_C17542) , ($fila->G1137_C17543) , ($fila->G1137_C20680) , ($fila->G1137_C20681) , ($fila->G1137_C17544) , ($fila->G1137_C17545) , ($fila->G1137_C17546) , ($fila->G1137_C17547) , ($fila->G1137_C19153) , ($fila->G1137_C17548) , ($fila->G1137_C17549) , ($fila->G1137_C17550) , ($fila->G1137_C17551) , ($fila->G1137_C17552) , ($fila->G1137_C19154) , ($fila->G1137_C19155) , explode(' ', $fila->G1137_C19156)[0] , explode(' ', $fila->G1137_C19157)[0] , ($fila->G1137_C22660) , ($fila->G1137_C17553) , ($fila->G1137_C17554) , ($fila->G1137_C17555) , ($fila->G1137_C17556) , explode(' ', $fila->G1137_C17557)[0] , $hora_a , ($fila->G1137_C17559) , ($fila->G1137_C17560) , ($fila->G1137_C17561) , ($fila->G1137_C17562) , ($fila->G1137_C17563) , ($fila->G1137_C17564) , ($fila->G1137_C19158) , ($fila->G1137_C19159) , ($fila->G1137_C19161) , ($fila->G1137_C19162) , ($fila->G1137_C19163) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1137 WHERE G1137_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }


        
        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1137_ConsInte__b as id,  G1137_C17541 as camp1 , G1137_C17543 as camp2, G1137_C17554 as estado FROM '.$BaseDatos.'.G1137 ORDER BY FIELD(G1137_C17554,14280,14281,14282,14283,14284), G1137_FechaInsercion DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                
                $color = 'color:green;';
                $strIconoBackOffice = 'Cerrada';
                if($obj->estado == '14281' || $obj->estado == '14282'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14284'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }
                else if($obj->estado == '14280'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1137 SET "; 
  
            if(isset($_POST["G1137_C20679"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C20679 = '".$_POST["G1137_C20679"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17541"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17541 = '".$_POST["G1137_C17541"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17542"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17542 = '".$_POST["G1137_C17542"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17543"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17543 = '".$_POST["G1137_C17543"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C20680"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C20680 = '".$_POST["G1137_C20680"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C20681"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C20681 = '".$_POST["G1137_C20681"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17544"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17544 = '".$_POST["G1137_C17544"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17545 = '".$_POST["G1137_C17545"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17546"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17546 = '".$_POST["G1137_C17546"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17547"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17547 = '".$_POST["G1137_C17547"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C19153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19153 = '".$_POST["G1137_C19153"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17548 = '".$_POST["G1137_C17548"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17549"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17549 = '".$_POST["G1137_C17549"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17550"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17550 = '".$_POST["G1137_C17550"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17551 = '".$_POST["G1137_C17551"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17552"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17552 = '".$_POST["G1137_C17552"]."'";
                $validar = 1;
            }
             
  
            $G1137_C19154 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C19154"])){
                if($_POST["G1137_C19154"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19154 = $_POST["G1137_C19154"];
                    $LsqlU .= $separador." G1137_C19154 = ".$G1137_C19154."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1137_C19155"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19155 = '".$_POST["G1137_C19155"]."'";
                $validar = 1;
            }
             
 
            $G1137_C19156 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1137_C19156"])){    
                if($_POST["G1137_C19156"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1137_C19156"]);
                    if(count($tieneHora) > 1){
                    	$G1137_C19156 = "'".$_POST["G1137_C19156"]."'";
                    }else{
                    	$G1137_C19156 = "'".str_replace(' ', '',$_POST["G1137_C19156"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1137_C19156 = ".$G1137_C19156;
                    $validar = 1;
                }
            }
 
            $G1137_C19157 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1137_C19157"])){    
                if($_POST["G1137_C19157"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1137_C19157"]);
                    if(count($tieneHora) > 1){
                    	$G1137_C19157 = "'".$_POST["G1137_C19157"]."'";
                    }else{
                    	$G1137_C19157 = "'".str_replace(' ', '',$_POST["G1137_C19157"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1137_C19157 = ".$G1137_C19157;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1137_C22660"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C22660 = '".$_POST["G1137_C22660"]."'";
                $validar = 1;
            }
             
 
            $G1137_C17553 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1137_C17553 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1137_C17553 = ".$G1137_C17553;
                    $validar = 1;

                    
                }
            }
 
            $G1137_C17554 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1137_C17554 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1137_C17554 = ".$G1137_C17554;
                    $validar = 1;
                }
            }
  
            $G1137_C17555 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C17555"])){
                if($_POST["G1137_C17555"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C17555 = $_POST["G1137_C17555"];
                    $LsqlU .= $separador." G1137_C17555 = ".$G1137_C17555."";
                    $validar = 1;
                }
            }
  
            $G1137_C17556 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C17556"])){
                if($_POST["G1137_C17556"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C17556 = $_POST["G1137_C17556"];
                    $LsqlU .= $separador." G1137_C17556 = ".$G1137_C17556."";
                    $validar = 1;
                }
            }
 
            $G1137_C17559 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1137_C17559 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1137_C17559 = ".$G1137_C17559;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1137_C17560"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17560 = '".$_POST["G1137_C17560"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17561"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17561 = '".$_POST["G1137_C17561"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17562"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17562 = '".$_POST["G1137_C17562"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17563"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17563 = '".$_POST["G1137_C17563"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C17564"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C17564 = '".$_POST["G1137_C17564"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C19158"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19158 = '".$_POST["G1137_C19158"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C19159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19159 = '".$_POST["G1137_C19159"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1137_C19160"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19160 = '".$_POST["G1137_C19160"]."'";
                $validar = 1;
            }
             
  
            $G1137_C19161 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C19161"])){
                if($_POST["G1137_C19161"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19161 = $_POST["G1137_C19161"];
                    $LsqlU .= $separador." G1137_C19161 = ".$G1137_C19161."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1137_C19162"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_C19162 = '".$_POST["G1137_C19162"]."'";
                $validar = 1;
            }
             
  
            $G1137_C19163 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1137_C19163"])){
                if($_POST["G1137_C19163"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1137_C19163 = $_POST["G1137_C19163"];
                    $LsqlU .= $separador." G1137_C19163 = ".$G1137_C19163."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1137_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
             if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1137_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }
             
            if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1137_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            // echo $Lsql;
            // die();

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
