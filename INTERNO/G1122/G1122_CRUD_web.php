<?php

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
        
    //Inserciones o actualizaciones

    if(isset($_POST['getListaHija'])){
        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }
        
    if(isset($_POST["oper"])){
            $str_Lsql  = '';

            $validar = 0;
            $str_LsqlU = "UPDATE ".$BaseDatos.".G1122 SET "; 
            $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1122( G1122_FechaInsercion ,";
            $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        $G1122_C20661 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1122_C20661"])){
            if($_POST["G1122_C20661"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1122_C20661 = $_POST["G1122_C20661"];
                $G1122_C20661 = str_replace(".", "", $_POST["G1122_C20661"]);
                $G1122_C20661 =  str_replace(",", ".", $G1122_C20661);
                $str_LsqlU .= $separador." G1122_C20661 = '".$G1122_C20661."'";
                $str_LsqlI .= $separador." G1122_C20661";
                $str_LsqlV .= $separador."'".$G1122_C20661."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1122_C17121"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17121 = '".$_POST["G1122_C17121"]."'";
            $str_LsqlI .= $separador."G1122_C17121";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17121"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17122"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17122 = '".$_POST["G1122_C17122"]."'";
            $str_LsqlI .= $separador."G1122_C17122";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17122"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C18857"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C18857 = '".$_POST["G1122_C18857"]."'";
            $str_LsqlI .= $separador."G1122_C18857";
            $str_LsqlV .= $separador."'".$_POST["G1122_C18857"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20662"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20662 = '".$_POST["G1122_C20662"]."'";
            $str_LsqlI .= $separador."G1122_C20662";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20662"]."'";
            $validar = 1;
        }
         
 
        $G1122_C20663 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1122_C20663"])){    
            if($_POST["G1122_C20663"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C20663 = "'".str_replace(' ', '',$_POST["G1122_C20663"])." 00:00:00'";
                $str_LsqlU .= $separador." G1122_C20663 = ".$G1122_C20663;
                $str_LsqlI .= $separador." G1122_C20663";
                $str_LsqlV .= $separador.$G1122_C20663;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1122_C17123"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17123 = '".$_POST["G1122_C17123"]."'";
            $str_LsqlI .= $separador."G1122_C17123";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17123"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17124"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17124 = '".$_POST["G1122_C17124"]."'";
            $str_LsqlI .= $separador."G1122_C17124";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17124"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17125"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17125 = '".$_POST["G1122_C17125"]."'";
            $str_LsqlI .= $separador."G1122_C17125";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17125"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17126"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17126 = '".$_POST["G1122_C17126"]."'";
            $str_LsqlI .= $separador."G1122_C17126";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17126"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17127"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17127 = '".$_POST["G1122_C17127"]."'";
            $str_LsqlI .= $separador."G1122_C17127";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17127"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17128"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17128 = '".$_POST["G1122_C17128"]."'";
            $str_LsqlI .= $separador."G1122_C17128";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17128"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17129"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17129 = '".$_POST["G1122_C17129"]."'";
            $str_LsqlI .= $separador."G1122_C17129";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17129"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17130"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17130 = '".$_POST["G1122_C17130"]."'";
            $str_LsqlI .= $separador."G1122_C17130";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17130"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17131"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17131 = '".$_POST["G1122_C17131"]."'";
            $str_LsqlI .= $separador."G1122_C17131";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17131"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17132"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17132 = '".$_POST["G1122_C17132"]."'";
            $str_LsqlI .= $separador."G1122_C17132";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17132"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C18351"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C18351 = '".$_POST["G1122_C18351"]."'";
            $str_LsqlI .= $separador."G1122_C18351";
            $str_LsqlV .= $separador."'".$_POST["G1122_C18351"]."'";
            $validar = 1;
        }
         
 
        $G1122_C18859 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1122_C18859"])){    
            if($_POST["G1122_C18859"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C18859 = "'".str_replace(' ', '',$_POST["G1122_C18859"])." 00:00:00'";
                $str_LsqlU .= $separador." G1122_C18859 = ".$G1122_C18859;
                $str_LsqlI .= $separador." G1122_C18859";
                $str_LsqlV .= $separador.$G1122_C18859;
                $validar = 1;
            }
        }
 
        $G1122_C18860 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1122_C18860"])){    
            if($_POST["G1122_C18860"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C18860 = "'".str_replace(' ', '',$_POST["G1122_C18860"])." 00:00:00'";
                $str_LsqlU .= $separador." G1122_C18860 = ".$G1122_C18860;
                $str_LsqlI .= $separador." G1122_C18860";
                $str_LsqlV .= $separador.$G1122_C18860;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1122_C17144"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17144 = '".$_POST["G1122_C17144"]."'";
            $str_LsqlI .= $separador."G1122_C17144";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17144"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17145"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17145 = '".$_POST["G1122_C17145"]."'";
            $str_LsqlI .= $separador."G1122_C17145";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17145"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17146"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17146 = '".$_POST["G1122_C17146"]."'";
            $str_LsqlI .= $separador."G1122_C17146";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17146"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C18721"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C18721 = '".$_POST["G1122_C18721"]."'";
            $str_LsqlI .= $separador."G1122_C18721";
            $str_LsqlV .= $separador."'".$_POST["G1122_C18721"]."'";
            $validar = 1;
        }
         
  
        $G1122_C18720 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1122_C18720"])){
            if($_POST["G1122_C18720"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1122_C18720 = $_POST["G1122_C18720"];
                $G1122_C18720 = str_replace(".", "", $_POST["G1122_C18720"]);
                $G1122_C18720 =  str_replace(",", ".", $G1122_C18720);
                $str_LsqlU .= $separador." G1122_C18720 = '".$G1122_C18720."'";
                $str_LsqlI .= $separador." G1122_C18720";
                $str_LsqlV .= $separador."'".$G1122_C18720."'";
                $validar = 1;
            }
        }
  
        $G1122_C17147 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1122_C17147"])){
            if($_POST["G1122_C17147"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1122_C17147 = $_POST["G1122_C17147"];
                $G1122_C17147 = str_replace(".", "", $_POST["G1122_C17147"]);
                $G1122_C17147 =  str_replace(",", ".", $G1122_C17147);
                $str_LsqlU .= $separador." G1122_C17147 = '".$G1122_C17147."'";
                $str_LsqlI .= $separador." G1122_C17147";
                $str_LsqlV .= $separador."'".$G1122_C17147."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1122_C17149"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17149 = '".$_POST["G1122_C17149"]."'";
            $str_LsqlI .= $separador."G1122_C17149";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17149"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17150"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17150 = '".$_POST["G1122_C17150"]."'";
            $str_LsqlI .= $separador."G1122_C17150";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17150"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17151"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17151 = '".$_POST["G1122_C17151"]."'";
            $str_LsqlI .= $separador."G1122_C17151";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17151"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C17152"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C17152 = '".$_POST["G1122_C17152"]."'";
            $str_LsqlI .= $separador."G1122_C17152";
            $str_LsqlV .= $separador."'".$_POST["G1122_C17152"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20691"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20691 = '".$_POST["G1122_C20691"]."'";
            $str_LsqlI .= $separador."G1122_C20691";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20691"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20585"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20585 = '".$_POST["G1122_C20585"]."'";
            $str_LsqlI .= $separador."G1122_C20585";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20585"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20719"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20719 = '".$_POST["G1122_C20719"]."'";
            $str_LsqlI .= $separador."G1122_C20719";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20719"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20720"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20720 = '".$_POST["G1122_C20720"]."'";
            $str_LsqlI .= $separador."G1122_C20720";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20720"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20721"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20721 = '".$_POST["G1122_C20721"]."'";
            $str_LsqlI .= $separador."G1122_C20721";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20721"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20722"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20722 = '".$_POST["G1122_C20722"]."'";
            $str_LsqlI .= $separador."G1122_C20722";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20722"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20723"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20723 = '".$_POST["G1122_C20723"]."'";
            $str_LsqlI .= $separador."G1122_C20723";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20723"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20724"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20724 = '".$_POST["G1122_C20724"]."'";
            $str_LsqlI .= $separador."G1122_C20724";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20724"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20725"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20725 = '".$_POST["G1122_C20725"]."'";
            $str_LsqlI .= $separador."G1122_C20725";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20725"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1122_C20726"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_C20726 = '".$_POST["G1122_C20726"]."'";
            $str_LsqlI .= $separador."G1122_C20726";
            $str_LsqlV .= $separador."'".$_POST["G1122_C20726"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1122_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1122_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1122_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //Si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;
                //ahora toca ver lo de la muestra asi que toca ver que pasa 
                /* primero buscamos la campaña que nos esta llegando */
                $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana"];

                //echo $Lsql_Campan;

                $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
                $datoCampan = $res_Lsql_Campan->fetch_array();
                $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
                $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
                $int_CAMPAN_ActPo = $datoCampan['CAMPAN_ActPobGui_b'];


                if($int_CAMPAN_ActPo == '-1'){
                    /* toca hacer actualizacion desde Script */
                    
                    $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana"];
                    $resultcampSql = $mysqli->query($campSql);
                    $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
                    $i=0;
                    while($key = $resultcampSql->fetch_object()){

                        if($i == 0){
                            $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }else{
                            $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }
                        $i++;
                    } 
                    $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$ultimoResgistroInsertado.' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
                    //echo "Esta ".$Lsql;
                    if($mysqli->query($Lsql) === TRUE ){

                    }else{
                        echo "NO sE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
                    }
                }


                //Ahora toca actualizar la muestra
                $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 3, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = '-9' , 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".date('Y-m-d H:i:s')."', 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = 3,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = 'No desea participar',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = NULL,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = '-9',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b ='".date('Y-m-d H:i:s')."'";
                $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_POST['id'];
                // echo $MuestraSql;
                if($mysqli->query($MuestraSql) === true){

                }else{
                    echo "Error insertando la muesta => ".$mysqli->error;
                }
                
                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTEyMg==&result=1');

            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
