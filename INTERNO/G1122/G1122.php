<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>

<div class="modal fade-in" id="verAgenda" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-md" style="width: 91% !important">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">VER AGENDAS</h4>
            </div>
            <div class="modal-body">
            	<div class="table-responsive">
            		<table class="table table-bordered table-hover" id="misagendasTable">
						<thead>
							<tr>
								<th>#</th>
								<th>RAZÓN SOCIAL</th>
								<th>CORREO</th>
								<th>TELEFONO 1</th>
								<th>TELEFONO 2</th>
								<th>TELEFONO 3</th>
								<th>FECHA AGENDA</th>
							</tr>
						</thead>
						<tbody>
							<?php 
			    				$idUsuario = getIdentificacionUser($token);

							$Lsql = "SELECT G1121_C17106 as RazonSocial, G1121_C17114 as Mail, G1121_C17109 as Tel1, G1121_C17110 as Tel2, G1121_C17111 as Tel3, G1121_M679_FecHorAge_b as Agenda FROM DYALOGOCRM_WEB.G1121 JOIN DYALOGOCRM_WEB.G1121_M679 ON G1121_ConsInte__b = G1121_M679_CoInMiPo__b WHERE G1121_M679_Activo____b=-1 and G1121_M679_Estado____b = 2 and G1121_M679_ConIntUsu_b = ".$idUsuario." and (LENGTH(G1121_C17109)>5) and G1121_M679_FecHorAge_b>='2000-01-01 00:00:00' ORDER BY G1121_M679_FecHorAge_b ASC,G1121_M679_CoInMiPo__b LIMIT 1000;";
									////

								//echo $Lsql;
								$res = $mysqli->query($Lsql);
								if($res){
									$i = 1;
									while ($mikey = $res->fetch_object()) {
										echo "<tr>";
											echo "<td>".$i."</td>";
											echo "<td>".$mikey->RazonSocial."</td>";
											echo "<td>".$mikey->Mail."</td>";
											echo "<td>".$mikey->Tel1."</td>";
											echo "<td>".$mikey->Tel2."</td>";
											echo "<td>".$mikey->Tel3."</td>";
											echo "<td>".$mikey->Agenda."</td>";
										echo "</tr>";
										$i++;
									}
								}
			                ?>
						</tbody>
					</table>
            	</div>
            </div>
            <div class="modal-footer">
            	<button class="btn btn-default" type="button"  data-dismiss="modal" >
                    CERRAR
                </button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade-in" id="EditarDatosTarea" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close cerradorDeModal" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">DATOS DE LA TAREA</h4>
            </div>
            <div class="modal-body">
            	<form id="tareasBackoffice" method="post">
	                <div class="panel box box-primary tareaG" id="TareaG1133" style="display: none;">
					    <div class="box-header with-border">
					        <h4 class="box-title">
					          	DATOS - ENVIAR PROPUESTA
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
       
					            <div class="col-md-6 col-xs-6">

					 
								        <!-- CAMPO TIPO TEXTO -->
								        <div class="form-group">
								            <label for="G1133_C21451" id="LblG1133_C21451">Cupo Usado</label>
								            <input type="text" onchange="clearText(this)" class="form-control input-sm" id="G1133_C21451" value=""  name="G1133_C21451"  placeholder="Cupo Usado">
								        </div>
								        <!-- FIN DEL CAMPO TIPO TEXTO -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					 
								        <!-- CAMPO TIPO TEXTO -->
								        <div class="form-group">
								            <label for="G1133_C21452" id="LblG1133_C21452">Cupo Nuevo</label>
								            <input type="text" class="form-control input-sm" id="G1133_C21452" value="" onchange="clearText(this)"  name="G1133_C21452"  placeholder="Cupo Nuevo">
								        </div>
								        <!-- FIN DEL CAMPO TIPO TEXTO -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1133_C18554" id="LblG1133_C18554">Pack</label>
								            <select class="form-control input-sm select2" style="width: 100%;" name="G1133_C18554" id="G1133_C18554">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1133_C27095" id="LblG1133_C27095">Cupo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1133_C27095" id="G1133_C27095">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>

					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1133_C27093" id="LblG1133_C27093">Segundo Pack</label>
								            <select class="form-control input-sm select2" style="width: 100%;" name="G1133_C27093" id="G1133_C27093">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1133_C27094" id="LblG1133_C27094">Segundo Cupo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1133_C27094" id="G1133_C27094">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1133_C18555" id="LblG1133_C18555">Tiempo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1133_C18555" id="G1133_C18555">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO DECIMAL -->
								        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="" id="LblG1133_C18556">Precio full sin IVA</label>
								            <input onchange="clearNum(this)" type="text" class="form-control input-sm Decimal" value=""  name="G1133_C18556" id="G1133_C18556" placeholder="Precio full sin IVA">
								        </div>
								        <!-- FIN DEL CAMPO TIPO DECIMAL -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1133_C18557" id="LblG1133_C18557">Descuento</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1133_C18557" id="G1133_C18557">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO DECIMAL -->
								        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1133_C18558" id="LblG1133_C18558">Valor con dto sin IVA</label>
								            <input onchange="clearNum(this)" type="text" class="form-control input-sm Decimal" value=""  name="G1133_C18558" id="G1133_C18558" placeholder="Valor con dto sin IVA">
								        </div>
								        <!-- FIN DEL CAMPO TIPO DECIMAL -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1133_C18559" id="LblG1133_C18559">Vigencia</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1133_C18559" id="G1133_C18559" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					 
								        <!-- CAMPO TIPO TEXTO -->
								        <div class="form-group">
								            <label for="G1133_C18560" id="LblG1133_C18560">Obsequio</label>
								            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1133_C18560" value=""  name="G1133_C18560"  placeholder="Obsequio">
								        </div>
								        <!-- FIN DEL CAMPO TIPO TEXTO -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO MEMO -->
								        <div class="form-group">
								            <label for="G1133_C18561" id="LblG1133_C18561">Observación</label>
								            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1133_C18561" id="G1133_C18561"  value="" placeholder="Observación"></textarea>
								        </div>
								        <!-- FIN DEL CAMPO TIPO MEMO -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1133_C20960" id="LblG1133_C20960">Fecha de lectura de contrato </label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1133_C20960" id="G1133_C20960" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIMEPICKER -->
								        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
								        <div class="bootstrap-timepicker">
								            <div class="form-group">
								                <label for="G1133_C20961" id="LblG1133_C20961">Hora de lectura de contrato</label>
								                <div class="input-group">
								                    <input type="text" class="form-control input-sm Hora"  name="G1133_C20961" id="G1133_C20961" placeholder="HH:MM:SS" >
								                    <div class="input-group-addon" id="TMP_G1133_C20961">
								                        <i class="fa fa-clock-o"></i>
								                    </div>
								                </div>
								                <!-- /.input group -->
								            </div>
								            <!-- /.form group -->
								        </div>
								        <!-- FIN DEL CAMPO TIMEPICKER -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1133_C20962" id="LblG1133_C20962">Fecha de activación </label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1133_C20962" id="G1133_C20962" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIMEPICKER -->
								        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
								        <div class="bootstrap-timepicker">
								            <div class="form-group">
								                <label for="G1133_C20963" id="LblG1133_C20963">Hora de activación </label>
								                <div class="input-group">
								                    <input type="text" class="form-control input-sm Hora"  name="G1133_C20963" id="G1133_C20963" placeholder="HH:MM:SS" >
								                    <div class="input-group-addon" id="TMP_G1133_C20963">
								                        <i class="fa fa-clock-o"></i>
								                    </div>
								                </div>
								                <!-- /.input group -->
								            </div>
								            <!-- /.form group -->
								        </div>
								        <!-- FIN DEL CAMPO TIMEPICKER -->
								<div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1133_Usuario" id="LblG1133_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1133_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>        

					  
					            </div>


					        </div>
					    </div>
					</div>

					<div class="panel box box-primary tareaG" id="TareaG1134" style="display: none;">
					    <div class="box-header with-border">
					        <h4 class="box-title">
					            DATOS - ENVIAR LINK
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
					            <div class="col-md-6 col-xs-6">
							        <div class="form-group">
							            <label for="G1134_C21449" id="LblG1134_C21449">Cupo Usado</label>
							            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1134_C21449" value=""  name="G1134_C21449"  placeholder="Cupo Usado">
							        </div>
							        <!-- FIN DEL CAMPO TIPO TEXTO -->
					            </div>
					            <div class="col-md-6 col-xs-6">
							        <!-- CAMPO TIPO TEXTO -->
							        <div class="form-group">
							            <label for="G1134_C21450" id="LblG1134_C21450">Cupo Nuevo</label>
							            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1134_C21450" value=""  name="G1134_C21450"  placeholder="Cupo Nuevo">
							        </div>
							        <!-- FIN DEL CAMPO TIPO TEXTO -->
					            </div>
					        </div>
					        <div class="row">
					            <div class="col-md-6 col-xs-6">
							        <!-- CAMPO DE TIPO LISTA -->
							        <div class="form-group">
							            <label for="G1134_C18344" id="LblG1134_C18344">Pack</label>
							            <select class="form-control input-sm select2"  style="width: 100%;" name="G1134_C18344" id="G1134_C18344">
							                <option value="0">Seleccione</option>
							                <?php
							                    /*
							                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
							                    */
							                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

							                    $obj = $mysqli->query($Lsql);
							                    while($obje = $obj->fetch_object()){
							                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

							                    }    
							                    
							                ?>
							            </select>
							        </div>
							        <!-- FIN DEL CAMPO TIPO LISTA -->
					            </div>
					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1134_C17481" id="LblG1134_C17481">Cupo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1134_C17481" id="G1134_C17481">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1134_C18345" id="LblG1134_C18345">Tiempo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1134_C18345" id="G1134_C18345">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO DECIMAL -->
								        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1134_C18346" id="LblG1134_C18346">Precio full sin IVA</label>
								            <input onchange="clearNum(this)" type="text" class="form-control input-sm Decimal" value=""  name="G1134_C18346" id="G1134_C18346" placeholder="Precio full sin IVA">
								        </div>
								        <!-- FIN DEL CAMPO TIPO DECIMAL -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1134_C18347" id="LblG1134_C18347">Descuento</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1134_C18347" id="G1134_C18347">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO DECIMAL -->
								        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1134_C18348" id="LblG1134_C18348">Valor con dto sin IVA</label>
								            <input onchange="clearNum(this)" type="text" class="form-control input-sm Decimal" value=""  name="G1134_C18348" id="G1134_C18348" placeholder="Valor con dto sin IVA">
								        </div>
								        <!-- FIN DEL CAMPO TIPO DECIMAL -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1134_C18349" id="LblG1134_C18349">Vigencia</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1134_C18349" id="G1134_C18349" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					 
								        <!-- CAMPO TIPO TEXTO -->
								        <div class="form-group">
								            <label for="G1134_C18350" id="LblG1134_C18350">Obsequio</label>
								            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1134_C18350" value=""  name="G1134_C18350"  placeholder="Obsequio">
								        </div>
								        <!-- FIN DEL CAMPO TIPO TEXTO -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO MEMO -->
								        <div class="form-group">
								            <label for="G1134_C17482" id="LblG1134_C17482">Observación</label>
								            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1134_C17482" id="G1134_C17482"  value="" placeholder="Observación"></textarea>
								        </div>
								        <!-- FIN DEL CAMPO TIPO MEMO -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1134_C20956" id="LblG1134_C20956">Fecha de lectura de contrato</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1134_C20956" id="G1134_C20956" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIMEPICKER -->
								        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
								        <div class="bootstrap-timepicker">
								            <div class="form-group">
								                <label for="G1134_C20957" id="LblG1134_C20957">Hora de lectura de contrato</label>
								                <div class="input-group">
								                    <input type="text" class="form-control input-sm Hora"  name="G1134_C20957" id="G1134_C20957" placeholder="HH:MM:SS" >
								                    <div class="input-group-addon" id="TMP_G1134_C20957">
								                        <i class="fa fa-clock-o"></i>
								                    </div>
								                </div>
								                <!-- /.input group -->
								            </div>
								            <!-- /.form group -->
								        </div>
								        <!-- FIN DEL CAMPO TIMEPICKER -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1134_C20958" id="LblG1134_C20958">Fecha de activación </label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1134_C20958" id="G1134_C20958" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1134_C20959" id="LblG1134_C20959">Hora de activación </label>
								            <input type="text" class="form-control input-sm Hora" value=""  name="G1134_C20959" id="G1134_C20959" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>}

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1134_Usuario" id="LblG1134_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1134_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>


					        </div>
				        </div>
					</div>
					
					<div class="panel box box-primary tareaG" id="TareaG1135" style="display: none;">
					    <div class="box-header with-border">
					        <h4 class="box-title">
					            DATOS - CREAR ORDEN
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
					            <div class="col-md-6 col-xs-6">
							        <!-- CAMPO TIPO TEXTO -->
							        <div class="form-group">
							            <label for="G1135_C21447" id="LblG1135_C21447">Cupo Usado </label>
							            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1135_C21447" value=""  name="G1135_C21447"  placeholder="Cupo Usado ">
							        </div>
							        <!-- FIN DEL CAMPO TIPO TEXTO -->
					            </div>
					            <div class="col-md-6 col-xs-6">
							        <!-- CAMPO TIPO TEXTO -->
							        <div class="form-group">
							            <label for="G1135_C21448" id="LblG1135_C21448">Cupo Nuevo</label>
							            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1135_C21448" value=""  name="G1135_C21448"  placeholder="Cupo Nuevo">
							        </div>
							        <!-- FIN DEL CAMPO TIPO TEXTO -->
					            </div>
					        </div>
					        <div class="row">
					            <div class="col-md-6 col-xs-6">
							        <!-- CAMPO DE TIPO LISTA -->
							        <div class="form-group">
							            <label for="G1135_C18645" id="LblG1135_C18645">Tipo de cliente</label>
							            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18645" id="G1135_C18645">
							                <option value="0">Seleccione</option>
							                <?php
							                    /*
							                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
							                    */
							                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 912 ORDER BY LISOPC_Nombre____b ASC";

							                    $obj = $mysqli->query($Lsql);
							                    while($obje = $obj->fetch_object()){
							                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

							                    }    
							                    
							                ?>
							            </select>
							        </div>
							        <!-- FIN DEL CAMPO TIPO LISTA -->
					            </div>
					            <div class="col-md-6 col-xs-6">
							        <!-- lIBRETO O LABEL -->
							        <p style="text-align:justify;">Lectura de contrato</p>
							        <!-- FIN LIBRETO -->
					            </div>
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1135_C17507" id="LblG1135_C17507">Fecha lectura de contrato</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C17507" id="G1135_C17507" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIMEPICKER -->
								        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
								        <div class="bootstrap-timepicker">
								            <div class="form-group">
								                <label for="G1135_C17508" id="LblG1135_C17508">Hora de lectura de contrato</label>
								                <div class="input-group">
								                    <input type="text" class="form-control input-sm Hora"  name="G1135_C17508" id="G1135_C17508" placeholder="HH:MM:SS" >
								                    <div class="input-group-addon" id="TMP_G1135_C17508">
								                        <i class="fa fa-clock-o"></i>
								                    </div>
								                </div>
								                <!-- /.input group -->
								            </div>
								            <!-- /.form group -->
								        </div>
								        <!-- FIN DEL CAMPO TIMEPICKER -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					 
								        <!-- CAMPO TIPO TEXTO -->
								        <div class="form-group">
								            <label for="G1135_C17509" id="LblG1135_C17509">Extensión</label>
								            <input onchange="clearText(this)" type="text" class="form-control input-sm" id="G1135_C17509" value=""  name="G1135_C17509"  placeholder="Extensión">
								        </div>
								        <!-- FIN DEL CAMPO TIPO TEXTO -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1135_C18646" id="LblG1135_C18646">Pack adquirido</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18646" id="G1135_C18646">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1135_C18647" id="LblG1135_C18647">Cupo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18647" id="G1135_C18647">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1135_C18648" id="LblG1135_C18648">Tiempo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18648" id="G1135_C18648">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1135_C18649" id="LblG1135_C18649">Descuento aplicado</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18649" id="G1135_C18649">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1135_C18650" id="LblG1135_C18650">Fecha de activación</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C18650" id="G1135_C18650" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIMEPICKER -->
								        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
								        <div class="bootstrap-timepicker">
								            <div class="form-group">
								                <label for="G1135_C20955" id="LblG1135_C20955">Hora de activación </label>
								                <div class="input-group">
								                    <input type="text" class="form-control input-sm Hora"  name="G1135_C20955" id="G1135_C20955" placeholder="HH:MM:SS" >
								                    <div class="input-group-addon" id="TMP_G1135_C20955">
								                        <i class="fa fa-clock-o"></i>
								                    </div>
								                </div>
								                <!-- /.input group -->
								            </div>
								            <!-- /.form group -->
								        </div>
								        <!-- FIN DEL CAMPO TIMEPICKER -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1135_C18651" id="LblG1135_C18651">Fecha fin</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C18651" id="G1135_C18651" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO MEMO -->
								        <div class="form-group">
								            <label for="G1135_C17512" id="LblG1135_C17512">Observación</label>
								            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1135_C17512" id="G1135_C17512"  value="" placeholder="Observación"></textarea>
								        </div>
								        
					  
					            </div>

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								       <!--  <div class="form-group" hidden="">
								            <label for="G1135_Usuario" id="LblG1135_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1135_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>


					        </div>
					    </div>
					</div>

					<div class="panel box box-primary tareaG" id="TareaG1136" style="display: none;">
					    <div class="box-header with-border">
					        <h4 class="box-title">
					            DATOS - CLIENTE A ESCALAR
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO DECIMAL -->
								        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1136_C17537" id="LblG1136_C17537">Cupo usados</label>
								            <input onchange="clearNum(this)" type="text" class="form-control input-sm Decimal" value=""  name="G1136_C17537" id="G1136_C17537" placeholder="Cupo usados">
								        </div>
								        <!-- FIN DEL CAMPO TIPO DECIMAL -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
								        <div class="form-group">
								            <label for="G1136_C17536" id="LblG1136_C17536">Usados</label>
								            <div class="checkbox">
								                <label>
								                    <input type="checkbox" value="1" name="G1136_C17536" id="G1136_C17536" data-error="Before you wreck yourself"  > 
								                </label>
								            </div>
								        </div>
								        <!-- FIN DEL CAMPO SI/NO -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO DECIMAL -->
								        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1136_C17539" id="LblG1136_C17539">Cupo nuevos</label>
								            <input onchange="clearNum(this)" type="text" class="form-control input-sm Decimal" value=""  name="G1136_C17539" id="G1136_C17539" placeholder="Cupo nuevos">
								        </div>
								        <!-- FIN DEL CAMPO TIPO DECIMAL -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
								        <div class="form-group">
								            <label for="G1136_C17538" id="LblG1136_C17538">Nuevos</label>
								            <div class="checkbox">
								                <label>
								                    <input type="checkbox" value="1" name="G1136_C17538" id="G1136_C17538" data-error="Before you wreck yourself"  > 
								                </label>
								            </div>
								        </div>
								        <!-- FIN DEL CAMPO SI/NO -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO MEMO -->
								        <div class="form-group">
								            <label for="G1136_C17540" id="LblG1136_C17540">Observación</label>
								            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1136_C17540" id="G1136_C17540"  value="" placeholder="Observación"></textarea>
								        </div>
								        <!-- FIN DEL CAMPO TIPO MEMO -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1136_C18644" id="LblG1136_C18644">Categoría</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C18644" id="G1136_C18644">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 911 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1136_C20948" id="LblG1136_C20948">Pack adquirido</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C20948" id="G1136_C20948">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1136_C20949" id="LblG1136_C20949">Tiempo</label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C20949" id="G1136_C20949">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">


								        <!-- CAMPO DE TIPO LISTA -->
								        <div class="form-group">
								            <label for="G1136_C20950" id="LblG1136_C20950">Descuento </label>
								            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C20950" id="G1136_C20950">
								                <option value="0">Seleccione</option>
								                <?php
								                    /*
								                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
								                    */
								                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

								                    $obj = $mysqli->query($Lsql);
								                    while($obje = $obj->fetch_object()){
								                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

								                    }    
								                    
								                ?>
								            </select>
								        </div>
								        <!-- FIN DEL CAMPO TIPO LISTA -->
					  
					            </div>


					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIPO FECHA -->
								        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
								        <div class="form-group">
								            <label for="G1136_C20951" id="LblG1136_C20951">Fecha de lectura del contrato</label>
								            <input type="text" class="form-control input-sm Fecha" value=""  name="G1136_C20951" id="G1136_C20951" placeholder="YYYY-MM-DD">
								        </div>
								        <!-- FIN DEL CAMPO TIPO FECHA-->
					  
					            </div>

					  
					        </div>


					        <div class="row">
					        

					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO TIMEPICKER -->
								        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
								        <div class="bootstrap-timepicker">
								            <div class="form-group">
								                <label for="G1136_C20952" id="LblG1136_C20952">Hora de lectura del contrato </label>
								                <div class="input-group">
								                    <input type="text" class="form-control input-sm Hora"  name="G1136_C20952" id="G1136_C20952" placeholder="HH:MM:SS" >
								                    <div class="input-group-addon" id="TMP_G1136_C20952">
								                        <i class="fa fa-clock-o"></i>
								                    </div>
								                </div>
								                <!-- /.input group -->
								            </div>
								            <!-- /.form group -->
								        </div>
								        <!-- FIN DEL CAMPO TIMEPICKER -->
							        				  
					            </div>
					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1136_Usuario" id="LblG1136_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1136_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>


					        </div>
					    </div>
					</div>

					<div class="panel box box-primary tareaG" id="TareaG1137" style="display: none;">
					    <div class="box-header with-border">
					        <h4 class="box-title">
					            DATOS - ENVIAR TUTORIALES
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
					            <div class="col-md-12 col-xs-12">
							        <div class="form-group">
							            <label for="G1137_C17564" id="LblG1137_C17564">Observación</label>
							            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1137_C17564" id="G1137_C17564"  value="" placeholder="Observación"></textarea>
							        </div>
					            </div>
					           
					        </div>
					        <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1137_Usuario" id="LblG1137_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1137_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>
					    </div>
					</div>

					<div class="panel box box-primary tareaG" id="TareaG1138" style="display: none;">
					    <div class="box-header with-border">
					        <h4 class="box-title">
					            DATOS - ENVIAR PRESENTACIÓN
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
					            <div class="col-md-12 col-xs-12">
							        <div class="form-group">
							            <label for="G1138_C17588" id="LblG1138_C17588">Observacion</label>
							            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1138_C17588" id="G1138_C17588"  value="" placeholder="Observacion"></textarea>
							        </div>
					            </div>
					            
					        </div>
					        <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1138_Usuario" id="LblG1138_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1138_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>
					    </div>
					</div>

					<div class="panel box box-primary tareaG" id="TareaG1139" style="display: none;" >
					    <div class="box-header with-border">
					        <h4 class="box-title">
					            DATOS - SACAR DE LA LISTA NEGRA
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
					            <div class="col-md-12 col-xs-12">
							        <div class="form-group">
							            <label for="G1139_C17612" id="LblG1139_C17612">Causa</label>
							            <select class="form-control input-sm select2"  style="width: 100%;" name="G1139_C17612" id="G1139_C17612">
							                <option value="0">Seleccione</option>
							                <?php
							                    /*
							                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
							                    */
							                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 861 ORDER BY LISOPC_Nombre____b ASC";

							                    $obj = $mysqli->query($Lsql);
							                    while($obje = $obj->fetch_object()){
							                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

							                    }    
							                    
							                ?>
							            </select>
							        </div>
					            </div>
					            <div class="col-md-12 col-xs-12">
							        <div class="form-group">
							            <label for="G1139_C17613" id="LblG1139_C17613">Observación</label>
							            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1139_C17613" id="G1139_C17613"  value="" placeholder="Observación"></textarea>
							        </div>

					            </div>
					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1139_Usuario" id="LblG1139_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1139_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>
					            
					        </div>
					    </div>
					</div>

					<div class="panel box box-primary tareaG" id="TareaG1140" style="display: none;" >
					    <div class="box-header with-border">
					        <h4 class="box-title">
								DATOS - SOLICITUD ESPECIAL
					        </h4>
					    </div>
					    <div class="box-body">
					        <div class="row">
					            <div class="col-md-12 col-xs-12">
							        <div class="form-group">
							            <label for="G1140_C17637" id="LblG1140_C17637">Tipo de solicitud</label>
							            <select class="form-control input-sm select2"  style="width: 100%;" name="G1140_C17637" id="G1140_C17637">
							                <option value="0">Seleccione</option>
							                <?php
							                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 864 ORDER BY LISOPC_Nombre____b ASC";

							                    $obj = $mysqli->query($Lsql);
							                    while($obje = $obj->fetch_object()){
							                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

							                    }    
							                ?>
							            </select>
							        </div>
					            </div>
					            <div class="col-md-12 col-xs-12">
							        <div class="form-group">
							            <label for="G1140_C17638" id="LblG1140_C17638">Observación</label>
							            <textarea onchange="clearText(this)" class="form-control input-sm" name="G1140_C17638" id="G1140_C17638"  value="" placeholder="Observación"></textarea>
							        </div>
							        
					            </div>
					            <div class="col-md-6 col-xs-6">

					  
								        <!-- CAMPO hidden nombre asesor-->
								        <!-- este campo esta oculto para enviar el ID del asesor y enviar al formulario -->
								        <!-- <div class="form-group" hidden="">
								            <label for="G1140_Usuario" id="LblG1140_Usuario">Nombre  </label>
								            <input type="text" class="form-control input-sm" value=""  name="G1140_Usuario" id="G1122_Usuario" placeholder="Nombre">

								        </div> -->
								        <!-- FIN DEL CAMPO nombre asesor -->
					  
					            </div>
					        </div>
					    </div>
					</div>


<input type="hidden" name="#idUltimo" id="#idUltimo">					

		            <input type="hidden" name="id" id="iddelatablabackoficee">
		            <!-- <input type="hidden" name="oper" id="oper" value='edit'> -->
		            <input type="hidden" name="gestion" id="gestion" value=''>
				</form>
            </div>
            
            <div class="modal-footer">
            	<button class="btn btn-default cerradorDeModal" type="button"  data-dismiss="modal" >
                    CERRAR
                </button>
                <button class="btn btn-primary pull-right" type="button" id="BackoficceGuardar">
                	GUARDAR
                </button>
            </div>
        </div>
    </div>
</div>

<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1122/G1122_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1122_ConsInte__b as id, G1122_C17121 as camp1 , G1122_C17122 as camp2 FROM ".$BaseDatos.".G1122  WHERE G1122_Usuario = ".$idUsuario." ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1122_ConsInte__b as id, G1122_C17121 as camp1 , G1122_C17122 as camp2 FROM ".$BaseDatos.".G1122  ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1122_ConsInte__b as id, G1122_C17121 as camp1 , G1122_C17122 as camp2 FROM ".$BaseDatos.".G1122  ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php


if(isset($_GET['user'])){


	$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);

    $datoCampan = $res_Lsql_Campan->fetch_array();


    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

	$XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";

	$nombre = $mysqli->query($XLsql);

	$nombreUsuario = NULL;

	$color = 'rgb(110, 197, 255)';
	$devuelto = null;
	//echo $int_Muest_Campan;
	$LsqlVencidas = "SELECT * FROM ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 2 AND ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = '2000-01-02 00:00:00' AND ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_GET['user'];

	$resVencida = $mysqli->query($LsqlVencidas);


	if($resVencida){
		if($resVencida->num_rows > 0){
			/*Esta agendado devuelto*/
			$color = 'rgb(255, 0, 0)';
			$devuelto = '(Tarea regresada)';
		}
	}
	//echo $XLsql;

	while ($key = $nombre->fetch_object()) {

	 	echo "<div class='row'><div class='col-md-9'><h3 style='color: ".$color.";'>".$key->nombre." ".$devuelto."</h3></div><div class='col-md-3'><button type=\"button\" data-toggle=\"modal\" data-target=\"#verAgenda\" class=\"btn btn-default pull-right\"><i class=\"fa fa-calendar\"></i>&nbsp;VER MIS AGENDAS</button></div></div>";  
	 	$nombreUsuario = $key->nombre;

	 	break;
	} 


	if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


					
		$data = array(	"strToken_t" => $_GET['token'], 
						"strIdGestion_t" => $_GET['id_gestion_cbx'],
						"strDatoPrincipal_t" => $nombreUsuario,
						"strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
		$data_string = json_encode($data);    

		$ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                      
		); 
		//recogemos la respuesta
		$respuesta = curl_exec ($ch);
		//o el error, por si falla
		$error = curl_error($ch);
		//y finalmente cerramos curl
		//echo "Respuesta =>  ". $respuesta;
		//echo "<br/>Error => ".$error;
		//include "Log.class.php";
		//$log = new Log("log", "./Log/");
		//$log->insert($error, $respuesta, false, true, false);
		//echo "nada";
		curl_close ($ch);
	}
}else{
	echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";	
}
?>



<?php 
	$codigoCliente= NULL;
	if(isset($_GET['nuevoregistro'])){ 
		/*
		$Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 20661"; 
		$res = $mysqli->query($Lsql); 

		$dato = $res->fetch_array(); 
		$codigoCliente = $dato["CONTADORES_Valor_b"] + 1; 
		$codigoCliente = date("y").str_pad($codigoCliente, 7, "0", STR_PAD_LEFT);
		$XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = CONTADORES_Valor_b + 1 WHERE CONTADORES_ConsInte__PREGUN_b = 20661 ";*/
		$Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 20661";
        $res = $mysqli->query($Lsql);
        $dato = $res->fetch_array();
        $codigoCliente = $dato["CONTADORES_Valor_b"] + 1;
        $nuevoNumero=$dato["CONTADORES_Valor_b"] + 1;
        $codigoCliente = date("y").str_pad($codigoCliente, 7, "0", STR_PAD_LEFT);
        $XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = ".$nuevoNumero." WHERE CONTADORES_ConsInte__PREGUN_b = 20661 ";
        $actualizacion=$mysqli->query($XLsql);


	} 
?>
<input type="hidden" id="idRegTareas">
<input type="hidden" id="idTareasBack">
<input type="hidden" name="intConsInteBd" id="intConsInteBd" value="<?php if(isset($_GET["user"])) { echo $_GET["user"]; }else{ echo "-1";  } ?>">
<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2140">
                DATOS DE LA BASE DE DATOS  
            </a>
        </h4>
    </div>
    <div id="s_2140" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C20661" id="LblG1122_C20661">Código Cliente </label>
			            <input type="text" class="form-control input-sm" id="G1122_C20661" readonly value="<?php echo $codigoCliente; ?>"  name="G1122_C20661"  placeholder="Código Cliente ">
			        </div>
			        <div class="form-group">
			            <label for="G1122_CodigoMiembro" id="LblG1122_CodigoMiembro">Código Miembro</label>
			            <input type="text" class="form-control input-sm" id="G1122_CodigoMiembro" readonly value="" name="G1122_CodigoMiembro"  placeholder="Código Miembro">
			        </div>			        
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17121" id="LblG1122_C17121">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17121" value="" name="G1122_C17121"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C30194" id="LblG1122_C30194">OBSERVACIONES PERFILES</label>
			            <textarea type="text" readonly class="form-control input-sm" id="G1122_C30194" value="" name="G1122_C30194"  placeholder="OBSERVACIONES PERFILES"></textarea> 
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			         
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17122" id="LblG1122_C17122">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17122" value="" name="G1122_C17122"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C18857" id="LblG1122_C18857">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C18857" value="" name="G1122_C18857"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C20662" id="LblG1122_C20662">ID Contacto Comercial </label>
			            <input type="text" class="form-control input-sm" id="G1122_C20662" value=""  name="G1122_C20662"  placeholder="ID Contacto Comercial ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C20663" id="LblG1122_C20663">Fecha de expedición </label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1122_C20663" id="G1122_C20663" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17123" id="LblG1122_C17123">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17123" value="<?php if (isset($_GET["G1122_C17123"])) {
			            	echo $_GET["G1122_C17123"];
			            } ?>" name="G1122_C17123"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17124" id="LblG1122_C17124">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17124" value="<?php if (isset($_GET["G1122_C17124"])) {
			            	echo $_GET["G1122_C17124"];
			            } ?>" name="G1122_C17124"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17125" id="LblG1122_C17125">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17125" value="" name="G1122_C17125"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17126" id="LblG1122_C17126">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17126" value="" name="G1122_C17126"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17127" id="LblG1122_C17127">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17127" value="" name="G1122_C17127"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17128" id="LblG1122_C17128">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17128" value="" name="G1122_C17128"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17129" id="LblG1122_C17129">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17129" value="<?php if (isset($_GET["G1122_C17129"])) {
			            	echo $_GET["G1122_C17129"];
			            } ?>" name="G1122_C17129"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17130" id="LblG1122_C17130">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17130" value="" name="G1122_C17130"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17131" id="LblG1122_C17131">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17131" value="" name="G1122_C17131"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17132" id="LblG1122_C17132">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17132" value="" name="G1122_C17132"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18858" id="LblG1122_C18858">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value="" name="G1122_C18858" id="G1122_C18858" placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C18351" id="LblG1122_C18351">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C18351" id="G1122_C18351">
			                <option value="0">Seleccione</option>


<?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                //cambio
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

                                

                                $obj = $mysqli->query($Lsql);
                                ?><option value="15415">vetado</option><?php
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }

                                //fin
                                
                            ?>
                        </select>



			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18859" id="LblG1122_C18859">FECHA ACTIVACION</label>
			            <input type="text" class="form-control input-sm Fecha" value="" name="G1122_C18859" id="G1122_C18859" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18860" id="LblG1122_C18860">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value="" name="G1122_C18860" id="G1122_C18860" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div id="2142" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17138" id="LblG1122_C17138">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17138" value="<?php echo getNombreUser($token);?>" readonly name="G1122_C17138"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17139" id="LblG1122_C17139">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17139" value="<?php echo date('Y-m-d');?>" readonly name="G1122_C17139"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17140" id="LblG1122_C17140">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17140" value="<?php echo date('H:i:s');?>" readonly name="G1122_C17140"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17141" id="LblG1122_C17141">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17141" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1122_C17141"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2144">
                DATOS DE PERFIL
            </a>
        </h4>
    </div>
    <div id="s_2144" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17144" id="LblG1122_C17144">¿Es ó ha sido cliente de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17144" id="G1122_C17144">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);

			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17145" id="LblG1122_C17145">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17145" id="G1122_C17145">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1122_C17146" id="LblG1122_C17146">¿Desde que departamento realiza la administración de los inmuebles?</label>
			            <select class="form-control input-sm select2" style="width: 100%;"  name="G1122_C17146" id="G1122_C17146">
			                <option value="0">Seleccione</option>

			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C18721" id="LblG1122_C18721">Cupo autorizado</label>
			            <input type="text" class="form-control input-sm" id="G1122_C18721" value="" readonly="true"  name="G1122_C18721"  placeholder="Cupo autorizado">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18720" id="LblG1122_C18720">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1122_C18720" id="G1122_C18720" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C17147" id="LblG1122_C17147">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1122_C17147" id="G1122_C17147" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>


<div id="2141" >


</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">HISTÓRICO </a>
        </li>
        <li class="">
            <a href="#tab_1" data-toggle="tab" id="tabs_click_1">ENVIAR TUTORIALES</a>
        </li>

        <li class="">
            <a href="#tab_2" data-toggle="tab" id="tabs_click_2">ENVIAR PROPUESTA</a>
        </li>

        <li class="">
            <a href="#tab_3" data-toggle="tab" id="tabs_click_3">ENVIAR LINK</a>
        </li>

        <li class="">
            <a href="#tab_4" data-toggle="tab" id="tabs_click_4">CREAR ORDEN</a>
        </li>

        <li class="">
            <a href="#tab_5" data-toggle="tab" id="tabs_click_5">CLIENTE A ESCALAR</a>
        </li>

        <li class="">
            <a href="#tab_6" data-toggle="tab" id="tabs_click_6">ENVIAR PRESENTACIÓN</a>
        </li>

        <li class="">
            <a href="#tab_7" data-toggle="tab" id="tabs_click_7">SACAR DE LISTA NEGRA</a>
        </li>

        <li class="">
            <a href="#tab_8" data-toggle="tab" id="tabs_click_8">SOLICITUD ESPECIAL</a>
        </li>
    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
        </div>

                <div class="tab-pane " id="tab_1"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless1" width="100%">
            </table>
            <div id="pagerDetalles1">
            </div>
        </div>

        <div class="tab-pane " id="tab_2"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless2" width="100%">
            </table>
            <div id="pagerDetalles2">
            </div> 
        </div>

        <div class="tab-pane " id="tab_3"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless3" width="100%">
            </table>
            <div id="pagerDetalles3">
            </div> 
        </div>

        <div class="tab-pane " id="tab_4"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless4" width="100%">
            </table>
            <div id="pagerDetalles4">
            </div> 
        </div>

        <div class="tab-pane " id="tab_5"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless5" width="100%">
            </table>
            <div id="pagerDetalles5">
            </div> 
        </div>

        <div class="tab-pane " id="tab_6"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless6" width="100%">
            </table>
            <div id="pagerDetalles6">
            </div> 
        </div>

        <div class="tab-pane " id="tab_7"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless7" width="100%">
            </table>
            <div id="pagerDetalles7">
            </div> 
        </div>

        <div class="tab-pane " id="tab_8"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless8" width="100%">
            </table>
            <div id="pagerDetalles8">
            </div> 
        </div>


    </div>

</div>
<div class="row" id="crearTareas">
    <div class="col-md-6 col-xs-6">
        <select class="form-control input-sm"  name="tarea" id="tarea">
            <option value="0">Seleccione</option>
            <option value="1133" >Enviar Propuesta</option>
            <option value="1136" >Cliente Escalar</option>
            <option value="1134" >Enviar Link</option>
            <option value="1135" >Crear Orden</option>
            <option value="1137" >Enviar Tutoria</option>
            <option value="1138" >Enviar Presentacion</option>
            <option value="1139" >Sacar Lista Negra</option>
            <option value="1140" >Solicitud Especial</option>
        </select>
    </div>
    <div class="col-md-6 col-xs-6">
        <button class="btn btn-primary btn-block" id="crearTarea" type="button">
            GENERAR TAREA
        </button>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-4">
        <div class="form-group">
        	<label for="G1122_C17150">TIPIFICACION 1</label>
	            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17150" id="G1122_C17150">
	                <option value="0">Seleccione</option>


					<?php
                        /*
                            SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                        */
                        //cambio
	                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 829 ORDER BY LISOPC_Nombre____b ASC";

                        

                        $obj = $mysqli->query($Lsql);
                        while($obje = $obj->fetch_object()){
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                        }

                        //fin
                        
                    ?>
                </select>
    	</div>
    </div>
    <div class="col-md-10 col-xs-4">
        <div class="form-group">
        	<label for="G1122_C17133">TIPIFICACION 2</label>
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1122_C17133">
                <option value="0">Seleccione</option>
                <?php
                // $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                //         JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                //         WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 826;";
                // $obj = $mysqli->query($Lsql);
                // while($obje = $obj->fetch_object()){
                //     echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                // }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
   	<div class="col-md-2 col-xs-4" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-6 col-xs-6">
        <div class="form-group">
        	<label for="G1122_C17150">TIPIFICACION 1</label>
	            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17150" id="G1122_C17150">
	                <option value="0">Seleccione</option>


					<?php
                        /*
                            SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                        */
                        //cambio
	                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 829 ORDER BY LISOPC_Nombre____b ASC";

                        

                        $obj = $mysqli->query($Lsql);
                        while($obje = $obj->fetch_object()){
                            echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                        }

                        //fin
                        
                    ?>
                </select>
    	</div>
    </div>
    <div class="col-md-6 col-xs-6">
        <div class="form-group">
        	<label for="G1122_C17133">TIPIFICACION 2</label>
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1122_C17133">
                <option value="0">Seleccione</option>
                <?php
                // $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                //         JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                //         WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 826;";
                // $obj = $mysqli->query($Lsql);
                // while($obje = $obj->fetch_object()){
                //     echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                // }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
            <input type="hidden" name="valReintento" id="valReintento" value="0">
        </div>
    </div>
    <?php } ?>
</div>

<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
        <label for="G1122_C17134">Tipo de reintento</label>
            <select disabled class="form-control input-sm reintento" name="reintento" id="G1122_C17134">
                <option value="0"></option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
        	<label for="G1122_C17135">Fecha Agenda</label>
            <input type="text" name="TxtFechaReintento" id="G1122_C17135" class="form-control input-sm TxtFechaReintento">
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
        	<label for="G1122_C17136">Hora Agenda</label>
            <input type="text" name="TxtHoraReintento" id="G1122_C17136" class="form-control input-sm TxtHoraReintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea onchange="clearText(this)" class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1122_C17137" placeholder="Observaciones" style="height: 120px"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1122/G1122_eventos.js"></script> 
<script type="text/javascript" src="formularios/G1122/monent.js"></script> 
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

	function nulear(valor_p){

		if (valor_p == "" || valor_p == '' || valor_p === null) {

			return "NULL";

		}else{

			return valor_p;
		}

	}

    function updateFicha(intIdFicha_p){

    	objFicha_t = {
    					G1121_ConsInte__b : intIdFicha_p,
    					G1121_C17108 : nulear($("#G1122_C17123").val()),
    					G1121_C17112 : nulear($("#G1122_C17127").val()),
    					G1121_C17117 : nulear($("#G1122_C17132").val()),
    					G1121_C20659 : nulear($("#G1122_C20661").val()),
    					G1121_C17643 : nulear($("#G1122_C18857").val()),
    					G1121_C18889 : nulear($("#G1122_C17147").val()),
    					G1121_C18888 : nulear($("#G1122_C18720").val()),
    					G1121_C17644 : nulear($("#G1122_C18858").val()),
    					G1121_C17113 : nulear($("#G1122_C17128").val()),
    					G1121_C18887 : nulear($("#G1122_C17146").val()),
    					G1121_C17115 : nulear($("#G1122_C17130").val()),
    					G1121_C18885 : nulear($("#G1122_C17144").val()),
    					G1121_C17645 : nulear($("#G1122_C18351").val()),
    					G1121_C17647 : nulear($("#G1122_C18859").val()),
    					G1121_C17646 : nulear($("#G1122_C18860").val()),
    					G1121_C20660 : nulear($("#G1122_C20662").val()),
    					G1121_C18886 : nulear($("#G1122_C17145").val()),
    					G1121_C17114 : nulear($("#G1122_C17129").val()),
    					G1121_C17106 : nulear($("#G1122_C17121").val()),
    					G1121_C17107 : nulear($("#G1122_C17122").val()),
    					G1121_C17109 : nulear($("#G1122_C17124").val()),
    					G1121_C17110 : nulear($("#G1122_C17125").val()),
    					G1121_C17111 : nulear($("#G1122_C17126").val()),
    					updateFicha : true
    				  }

        $.ajax({
            url    : '<?php echo $url_crud; ?>?',
            type   : 'post',
            data   : objFicha_t,
            success : function(data){
            	console.log(data)
            }
        });

    }

    function clearText(input){
        input.value = input.value.replace(/[^a-zA-Z0-9 ñÑ @ ., _-]/g, "")
    }
    function clearNum(input){
        input.value = input.value.replace(/[^0-9]/g, "")
    }
    $(function(){


    $("#G1122_C17150").change(function(){  

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHijaTip : true, idPadre : $(this).val() },
            success : function(data){
                $("#G1122_C17133").html(data);
                $("#G1122_C17133").val($("#G1122_C17133 option:first").val()).trigger("change"); 
            }
        });
        
    });


    	$("#G1122_C17133").select2();

		$("#crearTarea").click(function(){

			intIdFicha_t = <?php if (isset($_GET["user"])) { echo $_GET["user"]; }else{ echo 0; } ?>

			if (intIdFicha_t != 0) {

				updateFicha(intIdFicha_t);
				
			}


			enviaralCrud(11,$('#idUltimo').val(),$('#tarea').val());

		});


		$("#crearTarea").attr('disabled',true);

        $("#tarea").on('change', function() {
        	if ($(this).val() == "0") {
	    		$("#crearTarea").attr('disabled',true);
	    	}else{
        		$("#crearTarea").attr('disabled',false);
	    	}
        });


    	$("#misagendasTable").DataTable({
    		"language" : {
		        "sProcessing":     "Procesando...",
		        "sLengthMenu":     "Mostrar _MENU_ registros",
		        "sZeroRecords":    "No se encontraron resultados",
		        "sEmptyTable":     "Ningún dato disponible en esta tabla",
		        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
		        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
		        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		        "sInfoPostFix":    "",
		        "sSearch":         "Buscar:",
		        "sUrl":            "",
		        "sInfoThousands":  ",",
		        "sLoadingRecords": "Cargando...",
		        "oPaginate": {
		            "sFirst":    "Primero",
		            "sLast":     "Último",
		            "sNext":     "Siguiente",
		            "sPrevious": "Anterior"
		        },

		        "oAria": {
		            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
		        }
		    } ,
		    "scrollX": false
    	});

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario

                $.each(data, function(i, item) {
                   

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id);
 
                    $("#G1122_C20661").val(item.G1122_C20661);

                    $("#G1122_CodigoMiembro").val(item.G1122_CodigoMiembro);

                    $("#G1122_Usuario").val(item.G1122_Usuario);
 
                    $("#G1122_C17121").val(item.G1122_C17121);
 
                    $("#G1122_C17122").val(item.G1122_C17122);
 
                    $("#G1122_C18857").val(item.G1122_C18857);
 
                    $("#G1122_C20662").val(item.G1122_C20662);
 
                    $("#G1122_C20663").val(item.G1122_C20663);
 
                    $("#G1122_C17123").val(item.G1122_C17123);
 
                    $("#G1122_C17124").val(item.G1122_C17124);
 
                    $("#G1122_C17125").val(item.G1122_C17125);
 
                    $("#G1122_C17126").val(item.G1122_C17126);
 
                    $("#G1122_C17127").val(item.G1122_C17127);
 
                    $("#G1122_C17128").val(item.G1122_C17128);
 
                    $("#G1122_C17129").val(item.G1122_C17129);
 
                    $("#G1122_C17130").val(item.G1122_C17130);
 
                    $("#G1122_C17131").val(item.G1122_C17131);
 
                    $("#G1122_C17132").val(item.G1122_C17132);
 
                    $("#G1122_C18858").val(item.G1122_C18858);
 
                    $("#G1122_C18351").val(item.G1122_C18351);
 
                    $("#G1122_C18859").val(item.G1122_C18859);
 
                    $("#G1122_C18860").val(item.G1122_C18860);
 
                    $("#G1122_C17133").val(item.G1122_C17133);
 
                    $("#G1122_C17134").val(item.G1122_C17134);
 
                    $("#G1122_C17135").val(item.G1122_C17135);
 
                    $("#G1122_C17136").val(item.G1122_C17136);
 
                    $("#G1122_C17137").val(item.G1122_C17137);
 
                    $("#G1122_C17138").val(item.G1122_C17138);
 
                    $("#G1122_C17139").val(item.G1122_C17139);
 
                    $("#G1122_C17140").val(item.G1122_C17140);
 
                    $("#G1122_C17141").val(item.G1122_C17141);

                    $("#G1122_C30194").val(item.G1122_C30194);
   
                    if(item.G1122_C17142 == 1){
                        $("#G1122_C17142").attr('checked', true);
                    } 
   
                    if(item.G1122_C17143 == 1){
                        $("#G1122_C17143").attr('checked', true);
                    } 
 
                    $("#G1122_C17144").val(item.G1122_C17144);
 
                    $("#G1122_C17145").val(item.G1122_C17145);
 
                    $("#G1122_C17146").val(item.G1122_C17146);
 
                    $("#G1122_C17146").val(item.G1122_C17146).trigger("change"); 
 
                    $("#G1122_C18721").val(item.G1122_C18721);
 
                    $("#G1122_C18720").val(item.G1122_C18720);
 
                    $("#G1122_C17147").val(item.G1122_C17147);
 
                    $("#G1122_C17149").val(item.G1122_C17149);
 
                    $("#G1122_C17150").val(item.G1122_C17150);
 
                    $("#G1122_C17151").val(item.G1122_C17151);
 
                    $("#G1122_C17152").val(item.G1122_C17152);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });
		

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
    		$("#btnLlamar_0").attr('padre', $("#G1122_C17129").val());
    		var id_0 = $("#G1122_C17129").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_1").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_2").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_3").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_4").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_5").click(function(){ 
           	vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_6").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_7").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#tabs_click_8").click(function(){ 
            vamosRecargaLasGrillasPorfavor(0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1226&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20657<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    //$("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);

                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>', 

                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1226&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20657&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                //$("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1226&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20657&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        

	    $("#G1122_C17144").select2();

	    $("#G1122_C17145").select2();

        $("#G1122_C17146").select2({
        	placeholder: "Buscar",
	        allowClear: false,
	        minimumInputLength: 3,
	        ajax: {
	            url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1122_C17146=si',
	            dataType: 'json',
	            type : 'post',
	            delay: 250,
	            data: function (params) {
	                return {
	                    q: params.term
	                };
	            },
              	processResults: function(data) {
				  	return {
				    	results: $.map(data, function(obj) {
				      		return {
				        		id: obj.id,
				        		text: obj.text
				      		};
				    	})
				  	};
				},
	            cache: true
	        }
    	});            
        
        $("#G1122_C17146").change(function(){
            $.ajax({
        		url   : '<?php echo $url_crud;?>',
        		data  : { dameValoresCamposDinamicos_Guion_G1122_C17146 : $(this).val() },
        		type  : 'post',
        		dataType : 'json',
        		success  : function(data){
        			$("#G1122_C18721").val(data[0].cupo);	
        		}
        	});
        });
                                      

	    $("#G1122_C17149").select2();

	    $("#G1122_C17150").select2();

	    $("#G1122_C17151").select2();

	    $("#G1122_C17152").select2();
        //datepickers
        

        $("#G1122_C20663").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1122_C18859").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1122_C18860").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1122_C17135").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });


        $(".Fecha").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1122_C17136").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1122_C18858").numeric();
		        
    	$("#G1122_C18720").numeric();
		        
    	$("#G1122_C17147").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


	    //function para ESTADO CLIENTE 

	    $("#G1122_C18351").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

	    });

	    //function para ¿Es ó ha sido cliente de Fincaraiz? 

	    $("#G1122_C17144").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

	    });

	    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

	    $("#G1122_C17145").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

	    });

	    //function para ESTADO 

	 

	    //function para TIPIFICACION 1 

	    $("#G1122_C17150").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

			$.ajax({
				url    : '<?php echo $url_crud; ?>',
				type   : 'post',
				data   : { getListaHija : true , opcionID : '830' , idPadre : $(this).val() },
				success : function(data){
					$("#G1122_C17151").html(data);
				}
			});
			
	    });

	    //function para TIPIFICACION 2 

	    $("#G1122_C17151").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

			$.ajax({
				url    : '<?php echo $url_crud; ?>',
				type   : 'post',
				data   : { getListaHija : true , opcionID : '831' , idPadre : $(this).val() },
				success : function(data){
					$("#G1122_C17152").html(data);
				}
			});
			
	    });

	    //function para TIPIFICACION 3 

	    $("#G1122_C17152").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

	    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){

		        	$("#Save").attr('disabled');
		        	var bol_respuesta = before_save();
		        	var d = new Date();
		            var h = d.getHours();
		            var horas = (h < 10) ? '0' + h : h;
		            var dia = d.getDate();
		            var dias = (dia < 10) ? '0' + dia : dia;
		            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
		            $("#FechaFinal").val(fechaFinal);
		            var valido = 0;
		            
		            if($(".tipificacion").val() == '0'){
		            	alertify.error("Es necesaria la tipificación!");
		            	$("#G1122_C17133").closest('.form-group').addClass('has-error');
		            	valido = 1;
		            }

		            if($(".reintento").val() == '2'){
		            	if($(".TxtFechaReintento").val().length < 1){
		            		alertify.error("Es necesario llenar la fecha de reintento!");
		            		$(".TxtFechaReintento").focus();
		            		valido = 1;
		            	}

		            	if($(".TxtHoraReintento").val().length < 1){
		            		alertify.error("Es necesario llenar la hora de reintento!");
		            		$(".TxtHoraReintento").focus();
		            		valido = 1;
		            	}
		            }

		            if (Number($("#G1122_C17150").val())<1) {
		            	alertify.error('"TIPIFICACION 1" debe ser diligenciado!');
		            	$("#G1122_C17150").closest(".form-group").addClass("has-error");
		            	valido = 1;
		            }

		            if (Number($("#G1122_C17133 > option").length) > 1) {

			            if (Number($("#G1122_C17133").val())<1) {
			            	alertify.error('"TIPIFICACION 2" debe ser diligenciado!');
			            	$("#G1122_C17133").closest(".form-group").addClass("has-error");
			            	valido = 1;
			            }

		            }

		            if($(".textAreaComentarios").val().length < 1){
		            	alertify.error("Es necesario escribir una observación!");
		        		$(".textAreaComentarios").focus();
		        		valido = 1;
		            }

		            if(valido == '0'){
		            			$("#Save").attr("disabled",true);
			        			swal({
						            title: '¿Desea Salir de Tarea?',
						            text: "¡Puede cancelar si no esta seguro!",
						            type: 'warning',
						            showCancelButton: true,
						            confirmButtonColor: '#3085d6',	
						            cancelButtonColor: '#d33',
						            cancelButtonText: 'Cancelar',
						            confirmButtonText: 'Si, Salir!'
						        },function(isConfirm) {

						       				        	
						            if (isConfirm) {

						            	
						            	enviaralCrud(0,'');	            	
						            					               

						            }
						            else{
						            	$('.modal-backdrop').remove();       //eliminamos el backdrop del modal  JLC
  										$('body').removeClass('modal-open'); //eliminamos la clase del body para poder hacer scroll JLC
										$("#EditarDatosTarea").modal('hide');//ocultamos el modal  JLC	
										$(".tareaG").css("display","none");	
										$("#Save").attr('disabled', false);
						            	// enviaralCrud(1,'');

						            }
						        });
		          	}else{
		          		$("#Save").attr('disabled', false);
		          	}
        });

		
		$("#G1122_C17133").change(function(){
			var id = $(this).attr('id');
            var valor = $("#"+ id +" option:selected").attr('efecividad');
            var monoef = $("#"+ id +" option:selected").attr('monoef');
            var TipNoEF = $("#"+ id +" option:selected").attr('TipNoEF');
            var cambio = $("#"+ id +" option:selected").attr('cambio');
            var importancia = $("#"+ id + " option:selected").attr('importancia');
            var contacto = $("#"+id+" option:selected").attr('contacto');
            $("#Efectividad").val(valor);
            $("#MonoEf").val(monoef);
            $("#TipNoEF").val(TipNoEF);
            $("#MonoEfPeso").val(importancia);
            $("#ContactoMonoEf").val(contacto);
            
            // if(cambio != '-1'){
            //     $(".reintento").attr('disabled', true);
            // }
			$(".reintento").val(TipNoEF).change();

		});



		$("#BackoficceGuardar").click(function(){
			
			var form = $("#tareasBackoffice");
	        //Se crean un array con los datos a enviar, apartir del formulario 
	        var formData = new FormData($("#tareasBackoffice")[0]);

	        formData.append("oper","edit");

	        var urlGestion = $("#gestion").val();
	        console.log(formData);
	    	$.ajax({
	    		url   : urlGestion+'?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>&ValAgen=<?php echo getIdentificacionUser($token);?>',
	    		type  : "post",
	    		data  : formData,
			 	cache: false,
	            contentType: false,
	            processData: false,
	    		success : function(xt){
	    			/*linea quitada*/
	    			$('.modal-backdrop').remove();       //eliminamos el backdrop del modal  JLC
  					$('body').removeClass('modal-open'); //eliminamos la clase del body para poder hacer scroll JLC
					$("#EditarDatosTarea").modal('hide');//ocultamos el modal  JLC	
					$(".tareaG").css("display","none"); //limpia el modal para eviatar la multitarea 
	    			// enviarfinalizacion($("#idUltimoGuardado").val());//envia datos complete 
	    			

	    			/*linea agregada*/
	    			//window.location.href = "quitar.php";

	    		}
	    	});
			
		});

		$(".cerradorDeModal").click(function(){

	        var formData = new FormData($("#tareasBackoffice")[0]);
	        var urlGestion = $("#gestion").val();
	    	$.ajax({
	    		url   : urlGestion+'?EliminarTareaCancelada=si',
	    		type  : "post",
	    		data  : formData,
			 	cache: false,
	            contentType: false,
	            processData: false,
	    		success : function(xt){
	    			// enviarfinalizacion($("#idUltimoGuardado").val());

	    		}
	    	});
					$('.modal-backdrop').remove();       //eliminamos el backdrop del modal  JLC
  					$('body').removeClass('modal-open'); //eliminamos la clase del body para poder hacer scroll JLC
					$("#EditarDatosTarea").modal('hide');//ocultamos el modal  JLC	
					$(".tareaG").css("display","none");  //limpia el modal para eviatar la multitarea 
		});

    });

        //funcionalidad del boton Gestion botonCerrarErronea
        
    function enviarfinalizacion(ultimoId,mensaje = null){

<?php $tiempoDesdeInicio = date('Y-m-d H:i:s'); ?>

    	var form = $("#FormularioDatos");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#FormularioDatos")[0]);
    	$.ajax({
    		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+ultimoId +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
    		type  : "post",
    		data  : formData,
		 	cache: false,
            contentType: false,
            processData: false,
    		success : function(xt){
    			console.log("IdDeLlamada1:"+$("#idLlamada").val());
    			idRegTareas = $("#idRegTareas").val();
    			idTareasBack = $("#idTareasBack").val()

    			$.ajax({
    				url : '<?=$url_crud;?>?UpdateTask=si',
    				type: "post",
    				data: {idRegTareas : idRegTareas, idTareasBack : idTareasBack},
    				success : function(data){}
    			});	   
				if (mensaje) {
					alertify.success(mensaje);
				}   
    		},
    		complete : function(){
				function quitarScript(){
					window.location.href = "quitar.php";
				}
				setInterval(function(){quitarScript()},5000);
	        }
    	});
    }
     /*funcion agregada*/
     function enviarfinalizacion2(ultimoId,mensaje = null){
<?php $tiempoDesdeInicio = date('Y-m-d H:i:s'); ?>
    	var form = $("#FormularioDatos");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#FormularioDatos")[0]);
         
    	$.ajax({
    		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+ultimoId +'<?php if(isset($_GET['token'])) { echo "&token=123"; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
    		type  : "post",
    		data  : formData,
		 	cache: false,
            contentType: false,
            processData: false,
    		success : function(xt){
    			console.log("IdDeLlamada2:"+$("#idLlamada").val());
    			
    		},
			complete:function(){

				if (mensaje) {
					alertify.success(mensaje);
				}
	        }
    	});
    }
    /*finalizacion de la funcion */

    function enviaralCrud($siNo,$idUltimoDevuelto,$tarea = null){
        
        if (!$tarea) {
        	$tarea = "0";
        }

    	var form = $("#FormularioDatos");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#FormularioDatos")[0]);
        formData.append('creamos', $siNo);
        //linea agregada
        formData.append('idUltimoDevuelto', $idUltimoDevuelto);

        formData.append('tarea', $tarea);




        $.ajax({
           url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType:'html',	
            //una vez finalizado correctamente
            success: function(data){
            	data = data.replace(/[\u200B-\u200D\uFEFF]/g, '');
                data = JSON.parse(data);
                if (data.campana_crm) {
	            	$("#idRegTareas").val($("#idRegTareas").val()+","+data.ultimoId)
					$("#idTareasBack").val($("#idTareasBack").val()+","+data.campana_crm)
                }
                
            	//data = data.replace(/[\u200B-\u200D\uFEFF]/g, '');
                //data = JSON.parse(data);
                if(data.code != '-1' && data.code != '0'){
                	/* Aqui se hace el envio de la info de los detalles*/

                	<?php if(!isset($_GET['campan'])){ ?>
                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                        if($("#oper").val() == 'add'){
                        	
                            idTotal = data;	
                        }else{

                            idTotal= $("#hidId").val();
                        }
                       
                        //Limpiar formulario
                        form[0].reset();
                        after_save();
                        <?php if(isset($_GET['registroId'])){ ?>
                        $.ajax({
                            url      : '<?=$url_crud;?>',
                            type     : 'POST',
                            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
                            dataType : 'json',
                            success  : function(data){
                                //recorrer datos y enviarlos al formulario
                                $.each(data, function(i, item) {
                                	

                                	$("#G1122_C20661").val(item.G1122_C20661);

                                	$("#G1122_CodigoMiembro").val(item.G1122_CodigoMiembro);

                                	$("#G1122_Usuario").val(item.G1122_Usuario);

                                	$("#G1122_C17121").val(item.G1122_C17121);

                                	$("#G1122_C17122").val(item.G1122_C17122);

                                	$("#G1122_C18857").val(item.G1122_C18857);

                                	$("#G1122_C20662").val(item.G1122_C20662);

                                	$("#G1122_C20663").val(item.G1122_C20663);

                                	$("#G1122_C17123").val(item.G1122_C17123);

                                	$("#G1122_C17124").val(item.G1122_C17124);

                                	$("#G1122_C17125").val(item.G1122_C17125);

                                	$("#G1122_C17126").val(item.G1122_C17126);

                                	$("#G1122_C17127").val(item.G1122_C17127);

                                	$("#G1122_C17128").val(item.G1122_C17128);

                                	$("#G1122_C17129").val(item.G1122_C17129);

                                	$("#G1122_C17130").val(item.G1122_C17130);

                                	$("#G1122_C17131").val(item.G1122_C17131);

                                	$("#G1122_C17132").val(item.G1122_C17132);

                                	$("#G1122_C18858").val(item.G1122_C18858);

                                	$("#G1122_C18351").val(item.G1122_C18351);

                                	$("#G1122_C18859").val(item.G1122_C18859);

                                	$("#G1122_C18860").val(item.G1122_C18860);

                                	$("#G1122_C17133").val(item.G1122_C17133);

                                	$("#G1122_C17134").val(item.G1122_C17134);

                                	$("#G1122_C17135").val(item.G1122_C17135);

                                	$("#G1122_C17136").val(item.G1122_C17136);

                                	$("#G1122_C17137").val(item.G1122_C17137);

                                	$("#G1122_C17138").val(item.G1122_C17138);

                                	$("#G1122_C17139").val(item.G1122_C17139);

                                	$("#G1122_C17140").val(item.G1122_C17140);

                                	$("#G1122_C17141").val(item.G1122_C17141);

                                	$("#G1122_C30194").val(item.G1122_C30194);

                                    if(item.G1122_C17142 == 1){
                                       $("#G1122_C17142").attr('checked', true);
                                    } 

                                    if(item.G1122_C17143 == 1){
                                       $("#G1122_C17143").attr('checked', true);
                                    } 

                                	$("#G1122_C17144").val(item.G1122_C17144);

                                	$("#G1122_C17145").val(item.G1122_C17145);

                                	$("#G1122_C17146").val(item.G1122_C17146);

                                	$("#G1122_C17146").val(item.G1122_C17146).trigger("change"); 

                                	$("#G1122_C18721").val(item.G1122_C18721);

                                	$("#G1122_C18720").val(item.G1122_C18720);

                                	$("#G1122_C17147").val(item.G1122_C17147);

                                	$("#G1122_C17149").val(item.G1122_C17149);

                                	$("#G1122_C17150").val(item.G1122_C17150);

                                	$("#G1122_C17151").val(item.G1122_C17151);

                                	$("#G1122_C17152").val(item.G1122_C17152);
          							$("#h3mio").html(item.principal);
                                });

                                //Deshabilitar los campos

                                //Habilitar todos los campos para edicion
                                $('#FormularioDatos :input').each(function(){
                                    $(this).attr('disabled', true);
                                });

                                //Habilidar los botones de operacion, add, editar, eliminar
                                $("#add").attr('disabled', false);
                                $("#edit").attr('disabled', false);
                                $("#delete").attr('disabled', false);

                                //Desahabiliatra los botones de salvar y seleccionar_registro
                                $("#cancel").attr('disabled', true);
                                $("#Save").attr('disabled', true);
                            } 
                        })
                        $("#hidId").val(<?php echo $_GET['registroId'];?>);
                        <?php } else { ?>
                            llenar_lista_navegacion('');

                        <?php } ?>   

                    <?php }else{ 
                    	if(!isset($_GET['formulario'])){
                    ?>
                    	if(data.code != '1'){
                    		$("#idUltimoGuardado").val(data.idFinalizacion);
							$("#iddelatablabackoficee").val(data.ultimoId);
				

                    	}

						//if cambiado de poscion
                    	if(data.code == '1'){
                    		// si es 1 Solo llama al finalizar tareas
                    		 enviarfinalizacion(data.ultimoId,data.mensaje);
                    	}else if(data.code == '2'){
                    		$("#TareaG1133").show();
                    		$("#gestion").val('formularios/G1133/G1133_CRUD.php');
                    		$("#EditarDatosTarea").modal();
                    	}else if(data.code == '3'){
							$("#TareaG1134").show();
							$("#gestion").val('formularios/G1134/G1134_CRUD.php');
							$("#EditarDatosTarea").modal();
                    	}else if(data.code == '4'){
                    		$("#TareaG1136").show();
                    		$("#gestion").val('formularios/G1136/G1136_CRUD.php');
                    		$("#EditarDatosTarea").modal();
                    	}else if(data.code == '5'){
							$("#TareaG1139").show();
							$("#gestion").val('formularios/G1139/G1139_CRUD.php');
							$("#EditarDatosTarea").modal();
                    	}else if(data.code == '6'){
                    		$("#TareaG1135").show();
                    		$("#gestion").val('formularios/G1135/G1135_CRUD.php');
                    		$("#EditarDatosTarea").modal();
                    	}else if(data.code == '7'){
                    		$("#TareaG1140").show();
                    		$("#gestion").val('formularios/G1140/G1140_CRUD.php');
                    		$("#EditarDatosTarea").modal();
                    	}else if(data.code == '8'){
                    		$("#TareaG1138").show();
                    		$("#gestion").val('formularios/G1138/G1138_CRUD.php');
                    		$("#EditarDatosTarea").modal();
                    	}else if(data.code == '10'){
                    		$("#TareaG1137").show();
                    		$("#gestion").val('formularios/G1137/G1137_CRUD.php');
                    		$("#EditarDatosTarea").modal();
                    	}else if(data.code == '9'){//if agregado
                    		// si es 1 Solo llama al finalizar tareas
                    		//linea agregada
               
                    		//$.when(enviarfinalizacion2(data.ultimoId)).then(enviaralCrud(1,data.ultimoId));
             			  $('#idUltimo').val(data.ultimoId);
                            enviarfinalizacion2(data.ultimoId,data.mensaje); 		 
                    	}else{
                    		console.log('no ha entrado a ningun lado');
                    	}
                    	
                    	
	
                    <?php } 
                    	}
                    ?>            
                }else{
                    //Algo paso, hay un error
                    console.log('paso un error');
                    $("#Save").attr('disabled', false);
                    alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                }                
            },
            //si ha ocurrido un error
            error: function(){
                after_save_error();
                $("#Save").attr('disabled', false);
                alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
            }
        });
    }



    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto Comercial ','Fecha de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que departamento realiza la administración de los inmuebles?','Cupo autorizado','¿Cuántos inmuebles USADOS tiene en su inventario?','¿Cuántos inmuebles NUEVOS tiene en su inventario?','ESTADO','TIPIFICACION 1','TIPIFICACION 2','TIPIFICACION 3'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G1122_C20661', 
	                    index: 'G1122_C20661', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_CodigoMiembro', 
	                    index: 'G1122_CodigoMiembro', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_Usuario', 
	                    index: 'G1122_Usuario', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,	                
	                { 
	                    name:'G1122_C17121', 
	                    index: 'G1122_C17121', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17122', 
	                    index: 'G1122_C17122', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C18857', 
	                    index: 'G1122_C18857', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C20662', 
	                    index: 'G1122_C20662', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1122_C20663', 
	                    index:'G1122_C20663', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17123', 
	                    index: 'G1122_C17123', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17124', 
	                    index: 'G1122_C17124', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17125', 
	                    index: 'G1122_C17125', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17126', 
	                    index: 'G1122_C17126', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17127', 
	                    index: 'G1122_C17127', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17128', 
	                    index: 'G1122_C17128', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17129', 
	                    index: 'G1122_C17129', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17130', 
	                    index: 'G1122_C17130', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17131', 
	                    index: 'G1122_C17131', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17132', 
	                    index: 'G1122_C17132', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1122_C18858', 
	                    index:'G1122_C18858', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1122_C18351', 
	                    index:'G1122_C18351', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=895&campo=G1122_C18351'
	                    }
	                }

	                ,
	                {  
	                    name:'G1122_C18859', 
	                    index:'G1122_C18859', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1122_C18860', 
	                    index:'G1122_C18860', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17138', 
	                    index: 'G1122_C17138', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17139', 
	                    index: 'G1122_C17139', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17140', 
	                    index: 'G1122_C17140', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17141', 
	                    index: 'G1122_C17141', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17144', 
	                    index:'G1122_C17144', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1122_C17144'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17145', 
	                    index:'G1122_C17145', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1122_C17145'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17146', 
	                    index:'G1122_C17146', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1122_C17146=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C18721', 
	                    index: 'G1122_C18721', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1122_C18720', 
	                    index:'G1122_C18720', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1122_C17147', 
	                    index:'G1122_C17147', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1122_C17149', 
	                    index:'G1122_C17149', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=828&campo=G1122_C17149'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17150', 
	                    index:'G1122_C17150', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=829&campo=G1122_C17150'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17151', 
	                    index:'G1122_C17151', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=830&campo=G1122_C17151'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17152', 
	                    index:'G1122_C17152', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=831&campo=G1122_C17152'
	                    }
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1122_C17146").change(function(){
                        var valores = $("#"+ rowid +"_G1122_C17146 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1122_C17146 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1122_C17121',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
	            ,subGrid: true,
	            subGridRowExpanded: function(subgrid_id, row_id) { 
	                // we pass two parameters 
	                // subgrid_id is a id of the div tag created whitin a table data 
	                // the id of this elemenet is a combination of the "sg_" + id of the row 
	                // the row_id is the id of the row 
	                // If we wan to pass additinal parameters to the url we can use 
	                // a method getRowData(row_id) - which returns associative array in type name-value 
	                // here we can easy construct the flowing 
	                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Campo1','Campo2', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1226_C20657', 
                            index: 'G1226_C20657', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1226_C20658', 
                            index: 'G1226_C20658', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id);

                        $("#G1122_C20661").val(item.G1122_C20661);

                        $("#G1122_CodigoMiembro").val(item.G1122_CodigoMiembro);

                        $("#G1122_Usuario").val(item.G1122_Usuario);                

                        $("#G1122_C17121").val(item.G1122_C17121);

                        $("#G1122_C17122").val(item.G1122_C17122);

                        $("#G1122_C18857").val(item.G1122_C18857);

                        $("#G1122_C20662").val(item.G1122_C20662);

                        $("#G1122_C20663").val(item.G1122_C20663);

                        $("#G1122_C17123").val(item.G1122_C17123);

                        $("#G1122_C17124").val(item.G1122_C17124);

                        $("#G1122_C17125").val(item.G1122_C17125);

                        $("#G1122_C17126").val(item.G1122_C17126);

                        $("#G1122_C17127").val(item.G1122_C17127);

                        $("#G1122_C17128").val(item.G1122_C17128);

                        $("#G1122_C17129").val(item.G1122_C17129);

                        $("#G1122_C17130").val(item.G1122_C17130);

                        $("#G1122_C17131").val(item.G1122_C17131);

                        $("#G1122_C17132").val(item.G1122_C17132);

                        $("#G1122_C18858").val(item.G1122_C18858);

                        $("#G1122_C18351").val(item.G1122_C18351);
 
        	            $("#G1122_C18351").val(item.G1122_C18351).trigger("change"); 

                        $("#G1122_C18859").val(item.G1122_C18859);

                        $("#G1122_C18860").val(item.G1122_C18860);

                        $("#G1122_C17133").val(item.G1122_C17133);
 
        	            $("#G1122_C17133").val(item.G1122_C17133).trigger("change"); 

                        $("#G1122_C17134").val(item.G1122_C17134);
 
        	            $("#G1122_C17134").val(item.G1122_C17134).trigger("change"); 

                        $("#G1122_C17135").val(item.G1122_C17135);

                        $("#G1122_C17136").val(item.G1122_C17136);

                        $("#G1122_C17137").val(item.G1122_C17137);

                        $("#G1122_C17138").val(item.G1122_C17138);

                        $("#G1122_C17139").val(item.G1122_C17139);

                        $("#G1122_C17140").val(item.G1122_C17140);

                        $("#G1122_C17141").val(item.G1122_C17141);
    
                        if(item.G1122_C17142 == 1){
                           $("#G1122_C17142").attr('checked', true);
                        } 
    
                        if(item.G1122_C17143 == 1){
                           $("#G1122_C17143").attr('checked', true);
                        } 

                        $("#G1122_C17144").val(item.G1122_C17144);
 
        	            $("#G1122_C17144").val(item.G1122_C17144).trigger("change"); 

                        $("#G1122_C17145").val(item.G1122_C17145);
 
        	            $("#G1122_C17145").val(item.G1122_C17145).trigger("change"); 

                        $("#G1122_C17146").val(item.G1122_C17146);
 
        	            $("#G1122_C17146").val(item.G1122_C17146).trigger("change"); 

                        $("#G1122_C18721").val(item.G1122_C18721);

                        $("#G1122_C18720").val(item.G1122_C18720);

                        $("#G1122_C17147").val(item.G1122_C17147);

                        $("#G1122_C17149").val(item.G1122_C17149);
 
        	            $("#G1122_C17149").val(item.G1122_C17149).trigger("change"); 

                        $("#G1122_C17150").val(item.G1122_C17150);
 
        	            $("#G1122_C17150").val(item.G1122_C17150).trigger("change"); 

                        $("#G1122_C17151").val(item.G1122_C17151);
 
        	            $("#G1122_C17151").val(item.G1122_C17151).trigger("change"); 

                        $("#G1122_C17152").val(item.G1122_C17152);

                        $("#G1122_C30194").val(item.G1122_C30194);
 
        	            $("#G1122_C17152").val(item.G1122_C17152).trigger("change"); 
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ESTADO Y TIPIFICACION','TIPO REINTENTO', 'AGENTE', 'FECHA GESTION', 'OBSERVACIÓN', 'FECHA AGENDA', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
                ,
                { 
                    name:'G1122_C17133', 
                    index: 'G1122_C17133', 
                    width:200, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                },
                { 
                    name:'G1122_C17138', 
                    index: 'G1122_C17138', 
                    width:90, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                },
                { 
                    name:'G1122_C17134', 
                    index: 'G1122_C17134', 
                    width:90, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                },
                { 
                    name:'G1122_C17139', 
                    index: 'G1122_C17139', 
                    width:90, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                ,
                { 
                    name:'G1122_C17137', 
                    index: 'G1122_C17137', 
                    width:100, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                },
                { 
                    name:'G1122_C17136', 
                    index: 'G1122_C17136', 
                    width:80, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 50,
            pager: "#pagerDetalles0",
            rowList: [50,100],
            sortable: true,
            sortname: 'G1122_C17139',
            sortorder: 'desc',
            viewrecords: true,
            caption: 'HISTÓRICO ',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            
        });
        $('#tablaDatosDetalless0').navGrid("#pagerDetalles0", { add:false, del: false , edit: false });
        $('#tablaDatosDetalless0').inlineNav('#pagerDetalles0',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: false, 
            add: false, 
            del: false, 
            cancel: false,
            editParams: {
                keys: false,
            },
            addParams: {
                keys: false
            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }	

    function cargarHijos_1(id_1){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless1").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+id_1,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id', 'OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN', 'FECHA TAREA' , 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                },
                { 
                    name:'G1137_C17564', 
                    index: 'G1137_C17564', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                }
                ,
                { 
                    name:'G1137_C17559', 
                    index: 'G1137_C17559', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                }
                ,
                { 
                    name:'G1137_FechaInsercion', 
                    index: 'G1137_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_1); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles1",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1137_C20679',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'ENVIAR TUTORIALES',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_1=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=0&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless1").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_2(id_2){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless2").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+id_2,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id', 'PACK', 'CUPO', 'TIEMPO', 'PRECIO FULL SIN IVA', 'DESCUENTO', 'VALOR CON DESCUENTO SIN IVA', 'VIGENCIA', 'OBSEQUIO', 'OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN', 'FECHA TAREA' , 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                },

                {
                    name:'G1133_C18554', 
                    index: 'G1133_C18554', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },
               
                {
                    name:'G1133_C27095', 
                    index: 'G1133_C27095', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18555', 
                    index: 'G1133_C18555', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18556', 
                    index: 'G1133_C18556', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18557', 
                    index: 'G1133_C18557', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18558', 
                    index: 'G1133_C18558', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18559', 
                    index: 'G1133_C18559', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18560', 
                    index: 'G1133_C18560', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1133_C18561', 
                    index: 'G1133_C18561', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 
                },

                {
                    name:'G1133_C17450', 
                    index: 'G1133_C17450', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 
                },

                { 
                    name:'G1133_FechaInsercion', 
                    index: 'G1133_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },

                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_2); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles2",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1133_C18554',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'ENVIAR PROPUESTA',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_2=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=0&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless2").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_3(id_3){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless3").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_3=si&id='+id_3,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id', 'PACK', 'CUPO', 'TIEMPO', 'PRECIO FULL SIN IVA', 'DESCUENTO', 'VALOR CON DESCUENTO SIN IVA', 'VIGENCIA', 'OBSEQUIO', 'OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN', 'FECHA TAREA' , 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                },

                {
                    name:'G1134_C18344', 
                    index: 'G1134_C18344', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },
               
                {
                    name:'G1134_C17481', 
                    index: 'G1134_C17481', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C18345', 
                    index: 'G1134_C18345', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C18346', 
                    index: 'G1134_C18346', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C18347', 
                    index: 'G1134_C18347', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C18348', 
                    index: 'G1134_C18348', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C18349', 
                    index: 'G1134_C18349', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C18350', 
                    index: 'G1134_C18350', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 

                },

                {
                    name:'G1134_C17482', 
                    index: 'G1134_C17482', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 
                },
                
                {
                    name:'G1134_C17475', 
                    index: 'G1134_C17475', 
                    resizable:true, 
                    sortable:true , 
                    editable: true 
                }
                ,
                { 
                    name:'G1134_FechaInsercion', 
                    index: 'G1134_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_2); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles3",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1134_C18344',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'ENVIAR LINK',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_3=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=0&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless3").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_4(id_4){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless4").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_4=si&id='+id_4,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','TIPO DE CLIENTE','FECHA LECTURA DEL CONTRATO','HORA','EXTENSIÓN','PACK ADQUIRIDO','CUPO','TIEMPO','DESCUENTO APLICADO','FECHA DE ACTIVACIÓN','FECHA FIN','OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN', 'FECHA TAREA' ,'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                {  
                    name:'G1135_C18645', 
                    index:'G1135_C18645', 
                    width:120 ,
                    editable: true
                }

                ,
                {  
                    name:'G1135_C17507', 
                    index:'G1135_C17507', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1135_C17508', 
                    index:'G1135_C17508', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                { 
                    name:'G1135_C17509', 
                    index: 'G1135_C17509', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C18646', 
                    index: 'G1135_C18646', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C18647', 
                    index:'G1135_C18647', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1135_C18648', 
                    index: 'G1135_C18648', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C18649', 
                    index: 'G1135_C18649', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C18650', 
                    index:'G1135_C18650', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1135_C18651', 
                    index:'G1135_C18651', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {
                    name:'G1135_C17512', 
                    index:'G1135_C17512', 
                    width:150, 
                    editable: true 
                }
                ,

                {
                    name:'G1135_C17501', 
                    index:'G1135_C17501', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name:'G1135_FechaInsercion', 
                    index: 'G1135_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_4); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles4",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1135_C20667',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Crear orden',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_4=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1135&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20667&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless4").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_5(id_5){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless5").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_5=si&id='+id_5,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','CUPO USADOS','USADOS','CUPO NUEVOS','NUEVOS','OBSERVACIÓN','CATEGORIA', 'OBSERVACIÓN TIPIFICACIÓN' , 'FECHA TAREA' ,'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                {  
                    name:'G1136_C17537', 
                    index:'G1136_C17537', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                , 
                { 
                    name:'G1136_C17536', 
                    index:'G1136_C17536', 
                    width:70 ,
                    editable: true, 
                    edittype:"checkbox",
                    editoptions: {
                        value:"1:0"
                    } 
                }

                ,
                {  
                    name:'G1136_C17539', 
                    index:'G1136_C17539', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                , 
                { 
                    name:'G1136_C17538', 
                    index:'G1136_C17538', 
                    width:70 ,
                    editable: true, 
                    edittype:"checkbox",
                    editoptions: {
                        value:"1:0"
                    } 
                }

                ,
                {
                    name:'G1136_C17540', 
                    index:'G1136_C17540', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1136_C18644', 
                    index:'G1136_C18644', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=911&campo=G1136_C18644'
                    }
                }

                ,
                {
                    name:'G1136_C17531', 
                    index:'G1136_C17531', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name:'G1136_FechaInsercion', 
                    index: 'G1136_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_5); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles5",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1136_C20664',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Cliente a escalar',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_5=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1136&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20664&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless5").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_6(id_6){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless6").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_6=si&id='+id_6,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN', 'FECHA TAREA', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                {
                    name:'G1138_C17588', 
                    index:'G1138_C17588', 
                    width:150, 
                    editable: true 
                }

                ,
                {
                    name:'G1138_C17583', 
                    index:'G1138_C17583', 
                    width:150, 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_FechaInsercion', 
                    index: 'G1138_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_6); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles6",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1138_C20673',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Enviar presentación',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_6=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1138&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20673&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless6").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_7(id_7){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless7").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_7=si&id='+id_7,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','CAUSA','OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN', 'FECHA TAREA' ,'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,

                {  
                    name:'G1139_C17612', 
                    index:'G1139_C17612', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=861&campo=G1139_C17612'
                    }
                }

                ,
                {
                    name:'G1139_C17613', 
                    index:'G1139_C17613', 
                    width:150, 
                    editable: true 
                }

                ,
                {
                    name:'G1139_C17607', 
                    index:'G1139_C17607', 
                    width:150, 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_FechaInsercion', 
                    index: 'G1139_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_7); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles7",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1139_C20682',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Sacar de lista negra',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_7=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1139&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20682&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless7").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_8(id_8){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless8").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_8=si&id='+id_8,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','TIPO DE SOLICITUD','OBSERVACIÓN', 'OBSERVACIÓN TIPIFICACIÓN','FECHA TAREA' , 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
             
                {  
                    name:'G1140_C17637', 
                    index:'G1140_C17637', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=864&campo=G1140_C17637'
                    }
                }

                ,
                {
                    name:'G1140_C17638', 
                    index:'G1140_C17638', 
                    width:150, 
                    editable: true 
                }
                ,
                {
                    name:'G1140_C17632', 
                    index:'G1140_C17632', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name:'G1140_FechaInsercion', 
                    index: 'G1140_FechaInsercion', 
                    resizable:false, 
                    sortable:true , 
                    editable: true 

                },
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_8); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles8",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1140_C20685',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Solicitud especial',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_8=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                //$("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1140&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20685&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                //$("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless8").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){

        
		var id_0 = '<?php if(isset($_GET['user'])){ echo $_GET['user']; }else{ echo "0"; }?>';
			
		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
		cargarHijos_0(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
		cargarHijos_1(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
		cargarHijos_2(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
		cargarHijos_3(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion Recargar 
		cargarHijos_4(id_0);	
    	
    	$.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion Recargar 
		cargarHijos_5(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion Recargar 
		cargarHijos_6(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion Recargar 
		cargarHijos_7(id_0);

		$.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion Recargar 
		cargarHijos_8(id_0);	

    }	
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#G1122_Usuario').val('<?php echo getIdentificacionUser($token);?>');		
    	<?php if(isset($_GET['user'])){ ?>
        	
    		vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user']; ?>');
        <?php } ?>

		$('#G1122_CodigoMiembro').val('<?php echo $_GET['user']?>');		
    	<?php if(isset($_GET['user'])){ ?>
        	
    		vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user']; ?>');
        <?php } ?>

        <?php if(isset($_GET['nuevoregistro'])){  ?>
        	$('#FormularioDatos :input').each(function(){
                $(this).attr('readonly', false);
            });
            $("#G1122_C20661").attr('readonly', true);
            $("#G1122_C18721").attr('readonly', true);
    	<?php } ?>
		
	});
</script>
