<?php
	<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');

    //Este archivo es para agregar funcionalidades al G, y que al momento de generar de nuevo no se pierdan

    //Cosas como nuevas consultas, nuevos Inserts, Envio de correos, etc, en fin extender mas los formularios en PHP

    //Inserciones o actualizaciones
    if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
        $Lsql  = '';

        $validar = 0;
        $LsqlU = "UPDATE ".$BaseDatos.".G1122 SET "; 
        $LsqlI = "INSERT INTO ".$BaseDatos.".G1122(";
        $LsqlV = " VALUES ("; 

        $G1122_C20661 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1122_C20661"])){
            if($_POST["G1122_C20661"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $G1122_C20661 = $_POST["G1122_C20661"];
                $LsqlU .= $separador." G1122_C20661 = ".$G1122_C20661."";
                $LsqlI .= $separador." G1122_C20661";
                $LsqlV .= $separador.$G1122_C20661;
                $validar = 1;
            }
        }

        if(isset($_POST["G1122_C17121"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17121 = '".$_POST["G1122_C17121"]."'";
            $LsqlI .= $separador."G1122_C17121";
            $LsqlV .= $separador."'".$_POST["G1122_C17121"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17122"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17122 = '".$_POST["G1122_C17122"]."'";
            $LsqlI .= $separador."G1122_C17122";
            $LsqlV .= $separador."'".$_POST["G1122_C17122"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C18857"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C18857 = '".$_POST["G1122_C18857"]."'";
            $LsqlI .= $separador."G1122_C18857";
            $LsqlV .= $separador."'".$_POST["G1122_C18857"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C20662"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C20662 = '".$_POST["G1122_C20662"]."'";
            $LsqlI .= $separador."G1122_C20662";
            $LsqlV .= $separador."'".$_POST["G1122_C20662"]."'";
            $validar = 1;
        }
         

        $G1122_C20663 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1122_C20663"])){    
            if($_POST["G1122_C20663"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $tieneHora = explode(' ' , $_POST["G1122_C20663"]);
                if(count($tieneHora) > 1){
                    $G1122_C20663 = "'".$_POST["G1122_C20663"]."'";
                }else{
                    $G1122_C20663 = "'".str_replace(' ', '',$_POST["G1122_C20663"])." 00:00:00'";
                }


                $LsqlU .= $separador." G1122_C20663 = ".$G1122_C20663;
                $LsqlI .= $separador." G1122_C20663";
                $LsqlV .= $separador.$G1122_C20663;
                $validar = 1;
            }
        }

        if(isset($_POST["G1122_C17123"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17123 = '".$_POST["G1122_C17123"]."'";
            $LsqlI .= $separador."G1122_C17123";
            $LsqlV .= $separador."'".$_POST["G1122_C17123"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17124"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17124 = '".$_POST["G1122_C17124"]."'";
            $LsqlI .= $separador."G1122_C17124";
            $LsqlV .= $separador."'".$_POST["G1122_C17124"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17125"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17125 = '".$_POST["G1122_C17125"]."'";
            $LsqlI .= $separador."G1122_C17125";
            $LsqlV .= $separador."'".$_POST["G1122_C17125"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17126"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17126 = '".$_POST["G1122_C17126"]."'";
            $LsqlI .= $separador."G1122_C17126";
            $LsqlV .= $separador."'".$_POST["G1122_C17126"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17127"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17127 = '".$_POST["G1122_C17127"]."'";
            $LsqlI .= $separador."G1122_C17127";
            $LsqlV .= $separador."'".$_POST["G1122_C17127"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17128"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17128 = '".$_POST["G1122_C17128"]."'";
            $LsqlI .= $separador."G1122_C17128";
            $LsqlV .= $separador."'".$_POST["G1122_C17128"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17129"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17129 = '".$_POST["G1122_C17129"]."'";
            $LsqlI .= $separador."G1122_C17129";
            $LsqlV .= $separador."'".$_POST["G1122_C17129"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17130"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17130 = '".$_POST["G1122_C17130"]."'";
            $LsqlI .= $separador."G1122_C17130";
            $LsqlV .= $separador."'".$_POST["G1122_C17130"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17131"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17131 = '".$_POST["G1122_C17131"]."'";
            $LsqlI .= $separador."G1122_C17131";
            $LsqlV .= $separador."'".$_POST["G1122_C17131"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17132"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17132 = '".$_POST["G1122_C17132"]."'";
            $LsqlI .= $separador."G1122_C17132";
            $LsqlV .= $separador."'".$_POST["G1122_C17132"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C18351"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C18351 = '".$_POST["G1122_C18351"]."'";
            $LsqlI .= $separador."G1122_C18351";
            $LsqlV .= $separador."'".$_POST["G1122_C18351"]."'";
            $validar = 1;
        }
         

        $G1122_C18859 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1122_C18859"])){    
            if($_POST["G1122_C18859"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $tieneHora = explode(' ' , $_POST["G1122_C18859"]);
                if(count($tieneHora) > 1){
                    $G1122_C18859 = "'".$_POST["G1122_C18859"]."'";
                }else{
                    $G1122_C18859 = "'".str_replace(' ', '',$_POST["G1122_C18859"])." 00:00:00'";
                }


                $LsqlU .= $separador." G1122_C18859 = ".$G1122_C18859;
                $LsqlI .= $separador." G1122_C18859";
                $LsqlV .= $separador.$G1122_C18859;
                $validar = 1;
            }
        }

        $G1122_C18860 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1122_C18860"])){    
            if($_POST["G1122_C18860"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $tieneHora = explode(' ' , $_POST["G1122_C18860"]);
                if(count($tieneHora) > 1){
                    $G1122_C18860 = "'".$_POST["G1122_C18860"]."'";
                }else{
                    $G1122_C18860 = "'".str_replace(' ', '',$_POST["G1122_C18860"])." 00:00:00'";
                }


                $LsqlU .= $separador." G1122_C18860 = ".$G1122_C18860;
                $LsqlI .= $separador." G1122_C18860";
                $LsqlV .= $separador.$G1122_C18860;
                $validar = 1;
            }
        }

        $G1122_C17133 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["tipificacion"])){    
            if($_POST["tipificacion"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C17133 = str_replace(' ', '',$_POST["tipificacion"]);
                $LsqlU .= $separador." G1122_C17133 = ".$G1122_C17133;
                $LsqlI .= $separador." G1122_C17133";
                $LsqlV .= $separador.$G1122_C17133;
                $validar = 1;

                
            }
        }

        $G1122_C17134 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["reintento"])){    
            if($_POST["reintento"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C17134 = str_replace(' ', '',$_POST["reintento"]);
                $LsqlU .= $separador." G1122_C17134 = ".$G1122_C17134;
                $LsqlI .= $separador." G1122_C17134";
                $LsqlV .= $separador.$G1122_C17134;
                $validar = 1;
            }
        }

        $G1122_C17135 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["TxtFechaReintento"])){    
            if($_POST["TxtFechaReintento"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C17135 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                $LsqlU .= $separador." G1122_C17135 = ".$G1122_C17135;
                $LsqlI .= $separador." G1122_C17135";
                $LsqlV .= $separador.$G1122_C17135;
                $validar = 1;
            }
        }

        $G1122_C17136 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["TxtHoraReintento"])){    
            if($_POST["TxtHoraReintento"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C17136 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                $LsqlU .= $separador." G1122_C17136 = ".$G1122_C17136;
                $LsqlI .= $separador." G1122_C17136";
                $LsqlV .= $separador.$G1122_C17136;
                $validar = 1;
            }
        }

        $G1122_C17137 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["textAreaComentarios"])){    
            if($_POST["textAreaComentarios"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1122_C17137 = "'".$_POST["textAreaComentarios"]."'";
                $LsqlU .= $separador." G1122_C17137 = ".$G1122_C17137;
                $LsqlI .= $separador." G1122_C17137";
                $LsqlV .= $separador.$G1122_C17137;
                $validar = 1;
            }
        }

            /*$separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17138 = '".getNombreUser($_GET['token'])."'";
            $LsqlI .= $separador."G1122_C17138";
            $LsqlV .= $separador."'".getNombreUser($_GET['token'])."'";
            $validar = 1;*/
        
         

        $G1122_C17139 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        $separador = "";
        if($validar == 1){
            $separador = ",";
        }
        $G1122_C17139 = "'".date('Y-m-d H:i:s')."'";
        $LsqlU .= $separador." G1122_C17139 = ".$G1122_C17139;
        $LsqlI .= $separador." G1122_C17139";
        $LsqlV .= $separador.$G1122_C17139;
        $validar = 1;
       

        $G1122_C17140 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        $separador = "";
        if($validar == 1){
            $separador = ",";
        }
        $G1122_C17140 = "'".date('Y-m-d H:i:s')."'";
        $LsqlU .= $separador." G1122_C17140 = ".$G1122_C17140;
        $LsqlI .= $separador." G1122_C17140";
        $LsqlV .= $separador.$G1122_C17140;
        $validar = 1;
       

           /* $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
            $resCampa = $mysqli->query($cmapa);
            $dataCampa = $resCampa->fetch_array();
            $LsqlU .= $separador."G1122_C17141 = '".$dataCampa["CAMPAN_Nombre____b"]."'";
            $LsqlI .= $separador."G1122_C17141";
            $LsqlV .= $separador."'".$dataCampa["CAMPAN_Nombre____b"]."'";
            $validar = 1;*/
        
         

        if(isset($_POST["G1122_C17144"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17144 = '".$_POST["G1122_C17144"]."'";
            $LsqlI .= $separador."G1122_C17144";
            $LsqlV .= $separador."'".$_POST["G1122_C17144"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17145"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17145 = '".$_POST["G1122_C17145"]."'";
            $LsqlI .= $separador."G1122_C17145";
            $LsqlV .= $separador."'".$_POST["G1122_C17145"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17146"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17146 = '".$_POST["G1122_C17146"]."'";
            $LsqlI .= $separador."G1122_C17146";
            $LsqlV .= $separador."'".$_POST["G1122_C17146"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C18721"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C18721 = '".$_POST["G1122_C18721"]."'";
            $LsqlI .= $separador."G1122_C18721";
            $LsqlV .= $separador."'".$_POST["G1122_C18721"]."'";
            $validar = 1;
        }
         

        $G1122_C18720 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1122_C18720"])){
            if($_POST["G1122_C18720"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $G1122_C18720 = $_POST["G1122_C18720"];
                $LsqlU .= $separador." G1122_C18720 = ".$G1122_C18720."";
                $LsqlI .= $separador." G1122_C18720";
                $LsqlV .= $separador.$G1122_C18720;
                $validar = 1;
            }
        }

        $G1122_C17147 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1122_C17147"])){
            if($_POST["G1122_C17147"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $G1122_C17147 = $_POST["G1122_C17147"];
                $LsqlU .= $separador." G1122_C17147 = ".$G1122_C17147."";
                $LsqlI .= $separador." G1122_C17147";
                $LsqlV .= $separador.$G1122_C17147;
                $validar = 1;
            }
        }

        if(isset($_POST["G1122_C17149"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17149 = '".$_POST["G1122_C17149"]."'";
            $LsqlI .= $separador."G1122_C17149";
            $LsqlV .= $separador."'".$_POST["G1122_C17149"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17150"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17150 = '".$_POST["G1122_C17150"]."'";
            $LsqlI .= $separador."G1122_C17150";
            $LsqlV .= $separador."'".$_POST["G1122_C17150"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17151"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17151 = '".$_POST["G1122_C17151"]."'";
            $LsqlI .= $separador."G1122_C17151";
            $LsqlV .= $separador."'".$_POST["G1122_C17151"]."'";
            $validar = 1;
        }
         

        if(isset($_POST["G1122_C17152"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_C17152 = '".$_POST["G1122_C17152"]."'";
            $LsqlI .= $separador."G1122_C17152";
            $LsqlV .= $separador."'".$_POST["G1122_C17152"]."'";
            $validar = 1;
        }
         

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $LsqlU .= $separador."G1122_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $LsqlI .= $separador."G1122_IdLlamada";
            $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1122_C";
                $valorH = $valorG.$campo;
                $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $LsqlI .= $separador." ".$valorH;
                $LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }
        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                $LsqlI .= ", G1122_Usuario , G1122_FechaInsercion, G1122_CodigoMiembro";
                $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                $Lsql = $LsqlI.")" . $LsqlV.")";
            }else if($_POST["oper"] == 'edit' ){
                $Lsql = $LsqlU." WHERE G1122_ConsInte__b =".$_POST["id"]; 
            }else if($_POST["oper"] == 'del' ){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1122 WHERE G1122_ConsInte__b = ".$_POST['id'];
                $validar = 1;
            }
        }
        //si trae algo que insertar inserta

        //echo $Lsql;
        if($validar == 1){
            if ($mysqli->query($Lsql) === TRUE) {
                if($_POST["oper"] == 'add' ){
                    
                    $ultimoId =  $mysqli->insert_id;
                   // echo "empezar => ". $_POST['G1122_C17151'];
                    $tipificacion2 = $_POST['G1122_C17151'];
                    switch ($tipificacion2) {
                        case '14047':
                            //Enviar Propuesta
                            llenar_tareas_finca_raiz(565 , $_GET['CodigoMiembro'], 2);
                            break;

                        case '14082':
                            //Enviar Link de pago
                            llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3);
                            break;

                        case '14059':
                            //Tiene un cupo superior
                            if($_POST['G1122_C17152'] == '14112'){
                                //Cliente a escalar
                                llenar_tareas_finca_raiz(568, $_GET['CodigoMiembro'], 4);
                            }else{
                                $datos = array();
                                $datos['code'] = $respuesta;
                                $datos['ultimoId'] = $ultimoId;
                                header('Content-type: application/json; charset=utf-8');
                                echo json_encode($datos);
                            }
                            
                            break;

                        case '14063':
                            //Solo tiene immuebles nuevos
                            if($_POST['G1122_C17152'] == '14120'){
                                //Cliente a escalar
                                llenar_tareas_finca_raiz(568, $_GET['CodigoMiembro'], 4);
                            }else{
                                $datos = array();
                                $datos['code'] = $respuesta;
                                $datos['ultimoId'] = $ultimoId;
                                header('Content-type: application/json; charset=utf-8');
                                echo json_encode($datos);
                            }

                            break;

                        case '14064':
                            //Otros municipios
                            if($_POST['G1122_C17152'] == '14122'){
                                //Cliente a escalar
                                llenar_tareas_finca_raiz(568, $_GET['CodigoMiembro'], 4);
                            }else{
                                $datos = array();
                                $datos['code'] = $respuesta;
                                $datos['ultimoId'] = $ultimoId;
                                header('Content-type: application/json; charset=utf-8');
                                echo json_encode($datos);
                            }
                            break;

                        case '14048':
                            //se envia Link de pago -- enviar link
                            llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3);

                            break;
                            
                        case '14083':
                            //se le vencio el link
                            if($_POST['G1122_C17152'] == '14157'){
                                //se envia el mismo link -- enviar link
                                llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3);

                            }else if($_POST['G1122_C17152'] == '14158'){
                                // se renueva con nueva oferta -- enviar link
                                llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3);
                            }else{
                                $datos = array();
                                $datos['code'] = $respuesta;
                                $datos['ultimoId'] = $ultimoId;
                                header('Content-type: application/json; charset=utf-8');
                                echo json_encode($datos);
                            }
                            
                            break;

                        case '14089':
                            //cliente en lista negra -- sacar de lista negra
                            if($_POST['G1122_C17152'] == '14164'){
                                //EL cliente agrega eñ contacto - dacar de ista negra
                                llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5);
                            }else if($_POST['G1122_C17152'] == '14165'){
                                // cambo de correo - savar de lista negra
                                llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5);
                            }else if($_POST['G1122_C17152'] == '14166'){
                                //confirma que ya se agrego y no le llegan los correos - sacar de lista negra
                                llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5);
                            }else if($_POST['G1122_C17152'] == '14167'){
                                //bandeja de entrada llena - sacar de lista negra
                                llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5);
                            }else if($_POST['G1122_C17152'] == '14168'){
                                // sproblemas con el servidor - sacar de lista negra
                                llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5);
                            }else{
                                $datos = array();
                                $datos['code'] = $respuesta;
                                $datos['ultimoId'] = $ultimoId;
                                header('Content-type: application/json; charset=utf-8');
                                echo json_encode($datos);
                            }
                            break;

                        case '14086':
                            //activacion de la Ovi . crear orden
                            llenar_tareas_finca_raiz(567, $_GET['CodigoMiembro'], 6);
                            break;

                        default:
                            $datos = array();
                            $datos['code'] = $respuesta;
                            $datos['ultimoId'] = $ultimoId;
                            header('Content-type: application/json; charset=utf-8');
                            echo json_encode($datos);
                            break;
                    }

                    
                }else{
                    echo "1";           
                }

            } else {
                header('Content-type: application/json; charset=utf-8');
                $datos = array();
                $datos['code'] = 0;
                $datos['error'] = $mysqli->error;
                header('Content-type: application/json; charset=utf-8');
                echo json_encode($datos);
               // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }

    function llenar_tareas_finca_raiz($campana_crm, $id_usuario, $respuesta){
        
        header('Content-type: application/json; charset=utf-8');
        include(__DIR__."/../../conexion.php");
        $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana_crm;
       
        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);

        $PasoLsql = "SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__CAMPAN_b ".$campana_crm;
        $resPaso = $mysqli->query($PasoLsql);
        $pasoInicial = 0;
        if($resPaso){
            $estosPasos = $resPaso->fetch_array();
            $pasoInicial = $estosPasos['ESTPAS_ConsInte__b'];
        }

        if($res_Lsql_Campan){
            $datoCampan = $res_Lsql_Campan->fetch_array();
            $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
            $int_Guion_Camp_2 = "G".$datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
            $rea_ConfD_Campan = $datoCampan['CAMPAN_ConfDinam_b'];



            /* ya tenemos la muestra ahora toca insertar esa jugada */
            $LsqPregun  = "SELECT PREGUN_ConsInte__b , PREGUN_ConsInte__OPCION_B FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan." AND PREGUN_Texto_____b = 'ESTADO_TAREA'";
            $resPregun  = $mysqli->query($LsqPregun);
            $idEstado = 0;
            $preguntaEstado = '';
            if($resPregun->num_rows > 0){
                $datoPregun = $resPregun->fetch_array();
                $preguntaEstado = $datoPregun['PREGUN_ConsInte__b'];
                /* Vamos a bscar el Id de sin gestion */
                $LisopcLsql = "SELECT LISOPC_ConsInte__b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$datoPregun['PREGUN_ConsInte__OPCION_B']." AND LISOPC_Nombre____b = 'Sin gestión'" ;


                $resLisop = $mysqli->query($LisopcLsql);
                if($resLisop){
                    $datosLisop = $resLisop->fetch_array();
                    $idEstado = $datosLisop['LISOPC_ConsInte__b'];
                    
                }
            }else{
                $array = array(
                    array('Sin gestión'),
                    array('En gestión'),
                    array('En gestión por devolución'),
                    array('Cerrada'),
                    array('Devuelta')
                );
                $tamanho = 5;
                /*Insertamos el OPCION */
                $insertLsql = "INSERT INTO ".$BaseDatos_systema.".OPCION (OPCION_ConsInte__GUION__b, OPCION_Nombre____b, OPCION_ConsInte__PROYEC_b, OPCION_FechCrea__b, OPCION_UsuaCrea__b) VALUES (".$int_Guion_Campan.", 'ESTADO_TAREA - ".$int_Guion_Campan."', ".$_SESSION['HUESPED'].", '".date('Y-m-d H:s:i')."', ".$_SESSION['IDENTIFICACION'].");";
                if($mysqli->query($insertLsql) === true){
                    /* Se inserto la lista perfectamente */
                    $ultimoLista = $mysqli->insert_id;
                    for ($i=0; $i < $tamanho ; $i++) { 
                        $insertLisopc = "INSERT INTO ".$BaseDatos_systema.".LISOPC (LISOPC_Nombre____b, LISOPC_ConsInte__OPCION_b, LISOPC_Posicion__b) VALUES ('".$array[$i][0]."', ".$ultimoLista.", ".$i.");";
                        if($mysqli->query($insertLisopc) === true){
                     
                        }else{
                            echo $mysqli->error;
                        }
                    }

                    $Lsql_Reintento_campo = "INSERT INTO ".$BaseDatos_systema.".G6(G6_C39, G6_C40, G6_C51, G6_C207, G6_C209, G6_C44) VALUES ('ESTADO_TAREA', 6, 1, ".$int_Guion_Campan.", 0, ".$ultimoLista.");";
                    if($mysqli->query($Lsql_Reintento_campo) === true){
                        $int_Reintento_campo = $mysqli->insert_id;
                        $Lsql_Editar_Guion = "UPDATE ".$BaseDatos_systema.".G5 SET G5_C312 = ".$int_Reintento_campo." WHERE G5_ConsInte__b = ".$int_Guion_Campan;
                        if($mysqli->query($Lsql_Editar_Guion) !== true){
                            echo "error => ".$mysqli->error;
                        }

                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_C".$int_Reintento_campo." int(10) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                }
            }

            $numeroPaso = 0;
            $LsqPregun  = "SELECT PREGUN_ConsInte__b , PREGUN_ConsInte__GUION__PRE_B FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan." AND PREGUN_Texto_____b = 'PASO_ID'";
            $resPregun  = $mysqli->query($LsqPregun);
            if($resPregun->num_rows === 0){
                $Lsql_Paso_campo = "INSERT INTO ".$BaseDatos_systema.".G6(G6_C39, G6_C40, G6_C51, G6_C207, G6_C209) VALUES ('PASO_ID', 3, 1, ".$int_Guion_Campan.",0);";
                if($mysqli->query($Lsql_Paso_campo) === true){
                    $int_Reintento_campo = $mysqli->insert_id;
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_C".$int_Reintento_campo." int(10) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }
            }else{
                $paso = $resPregun->fetch_array();
                $numeroPaso = $paso['PREGUN_ConsInte__b'];
            }


            $LsqPregun  = "SELECT PREGUN_ConsInte__b , PREGUN_ConsInte__GUION__PRE_B FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan." AND PREGUN_Texto_____b = 'REGISTRO_ID'";
            $resPregun  = $mysqli->query($LsqPregun);
            if($resPregun->num_rows === 0){
                $Lsql_Paso_campo = "INSERT INTO ".$BaseDatos_systema.".G6(G6_C39, G6_C40, G6_C51, G6_C207, G6_C209) VALUES ('REGISTRO_ID', 3, 1, ".$int_Guion_Campan.", 0);";
                if($mysqli->query($Lsql_Paso_campo) === true){
                    $int_Reintento_campo = $mysqli->insert_id;
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_C".$int_Reintento_campo." int(10) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }
            }

           
            $Lsql = "SELECT * FROM ".$BaseDatos.".".$int_Guion_Camp_2." WHERE ".$int_Guion_Camp_2."_CodigoMiembro = ".$id_usuario." AND ".$int_Guion_Camp_2."_PoblacionOrigen =".$int_Pobla_Camp_2;
            //echo $Lsql;
            $resLsql = $mysqli->query($Lsql);
            if($resLsql->num_rows === 0){
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana_crm;
                $resultcampSql = $mysqli->query($campSql);
                //$Lsql = 'UPDATE '.$BaseDatos.'.'.$int_Guion_Camp_2.' , '.$BaseDatos.'.'.$str_Pobla_Campan.' SET ';
                $i=0;
                $select = 'SELECT '.$str_Pobla_Campan.'_ConsInte__b';
                $insert = 'INSERT INTO '.$BaseDatos.'.'.$int_Guion_Camp_2.'(G'.$int_Guion_Campan.'_CodigoMiembro';
                while($key = $resultcampSql->fetch_object()){
                    $validoparaedicion = false;
                    $valorScript = $key->CAMINC_NomCamGui_b;

                    $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

                    //echo $LsqlShow;
                    $resultShow = $mysqli->query($LsqlShow);
                    if($resultShow->num_rows === 0){
                        //comentario el campo no existe
                        $validoparaedicion = false;
                    }else{
                        $validoparaedicion = true;
                    } 

                    $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$int_Guion_Camp_2." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
                    //echo $LsqlShow;
                    $resultShow = $mysqli->query($LsqlShow);
                    if($resultShow->num_rows === 0 ){
                        //comentario el campo no existe
                        $validoparaedicion = false;
                    }else{
                        $validoparaedicion = true;
                    } 

                
                    if($validoparaedicion){
                        $select .= ', '.$key->CAMINC_NomCamPob_b;
                        $insert .= ', '.$valorScript;
                    }
                    
                } 
                $select .= ' FROM '.$BaseDatos.'.'.$str_Pobla_Campan.' WHERE '.$str_Pobla_Campan.'_ConsInte__b = '.$id_usuario; 
                $insert .= ') ';
                $Lsql = $insert.$select; 
                //echo "Esta select => ".$select;;
                if($mysqli->query($Lsql) === TRUE ){
                    /* Actualizamos el estado */
                    if($preguntaEstado != ''){

                        $LsqlUpdate = "UPDATE ".$BaseDatos.".G".$int_Guion_Campan." SET ".$int_Guion_Camp_2."_C".$preguntaEstado." = ".$idEstado.", ".$int_Guion_Camp_2."_PoblacionOrigen = ".$int_Pobla_Camp_2." , ".$int_Guion_Camp_2."_FechaInsercion = '".date('Y-m-d H:i:s')."', ".$int_Guion_Camp_2."_C".$numeroPaso." = ".$pasoInicial." WHERE  G".$int_Guion_Campan.'_CodigoMiembro = '.$id_usuario;
                    
                        if($mysqli->query($LsqlUpdate) === true){
                            $datos = array();
                            $datos['code'] = $respuesta;
                            $datos['ultimoId'] = $id_usuario;
                            echo json_encode($datos);
                        }else{
                            $datos = array();
                            $datos['code'] = '-1';
                            $datos['error'] = "Error Actualizando el estado_dy".$mysqli->error;
                        }
                    }
                
                }else{
                    $datos = array();
                    $datos['code'] = '-1';
                    $datos['error'] = "NO SE ACTALIZO LA BASE DE DATOS => ".$mysqli->error;
                    echo json_encode($datos);
                }
            }  else {
                $datos = array();
                $datos['code'] = $respuesta;
                $datos['ultimoId'] = $id_usuario;
                echo json_encode($datos);
            } 
                
            
        }
    }