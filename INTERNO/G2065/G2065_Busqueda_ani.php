 	
<?php
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
?>
<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<div class="row" id="resulados">
	<div class="col-md-12" id="resultadosBusqueda">
		
	</div>
</div>
<div class="row" >
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
	
</div>
<script type="text/javascript">
	$(function(){
		buscar_valores();
	});
    
    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }

    function llamarHijo(datos){
        setTimeout(function(){
           var formDatas = datos;
            var iframe = document.getElementById('frameContenedor'); 
            iframe.contentWindow.postMessage(formDatas, '*');                                 
        },2000);  
    }

	function buscar_valores(){
		$.ajax({
			url     	: 'formularios/generados/PHP_Busqueda_Ani.php?action=GET_DATOS&campana_crm=<?php echo $_GET['id_campana_crm'];?>	',
			type		: 'post',
			dataType	: 'json',
			data		: { <?php if(isset($_GET['consinte'])){ if($_GET['consinte'] != '-1' ){ echo "consinte:". $_GET['consinte']." , ";} } ?><?php if(isset($_GET['dato_adicional_1']) && $_GET['dato_adicional_1'] != ''){ echo " dato_adicional_1 : '". $_GET['dato_adicional_1']."',"; }?><?php if(isset($_GET['dato_adicional_2']) && $_GET['dato_adicional_2'] != '' ){ echo " dato_adicional_2 : '". $_GET['dato_adicional_2']."',"; }?><?php if(isset($_GET['dato_adicional_3']) && $_GET['dato_adicional_3'] != '' ){ echo " dato_adicional_3 : '". $_GET['dato_adicional_3']."',"; }?><?php if(isset($_GET['dato_adicional_4']) && $_GET['dato_adicional_4'] != '' ){ echo "dato_adicional_4: '". $_GET['dato_adicional_4']."',"; }?><?php if(isset($_GET['dato_adicional_5']) && $_GET['dato_adicional_5'] != ''){ echo " dato_adicional_5 : '". $_GET['dato_adicional_5']."',"; }?><?php if (isset($_GET['canal'])) { echo " canal : '".$_GET['canal']."',";}?><?php if (isset($_GET['ani'])) { echo " ani : '".$_GET['ani']."',"; } ?> },
			success 	: function(datosq){
				//alert(datosq[0].cantidad_registros);
				if(datosq[0].cantidad_registros > 1){

                    var iframe = document.getElementById('frameContenedor'); 
                    iframe.contentWindow.postMessage(datosq, '*');                                 
                   
                    $("#resulados").hide();
                    <?php if(isset($_GET['token'])){ ?>
                    $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?>&id_campana_crm=<?php echo $_GET['id_campana_crm'];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET['ani'])){ echo "&ani=".$_GET['ani'];}?>&busqueda_manual_forzada=true&dato_adicional_1=Llamada<?php if(isset($_GET['origen'])) { echo '&origen='.$_GET['origen']; }?><?php if(isset($_GET['id_campana_cbx'])){ echo "&id_campana_cbx=".$_GET['id_campana_cbx'];}?>');
                    <?php } ?>
                    
                    llamarHijo(datosq);
                
				}else if(datosq[0].cantidad_registros == 1){
					var id = datosq[0].registros[0].G2065_ConsInte__b;

            		$("#resulados").hide();
					<?php if(isset($_GET['token'])){ ?>
        			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?>&campana_crm=<?php echo $_GET['id_campana_crm'];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET['ani'])){ echo "&ani=".$_GET['ani'];}?><?php if(isset($_GET['origen'])) { echo '&origen='.$_GET['origen']; }?><?php if(isset($_GET['id_campana_cbx'])){ echo "&id_campana_cbx=".$_GET['id_campana_cbx'];}?>');
            		<?php } ?>
				}else{
                $("#resulados").hide();
                <?php if(isset($_GET['token'])){ ?>
                $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?>&id_campana_crm=<?php echo $_GET['id_campana_crm'];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET['ani'])){ echo "&ani=".$_GET['ani'];}?>&busqueda_manual_forzada=true&dato_adicional_1=Llamada<?php if(isset($_GET['origen'])) { echo '&origen='.$_GET['origen']; }?><?php if(isset($_GET['id_campana_cbx'])){ echo "&id_campana_cbx=".$_GET['id_campana_cbx'];}?>');
                <?php } ?>
				}
			}
		});
	}
</script>
