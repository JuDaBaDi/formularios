<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G2065/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G2065/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G2065/".$archivo);
                        $tamaÃ±o = filesize("/Dyalogo/tmp/G2065/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaÃ±o);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G2065/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2065_ConsInte__b, G2065_FechaInsercion , G2065_Usuario ,  G2065_CodigoMiembro  , G2065_PoblacionOrigen , G2065_EstadoDiligenciamiento ,  G2065_IdLlamada , G2065_C37900 as principal ,G2065_C37899,G2065_C37900,G2065_C37901,G2065_C37902,G2065_C37903,G2065_C37904,G2065_C37905,G2065_C37906,G2065_C37927,G2065_C38105,G2065_C38110,G2065_C39715,G2065_C40060,G2065_C37896,G2065_C37897,G2065_C37898 FROM '.$BaseDatos.'.G2065 WHERE G2065_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2065_C37899'] = $key->G2065_C37899;

                $datos[$i]['G2065_C37900'] = $key->G2065_C37900;

                $datos[$i]['G2065_C37901'] = $key->G2065_C37901;

                $datos[$i]['G2065_C37902'] = $key->G2065_C37902;

                $datos[$i]['G2065_C37903'] = $key->G2065_C37903;

                $datos[$i]['G2065_C37904'] = explode(' ', $key->G2065_C37904)[0];
  
                $hora = '';
                if(!is_null($key->G2065_C37905)){
                    $hora = explode(' ', $key->G2065_C37905)[1];
                }

                $datos[$i]['G2065_C37905'] = $hora;

                $datos[$i]['G2065_C37906'] = $key->G2065_C37906;

                $datos[$i]['G2065_C37927'] = $key->G2065_C37927;

                $datos[$i]['G2065_C38105'] = $key->G2065_C38105;

                $datos[$i]['G2065_C38110'] = $key->G2065_C38110;

                $datos[$i]['G2065_C39715'] = $key->G2065_C39715;

                $datos[$i]['G2065_C40060'] = $key->G2065_C40060;

                $datos[$i]['G2065_C37896'] = $key->G2065_C37896;

                $datos[$i]['G2065_C37897'] = $key->G2065_C37897;

                $datos[$i]['G2065_C37898'] = $key->G2065_C37898;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2065";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2065_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2065_ConsInte__b as id,  G2065_C37899 as camp2 , G2065_C37900 as camp1 
                     FROM ".$BaseDatos.".G2065  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2065_ConsInte__b as id,  G2065_C37899 as camp2 , G2065_C37900 as camp1  
                    FROM ".$BaseDatos.".G2065  JOIN ".$BaseDatos.".G2065_M".$_POST['muestra']." ON G2065_ConsInte__b = G2065_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2065_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2065_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2065_C37899 LIKE '%".$B."%' OR G2065_C37900 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2065_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2065_C38110'])){
                                $Ysql = "SELECT G2077_ConsInte__b as id, G2077_C38109 as text FROM ".$BaseDatos.".G2077 WHERE G2077_C38109 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2065_C38110"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2077 WHERE G2077_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2065_C38110"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2065");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2065_ConsInte__b, G2065_FechaInsercion , G2065_Usuario ,  G2065_CodigoMiembro  , G2065_PoblacionOrigen , G2065_EstadoDiligenciamiento ,  G2065_IdLlamada , G2065_C37900 as principal ,G2065_C37899,G2065_C37900,G2065_C37901,G2065_C37902,G2065_C37903,G2065_C37904,G2065_C37905, a.LISOPC_Nombre____b as G2065_C37906,G2065_C37927,G2065_C38105, G2077_C38109,G2065_C39715,G2065_C40060,G2065_C37896,G2065_C37897, b.LISOPC_Nombre____b as G2065_C37898 FROM '.$BaseDatos.'.G2065 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2065_C37906 LEFT JOIN '.$BaseDatos.'.G2077 ON G2077_ConsInte__b  =  G2065_C38110 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2065_C37898';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2065_C37905)){
                    $hora_a = explode(' ', $fila->G2065_C37905)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2065_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2065_ConsInte__b , ($fila->G2065_C37899) , ($fila->G2065_C37900) , ($fila->G2065_C37901) , ($fila->G2065_C37902) , ($fila->G2065_C37903) , explode(' ', $fila->G2065_C37904)[0] , $hora_a , ($fila->G2065_C37906) , ($fila->G2065_C37927) , ($fila->G2065_C38105) , ($fila->G2077_C38109) , ($fila->G2065_C39715) , ($fila->G2065_C40060) , ($fila->G2065_C37896) , ($fila->G2065_C37897) , ($fila->G2065_C37898) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2065 WHERE G2065_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2065";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2065_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2065_ConsInte__b as id,  G2065_C37899 as camp2 , G2065_C37900 as camp1  FROM '.$BaseDatos.'.G2065 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2065_ConsInte__b as id,  G2065_C37899 as camp2 , G2065_C37900 as camp1  
                    FROM ".$BaseDatos.".G2065  JOIN ".$BaseDatos.".G2065_M".$_POST['muestra']." ON G2065_ConsInte__b = G2065_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2065_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2065_C37899 LIKE "%'.$B.'%" OR G2065_C37900 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2065_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2065 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2065(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2065_C37899"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37899 = '".$_POST["G2065_C37899"]."'";
                $LsqlI .= $separador."G2065_C37899";
                $LsqlV .= $separador."'".$_POST["G2065_C37899"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C37900"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37900 = '".$_POST["G2065_C37900"]."'";
                $LsqlI .= $separador."G2065_C37900";
                $LsqlV .= $separador."'".$_POST["G2065_C37900"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C37901"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37901 = '".$_POST["G2065_C37901"]."'";
                $LsqlI .= $separador."G2065_C37901";
                $LsqlV .= $separador."'".$_POST["G2065_C37901"]."'";
                $validar = 1;
            }
             
  
            $G2065_C37902 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2065_C37902"])){
                if($_POST["G2065_C37902"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2065_C37902 = $_POST["G2065_C37902"];
                    $LsqlU .= $separador." G2065_C37902 = ".$G2065_C37902."";
                    $LsqlI .= $separador." G2065_C37902";
                    $LsqlV .= $separador.$G2065_C37902;
                    $validar = 1;
                }
            }
  
            $G2065_C37903 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2065_C37903"])){
                if($_POST["G2065_C37903"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2065_C37903 = $_POST["G2065_C37903"];
                    $LsqlU .= $separador." G2065_C37903 = ".$G2065_C37903."";
                    $LsqlI .= $separador." G2065_C37903";
                    $LsqlV .= $separador.$G2065_C37903;
                    $validar = 1;
                }
            }
 
            $G2065_C37904 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2065_C37904"])){    
                if($_POST["G2065_C37904"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2065_C37904"]);
                    if(count($tieneHora) > 1){
                        $G2065_C37904 = "'".$_POST["G2065_C37904"]."'";
                    }else{
                        $G2065_C37904 = "'".str_replace(' ', '',$_POST["G2065_C37904"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2065_C37904 = ".$G2065_C37904;
                    $LsqlI .= $separador." G2065_C37904";
                    $LsqlV .= $separador.$G2065_C37904;
                    $validar = 1;
                }
            }
  
            $G2065_C37905 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G2065_C37905"])){   
                if($_POST["G2065_C37905"] != '' && $_POST["G2065_C37905"] != 'undefined' && $_POST["G2065_C37905"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2065_C37905 = "'".$fecha." ".str_replace(' ', '',$_POST["G2065_C37905"])."'";
                    $LsqlU .= $separador." G2065_C37905 = ".$G2065_C37905."";
                    $LsqlI .= $separador." G2065_C37905";
                    $LsqlV .= $separador.$G2065_C37905;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2065_C37906"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37906 = '".$_POST["G2065_C37906"]."'";
                $LsqlI .= $separador."G2065_C37906";
                $LsqlV .= $separador."'".$_POST["G2065_C37906"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C37927"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37927 = '".$_POST["G2065_C37927"]."'";
                $LsqlI .= $separador."G2065_C37927";
                $LsqlV .= $separador."'".$_POST["G2065_C37927"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C38105"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C38105 = '".$_POST["G2065_C38105"]."'";
                $LsqlI .= $separador."G2065_C38105";
                $LsqlV .= $separador."'".$_POST["G2065_C38105"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C38110"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C38110 = '".$_POST["G2065_C38110"]."'";
                $LsqlI .= $separador."G2065_C38110";
                $LsqlV .= $separador."'".$_POST["G2065_C38110"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C39715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C39715 = '".$_POST["G2065_C39715"]."'";
                $LsqlI .= $separador."G2065_C39715";
                $LsqlV .= $separador."'".$_POST["G2065_C39715"]."'";
                $validar = 1;
            }
             
  
            if (isset($_FILES["FG2065_C40060"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G2065/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G2065")){
                    mkdir("/Dyalogo/tmp/G2065", 0777);
                }
                if ($_FILES["FG2065_C40060"]["size"] != 0) {
                    $G2065_C40060 = $_FILES["FG2065_C40060"]["tmp_name"];
                    $nG2065_C40060 = $fechUp."_".$_FILES["FG2065_C40060"]["name"];
                    $rutaFinal = $destinoFile.$nG2065_C40060;
                    if (is_uploaded_file($G2065_C40060)) {
                        move_uploaded_file($G2065_C40060, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2065_C40060 = '".$nG2065_C40060."'";
                    $LsqlI .= $separador."G2065_C40060";
                    $LsqlV .= $separador."'".$nG2065_C40060."'";
                    $validar = 1;
                }
            }
            
  

            if(isset($_POST["G2065_C37896"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37896 = '".$_POST["G2065_C37896"]."'";
                $LsqlI .= $separador."G2065_C37896";
                $LsqlV .= $separador."'".$_POST["G2065_C37896"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C37897"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37897 = '".$_POST["G2065_C37897"]."'";
                $LsqlI .= $separador."G2065_C37897";
                $LsqlV .= $separador."'".$_POST["G2065_C37897"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2065_C37898"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_C37898 = '".$_POST["G2065_C37898"]."'";
                $LsqlI .= $separador."G2065_C37898";
                $LsqlV .= $separador."'".$_POST["G2065_C37898"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2065_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2065_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2065_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2065_Usuario , G2065_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2065_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2065 WHERE G2065_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2065 SET G2065_UltiGest__b =-14, G2065_GesMasImp_b =-14, G2065_TipoReintentoUG_b =0, G2065_TipoReintentoGMI_b =0, G2065_ClasificacionUG_b =3, G2065_ClasificacionGMI_b =3, G2065_EstadoUG_b =-14, G2065_EstadoGMI_b =-14, G2065_CantidadIntentos =0, G2065_CantidadIntentosGMI_b =0 WHERE G2065_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
