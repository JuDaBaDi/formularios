<script>
    document.cookie = "same-site-cookie=foo; SameSite=Lax"; 
    document.cookie = "cross-site-cookie=bar; SameSite=None; Secure";
</script>
            
<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    define("RECAPTCHA_V3_SECRET_KEY", "6Lcc1dYUAAAAAHgqTohTDsl2g-0V5-egYLC4atVb");
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   
    
        //Inserciones o actualizaciones
        if(isset($_POST["oper"])){
            $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G2226 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G2226( G2226_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G2226_C40324"]) && $_POST["G2226_C40324"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2226_C40324 = '".$_POST["G2226_C40324"]."'";
            $str_LsqlI .= $separador."G2226_C40324";
            $str_LsqlV .= $separador."'".$_POST["G2226_C40324"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2226_C40325"]) && $_POST["G2226_C40325"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2226_C40325 = '".$_POST["G2226_C40325"]."'";
            $str_LsqlI .= $separador."G2226_C40325";
            $str_LsqlV .= $separador."'".$_POST["G2226_C40325"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2226_C40326"]) && $_POST["G2226_C40326"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2226_C40326 = '".$_POST["G2226_C40326"]."'";
            $str_LsqlI .= $separador."G2226_C40326";
            $str_LsqlV .= $separador."'".$_POST["G2226_C40326"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G2226_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2226_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G2226_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2226 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    // $str_LsqlU .= $separador."G2226_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G2226_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2226 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    // $str_LsqlU .= $separador."G2226_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G2226_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        // Aqui es donde tengo que definir si el registro ya existe con anterioridad basados en la llave
        $sqlPregun = "SELECT WEBFORM_ConsInte_PREGUN_Campo_Llave_b AS pregun FROM $BaseDatos_systema.WEBFORM WHERE WEBFORM_Guion____b = 2226 LIMIT 1";
        $resPregun = $mysqli->query($sqlPregun);

        $registroExiste = false;
        $idRegistroExiste = 0;

        if($resPregun && $resPregun->num_rows > 0){
            $row = $resPregun->fetch_array();
            
            // Para que valide la llave tiene que ser diferente de cero
            if($row['pregun'] != 0){
                $llave = $row['pregun'];
                
                if(isset($_POST["G2226_C".$llave])){
    
                    $registroSql = "SELECT G2226_ConsInte__b AS id FROM $BaseDatos.G2226 WHERE G2226_C$llave = '".$_POST["G2226_C".$llave]."'";
                    $resRegistro = $mysqli->query($registroSql);

                    if($resRegistro && $resRegistro->num_rows > 0){
                        
                        $registro = $resRegistro->fetch_array();

                        $registroExiste = true;
                        $idRegistroExiste = $registro['id'];
                        
                    }
                }
            }
        }

        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' && !$registroExiste){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }else{
                $str_Lsql = $str_LsqlU. " WHERE G2226_ConsInte__b = ".$idRegistroExiste;
            }
        }

        //si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;

                // Si el registro no existe se inicializan los campos
                if(!$registroExiste){
                    // Criterios de inicialización que se usan en el cargador
                    $UpdContext = "UPDATE ".$BaseDatos.".G2226 SET G2226_UltiGest__b = -14, G2226_GesMasImp_b = -14, G2226_TipoReintentoUG_b = 0, G2226_TipoReintentoGMI_b = 0, G2226_ClasificacionUG_b = 3, G2226_ClasificacionGMI_b = 3, G2226_EstadoUG_b = -14, G2226_EstadoGMI_b = -14, G2226_CantidadIntentos = 0, G2226_CantidadIntentosGMI_b = 0 WHERE G2226_ConsInte__b = ".$ultimoResgistroInsertado;
                    $UpdContext = $mysqli->query($UpdContext);
                }else{
                    // Si el registro existe paso el id
                    $ultimoResgistroInsertado = $idRegistroExiste;
                }

                $strParamPaso_t ='';

                if (isset($_POST["pasoId"])) {
                    
                    $strParamPaso_t = '&paso='.$_POST["pasoId"];

                    DispararProceso($_POST["pasoId"], $ultimoResgistroInsertado);
                
                }

                header('Location:https://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MjIyNg==&result=1'.$strParamPaso_t);

            } else {
                echo "Error Haciendo el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
