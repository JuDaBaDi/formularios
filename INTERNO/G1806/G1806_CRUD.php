<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1806;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1806
                  SET G1806_C37505 = -201
                  WHERE G1806_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1806 
                    WHERE G1806_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1806_C37504"]) || $gestion["G1806_C37504"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1806_C37504"];
        }

        if (is_null($gestion["G1806_C37506"]) || $gestion["G1806_C37506"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1806_C37506"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1806_FechaInsercion"]."',".$gestion["G1806_Usuario"].",'".$gestion["G1806_C".$P["P"]]."','".$gestion["G1806_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "bpo.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1806 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1806_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1806_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1806_LinkContenido as url FROM ".$BaseDatos.".G1806 WHERE G1806_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1806_ConsInte__b, G1806_FechaInsercion , G1806_Usuario ,  G1806_CodigoMiembro  , G1806_PoblacionOrigen , G1806_EstadoDiligenciamiento ,  G1806_IdLlamada , G1806_C33312 as principal ,G1806_C33314,G1806_C33302,G1806_C33303,G1806_C33304,G1806_C33305,G1806_C33306,G1806_C33307,G1806_C33308,G1806_C33309,G1806_C33310,G1806_C33311,G1806_C33312,G1806_C33316,G1806_C33317,G1806_C33318,G1806_C33319,G1806_C33324,G1806_C33325,G1806_C33326,G1806_C33333,G1806_C37494,G1806_C37495,G1806_C37496,G1806_C37498,G1806_C37499,G1806_C37500,G1806_C37501,G1806_C37503,G1806_C37505,G1806_C37504,G1806_C37506,G1806_C37507 FROM '.$BaseDatos.'.G1806 WHERE G1806_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1806_C33314'] = $key->G1806_C33314;

                $datos[$i]['G1806_C33302'] = $key->G1806_C33302;

                $datos[$i]['G1806_C33303'] = $key->G1806_C33303;

                $datos[$i]['G1806_C33304'] = explode(' ', $key->G1806_C33304)[0];
  
                $hora = '';
                if(!is_null($key->G1806_C33305)){
                    $hora = explode(' ', $key->G1806_C33305)[1];
                }

                $datos[$i]['G1806_C33305'] = $hora;

                $datos[$i]['G1806_C33306'] = $key->G1806_C33306;

                $datos[$i]['G1806_C33307'] = $key->G1806_C33307;

                $datos[$i]['G1806_C33308'] = $key->G1806_C33308;

                $datos[$i]['G1806_C33309'] = $key->G1806_C33309;

                $datos[$i]['G1806_C33310'] = $key->G1806_C33310;

                $datos[$i]['G1806_C33311'] = $key->G1806_C33311;

                $datos[$i]['G1806_C33312'] = $key->G1806_C33312;

                $datos[$i]['G1806_C33316'] = $key->G1806_C33316;

                $datos[$i]['G1806_C33317'] = $key->G1806_C33317;

                $datos[$i]['G1806_C33318'] = $key->G1806_C33318;

                $datos[$i]['G1806_C33319'] = $key->G1806_C33319;

                $datos[$i]['G1806_C33324'] = $key->G1806_C33324;

                $datos[$i]['G1806_C33325'] = $key->G1806_C33325;

                $datos[$i]['G1806_C33326'] = $key->G1806_C33326;

                $datos[$i]['G1806_C33333'] = $key->G1806_C33333;

                $datos[$i]['G1806_C37494'] = $key->G1806_C37494;

                $datos[$i]['G1806_C37495'] = $key->G1806_C37495;

                $datos[$i]['G1806_C37496'] = $key->G1806_C37496;

                $datos[$i]['G1806_C37498'] = $key->G1806_C37498;

                $datos[$i]['G1806_C37499'] = $key->G1806_C37499;

                $datos[$i]['G1806_C37500'] = $key->G1806_C37500;

                $datos[$i]['G1806_C37501'] = $key->G1806_C37501;

                $datos[$i]['G1806_C37503'] = $key->G1806_C37503;

                $datos[$i]['G1806_C37505'] = $key->G1806_C37505;

                $datos[$i]['G1806_C37504'] = $key->G1806_C37504;

                $datos[$i]['G1806_C37506'] = $key->G1806_C37506;

                $datos[$i]['G1806_C37507'] = $key->G1806_C37507;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1806";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1806_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1806_ConsInte__b as id,  G1806_C33312 as camp1 , G1806_C33316 as camp2 
                     FROM ".$BaseDatos.".G1806  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1806_ConsInte__b as id,  G1806_C33312 as camp1 , G1806_C33316 as camp2  
                    FROM ".$BaseDatos.".G1806  JOIN ".$BaseDatos.".G1806_M".$_POST['muestra']." ON G1806_ConsInte__b = G1806_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1806_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1806_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1806_C33312 LIKE '%".$B."%' OR G1806_C33316 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1806_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1806");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1806_ConsInte__b, G1806_FechaInsercion , G1806_Usuario ,  G1806_CodigoMiembro  , G1806_PoblacionOrigen , G1806_EstadoDiligenciamiento ,  G1806_IdLlamada , G1806_C33312 as principal , a.LISOPC_Nombre____b as G1806_C33314, b.LISOPC_Nombre____b as G1806_C33302, c.LISOPC_Nombre____b as G1806_C33303,G1806_C33304,G1806_C33305,G1806_C33306,G1806_C33307,G1806_C33308,G1806_C33309,G1806_C33310, d.LISOPC_Nombre____b as G1806_C33311,G1806_C33312,G1806_C33316,G1806_C33317,G1806_C33318,G1806_C33319,G1806_C33324,G1806_C33325,G1806_C33326,G1806_C33333,G1806_C37494,G1806_C37495,G1806_C37496,G1806_C37498,G1806_C37499,G1806_C37500,G1806_C37501,G1806_C37503, e.LISOPC_Nombre____b as G1806_C37505,G1806_C37504,G1806_C37506,G1806_C37507 FROM '.$BaseDatos.'.G1806 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1806_C33314 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1806_C33302 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1806_C33303 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1806_C33311 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1806_C37505';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1806_C33305)){
                    $hora_a = explode(' ', $fila->G1806_C33305)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1806_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1806_ConsInte__b , ($fila->G1806_C33314) , ($fila->G1806_C33302) , ($fila->G1806_C33303) , explode(' ', $fila->G1806_C33304)[0] , $hora_a , ($fila->G1806_C33306) , ($fila->G1806_C33307) , ($fila->G1806_C33308) , ($fila->G1806_C33309) , ($fila->G1806_C33310) , ($fila->G1806_C33311) , ($fila->G1806_C33312) , ($fila->G1806_C33316) , ($fila->G1806_C33317) , ($fila->G1806_C33318) , ($fila->G1806_C33319) , ($fila->G1806_C33324) , ($fila->G1806_C33325) , ($fila->G1806_C33326) , ($fila->G1806_C33333) , ($fila->G1806_C37494) , ($fila->G1806_C37495) , ($fila->G1806_C37496) , ($fila->G1806_C37498) , ($fila->G1806_C37499) , ($fila->G1806_C37500) , ($fila->G1806_C37501) , ($fila->G1806_C37503) , ($fila->G1806_C37505) , ($fila->G1806_C37504) , ($fila->G1806_C37506) , ($fila->G1806_C37507) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1806 WHERE G1806_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1806";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1806_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1806_ConsInte__b as id,  G1806_C33312 as camp1 , G1806_C33316 as camp2  FROM '.$BaseDatos.'.G1806 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1806_ConsInte__b as id,  G1806_C33312 as camp1 , G1806_C33316 as camp2  
                    FROM ".$BaseDatos.".G1806  JOIN ".$BaseDatos.".G1806_M".$_POST['muestra']." ON G1806_ConsInte__b = G1806_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1806_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1806_C33312 LIKE "%'.$B.'%" OR G1806_C33316 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1806_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1806 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1806(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1806_C33313"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33313 = '".$_POST["G1806_C33313"]."'";
                $LsqlI .= $separador."G1806_C33313";
                $LsqlV .= $separador."'".$_POST["G1806_C33313"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33314"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33314 = '".$_POST["G1806_C33314"]."'";
                $LsqlI .= $separador."G1806_C33314";
                $LsqlV .= $separador."'".$_POST["G1806_C33314"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33315"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33315 = '".$_POST["G1806_C33315"]."'";
                $LsqlI .= $separador."G1806_C33315";
                $LsqlV .= $separador."'".$_POST["G1806_C33315"]."'";
                $validar = 1;
            }
             
 
            $G1806_C33302 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1806_C33302 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1806_C33302 = ".$G1806_C33302;
                    $LsqlI .= $separador." G1806_C33302";
                    $LsqlV .= $separador.$G1806_C33302;
                    $validar = 1;

                    
                }
            }
 
            $G1806_C33303 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1806_C33303 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1806_C33303 = ".$G1806_C33303;
                    $LsqlI .= $separador." G1806_C33303";
                    $LsqlV .= $separador.$G1806_C33303;
                    $validar = 1;
                }
            }
 
            $G1806_C33304 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1806_C33304 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1806_C33304 = ".$G1806_C33304;
                    $LsqlI .= $separador." G1806_C33304";
                    $LsqlV .= $separador.$G1806_C33304;
                    $validar = 1;
                }
            }
 
            $G1806_C33305 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1806_C33305 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1806_C33305 = ".$G1806_C33305;
                    $LsqlI .= $separador." G1806_C33305";
                    $LsqlV .= $separador.$G1806_C33305;
                    $validar = 1;
                }
            }
 
            $G1806_C33306 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1806_C33306 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1806_C33306 = ".$G1806_C33306;
                    $LsqlI .= $separador." G1806_C33306";
                    $LsqlV .= $separador.$G1806_C33306;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1806_C33307"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33307 = '".$_POST["G1806_C33307"]."'";
                $LsqlI .= $separador."G1806_C33307";
                $LsqlV .= $separador."'".$_POST["G1806_C33307"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33308"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33308 = '".$_POST["G1806_C33308"]."'";
                $LsqlI .= $separador."G1806_C33308";
                $LsqlV .= $separador."'".$_POST["G1806_C33308"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33309"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33309 = '".$_POST["G1806_C33309"]."'";
                $LsqlI .= $separador."G1806_C33309";
                $LsqlV .= $separador."'".$_POST["G1806_C33309"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33310"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33310 = '".$_POST["G1806_C33310"]."'";
                $LsqlI .= $separador."G1806_C33310";
                $LsqlV .= $separador."'".$_POST["G1806_C33310"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33311"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33311 = '".$_POST["G1806_C33311"]."'";
                $LsqlI .= $separador."G1806_C33311";
                $LsqlV .= $separador."'".$_POST["G1806_C33311"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33312"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33312 = '".$_POST["G1806_C33312"]."'";
                $LsqlI .= $separador."G1806_C33312";
                $LsqlV .= $separador."'".$_POST["G1806_C33312"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33316"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33316 = '".$_POST["G1806_C33316"]."'";
                $LsqlI .= $separador."G1806_C33316";
                $LsqlV .= $separador."'".$_POST["G1806_C33316"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33317"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33317 = '".$_POST["G1806_C33317"]."'";
                $LsqlI .= $separador."G1806_C33317";
                $LsqlV .= $separador."'".$_POST["G1806_C33317"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33318"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33318 = '".$_POST["G1806_C33318"]."'";
                $LsqlI .= $separador."G1806_C33318";
                $LsqlV .= $separador."'".$_POST["G1806_C33318"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33319"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33319 = '".$_POST["G1806_C33319"]."'";
                $LsqlI .= $separador."G1806_C33319";
                $LsqlV .= $separador."'".$_POST["G1806_C33319"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33320"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33320 = '".$_POST["G1806_C33320"]."'";
                $LsqlI .= $separador."G1806_C33320";
                $LsqlV .= $separador."'".$_POST["G1806_C33320"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33321"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33321 = '".$_POST["G1806_C33321"]."'";
                $LsqlI .= $separador."G1806_C33321";
                $LsqlV .= $separador."'".$_POST["G1806_C33321"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33322 = '".$_POST["G1806_C33322"]."'";
                $LsqlI .= $separador."G1806_C33322";
                $LsqlV .= $separador."'".$_POST["G1806_C33322"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33323"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33323 = '".$_POST["G1806_C33323"]."'";
                $LsqlI .= $separador."G1806_C33323";
                $LsqlV .= $separador."'".$_POST["G1806_C33323"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33324"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33324 = '".$_POST["G1806_C33324"]."'";
                $LsqlI .= $separador."G1806_C33324";
                $LsqlV .= $separador."'".$_POST["G1806_C33324"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33325"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33325 = '".$_POST["G1806_C33325"]."'";
                $LsqlI .= $separador."G1806_C33325";
                $LsqlV .= $separador."'".$_POST["G1806_C33325"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33326"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33326 = '".$_POST["G1806_C33326"]."'";
                $LsqlI .= $separador."G1806_C33326";
                $LsqlV .= $separador."'".$_POST["G1806_C33326"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33327"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33327 = '".$_POST["G1806_C33327"]."'";
                $LsqlI .= $separador."G1806_C33327";
                $LsqlV .= $separador."'".$_POST["G1806_C33327"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33328"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33328 = '".$_POST["G1806_C33328"]."'";
                $LsqlI .= $separador."G1806_C33328";
                $LsqlV .= $separador."'".$_POST["G1806_C33328"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33329"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33329 = '".$_POST["G1806_C33329"]."'";
                $LsqlI .= $separador."G1806_C33329";
                $LsqlV .= $separador."'".$_POST["G1806_C33329"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33330"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33330 = '".$_POST["G1806_C33330"]."'";
                $LsqlI .= $separador."G1806_C33330";
                $LsqlV .= $separador."'".$_POST["G1806_C33330"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33331"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33331 = '".$_POST["G1806_C33331"]."'";
                $LsqlI .= $separador."G1806_C33331";
                $LsqlV .= $separador."'".$_POST["G1806_C33331"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33332"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33332 = '".$_POST["G1806_C33332"]."'";
                $LsqlI .= $separador."G1806_C33332";
                $LsqlV .= $separador."'".$_POST["G1806_C33332"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33333"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33333 = '".$_POST["G1806_C33333"]."'";
                $LsqlI .= $separador."G1806_C33333";
                $LsqlV .= $separador."'".$_POST["G1806_C33333"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C33334"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C33334 = '".$_POST["G1806_C33334"]."'";
                $LsqlI .= $separador."G1806_C33334";
                $LsqlV .= $separador."'".$_POST["G1806_C33334"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C37493"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C37493 = '".$_POST["G1806_C37493"]."'";
                $LsqlI .= $separador."G1806_C37493";
                $LsqlV .= $separador."'".$_POST["G1806_C37493"]."'";
                $validar = 1;
            }
             
  
            $G1806_C37494 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37494"])){
                if($_POST["G1806_C37494"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37494 = $_POST["G1806_C37494"];
                    $LsqlU .= $separador." G1806_C37494 = ".$G1806_C37494."";
                    $LsqlI .= $separador." G1806_C37494";
                    $LsqlV .= $separador.$G1806_C37494;
                    $validar = 1;
                }
            }
  
            $G1806_C37495 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37495"])){
                if($_POST["G1806_C37495"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37495 = $_POST["G1806_C37495"];
                    $LsqlU .= $separador." G1806_C37495 = ".$G1806_C37495."";
                    $LsqlI .= $separador." G1806_C37495";
                    $LsqlV .= $separador.$G1806_C37495;
                    $validar = 1;
                }
            }
  
            $G1806_C37496 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37496"])){
                if($_POST["G1806_C37496"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37496 = $_POST["G1806_C37496"];
                    $LsqlU .= $separador." G1806_C37496 = ".$G1806_C37496."";
                    $LsqlI .= $separador." G1806_C37496";
                    $LsqlV .= $separador.$G1806_C37496;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1806_C37497"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C37497 = '".$_POST["G1806_C37497"]."'";
                $LsqlI .= $separador."G1806_C37497";
                $LsqlV .= $separador."'".$_POST["G1806_C37497"]."'";
                $validar = 1;
            }
             
  
            $G1806_C37498 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37498"])){
                if($_POST["G1806_C37498"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37498 = $_POST["G1806_C37498"];
                    $LsqlU .= $separador." G1806_C37498 = ".$G1806_C37498."";
                    $LsqlI .= $separador." G1806_C37498";
                    $LsqlV .= $separador.$G1806_C37498;
                    $validar = 1;
                }
            }
  
            $G1806_C37499 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37499"])){
                if($_POST["G1806_C37499"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37499 = $_POST["G1806_C37499"];
                    $LsqlU .= $separador." G1806_C37499 = ".$G1806_C37499."";
                    $LsqlI .= $separador." G1806_C37499";
                    $LsqlV .= $separador.$G1806_C37499;
                    $validar = 1;
                }
            }
  
            $G1806_C37500 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37500"])){
                if($_POST["G1806_C37500"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37500 = $_POST["G1806_C37500"];
                    $LsqlU .= $separador." G1806_C37500 = ".$G1806_C37500."";
                    $LsqlI .= $separador." G1806_C37500";
                    $LsqlV .= $separador.$G1806_C37500;
                    $validar = 1;
                }
            }
  
            $G1806_C37501 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37501"])){
                if($_POST["G1806_C37501"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37501 = $_POST["G1806_C37501"];
                    $LsqlU .= $separador." G1806_C37501 = ".$G1806_C37501."";
                    $LsqlI .= $separador." G1806_C37501";
                    $LsqlV .= $separador.$G1806_C37501;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1806_C37502"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C37502 = '".$_POST["G1806_C37502"]."'";
                $LsqlI .= $separador."G1806_C37502";
                $LsqlV .= $separador."'".$_POST["G1806_C37502"]."'";
                $validar = 1;
            }
             
  
            $G1806_C37503 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37503"])){
                if($_POST["G1806_C37503"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37503 = $_POST["G1806_C37503"];
                    $LsqlU .= $separador." G1806_C37503 = ".$G1806_C37503."";
                    $LsqlI .= $separador." G1806_C37503";
                    $LsqlV .= $separador.$G1806_C37503;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1806_C37505"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C37505 = '".$_POST["G1806_C37505"]."'";
                $LsqlI .= $separador."G1806_C37505";
                $LsqlV .= $separador."'".$_POST["G1806_C37505"]."'";
                $validar = 1;
            }
             
  
            $G1806_C37504 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1806_C37504"])){
                if($_POST["G1806_C37504"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1806_C37504 = $_POST["G1806_C37504"];
                    $LsqlU .= $separador." G1806_C37504 = ".$G1806_C37504."";
                    $LsqlI .= $separador." G1806_C37504";
                    $LsqlV .= $separador.$G1806_C37504;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1806_C37506"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C37506 = '".$_POST["G1806_C37506"]."'";
                $LsqlI .= $separador."G1806_C37506";
                $LsqlV .= $separador."'".$_POST["G1806_C37506"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1806_C37507"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_C37507 = '".$_POST["G1806_C37507"]."'";
                $LsqlI .= $separador."G1806_C37507";
                $LsqlV .= $separador."'".$_POST["G1806_C37507"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1806_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1806_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1806_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1806_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1806_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1806_Usuario , G1806_FechaInsercion, G1806_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1806_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1806 WHERE G1806_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
