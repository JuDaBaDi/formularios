<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
        $archivo=$_GET["adjunto"];
        if (is_file($archivo)) {
            $size = strlen($archivo);

            if ($size>0) {
                $nombre=basename($archivo);
                $masa = filesize($archivo);
                header("Content-Description: File Transfer");
                header("Content-type: application/force-download");
                header("Content-disposition: attachment; filename=".$nombre);
                header("Content-Transfer-Encoding: binary");
                header("Expires: 0");
                header("Cache-Control: must-revalidate");
                header("Pragma: public");
                header("Content-Length: " . $masa);
                ob_clean();
                flush();
                readfile($archivo);
            }  

        }else{ 
            // header("Location:" . getenv("HTTP_REFERER"));
            echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
        }   

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 2066;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G2066
                  SET G2066_C40071 = -201
                  WHERE G2066_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G2066 
                    WHERE G2066_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G2066_C40070"]) || $gestion["G2066_C40070"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G2066_C40070"];
        }

        if (is_null($gestion["G2066_C40072"]) || $gestion["G2066_C40072"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G2066_C40072"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G2066_FechaInsercion"]."',".$gestion["G2066_Usuario"].",'".$gestion["G2066_C".$P["P"]]."','".$gestion["G2066_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "interno.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>AÃ±adir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 2066 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G2066_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G2066_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G2066_LinkContenido as url FROM ".$BaseDatos.".G2066 WHERE G2066_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2066_ConsInte__b, G2066_FechaInsercion , G2066_Usuario ,  G2066_CodigoMiembro  , G2066_PoblacionOrigen , G2066_EstadoDiligenciamiento ,  G2066_IdLlamada , G2066_C37919 as principal ,G2066_C37918,G2066_C37919,G2066_C37920,G2066_C37921,G2066_C37922,G2066_C37923,G2066_C37924,G2066_C37925,G2066_C37926,G2066_C38551,G2066_C39543,G2066_C39744,G2066_C40061,G2066_C37907,G2066_C37908,G2066_C37909,G2066_C37910,G2066_C37911,G2066_C37912,G2066_C37913,G2066_C37914,G2066_C37915,G2066_C40076,G2066_C40071,G2066_C40070,G2066_C40072,G2066_C40073,G2066_C40074,G2066_C40075 FROM '.$BaseDatos.'.G2066 WHERE G2066_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2066_C37918'] = $key->G2066_C37918;

                $datos[$i]['G2066_C37919'] = $key->G2066_C37919;

                $datos[$i]['G2066_C37920'] = $key->G2066_C37920;

                $datos[$i]['G2066_C37921'] = $key->G2066_C37921;

                $datos[$i]['G2066_C37922'] = $key->G2066_C37922;

                $datos[$i]['G2066_C37923'] = explode(' ', $key->G2066_C37923)[0];
  
                $hora = '';
                if(!is_null($key->G2066_C37924)){
                    $hora = explode(' ', $key->G2066_C37924)[1];
                }

                $datos[$i]['G2066_C37924'] = $hora;

                $datos[$i]['G2066_C37925'] = $key->G2066_C37925;

                $datos[$i]['G2066_C37926'] = $key->G2066_C37926;

                $datos[$i]['G2066_C38551'] = $key->G2066_C38551;

                $datos[$i]['G2066_C39543'] = $key->G2066_C39543;

                $datos[$i]['G2066_C39744'] = $key->G2066_C39744;

                $datos[$i]['G2066_C40061'] = $key->G2066_C40061;

                $datos[$i]['G2066_C37907'] = $key->G2066_C37907;

                $datos[$i]['G2066_C37908'] = $key->G2066_C37908;

                $datos[$i]['G2066_C37909'] = explode(' ', $key->G2066_C37909)[0];
  
                $hora = '';
                if(!is_null($key->G2066_C37910)){
                    $hora = explode(' ', $key->G2066_C37910)[1];
                }

                $datos[$i]['G2066_C37910'] = $hora;

                $datos[$i]['G2066_C37911'] = $key->G2066_C37911;

                $datos[$i]['G2066_C37912'] = $key->G2066_C37912;

                $datos[$i]['G2066_C37913'] = $key->G2066_C37913;

                $datos[$i]['G2066_C37914'] = $key->G2066_C37914;

                $datos[$i]['G2066_C37915'] = $key->G2066_C37915;

                $datos[$i]['G2066_C40076'] = $key->G2066_C40076;

                $datos[$i]['G2066_C40071'] = $key->G2066_C40071;

                $datos[$i]['G2066_C40070'] = $key->G2066_C40070;

                $datos[$i]['G2066_C40072'] = $key->G2066_C40072;

                $datos[$i]['G2066_C40073'] = $key->G2066_C40073;

                $datos[$i]['G2066_C40074'] = explode(' ', $key->G2066_C40074)[0];

                $datos[$i]['G2066_C40075'] = $key->G2066_C40075;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2066";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2066_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2066_ConsInte__b as id,  G2066_C37918 as camp2 , G2066_C37919 as camp1 
                     FROM ".$BaseDatos.".G2066  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2066_ConsInte__b as id,  G2066_C37918 as camp2 , G2066_C37919 as camp1  
                    FROM ".$BaseDatos.".G2066  JOIN ".$BaseDatos.".G2066_M".$_POST['muestra']." ON G2066_ConsInte__b = G2066_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2066_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2066_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2066_C37918 LIKE '%".$B."%' OR G2066_C37919 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2066_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2066");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2066_ConsInte__b, G2066_FechaInsercion , G2066_Usuario ,  G2066_CodigoMiembro  , G2066_PoblacionOrigen , G2066_EstadoDiligenciamiento ,  G2066_IdLlamada , G2066_C37919 as principal ,G2066_C37918,G2066_C37919,G2066_C37920,G2066_C37921,G2066_C37922,G2066_C37923,G2066_C37924, a.LISOPC_Nombre____b as G2066_C37925,G2066_C37926, b.LISOPC_Nombre____b as G2066_C38551,G2066_C39543,G2066_C39744,G2066_C40061, c.LISOPC_Nombre____b as G2066_C37907, d.LISOPC_Nombre____b as G2066_C37908,G2066_C37909,G2066_C37910,G2066_C37911,G2066_C37912,G2066_C37913,G2066_C37914,G2066_C37915,G2066_C40076, e.LISOPC_Nombre____b as G2066_C40071,G2066_C40070,G2066_C40072,G2066_C40073,G2066_C40074,G2066_C40075 FROM '.$BaseDatos.'.G2066 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2066_C37925 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2066_C38551 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2066_C37907 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2066_C37908 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2066_C40071';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2066_C37910)){
                    $hora_a = explode(' ', $fila->G2066_C37910)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2066_C37924)){
                    $hora_b = explode(' ', $fila->G2066_C37924)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2066_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2066_ConsInte__b , ($fila->G2066_C37918) , ($fila->G2066_C37919) , ($fila->G2066_C37920) , ($fila->G2066_C37921) , ($fila->G2066_C37922) , explode(' ', $fila->G2066_C37923)[0] , $hora_a , ($fila->G2066_C37925) , ($fila->G2066_C37926) , ($fila->G2066_C38551) , ($fila->G2066_C39543) , ($fila->G2066_C39744) , ($fila->G2066_C40061) , ($fila->G2066_C37907) , ($fila->G2066_C37908) , explode(' ', $fila->G2066_C37909)[0] , $hora_b , ($fila->G2066_C37911) , ($fila->G2066_C37912) , ($fila->G2066_C37913) , ($fila->G2066_C37914) , ($fila->G2066_C37915) , ($fila->G2066_C40076) , ($fila->G2066_C40071) , ($fila->G2066_C40070) , ($fila->G2066_C40072) , ($fila->G2066_C40073) , explode(' ', $fila->G2066_C40074)[0] , ($fila->G2066_C40075) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2066 WHERE G2066_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2066";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2066_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2066_ConsInte__b as id,  G2066_C37918 as camp2 , G2066_C37919 as camp1  FROM '.$BaseDatos.'.G2066 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2066_ConsInte__b as id,  G2066_C37918 as camp2 , G2066_C37919 as camp1  
                    FROM ".$BaseDatos.".G2066  JOIN ".$BaseDatos.".G2066_M".$_POST['muestra']." ON G2066_ConsInte__b = G2066_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2066_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2066_C37918 LIKE "%'.$B.'%" OR G2066_C37919 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2066_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2066 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2066(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2066_C37918"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37918 = '".$_POST["G2066_C37918"]."'";
                $LsqlI .= $separador."G2066_C37918";
                $LsqlV .= $separador."'".$_POST["G2066_C37918"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C37919"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37919 = '".$_POST["G2066_C37919"]."'";
                $LsqlI .= $separador."G2066_C37919";
                $LsqlV .= $separador."'".$_POST["G2066_C37919"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C37920"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37920 = '".$_POST["G2066_C37920"]."'";
                $LsqlI .= $separador."G2066_C37920";
                $LsqlV .= $separador."'".$_POST["G2066_C37920"]."'";
                $validar = 1;
            }
             
  
            $G2066_C37921 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2066_C37921"])){
                if($_POST["G2066_C37921"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2066_C37921 = $_POST["G2066_C37921"];
                    $LsqlU .= $separador." G2066_C37921 = ".$G2066_C37921."";
                    $LsqlI .= $separador." G2066_C37921";
                    $LsqlV .= $separador.$G2066_C37921;
                    $validar = 1;
                }
            }
  
            $G2066_C37922 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2066_C37922"])){
                if($_POST["G2066_C37922"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2066_C37922 = $_POST["G2066_C37922"];
                    $LsqlU .= $separador." G2066_C37922 = ".$G2066_C37922."";
                    $LsqlI .= $separador." G2066_C37922";
                    $LsqlV .= $separador.$G2066_C37922;
                    $validar = 1;
                }
            }
 
            $G2066_C37923 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2066_C37923"])){    
                if($_POST["G2066_C37923"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2066_C37923"]);
                    if(count($tieneHora) > 1){
                        $G2066_C37923 = "'".$_POST["G2066_C37923"]."'";
                    }else{
                        $G2066_C37923 = "'".str_replace(' ', '',$_POST["G2066_C37923"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2066_C37923 = ".$G2066_C37923;
                    $LsqlI .= $separador." G2066_C37923";
                    $LsqlV .= $separador.$G2066_C37923;
                    $validar = 1;
                }
            }
  
            $G2066_C37924 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G2066_C37924"])){   
                if($_POST["G2066_C37924"] != '' && $_POST["G2066_C37924"] != 'undefined' && $_POST["G2066_C37924"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2066_C37924 = "'".$fecha." ".str_replace(' ', '',$_POST["G2066_C37924"])."'";
                    $LsqlU .= $separador." G2066_C37924 = ".$G2066_C37924."";
                    $LsqlI .= $separador." G2066_C37924";
                    $LsqlV .= $separador.$G2066_C37924;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2066_C37925"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37925 = '".$_POST["G2066_C37925"]."'";
                $LsqlI .= $separador."G2066_C37925";
                $LsqlV .= $separador."'".$_POST["G2066_C37925"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C37926"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37926 = '".$_POST["G2066_C37926"]."'";
                $LsqlI .= $separador."G2066_C37926";
                $LsqlV .= $separador."'".$_POST["G2066_C37926"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C38551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C38551 = '".$_POST["G2066_C38551"]."'";
                $LsqlI .= $separador."G2066_C38551";
                $LsqlV .= $separador."'".$_POST["G2066_C38551"]."'";
                $validar = 1;
            }
             
  
            $G2066_C39543 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2066_C39543"])){
                if($_POST["G2066_C39543"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2066_C39543 = $_POST["G2066_C39543"];
                    $LsqlU .= $separador." G2066_C39543 = ".$G2066_C39543."";
                    $LsqlI .= $separador." G2066_C39543";
                    $LsqlV .= $separador.$G2066_C39543;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2066_C39744"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C39744 = '".$_POST["G2066_C39744"]."'";
                $LsqlI .= $separador."G2066_C39744";
                $LsqlV .= $separador."'".$_POST["G2066_C39744"]."'";
                $validar = 1;
            }
             
                
            if (isset($_FILES["FG2066_C40061"]["tmp_name"])) {

                if (!file_exists("/Dyalogo/tmp/adjuntos")){
                    mkdir("/Dyalogo/tmp/adjuntos", 0777);
                }

                if (!file_exists("/Dyalogo/tmp/adjuntos/G2066")){
                    mkdir("/Dyalogo/tmp/adjuntos/G2066", 0777);
                }

                if ($_FILES["FG2066_C40061"]["size"] != 0) {

                    $G2066_C40061 = $_FILES["FG2066_C40061"]["tmp_name"];

                    $rutaFinal = $_POST["G2066_C40061"];

                    if (is_uploaded_file($G2066_C40061)) {
                        move_uploaded_file($G2066_C40061, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2066_C40061 = '".$rutaFinal."'";
                    $LsqlI .= $separador."G2066_C40061";
                    $LsqlV .= $separador."'".$rutaFinal."'";
                    $validar = 1;
                }
            }
            
 
            $G2066_C37907 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2066_C37907 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2066_C37907 = ".$G2066_C37907;
                    $LsqlI .= $separador." G2066_C37907";
                    $LsqlV .= $separador.$G2066_C37907;
                    $validar = 1;

                    
                }
            }
 
            $G2066_C37908 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2066_C37908 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2066_C37908 = ".$G2066_C37908;
                    $LsqlI .= $separador." G2066_C37908";
                    $LsqlV .= $separador.$G2066_C37908;
                    $validar = 1;
                }
            }
 
            $G2066_C37909 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2066_C37909 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2066_C37909 = ".$G2066_C37909;
                    $LsqlI .= $separador." G2066_C37909";
                    $LsqlV .= $separador.$G2066_C37909;
                    $validar = 1;
                }
            }
 
            $G2066_C37910 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2066_C37910 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2066_C37910 = ".$G2066_C37910;
                    $LsqlI .= $separador." G2066_C37910";
                    $LsqlV .= $separador.$G2066_C37910;
                    $validar = 1;
                }
            }
 
            $G2066_C37911 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2066_C37911 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2066_C37911 = ".$G2066_C37911;
                    $LsqlI .= $separador." G2066_C37911";
                    $LsqlV .= $separador.$G2066_C37911;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2066_C37912"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37912 = '".$_POST["G2066_C37912"]."'";
                $LsqlI .= $separador."G2066_C37912";
                $LsqlV .= $separador."'".$_POST["G2066_C37912"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C37913"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37913 = '".$_POST["G2066_C37913"]."'";
                $LsqlI .= $separador."G2066_C37913";
                $LsqlV .= $separador."'".$_POST["G2066_C37913"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C37914"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37914 = '".$_POST["G2066_C37914"]."'";
                $LsqlI .= $separador."G2066_C37914";
                $LsqlV .= $separador."'".$_POST["G2066_C37914"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C37915"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C37915 = '".$_POST["G2066_C37915"]."'";
                $LsqlI .= $separador."G2066_C37915";
                $LsqlV .= $separador."'".$_POST["G2066_C37915"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C40076"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C40076 = '".$_POST["G2066_C40076"]."'";
                $LsqlI .= $separador."G2066_C40076";
                $LsqlV .= $separador."'".$_POST["G2066_C40076"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C40071"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C40071 = '".$_POST["G2066_C40071"]."'";
                $LsqlI .= $separador."G2066_C40071";
                $LsqlV .= $separador."'".$_POST["G2066_C40071"]."'";
                $validar = 1;
            }
             
  
            $G2066_C40070 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2066_C40070"])){
                if($_POST["G2066_C40070"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2066_C40070 = $_POST["G2066_C40070"];
                    $LsqlU .= $separador." G2066_C40070 = ".$G2066_C40070."";
                    $LsqlI .= $separador." G2066_C40070";
                    $LsqlV .= $separador.$G2066_C40070;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2066_C40072"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C40072 = '".$_POST["G2066_C40072"]."'";
                $LsqlI .= $separador."G2066_C40072";
                $LsqlV .= $separador."'".$_POST["G2066_C40072"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2066_C40073"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C40073 = '".$_POST["G2066_C40073"]."'";
                $LsqlI .= $separador."G2066_C40073";
                $LsqlV .= $separador."'".$_POST["G2066_C40073"]."'";
                $validar = 1;
            }
             
 
            $G2066_C40074 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2066_C40074"])){    
                if($_POST["G2066_C40074"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2066_C40074"]);
                    if(count($tieneHora) > 1){
                        $G2066_C40074 = "'".$_POST["G2066_C40074"]."'";
                    }else{
                        $G2066_C40074 = "'".str_replace(' ', '',$_POST["G2066_C40074"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2066_C40074 = ".$G2066_C40074;
                    $LsqlI .= $separador." G2066_C40074";
                    $LsqlV .= $separador.$G2066_C40074;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2066_C40075"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_C40075 = '".$_POST["G2066_C40075"]."'";
                $LsqlI .= $separador."G2066_C40075";
                $LsqlV .= $separador."'".$_POST["G2066_C40075"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    
                    if ($resMonoEf = $mysqli->query($LmonoEfLSql)) {
                        if ($resMonoEf->num_rows > 0) {

                            $dataMonoEf = $resMonoEf->fetch_object();

                            $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                            $separador = "";
                            if($validar == 1){
                                $separador = ",";
                            }

                            $LsqlU .= $separador."G2066_Clasificacion = ".$conatcto;
                            $LsqlI .= $separador."G2066_Clasificacion";
                            $LsqlV .= $separador.$conatcto;
                            $validar = 1;

                        }
                    }
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2066_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2066_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2066_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2066_Usuario , G2066_FechaInsercion, G2066_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2066_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2066 WHERE G2066_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                            $UltimoID = $_POST["id"];
                            echo $UltimoID;
                        }
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2066_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2066 WHERE G2066_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2070_ConsInte__b, G2070_C38018, G2070_C38019 FROM ".$BaseDatos.".G2070  ";

        $SQL .= " WHERE G2070_C38018 = '".$numero."'"; 

        $SQL .= " ORDER BY G2070_C38018";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2070_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2070_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2070_C38018)."</cell>";

                echo "<cell>". ($fila->G2070_C38019)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2070 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2070(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2070_C38019"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2070_C38019 = '".$_POST["G2070_C38019"]."'";
                    $LsqlI .= $separador."G2070_C38019";
                    $LsqlV .= $separador."'".$_POST["G2070_C38019"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2070_C38018 = $numero;
                    $LsqlU .= ", G2070_C38018 = ".$G2070_C38018."";
                    $LsqlI .= ", G2070_C38018";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2070_Usuario ,  G2070_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2070_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2070 WHERE  G2070_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO ".$BaseDatos_systema.".LOGGEST (LOGGEST_SQL_b,LOGGEST_Error_b,LOGGEST_Comentario_b)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
