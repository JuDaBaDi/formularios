<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	include(__DIR__."/../../funciones.php");
	date_default_timezone_set('America/Bogota');

	function informacionNula($strCadena_p){

		if (is_null($strCadena_p) || $strCadena_p == NULL || $strCadena_p == null || $strCadena_p == "" || $strCadena_p == '' || $strCadena_p == "NULL" || $strCadena_p == "null") {
			
			return "NULL";

		}else{

			return "'".$strCadena_p."'";

		}		

	}

	session_start(); //DLAB Activamos la sesion para poder obtener el dato del agente activo	
	
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $arrReturn = array();
    	if (isset($_GET["traerMonoef"])) {

    		$intIdMonoef_t = $_POST["intIdMonoef_t"];

    		$strSQLTraerMonoef_t = "SELECT LISOPC_ConsInte__b AS id FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_Clasifica_b = ".$intIdMonoef_t;
    		$resSQLTraerMonoef_t = $mysqli->query($strSQLTraerMonoef_t);

    		if ($resSQLTraerMonoef_t->num_rows > 0) {
    			
				$objSQLTraerMonoef_t = $resSQLTraerMonoef_t->fetch_object();   
				$intIdLisopc_t = $objSQLTraerMonoef_t->id;

				echo $intIdLisopc_t;  			

    		}else{

    			echo -1;

    		}


    	}

    	$strOrigen_t = "normal";

    	if (isset($_POST["origenGestion"])) {
    		$strOrigen_t = $_POST["origenGestion"];
    	}

    	$strCanal_t = "sin canal";

    	if (isset($_POST["strCanal_t"])) {
    		$strCanal_t = $_POST["strCanal_t"];
    	}

    	if (isset($_GET['action'])) {
	    	if($_GET['action'] == 'ADD'){

				/* toca insertar un registro vacio y editarlo desde el script */
				$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
		        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
		        $datoCampan = $res_Lsql_Campan->fetch_array();
		        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
		        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
		        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
		        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	         	$rea_ConfD_Campan = $datoCampan['CAMPAN_ConfDinam_b'];
                $origen=$mysqli->query("SELECT PREGUN_ConsInte__b FROM DYALOGOCRM_SISTEMA.PREGUN WHERE PREGUN_ConsInte__GUION__b = {$datoCampan['CAMPAN_ConsInte__GUION__Pob_b']} AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'");
                $strCampoOrigen='';
                $strOrigen='';
                if($origen && $origen->num_rows == 1){
                    $origen=$origen->fetch_object();
                    $idCampoOrigen=$origen->PREGUN_ConsInte__b;
                    $strCampoOrigen=$str_Pobla_Campan."_C".$idCampoOrigen;
                    if(isset($_GET['canal'])){
                        if($_GET['canal'] == 'telefonia'){
                            $strOrigen='Insertado desde el agente';
                        }else{
                            $strOrigen=$_GET['canal'];
                        }
                    }
                }
				$Lsql = "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan." (".$str_Pobla_Campan."_FechaInsercion,{$strCampoOrigen},{$str_Pobla_Campan}_UltiGest__b,{$str_Pobla_Campan}_GesMasImp_b,{$str_Pobla_Campan}_TipoReintentoUG_b,{$str_Pobla_Campan}_TipoReintentoGMI_b,{$str_Pobla_Campan}_ClasificacionUG_b,{$str_Pobla_Campan}_ClasificacionGMI_b,{$str_Pobla_Campan}_EstadoUG_b,{$str_Pobla_Campan}_EstadoGMI_b,{$str_Pobla_Campan}_CantidadIntentos,{$str_Pobla_Campan}_CantidadIntentosGMI_b) VALUES ('".date('Y-m-d H:i:s')."','{$strOrigen}',-14,-14,0,0,3,3,-14,-14,0,0);";
				if ($mysqli->query($Lsql) === TRUE) {

	                $resultado = $mysqli->insert_id;
	                
	                if($rea_ConfD_Campan == '-1'){

	                	$InsertMuestra = "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." (".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b , ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b, ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b) VALUES ({$resultado}, 0, 0);";
	                	if($mysqli->query($InsertMuestra) !== true){
	                		echo "error muestra 1= > ".$mysqli->error;
                            echo $InsertMuestra."||";
	                	}
	                
	                }else{//Si la configuracion es predefinida

	                	$muestraCompleta = $str_Pobla_Campan."_M".$int_Muest_Campan;

	                	$Xlsql = "SELECT ASITAR_ConsInte__USUARI_b, COUNT(".$muestraCompleta."_ConIntUsu_b) AS total FROM     ".$BaseDatos_systema.".ASITAR LEFT JOIN ".$BaseDatos.".".$muestraCompleta." ON ASITAR_ConsInte__USUARI_b = ".$muestraCompleta."_ConIntUsu_b WHERE ASITAR_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND (".$muestraCompleta."_Estado____b <> 3 OR (".$muestraCompleta."_Estado____b IS NULL)) AND (ASITAR_Automaticos_b <> 0 OR (ASITAR_Automaticos_b IS NULL)) GROUP BY ASITAR_ConsInte__USUARI_b ORDER BY COUNT(".$muestraCompleta."_ConIntUsu_b) LIMIT 1;";
						
						//DLAB Modificacion para setear el ID del usuario y no el de la consulta rara
						if(!isset($_SESSION['USER_ID'])){
							if (isset($_GET['token'])) {
								$token = $_GET["token"];
								$idAgente = "SELECT SESSIONS__USUARI_ConsInte__b FROM DYALOGOCRM_SISTEMA.SESSIONS where SESSIONS__Token='" . $token . "';";
								$query = $mysqli->query($idAgente);
								$datosAgente = $query->fetch_array();
								$insertarMuestraLsql = "INSERT INTO " . $BaseDatos . "." . $muestraCompleta . " (" . $muestraCompleta . "_CoInMiPo__b , " . $muestraCompleta . "_NumeInte__b, " . $muestraCompleta . "_Estado____b , " . $muestraCompleta . "_ConIntUsu_b) VALUES (" . $resultado . ", 0 , 0, " . $datosAgente['SESSIONS__USUARI_ConsInte__b'] . ");";
							} else {
								$res = $mysqli->query($Xlsql);
								$datoLsql = $res->fetch_array();
								$insertarMuestraLsql = "INSERT INTO  " . $BaseDatos . "." . $muestraCompleta . " (" . $muestraCompleta . "_CoInMiPo__b ,  " . $muestraCompleta . "_NumeInte__b, " . $muestraCompleta . "_Estado____b , " . $muestraCompleta . "_ConIntUsu_b) VALUES (" . $resultado . ", 0 , 0, " . $datoLsql['ASITAR_ConsInte__USUARI_b'] . ");";
							}
									
						}else{ //Si el usuario si esta seteado en la sesion
							$insertarMuestraLsql = "INSERT INTO " . $BaseDatos . "." . $muestraCompleta . " (" . $muestraCompleta . "_CoInMiPo__b , " . $muestraCompleta . "_NumeInte__b, " . $muestraCompleta . "_Estado____b , " . $muestraCompleta . "_ConIntUsu_b) VALUES (" . $resultado . ", 0 , 0, " . $_SESSION['USER_ID'] . ");";
						}

						if($mysqli->query($insertarMuestraLsql) !== true){
							echo "error muestra 2= > ".$mysqli->error;
                            echo $insertarMuestraLsql."||";
						}
					}

						
	                
	                echo $resultado;

	            
	            }

			}
		
			if($_GET['action'] == 'EDIT'){
				crearMiembroDefault($_GET["campana_crm"]);//funcioon que crea usuario por defecto
				/* primero buscamos la campaña que nos esta llegando */
				$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b,CAMPAN_TipoCamp__b,CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];			

				//echo $Lsql_Campan;

		        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
		        $datoCampan = $res_Lsql_Campan->fetch_array();
		        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
		        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
		        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
		        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
		        $tipoMarcador=$datoCampan['CAMPAN_TipoCamp__b'];
	            $rea_ConfD_Campan = $datoCampan['CAMPAN_ConfDinam_b'];


		        /* Aqui se hace la jugada de la actualizacion */
		        if(!isset($_GET['cerrarForzado'])){
		        	$ActualizaLsql = "SELECT CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b =".$_GET['campana_crm']; 

			        $resultado = $mysqli->query($ActualizaLsql);
			        $datoArray = $resultado->fetch_array();
			        if($datoArray['CAMPAN_ActPobGui_b'] == '-1'){
			        	/* toca hacer actualizacion desde Script */
			        	
			        	$campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_GET["campana_crm"];
						$resultcampSql = $mysqli->query($campSql);
						$Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
						$i=0;

						//echo "SELECT CAMINC ".$campSql;
				        while($key = $resultcampSql->fetch_object()){
	                        $validoparaedicion = false;
	                        $valorScript = $key->CAMINC_NomCamGui_b;

	                        $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                        $resultShow = $mysqli->query($LsqlShow);
	                        if($resultShow->num_rows === 0){
	                            //comentario el campo no existe
	                            $validoparaedicion = false;
	                        }else{
	                            $validoparaedicion = true;
	                        } 

	                        $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                        //echo $LsqlShow;
	                        $resultShow = $mysqli->query($LsqlShow);
	                        if($resultShow->num_rows === 0 ){
	                            //comentario el campo no existe
	                            $validoparaedicion = false;
	                        }else{
	                            $validoparaedicion = true;
	                        } 

	                        $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_GET['ConsInteRegresado'];
	                        $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                        if($LsqlRes){
	                            $sata = $LsqlRes->fetch_array();
	                            if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                            }else{
	                                $valorScript = 'NULL';
	                            }
	                        }

	                        if($validoparaedicion){
	                            if($i == 0){
	                                $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                            }else{
	                                $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                            }
	                            $i++;    
	                        }
	                        
				        } 


				        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_GET['ConsInteRegresado'].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
				        //echo "Esta CONSULTA UPDATE BD ".$Lsql;
				        if($mysqli->query($Lsql) === TRUE ){
                            $arrReturn["BD-GUION"]="OK";
				        }else{
				        	$arrReturn["BD-GUION"]="NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
				        }
			        }

		        }

		       

		                
		        
				$UltiGest="NULL";
				$gestionMIMP="NULL";
				$FecUltGes="NULL";
				$fechasGMIMP="NULL";
				$reintento = "NULL";			       
		        $TipoReintentoGMI="NULL";
		        $fechaAgenda = "NULL";
				$FecHorAgeGMI="NULL";
				$conatcto = "NULL";
		        $contactoMasImp = "NULL";
		        $EstadoUG="NULL";
			    $EstadoGMI="NULL";
			    $UsuarioUG="NULL";
		 		$UsuarioGMI="NULL";
		 		$CanalUG="";
		        $CanalGMI="";
		        $SentidoUG=""; 
		        $SentidoGMI="";
		 		$CantidadIntentosGMI="NULL";
		 		$ComentarioGMI="";
		 		$ComentarioUG="";
		        $LinkContenidoUG="";
				$LinkContenidoGMI="";
				$DetalleCanalUG="";
	            $DetalleCanalGMI="";
	            $DatoContactoUG="";
	            $DatoContactoGMI="";	
				$PasoUG="NULL";
	            $PasoGMI="NULL";
                $booCanal=false;
				$strTelefono_t=""; 
		
	     			
						

	            if( isset($_GET["campana_crm"]) ){
	            	$Lsql="SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND (ESTPAS_Tipo______b = 1 OR ESTPAS_Tipo______b = 6)";            
		             if( ($query = $mysqli->query($Lsql)) == TRUE ){
		                $array = $query->fetch_array();  
		                if($array["ESTPAS_ConsInte__b"] != ''){
		                	 $PasoUG=$array["ESTPAS_ConsInte__b"];    
		                	 $PasoGMI=$array["ESTPAS_ConsInte__b"];  
		                }
		                    
		  
		            }
	            }

	            
	            if(isset($_POST['datoContacto']) && $_POST['datoContacto'] != "" && $_POST['datoContacto'] != "0" && $_POST['datoContacto'] != NULL){
                        $strTelefono_t=$_POST['datoContacto'];
	            		$DatoContactoUG=str_replace("'", "", $_POST['datoContacto']);
		            	$strQuitar="A".$_GET["campana_crm"];
		            	$DatoContactoUG = str_replace($strQuitar,"", $DatoContactoUG);
		            	$DatoContactoUG=str_replace("'", "", $DatoContactoUG);
		            	$DatoContactoGMI=$DatoContactoUG;
	            	
	            }

	           
	           if ($DatoContactoUG != "NULL" && $PasoUG != "NULL" &&  $_GET["campana_crm"] != null && $DatoContactoUG != "" && $PasoUG != "" && $_GET["campana_crm"] != "" && isset($_GET["campana_crm"])){
	               $DetalleCanalUG=detalle_canal($DatoContactoUG,$PasoUG ,$_GET["campana_crm"]);
	               $DetalleCanalGMI=$DetalleCanalUG;
	           }


				
				if(isset($_POST['MonoEf'])  &&  $_POST['MonoEf'] != '' &&  $_POST['MonoEf'] !=  '0' &&  $_POST['MonoEf'] != null ){

					$LmonoEfLSql = "SELECT * FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
			        $resMonoEf = $mysqli->query($LmonoEfLSql);
			        $dataMonoEf = $resMonoEf->fetch_array();

					$gestionMIMP = $_POST['MonoEf'];
					$UltiGest=$_POST['MonoEf'];
	                

					$reintento = $dataMonoEf['MONOEF_TipNo_Efe_b'];			       
		        	$TipoReintentoGMI=$dataMonoEf['MONOEF_TipNo_Efe_b'];	

					$conatcto = $dataMonoEf['MONOEF_Contacto__b'];
					$contactoMasImp = $dataMonoEf['MONOEF_Contacto__b'];
	                
	                $FecHorMinProGes__b =$dataMonoEf['MONOEF_CanHorProxGes__b'];

					if($dataMonoEf['MONOEF_TipiCBX___b'] != '' && $dataMonoEf['MONOEF_TipiCBX___b'] != NULL && $dataMonoEf['MONOEF_TipiCBX___b'] != '0' ){

			        	$EstadoUG=$dataMonoEf['MONOEF_TipiCBX___b'];
			        	$EstadoGMI=$dataMonoEf['MONOEF_TipiCBX___b'];
	                    

			        }


				}		
				
				
				if(isset($_GET['tiempo'])){
					$fechasGMIMP = "'".$_GET['tiempo']."'";;
					$FecUltGes="'".$_GET['tiempo']."'";;
				}
		        
		        if ( isset($_GET['usuario']) ) {
		        	$UsuarioUG=$_GET['usuario'];
		 			$UsuarioGMI=$_GET['usuario'];
		        }	        
		 		
		 		if (isset($_POST['textAreaComentarios'])) {
		 			$ComentarioGMI=$_POST['textAreaComentarios'];
		 			$ComentarioUG=$_POST['textAreaComentarios'];
		 		}
		 		
		 		 if(isset($_POST['TxtFechaReintento']) && $_POST['TxtFechaReintento'] != '' && $_POST['TxtFechaReintento'] != null){
		        	$fechaAgenda =  "'".$_POST['TxtFechaReintento']." ".str_replace(" ", "",$_POST['TxtHoraReintento'])."'";
		        	$FecHorAgeGMI=$fechaAgenda;
				}            


	            $valorId_Gestion_Cbx = $_POST['id_gestion_cbx'];
	            $valorId_Gestion_Cbx_2 = $_POST['id_gestion_cbx'];
	            if(isset($_POST['idLlamada']) && $_POST['idLlamada'] != 0 && $_POST['idLlamada'] != null){            	
	                
	                /* Toca averiguar lo del Coninte de la gestio */
	                $valorSentido = $_POST['idLlamada'];
	                $LsqlXUnique = "SELECT unique_id FROM dyalogo_telefonia.dy_llamadas_salientes where bunique_id LIKE '%".$valorSentido."%';";
	                $resPXunique = $mysqli->query($LsqlXUnique);
	                if($resPXunique){
	                    $estoUnique = $resPXunique->fetch_array();
	                    if( $estoUnique['unique_id'] != null &&  $estoUnique['unique_id'] != ''){
	                        $valorId_Gestion_Cbx = $estoUnique['unique_id'];  
	                    }else{
	                        $valorId_Gestion_Cbx = $valorSentido;
	                    }
	                }else{
	                    $valorId_Gestion_Cbx = $valorSentido;
	                }
	            	

	            }else{
	                
	                /* Toca averiguar lo del Coninte de la gestio */
	                $valorSentido = explode('_', $valorId_Gestion_Cbx_2)[1];
	                $LsqlXUnique = "SELECT unique_id FROM dyalogo_telefonia.dy_llamadas_salientes where bunique_id LIKE '%".$valorSentido."%';";
	                $resPXunique = $mysqli->query($LsqlXUnique);
	                if($resPXunique){
	                    $estoUnique = $resPXunique->fetch_array();
	                    if( $estoUnique['unique_id'] != null &&  $estoUnique['unique_id'] != ''){
	                        $valorId_Gestion_Cbx = $estoUnique['unique_id'];  
	                    }else{
	                        $valorId_Gestion_Cbx = $valorSentido;
	                    }
	                }else{
	                    $valorId_Gestion_Cbx = $valorSentido;
	                }
	            }

	            //armamos el link para descargar grabaciones


	            if( $valorId_Gestion_Cbx != '' ){
                    $Lsql = "SELECT ip_servidor FROM dyalogo_telefonia.dy_configuracion_crm WHERE id_huesped = -1 AND sistema = 0";
                    if( ($query = $mysqli->query($Lsql)) == TRUE ){
                        $array = $query->fetch_array();    
                        $rest = substr($valorId_Gestion_Cbx, 0,1);
                        if($rest !== 'd' && $rest !== 'D'){
                            $booCanal=true;

                              if(isset($_POST['idLlamada']) && $_POST['idLlamada'] != 0){
                                $LinkContenidoUG="https://".$array["ip_servidor"].":8181/dyalogocore/api/voip/downloadrecord?tk=25L8cKxojzX5HFeXgy2L&uid=".$valorId_Gestion_Cbx."&uid2=".$valorSentido;
                              }else{
                                $LinkContenidoUG="https://".$array["ip_servidor"].":8181/dyalogocore/api/voip/downloadrecord?tk=25L8cKxojzX5HFeXgy2L&uid=".$valorId_Gestion_Cbx."&uid2=".$valorSentido;
                              }	              

                              $LinkContenidoGMI= $LinkContenidoUG;	  

                        }else{
                            $intUniqueId_t=$mysqli->query("SELECT unique_id FROM dyalogo_telefonia.dy_llamadas_salientes WHERE numero_marcado={$strTelefono_t} order by fecha_hora desc limit 1");
                            if($intUniqueId_t && $intUniqueId_t->num_rows ==1){
                                $intUniqueId_t=$intUniqueId_t->fetch_object();
                                $LinkContenidoUG="https://".$array["ip_servidor"].":8181/dyalogocore/api/voip/downloadrecord?tk=25L8cKxojzX5HFeXgy2L&uid=".$intUniqueId_t->unique_id."&uid2=".$intUniqueId_t->unique_id;
                            }else{
                                $LinkContenidoUG='No hubo comunicación por lo tanto no se genera un link de grabación';

                            }
                        }
                    }
				}

	            $sentidoX = NULL;
	            $sentidoY = 0;
	            $valorSentido = NULL;
	            /* Rellenar los datos del Script */
	            $validoCamposX = 1;
		      
	        	if(isset($_POST['cbx_sentido']) && $_POST['cbx_sentido'] != 0){
	                $sentidoY = $_POST['cbx_sentido'];
	        		if($_POST['cbx_sentido'] == '1'){
	        			$sentidoX = 'Saliente';
	        		}else{
	        			$sentidoX = 'Entrante';
	        		}
	        	}else{
	        		$sentidoX = 0;
	        	}

	        	$SentidoUG=$sentidoX;
	        	$SentidoGMI=$sentidoX;
	        	$valorSentido = explode('_', $valorId_Gestion_Cbx_2)[0];
	        	$CanalUG=$strCanal_t;
	        	$CanalGMI=$strCanal_t;
	        	$fechaInicial = new DateTime($_GET['tiempo']);
	            $fechaFinal = new DateTime($_POST['FechaFinal']);
	            $duracion = $fechaInicial->diff($fechaFinal);
	            $fechaInsercion = date('Y-m-d H:i:s');
                $strCanalLink_t='';
                if($booCanal){
                    $strCanalLink_t="&canal={$CanalUG}";
                }else{
                    // $CanalUG='sin canal';
                    // $strCanal_t = "sin canal";
                    // $valorSentido= 'sin canal';
                }
	            /**
			     *actualizacion del script
			    */

	        	$LsqlUpdateCamposX = "UPDATE ".$BaseDatos.".G".$int_Guion_Campan." SET ";
	        	$LsqlUpdateCamposX .= "G".$int_Guion_Campan."_FechaInsercion = '".$_GET['tiempo']."'";
	        	$LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_IdLlamada =  '".$valorId_Gestion_Cbx."'";
		        $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_Sentido___b =  '".$SentidoUG."'";
		        $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_Canal_____b =  '".$strCanal_t."'";
		        $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_Origen_b =  '".$strOrigen_t."'";
		        $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_LinkContenido =  '".$LinkContenidoUG.$strCanalLink_t."'";
		        $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_Clasificacion =  ".$conatcto;
	            $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_Paso =  ".$PasoUG;
	            $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_DatoContacto =  '".$DatoContactoUG."'";
	            $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_DetalleCanal =  '".$DetalleCanalUG."'";
	            $LsqlUpdateCamposX .= ", G".$int_Guion_Campan."_Duracion___b =  '".$duracion->format("%H:%I:%S")."'";
	           

	            

	            $LsqlReintento = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_Texto_____b = 'Reintento' AND PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan.";";
	            $res = $mysqli->query($LsqlReintento);
	            if($res){
	                $datoReintento = $res->fetch_array();
	                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = 'G".$int_Guion_Campan."_C".$datoReintento['PREGUN_ConsInte__b']."' ";
	                $result = $mysqli->query($Lsql);
	                if($result->num_rows === 0){
	                    
	                }else{
	                    $LsqlUpdateCamposX .= " , G".$int_Guion_Campan."_C".$datoReintento['PREGUN_ConsInte__b']." =  '".$reintento."' ";
	                }
	            }
	            

	        	$LsqlUpdateCamposX .= "  WHERE G".$int_Guion_Campan."_ConsInte__b = ".$_GET['ConsInteRegresado'];

	        	if($mysqli->query($LsqlUpdateCamposX) === true){
                    $arrReturn["SCRIPT"]="OK";
                    $arrReturn["SCRIPT-QUERY"]=$LsqlUpdateCamposX;
	        	}else{
	        		$arrReturn["SCRIPT"]="Error actualizando el sentido y canal del Script => ".$mysqli->error;        		
	        		$queryScript="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
					VALUES(\"".$LsqlUpdateCamposX."\",\"".$mysqli->error."\",'SCRIPT')";
					$mysqli->query($queryScript);
	        	}

	        	
	        	//JDBD - Verificamos si la gestion que llega es mas importante o igual que la que ya esta en la base.
	        	$strSQLGesMasImp_t = "SELECT ".$str_Pobla_Campan."_ClasificacionGMI_b AS clasificacionBD, ".$str_Pobla_Campan."_CantidadIntentosGMI_b AS intentosBD, ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CantidadIntentosGMI_b AS intentosMT, ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b AS clasificacionMT FROM ".$BaseDatos.".".$str_Pobla_Campan." LEFT JOIN ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." ON ".$str_Pobla_Campan."_ConsInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['CodigoMiembro'];

	        	$resSQLGesMasImp_t = $mysqli->query($strSQLGesMasImp_t);

	        	$objSQLGesMasImp_t = $resSQLGesMasImp_t->fetch_object();

		        $intIntentosGMI_BD_t = $objSQLGesMasImp_t->intentosBD;
		        if( $intIntentosGMI_BD_t == '' ||  $intIntentosGMI_BD_t == null || $intIntentosGMI_BD_t == 'NULL'){
		        	 $intIntentosGMI_BD_t=0;
		        }

		        $intIntentosGMI_MT_t = $objSQLGesMasImp_t->intentosMT;
		        if( $intIntentosGMI_MT_t == '' ||  $intIntentosGMI_MT_t == null || $intIntentosGMI_MT_t == 'NULL'){
		        	 $intIntentosGMI_MT_t=0;
		        }

	        	$strSQLGesMasImpBD_t = "";
	        	$strSQLGesMasImpMT_t = "";

	        	$intClasificacionBD_t = $objSQLGesMasImp_t->clasificacionBD;

	        	if (is_null($intClasificacionBD_t)) {
	        		$intClasificacionBD_t = 0;
	        	}

	        	$intClasificacionMT_t = $objSQLGesMasImp_t->clasificacionMT;

	        	if (is_null($intClasificacionMT_t)) {
	        		$intClasificacionMT_t = 0;
	        	}

	        	if ($conatcto >= $intClasificacionBD_t) {

	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_GesMasImp_b =  ".$gestionMIMP;
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_TipoReintentoGMI_b =  ".$TipoReintentoGMI;
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_ClasificacionGMI_b =  ".$contactoMasImp;
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_UsuarioGMI_b =  ".$UsuarioGMI;
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_CantidadIntentosGMI_b =  ".($intIntentosGMI_BD_t+1);
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_EstadoGMI_b =  ".$EstadoGMI;
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_PasoGMI_b =  ".$PasoGMI; 
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_ComentarioGMI_b =  '".$ComentarioGMI."'";
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_CanalGMI_b =  '".$CanalGMI."'";
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_SentidoGMI_b =  '".$SentidoGMI."'";
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_LinkContenidoGMI_b =  '".$LinkContenidoGMI."'";
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_DatoContactoGMI_b =  '".$DatoContactoGMI."'";
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_DetalleCanalGMI_b =  '".$DetalleCanalGMI."'";
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_FeGeMaIm__b =  ".$fechasGMIMP; 
	        		$strSQLGesMasImpBD_t .= ",".$str_Pobla_Campan."_FecHorAgeGMI_b =  ".$FecHorAgeGMI;

	        	}

	        	if ($conatcto >= $intClasificacionMT_t) {

	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = ".$gestionMIMP;
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_TipoReintentoGMI_b = ".$TipoReintentoGMI;
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = ".$contactoMasImp;
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_UsuarioGMI_b = ".$UsuarioGMI;
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_CantidadIntentosGMI_b = ".($intIntentosGMI_MT_t+1);
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_EstadoGMI_b  =".$EstadoGMI;
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_ComentarioGMI_b  = '".$ComentarioGMI."'";
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_CanalGMI_b = '".$CanalGMI."'"; 
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_SentidoGMI_b = '".$SentidoGMI."'";
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_LinkContenidoGMI_b = '".$LinkContenidoGMI."'";
	        		$strSQLGesMasImpMT_t .=",".$str_Pobla_Campan."_M".$int_Muest_Campan."_DatoContactoGMI_b  = '".$DatoContactoGMI."'";
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_DetalleCanalGMI_b  = '".$DetalleCanalGMI."'";
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b = ".$fechasGMIMP;	
	        		$strSQLGesMasImpMT_t .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAgeGMI_b = ".$FecHorAgeGMI;

	        	}



		       /**
		       *actualizacion muestra
		       */

		        $MuestraSql  =  "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET ";
		        $MuestraSql .= $str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = ".$UltiGest;
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = ".$FecUltGes;
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = ".$reintento;
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = ".$fechaAgenda;
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = ".$conatcto;
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_UsuarioUG_b = ".$UsuarioUG;
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1";
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = '".$ComentarioUG."'";
		        $MuestraSql .=",".$str_Pobla_Campan."_M".$int_Muest_Campan."_DatoContactoUG_b  = '".$DatoContactoUG."'";
		        $MuestraSql .=",".$str_Pobla_Campan."_M".$int_Muest_Campan."_DetalleCanalUG_b  = '".$DetalleCanalUG."'";        
		        $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_LinkContenidoUG_b = '".$LinkContenidoUG.$strCanalLink_t."'";
	            $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_CanalUG_b = '".$CanalUG."'"; 
	            $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_SentidoUG_b = '".$SentidoUG."'";  
	            $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorMinProGes__b = DATE_ADD(NOW(), INTERVAL ".$FecHorMinProGes__b." HOUR)";
	            $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = '".$_POST['Efectividad']."'";
	        	$MuestraSql.=",".$str_Pobla_Campan."_M".$int_Muest_Campan."_EstadoUG_b  = ".$EstadoUG;
		        $MuestraSql .= $strSQLGesMasImpMT_t;
	            
	            if($rea_ConfD_Campan == '-1'){
	                if($reintento=='2'){
	                    $Agenda=$mysqli->query('select CAMPAN_Agenda_fija, CAMPAN_ConfDinam_b FROM DYALOGOCRM_SISTEMA.CAMPAN where CAMPAN_ConsInte__b='.$_GET["campana_crm"]);
	                    if($Agenda){
	                        $dato=$Agenda->fetch_object();
	                        if($dato->CAMPAN_Agenda_fija == '-1'){                     
	                            $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConIntUsu_b = ".$UsuarioUG;
	                        }
	                    }                   
	                }
//                    else{
//	                    $MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConIntUsu_b = NULL";    
//	                }
	            }

		        $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_GET['CodigoMiembro'];

	        	if($mysqli->query($MuestraSql) === true){
                    $arrReturn["MUESTRA"]="OK";
                    $arrReturn["MUESTRA-QUERY"]=$MuestraSql;
	        	}else{
	        		$arrReturn["MUESTRA"]="Error insertando la muestra => ".$mysqli->error;
	        		$queryMuestra="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
					VALUES(\"".$MuestraSql ."\",\"".$mysqli->error."\",'MUESTRA')";
					$mysqli->query($queryMuestra);
	        	}

	           

	        	/**
		       *actualizacion Bases de datos
		       */
		     
		        $LsqlUpdateCampos = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan." SET ";
		        $LsqlUpdateCampos .= $str_Pobla_Campan."_UltiGest__b =  ".$UltiGest;	      
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_FecUltGes_b =  ".$FecUltGes;
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_TipoReintentoUG_b =  ".$reintento;
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_FecHorAgeUG_b =  ".$fechaAgenda;
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_ClasificacionUG_b =  ".$conatcto;
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_UsuarioUG_b =  ".$UsuarioUG;
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_CantidadIntentos =  ".$str_Pobla_Campan."_CantidadIntentos + 1";
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_ComentarioUG_b =  '".$ComentarioUG."'";
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_PasoUG_b =  ".$PasoUG; 
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_DatoContactoUG_b =  '".$DatoContactoUG."'";
	            $LsqlUpdateCampos .= ",".$str_Pobla_Campan."_DetalleCanalUG_b =  '".$DetalleCanalUG."'";         	
	          	$LsqlUpdateCampos .= ",".$str_Pobla_Campan."_LinkContenidoUG_b =  '".$LinkContenidoUG.$strCanalLink_t."'";
	          	$LsqlUpdateCampos .= ",".$str_Pobla_Campan."_Canal_____b =  '".$strCanal_t."'";
	          	$LsqlUpdateCampos .= ",".$str_Pobla_Campan."_Sentido___b =  '".$SentidoUG."'";
	          	$LsqlUpdateCampos .= ",".$str_Pobla_Campan."_IdLlamada =  '".$valorId_Gestion_Cbx."'";
	        	$LsqlUpdateCampos .= ",".$str_Pobla_Campan."_EstadoUG_b =  ".$EstadoUG;
	        	$LsqlUpdateCampos .= ",".$str_Pobla_Campan."_EstadoGMI_b =  ".$EstadoGMI;
	        	$LsqlUpdateCampos .= $strSQLGesMasImpBD_t;
	            $LsqlUpdateCampos .= " WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['CodigoMiembro'];

	        	if($mysqli->query($LsqlUpdateCampos) === true){
                    $arrReturn["BD"]="OK";
	                $arrReturn["BD-QUERY"]=$LsqlUpdateCampos;

	        	}else{
	        		$arrReturn["BD"]="Error actualizando la poblacion => ".$mysqli->error;
	        		$queryBD="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
					VALUES(\"".$LsqlUpdateCampos ."\",\"".$mysqli->error."\",'DB')";
					$mysqli->query($queryBD);
	        	}

		        //Rellenar los datos de condia         
	            
		        

		       	$SqlGESTIEM = "SELECT GESTIEM_TiempoUP FROM ".$BaseDatos_systema.".GESTIEM WHERE GESTIEM_Id_Gestion_cbx = '".$valorId_Gestion_Cbx_2."'";   
		        $re = $mysqli->query($SqlGESTIEM);
		        $fechaTimeUp = NULL;
		        while($k = $re->fetch_object()){
		        	$fechaTimeUp = $k->GESTIEM_TiempoUP;	
		        }

		        $CONDIA_IndiEfec__b = "'".$_POST['Efectividad']."'";

		        if (is_null($_POST['Efectividad']) || $_POST['Efectividad'] == null || $_POST['Efectividad'] == "" || $_POST['Efectividad'] == "NULL") {

		        	$CONDIA_IndiEfec__b = "'0'";
		        	
		        }

		        $CONDIA_TipNo_Efe_b = informacionNula($reintento);
		        $CONDIA_ConsInte__MONOEF_b = informacionNula($_POST['MonoEf']);
		        $CONDIA_TiemDura__b = "'".date('Y-m-d').' '.$duracion->format("%H:%I:%S")."'";
		        $CONDIA_Fecha_____b = informacionNula($_GET['tiempo']);
		        $CONDIA_ConsInte__CAMPAN_b = informacionNula($_GET["campana_crm"]);
		        $CONDIA_ConsInte__USUARI_b = $_GET['usuario'];
		        $CONDIA_ConsInte__GUION__Gui_b = informacionNula($int_Guion_Campan);
		        $CONDIA_ConsInte__GUION__Pob_b = informacionNula($int_Pobla_Camp_2);
		        $CONDIA_ConsInte__MUESTR_b = informacionNula($int_Muest_Campan);
		        $CONDIA_CodiMiem__b = informacionNula($_GET['CodigoMiembro']);
		        $CONDIA_Observacio_b = informacionNula($_POST['textAreaComentarios']);
		        $CONDIA_FechaAgenda_b = $fechaAgenda;
		        $CONDIA_IdenLlam___b = informacionNula($valorId_Gestion_Cbx);
		        $CONDIA_TiemPrev__b = informacionNula("0");
		        $CONDIA_TiemUp____b = informacionNula($fechaTimeUp);
		        $CONDIA_Canal_b = informacionNula($valorSentido);
		        $CONDIA_Sentido___b = informacionNula($sentidoY);
		        $CONDIA_UniqueId_b = informacionNula($valorId_Gestion_Cbx);

		        $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA (
			        	CONDIA_IndiEfec__b, 
			        	CONDIA_TipNo_Efe_b, 
			        	CONDIA_ConsInte__MONOEF_b, 
			        	CONDIA_TiemDura__b, 
			        	CONDIA_Fecha_____b, 
			        	CONDIA_ConsInte__CAMPAN_b, 
			        	CONDIA_ConsInte__USUARI_b, 
			        	CONDIA_ConsInte__GUION__Gui_b, 
			        	CONDIA_ConsInte__GUION__Pob_b, 
			        	CONDIA_ConsInte__MUESTR_b, 
			        	CONDIA_CodiMiem__b, 
			        	CONDIA_Observacio_b, 
			        	CONDIA_FechaAgenda_b,
			        	CONDIA_IdenLlam___b,
			        	CONDIA_TiemPrev__b,
			        	CONDIA_TiemUp____b, 
			        	CONDIA_Canal_b,
			        	CONDIA_Sentido___b,
                        CONDIA_UniqueId_b) 
			        	VALUES (
		        		".$CONDIA_IndiEfec__b.", 
		        		".$CONDIA_TipNo_Efe_b.",
		        		".$CONDIA_ConsInte__MONOEF_b.",
		        		".$CONDIA_TiemDura__b.",
		        		".$CONDIA_Fecha_____b.",
		        		".$CONDIA_ConsInte__CAMPAN_b.",
		        		".$CONDIA_ConsInte__USUARI_b.",
		        		".$CONDIA_ConsInte__GUION__Gui_b.",
		        		".$CONDIA_ConsInte__GUION__Pob_b.",
		        		".$CONDIA_ConsInte__MUESTR_b.",
		        		".$CONDIA_CodiMiem__b.",
		        		".$CONDIA_Observacio_b." ,
		        		".$CONDIA_FechaAgenda_b.",
		        		".$CONDIA_IdenLlam___b.",
		        		".$CONDIA_TiemPrev__b.",
		        		".$CONDIA_TiemUp____b.",
		        		".$CONDIA_Canal_b.",
		        		".$CONDIA_Sentido___b.",
                        ".$CONDIA_UniqueId_b."
	        		)";

		        if($mysqli->query($CondiaSql) === true){
                    $arrReturn["CONDIA"]="OK";
                    $arrReturn["CONDIA-QUERY"]=trim(preg_replace('/\s+/', ' ', $CondiaSql));
		        }else{
		        	$arrReturn["CONDIA"]="Error insertando Condia => ".$mysqli->error;
	        		$queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
					VALUES(\"".$CondiaSql."\",\"".$mysqli->error."\",'CONDIA')";
					$mysqli->query($queryCondia);
		        }

		        if( !(isset($_POST['llamarApi'])) ){
		        	 if(isset($_GET['token']) && isset($_GET['id_gestion_cbx']) ){
		        	
			        	$data =  array();
			        	$conatacto = 0;
			        	if(isset($_POST['ContactoMonoEf']) && $_POST['ContactoMonoEf'] != '' ){
			        		$conatacto = $_POST['ContactoMonoEf'];
			        	}

			        	$consinte = -1;
			        	if(isset($_GET['consinte']) && $_GET['consinte'] != ''){
			        		$consinte = $_GET['consinte'];
			        	}

			        	if(isset($_GET['usuario']) && $_GET['usuario'] != '')
			        	{
			        		$consinte = $_GET['usuario'];
			        	}

		        		if(!isset($_POST['TxtFechaReintento'])){
			        		$data = array(	
			        					"strToken_t" => $_GET['token'], 
										"strIdGestion_t" => $valorId_Gestion_Cbx_2, 
										"intTipoReintento_t" => $reintento,
										"intConsInte_t"	=> $consinte,
										"strFechaHoraAgenda_t" => null,
										"booForzarCierre_t" => true,
										"intMonoefEfectiva_t" => $_POST['Efectividad'],
										"intConsInteTipificacion_t" => $_POST['MonoEf'],
										"boolFinalizacionDesdeBlend_t" => false,
										"intMonoefContacto_t" => $conatacto
									); 
						}else{
							
							$data = array(	
									"strToken_t" => $_GET['token'], 
									"strIdGestion_t" => $valorId_Gestion_Cbx_2, 
									"intTipoReintento_t" => $reintento,
									"strFechaHoraAgenda_t" => $_POST['TxtFechaReintento']." ".str_replace(" ", "",$_POST['TxtHoraReintento']),
									"intConsInte_t"	=> $consinte,
									"booForzarCierre_t" => true,
									"intMonoefEfectiva_t" => $_POST['Efectividad'],
									"intConsInteTipificacion_t" => $_POST['MonoEf'],
									"boolFinalizacionDesdeBlend_t" => false,
									"intMonoefContacto_t" => $conatacto
							); 
						}
			        	
                        if(isset($_GET["cerrarViaPost"])){
                            $arrReturn['dataGestion']=$data;
                        }else{
                            $ch = curl_init($IP_CONFIGURADA.'gestion/finalizar');

                            $data_string = json_encode($data);  

                            echo $data_string;  
                            //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 

                            //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
                            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                                'Content-Type: application/json',                                                                                
                                'Content-Length: ' . strlen($data_string))                                                                      
                            ); 
                            //recogemos la respuesta
                            $respuesta = curl_exec ($ch);
                            //o el error, por si falla
                            $error = curl_error($ch);
                            //y finalmente cerramos curl
                            echo "Respuesta =>  ". $respuesta;
                            echo "<br/>Error => ".$error;

                            curl_close ($ch);
                        }
                            echo json_encode($arrReturn);
					}
		        }
				// BSV - Aqui deberia llamar la funcion de insertar en la muestra segun la flecha 
				// NOTA: SI EXPLOTA ES PORQUE FALTO HACER MAS PRUEBAS 
				if(isset($_GET['campana_crm']) && $_GET['campana_crm'] != 0){
					$psql = "SELECT ESTPAS_ConsInte__b as pasoId FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__CAMPAN_b = ".$_GET['campana_crm']. " LIMIT 1";
					$pasoq = $mysqli->query($psql);
					
					if($pasoq->num_rows > 0){
						$data = $pasoq->fetch_array();

						//Ejecuto el proceso
						DispararProceso($data['pasoId'], $_GET['CodigoMiembro']);
					}

				}                
			}
	        
	//      NBG*2020-05* Insertar registros que existen en la bd pero no lo muestra        
	        if($_GET['action'] == 'ADD_MUESTRA'){
				$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConfDinam_b FROM {$BaseDatos_systema}.CAMPAN WHERE CAMPAN_ConsInte__b = {$_GET["campana_crm"]}";			

				//echo $Lsql_Campan;

		        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
		        $datoCampan = $res_Lsql_Campan->fetch_array();
		        $str_Pobla_Campan = "G{$datoCampan['CAMPAN_ConsInte__GUION__Pob_b']}";
		        $int_Muest_Campan = "{$str_Pobla_Campan}_M{$datoCampan['CAMPAN_ConsInte__MUESTR_b']}";
	            $rea_ConfD_Campan = $datoCampan['CAMPAN_ConfDinam_b'];
	            
	            $sqlMuestra="SELECT * FROM {$BaseDatos}.{$int_Muest_Campan} WHERE {$int_Muest_Campan}_CoInMiPo__b={$_POST['id']}";
	            $sqlMuestra=$mysqli->query($sqlMuestra);
	            if($sqlMuestra && $sqlMuestra->num_rows === 0){
	                if($rea_ConfD_Campan == '-1'){
	                    $sqlMuestra="INSERT INTO {$BaseDatos}.{$int_Muest_Campan} ({$int_Muest_Campan}_CoInMiPo__b , {$int_Muest_Campan}_NumeInte__b, {$int_Muest_Campan}_Estado____b) VALUES ({$_POST['id']}, 0, 0);";
	                        if($mysqli->query($sqlMuestra) !== true){
	                            echo "error muestra = > ".$mysqli->error;
	                        }
	                }else{//Si la configuracion es predefinida

	                	$Xlsql = "SELECT ASITAR_ConsInte__USUARI_b, COUNT(".$int_Muest_Campan."_ConIntUsu_b) AS total FROM     ".$BaseDatos_systema.".ASITAR LEFT JOIN ".$BaseDatos.".".$int_Muest_Campan." ON ASITAR_ConsInte__USUARI_b = ".$int_Muest_Campan."_ConIntUsu_b WHERE ASITAR_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND (".$int_Muest_Campan."_Estado____b <> 3 OR (".$int_Muest_Campan."_Estado____b IS NULL)) AND (ASITAR_Automaticos_b <> 0 OR (ASITAR_Automaticos_b IS NULL)) GROUP BY ASITAR_ConsInte__USUARI_b ORDER BY COUNT(".$int_Muest_Campan."_ConIntUsu_b) LIMIT 1;";
						
						if(!isset($_SESSION['USER_ID'])){
							if (isset($_GET['token'])) {
								$token = $_GET["token"];
								$idAgente = "SELECT SESSIONS__USUARI_ConsInte__b FROM DYALOGOCRM_SISTEMA.SESSIONS where SESSIONS__Token='" . $token . "';";
								$query = $mysqli->query($idAgente);
								$datosAgente = $query->fetch_array();
								$insertarMuestraLsql = "INSERT INTO " . $BaseDatos . "." . $int_Muest_Campan . " (" . $int_Muest_Campan . "_CoInMiPo__b , " . $int_Muest_Campan . "_NumeInte__b, " . $int_Muest_Campan . "_Estado____b , " . $int_Muest_Campan . "_ConIntUsu_b) VALUES (" . $_POST['id'] . ", 0 , 0, " . $datosAgente['SESSIONS__USUARI_ConsInte__b'] . ");";
							} else {
								$res = $mysqli->query($Xlsql);
								$datoLsql = $res->fetch_array();
								$insertarMuestraLsql = "INSERT INTO  " . $BaseDatos . "." . $int_Muest_Campan . " (" . $int_Muest_Campan . "_CoInMiPo__b ,  " . $int_Muest_Campan . "_NumeInte__b, " . $int_Muest_Campan . "_Estado____b , " . $int_Muest_Campan . "_ConIntUsu_b) VALUES (" . $_POST['id'] . ", 0 , 0, " . $datoLsql['ASITAR_ConsInte__USUARI_b'] . ");";
							}
									
						}else{ //Si el usuario si esta seteado en la sesion
							$insertarMuestraLsql = "INSERT INTO " . $BaseDatos . "." . $int_Muest_Campan . " (" . $int_Muest_Campan . "_CoInMiPo__b , " . $int_Muest_Campan . "_NumeInte__b, " . $int_Muest_Campan . "_Estado____b , " . $int_Muest_Campan . "_ConIntUsu_b) VALUES (" . $_POST['id'] . ", 0 , 0, " . $_SESSION['USER_ID'] . ");";
						}

						if($mysqli->query($insertarMuestraLsql) !== true){
							echo "error muestra = > ".$mysqli->error;
						}
					}

	            }
	            
	        }   
    	}
        
	}