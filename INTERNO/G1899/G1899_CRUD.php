<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G1899/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G1899/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G1899/".$archivo);
                        $tamaÃ±o = filesize("/Dyalogo/tmp/G1899/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaÃ±o);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G1899/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1899_ConsInte__b, G1899_FechaInsercion , G1899_Usuario ,  G1899_CodigoMiembro  , G1899_PoblacionOrigen , G1899_EstadoDiligenciamiento ,  G1899_IdLlamada , G1899_C31043 as principal ,G1899_C31043,G1899_C31044,G1899_C31072,G1899_C31074,G1899_C31104,G1899_C31040,G1899_C31041,G1899_C31042 FROM '.$BaseDatos.'.G1899 WHERE G1899_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1899_C31043'] = $key->G1899_C31043;

                $datos[$i]['G1899_C31044'] = $key->G1899_C31044;

                $datos[$i]['G1899_C31072'] = $key->G1899_C31072;

                $datos[$i]['G1899_C31074'] = $key->G1899_C31074;

                $datos[$i]['G1899_C31104'] = $key->G1899_C31104;

                $datos[$i]['G1899_C31040'] = $key->G1899_C31040;

                $datos[$i]['G1899_C31041'] = $key->G1899_C31041;

                $datos[$i]['G1899_C31042'] = $key->G1899_C31042;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1899";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1899_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1899_ConsInte__b as id,  G1899_C31043 as camp1 , G1899_C31044 as camp2 
                     FROM ".$BaseDatos.".G1899  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1899_ConsInte__b as id,  G1899_C31043 as camp1 , G1899_C31044 as camp2  
                    FROM ".$BaseDatos.".G1899  JOIN ".$BaseDatos.".G1899_M".$_POST['muestra']." ON G1899_ConsInte__b = G1899_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1899_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1899_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1899_C31043 LIKE '%".$B."%' OR G1899_C31044 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1899_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1899");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1899_ConsInte__b, G1899_FechaInsercion , G1899_Usuario ,  G1899_CodigoMiembro  , G1899_PoblacionOrigen , G1899_EstadoDiligenciamiento ,  G1899_IdLlamada , G1899_C31043 as principal ,G1899_C31043,G1899_C31044,G1899_C31072,G1899_C31074,G1899_C31104,G1899_C31040,G1899_C31041, a.LISOPC_Nombre____b as G1899_C31042 FROM '.$BaseDatos.'.G1899 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1899_C31042';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G1899_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1899_ConsInte__b , ($fila->G1899_C31043) , ($fila->G1899_C31044) , ($fila->G1899_C31072) , ($fila->G1899_C31074) , ($fila->G1899_C31104) , ($fila->G1899_C31040) , ($fila->G1899_C31041) , ($fila->G1899_C31042) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1899 WHERE G1899_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1899";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1899_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1899_ConsInte__b as id,  G1899_C31043 as camp1 , G1899_C31044 as camp2  FROM '.$BaseDatos.'.G1899 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1899_ConsInte__b as id,  G1899_C31043 as camp1 , G1899_C31044 as camp2  
                    FROM ".$BaseDatos.".G1899  JOIN ".$BaseDatos.".G1899_M".$_POST['muestra']." ON G1899_ConsInte__b = G1899_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1899_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1899_C31043 LIKE "%'.$B.'%" OR G1899_C31044 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1899_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1899 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1899(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1899_C31043"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_C31043 = '".$_POST["G1899_C31043"]."'";
                $LsqlI .= $separador."G1899_C31043";
                $LsqlV .= $separador."'".$_POST["G1899_C31043"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1899_C31044"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_C31044 = '".$_POST["G1899_C31044"]."'";
                $LsqlI .= $separador."G1899_C31044";
                $LsqlV .= $separador."'".$_POST["G1899_C31044"]."'";
                $validar = 1;
            }
             
  
            $G1899_C31072 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1899_C31072"])){
                if($_POST["G1899_C31072"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1899_C31072 = $_POST["G1899_C31072"];
                    $LsqlU .= $separador." G1899_C31072 = ".$G1899_C31072."";
                    $LsqlI .= $separador." G1899_C31072";
                    $LsqlV .= $separador.$G1899_C31072;
                    $validar = 1;
                }
            }
  
            if (isset($_FILES["FG1899_C31074"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G1899/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G1899")){
                    mkdir("/Dyalogo/tmp/G1899", 0777);
                }
                if ($_FILES["FG1899_C31074"]["size"] != 0) {
                    $G1899_C31074 = $_FILES["FG1899_C31074"]["tmp_name"];
                    $nG1899_C31074 = $fechUp."_".$_FILES["FG1899_C31074"]["name"];
                    $rutaFinal = $destinoFile.$nG1899_C31074;
                    if (is_uploaded_file($G1899_C31074)) {
                        move_uploaded_file($G1899_C31074, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1899_C31074 = '".$nG1899_C31074."'";
                    $LsqlI .= $separador."G1899_C31074";
                    $LsqlV .= $separador."'".$nG1899_C31074."'";
                    $validar = 1;
                }
            }
            
  
            if(isset($_POST["G1899_C31104"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_C31104 = '".$_POST["G1899_C31104"]."'";
                $LsqlI .= $separador."G1899_C31104";
                $LsqlV .= $separador."'".$_POST["G1899_C31104"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1899_C31040"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_C31040 = '".$_POST["G1899_C31040"]."'";
                $LsqlI .= $separador."G1899_C31040";
                $LsqlV .= $separador."'".$_POST["G1899_C31040"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1899_C31041"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_C31041 = '".$_POST["G1899_C31041"]."'";
                $LsqlI .= $separador."G1899_C31041";
                $LsqlV .= $separador."'".$_POST["G1899_C31041"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1899_C31042"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_C31042 = '".$_POST["G1899_C31042"]."'";
                $LsqlI .= $separador."G1899_C31042";
                $LsqlV .= $separador."'".$_POST["G1899_C31042"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1899_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1899_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1899_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1899_Usuario , G1899_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1899_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1899 WHERE G1899_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G1899 SET G1899_UltiGest__b =-14, G1899_GesMasImp_b =-14, G1899_TipoReintentoUG_b =0, G1899_TipoReintentoGMI_b =0, G1899_EstadoUG_b =-14, G1899_EstadoGMI_b =-14, G1899_CantidadIntentos =0, G1899_CantidadIntentosGMI_b =0 WHERE G1899_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
