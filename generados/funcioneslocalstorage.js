function insertarRegistroG(formulario_crm, agente, datoContacto=null,canal=null,origen=null,sentido=null) {
    var idInsertado = '';
    var contacto='';
    var strSentido='';
    if(sentido =='2'){
        strSentido='Entrante';  
    }
    
    if(sentido =='1'){
        strSentido='Saliente';  
    }    
    if(datoContacto != null){
        contacto=datoContacto;
    }
    try {
        $.ajax({
            url: 'formularios/generados/PHP_Ejecutar.php',
            type: 'post',
            async: false,
            data: {
                insertaRegistro: 'si',
                formulario: formulario_crm,
                agente: agente,
                datoContacto:contacto,
                canal:canal,
                origen:origen,
                sentido:strSentido
            },
            success: function (resultado) {
                idInsertado = resultado;
            }
        });
        console.log('insertarRegistroG('+formulario_crm, agente, datoContacto,canal,origen,sentido+')');
        return idInsertado;

    } catch (e) {
        console.log(e);
        return false;
    }
}

function guardarStorage(idGestion, id_user, dataGestion, formData, formulario_crm = null, agente = null, datoContacto=null, canal=null, origen=null,sentido=null) {
    try{

        if (typeof (Storage) !== 'undefined') {
            var d = new Date();
            var mes = '0';
            var minuto = '0';
            var segundo = '0';
            var dia = '0';
            if (d.getMonth() > 9) {
                mes = '';
            }
            if (d.getMinutes() > 9) {
                minuto = '';
            }
            if (d.getSeconds() > 9) {
                segundo = '';
            }
            if (d.getDate() > 9) {
                dia = '';
            }
            var strFecha_t = d.getFullYear() + '-' + mes + (d.getMonth() + 1) + '-' + dia + d.getDate() + ' ' + d.getHours() + ':' + minuto + d.getMinutes() + ':' + segundo + d.getSeconds();

            var ObjInicio_t = {};
            var saveLocal = {};
            var allGestiones = {};
            var regGestionado = {};
            var ObjCampos_t = {};
            var cliGestionado = {};
            var select = {};

            var getGestiones = sessionStorage.getItem("gestiones");
            var yaExiste = 0;
            var campoEditado = 0;
            var contador, gestion, conteo, section, collapse, RegistroInsertado;
            
            var strOrigen='Sin origen';
            var strAni='0';
            if (getGestiones !== null && getGestiones !== "" && getGestiones !== false && getGestiones !== undefined) {
                gestion = JSON.parse(getGestiones);
                $.each(gestion, function (index, value) {
                    contador = index;
                    allGestiones[index] = value;
                    if (typeof (id_user) !== 'undefined' && id_user !== null && id_user !== 'N/A') {
                        yaExiste = 1;
                        if (typeof (value) == 'object' && value.hasOwnProperty('id_gestion')) {
                            if (value.id_gestion == idGestion) {
                                regGestionado.FormAbierto = 'si';
                                regGestionado.id_gestion = value.id_gestion;
                                regGestionado.fechaInicio = value.fechaInicio;
                                regGestionado.id_user = id_user;
                                regGestionado.ObjGestion = dataGestion;
                                if (value.hasOwnProperty('RegistroInsertado')) {
                                    regGestionado.RegistroInsertado = value.RegistroInsertado;
                                }
                                allGestiones[index] = regGestionado;
                            }
                        }
                    } else {
                        if (typeof (allGestiones[index]) == 'object') {
                            if (allGestiones[index].id_gestion == idGestion) {
                                yaExiste = 1;
                                if (allGestiones[index].hasOwnProperty('FormAbierto')) {
                                    if (allGestiones[index].hasOwnProperty('id_user') && dataGestion !== null && dataGestion !== 'N/A') {
                                        console.log('ya hay registro activo, es el ' + allGestiones[index].id_user + ' para la gesti�n ' + idGestion);
                                        if (typeof (allGestiones[index].ObjGestion == 'object')) {
                                            console.log('hay objeto');
                                            $("#buscador").hide();
                                            $("#botones").hide();
                                            $("#resulados").hide();
                                            if(allGestiones[index].ObjGestion["origen"].length >0){
                                                strOrigen=allGestiones[index].ObjGestion["origen"];
                                            }
                                            if(allGestiones[index].ObjGestion["ani"].length >0){
                                                strAni=allGestiones[index].ObjGestion["ani"];
                                            }                                            
                                            $("#frameContenedor").attr('src', allGestiones[index].ObjGestion["server"] + '/crm_php/Estacion_contact_center.php?campan=true&user=' + allGestiones[index].id_user + '&view=si&canal=' + allGestiones[index].ObjGestion["canal"] + '&token=' + allGestiones[index].ObjGestion["token"] + '&id_gestion_cbx=' + allGestiones[index].id_gestion + '&predictiva=' + allGestiones[index].ObjGestion["predictiva"] + '&consinte=' + allGestiones[index].ObjGestion["consinte"] + '&campana_crm=' + allGestiones[index].ObjGestion["id_campana_crm"] + '&id_campana_cbx=' + allGestiones[index].ObjGestion["id_campana_cbx"] + '&sentido=' + allGestiones[index].ObjGestion["sentido"] + '&usuario=' + allGestiones[index].ObjGestion["usuario"]+ '&origen='+strOrigen+'&ani='+strAni);
                                        }
                                    }

                                    try{
                                        if (allGestiones[index].hasOwnProperty('ObjCliente')) {
                                            if(typeof (allGestiones[index].ObjCliente == 'object'))
                                            $.each(allGestiones[index].ObjCliente, function (i, item) {
                                                conteo = i;
                                                allGestiones[index].ObjCliente[i] = item;

                                                    if (allGestiones[index].ObjCliente[i].hasOwnProperty('collapse')) {
                                                        var click = {};
                                                        section = document.getElementsByTagName('a');
                                                        $.each(section, function (h, href) {
                                                            if ($(this).attr('href') == allGestiones[index].ObjCliente[i].href) {
                                                                collapse = allGestiones[index].ObjCliente[i].href;
                                                                if ($(collapse).hasClass('in')) {
                                                                    if (allGestiones[index].ObjCliente[i].collapse == 'false') {
                                                                        click[i] = {};
                                                                        click[i].href = allGestiones[index].ObjCliente[i].href;
                                                                        if (click[i].hasOwnProperty('click')) {

                                                                        } else {
                                                                            $(this).attr('aria-expanded', 'false');
                                                                            $(collapse).removeClass('in');
                                                                            $(collapse).attr('aria-expanded', 'false');
                                                                            click[i].click = 'si';
                                                                            console.log(click[i]);
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (allGestiones[index].ObjCliente[i].collapse == 'true') {
                                                                        click[i] = {};
                                                                        click[i].href = allGestiones[index].ObjCliente[i].href;
                                                                        if (click[i].hasOwnProperty('click')) {

                                                                        } else {
                                                                            $(this).attr('aria-expanded', 'true');
                                                                            $(collapse).addClass('in');
                                                                            $(collapse).attr('aria-expanded', 'true');
                                                                            click[i].click = 'si';
                                                                            console.log(click[i]);
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        });
                                                    }

                                                if (allGestiones[index].ObjCliente[i].type == 'INPUT' || allGestiones[index].ObjCliente[i].type == 'TEXTAREA') {
                                                    $("#" + allGestiones[index].ObjCliente[i].id).val(allGestiones[index].ObjCliente[i].value);
                                                }

                                                if (typeof (formData) == 'object' && formData != null) {
                                                    if (formData.hasOwnProperty('collapse')) {
                                                        if (allGestiones[index].ObjCliente[i].hasOwnProperty('collapse') && allGestiones[index].ObjCliente[i].href == formData.href) {
                                                            campoEditado = 1;
                                                            allGestiones[index].ObjCliente[i].href = formData.href;
                                                            allGestiones[index].ObjCliente[i].collapse = formData.collapse;
                                                        }
                                                    } else {
                                                        if (allGestiones[index].ObjCliente[i].hasOwnProperty('id') && allGestiones[index].ObjCliente[i].id == formData.id) {
                                                            campoEditado = 1;
                                                            allGestiones[index].ObjCliente[i].value = formData.value;
                                                            $("#" + allGestiones[index].ObjCliente[i].id).val(allGestiones[index].ObjCliente[i].value);
                                                        }
                                                    }
                                                }
                                            });

                                            if (campoEditado === 0) {
                                                conteo++;
                                                if (typeof (formData) == 'object' && formData != null) {
                                                    allGestiones[index].ObjCliente[conteo] = formData;
                                                }
                                            }
                                        } else {
                                            if (typeof (formData) == 'object' && formData != null) {
                                                if (typeof (value) == 'object') {
                                                    cliGestionado.id_gestion = value.id_gestion;
                                                    cliGestionado.fechaInicio = value.fechaInicio;
                                                    if (allGestiones[index].hasOwnProperty('id_user')) {
                                                        cliGestionado.id_user = value.id_user;
                                                    }
                                                    if (allGestiones[index].hasOwnProperty('ObjGestion')) {
                                                        cliGestionado.ObjGestion = value.ObjGestion;
                                                    }

                                                    if (allGestiones[index].hasOwnProperty('FormAbierto')) {
                                                        cliGestionado.FormAbierto = value.FormAbierto;
                                                    }
                                                    if (allGestiones[index].hasOwnProperty('RegistroInsertado')) {
                                                        cliGestionado.RegistroInsertado = value.RegistroInsertado;
                                                    } else {
                                                        if (formulario_crm != 'null' && formulario_crm != null) {
                                                            RegistroInsertado = insertarRegistroG(formulario_crm, agente, datoContacto, canal, origen, sentido);
                                                            if (RegistroInsertado) {
                                                                cliGestionado.RegistroInsertado = RegistroInsertado;
                                                            }
                                                        }
                                                    }
                                                }
                                                ObjCampos_t[0] = formData;
                                                cliGestionado.ObjCliente = ObjCampos_t;
                                                allGestiones[index] = cliGestionado;
                                            }

                                        }
                                    }catch(e){
                                        console.log(e);
                                    }
                                    if (!allGestiones[index].hasOwnProperty('RegistroInsertado')) {
                                        if (formulario_crm != 'null' && formulario_crm != null) {
                                            cliGestionado.id_gestion = value.id_gestion;
                                            cliGestionado.fechaInicio = value.fechaInicio;
                                            if (allGestiones[index].hasOwnProperty('FormAbierto')) {
                                                cliGestionado.FormAbierto = value.FormAbierto;
                                            }
                                            if (allGestiones[index].hasOwnProperty('id_user')) {
                                                cliGestionado.id_user = value.id_user;
                                            }
                                            if (allGestiones[index].hasOwnProperty('ObjGestion')) {
                                                cliGestionado.ObjGestion = value.ObjGestion;
                                            }
                                            RegistroInsertado = insertarRegistroG(formulario_crm, agente, datoContacto, canal, origen,sentido);
                                            if (RegistroInsertado) {
                                                cliGestionado.RegistroInsertado = RegistroInsertado;
                                            }

                                            allGestiones[index] = cliGestionado;
                                        }
                                    }

                                } else {
                                    if (allGestiones[index].hasOwnProperty('id_user') && dataGestion !== null) {
                                        if (typeof (allGestiones[index].ObjGestion == 'object')) {
                                            console.log('hay objeto');
                                            allGestiones[index].FormAbierto = 'si';
                                            $("#buscador").hide();
                                            $("#botones").hide();
                                            $("#resulados").hide();
                                            if(allGestiones[index].ObjGestion["origen"].length >0){
                                                strOrigen=allGestiones[index].ObjGestion["origen"];
                                            }
                                            if(allGestiones[index].ObjGestion["ani"].length >0){
                                                strAni=allGestiones[index].ObjGestion["ani"];
                                            }                                            
                                            $("#frameContenedor").attr('src', allGestiones[index].ObjGestion["server"] + '/crm_php/Estacion_contact_center.php?campan=true&user=' + allGestiones[index].id_user + '&view=si&canal=' + allGestiones[index].ObjGestion["canal"] + '&token=' + allGestiones[index].ObjGestion["token"] + '&id_gestion_cbx=' + allGestiones[index].id_gestion + '&predictiva=' + allGestiones[index].ObjGestion["predictiva"] + '&consinte=' + allGestiones[index].ObjGestion["consinte"] + '&campana_crm=' + allGestiones[index].ObjGestion["id_campana_crm"] + '&id_campana_cbx=' + allGestiones[index].ObjGestion["id_campana_cbx"] + '&sentido=' + allGestiones[index].ObjGestion["sentido"] + '&usuario=' + allGestiones[index].ObjGestion["usuario"]+ '&origen='+strOrigen+'&ani='+strAni);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (idGestion == value || idGestion === null) {
                                yaExiste = 1;
                            }
                        }
                    }
                });

                if (yaExiste === 0 && idGestion !== null && idGestion !== '' && idGestion !== '0') {
                    contador++;
                    ObjInicio_t.fechaInicio = strFecha_t;
                    ObjInicio_t.id_gestion = idGestion;
                    ObjInicio_t.FormAbierto = 'si';
                    if (formulario_crm != 'null' && formulario_crm != null) {
                        RegistroInsertado = insertarRegistroG(formulario_crm, agente, datoContacto, canal, origen,sentido);
                        if (RegistroInsertado) {
                            ObjInicio_t.RegistroInsertado = RegistroInsertado;
                        }
                    }
                    allGestiones[contador] = ObjInicio_t;
                }
                sessionStorage.setItem("gestiones", JSON.stringify(allGestiones));

            } else {
                if (idGestion !== '' && idGestion !== '0') {
                    ObjInicio_t.fechaInicio = strFecha_t;
                    ObjInicio_t.id_gestion = idGestion;
                    ObjInicio_t.FormAbierto = 'si';
                    if (formulario_crm != 'null' && formulario_crm != null) {
                        RegistroInsertado = insertarRegistroG(formulario_crm, agente, datoContacto, canal, origen,sentido);
                        if (RegistroInsertado) {
                            ObjInicio_t.RegistroInsertado = RegistroInsertado;
                        }
                    }
                    saveLocal[0] = ObjInicio_t;
                    sessionStorage.setItem("gestiones", JSON.stringify(saveLocal));
                }
            }

        } else {
            console.log('sessionStorage no compatible');
        }
    }catch(e){
        console.log(e);
    }

}

function borrarStorage(idGestion) {
    var getGestion = sessionStorage.getItem("gestiones");
    getGestion = JSON.parse(getGestion);
    $.each(getGestion, function (i, item) {
        if (typeof (item) == 'object') {
            if (item.hasOwnProperty('id_gestion') && item.id_gestion == idGestion) {
                delete getGestion[i];
                sessionStorage.setItem("gestiones", JSON.stringify(getGestion));
            }
        }

        if (Object.entries(getGestion).length === 0) {
            sessionStorage.removeItem("gestiones");
        }
    });

}

try {
    var targetNode = document.querySelector("form");

    for (let i = 0, len = targetNode.length; i < len; i += 1) {
        $(targetNode[i]).on("change", ev => {
            var Campo = {
                id: ev.target.id,
                value: ev.target.value,
                type: ev.target.tagName
            };
            var gestion = document.getElementById('id_gestion_cbx').value;
            guardarStorage(gestion, null, null, Campo);
        });
    }

} catch (e) {
    console.log(e);
}

$('a').click(function () {
    if ($(this).attr('data-toggle')) {
        var collapse = $(this).attr('href');
        var campo = {
            href: collapse
        };

        if ($(collapse).hasClass('in')) {
            campo.collapse = 'false';
        } else {
            campo.collapse = 'true';
        }

        var gestion = document.getElementById('id_gestion_cbx').value;
        guardarStorage(gestion, null, null, campo);
    }
});
