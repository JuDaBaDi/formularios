<div class="modal fade-in" id="modal_sendMail" data-backdrop="static" data-keyboard="false" role="dialog" style="z-index:1111">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Enviar correo electronico</h4>
            </div>
            <div class="modal-body cuerpoMail">
                <form action="#" method="post" id="formMail">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="para" id="Lblpara">PARA</label>
                                <input type="text" class="form-control input-sm" id="para" value="" name="para" placeholder="PARA" required>
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="cc" id="Lblcc">CC</label>
                                <input type="text" class="form-control input-sm" id="cc" value="" name="cc" placeholder="CC">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="cco" id="Lblcco">CCO</label>
                                <input type="text" class="form-control input-sm" id="cco" value="" name="cco" placeholder="CCO">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="asunto" id="Lblasunto">ASUNTO</label>
                                <input type="text" class="form-control input-sm" id="asunto" value="" name="asunto" placeholder="ASUNTO" required>
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="cuerpo" id="Lblcuerpo">CUERPO</label>
                                <textarea class="form-control input-sm" maxlength="160" name="cuerpo" id="cuerpo" value="" placeholder="MENSAJE" required></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""> </label>
                                <button title="Enviar" class="btn btn-primary btn-sm" id="sendMail">ENVIAR</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="cuentaMail" name="cuenta" value="">
                    <input type="hidden" id="stradjuntos" name="stradjuntos" value="">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade-in" id="modal_SendSms" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">ENVIAR MENSAJE DE TEXTO</h4>
            </div>
            <div class="modal-body cuerpoMail">
                <form action="#" method="post" id="formSms">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="destinatario" id="Lbldestinatario">DESTINATARIO</label>
                                <input type="text" class="form-control input-sm" id="destinatario" value="" name="destinatario" placeholder="DESTINATARIO">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="mensaje" id="Lblmensaje">MENSAJE</label>
                                <textarea class="form-control input-sm" maxlength="160" name="mensaje" id="mensaje" value="" placeholder="MENSAJE"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for=""> </label>
                                <button title="Enviar" class="btn btn-primary btn-sm" id="sendSms">ENVIAR</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="cuentaSms" name="cuenta" value="">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade-in" id="modal_SearchMail" data-backdrop="static" data-keyboard="false" role="dialog" style="overflow:auto">
    <div class="modal-dialog" style="width:90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Buscar Correo</h4>
            </div>
            <div class="modal-body cuerpoMail">
                <form id="formBusqueda">
                    <input type="hidden" id="cuentaSearchMail" name="cuenta" value="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">CAMPOS DE BUSQUEDA</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">FECHA INICIAL</label>
                                <input type="text" class="form-control input-sm fecha" name="fecha_inicial" id="fecha_inicial" value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">FECHA FINAL</label>
                                <input type="text" class="form-control input-sm fecha" name="fecha_final" id="fecha_final" value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">ASUNTO</label>
                                <input type="text" class="form-control input-sm" name="asunto" id="asunto" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">EL CUERPO CONTIENE</label>
                                <input type="text" class="form-control input-sm" name="cuerpo" id="cuerpo" value="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="">DE</label>
                                <input type="text" class="form-control input-sm" name="de" id="de" value="">
                            </div>
                        </div>
                        <div class="col-md-1 col-xs-1">
                            <div class="form-group">
                                <label for="adjuntos" id="">ADJUNTOS</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="adjuntos" id="adjuntos">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for=""> </label>
                                <button class="btn btn-primary form-control input-sm" id="searchMail">BUSCAR</button><span>Se mostrará un maximo de 20 correos</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="mails" style="width:90%;margin:auto"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function enviarMailDesdeBtn(mail, cuenta) {
        $("#modal_sendMail").modal('show');
        $("#para").val(mail);
        $("#cuentaMail").val(cuenta);
    }

    function enviarSmsDesdeBtn(destino, cuenta, campo=null, valorDefecto=null) {
        $("#mensaje").val('');
        $("#destinatario").val('');
        $("#modal_SendSms").modal('show');
        $("#cuentaSms").val(cuenta);
        $("#destinatario").val(destino);     
        if(valorDefecto!=null && valorDefecto!='' && valorDefecto!=0 && campo!=null && campo!='' && campo!=0){
            $.ajax({
                url: 'formularios/enviarSms_Mail_crud.php?textDefaultSms=si',
                type: 'POST',
                dataType: 'json',
                data: {campo:campo,valor:$("#"+valorDefecto).val()},
                cache: false,
                beforeSend: function() {
                    $.blockUI({
                        baseZ: 2000,
                        message: '<img src="assets/img/clock.gif" /> Espere por favor'
                    });
                },
                success: function(data) {
                    if (data.estado == 'ok') {
                        if(data.mensaje != null && data.mensaje != '' && data.mensaje !=0){
                            $("#mensaje").val(data.mensaje);
                            $("#mensaje").html(data.mensaje);
                            $("#mensaje").attr('readonly',true);
                        }else{
                            $("#mensaje").attr('readonly',false);
                        }
                    } else {
                        alertify.error(data.mensaje);
                        $("#mensaje").attr('readonly',false);
                    }
                },
                error: function() {
                    alertify.error("Ocurrio un error al cargar el mensaje predefinido");
                },
                complete: function() {
                    $.unblockUI();
                }
            });        
        }else{
            $("#mensaje").attr('readonly',false);
        }
    }

    function buscarMailDesdeBtn(cuenta) {
        $("#modal_SearchMail").modal('show');
        $("#cuentaSearchMail").val(cuenta);
    }

    function sendMail() {
        var form = new FormData($("#formMail")[0]);
        $.ajax({
            url: 'formularios/enviarSms_Mail_crud.php?sendMail=si',
            type: 'POST',
            dataType: 'json',
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $.blockUI({
                    baseZ: 2000,
                    message: '<img src="assets/img/clock.gif" /> Espere mientras se envia el correo',
                    css:{
                        'z-index':10500
                    }
                });
            },
            success: function(data) {
                if (data.strEstado_t == 'ok') {
                    alertify.success('correo enviado exitosamente');
                    $("#modal_sendMail").modal('hide');
                } else {
                    alertify.error(data.strMensaje_t);
                }
            },
            error: function() {
                alertify.error("Ocurrio un error al enviar el correo");
            },
            complete: function() {
                $.unblockUI();
            }
        });
    }

    function sendSms() {
        var form = new FormData($("#formSms")[0]);
        <?php 
            $agente=isset($_GET['idUSUARI']) ? $_GET['idUSUARI'] :false;
            if(!$agente){
                $agente=isset($_GET['usuario']) ? $_GET['usuario'] :0;
            }
        ?>
        form.append('agente','<?=$agente?>');
        $.ajax({
            url: 'formularios/enviarSms_Mail_crud.php?sendSms=si',
            type: 'POST',
            dataType: 'json',
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $.blockUI({
                    baseZ: 2000,
                    message: '<img src="assets/img/clock.gif" /> Espere mientras se envia el SMS'
                });
            },
            success: function(data) {
                if (data.strEstado_t == 'ok') {
                    alertify.success('SMS enviado exitosamente');
                    $("#modal_SendSms").modal('hide');
                } else {
                    alertify.error(data.strMensaje_t);
                }
            },
            error: function() {
                alertify.error("Ocurrio un error al enviar el SMS");
            },
            complete: function() {
                $.unblockUI();
            }
        });
    }

    function searchMail() {
        var form = new FormData($("#formBusqueda")[0]);
        $.ajax({
            url: 'formularios/enviarSms_Mail_crud.php?searchMail=si',
            type: 'POST',
            dataType: 'json',
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $.blockUI({
                    baseZ: 2000,
                    message: '<img src="assets/img/clock.gif" /> Espere mientras el sistema realiza la busqueda',
                    css:{
                        'z-index':10500
                    }                    
                });
            },
            success: function(data) {
                if (data.estado == 'ok') {
                    $("#mails").html(data.mensaje);
                } else {
                    alertify.error(data.mensaje);
                }
            },
            error: function() {
                alertify.error("Ocurrio un error al realizar la busqueda");
            },
            complete: function() {
                $.unblockUI();
            }
        });
    }
    

    $("#sendMail").on('click', function(e) {
        e.preventDefault();
        sendMail();
    });

    $("#sendSms").on('click', function(e) {
        e.preventDefault();
        sendSms();
    });

    $("#searchMail").on('click', function(e) {
        e.preventDefault();
        searchMail();
    });

    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Today",
        clear: "Clear",
        format: "yyyy-mm-dd",
        titleFormat: "yyyy-mm-dd",
        weekStart: 0
    };

    $("#fecha_inicial").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true,
        dateFormat: 'yyyy-mm-dd'
    });

    $("#fecha_final").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true,
        dateFormat: 'yyyy-mm-dd'
    });

</script>
