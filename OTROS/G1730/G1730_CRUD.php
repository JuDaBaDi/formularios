<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G1730/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G1730/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G1730/".$archivo);
                        $tamaño = filesize("/Dyalogo/tmp/G1730/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaño);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G1730/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1730_ConsInte__b, G1730_FechaInsercion , G1730_Usuario ,  G1730_CodigoMiembro  , G1730_PoblacionOrigen , G1730_EstadoDiligenciamiento ,  G1730_IdLlamada , G1730_C29075 as principal ,G1730_C29075,G1730_C29076,G1730_C29077,G1730_C29078,G1730_C29079,G1730_C29080,G1730_C29081,G1730_C29082,G1730_C30600,G1730_C30603,G1730_C30604,G1730_C29083,G1730_C29084,G1730_C29085 FROM '.$BaseDatos.'.G1730 WHERE G1730_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1730_C29075'] = explode(' ', $key->G1730_C29075)[0];

                $datos[$i]['G1730_C29076'] = $key->G1730_C29076;

                $datos[$i]['G1730_C29077'] = $key->G1730_C29077;

                $datos[$i]['G1730_C29078'] = $key->G1730_C29078;

                $datos[$i]['G1730_C29079'] = $key->G1730_C29079;

                $datos[$i]['G1730_C29080'] = $key->G1730_C29080;

                $datos[$i]['G1730_C29081'] = $key->G1730_C29081;

                $datos[$i]['G1730_C29082'] = $key->G1730_C29082;

                $datos[$i]['G1730_C30600'] = $key->G1730_C30600;

                $datos[$i]['G1730_C30603'] = $key->G1730_C30603;
  
                $hora = '';
                if(!is_null($key->G1730_C30604)){
                    $hora = explode(' ', $key->G1730_C30604)[1];
                }

                $datos[$i]['G1730_C30604'] = $hora;

                $datos[$i]['G1730_C29083'] = $key->G1730_C29083;

                $datos[$i]['G1730_C29084'] = $key->G1730_C29084;

                $datos[$i]['G1730_C29085'] = $key->G1730_C29085;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1730";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1730_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1730_ConsInte__b as id,  G1730_C29075 as camp1 , G1730_C29076 as camp2 
                     FROM ".$BaseDatos.".G1730  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1730_ConsInte__b as id,  G1730_C29075 as camp1 , G1730_C29076 as camp2  
                    FROM ".$BaseDatos.".G1730  JOIN ".$BaseDatos.".G1730_M".$_POST['muestra']." ON G1730_ConsInte__b = G1730_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1730_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1730_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1730_C29075 LIKE '%".$B."%' OR G1730_C29076 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1730_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1730");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1730_ConsInte__b, G1730_FechaInsercion , G1730_Usuario ,  G1730_CodigoMiembro  , G1730_PoblacionOrigen , G1730_EstadoDiligenciamiento ,  G1730_IdLlamada , G1730_C29075 as principal ,G1730_C29075,G1730_C29076,G1730_C29077,G1730_C29078,G1730_C29079,G1730_C29080,G1730_C29081,G1730_C29082,G1730_C30600,G1730_C30603,G1730_C30604,G1730_C29083,G1730_C29084, a.LISOPC_Nombre____b as G1730_C29085 FROM '.$BaseDatos.'.G1730 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1730_C29085';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1730_C30604)){
                    $hora_a = explode(' ', $fila->G1730_C30604)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1730_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1730_ConsInte__b , explode(' ', $fila->G1730_C29075)[0] , ($fila->G1730_C29076) , ($fila->G1730_C29077) , ($fila->G1730_C29078) , ($fila->G1730_C29079) , ($fila->G1730_C29080) , ($fila->G1730_C29081) , ($fila->G1730_C29082) , ($fila->G1730_C30600) , ($fila->G1730_C30603) , $hora_a , ($fila->G1730_C29083) , ($fila->G1730_C29084) , ($fila->G1730_C29085) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1730 WHERE G1730_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1730";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1730_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1730_ConsInte__b as id,  G1730_C29075 as camp1 , G1730_C29076 as camp2  FROM '.$BaseDatos.'.G1730 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1730_ConsInte__b as id,  G1730_C29075 as camp1 , G1730_C29076 as camp2  
                    FROM ".$BaseDatos.".G1730  JOIN ".$BaseDatos.".G1730_M".$_POST['muestra']." ON G1730_ConsInte__b = G1730_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1730_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1730_C29075 LIKE "%'.$B.'%" OR G1730_C29076 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1730_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1730 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1730(";
            $LsqlV = " VALUES ("; 
 
            $G1730_C29075 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1730_C29075"])){    
                if($_POST["G1730_C29075"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1730_C29075"]);
                    if(count($tieneHora) > 1){
                        $G1730_C29075 = "'".$_POST["G1730_C29075"]."'";
                    }else{
                        $G1730_C29075 = "'".str_replace(' ', '',$_POST["G1730_C29075"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1730_C29075 = ".$G1730_C29075;
                    $LsqlI .= $separador." G1730_C29075";
                    $LsqlV .= $separador.$G1730_C29075;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1730_C29076"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29076 = '".$_POST["G1730_C29076"]."'";
                $LsqlI .= $separador."G1730_C29076";
                $LsqlV .= $separador."'".$_POST["G1730_C29076"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29077"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29077 = '".$_POST["G1730_C29077"]."'";
                $LsqlI .= $separador."G1730_C29077";
                $LsqlV .= $separador."'".$_POST["G1730_C29077"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29078"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29078 = '".$_POST["G1730_C29078"]."'";
                $LsqlI .= $separador."G1730_C29078";
                $LsqlV .= $separador."'".$_POST["G1730_C29078"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29079"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29079 = '".$_POST["G1730_C29079"]."'";
                $LsqlI .= $separador."G1730_C29079";
                $LsqlV .= $separador."'".$_POST["G1730_C29079"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29080"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29080 = '".$_POST["G1730_C29080"]."'";
                $LsqlI .= $separador."G1730_C29080";
                $LsqlV .= $separador."'".$_POST["G1730_C29080"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29081"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29081 = '".$_POST["G1730_C29081"]."'";
                $LsqlI .= $separador."G1730_C29081";
                $LsqlV .= $separador."'".$_POST["G1730_C29081"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29082"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29082 = '".$_POST["G1730_C29082"]."'";
                $LsqlI .= $separador."G1730_C29082";
                $LsqlV .= $separador."'".$_POST["G1730_C29082"]."'";
                $validar = 1;
            }
             
  
            if (isset($_FILES["FG1730_C30600"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G1730/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G1730")){
                    mkdir("/Dyalogo/tmp/G1730", 0777);
                }
                if ($_FILES["FG1730_C30600"]["size"] != 0) {
                    $G1730_C30600 = $_FILES["FG1730_C30600"]["tmp_name"];
                    $nG1730_C30600 = $fechUp."_".$_FILES["FG1730_C30600"]["name"];
                    $rutaFinal = $destinoFile.$nG1730_C30600;
                    if (is_uploaded_file($G1730_C30600)) {
                        move_uploaded_file($G1730_C30600, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1730_C30600 = '".$nG1730_C30600."'";
                    $LsqlI .= $separador."G1730_C30600";
                    $LsqlV .= $separador."'".$nG1730_C30600."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG1730_C30603"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G1730/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G1730")){
                    mkdir("/Dyalogo/tmp/G1730", 0777);
                }
                if ($_FILES["FG1730_C30603"]["size"] != 0) {
                    $G1730_C30603 = $_FILES["FG1730_C30603"]["tmp_name"];
                    $nG1730_C30603 = $fechUp."_".$_FILES["FG1730_C30603"]["name"];
                    $rutaFinal = $destinoFile.$nG1730_C30603;
                    if (is_uploaded_file($G1730_C30603)) {
                        move_uploaded_file($G1730_C30603, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1730_C30603 = '".$nG1730_C30603."'";
                    $LsqlI .= $separador."G1730_C30603";
                    $LsqlV .= $separador."'".$nG1730_C30603."'";
                    $validar = 1;
                }
            }
            
  
            $G1730_C30604 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1730_C30604"])){   
                if($_POST["G1730_C30604"] != '' && $_POST["G1730_C30604"] != 'undefined' && $_POST["G1730_C30604"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1730_C30604 = "'".$fecha." ".str_replace(' ', '',$_POST["G1730_C30604"])."'";
                    $LsqlU .= $separador." G1730_C30604 = ".$G1730_C30604."";
                    $LsqlI .= $separador." G1730_C30604";
                    $LsqlV .= $separador.$G1730_C30604;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1730_C29083"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29083 = '".$_POST["G1730_C29083"]."'";
                $LsqlI .= $separador."G1730_C29083";
                $LsqlV .= $separador."'".$_POST["G1730_C29083"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29084"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29084 = '".$_POST["G1730_C29084"]."'";
                $LsqlI .= $separador."G1730_C29084";
                $LsqlV .= $separador."'".$_POST["G1730_C29084"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1730_C29085"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_C29085 = '".$_POST["G1730_C29085"]."'";
                $LsqlI .= $separador."G1730_C29085";
                $LsqlV .= $separador."'".$_POST["G1730_C29085"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1730_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1730_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1730_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1730_Usuario , G1730_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1730_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1730 WHERE G1730_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G1730 SET G1730_UltiGest__b =-14, G1730_GesMasImp_b =-14, G1730_TipoReintentoUG_b =0, G1730_TipoReintentoGMI_b =0, G1730_EstadoUG_b =-14, G1730_EstadoGMI_b =-14, G1730_CantidadIntentos =0, G1730_CantidadIntentosGMI_b =0 WHERE G1730_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
