<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2145_ConsInte__b, G2145_FechaInsercion , G2145_Usuario ,  G2145_CodigoMiembro  , G2145_PoblacionOrigen , G2145_EstadoDiligenciamiento ,  G2145_IdLlamada , G2145_C41248 as principal ,G2145_C41248,G2145_C41249,G2145_C41250,G2145_C41251,G2145_C41252,G2145_C41253,G2145_C41254,G2145_C41159,G2145_C41160,G2145_C41255,G2145_C41256,G2145_C41257,G2145_C41258,G2145_C41259,G2145_C41260,G2145_C41261,G2145_C41262,G2145_C41263,G2145_C41264,G2145_C41265,G2145_C41266,G2145_C41267,G2145_C41268,G2145_C49291,G2145_C41269,G2145_C41306,G2145_C41270,G2145_C41271,G2145_C41272,G2145_C41273,G2145_C41274,G2145_C41275,G2145_C41276,G2145_C41277,G2145_C41278,G2145_C41279,G2145_C41280,G2145_C41281,G2145_C41282,G2145_C41283,G2145_C41284,G2145_C41285,G2145_C41286,G2145_C41287,G2145_C41298,G2145_C41299,G2145_C41300,G2145_C41301,G2145_C41914,G2145_C41302,G2145_C41303,G2145_C41304,G2145_C41305,G2145_C41568,G2145_C41564,G2145_C41567,G2145_C41570,G2145_C41601,G2145_C41572,G2145_C49281,G2145_C49283,G2145_C41565,G2145_C41573,G2145_C41566,G2145_C41574,G2145_C41571,G2145_C41576,G2145_C41577,G2145_C41578,G2145_C41579,G2145_C41581,G2145_C41583,G2145_C49282,G2145_C41600,G2145_C52335, G2145_C52828 FROM '.$BaseDatos.'.G2145 WHERE G2145_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2145_C41248'] = $key->G2145_C41248;

                $datos[$i]['G2145_C41249'] = $key->G2145_C41249;

                $datos[$i]['G2145_C41250'] = $key->G2145_C41250;

                $datos[$i]['G2145_C41251'] = $key->G2145_C41251;

                $datos[$i]['G2145_C41252'] = $key->G2145_C41252;

                $datos[$i]['G2145_C41253'] = $key->G2145_C41253;

                $datos[$i]['G2145_C41254'] = $key->G2145_C41254;

                $datos[$i]['G2145_C41159'] = $key->G2145_C41159;

                $datos[$i]['G2145_C41160'] = $key->G2145_C41160;

                $datos[$i]['G2145_C41255'] = $key->G2145_C41255;

                $datos[$i]['G2145_C41256'] = $key->G2145_C41256;

                $datos[$i]['G2145_C41257'] = explode(' ', $key->G2145_C41257)[0];

                $datos[$i]['G2145_C41258'] = explode(' ', $key->G2145_C41258)[0];

                $datos[$i]['G2145_C41259'] = $key->G2145_C41259;

                $datos[$i]['G2145_C41260'] = $key->G2145_C41260;

                $datos[$i]['G2145_C41261'] = $key->G2145_C41261;

                $datos[$i]['G2145_C41262'] = $key->G2145_C41262;

                $datos[$i]['G2145_C41263'] = explode(' ', $key->G2145_C41263)[0];

                $datos[$i]['G2145_C41264'] = explode(' ', $key->G2145_C41264)[0];

                $datos[$i]['G2145_C41265'] = $key->G2145_C41265;

                $datos[$i]['G2145_C41266'] = $key->G2145_C41266;

                $datos[$i]['G2145_C41267'] = $key->G2145_C41267;

                $datos[$i]['G2145_C41268'] = explode(' ', $key->G2145_C41268)[0];

                $datos[$i]['G2145_C49291'] = $key->G2145_C49291;

                $datos[$i]['G2145_C41269'] = explode(' ', $key->G2145_C41269)[0];

                $datos[$i]['G2145_C52828'] = $key->G2145_C52828;

                $datos[$i]['G2145_C41306'] = $key->G2145_C41306;

                $datos[$i]['G2145_C41270'] = $key->G2145_C41270;

                $datos[$i]['G2145_C41271'] = $key->G2145_C41271;

                $datos[$i]['G2145_C41272'] = $key->G2145_C41272;

                $datos[$i]['G2145_C41273'] = $key->G2145_C41273;

                $datos[$i]['G2145_C41274'] = $key->G2145_C41274;

                $datos[$i]['G2145_C41275'] = $key->G2145_C41275;

                $datos[$i]['G2145_C41276'] = $key->G2145_C41276;

                $datos[$i]['G2145_C41277'] = $key->G2145_C41277;

                $datos[$i]['G2145_C41278'] = $key->G2145_C41278;

                $datos[$i]['G2145_C41279'] = $key->G2145_C41279;

                $datos[$i]['G2145_C41280'] = $key->G2145_C41280;

                $datos[$i]['G2145_C41281'] = $key->G2145_C41281;

                $datos[$i]['G2145_C41282'] = $key->G2145_C41282;

                $datos[$i]['G2145_C41283'] = $key->G2145_C41283;

                $datos[$i]['G2145_C41284'] = $key->G2145_C41284;

                $datos[$i]['G2145_C41285'] = $key->G2145_C41285;

                $datos[$i]['G2145_C41286'] = $key->G2145_C41286;

                $datos[$i]['G2145_C41287'] = $key->G2145_C41287;

                $datos[$i]['G2145_C41298'] = $key->G2145_C41298;

                $datos[$i]['G2145_C41299'] = $key->G2145_C41299;

                $datos[$i]['G2145_C41300'] = $key->G2145_C41300;

                $datos[$i]['G2145_C41301'] = $key->G2145_C41301;

                $datos[$i]['G2145_C41914'] = $key->G2145_C41914;

                $datos[$i]['G2145_C41302'] = $key->G2145_C41302;

                $datos[$i]['G2145_C41303'] = $key->G2145_C41303;

                $datos[$i]['G2145_C41304'] = $key->G2145_C41304;

                $datos[$i]['G2145_C41305'] = $key->G2145_C41305;

                $datos[$i]['G2145_C41568'] = $key->G2145_C41568;

                $datos[$i]['G2145_C41564'] = $key->G2145_C41564;

                $datos[$i]['G2145_C41567'] = $key->G2145_C41567;

                $datos[$i]['G2145_C41570'] = $key->G2145_C41570;

                $datos[$i]['G2145_C41601'] = $key->G2145_C41601;

                $datos[$i]['G2145_C41572'] = explode(' ', $key->G2145_C41572)[0];

                $datos[$i]['G2145_C49281'] = $key->G2145_C49281;

                $datos[$i]['G2145_C49283'] = $key->G2145_C49283;

                $datos[$i]['G2145_C41565'] = $key->G2145_C41565;

                $datos[$i]['G2145_C41573'] = $key->G2145_C41573;

                $datos[$i]['G2145_C41566'] = $key->G2145_C41566;

                $datos[$i]['G2145_C41574'] = $key->G2145_C41574;

                $datos[$i]['G2145_C41571'] = $key->G2145_C41571;

                $datos[$i]['G2145_C41576'] = $key->G2145_C41576;

                $datos[$i]['G2145_C41577'] = $key->G2145_C41577;

                $datos[$i]['G2145_C41578'] = explode(' ', $key->G2145_C41578)[0];

                $datos[$i]['G2145_C41579'] = $key->G2145_C41579;

                $datos[$i]['G2145_C41581'] = $key->G2145_C41581;

                $datos[$i]['G2145_C41583'] = $key->G2145_C41583;

                $datos[$i]['G2145_C49282'] = explode(' ', $key->G2145_C49282)[0];

                $datos[$i]['G2145_C41600'] = $key->G2145_C41600;

                $datos[$i]['G2145_C52335'] = $key->G2145_C52335;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2145";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1855_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2145_ConsInte__b as id,  G2145_C41248 as camp1 , G2145_C41249 as camp2 
                     FROM ".$BaseDatos.".G2145  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2145_ConsInte__b as id,  G2145_C41248 as camp1 , G2145_C41249 as camp2  
                    FROM ".$BaseDatos.".G2145  JOIN ".$BaseDatos.".G2145_M".$_POST['muestra']." ON G2145_ConsInte__b = G2145_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2145_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2145_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2145_C41248 LIKE '%".$B."%' OR G2145_C41249 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2145_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2145");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2145_ConsInte__b, G2145_FechaInsercion , G2145_Usuario ,  G2145_CodigoMiembro  , G2145_PoblacionOrigen , G2145_EstadoDiligenciamiento ,  G2145_IdLlamada , G2145_C41248 as principal ,G2145_C41248,G2145_C41249,G2145_C41250, a.LISOPC_Nombre____b as G2145_C41251,G2145_C41252,G2145_C41253, b.LISOPC_Nombre____b as G2145_C41254,G2145_C41159,G2145_C41160,G2145_C41255,G2145_C41256,G2145_C41257,G2145_C41258,G2145_C41259, c.LISOPC_Nombre____b as G2145_C41260,G2145_C41261,G2145_C41262,G2145_C41263,G2145_C41264,G2145_C41265, d.LISOPC_Nombre____b as G2145_C41266,G2145_C41267,G2145_C41268, e.LISOPC_Nombre____b as G2145_C49291,G2145_C41269,G2145_C41306, f.LISOPC_Nombre____b as G2145_C41270, g.LISOPC_Nombre____b as G2145_C41271,G2145_C41272, h.LISOPC_Nombre____b as G2145_C41273, i.LISOPC_Nombre____b as G2145_C41274, j.LISOPC_Nombre____b as G2145_C41275,G2145_C41276, k.LISOPC_Nombre____b as G2145_C41277, l.LISOPC_Nombre____b as G2145_C41278, m.LISOPC_Nombre____b as G2145_C41279,G2145_C41280, n.LISOPC_Nombre____b as G2145_C41281, o.LISOPC_Nombre____b as G2145_C41282, p.LISOPC_Nombre____b as G2145_C41283,G2145_C41284, q.LISOPC_Nombre____b as G2145_C41285,G2145_C41286,G2145_C41287, r.LISOPC_Nombre____b as G2145_C41298, s.LISOPC_Nombre____b as G2145_C41299,G2145_C41300,G2145_C41301,G2145_C41914, t.LISOPC_Nombre____b as G2145_C41302, u.LISOPC_Nombre____b as G2145_C41303,G2145_C41304, v.LISOPC_Nombre____b as G2145_C41305,G2145_C41568, w.LISOPC_Nombre____b as G2145_C41564,G2145_C41567,G2145_C41570, x.LISOPC_Nombre____b as G2145_C41601,G2145_C41572,G2145_C49281,G2145_C49283, y.LISOPC_Nombre____b as G2145_C41565, z.LISOPC_Nombre____b as G2145_C41573,G2145_C41566,G2145_C41574,G2145_C41571,G2145_C41576,G2145_C41577,G2145_C41578,G2145_C41579,G2145_C41581,G2145_C41583,G2145_C49282,G2145_C41600 FROM '.$BaseDatos.'.G2145 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2145_C41251 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2145_C41254 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2145_C41260 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2145_C41266 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2145_C49291 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2145_C41270 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2145_C41271 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2145_C41273 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2145_C41274 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2145_C41275 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G2145_C41277 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G2145_C41278 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G2145_C41279 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G2145_C41281 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G2145_C41282 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G2145_C41283 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as q ON q.LISOPC_ConsInte__b =  G2145_C41285 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as r ON r.LISOPC_ConsInte__b =  G2145_C41298 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as s ON s.LISOPC_ConsInte__b =  G2145_C41299 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as t ON t.LISOPC_ConsInte__b =  G2145_C41302 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as u ON u.LISOPC_ConsInte__b =  G2145_C41303 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as v ON v.LISOPC_ConsInte__b =  G2145_C41305 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as w ON w.LISOPC_ConsInte__b =  G2145_C41564 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as x ON x.LISOPC_ConsInte__b =  G2145_C41601 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as y ON y.LISOPC_ConsInte__b =  G2145_C41565 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as z ON z.LISOPC_ConsInte__b =  G2145_C41573';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2145_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2145_ConsInte__b , ($fila->G2145_C41248) , ($fila->G2145_C41249) , ($fila->G2145_C41250) , ($fila->G2145_C41251) , ($fila->G2145_C41252) , ($fila->G2145_C41253) , ($fila->G2145_C41254) , ($fila->G2145_C41159) , ($fila->G2145_C41160) , ($fila->G2145_C41255) , ($fila->G2145_C41256) , explode(' ', $fila->G2145_C41257)[0] , explode(' ', $fila->G2145_C41258)[0] , ($fila->G2145_C41259) , ($fila->G2145_C41260) , ($fila->G2145_C41261) , ($fila->G2145_C41262) , explode(' ', $fila->G2145_C41263)[0] , explode(' ', $fila->G2145_C41264)[0] , ($fila->G2145_C41265) , ($fila->G2145_C41266) , ($fila->G2145_C41267) , explode(' ', $fila->G2145_C41268)[0] , ($fila->G2145_C49291) , explode(' ', $fila->G2145_C41269)[0] , ($fila->G2145_C41306) , ($fila->G2145_C41270) , ($fila->G2145_C41271) , ($fila->G2145_C41272) , ($fila->G2145_C41273) , ($fila->G2145_C41274) , ($fila->G2145_C41275) , ($fila->G2145_C41276) , ($fila->G2145_C41277) , ($fila->G2145_C41278) , ($fila->G2145_C41279) , ($fila->G2145_C41280) , ($fila->G2145_C41281) , ($fila->G2145_C41282) , ($fila->G2145_C41283) , ($fila->G2145_C41284) , ($fila->G2145_C41285) , ($fila->G2145_C41286) , ($fila->G2145_C41287) , ($fila->G2145_C41298) , ($fila->G2145_C41299) , ($fila->G2145_C41300) , ($fila->G2145_C41301) , ($fila->G2145_C41914) , ($fila->G2145_C41302) , ($fila->G2145_C41303) , ($fila->G2145_C41304) , ($fila->G2145_C41305) , ($fila->G2145_C41568) , ($fila->G2145_C41564) , ($fila->G2145_C41567) , ($fila->G2145_C41570) , ($fila->G2145_C41601) , explode(' ', $fila->G2145_C41572)[0] , ($fila->G2145_C49281) , ($fila->G2145_C49283) , ($fila->G2145_C41565) , ($fila->G2145_C41573) , ($fila->G2145_C41566) , ($fila->G2145_C41574) , ($fila->G2145_C41571) , ($fila->G2145_C41576) , ($fila->G2145_C41577) , explode(' ', $fila->G2145_C41578)[0] , ($fila->G2145_C41579) , ($fila->G2145_C41581) , ($fila->G2145_C41583) , explode(' ', $fila->G2145_C49282)[0] , ($fila->G2145_C41600) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2145 WHERE G2145_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2145";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1855_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }

            }else{
                $strRegProp_t = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2145_ConsInte__b as id,  G2145_C41248 as camp1 , G2145_C41249 as camp2  FROM '.$BaseDatos.'.G2145 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2145_ConsInte__b as id,  G2145_C41248 as camp1 , G2145_C41249 as camp2  
                    FROM ".$BaseDatos.".G2145  JOIN ".$BaseDatos.".G2145_M".$_POST['muestra']." ON G2145_ConsInte__b = G2145_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2145_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2145_C41248 LIKE "%'.$B.'%" OR G2145_C41249 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2145_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2145 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2145(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2145_C41248"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41248 = '".$_POST["G2145_C41248"]."'";
                $LsqlI .= $separador."G2145_C41248";
                $LsqlV .= $separador."'".$_POST["G2145_C41248"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41249"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41249 = '".$_POST["G2145_C41249"]."'";
                $LsqlI .= $separador."G2145_C41249";
                $LsqlV .= $separador."'".$_POST["G2145_C41249"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41250"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41250 = '".$_POST["G2145_C41250"]."'";
                $LsqlI .= $separador."G2145_C41250";
                $LsqlV .= $separador."'".$_POST["G2145_C41250"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41251"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41251 = '".$_POST["G2145_C41251"]."'";
                $LsqlI .= $separador."G2145_C41251";
                $LsqlV .= $separador."'".$_POST["G2145_C41251"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41252"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41252 = '".$_POST["G2145_C41252"]."'";
                $LsqlI .= $separador."G2145_C41252";
                $LsqlV .= $separador."'".$_POST["G2145_C41252"]."'";
                $validar = 1;
            }

            if(isset($_POST["G2145_C52335"])){
                if ($_POST["G2145_C52335"] != "") {
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2145_C52335 = '".$_POST["G2145_C52335"]."'";
                    $LsqlI .= $separador."G2145_C52335";
                    $LsqlV .= $separador."'".$_POST["G2145_C52335"]."'";
                    $validar = 1;
                }
            }
             
  
            if(isset($_POST["G2145_C41253"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41253 = '".$_POST["G2145_C41253"]."'";
                $LsqlI .= $separador."G2145_C41253";
                $LsqlV .= $separador."'".$_POST["G2145_C41253"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41254"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41254 = '".$_POST["G2145_C41254"]."'";
                $LsqlI .= $separador."G2145_C41254";
                $LsqlV .= $separador."'".$_POST["G2145_C41254"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41159 = '".$_POST["G2145_C41159"]."'";
                $LsqlI .= $separador."G2145_C41159";
                $LsqlV .= $separador."'".$_POST["G2145_C41159"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41160"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41160 = '".$_POST["G2145_C41160"]."'";
                $LsqlI .= $separador."G2145_C41160";
                $LsqlV .= $separador."'".$_POST["G2145_C41160"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41255"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41255 = '".$_POST["G2145_C41255"]."'";
                $LsqlI .= $separador."G2145_C41255";
                $LsqlV .= $separador."'".$_POST["G2145_C41255"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41256"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41256 = '".$_POST["G2145_C41256"]."'";
                $LsqlI .= $separador."G2145_C41256";
                $LsqlV .= $separador."'".$_POST["G2145_C41256"]."'";
                $validar = 1;
            }
             
 
            $G2145_C41257 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41257"])){    
                if($_POST["G2145_C41257"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41257"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41257 = "'".$_POST["G2145_C41257"]."'";
                    }else{
                        $G2145_C41257 = "'".str_replace(' ', '',$_POST["G2145_C41257"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41257 = ".$G2145_C41257;
                    $LsqlI .= $separador." G2145_C41257";
                    $LsqlV .= $separador.$G2145_C41257;
                    $validar = 1;
                }
            }
 
            $G2145_C41258 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41258"])){    
                if($_POST["G2145_C41258"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41258"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41258 = "'".$_POST["G2145_C41258"]."'";
                    }else{
                        $G2145_C41258 = "'".str_replace(' ', '',$_POST["G2145_C41258"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41258 = ".$G2145_C41258;
                    $LsqlI .= $separador." G2145_C41258";
                    $LsqlV .= $separador.$G2145_C41258;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C41259"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41259 = '".$_POST["G2145_C41259"]."'";
                $LsqlI .= $separador."G2145_C41259";
                $LsqlV .= $separador."'".$_POST["G2145_C41259"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41260"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41260 = '".$_POST["G2145_C41260"]."'";
                $LsqlI .= $separador."G2145_C41260";
                $LsqlV .= $separador."'".$_POST["G2145_C41260"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41261"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41261 = '".$_POST["G2145_C41261"]."'";
                $LsqlI .= $separador."G2145_C41261";
                $LsqlV .= $separador."'".$_POST["G2145_C41261"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41262"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41262 = '".$_POST["G2145_C41262"]."'";
                $LsqlI .= $separador."G2145_C41262";
                $LsqlV .= $separador."'".$_POST["G2145_C41262"]."'";
                $validar = 1;
            }
             
 
            $G2145_C41263 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41263"])){    
                if($_POST["G2145_C41263"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41263"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41263 = "'".$_POST["G2145_C41263"]."'";
                    }else{
                        $G2145_C41263 = "'".str_replace(' ', '',$_POST["G2145_C41263"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41263 = ".$G2145_C41263;
                    $LsqlI .= $separador." G2145_C41263";
                    $LsqlV .= $separador.$G2145_C41263;
                    $validar = 1;
                }
            }
 
            $G2145_C41264 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41264"])){    
                if($_POST["G2145_C41264"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41264"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41264 = "'".$_POST["G2145_C41264"]."'";
                    }else{
                        $G2145_C41264 = "'".str_replace(' ', '',$_POST["G2145_C41264"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41264 = ".$G2145_C41264;
                    $LsqlI .= $separador." G2145_C41264";
                    $LsqlV .= $separador.$G2145_C41264;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C41265"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41265 = '".$_POST["G2145_C41265"]."'";
                $LsqlI .= $separador."G2145_C41265";
                $LsqlV .= $separador."'".$_POST["G2145_C41265"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41266"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41266 = '".$_POST["G2145_C41266"]."'";
                $LsqlI .= $separador."G2145_C41266";
                $LsqlV .= $separador."'".$_POST["G2145_C41266"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41267"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41267 = '".$_POST["G2145_C41267"]."'";
                $LsqlI .= $separador."G2145_C41267";
                $LsqlV .= $separador."'".$_POST["G2145_C41267"]."'";
                $validar = 1;
            }
             
 
            $G2145_C41268 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41268"])){    
                if($_POST["G2145_C41268"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41268"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41268 = "'".$_POST["G2145_C41268"]."'";
                    }else{
                        $G2145_C41268 = "'".str_replace(' ', '',$_POST["G2145_C41268"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41268 = ".$G2145_C41268;
                    $LsqlI .= $separador." G2145_C41268";
                    $LsqlV .= $separador.$G2145_C41268;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C49291"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C49291 = '".$_POST["G2145_C49291"]."'";
                $LsqlI .= $separador."G2145_C49291";
                $LsqlV .= $separador."'".$_POST["G2145_C49291"]."'";
                $validar = 1;
            }
             
 
            $G2145_C41269 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41269"])){    
                if($_POST["G2145_C41269"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41269"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41269 = "'".$_POST["G2145_C41269"]."'";
                    }else{
                        $G2145_C41269 = "'".str_replace(' ', '',$_POST["G2145_C41269"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41269 = ".$G2145_C41269;
                    $LsqlI .= $separador." G2145_C41269";
                    $LsqlV .= $separador.$G2145_C41269;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C41306"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41306 = '".$_POST["G2145_C41306"]."'";
                $LsqlI .= $separador."G2145_C41306";
                $LsqlV .= $separador."'".$_POST["G2145_C41306"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41270"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41270 = '".$_POST["G2145_C41270"]."'";
                $LsqlI .= $separador."G2145_C41270";
                $LsqlV .= $separador."'".$_POST["G2145_C41270"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41271"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41271 = '".$_POST["G2145_C41271"]."'";
                $LsqlI .= $separador."G2145_C41271";
                $LsqlV .= $separador."'".$_POST["G2145_C41271"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41272"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41272 = '".$_POST["G2145_C41272"]."'";
                $LsqlI .= $separador."G2145_C41272";
                $LsqlV .= $separador."'".$_POST["G2145_C41272"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41273"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41273 = '".$_POST["G2145_C41273"]."'";
                $LsqlI .= $separador."G2145_C41273";
                $LsqlV .= $separador."'".$_POST["G2145_C41273"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41274"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41274 = '".$_POST["G2145_C41274"]."'";
                $LsqlI .= $separador."G2145_C41274";
                $LsqlV .= $separador."'".$_POST["G2145_C41274"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41275"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41275 = '".$_POST["G2145_C41275"]."'";
                $LsqlI .= $separador."G2145_C41275";
                $LsqlV .= $separador."'".$_POST["G2145_C41275"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41276"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41276 = '".$_POST["G2145_C41276"]."'";
                $LsqlI .= $separador."G2145_C41276";
                $LsqlV .= $separador."'".$_POST["G2145_C41276"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41277"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41277 = '".$_POST["G2145_C41277"]."'";
                $LsqlI .= $separador."G2145_C41277";
                $LsqlV .= $separador."'".$_POST["G2145_C41277"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41278"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41278 = '".$_POST["G2145_C41278"]."'";
                $LsqlI .= $separador."G2145_C41278";
                $LsqlV .= $separador."'".$_POST["G2145_C41278"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41279"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41279 = '".$_POST["G2145_C41279"]."'";
                $LsqlI .= $separador."G2145_C41279";
                $LsqlV .= $separador."'".$_POST["G2145_C41279"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41280"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41280 = '".$_POST["G2145_C41280"]."'";
                $LsqlI .= $separador."G2145_C41280";
                $LsqlV .= $separador."'".$_POST["G2145_C41280"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41281"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41281 = '".$_POST["G2145_C41281"]."'";
                $LsqlI .= $separador."G2145_C41281";
                $LsqlV .= $separador."'".$_POST["G2145_C41281"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41282"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41282 = '".$_POST["G2145_C41282"]."'";
                $LsqlI .= $separador."G2145_C41282";
                $LsqlV .= $separador."'".$_POST["G2145_C41282"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41283"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41283 = '".$_POST["G2145_C41283"]."'";
                $LsqlI .= $separador."G2145_C41283";
                $LsqlV .= $separador."'".$_POST["G2145_C41283"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41284"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41284 = '".$_POST["G2145_C41284"]."'";
                $LsqlI .= $separador."G2145_C41284";
                $LsqlV .= $separador."'".$_POST["G2145_C41284"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41285"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41285 = '".$_POST["G2145_C41285"]."'";
                $LsqlI .= $separador."G2145_C41285";
                $LsqlV .= $separador."'".$_POST["G2145_C41285"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41286"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41286 = '".$_POST["G2145_C41286"]."'";
                $LsqlI .= $separador."G2145_C41286";
                $LsqlV .= $separador."'".$_POST["G2145_C41286"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41287"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41287 = '".$_POST["G2145_C41287"]."'";
                $LsqlI .= $separador."G2145_C41287";
                $LsqlV .= $separador."'".$_POST["G2145_C41287"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41298"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41298 = '".$_POST["G2145_C41298"]."'";
                $LsqlI .= $separador."G2145_C41298";
                $LsqlV .= $separador."'".$_POST["G2145_C41298"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41299"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41299 = '".$_POST["G2145_C41299"]."'";
                $LsqlI .= $separador."G2145_C41299";
                $LsqlV .= $separador."'".$_POST["G2145_C41299"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41300"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41300 = '".$_POST["G2145_C41300"]."'";
                $LsqlI .= $separador."G2145_C41300";
                $LsqlV .= $separador."'".$_POST["G2145_C41300"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41301"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41301 = '".$_POST["G2145_C41301"]."'";
                $LsqlI .= $separador."G2145_C41301";
                $LsqlV .= $separador."'".$_POST["G2145_C41301"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41914"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41914 = '".$_POST["G2145_C41914"]."'";
                $LsqlI .= $separador."G2145_C41914";
                $LsqlV .= $separador."'".$_POST["G2145_C41914"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41302"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41302 = '".$_POST["G2145_C41302"]."'";
                $LsqlI .= $separador."G2145_C41302";
                $LsqlV .= $separador."'".$_POST["G2145_C41302"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41303"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41303 = '".$_POST["G2145_C41303"]."'";
                $LsqlI .= $separador."G2145_C41303";
                $LsqlV .= $separador."'".$_POST["G2145_C41303"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41304"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41304 = '".$_POST["G2145_C41304"]."'";
                $LsqlI .= $separador."G2145_C41304";
                $LsqlV .= $separador."'".$_POST["G2145_C41304"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41305"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41305 = '".$_POST["G2145_C41305"]."'";
                $LsqlI .= $separador."G2145_C41305";
                $LsqlV .= $separador."'".$_POST["G2145_C41305"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41563"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41563 = '".$_POST["G2145_C41563"]."'";
                $LsqlI .= $separador."G2145_C41563";
                $LsqlV .= $separador."'".$_POST["G2145_C41563"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41568"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41568 = '".$_POST["G2145_C41568"]."'";
                $LsqlI .= $separador."G2145_C41568";
                $LsqlV .= $separador."'".$_POST["G2145_C41568"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41564"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41564 = '".$_POST["G2145_C41564"]."'";
                $LsqlI .= $separador."G2145_C41564";
                $LsqlV .= $separador."'".$_POST["G2145_C41564"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41567"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41567 = '".$_POST["G2145_C41567"]."'";
                $LsqlI .= $separador."G2145_C41567";
                $LsqlV .= $separador."'".$_POST["G2145_C41567"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41570"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41570 = '".$_POST["G2145_C41570"]."'";
                $LsqlI .= $separador."G2145_C41570";
                $LsqlV .= $separador."'".$_POST["G2145_C41570"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41601"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41601 = '".$_POST["G2145_C41601"]."'";
                $LsqlI .= $separador."G2145_C41601";
                $LsqlV .= $separador."'".$_POST["G2145_C41601"]."'";
                $validar = 1;
            }
             
 
            $G2145_C41572 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41572"])){    
                if($_POST["G2145_C41572"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41572"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41572 = "'".$_POST["G2145_C41572"]."'";
                    }else{
                        $G2145_C41572 = "'".str_replace(' ', '',$_POST["G2145_C41572"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41572 = ".$G2145_C41572;
                    $LsqlI .= $separador." G2145_C41572";
                    $LsqlV .= $separador.$G2145_C41572;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C49281"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C49281 = '".$_POST["G2145_C49281"]."'";
                $LsqlI .= $separador."G2145_C49281";
                $LsqlV .= $separador."'".$_POST["G2145_C49281"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C49283"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C49283 = '".$_POST["G2145_C49283"]."'";
                $LsqlI .= $separador."G2145_C49283";
                $LsqlV .= $separador."'".$_POST["G2145_C49283"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41565"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41565 = '".$_POST["G2145_C41565"]."'";
                $LsqlI .= $separador."G2145_C41565";
                $LsqlV .= $separador."'".$_POST["G2145_C41565"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41573"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41573 = '".$_POST["G2145_C41573"]."'";
                $LsqlI .= $separador."G2145_C41573";
                $LsqlV .= $separador."'".$_POST["G2145_C41573"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41566"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41566 = '".$_POST["G2145_C41566"]."'";
                $LsqlI .= $separador."G2145_C41566";
                $LsqlV .= $separador."'".$_POST["G2145_C41566"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41574"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41574 = '".$_POST["G2145_C41574"]."'";
                $LsqlI .= $separador."G2145_C41574";
                $LsqlV .= $separador."'".$_POST["G2145_C41574"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41571"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41571 = '".$_POST["G2145_C41571"]."'";
                $LsqlI .= $separador."G2145_C41571";
                $LsqlV .= $separador."'".$_POST["G2145_C41571"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41576"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41576 = '".$_POST["G2145_C41576"]."'";
                $LsqlI .= $separador."G2145_C41576";
                $LsqlV .= $separador."'".$_POST["G2145_C41576"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41577"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41577 = '".$_POST["G2145_C41577"]."'";
                $LsqlI .= $separador."G2145_C41577";
                $LsqlV .= $separador."'".$_POST["G2145_C41577"]."'";
                $validar = 1;
            }
             
 
            $G2145_C41578 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C41578"])){    
                if($_POST["G2145_C41578"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C41578"]);
                    if(count($tieneHora) > 1){
                        $G2145_C41578 = "'".$_POST["G2145_C41578"]."'";
                    }else{
                        $G2145_C41578 = "'".str_replace(' ', '',$_POST["G2145_C41578"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C41578 = ".$G2145_C41578;
                    $LsqlI .= $separador." G2145_C41578";
                    $LsqlV .= $separador.$G2145_C41578;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C41579"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41579 = '".$_POST["G2145_C41579"]."'";
                $LsqlI .= $separador."G2145_C41579";
                $LsqlV .= $separador."'".$_POST["G2145_C41579"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41581"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41581 = '".$_POST["G2145_C41581"]."'";
                $LsqlI .= $separador."G2145_C41581";
                $LsqlV .= $separador."'".$_POST["G2145_C41581"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2145_C41583"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41583 = '".$_POST["G2145_C41583"]."'";
                $LsqlI .= $separador."G2145_C41583";
                $LsqlV .= $separador."'".$_POST["G2145_C41583"]."'";
                $validar = 1;
            }
             
 
            $G2145_C49282 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2145_C49282"])){    
                if($_POST["G2145_C49282"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2145_C49282"]);
                    if(count($tieneHora) > 1){
                        $G2145_C49282 = "'".$_POST["G2145_C49282"]."'";
                    }else{
                        $G2145_C49282 = "'".str_replace(' ', '',$_POST["G2145_C49282"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2145_C49282 = ".$G2145_C49282;
                    $LsqlI .= $separador." G2145_C49282";
                    $LsqlV .= $separador.$G2145_C49282;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2145_C41600"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_C41600 = '".$_POST["G2145_C41600"]."'";
                $LsqlI .= $separador."G2145_C41600";
                $LsqlV .= $separador."'".$_POST["G2145_C41600"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2145_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2145_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2145_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2145_Usuario , G2145_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2145_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2145 WHERE G2145_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2145 SET G2145_UltiGest__b =-14, G2145_GesMasImp_b =-14, G2145_TipoReintentoUG_b =0, G2145_TipoReintentoGMI_b =0, G2145_EstadoUG_b =-14, G2145_EstadoGMI_b =-14, G2145_CantidadIntentos =0, G2145_CantidadIntentosGMI_b =0 WHERE G2145_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
