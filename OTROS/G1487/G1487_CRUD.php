<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1487_ConsInte__b, G1487_FechaInsercion , G1487_Usuario ,  G1487_CodigoMiembro  , G1487_PoblacionOrigen , G1487_EstadoDiligenciamiento ,  G1487_IdLlamada , G1487_C26585 as principal ,G1487_C26583,G1487_C26584,G1487_C26585,G1487_C26586,G1487_C26587,G1487_C26588,G1487_C26589,G1487_C26590,G1487_C26591,G1487_C34080,G1487_C34634,G1487_C26592,G1487_C26593,G1487_C26594,G1487_C26595,G1487_C26596,G1487_C26597,G1487_C26598,G1487_C26599,G1487_C26600 FROM '.$BaseDatos.'.G1487 WHERE G1487_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1487_C26583'] = $key->G1487_C26583;

                $datos[$i]['G1487_C26584'] = $key->G1487_C26584;

                $datos[$i]['G1487_C26585'] = $key->G1487_C26585;

                $datos[$i]['G1487_C26586'] = $key->G1487_C26586;

                $datos[$i]['G1487_C26587'] = $key->G1487_C26587;

                $datos[$i]['G1487_C26588'] = $key->G1487_C26588;

                $datos[$i]['G1487_C26589'] = $key->G1487_C26589;

                $datos[$i]['G1487_C26590'] = $key->G1487_C26590;

                $datos[$i]['G1487_C26591'] = $key->G1487_C26591;

                $datos[$i]['G1487_C34080'] = explode(' ', $key->G1487_C34080)[0];

                $datos[$i]['G1487_C34634'] = $key->G1487_C34634;

                $datos[$i]['G1487_C26592'] = $key->G1487_C26592;

                $datos[$i]['G1487_C26593'] = $key->G1487_C26593;

                $datos[$i]['G1487_C26594'] = explode(' ', $key->G1487_C26594)[0];
  
                $hora = '';
                if(!is_null($key->G1487_C26595)){
                    $hora = explode(' ', $key->G1487_C26595)[1];
                }

                $datos[$i]['G1487_C26595'] = $hora;

                $datos[$i]['G1487_C26596'] = $key->G1487_C26596;

                $datos[$i]['G1487_C26597'] = $key->G1487_C26597;

                $datos[$i]['G1487_C26598'] = $key->G1487_C26598;

                $datos[$i]['G1487_C26599'] = $key->G1487_C26599;

                $datos[$i]['G1487_C26600'] = $key->G1487_C26600;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1487";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1487_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1487_ConsInte__b as id,  G1487_C26584 as camp2 , G1487_C26585 as camp1 
                     FROM ".$BaseDatos.".G1487  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1487_ConsInte__b as id,  G1487_C26584 as camp2 , G1487_C26585 as camp1  
                    FROM ".$BaseDatos.".G1487  JOIN ".$BaseDatos.".G1487_M".$_POST['muestra']." ON G1487_ConsInte__b = G1487_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1487_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1487_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1487_C26584 LIKE '%".$B."%' OR G1487_C26585 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1487_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1487");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1487_ConsInte__b, G1487_FechaInsercion , G1487_Usuario ,  G1487_CodigoMiembro  , G1487_PoblacionOrigen , G1487_EstadoDiligenciamiento ,  G1487_IdLlamada , G1487_C26585 as principal , a.LISOPC_Nombre____b as G1487_C26583,G1487_C26584,G1487_C26585,G1487_C26586,G1487_C26587, b.LISOPC_Nombre____b as G1487_C26588,G1487_C26589,G1487_C26590, c.LISOPC_Nombre____b as G1487_C26591,G1487_C34080,G1487_C34634, d.LISOPC_Nombre____b as G1487_C26592, e.LISOPC_Nombre____b as G1487_C26593,G1487_C26594,G1487_C26595,G1487_C26596,G1487_C26597,G1487_C26598,G1487_C26599,G1487_C26600 FROM '.$BaseDatos.'.G1487 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1487_C26583 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1487_C26588 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1487_C26591 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1487_C26592 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1487_C26593';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1487_C26595)){
                    $hora_a = explode(' ', $fila->G1487_C26595)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1487_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1487_ConsInte__b , ($fila->G1487_C26583) , ($fila->G1487_C26584) , ($fila->G1487_C26585) , ($fila->G1487_C26586) , ($fila->G1487_C26587) , ($fila->G1487_C26588) , ($fila->G1487_C26589) , ($fila->G1487_C26590) , ($fila->G1487_C26591) , explode(' ', $fila->G1487_C34080)[0] , ($fila->G1487_C34634) , ($fila->G1487_C26592) , ($fila->G1487_C26593) , explode(' ', $fila->G1487_C26594)[0] , $hora_a , ($fila->G1487_C26596) , ($fila->G1487_C26597) , ($fila->G1487_C26598) , ($fila->G1487_C26599) , ($fila->G1487_C26600) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1487 WHERE G1487_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1487";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1487_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1487_ConsInte__b as id,  G1487_C26584 as camp2 , G1487_C26585 as camp1  FROM '.$BaseDatos.'.G1487 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1487_ConsInte__b as id,  G1487_C26584 as camp2 , G1487_C26585 as camp1  
                    FROM ".$BaseDatos.".G1487  JOIN ".$BaseDatos.".G1487_M".$_POST['muestra']." ON G1487_ConsInte__b = G1487_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1487_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1487_C26584 LIKE "%'.$B.'%" OR G1487_C26585 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1487_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1487 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1487(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1487_C26583"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26583 = '".$_POST["G1487_C26583"]."'";
                $LsqlI .= $separador."G1487_C26583";
                $LsqlV .= $separador."'".$_POST["G1487_C26583"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26584"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26584 = '".$_POST["G1487_C26584"]."'";
                $LsqlI .= $separador."G1487_C26584";
                $LsqlV .= $separador."'".$_POST["G1487_C26584"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26585"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26585 = '".$_POST["G1487_C26585"]."'";
                $LsqlI .= $separador."G1487_C26585";
                $LsqlV .= $separador."'".$_POST["G1487_C26585"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26586"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26586 = '".$_POST["G1487_C26586"]."'";
                $LsqlI .= $separador."G1487_C26586";
                $LsqlV .= $separador."'".$_POST["G1487_C26586"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26587"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26587 = '".$_POST["G1487_C26587"]."'";
                $LsqlI .= $separador."G1487_C26587";
                $LsqlV .= $separador."'".$_POST["G1487_C26587"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26588"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26588 = '".$_POST["G1487_C26588"]."'";
                $LsqlI .= $separador."G1487_C26588";
                $LsqlV .= $separador."'".$_POST["G1487_C26588"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26589"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26589 = '".$_POST["G1487_C26589"]."'";
                $LsqlI .= $separador."G1487_C26589";
                $LsqlV .= $separador."'".$_POST["G1487_C26589"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26590 = '".$_POST["G1487_C26590"]."'";
                $LsqlI .= $separador."G1487_C26590";
                $LsqlV .= $separador."'".$_POST["G1487_C26590"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26591"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26591 = '".$_POST["G1487_C26591"]."'";
                $LsqlI .= $separador."G1487_C26591";
                $LsqlV .= $separador."'".$_POST["G1487_C26591"]."'";
                $validar = 1;
            }
             
 
            $G1487_C34080 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1487_C34080"])){    
                if($_POST["G1487_C34080"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1487_C34080"]);
                    if(count($tieneHora) > 1){
                        $G1487_C34080 = "'".$_POST["G1487_C34080"]."'";
                    }else{
                        $G1487_C34080 = "'".str_replace(' ', '',$_POST["G1487_C34080"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1487_C34080 = ".$G1487_C34080;
                    $LsqlI .= $separador." G1487_C34080";
                    $LsqlV .= $separador.$G1487_C34080;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1487_C34634"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C34634 = '".$_POST["G1487_C34634"]."'";
                $LsqlI .= $separador."G1487_C34634";
                $LsqlV .= $separador."'".$_POST["G1487_C34634"]."'";
                $validar = 1;
            }
             
 
            $G1487_C26592 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1487_C26592 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1487_C26592 = ".$G1487_C26592;
                    $LsqlI .= $separador." G1487_C26592";
                    $LsqlV .= $separador.$G1487_C26592;
                    $validar = 1;

                    
                }
            }
 
            $G1487_C26593 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1487_C26593 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1487_C26593 = ".$G1487_C26593;
                    $LsqlI .= $separador." G1487_C26593";
                    $LsqlV .= $separador.$G1487_C26593;
                    $validar = 1;
                }
            }
 
            $G1487_C26594 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1487_C26594 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1487_C26594 = ".$G1487_C26594;
                    $LsqlI .= $separador." G1487_C26594";
                    $LsqlV .= $separador.$G1487_C26594;
                    $validar = 1;
                }
            }
 
            $G1487_C26595 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1487_C26595 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1487_C26595 = ".$G1487_C26595;
                    $LsqlI .= $separador." G1487_C26595";
                    $LsqlV .= $separador.$G1487_C26595;
                    $validar = 1;
                }
            }
 
            $G1487_C26596 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1487_C26596 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1487_C26596 = ".$G1487_C26596;
                    $LsqlI .= $separador." G1487_C26596";
                    $LsqlV .= $separador.$G1487_C26596;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1487_C26597"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26597 = '".$_POST["G1487_C26597"]."'";
                $LsqlI .= $separador."G1487_C26597";
                $LsqlV .= $separador."'".$_POST["G1487_C26597"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26598"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26598 = '".$_POST["G1487_C26598"]."'";
                $LsqlI .= $separador."G1487_C26598";
                $LsqlV .= $separador."'".$_POST["G1487_C26598"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26599"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26599 = '".$_POST["G1487_C26599"]."'";
                $LsqlI .= $separador."G1487_C26599";
                $LsqlV .= $separador."'".$_POST["G1487_C26599"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26600"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26600 = '".$_POST["G1487_C26600"]."'";
                $LsqlI .= $separador."G1487_C26600";
                $LsqlV .= $separador."'".$_POST["G1487_C26600"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26601"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26601 = '".$_POST["G1487_C26601"]."'";
                $LsqlI .= $separador."G1487_C26601";
                $LsqlV .= $separador."'".$_POST["G1487_C26601"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1487_C26602"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_C26602 = '".$_POST["G1487_C26602"]."'";
                $LsqlI .= $separador."G1487_C26602";
                $LsqlV .= $separador."'".$_POST["G1487_C26602"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1487_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1487_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1487_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1487_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1487_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1487_Usuario , G1487_FechaInsercion, G1487_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1487_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1487 WHERE G1487_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
