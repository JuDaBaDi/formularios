
<?php
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
	if(isset($_GET["accion_manual"])){
		$idMonoef=-19;
		$texto = "Busqueda No Aplica";
	}else{
		$idMonoef =-18;
		$texto = "No Suministra Información";
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<link rel="stylesheet" href="assets/plugins/WinPicker/dist/wickedpicker.min.css">
<script type="text/javascript" src="assets/plugins/WinPicker/dist/wickedpicker.min.js"></script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button"><?php echo $texto; ?></button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i>Buscar en toda la base de datos</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>

<script type="text/javascript">
<?php 
    $id_campan='';
    if(isset($_GET['id_campana_crm']) && $_GET['id_campana_crm'] != '0'){
        $id_campan='&id_campana_crm='.$_GET['id_campana_crm'];
    } 
?>

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }


    // Listen to message from child window
    bindEvent(window, 'message', function (e) {
        if(Array.isArray(e.data)){
            console.log(e.data);
            var keys=Object.keys(e.data[0].camposivr);
            var key=0;
            $.each(e.data[0].camposivr, function(i, valor){
                if($("#"+keys[key]).length > 0){
                    $("#"+keys[key]).val(valor); 
                }
                key++;
            });
            buscarRegistros(e.data);
//          $("#btnBuscar").click();
        }
    });
    
	$(function(){
    
        //DATEPICKER
        
        
        //TIMEPICKER
        
        $("#btnBuscar").click(function(){
            var valido=0;
            
            
            if(valido==0){
                var datos = $("#formId").serialize();
                $.ajax({
                    url     	: 'formularios/G2569/G2569_Funciones_Busqueda_Manual.php?action=GET_DATOS<?=$id_campan?>&agente=<?=getIdentificacionUser($_GET["token"])?>',
                    type		: 'post',
                    dataType	: 'json',
                    data		: datos,
                    success 	: function(datosq){
                        if(datosq[0].cantidad_registros > 1){
                            buscarRegistros(datosq);
                        }else if(datosq[0].cantidad_registros == 1){
                            $("#buscador").hide();
                            $("#botones").hide();
                            $("#resulados").hide();
                            var id = datosq[0].registros[0].G2569_ConsInte__b;
                            if(datosq[0].registros[0].id_muestra == null){
                                $.ajax({
                                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                    type		: 'post',
                                    data        : {id:id},
                                    success 	: function(data){
                                        <?php if(isset($_GET['token'])) { ?>
                                            $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']."'"; }?>);
                                        <?php } ?>
                                    }
                                });
                            }else{
                                <?php if(isset($_GET['token'])) { ?>
                                    $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']."'"; }?>);
                                <?php } ?>                        
                            }
                        }else{
                            adicionarRegistro(2,datosq[0].mensaje);
                        }
                    }
                });
            }
		});

		$("\#btnCancelar").click(function(){
			window.location = 'formularios/generados/PHP_Cerrar_Cancelar.php?token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php echo "&idMonoef=".$idMonoef;?><?php echo "&tiempoInicio=".date("Y-m-d H:i:s");?>';
		});
	});

    function buscarRegistros(datosq){
        var valores = null;
        var tabla_a_mostrar = '<div class="box box-default">'+
        '<div class="box-header">'+
            '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>'+
        '</div>'+
        '<div class="box-body">'+
            '<table class="table table-hover table-bordered" style="width:100%;">';
        tabla_a_mostrar += '<thead>';
        tabla_a_mostrar += '<tr>';
        tabla_a_mostrar += '  ';
        tabla_a_mostrar += '</tr>';
        tabla_a_mostrar += '</thead>';
        tabla_a_mostrar += '<tbody>';
        $.each(datosq[0].registros, function(i, item) {
            tabla_a_mostrar += '<tr idMuestra="'+ item.id_muestra +'" ConsInte="'+ item.G2569_ConsInte__b +'" class="EditRegistro">';
            tabla_a_mostrar += '';
            tabla_a_mostrar += '</tr>';
        });
        tabla_a_mostrar += '</tbody>';
        tabla_a_mostrar += '</table></div></div>';

        $("#resultadosBusqueda").html(tabla_a_mostrar);

        $(".EditRegistro").dblclick(function(){
            var id = $(this).attr("ConsInte");
            var muestra=$(this).attr("idMuestra");
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: 'Esta seguro de editar este registro?',
                type: "warning",
                confirmButtonText: "Editar registro",
                cancelButtonText : "No Editar registro",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        $("#buscador").hide();
                        $("#botones").hide();
                        $("#resulados").hide();
                        if(muestra == 'null'){
                            $.ajax({
                                url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                type		: 'post',
                                data        : {id:id},
                                success 	: function(data){
                                    <?php if(isset($_GET['token'])) { ?>
                                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']."'"; }?>);
                                    <?php } ?>
                                }
                            });
                        }else{
                            <?php if(isset($_GET['token'])) { ?>
                                $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']."'"; }?>);
                            <?php } ?>                        
                        }
                    }else{
                        $("#buscador").show();
                        $("#botones").show();
                        $("#resulados").show();
                    }
                });
            });    
    }
    
	function adicionarRegistro(tipo,mensaje){
    
        if(mensaje == null ){
            if(tipo == "1"){
                mensaje = "Desea adicionar un registro";
            }
            if(tipo == "2"){
                mensaje = "No se encontraron datos, desea adicionar un registro";
            }
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: mensaje,
                type: "warning",
                 confirmButtonText: "Adicionar registro",
                cancelButtonText : "Hacer otra busqueda",
                showCancelButton : true,
                closeOnConfirm : true
            },
            function(isconfirm){
                $("#buscador").hide();
                $("#botones").hide();
                $("#resulados").hide();
                if(isconfirm){
                    //JDBD - obtenemos los valores de la busqueda manual para enviarselos al formulario
                    var SinEnlace = "No";
                    $.ajax({
                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                    type		: 'post',
                    dataType	: 'json',
                    success 	: function(numeroIdnuevo){
                        <?php if(isset($_GET['token'])){ ?>
                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&nuevoregistro=true&SinEnlace='+SinEnlace);
                        <?php } ?>
                    }
                });
                }else{
                    limpiar();
                }
            });
        }else{
            swal("", mensaje, "warning");
        }
	}
	function limpiar(){
		$("#buscador").show();
		$("#buscador :input").each(function(){
			$(this).val("");
		});
		$("#resultadosBusqueda").html("");
		$("#botones").show();
		$("#resulados").show();
	}
</script>
