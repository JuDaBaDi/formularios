<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2569_ConsInte__b, G2569_FechaInsercion , G2569_Usuario ,  G2569_CodigoMiembro  , G2569_PoblacionOrigen , G2569_EstadoDiligenciamiento ,  G2569_IdLlamada , G2569_C50112 as principal ,G2569_C50112,G2569_C50111,G2569_C50113,G2569_C50114,G2569_C50115,G2569_C50116,G2569_C50117,G2569_C50118,G2569_C50119,G2569_C50120,G2569_C50121,G2569_C50657,G2569_C50658,G2569_C50659,G2569_C50660,G2569_C50656,G2569_C50105,G2569_C50106,G2569_C50107,G2569_C50598,G2569_C50599,G2569_C50600,G2569_C50601,G2569_C50602 FROM '.$BaseDatos.'.G2569 WHERE G2569_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2569_C50112'] = $key->G2569_C50112;

                $datos[$i]['G2569_C50111'] = $key->G2569_C50111;

                $datos[$i]['G2569_C50113'] = $key->G2569_C50113;

                $datos[$i]['G2569_C50114'] = $key->G2569_C50114;

                $datos[$i]['G2569_C50115'] = $key->G2569_C50115;

                $datos[$i]['G2569_C50116'] = explode(' ', $key->G2569_C50116)[0];

                $datos[$i]['G2569_C50117'] = $key->G2569_C50117;

                $datos[$i]['G2569_C50118'] = $key->G2569_C50118;

                $datos[$i]['G2569_C50119'] = $key->G2569_C50119;

                $datos[$i]['G2569_C50120'] = $key->G2569_C50120;

                $datos[$i]['G2569_C50121'] = $key->G2569_C50121;

                $datos[$i]['G2569_C50657'] = $key->G2569_C50657;

                $datos[$i]['G2569_C50658'] = $key->G2569_C50658;

                $datos[$i]['G2569_C50659'] = $key->G2569_C50659;

                $datos[$i]['G2569_C50660'] = $key->G2569_C50660;

                $datos[$i]['G2569_C50656'] = $key->G2569_C50656;

                $datos[$i]['G2569_C50105'] = $key->G2569_C50105;

                $datos[$i]['G2569_C50106'] = $key->G2569_C50106;

                $datos[$i]['G2569_C50107'] = $key->G2569_C50107;

                $datos[$i]['G2569_C50598'] = $key->G2569_C50598;

                $datos[$i]['G2569_C50599'] = $key->G2569_C50599;

                $datos[$i]['G2569_C50600'] = $key->G2569_C50600;

                $datos[$i]['G2569_C50601'] = $key->G2569_C50601;

                $datos[$i]['G2569_C50602'] = explode(' ', $key->G2569_C50602)[0];
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2569";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2569_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2569_ConsInte__b as id,  G2569_C50112 as camp1 , G2569_C50111 as camp2 
                     FROM ".$BaseDatos.".G2569  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2569_ConsInte__b as id,  G2569_C50112 as camp1 , G2569_C50111 as camp2  
                    FROM ".$BaseDatos.".G2569  JOIN ".$BaseDatos.".G2569_M".$_POST['muestra']." ON G2569_ConsInte__b = G2569_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2569_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2569_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2569_C50112 LIKE '%".$B."%' OR G2569_C50111 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2569_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2569");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2569_ConsInte__b, G2569_FechaInsercion , G2569_Usuario ,  G2569_CodigoMiembro  , G2569_PoblacionOrigen , G2569_EstadoDiligenciamiento ,  G2569_IdLlamada , G2569_C50112 as principal ,G2569_C50112,G2569_C50111,G2569_C50113,G2569_C50114,G2569_C50115,G2569_C50116,G2569_C50117,G2569_C50118,G2569_C50119,G2569_C50120,G2569_C50121,G2569_C50657,G2569_C50658,G2569_C50659,G2569_C50660,G2569_C50656,G2569_C50105,G2569_C50106, a.LISOPC_Nombre____b as G2569_C50107, b.LISOPC_Nombre____b as G2569_C50598, c.LISOPC_Nombre____b as G2569_C50599,G2569_C50600,G2569_C50601,G2569_C50602 FROM '.$BaseDatos.'.G2569 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2569_C50107 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2569_C50598 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2569_C50599';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2569_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2569_ConsInte__b , ($fila->G2569_C50112) , ($fila->G2569_C50111) , ($fila->G2569_C50113) , ($fila->G2569_C50114) , ($fila->G2569_C50115) , explode(' ', $fila->G2569_C50116)[0] , ($fila->G2569_C50117) , ($fila->G2569_C50118) , ($fila->G2569_C50119) , ($fila->G2569_C50120) , ($fila->G2569_C50121) , ($fila->G2569_C50657) , ($fila->G2569_C50658) , ($fila->G2569_C50659) , ($fila->G2569_C50660) , ($fila->G2569_C50656) , ($fila->G2569_C50105) , ($fila->G2569_C50106) , ($fila->G2569_C50107) , ($fila->G2569_C50598) , ($fila->G2569_C50599) , ($fila->G2569_C50600) , ($fila->G2569_C50601) , explode(' ', $fila->G2569_C50602)[0] );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2569 WHERE G2569_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2569";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2569_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2569_ConsInte__b as id,  G2569_C50112 as camp1 , G2569_C50111 as camp2  FROM '.$BaseDatos.'.G2569 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2569_ConsInte__b as id,  G2569_C50112 as camp1 , G2569_C50111 as camp2  
                    FROM ".$BaseDatos.".G2569  JOIN ".$BaseDatos.".G2569_M".$_POST['muestra']." ON G2569_ConsInte__b = G2569_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2569_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2569_C50112 LIKE "%'.$B.'%" OR G2569_C50111 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2569_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2569 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2569(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2569_C50112"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50112 = '".$_POST["G2569_C50112"]."'";
                $LsqlI .= $separador."G2569_C50112";
                $LsqlV .= $separador."'".$_POST["G2569_C50112"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50111"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50111 = '".$_POST["G2569_C50111"]."'";
                $LsqlI .= $separador."G2569_C50111";
                $LsqlV .= $separador."'".$_POST["G2569_C50111"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50113"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50113 = '".$_POST["G2569_C50113"]."'";
                $LsqlI .= $separador."G2569_C50113";
                $LsqlV .= $separador."'".$_POST["G2569_C50113"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50114"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50114 = '".$_POST["G2569_C50114"]."'";
                $LsqlI .= $separador."G2569_C50114";
                $LsqlV .= $separador."'".$_POST["G2569_C50114"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50115"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50115 = '".$_POST["G2569_C50115"]."'";
                $LsqlI .= $separador."G2569_C50115";
                $LsqlV .= $separador."'".$_POST["G2569_C50115"]."'";
                $validar = 1;
            }
             
 
            $G2569_C50116 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2569_C50116"])){    
                if($_POST["G2569_C50116"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2569_C50116"]);
                    if(count($tieneHora) > 1){
                        $G2569_C50116 = "'".$_POST["G2569_C50116"]."'";
                    }else{
                        $G2569_C50116 = "'".str_replace(' ', '',$_POST["G2569_C50116"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2569_C50116 = ".$G2569_C50116;
                    $LsqlI .= $separador." G2569_C50116";
                    $LsqlV .= $separador.$G2569_C50116;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2569_C50117"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50117 = '".$_POST["G2569_C50117"]."'";
                $LsqlI .= $separador."G2569_C50117";
                $LsqlV .= $separador."'".$_POST["G2569_C50117"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50118"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50118 = '".$_POST["G2569_C50118"]."'";
                $LsqlI .= $separador."G2569_C50118";
                $LsqlV .= $separador."'".$_POST["G2569_C50118"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50119"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50119 = '".$_POST["G2569_C50119"]."'";
                $LsqlI .= $separador."G2569_C50119";
                $LsqlV .= $separador."'".$_POST["G2569_C50119"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50120"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50120 = '".$_POST["G2569_C50120"]."'";
                $LsqlI .= $separador."G2569_C50120";
                $LsqlV .= $separador."'".$_POST["G2569_C50120"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50121"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50121 = '".$_POST["G2569_C50121"]."'";
                $LsqlI .= $separador."G2569_C50121";
                $LsqlV .= $separador."'".$_POST["G2569_C50121"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50657"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50657 = '".$_POST["G2569_C50657"]."'";
                $LsqlI .= $separador."G2569_C50657";
                $LsqlV .= $separador."'".$_POST["G2569_C50657"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50658"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50658 = '".$_POST["G2569_C50658"]."'";
                $LsqlI .= $separador."G2569_C50658";
                $LsqlV .= $separador."'".$_POST["G2569_C50658"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50659"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50659 = '".$_POST["G2569_C50659"]."'";
                $LsqlI .= $separador."G2569_C50659";
                $LsqlV .= $separador."'".$_POST["G2569_C50659"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50660"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50660 = '".$_POST["G2569_C50660"]."'";
                $LsqlI .= $separador."G2569_C50660";
                $LsqlV .= $separador."'".$_POST["G2569_C50660"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50656"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50656 = '".$_POST["G2569_C50656"]."'";
                $LsqlI .= $separador."G2569_C50656";
                $LsqlV .= $separador."'".$_POST["G2569_C50656"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50105"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50105 = '".$_POST["G2569_C50105"]."'";
                $LsqlI .= $separador."G2569_C50105";
                $LsqlV .= $separador."'".$_POST["G2569_C50105"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50106"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50106 = '".$_POST["G2569_C50106"]."'";
                $LsqlI .= $separador."G2569_C50106";
                $LsqlV .= $separador."'".$_POST["G2569_C50106"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50107"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50107 = '".$_POST["G2569_C50107"]."'";
                $LsqlI .= $separador."G2569_C50107";
                $LsqlV .= $separador."'".$_POST["G2569_C50107"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50598"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50598 = '".$_POST["G2569_C50598"]."'";
                $LsqlI .= $separador."G2569_C50598";
                $LsqlV .= $separador."'".$_POST["G2569_C50598"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50599"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50599 = '".$_POST["G2569_C50599"]."'";
                $LsqlI .= $separador."G2569_C50599";
                $LsqlV .= $separador."'".$_POST["G2569_C50599"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50600"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50600 = '".$_POST["G2569_C50600"]."'";
                $LsqlI .= $separador."G2569_C50600";
                $LsqlV .= $separador."'".$_POST["G2569_C50600"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2569_C50601"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_C50601 = '".$_POST["G2569_C50601"]."'";
                $LsqlI .= $separador."G2569_C50601";
                $LsqlV .= $separador."'".$_POST["G2569_C50601"]."'";
                $validar = 1;
            }
             
 
            $G2569_C50602 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2569_C50602"])){    
                if($_POST["G2569_C50602"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2569_C50602"]);
                    if(count($tieneHora) > 1){
                        $G2569_C50602 = "'".$_POST["G2569_C50602"]."'";
                    }else{
                        $G2569_C50602 = "'".str_replace(' ', '',$_POST["G2569_C50602"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2569_C50602 = ".$G2569_C50602;
                    $LsqlI .= $separador." G2569_C50602";
                    $LsqlV .= $separador.$G2569_C50602;
                    $validar = 1;
                }
            }

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2569_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2569_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2569_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2569_Usuario , G2569_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2569_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2569 WHERE G2569_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2569 SET G2569_UltiGest__b =-14, G2569_GesMasImp_b =-14, G2569_TipoReintentoUG_b =0, G2569_TipoReintentoGMI_b =0, G2569_EstadoUG_b =-14, G2569_EstadoGMI_b =-14, G2569_CantidadIntentos =0, G2569_CantidadIntentosGMI_b =0 WHERE G2569_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2570_ConsInte__b, G2570_C50125, G2570_C50126, G2570_C50127, G2570_C50137, G2570_C50140 FROM ".$BaseDatos.".G2570  ";

        $SQL .= " WHERE G2570_C50125 = '".$numero."'"; 

        $SQL .= " ORDER BY G2570_C50125";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2570_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2570_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2570_C50125)."</cell>";

                echo "<cell>". ($fila->G2570_C50126)."</cell>";

                echo "<cell>". ($fila->G2570_C50127)."</cell>";

                echo "<cell>". ($fila->G2570_C50137)."</cell>";

                echo "<cell>". ($fila->G2570_C50140)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2570 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2570(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2570_C50126"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2570_C50126 = '".$_POST["G2570_C50126"]."'";
                    $LsqlI .= $separador."G2570_C50126";
                    $LsqlV .= $separador."'".$_POST["G2570_C50126"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2570_C50127"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2570_C50127 = '".$_POST["G2570_C50127"]."'";
                    $LsqlI .= $separador."G2570_C50127";
                    $LsqlV .= $separador."'".$_POST["G2570_C50127"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2570_C50137"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2570_C50137 = '".$_POST["G2570_C50137"]."'";
                    $LsqlI .= $separador."G2570_C50137";
                    $LsqlV .= $separador."'".$_POST["G2570_C50137"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2570_C50140"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2570_C50140 = '".$_POST["G2570_C50140"]."'";
                    $LsqlI .= $separador."G2570_C50140";
                    $LsqlV .= $separador."'".$_POST["G2570_C50140"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2570_C50125 = $numero;
                    $LsqlU .= ", G2570_C50125 = ".$G2570_C50125."";
                    $LsqlI .= ", G2570_C50125";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2570_Usuario ,  G2570_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2570_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2570 WHERE  G2570_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
