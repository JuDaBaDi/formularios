<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2153_ConsInte__b, G2153_FechaInsercion , G2153_Usuario ,  G2153_CodigoMiembro  , G2153_PoblacionOrigen , G2153_EstadoDiligenciamiento ,  G2153_IdLlamada , G2153_C41323 as principal ,G2153_C41330,G2153_C41331,G2153_C41332,G2153_C41333,G2153_C41334,G2153_C41335,G2153_C41314,G2153_C41315,G2153_C41316,G2153_C41317,G2153_C41318,G2153_C41319,G2153_C41320,G2153_C41321,G2153_C41322,G2153_C41323,G2153_C41324,G2153_C41325,G2153_C41326,G2153_C41327,G2153_C41328,G2153_C41329,G2153_C41336,G2153_C41337,G2153_C41338,G2153_C41339,G2153_C41340,G2153_C41341,G2153_C41342,G2153_C41343,G2153_C41344,G2153_C41345,G2153_C41346,G2153_C41347,G2153_C41348,G2153_C41349,G2153_C41350,G2153_C41351,G2153_C41352,G2153_C41353,G2153_C41354,G2153_C41355,G2153_C41356,G2153_C41357,G2153_C41358,G2153_C41359,G2153_C41360,G2153_C41361,G2153_C41362,G2153_C41363,G2153_C41364,G2153_C41365,G2153_C41366,G2153_C41367,G2153_C41368,G2153_C41369,G2153_C41370,G2153_C41371,G2153_C41372,G2153_C41375,G2153_C41374,G2153_C41376,G2153_C41377,G2153_C41386,G2153_C41387,G2153_C41388,G2153_C44535,G2153_C49280,G2153_C44534,G2153_C41378, G2153_C52332, G2153_C52829 FROM '.$BaseDatos.'.G2153 WHERE G2153_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2153_C41330'] = $key->G2153_C41330;

                $datos[$i]['G2153_C41331'] = $key->G2153_C41331;

                $datos[$i]['G2153_C41332'] = explode(' ', $key->G2153_C41332)[0];

                $datos[$i]['G2153_C41333'] = explode(' ', $key->G2153_C41333)[0];

                $datos[$i]['G2153_C41334'] = $key->G2153_C41334;

                $datos[$i]['G2153_C41335'] = $key->G2153_C41335;

                $datos[$i]['G2153_C41314'] = $key->G2153_C41314;

                $datos[$i]['G2153_C41315'] = $key->G2153_C41315;

                $datos[$i]['G2153_C41316'] = explode(' ', $key->G2153_C41316)[0];
  
                $hora = '';
                if(!is_null($key->G2153_C41317)){
                    $hora = explode(' ', $key->G2153_C41317)[1];
                }

                $datos[$i]['G2153_C41317'] = $hora;

                $datos[$i]['G2153_C41318'] = $key->G2153_C41318;

                $datos[$i]['G2153_C41319'] = $key->G2153_C41319;

                $datos[$i]['G2153_C41320'] = $key->G2153_C41320;

                $datos[$i]['G2153_C41321'] = $key->G2153_C41321;

                $datos[$i]['G2153_C41322'] = $key->G2153_C41322;

                $datos[$i]['G2153_C41323'] = $key->G2153_C41323;

                $datos[$i]['G2153_C41324'] = $key->G2153_C41324;

                $datos[$i]['G2153_C41325'] = $key->G2153_C41325;

                $datos[$i]['G2153_C41326'] = $key->G2153_C41326;

                $datos[$i]['G2153_C41327'] = $key->G2153_C41327;

                $datos[$i]['G2153_C41328'] = $key->G2153_C41328;

                $datos[$i]['G2153_C41329'] = $key->G2153_C41329;

                $datos[$i]['G2153_C41336'] = $key->G2153_C41336;

                $datos[$i]['G2153_C41337'] = $key->G2153_C41337;

                $datos[$i]['G2153_C41338'] = explode(' ', $key->G2153_C41338)[0];

                $datos[$i]['G2153_C41339'] = explode(' ', $key->G2153_C41339)[0];

                $datos[$i]['G2153_C41340'] = $key->G2153_C41340;

                $datos[$i]['G2153_C41341'] = $key->G2153_C41341;

                $datos[$i]['G2153_C41342'] = $key->G2153_C41342;

                $datos[$i]['G2153_C41343'] = $key->G2153_C41343;

                $datos[$i]['G2153_C41344'] = $key->G2153_C41344;

                $datos[$i]['G2153_C41345'] = $key->G2153_C41345;

                $datos[$i]['G2153_C41346'] = $key->G2153_C41346;

                $datos[$i]['G2153_C41347'] = $key->G2153_C41347;

                $datos[$i]['G2153_C41348'] = $key->G2153_C41348;

                $datos[$i]['G2153_C41349'] = $key->G2153_C41349;

                $datos[$i]['G2153_C41350'] = $key->G2153_C41350;

                $datos[$i]['G2153_C41351'] = $key->G2153_C41351;

                $datos[$i]['G2153_C41352'] = $key->G2153_C41352;

                $datos[$i]['G2153_C41353'] = $key->G2153_C41353;

                $datos[$i]['G2153_C41354'] = $key->G2153_C41354;

                $datos[$i]['G2153_C41355'] = $key->G2153_C41355;

                $datos[$i]['G2153_C41356'] = $key->G2153_C41356;

                $datos[$i]['G2153_C41357'] = $key->G2153_C41357;

                $datos[$i]['G2153_C41358'] = $key->G2153_C41358;

                $datos[$i]['G2153_C41359'] = $key->G2153_C41359;

                $datos[$i]['G2153_C41360'] = $key->G2153_C41360;

                $datos[$i]['G2153_C41361'] = $key->G2153_C41361;

                $datos[$i]['G2153_C41362'] = $key->G2153_C41362;

                $datos[$i]['G2153_C41363'] = $key->G2153_C41363;

                $datos[$i]['G2153_C41364'] = $key->G2153_C41364;

                $datos[$i]['G2153_C41365'] = $key->G2153_C41365;

                $datos[$i]['G2153_C41366'] = $key->G2153_C41366;

                $datos[$i]['G2153_C41367'] = $key->G2153_C41367;

                $datos[$i]['G2153_C41368'] = $key->G2153_C41368;

                $datos[$i]['G2153_C41369'] = $key->G2153_C41369;

                $datos[$i]['G2153_C41370'] = $key->G2153_C41370;

                $datos[$i]['G2153_C41371'] = $key->G2153_C41371;

                $datos[$i]['G2153_C41372'] = $key->G2153_C41372;

                $datos[$i]['G2153_C41375'] = $key->G2153_C41375;

                $datos[$i]['G2153_C41374'] = $key->G2153_C41374;

                $datos[$i]['G2153_C41376'] = $key->G2153_C41376;

                $datos[$i]['G2153_C41377'] = $key->G2153_C41377;

                $datos[$i]['G2153_C41386'] = $key->G2153_C41386;

                $datos[$i]['G2153_C41387'] = explode(' ', $key->G2153_C41387)[0];

                $datos[$i]['G2153_C41388'] = explode(' ', $key->G2153_C41388)[0];

                $datos[$i]['G2153_C44535'] = $key->G2153_C44535;

                $datos[$i]['G2153_C49280'] = $key->G2153_C49280;

                $datos[$i]['G2153_C44534'] = $key->G2153_C44534;

                $datos[$i]['G2153_C41378'] = $key->G2153_C41378;

                $datos[$i]['G2153_C52332'] = $key->G2153_C52332;

                $datos[$i]['G2153_C52829'] = $key->G2153_C52829;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2153";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1855_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2153_ConsInte__b as id,  G2153_C41323 as camp1 , G2153_C41324 as camp2 
                     FROM ".$BaseDatos.".G2153  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2153_ConsInte__b as id,  G2153_C41323 as camp1 , G2153_C41324 as camp2  
                    FROM ".$BaseDatos.".G2153  JOIN ".$BaseDatos.".G2153_M".$_POST['muestra']." ON G2153_ConsInte__b = G2153_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2153_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2153_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2153_C41323 LIKE '%".$B."%' OR G2153_C41324 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2153_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2153");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2153_ConsInte__b, G2153_FechaInsercion , G2153_Usuario ,  G2153_CodigoMiembro  , G2153_PoblacionOrigen , G2153_EstadoDiligenciamiento ,  G2153_IdLlamada , G2153_C41323 as principal ,G2153_C41330,G2153_C41331,G2153_C41332,G2153_C41333,G2153_C41334, a.LISOPC_Nombre____b as G2153_C41335, b.LISOPC_Nombre____b as G2153_C41314, c.LISOPC_Nombre____b as G2153_C41315,G2153_C41316,G2153_C41317,G2153_C41318,G2153_C41319,G2153_C41320,G2153_C41321,G2153_C41322,G2153_C41323,G2153_C41324,G2153_C41325, d.LISOPC_Nombre____b as G2153_C41326,G2153_C41327,G2153_C41328, e.LISOPC_Nombre____b as G2153_C41329,G2153_C41336,G2153_C41337,G2153_C41338,G2153_C41339,G2153_C41340, f.LISOPC_Nombre____b as G2153_C41341, g.LISOPC_Nombre____b as G2153_C41342, h.LISOPC_Nombre____b as G2153_C41343,G2153_C41344, i.LISOPC_Nombre____b as G2153_C41345, j.LISOPC_Nombre____b as G2153_C41346, k.LISOPC_Nombre____b as G2153_C41347,G2153_C41348, l.LISOPC_Nombre____b as G2153_C41349, m.LISOPC_Nombre____b as G2153_C41350, n.LISOPC_Nombre____b as G2153_C41351,G2153_C41352, o.LISOPC_Nombre____b as G2153_C41353, p.LISOPC_Nombre____b as G2153_C41354, q.LISOPC_Nombre____b as G2153_C41355,G2153_C41356, r.LISOPC_Nombre____b as G2153_C41357, s.LISOPC_Nombre____b as G2153_C41358, t.LISOPC_Nombre____b as G2153_C41359,G2153_C41360, u.LISOPC_Nombre____b as G2153_C41361,G2153_C41362,G2153_C41363, v.LISOPC_Nombre____b as G2153_C41364,G2153_C41365,G2153_C41366, w.LISOPC_Nombre____b as G2153_C41367,G2153_C41368,G2153_C41369, x.LISOPC_Nombre____b as G2153_C41370,G2153_C41371, y.LISOPC_Nombre____b as G2153_C41372, z.LISOPC_Nombre____b as G2153_C41375, ab.LISOPC_Nombre____b as G2153_C41374,G2153_C41376,G2153_C41377,G2153_C41386,G2153_C41387,G2153_C41388,G2153_C44535, ac.LISOPC_Nombre____b as G2153_C49280,G2153_C44534,G2153_C41378 FROM '.$BaseDatos.'.G2153 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2153_C41335 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2153_C41314 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2153_C41315 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2153_C41326 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2153_C41329 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2153_C41341 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2153_C41342 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2153_C41343 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2153_C41345 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2153_C41346 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G2153_C41347 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as l ON l.LISOPC_ConsInte__b =  G2153_C41349 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as m ON m.LISOPC_ConsInte__b =  G2153_C41350 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as n ON n.LISOPC_ConsInte__b =  G2153_C41351 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as o ON o.LISOPC_ConsInte__b =  G2153_C41353 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as p ON p.LISOPC_ConsInte__b =  G2153_C41354 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as q ON q.LISOPC_ConsInte__b =  G2153_C41355 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as r ON r.LISOPC_ConsInte__b =  G2153_C41357 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as s ON s.LISOPC_ConsInte__b =  G2153_C41358 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as t ON t.LISOPC_ConsInte__b =  G2153_C41359 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as u ON u.LISOPC_ConsInte__b =  G2153_C41361 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as v ON v.LISOPC_ConsInte__b =  G2153_C41364 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as w ON w.LISOPC_ConsInte__b =  G2153_C41367 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as x ON x.LISOPC_ConsInte__b =  G2153_C41370 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as y ON y.LISOPC_ConsInte__b =  G2153_C41372 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as z ON z.LISOPC_ConsInte__b =  G2153_C41375 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ab ON ab.LISOPC_ConsInte__b =  G2153_C41374 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as ac ON ac.LISOPC_ConsInte__b =  G2153_C49280';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2153_C41317)){
                    $hora_a = explode(' ', $fila->G2153_C41317)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2153_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2153_ConsInte__b , ($fila->G2153_C41330) , ($fila->G2153_C41331) , explode(' ', $fila->G2153_C41332)[0] , explode(' ', $fila->G2153_C41333)[0] , ($fila->G2153_C41334) , ($fila->G2153_C41335) , ($fila->G2153_C41314) , ($fila->G2153_C41315) , explode(' ', $fila->G2153_C41316)[0] , $hora_a , ($fila->G2153_C41318) , ($fila->G2153_C41319) , ($fila->G2153_C41320) , ($fila->G2153_C41321) , ($fila->G2153_C41322) , ($fila->G2153_C41323) , ($fila->G2153_C41324) , ($fila->G2153_C41325) , ($fila->G2153_C41326) , ($fila->G2153_C41327) , ($fila->G2153_C41328) , ($fila->G2153_C41329) , ($fila->G2153_C41336) , ($fila->G2153_C41337) , explode(' ', $fila->G2153_C41338)[0] , explode(' ', $fila->G2153_C41339)[0] , ($fila->G2153_C41340) , ($fila->G2153_C41341) , ($fila->G2153_C41342) , ($fila->G2153_C41343) , ($fila->G2153_C41344) , ($fila->G2153_C41345) , ($fila->G2153_C41346) , ($fila->G2153_C41347) , ($fila->G2153_C41348) , ($fila->G2153_C41349) , ($fila->G2153_C41350) , ($fila->G2153_C41351) , ($fila->G2153_C41352) , ($fila->G2153_C41353) , ($fila->G2153_C41354) , ($fila->G2153_C41355) , ($fila->G2153_C41356) , ($fila->G2153_C41357) , ($fila->G2153_C41358) , ($fila->G2153_C41359) , ($fila->G2153_C41360) , ($fila->G2153_C41361) , ($fila->G2153_C41362) , ($fila->G2153_C41363) , ($fila->G2153_C41364) , ($fila->G2153_C41365) , ($fila->G2153_C41366) , ($fila->G2153_C41367) , ($fila->G2153_C41368) , ($fila->G2153_C41369) , ($fila->G2153_C41370) , ($fila->G2153_C41371) , ($fila->G2153_C41372) , ($fila->G2153_C41375) , ($fila->G2153_C41374) , ($fila->G2153_C41376) , ($fila->G2153_C41377) , ($fila->G2153_C41386) , explode(' ', $fila->G2153_C41387)[0] , explode(' ', $fila->G2153_C41388)[0] , ($fila->G2153_C44535) , ($fila->G2153_C49280) , ($fila->G2153_C44534) , ($fila->G2153_C41378) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2153 WHERE G2153_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2153";

            $resRegPro_t = $mysqli->query($strRegPro_t);
            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1855_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }

            }else{
                $strRegProp_t = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2153_ConsInte__b as id,  G2153_C41323 as camp1 , G2153_C41324 as camp2  FROM '.$BaseDatos.'.G2153 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2153_ConsInte__b as id,  G2153_C41323 as camp1 , G2153_C41324 as camp2  
                    FROM ".$BaseDatos.".G2153  JOIN ".$BaseDatos.".G2153_M".$_POST['muestra']." ON G2153_ConsInte__b = G2153_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2153_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2153_C41323 LIKE "%'.$B.'%" OR G2153_C41324 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2153_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2153 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2153(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2153_C41330"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41330 = '".$_POST["G2153_C41330"]."'";
                $LsqlI .= $separador."G2153_C41330";
                $LsqlV .= $separador."'".$_POST["G2153_C41330"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41331"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41331 = '".$_POST["G2153_C41331"]."'";
                $LsqlI .= $separador."G2153_C41331";
                $LsqlV .= $separador."'".$_POST["G2153_C41331"]."'";
                $validar = 1;
            }
             
 
            $G2153_C41332 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2153_C41332"])){    
                if($_POST["G2153_C41332"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2153_C41332"]);
                    if(count($tieneHora) > 1){
                        $G2153_C41332 = "'".$_POST["G2153_C41332"]."'";
                    }else{
                        $G2153_C41332 = "'".str_replace(' ', '',$_POST["G2153_C41332"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2153_C41332 = ".$G2153_C41332;
                    $LsqlI .= $separador." G2153_C41332";
                    $LsqlV .= $separador.$G2153_C41332;
                    $validar = 1;
                }
            }
 
            $G2153_C41333 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2153_C41333"])){    
                if($_POST["G2153_C41333"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2153_C41333"]);
                    if(count($tieneHora) > 1){
                        $G2153_C41333 = "'".$_POST["G2153_C41333"]."'";
                    }else{
                        $G2153_C41333 = "'".str_replace(' ', '',$_POST["G2153_C41333"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2153_C41333 = ".$G2153_C41333;
                    $LsqlI .= $separador." G2153_C41333";
                    $LsqlV .= $separador.$G2153_C41333;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2153_C41334"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41334 = '".$_POST["G2153_C41334"]."'";
                $LsqlI .= $separador."G2153_C41334";
                $LsqlV .= $separador."'".$_POST["G2153_C41334"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41335"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41335 = '".$_POST["G2153_C41335"]."'";
                $LsqlI .= $separador."G2153_C41335";
                $LsqlV .= $separador."'".$_POST["G2153_C41335"]."'";
                $validar = 1;
            }
             
 
            $G2153_C41314 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2153_C41314 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2153_C41314 = ".$G2153_C41314;
                    $LsqlI .= $separador." G2153_C41314";
                    $LsqlV .= $separador.$G2153_C41314;
                    $validar = 1;

                    
                }
            }
 
            $G2153_C41315 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2153_C41315 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2153_C41315 = ".$G2153_C41315;
                    $LsqlI .= $separador." G2153_C41315";
                    $LsqlV .= $separador.$G2153_C41315;
                    $validar = 1;
                }
            }
 
            $G2153_C41316 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2153_C41316 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2153_C41316 = ".$G2153_C41316;
                    $LsqlI .= $separador." G2153_C41316";
                    $LsqlV .= $separador.$G2153_C41316;
                    $validar = 1;
                }
            }
 
            $G2153_C41317 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2153_C41317 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2153_C41317 = ".$G2153_C41317;
                    $LsqlI .= $separador." G2153_C41317";
                    $LsqlV .= $separador.$G2153_C41317;
                    $validar = 1;
                }
            }
 
            $G2153_C41318 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2153_C41318 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2153_C41318 = ".$G2153_C41318;
                    $LsqlI .= $separador." G2153_C41318";
                    $LsqlV .= $separador.$G2153_C41318;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2153_C41319"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41319 = '".$_POST["G2153_C41319"]."'";
                $LsqlI .= $separador."G2153_C41319";
                $LsqlV .= $separador."'".$_POST["G2153_C41319"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41320"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41320 = '".$_POST["G2153_C41320"]."'";
                $LsqlI .= $separador."G2153_C41320";
                $LsqlV .= $separador."'".$_POST["G2153_C41320"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41321"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41321 = '".$_POST["G2153_C41321"]."'";
                $LsqlI .= $separador."G2153_C41321";
                $LsqlV .= $separador."'".$_POST["G2153_C41321"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41322"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41322 = '".$_POST["G2153_C41322"]."'";
                $LsqlI .= $separador."G2153_C41322";
                $LsqlV .= $separador."'".$_POST["G2153_C41322"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41323"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41323 = '".$_POST["G2153_C41323"]."'";
                $LsqlI .= $separador."G2153_C41323";
                $LsqlV .= $separador."'".$_POST["G2153_C41323"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41324"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41324 = '".$_POST["G2153_C41324"]."'";
                $LsqlI .= $separador."G2153_C41324";
                $LsqlV .= $separador."'".$_POST["G2153_C41324"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41325"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41325 = '".$_POST["G2153_C41325"]."'";
                $LsqlI .= $separador."G2153_C41325";
                $LsqlV .= $separador."'".$_POST["G2153_C41325"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41326"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41326 = '".$_POST["G2153_C41326"]."'";
                $LsqlI .= $separador."G2153_C41326";
                $LsqlV .= $separador."'".$_POST["G2153_C41326"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41327"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41327 = '".$_POST["G2153_C41327"]."'";
                $LsqlI .= $separador."G2153_C41327";
                $LsqlV .= $separador."'".$_POST["G2153_C41327"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41328"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41328 = '".$_POST["G2153_C41328"]."'";
                $LsqlI .= $separador."G2153_C41328";
                $LsqlV .= $separador."'".$_POST["G2153_C41328"]."'";
                $validar = 1;
            }

            if(isset($_POST["G2153_C52332"])){

                if ($_POST["G2153_C52332"] != "") {
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2153_C52332 = '".$_POST["G2153_C52332"]."'";
                    $LsqlI .= $separador."G2153_C52332";
                    $LsqlV .= $separador."'".$_POST["G2153_C52332"]."'";
                    $validar = 1;
                }
            }

            if(isset($_POST["G2153_C52829"])){

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFechaActual = date("Y-m-d H:i:s");
                
                if ($_POST["G2153_C52829"] != "") {


                    $LsqlU .= $separador."G2153_C52829 = '".$_POST["G2153_C52829"]."'";
                    $validar = 1;

                }
                    $LsqlI .= $separador."G2153_C52829";
                    $LsqlV .= $separador."'".$strFechaActual."'";
                    $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41329"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41329 = '".$_POST["G2153_C41329"]."'";
                $LsqlI .= $separador."G2153_C41329";
                $LsqlV .= $separador."'".$_POST["G2153_C41329"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41336"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41336 = '".$_POST["G2153_C41336"]."'";
                $LsqlI .= $separador."G2153_C41336";
                $LsqlV .= $separador."'".$_POST["G2153_C41336"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41337"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41337 = '".$_POST["G2153_C41337"]."'";
                $LsqlI .= $separador."G2153_C41337";
                $LsqlV .= $separador."'".$_POST["G2153_C41337"]."'";
                $validar = 1;
            }
             
 
            $G2153_C41338 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2153_C41338"])){    
                if($_POST["G2153_C41338"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2153_C41338"]);
                    if(count($tieneHora) > 1){
                        $G2153_C41338 = "'".$_POST["G2153_C41338"]."'";
                    }else{
                        $G2153_C41338 = "'".str_replace(' ', '',$_POST["G2153_C41338"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2153_C41338 = ".$G2153_C41338;
                    $LsqlI .= $separador." G2153_C41338";
                    $LsqlV .= $separador.$G2153_C41338;
                    $validar = 1;
                }
            }
 
            $G2153_C41339 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2153_C41339"])){    
                if($_POST["G2153_C41339"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2153_C41339"]);
                    if(count($tieneHora) > 1){
                        $G2153_C41339 = "'".$_POST["G2153_C41339"]."'";
                    }else{
                        $G2153_C41339 = "'".str_replace(' ', '',$_POST["G2153_C41339"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2153_C41339 = ".$G2153_C41339;
                    $LsqlI .= $separador." G2153_C41339";
                    $LsqlV .= $separador.$G2153_C41339;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2153_C41340"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41340 = '".$_POST["G2153_C41340"]."'";
                $LsqlI .= $separador."G2153_C41340";
                $LsqlV .= $separador."'".$_POST["G2153_C41340"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41341"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41341 = '".$_POST["G2153_C41341"]."'";
                $LsqlI .= $separador."G2153_C41341";
                $LsqlV .= $separador."'".$_POST["G2153_C41341"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41342"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41342 = '".$_POST["G2153_C41342"]."'";
                $LsqlI .= $separador."G2153_C41342";
                $LsqlV .= $separador."'".$_POST["G2153_C41342"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41343"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41343 = '".$_POST["G2153_C41343"]."'";
                $LsqlI .= $separador."G2153_C41343";
                $LsqlV .= $separador."'".$_POST["G2153_C41343"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41344"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41344 = '".$_POST["G2153_C41344"]."'";
                $LsqlI .= $separador."G2153_C41344";
                $LsqlV .= $separador."'".$_POST["G2153_C41344"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41345"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41345 = '".$_POST["G2153_C41345"]."'";
                $LsqlI .= $separador."G2153_C41345";
                $LsqlV .= $separador."'".$_POST["G2153_C41345"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41346"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41346 = '".$_POST["G2153_C41346"]."'";
                $LsqlI .= $separador."G2153_C41346";
                $LsqlV .= $separador."'".$_POST["G2153_C41346"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41347"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41347 = '".$_POST["G2153_C41347"]."'";
                $LsqlI .= $separador."G2153_C41347";
                $LsqlV .= $separador."'".$_POST["G2153_C41347"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41348"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41348 = '".$_POST["G2153_C41348"]."'";
                $LsqlI .= $separador."G2153_C41348";
                $LsqlV .= $separador."'".$_POST["G2153_C41348"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41349"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41349 = '".$_POST["G2153_C41349"]."'";
                $LsqlI .= $separador."G2153_C41349";
                $LsqlV .= $separador."'".$_POST["G2153_C41349"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41350"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41350 = '".$_POST["G2153_C41350"]."'";
                $LsqlI .= $separador."G2153_C41350";
                $LsqlV .= $separador."'".$_POST["G2153_C41350"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41351"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41351 = '".$_POST["G2153_C41351"]."'";
                $LsqlI .= $separador."G2153_C41351";
                $LsqlV .= $separador."'".$_POST["G2153_C41351"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41352"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41352 = '".$_POST["G2153_C41352"]."'";
                $LsqlI .= $separador."G2153_C41352";
                $LsqlV .= $separador."'".$_POST["G2153_C41352"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41353"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41353 = '".$_POST["G2153_C41353"]."'";
                $LsqlI .= $separador."G2153_C41353";
                $LsqlV .= $separador."'".$_POST["G2153_C41353"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41354"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41354 = '".$_POST["G2153_C41354"]."'";
                $LsqlI .= $separador."G2153_C41354";
                $LsqlV .= $separador."'".$_POST["G2153_C41354"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41355"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41355 = '".$_POST["G2153_C41355"]."'";
                $LsqlI .= $separador."G2153_C41355";
                $LsqlV .= $separador."'".$_POST["G2153_C41355"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41356"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41356 = '".$_POST["G2153_C41356"]."'";
                $LsqlI .= $separador."G2153_C41356";
                $LsqlV .= $separador."'".$_POST["G2153_C41356"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41357"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41357 = '".$_POST["G2153_C41357"]."'";
                $LsqlI .= $separador."G2153_C41357";
                $LsqlV .= $separador."'".$_POST["G2153_C41357"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41358"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41358 = '".$_POST["G2153_C41358"]."'";
                $LsqlI .= $separador."G2153_C41358";
                $LsqlV .= $separador."'".$_POST["G2153_C41358"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41359"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41359 = '".$_POST["G2153_C41359"]."'";
                $LsqlI .= $separador."G2153_C41359";
                $LsqlV .= $separador."'".$_POST["G2153_C41359"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41360"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41360 = '".$_POST["G2153_C41360"]."'";
                $LsqlI .= $separador."G2153_C41360";
                $LsqlV .= $separador."'".$_POST["G2153_C41360"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41361"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41361 = '".$_POST["G2153_C41361"]."'";
                $LsqlI .= $separador."G2153_C41361";
                $LsqlV .= $separador."'".$_POST["G2153_C41361"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41362"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41362 = '".$_POST["G2153_C41362"]."'";
                $LsqlI .= $separador."G2153_C41362";
                $LsqlV .= $separador."'".$_POST["G2153_C41362"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41363"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41363 = '".$_POST["G2153_C41363"]."'";
                $LsqlI .= $separador."G2153_C41363";
                $LsqlV .= $separador."'".$_POST["G2153_C41363"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41364"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41364 = '".$_POST["G2153_C41364"]."'";
                $LsqlI .= $separador."G2153_C41364";
                $LsqlV .= $separador."'".$_POST["G2153_C41364"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41365"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41365 = '".$_POST["G2153_C41365"]."'";
                $LsqlI .= $separador."G2153_C41365";
                $LsqlV .= $separador."'".$_POST["G2153_C41365"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41366"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41366 = '".$_POST["G2153_C41366"]."'";
                $LsqlI .= $separador."G2153_C41366";
                $LsqlV .= $separador."'".$_POST["G2153_C41366"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41367"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41367 = '".$_POST["G2153_C41367"]."'";
                $LsqlI .= $separador."G2153_C41367";
                $LsqlV .= $separador."'".$_POST["G2153_C41367"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41368"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41368 = '".$_POST["G2153_C41368"]."'";
                $LsqlI .= $separador."G2153_C41368";
                $LsqlV .= $separador."'".$_POST["G2153_C41368"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41369"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41369 = '".$_POST["G2153_C41369"]."'";
                $LsqlI .= $separador."G2153_C41369";
                $LsqlV .= $separador."'".$_POST["G2153_C41369"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41370"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41370 = '".$_POST["G2153_C41370"]."'";
                $LsqlI .= $separador."G2153_C41370";
                $LsqlV .= $separador."'".$_POST["G2153_C41370"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41371"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41371 = '".$_POST["G2153_C41371"]."'";
                $LsqlI .= $separador."G2153_C41371";
                $LsqlV .= $separador."'".$_POST["G2153_C41371"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41372"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41372 = '".$_POST["G2153_C41372"]."'";
                $LsqlI .= $separador."G2153_C41372";
                $LsqlV .= $separador."'".$_POST["G2153_C41372"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41375"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41375 = '".$_POST["G2153_C41375"]."'";
                $LsqlI .= $separador."G2153_C41375";
                $LsqlV .= $separador."'".$_POST["G2153_C41375"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41374 = '".$_POST["G2153_C41374"]."'";
                $LsqlI .= $separador."G2153_C41374";
                $LsqlV .= $separador."'".$_POST["G2153_C41374"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41376 = '".$_POST["G2153_C41376"]."'";
                $LsqlI .= $separador."G2153_C41376";
                $LsqlV .= $separador."'".$_POST["G2153_C41376"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41377 = '".$_POST["G2153_C41377"]."'";
                $LsqlI .= $separador."G2153_C41377";
                $LsqlV .= $separador."'".$_POST["G2153_C41377"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41386"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41386 = '".$_POST["G2153_C41386"]."'";
                $LsqlI .= $separador."G2153_C41386";
                $LsqlV .= $separador."'".$_POST["G2153_C41386"]."'";
                $validar = 1;
            }
             
 
            $G2153_C41387 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2153_C41387"])){    
                if($_POST["G2153_C41387"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2153_C41387"]);
                    if(count($tieneHora) > 1){
                        $G2153_C41387 = "'".$_POST["G2153_C41387"]."'";
                    }else{
                        $G2153_C41387 = "'".str_replace(' ', '',$_POST["G2153_C41387"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2153_C41387 = ".$G2153_C41387;
                    $LsqlI .= $separador." G2153_C41387";
                    $LsqlV .= $separador.$G2153_C41387;
                    $validar = 1;
                }
            }
 
            $G2153_C41388 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2153_C41388"])){    
                if($_POST["G2153_C41388"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2153_C41388"]);
                    if(count($tieneHora) > 1){
                        $G2153_C41388 = "'".$_POST["G2153_C41388"]."'";
                    }else{
                        $G2153_C41388 = "'".str_replace(' ', '',$_POST["G2153_C41388"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2153_C41388 = ".$G2153_C41388;
                    $LsqlI .= $separador." G2153_C41388";
                    $LsqlV .= $separador.$G2153_C41388;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2153_C44535"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C44535 = '".$_POST["G2153_C44535"]."'";
                $LsqlI .= $separador."G2153_C44535";
                $LsqlV .= $separador."'".$_POST["G2153_C44535"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C49280"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C49280 = '".$_POST["G2153_C49280"]."'";
                $LsqlI .= $separador."G2153_C49280";
                $LsqlV .= $separador."'".$_POST["G2153_C49280"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C44534"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C44534 = '".$_POST["G2153_C44534"]."'";
                $LsqlI .= $separador."G2153_C44534";
                $LsqlV .= $separador."'".$_POST["G2153_C44534"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2153_C41378"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_C41378 = '".$_POST["G2153_C41378"]."'";
                $LsqlI .= $separador."G2153_C41378";
                $LsqlV .= $separador."'".$_POST["G2153_C41378"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_TipNo_Efe_b, MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $reintento = $dataMonoEf->MONOEF_TipNo_Efe_b;
                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2153_C41315 = ".$reintento;
                    $LsqlI .= $separador."G2153_C41315";
                    $LsqlV .= $separador.$reintento;
                    $validar = 1;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2153_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2153_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2153_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2153_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2153_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2153_Usuario , G2153_FechaInsercion, G2153_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2153_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2153 WHERE G2153_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            // echo $Lsql; die();
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
