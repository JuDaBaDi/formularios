<?php 
    /*
        Document   : index
        Created on : 2020-05-14 21:42:50
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjE1Mw==  
    */
    $url_crud =  "formularios/G2153/G2153_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G2153/G2153_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41323" id="LblG2153_C41323">NOMBRE</label>
								<input type="text" class="form-control input-sm" id="G2153_C41323" value=""  name="G2153_C41323"  placeholder="NOMBRE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41324" id="LblG2153_C41324">CEDULA</label>
								<input type="text" class="form-control input-sm" id="G2153_C41324" value=""  name="G2153_C41324"  placeholder="CEDULA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41325" id="LblG2153_C41325">CELULAR</label>
								<input type="text" class="form-control input-sm" id="G2153_C41325" value=""  name="G2153_C41325"  placeholder="CELULAR">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41326" id="LblG2153_C41326">CIUDAD 1</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41326" id="G2153_C41326">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2291 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41327" id="LblG2153_C41327">FIJO</label>
								<input type="text" class="form-control input-sm" id="G2153_C41327" value=""  name="G2153_C41327"  placeholder="FIJO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41328" id="LblG2153_C41328">DIRECCION</label>
								<input type="text" class="form-control input-sm" id="G2153_C41328" value=""  name="G2153_C41328"  placeholder="DIRECCION">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41329" id="LblG2153_C41329">TIPO DE CLIENTE</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41329" id="G2153_C41329">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2270 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41330" id="LblG2153_C41330">TITULAR LINEA (JURIDICO)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41330" value=""  name="G2153_C41330"  placeholder="TITULAR LINEA (JURIDICO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41331" id="LblG2153_C41331">CEDULA TITULAR (JURIDICO)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41331" value=""  name="G2153_C41331"  placeholder="CEDULA TITULAR (JURIDICO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2153_C41332" id="LblG2153_C41332">FECHA DE EXPEDICION (JURIDICO)</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2153_C41332" id="G2153_C41332" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2153_C41333" id="LblG2153_C41333">FECHA DE NACIMIENTO (JURIDICO)</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2153_C41333" id="G2153_C41333" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41334" id="LblG2153_C41334">CORREO (JURIDICO)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41334" value=""  name="G2153_C41334"  placeholder="CORREO (JURIDICO)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41335" id="LblG2153_C41335">SCORE (JURIDICO)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41335" id="G2153_C41335">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2275 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41336" id="LblG2153_C41336">TITULAR LINEA (NATURAL)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41336" value=""  name="G2153_C41336"  placeholder="TITULAR LINEA (NATURAL)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41337" id="LblG2153_C41337">CEDULA TITULAR (NATURAL)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41337" value=""  name="G2153_C41337"  placeholder="CEDULA TITULAR (NATURAL)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2153_C41338" id="LblG2153_C41338">FECHA DE EXPEDICION (NATURAL)</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2153_C41338" id="G2153_C41338" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2153_C41339" id="LblG2153_C41339">FECHA DE NACIMIENTO (NATURAL)</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2153_C41339" id="G2153_C41339" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41340" id="LblG2153_C41340">CORREO (NATURAL)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41340" value=""  name="G2153_C41340"  placeholder="CORREO (NATURAL)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41341" id="LblG2153_C41341">SCORE (NATURAL)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41341" id="G2153_C41341">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2275 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41386" id="LblG2153_C41386">CANTIDAD DE LINEAS</label>
								<input type="text" class="form-control input-sm" id="G2153_C41386" value=""  name="G2153_C41386"  placeholder="CANTIDAD DE LINEAS">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2153_C41387" id="LblG2153_C41387">FECHA DE CORTE</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2153_C41387" id="G2153_C41387" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2153_C41388" id="LblG2153_C41388">FECHA DE ENTREGA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G2153_C41388" id="G2153_C41388" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C44535" id="LblG2153_C44535">NUMERO A PORTAR</label>
								<input type="text" class="form-control input-sm" id="G2153_C44535" value=""  name="G2153_C44535"  placeholder="NUMERO A PORTAR">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C49280" id="LblG2153_C49280">FRANJA HORARIA</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C49280" id="G2153_C49280">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2824 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C44534" id="LblG2153_C44534">FRANJA HORARIA (se eliminara)</label>
								<input type="text" class="form-control input-sm" id="G2153_C44534" value="" disabled name="G2153_C44534"  placeholder="FRANJA HORARIA (se eliminara)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2153_C41378" id="LblG2153_C41378">OBSERVACIONES GESTION</label>
                                <textarea class="form-control input-sm" name="G2153_C41378" id="G2153_C41378"  value="" placeholder="OBSERVACIONES GESTION"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41342" id="LblG2153_C41342">OPERADOR ACTUAL (LINEA 1)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41342" id="G2153_C41342">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41343" id="LblG2153_C41343">PLAN (LINEA 1)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41343" id="G2153_C41343">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41344" id="LblG2153_C41344">NIP (LINEA 1)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41344" value=""  name="G2153_C41344"  placeholder="NIP (LINEA 1)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41345" id="LblG2153_C41345">VALOR PLAN (LINEA 1)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41345" id="G2153_C41345">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41346" id="LblG2153_C41346">OPERADOR ACTUAL (LINEA 2)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41346" id="G2153_C41346">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41347" id="LblG2153_C41347">PLAN (LINEA 2)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41347" id="G2153_C41347">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41348" id="LblG2153_C41348">NIP (LINEA 2)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41348" value=""  name="G2153_C41348"  placeholder="NIP (LINEA 2)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41349" id="LblG2153_C41349">VALOR DEL PLAN (LINEA 2)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41349" id="G2153_C41349">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41350" id="LblG2153_C41350">OPERADOR ACTUAL (LINEA 3)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41350" id="G2153_C41350">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41351" id="LblG2153_C41351">PLAN (LINEA 3)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41351" id="G2153_C41351">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41352" id="LblG2153_C41352">NIP (LINEA 3)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41352" value=""  name="G2153_C41352"  placeholder="NIP (LINEA 3)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41353" id="LblG2153_C41353">VALOR DEL PLAN (LINEA 3)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41353" id="G2153_C41353">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41354" id="LblG2153_C41354">OPERADOR ACTUAL (LINEA 4)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41354" id="G2153_C41354">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41355" id="LblG2153_C41355">PLAN (LINEA 4)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41355" id="G2153_C41355">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41356" id="LblG2153_C41356">NIP (LINEA 4)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41356" value=""  name="G2153_C41356"  placeholder="NIP (LINEA 4)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41357" id="LblG2153_C41357">VALOR DEL PLAN (LINEA 4)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41357" id="G2153_C41357">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41358" id="LblG2153_C41358">OPERADOR ACTUAL (LINEA 5)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41358" id="G2153_C41358">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2276 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41359" id="LblG2153_C41359">PLAN (LINEA 5)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41359" id="G2153_C41359">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2277 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41360" id="LblG2153_C41360">NIP (LINEA 5)</label>
								<input type="text" class="form-control input-sm" id="G2153_C41360" value=""  name="G2153_C41360"  placeholder="NIP (LINEA 5)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41361" id="LblG2153_C41361">VALOR DEL PLAN (LINEA 5)</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41361" id="G2153_C41361">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2284 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41362" id="LblG2153_C41362">PERSONA DE CONTACTO</label>
								<input type="text" class="form-control input-sm" id="G2153_C41362" value=""  name="G2153_C41362"  placeholder="PERSONA DE CONTACTO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41363" id="LblG2153_C41363">NUMERO DE CONTACTO</label>
								<input type="text" class="form-control input-sm" id="G2153_C41363" value=""  name="G2153_C41363"  placeholder="NUMERO DE CONTACTO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41364" id="LblG2153_C41364">TIPO DE VIA 1</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41364" id="G2153_C41364">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2285 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41365" id="LblG2153_C41365">NUMERO DE VIA 1</label>
								<input type="text" class="form-control input-sm" id="G2153_C41365" value=""  name="G2153_C41365"  placeholder="NUMERO DE VIA 1">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41366" id="LblG2153_C41366">LITERAL 1</label>
								<input type="text" class="form-control input-sm" id="G2153_C41366" value=""  name="G2153_C41366"  placeholder="LITERAL 1">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41367" id="LblG2153_C41367">TIPO DE VIA 2</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41367" id="G2153_C41367">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2285 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41368" id="LblG2153_C41368">NUMERO DE VIA 2</label>
								<input type="text" class="form-control input-sm" id="G2153_C41368" value=""  name="G2153_C41368"  placeholder="NUMERO DE VIA 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41369" id="LblG2153_C41369">LITERAL 2</label>
								<input type="text" class="form-control input-sm" id="G2153_C41369" value=""  name="G2153_C41369"  placeholder="LITERAL 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41370" id="LblG2153_C41370">ORIENTACION</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41370" id="G2153_C41370">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2286 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41371" id="LblG2153_C41371">NUMERO DE VIA 3</label>
								<input type="text" class="form-control input-sm" id="G2153_C41371" value=""  name="G2153_C41371"  placeholder="NUMERO DE VIA 3">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41372" id="LblG2153_C41372">ORIENTACION 2</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41372" id="G2153_C41372">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2286 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41375" id="LblG2153_C41375">DEPARTAMENTO</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41375" id="G2153_C41375">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2292 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2153_C41374" id="LblG2153_C41374">CIUDAD</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2153_C41374" id="G2153_C41374">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2291 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2153_C41376" id="LblG2153_C41376">RECAUDO CLIENTE</label>
								<input type="text" class="form-control input-sm" id="G2153_C41376" value=""  name="G2153_C41376"  placeholder="RECAUDO CLIENTE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2153_C41377" id="LblG2153_C41377">OBSERVACION DIRECCION</label>
                                <textarea class="form-control input-sm" name="G2153_C41377" id="G2153_C41377"  value="" placeholder="OBSERVACION DIRECCION"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2153/G2153_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2153_C41332").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2153_C41333").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2153_C41316").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2153_C41338").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2153_C41339").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2153_C41387").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G2153_C41388").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G2153_C41317").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para CIUDAD 1 

    $("#G2153_C41326").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE CLIENTE 

    $("#G2153_C41329").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SCORE (JURIDICO) 

    $("#G2153_C41335").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para SCORE (NATURAL) 

    $("#G2153_C41341").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para FRANJA HORARIA 

    $("#G2153_C49280").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 1) 

    $("#G2153_C41342").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 1) 

    $("#G2153_C41343").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR PLAN (LINEA 1) 

    $("#G2153_C41345").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 2) 

    $("#G2153_C41346").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 2) 

    $("#G2153_C41347").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 2) 

    $("#G2153_C41349").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 3) 

    $("#G2153_C41350").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 3) 

    $("#G2153_C41351").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 3) 

    $("#G2153_C41353").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 4) 

    $("#G2153_C41354").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 4) 

    $("#G2153_C41355").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 4) 

    $("#G2153_C41357").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para OPERADOR ACTUAL (LINEA 5) 

    $("#G2153_C41358").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para PLAN (LINEA 5) 

    $("#G2153_C41359").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para VALOR DEL PLAN (LINEA 5) 

    $("#G2153_C41361").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE VIA 1 

    $("#G2153_C41364").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPO DE VIA 2 

    $("#G2153_C41367").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ORIENTACION 

    $("#G2153_C41370").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ORIENTACION 2 

    $("#G2153_C41372").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para DEPARTAMENTO 

    $("#G2153_C41375").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2291' , idPadre : $(this).val() },
            success : function(data){
                $("#G2153_C41374").html(data);
            }
        });
        
    });

    //function para CIUDAD 

    $("#G2153_C41374").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
